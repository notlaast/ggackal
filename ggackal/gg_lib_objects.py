#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# DESCRIPTION
#	All the primary OO classes that define the model and contain
#	its results are defined here.
#
# TODO
# - implement more magic methods
#	http://farmdev.com/src/secrets/magicmethod/index.html
#	http://minhhh.github.io/posts/a-guide-to-pythons-magic-methods
#	- could access filters better..
#		trial[<str>, <xxx>] -> trial.__getitem__(items) -> items[0] could be 'r' or 'm' and items[1] could be desired match
#		network(attributes, matches) -> network.__call__(*args) -> network.filter(*args)
#		molecules(<str>) -> molecules.__call__(<str>) -> molecules.get_molecule(<str>)
#	- a molecule could be compared better..
#		molecule.__eq__(self, other) -> performs (self.name == other)
#
# standard library
from __future__ import print_function
from __future__ import absolute_import
import os
import sys
import re
import random
import copy
if sys.version_info[0] == 3:
	izip = zip
else:
	from itertools import izip
# third-party
import scipy
from progressbar import *
# local
if not os.path.dirname(os.path.abspath(__file__)) in sys.path:
	sys.path.append(os.path.dirname(os.path.abspath(__file__)))
from gg_lib_sys import *
from gg_lib_reac import *
from gg_lib_physics import *
from gg_lib_formatter import *
from gg_lib_plot import *
import gg_lib_cfilter

if sys.version_info[0] == 3:
	xrange = range




class trial(object):
	"""
	Provides a class for describing the full trial for the
	current model. It should hold all the classes below.
	"""
	def __init__(self):
		self.trial = self
		self.network = None # see trial.init_network()
		self.molecules = None # see trial.init_molecules()
		self.getmol = None # see trial.init_molecules()
		self.physgrid = None # see trial.init_physgrid()
		self.allssfactors = None # see gg_main.rungg()
		self.solution = solution(self) # see gg_main.runsolver()
	
	def printsomething(self, *args):
		print(args, type(args))
	
	def init_network(self, fname):
		"""
		Loads a network from a filename, and sets up most of the properties
		as possible from the single file.
		"""
		if params['verbosity'] > 0: print("initializing the network")
		self.network = network(self)
		self.network.load_file(fname)
		self.network.process_kinetics()
		self.network.set_special_reactions()
	
	def init_molecules(self):
		"""
		Sets up a 'molecules' object based on a defined network. Also
		loads the initial abundances and surface properties from the
		other two input files.
		"""
		if params['verbosity'] > 0: print("initializing the molecules")
		self.molecules = molecules(self)
		self.molecules.load_from_network(self.network)
		self.molecules.set_initial_abundances(params['initialabundfname'], params['initadjfname'])
		self.molecules.set_surface_properties(params['surfspeciesfname'])
		self.getmol = self.molecules.get_molecule
	
	def init_physgrid(self):
		"""
		Sets the physical properties for each iteration, based on the
		input parameters.
		"""
		if params['verbosity'] > 0: print("initializing the physical properties")
		self.physgrid = physgrid(self)
		self.physgrid.set_times()
		self.physgrid.set_densities()
		self.physgrid.set_av()
		self.physgrid.set_temperatures()
	
	def set_conservation_constraints(self):
		"""
		Redefines certain molecules' DEs, based on what is requested
		to be conserved. See the input parameter 'particlestoconserve'
		for more details.
		"""
		if params['verbosity'] > 0: print("defining the conservation expressions to maintain the laws of conservation of mass/charge")
		if (not params['particlestoconserve']) or (params['particlestoconserve'] == "none"):
			return
		elif params['particlestoconserve'] == "electrons":
			self.getmol('E1m').de = 0
		elif params['particlestoconserve'] == "charge":
			raise NotImplementedError("this is broken at the moment, sorry")
			hoparticles['charge'] = []
			hoparticles['charge'].append(ggdvar('charge'))
			hoparticles['charge'].append(0)
			for s in los:
				hoparticles['charge'][1] += hop[s]['var']*hop[s]['charge']
		elif params['particlestoconserve'] == "all":
			raise NotImplementedError("this is broken at the moment, sorry")
			# first process the charges
			hoparticles['charge'] = []
			hoparticles['charge'].append(ggdvar('charge'))
			hoparticles['charge'].append(0)
			for s in allspecies:
				hoparticles['charge'][1] += hop[s]['var']*hop[s]['charge']
			# then process the grains
			hoparticles['grains'] = []
			hoparticles['grains'].append(ggdvar('grains'))
			hoparticles['grains'].append(0)
			hoparticles['grains'][1] = hop['G0']['var'] + hop['G1m']['var']
			# finally process the chemical elements
			elements = [
				'hydrogen'	,
				'helium'	,
				'carbon'	,
				'nitrogen'	,
				'oxygen'	,
				'fluorine'	,
				'sodium'	,
				'magnesium'	,
				'silicon'	,
				'phos'		,
				'sulfur'	,
				'chlorine'	,
				'iron'
			]
			for elem in elements:
				hoparticles[elem] = []
				hoparticles[elem].append(ggdvar(elem))
				hoparticles[elem].append(0)
			toskip = ['E1m','G0','G1m']
			for s in allspecies:
				# skip electrons and grains
				if s in toskip: continue
				for elem,stoi in list(hop[s]['stoi'].items()):
					hoparticles[elem][1] += hop[s]['var']*stoi
		else:
			raise SyntaxError("ERROR: could not interpret particlestoconserve: %s" % params['particlestoconserve'])
		
		
	def getssdata(self, fname):
		r"""
		Reads the self-shielding info from an input file.
		
		INPUTS:
		- ``f`` -- filename of input file containing self-shielding info, with the following shape:
			# comments are allowed like this (and also blank lines)
			# H2 self-shielding factors
			N(H2)       Theta
			x			y
			..			..
			# CO self-shielding factors
			N(CO)       Theta1
			x			y
			..			..
			# H2-shielding factors for CO
			N(H2)       Theta2
			x			y
			..			..
			# dust-shielding factors for CO
			Av          Theta3
			x			y
			..			..
		
		OUTPUT:
		hoss - H which contains the following keys:
			'<string>xy' - is a list of tuples (x,y) containing the ssdata
			'get<string>' - a callable object that takes (x) as an argument
			and returns the interpolated (y)
			where <string> is either 'h2theta','cotheta1','cotheta2','cotheta3',
		"""
		
		# open the data file
		try:
			filein = open(fname, 'r')
		except IOError:
			raise IOError("Tried to load the self-shielding file, but %s isn't a file!" % fname)
		
		# initialize containers
		hoss = {}
		h2thetax = []
		h2thetay = []
		cotheta1x = []
		cotheta1y = []
		cotheta2x = []
		cotheta2y = []
		cotheta3x = []
		cotheta3y = []
		
		# defines the headers of the ssdata
		headers = [
			("N(H2)","Theta"),
			("N(CO)","Theta1"),
			("N(H2)","Theta2"),
			("Av","Theta3")
		]
		# initialize section
		section = 0
		
		# loop through file
		linenum = 0
		for line in filein:
			linenum += 1
			# strip comments & blank lines
			line = re.sub(r'#.*', r'', line)
			if re.match(r'^\s*$', line): continue
			
			# remove whitespace padding and split to columns
			line = line.strip()
			x,y = line.split()
			
			# make note of current section by matching headers
			if (x,y) == headers[0]:
				if verbosity > 2: print("found the H2 self-shielding factors")
				section = 1
				continue
			if (x,y) == headers[1]:
				if verbosity > 2: print("found the CO self-shielding factors")
				section = 2
				continue
			if (x,y) == headers[2]:
				if verbosity > 2: print("found H2-shielding factors for CO")
				section = 3
				continue
			if (x,y) == headers[3]:
				if verbosity > 2: print("found dust-shielding factors for CO")
				section = 4
				continue
			
			# append values to appropriate arrays
			#	note that only loglog-scaled xy data gives a reasonable across the range
			if section == 1:
				h2thetax.append(float(x))
				h2thetay.append(float(y))
			elif section == 2:
				cotheta1x.append(float(x))
				cotheta1y.append(float(y))
			elif section == 3:
				cotheta2x.append(float(x))
				cotheta2y.append(float(y))
			elif section == 4:
				cotheta3x.append(float(x))
				cotheta3y.append(float(y))
			else:
				continue
		
		# populate the dict with the xy-data
		hoss["h2thetax"] = np.asarray(h2thetax); hoss["h2thetay"] = np.asarray(h2thetay)
		hoss["cotheta1x"] = np.asarray(cotheta1x); hoss["cotheta1y"] = np.asarray(cotheta1y)
		hoss["cotheta2x"] = np.asarray(cotheta2x); hoss["cotheta2y"] = np.asarray(cotheta2y)
		hoss["cotheta3x"] = np.asarray(cotheta3x); hoss["cotheta3y"] = np.asarray(cotheta3y)
		hoss["h2thetaxy"] = list(zip(h2thetax, h2thetay))
		hoss["cotheta1xy"] = list(zip(cotheta1x, cotheta1y))
		hoss["cotheta2xy"] = list(zip(cotheta2x, cotheta2y))
		hoss["cotheta3xy"] = list(zip(cotheta3x, cotheta3y))
		
		# define a set of splines and callable functions
		hoss["h2theta_spl"] = scipy.interpolate.splrep(hoss["h2thetax"], hoss["h2thetay"])
		hoss["cotheta1_spl"] = scipy.interpolate.splrep(hoss["cotheta1x"], hoss["cotheta1y"])
		hoss["cotheta2_spl"] = scipy.interpolate.splrep(hoss["cotheta2x"], hoss["cotheta2y"])
		hoss["cotheta3_spl"] = scipy.interpolate.splrep(hoss["cotheta3x"], hoss["cotheta3y"])
		def h2theta_impl(x):
			return scipy.interpolate.splev(x, hoss["h2theta_spl"])
		def cotheta1_impl(x):
			return scipy.interpolate.splev(x, hoss["cotheta1_spl"])
		def cotheta2_impl(x):
			return scipy.interpolate.splev(x, hoss["cotheta2_spl"])
		def cotheta3_impl(x):
			return scipy.interpolate.splev(x, hoss["cotheta3_spl"])
		def dh2theta_impl(x):
			return scipy.interpolate.splev(x, hoss["h2theta_spl"], der=1)
		def dcotheta1_impl(x):
			return scipy.interpolate.splev(x, hoss["cotheta1_spl"], der=1)
		def dcotheta2_impl(x):
			return scipy.interpolate.splev(x, hoss["cotheta2_spl"], der=1)
		def dcotheta3_impl(x):
			return scipy.interpolate.splev(x, hoss["cotheta3_spl"], der=1)
		hoss["h2theta_impl"] = h2theta_impl
		hoss["cotheta1_impl"] = cotheta1_impl
		hoss["cotheta2_impl"] = cotheta2_impl
		hoss["cotheta3_impl"] = cotheta3_impl
		hoss["dh2theta_impl"] = dh2theta_impl
		hoss["dcotheta1_impl"] = dcotheta1_impl
		hoss["dcotheta2_impl"] = dcotheta2_impl
		hoss["dcotheta3_impl"] = dcotheta3_impl
		
		###
		# define a set of functions that evaluates the expressions only when the argument is real
		# see more:
		#	https://github.com/sympy/sympy/wiki/About-implementing-special-functions
		#	http://stackoverflow.com/questions/24718313/define-numerical-evaluation-of-a-derivative-of-a-sympy-function
		#
		if mathlib == "symengine":
			msg = "The custom-defined functions for the self-shielding"
			msg += " data do not work with the symengine backend! You"
			msg += " must either switch the mathlib to sympy, or change"
			msg += " ssmodel to an approximation model."
			raise NotImplementedError(msg)
		class h2theta_sym(sympy.Function): # was sympy.Function
			_imp_ = staticmethod(h2theta_impl)
			def fdiff(self, argindex=1):
				return dh2theta_sym(self.args[0])
		class dh2theta_sym(sympy.Function):
			_imp_ = staticmethod(dh2theta_impl)
		
		class cotheta1_sym(sympy.Function):
			_imp_ = staticmethod(cotheta1_impl)
			def fdiff(self, argindex=1):
				return dcotheta1_sym(self.args[0])
		class dcotheta1_sym(sympy.Function):
			_imp_ = staticmethod(dcotheta1_impl)
		
		class cotheta2_sym(sympy.Function):
			_imp_ = staticmethod(cotheta2_impl)
			def fdiff(self, argindex=1):
				return dcotheta2_sym(self.args[0])
		class dcotheta2_sym(sympy.Function):
			_imp_ = staticmethod(dcotheta2_impl)
		
		class cotheta3_sym(sympy.Function):
			_imp_ = staticmethod(cotheta3_impl)
			def fdiff(self, argindex=1):
				return dcotheta3_sym(self.args[0])
		class dcotheta3_sym(sympy.Function):
			_imp_ = staticmethod(dcotheta3_impl)
		
		hoss["geth2theta"] = h2theta_sym
		hoss["getcotheta1"] = cotheta1_sym
		hoss["getcotheta2"] = cotheta2_sym
		hoss["getcotheta3"] = cotheta3_sym
		
		x = symengine.Symbol('x')
		hoss["geth2theta_lambda"] = sympy.lambdify([x], h2theta_sym(x))
		hoss["getcotheta1_lambda"] = sympy.lambdify([x], cotheta1_sym(x))
		hoss["getcotheta2_lambda"] = sympy.lambdify([x], cotheta2_sym(x))
		hoss["getcotheta3_lambda"] = sympy.lambdify([x], cotheta3_sym(x))
		#test = sympy.Function('h2theta_sym')
		#print test, type(test), dir(test)
		#print symengine.diff(test(x), x)
		#print test(4.636e15)
		#raise Exception
		#test = symengine.Lambdify([x], [test(x)])
		
		if (params['verbosity'] > 1) and (params['plot_ssdata']):
			for theta in ["h2theta","cotheta1","cotheta2","cotheta3"]:
				if params['verbosity'] > 2: print("processing", theta)
				# define data and strings to access them
				xstring = theta + "x"
				xdata = hoss[xstring]
				ystring = theta + "y"
				ydata = hoss[ystring]
				xystring = theta + "xy"
				xydata = hoss[xystring]
				intstring = "get" + theta + "_lambda"
				# define fit and residuals
				y_int = []
				y_res = []
				y_err = []
				print("-"*20)
				print(" xval      yval      y_fit     y_res     y_err")
				for xval,yval in xydata:
					intval = hoss[intstring](xval)
					y_int.append(intval)
					res = intval - yval
					y_res.append(res)
					try:
						err = res/yval
					except ZeroDivisionError:
						err = 1
					y_err.append(err)
					xval = "{:>9.2e}".format(float(xval))
					yval = "{:>9.2e}".format(float(yval))
					intval = "{:>9.2e}".format(float(intval))
					res = "{:>9.2e}".format(float(res))
					err = "{:>9.2e}".format(float(err))
					print(xval, yval, intval, res, err)
				if params['plot_lib'] in ["gnuplot", "pyqtgraph"]:
					print("showing the ssdata plots for", theta)
					# plot orig vs fit
					xmin = xdata[1]*.95
					xmax = xdata[-2]*1.05
					scale="log"
					genericgnuplot(
						xdict={"x":xdata},
						ydict={"y":ydata,
							"fit":[hoss[intstring](x) for x in xdata]},
						scale=scale)
		
		self.allssfactors = hoss
	


class network(object):
	"""
	Provides a class for describing a network of reactions.
	
	This class essentially holds a list self.reactions, and
	is associated with a number of methods that process this
	list. Each entry in self.reactions is of class reactions.
	"""
	def __init__(self, parent=None, fname=None):
		self.parent = parent
		self.trial = None
		if parent is not None:
			self.trial = parent.trial
		self.fname = None
		self.reactions = [] # see network.load_file()
		self.specrxns = {} # see network.set_special_reactions()
		
		if fname is not None:
			self.load_file(fname)
			self.process_kinetics()
		
	
	def load_file(self, fname):
		"""
		Loads a rate file containing all the reactions intended to be
		used for the current trial.
		
		:param fname: the name of the rate file
		:type fname: str
		"""
		self.fname = fname
		if params['verbosity'] > 0: print("loading reactions from file: %s" % fname)
		
		# open the network file
		if not os.path.isfile(fname):
			raise IOError("Tried to load the network, but %s isn't a file!" % fname)
		file = open(fname, 'r')
		
		# scan through file, line-by-line
		if params['verbosity'] > 2.5: print("contents is:")
		linenum = 0
		numTOT = 1
		for line in file:
			linenum += 1
			if params['verbosity'] > 2: print("\nchecking line #%s..." % (linenum), end=' ')
			origline = str(line)
			
			# strip comments
			fullline = line.split("#")
			line = fullline[0]
			# skip blank lines
			if re.match(r'^\s*$', line):
				if params['verbosity'] > 2: print("skipping it")
				continue
			
			# use this flag to keep track of whether to skip the entry
			toskip = False
			
			# tidy up line format
			line = line.strip() # whitespace padding
			line = line.replace('\t', ' ') # remove tabs
			line = re.sub(r'\s+', ' ', line) # remove repeated whitespace
			line = line.rstrip('\r\n') # strip newline character
			
			# check against excluded patterns
			if len(params['ignored_patterns']) and any(pattern in line for pattern in params['ignored_patterns']):
				continue
			
			curreaction = reaction(self)
			curreaction.linenum = linenum
			curreaction.rstring = origline.rstrip()
			curreaction.load_jcl(line)
			if len(fullline) > 1:
				curreaction.comment = fullline[1].strip()
			
			# assigns total number numTOT to reaction
			# 	note that zfill is used for zero-padding
			curreaction.numTOT = str(numTOT).zfill(4)
			
			if params['duplicatereactionaction'] in ["override","ignore"]:
				rtypestr = "RTYPE=%s" % (origline.split('RTYPE=')[1].split(' ')[0])
				duprxns = gg_lib_cfilter.cfilter(
					["reaction", "rstring"],
					[curreaction.reaction, [rtypestr]],
					self.reactions)
				if len(duprxns):
					# build useful message
					msg = "found a duplicate reaction:\n"
					msg += "\tcurrent  : %s (line %s)\n" % (curreaction.rstring, curreaction.linenum)
					msg += "\tmatch(es):\n"
					for dupr in rxnlist:
						msg += "\t           %s (line %s)\n" % (dupr.rstring, dupr.linenum)
					if (params['duplicatereactionaction'] == "override") and (not curreaction.errors):
						msg += "\taction: deleting the old one"
						numTOT -= 1
						del self.reactions[self.reactions.index(rxnlist[0])]
					elif params['duplicatereactionaction'] == "ignore":
						msg += "\taction: ignoring it"
						curreaction.errors = True
					if params['warn_duplicaterxns'] and (params['verbosity'] > 1):
						print(msg)
			
			if not curreaction.errors:
				del curreaction.errors
				self.reactions.append(curreaction)
				numTOT += 1
		
		# close file after finishing the loop
		file.close()
		if params['verbosity'] >= 2: print("finished with file: %s" % fname)
	
	def process_kinetics(self):
		"""
		Simply provides a wrapper for fixing various kinetics
		data immediately upon loading a file.
		
		It calls self.fix_reaction_stoichiometry(), self.set_kinetics(),
		and self.set_brs().
		"""
		if params['verbosity'] > 0: print("processing the kinetics")
		#self.fix_reaction_stoichiometry() # fix stoichiometry (i.e. "2*H" is equivalent to "H + H")
		self.set_kinetics() # assigns RTYPE numbers to each reaction
		self.set_brs()
	
	def fix_reaction_stoichiometry(self):
		"""
		Replaces stoichiometric coefficients with additional reactant/product
		entries. More specifically, it would convert "A + 2*B" to "A + B + B".
		"""
		if params['verbosity'] > 0: print("fixing stoichiometry")
		
		# for each reaction entry
		for rxn in self.reactions:
			# step through all the reactants
			index = 0
			for r in rxn.reactants:
				# if entry looks like NUM*SPECIES
				match = re.match(r'(\d+)[*](\S*)', r)
				if match:
					# determine the NUM
					stoi = matchobj.groups()[1]
					# determine the SPECIES
					species = matchobj.groups()[2]
					# replace the current entry..
					rxn.reactants[index] = species
					# append new entries for (stoiCoeff-1) times
					for num in range(1, stoi):
						rxn.reactants.append(species)
				index += 1
			# do again for the products..
			index = 0
			for p in rxn.products:
				match = re.match(r'(\d+)[*](\S*)', p)
				if match:
					stoi = matchobj.groups()[1]
					species = matchobj.groups()[2]
					rxn.products[index] = species
					for num in range(1, stoi):
						rxn.products.append(species)
				index += 1
	
	def set_kinetics(self):
		"""
		Loops through all reactions and sets kinetics information based on
		the strings from the rate file.
		
		Note that, where simply parsing the 'kinetics' string could be
		done on a per-reaction basis, index numbers are also assigned
		on a full-network basis, so this should remain a network's
		duty.
		"""
		if params['verbosity'] > 0: print("setting kinetics information")
			
		# define "special" (i.e. ignored) reactants/products
		extra_species = {
			1  : (' + cr',None),
			4  : (None,' + photon'),
			8  : (None,' + photon'),
			13 : (' + photon',None),
			15 : (' (evap)',None),
			16 : (' + cr',None), # (evap)
			17 : (' + cr',None), # (PD)
			18 : (' + cr',None), # (ind-PD)
			19 : (' + photon',None), # (PD)
			20 : (' + photon',None), # (ind-PD)
			80 : (' + photon',None), # (PD)
			99 : (' (accr)',None),
		}
		extra_species_tex = {
			1  : (' + cr',None),
			4  : (None,' + h$\\nu$'),
			8  : (None,' + h$\\nu$'),
			13 : (' + h$\\nu$',None),
			15 : (' (evap)',None),
			16 : (' + cr',None), # (evap)
			17 : (' + cr',None), # (PD)
			18 : (' + cr',None), # (ind-PD)
			19 : (' + h$\\nu$',None), # (PD)
			20 : (' + h$\\nu$',None), # (ind-PD)
			80 : (' + h$\\nu$',None), # (PD)
			99 : (' (accr)',None),
		}
		
		# assign rtype and coefficients
		for rxn in self.reactions:
			for entry in rxn.kinetics:
				s = entry.split('=')
				if s[0].lower() == "rtype": rxn.rtype = int(s[1])
				if s[0].lower() == "k1": rxn.k1 = float(s[1])
				if s[0].lower() == "k2": rxn.k2 = float(s[1])
				if s[0].lower() == "k3": rxn.k3 = float(s[1])
		
		# sort by rtype and then total reaction number
		self.reactions.sort(key=lambda x: (x.rtype, x.numTOT))
		
		# now step through all reactions again
		indexRTYPE = 1
		prevRTYPE = self.reactions[0].rtype
		for rxn in self.reactions:
			# determine curRTYPE
			curRTYPE = rxn.rtype
			reaction = rxn.reaction
			reactants,products = reaction.split(' -> ')
			# define human-readable reaction string
			rxn.extra_species = extra_species.setdefault(curRTYPE, (None,None))
			rxn.extra_species_tex = extra_species_tex.setdefault(curRTYPE, (None,None))
			r,p = rxn.extra_species
			if r: reactants += r
			if p: products += p
			reaction = reactants + " -> " + products
			rxn.reaction = reaction
			
			# assign RTYPE numbers as long as RTYPE is same as last
			if curRTYPE == prevRTYPE:
				indexRTYPE += 1
				rxn.numRTYPE = str(indexRTYPE).zfill(4)
			# otherwise start the numbers anew
			else:
				indexRTYPE = 1
				rxn.numRTYPE = str(indexRTYPE).zfill(4)
			
			# generate values for next entry
			prevRTYPE = curRTYPE
	
	def set_brs(self):
		"""
		Loops through all reactions and sets branching ratios for rtype=14.
		
		The br should range from 0 to 1, in case other reactions may
		compete for partial rates wrt to a single possible total.
		"""
		if params['verbosity'] > 0: print("settings branching ratios")
		
		# initialize all BRs
		for rxn in self.reactions:
			rxn.br = 1
		
		# sort by rtype and then reactants
		self.reactions.sort(key=lambda x: (x.rtype, x.reactants))
		
		# define subroutine to check if all elements are equal within a list
		def allareidentical(iterator):
			if (len(iterator) > 1) and (len(set(iterator)) == 1):
				return True
			else:
				return False
		
		# initialize hash(es) to keep track of BRs
		npaths = {}
		# loop through reactions and keep track of # of pathways
		for rxn in self.reactions:
			if (rxn.rtype == 14) and (rxn.products[0][0]=='g'):
				# because lists are not hashable, one just converts list to a string
				key = "".join(rxn.reactants)
				try:
					npaths[key] += 1
				except KeyError:
					npaths[key] = 1
		# now loop through reactions and set BRs
		for rxn in self.reactions:
			if (rxn.rtype == 14) and (rxn.products[0][0]=='g'):
				key = "".join(rxn.reactants)
				rxn.br *= 1.0 / npaths[key]
				if allareidentical(rxn.reactants): # also account for same species reactions
					rxn.br /= 2
	
	def set_special_reactions(self):
		"""
		Identifies special reactions which are specially modified in the future.
		
		Note that the network object has the attribute 'specrxns' added,
		which is a dictionary with key/value pairs: <DESCRIPTION> => <RXN>.
		These are used during the definition of reaction rate laws.
		"""
		if params['verbosity'] > 0: print("setting special reactions")
		
		self.specrxns = []
		# loop through all reactions
		for rxn in self.reactions:
			# identify dummy H2 formation reactions (only if rtype=14 is enabled)
			if (rxn.rtype == 0) and params['usesurfchem'] and params['usertype14']:
				if (rxn.reactants == ["H","H","G0"]) and (rxn.products == ["H2","G0"]):
					self.specrxns.append(rxn)
				if (rxn.reactants == ["H","H","G1m"]) and (rxn.products == ["H2","G1m"]):
					self.specrxns.append(rxn)
			# identify H2 and CO photodissociation reactions for self-shielding modifications
			if rxn.rtype == 13 and params['ssmodel']:
				if (rxn.reactants == ["H2"]) and (rxn.products == ["H","H"]):
					self.specrxns.append(rxn)
				if (rxn.reactants == ["CO"]) and (rxn.products == ["C","O"]):
					self.specrxns.append(rxn)
	
	def set_ratelaws(self):
		"""
		Looks through all the reactions of the network, and assigns the
		rate laws with respect to their reactants and other kinetics.
		
		Note that for most reactions, the generic functions
		getratefromrtypeXX() are used to define each rate. However, the
		special reactions are also processed separately here.
		"""
		if params['verbosity'] > 0:
			print("defining the rate laws")
			# define progress bar
			pbwidgets = [Percentage(), Bar('>')]
			pbar = ProgressBar(widgets=pbwidgets, maxval=len(self.reactions)).start()
		self.rtype14rxns = self.filter('rtype', 14)
		self.reactions.sort(key=lambda x: x.numTOT)
		for i,r in enumerate(self.reactions):
			if r in self.specrxns:
				if params['verbosity'] > 0: print("working on special reaction '%s'" % r.reaction)
				# catch H2 photodissociation reaction
				if r.rtype == 13 and (r.reactants == ["H2"]) and (r.products == ["H","H"]):
					if params['ssmodel'] == "table":
						# calculate column density & rate assign according to Lee et al. 1996 (Eqn.2)
						h2cd = params['H2cd0'] + params['totHcd0'] * (av-1) * self.trial.getmol('H2').var
						r.ratelaw = 2.54e-11 * self.trial.allssfactors['geth2theta'](h2cd) * gasdensity
						#r.ratelaw = 2.54e-11 * 1e-19 * self.trial.molecules.get_molecule('H2').var
					elif params['ssmodel'] == "approx":
						# determine parameterized approximation based on Draine & Bertoldi 1996
						b = 3 # km/s = 1e5 cm/s
						h2cd = params['H2cd0'] + self.trial.getmol('H2').var * params['distfromedge0']
						h2cd /= 5e14
						r.ratelaw = 2.54e-11 * params['uvgasscalefac'] # base rate
						r.ratelaw *= exp(-3.74*av) # account for dust shielding
						r.ratelaw *= 0.965/(1+h2cd/float(b))**2 + 0.035/(1+h2cd)**0.5 * exp(-8.5e-4 * (1+h2cd)**0.5) # account for self-shielding
						r.ratelaw *= gasdensity
					else:
						msg = "could not parse the ssmodel correctly!"
						raise Exception()
				# catch CO photodissociation reaction
				if r.rtype == 13 and (r.reactants == ["CO"]) and (r.products == ["C","O"]):
					if params['ssmodel'] == "table":
						h2cd = params['H2cd0'] + params['totHcd0']*(av-1)*self.trial.getmol('H2').var
						cocd = params['COcd0'] + params['totHcd0']*(av-1)*self.trial.getmol('CO').var
						r.ratelaw = 1.03e-10 * self.trial.allssfactors['getcotheta1'](cocd) * self.trial.allssfactors['getcotheta2'](h2cd) * self.trial.allssfactors['getcotheta3'](av)
						#r.ratelaw = 3.77e-13 * self.trial.molecules.get_molecule('CO').var	# for constant Av=1, density=3e3
						r.ratelaw *= gasdensity
					elif params['ssmodel'] == "approx":
						h2cd = params['H2cd0'] + self.trial.getmol('H2').var * params['distfromedge0']
						cocd = params['COcd0'] + self.trial.getmol('CO').var * params['distfromedge0']
						r.ratelaw = 1.03e-10 * exp(-2.54*av) * self.trial.getmol('CO').var * gasdensity # base rate
						if mathlib == "symengine":
							# note that this fit is based on the numerical approximation of erf(x) ~= tanh(x*sqrt(pi)*log(2))
							r.ratelaw *= (1 - erf((log(cocd)/log(10)-15.63)/0.6586))*0.06844 + (1 - erf((log(cocd)/log(10)-15.06)/1.305))*0.4329 # self-shielding
							r.ratelaw *= (1 - erf((log(h2cd)/log(10)-19.19)/1.909))*0.1477 + (1 - erf((log(h2cd)/log(10)-21.14)/0.622))*0.3407 # H2 shielding
							r.ratelaw *= (1 - erf((log(av)/log(10)+0.9127)/1.217))*0.5016 * exp(-2.531*23.09**(log(av)/log(10))) # dust shielding
						else:
							r.ratelaw *= (1 - erf((log(cocd)/log(10)-15.28)/0.8702))*0.2287 + (1 - erf((log(cocd)/log(10)-15.03)/1.557))*0.2725 # self-shielding
							r.ratelaw *= (1 - erf((log(h2cd)/log(10)-19.26)/1.861))*0.1543 + (1 - erf((log(h2cd)/log(10)-21.15)/0.547))*0.334 # H2 shielding
							r.ratelaw *= (1 - erf((log(av)/log(10)+0.6641)/1.35))*0.4995 * exp(-2.999*11.51**(log(av)/log(10))) # dust shielding
					else:
						msg = "could not parse the ssmodel correctly!"
						raise Exception()
				# catch dummy H2 formation reactions
				if params['usesurfchem'] and params['usertype14']:
					if(r.reactants == ["H","H","G0"]) and (r.products == ["H2","G0"]):
						r.ratelaw = 0
					if (r.reactants == ["H","H","G1m"]) and (r.products == ["H2","G1m"]):
						r.ratelaw = 0
			else:
				r.set_ratelaw()
			if params['verbosity'] > 0: pbar.update(i)
		if params['verbosity'] > 0: pbar.finish()
	
	def set_des(self):
		"""
		For each molecule in each reaction, the rxn's rate law is added/
		subtracted from the molecule's total DE. The properties 'numform'
		and 'numdest' are also used to keep track of how many reactions
		are involved on a per-molecule basis.
		"""
		if params['verbosity'] > 0:
			print("defining the DEs")
			# define progress bar
			pbwidgets = [Percentage(), Bar('>')]
			pbar = ProgressBar(widgets=pbwidgets, maxval=len(self.reactions)).start()
		for i,rxn in enumerate(self.reactions):
			if not rxn.ratelaw: continue # skips zero-valued rates
			for m in rxn.reactants:
				if m in rxn.products:
					print("\twarning: found species %s as a reactant but also a product of rxn '%s'" % (m, rxn.reaction))
					continue
				self.trial.getmol(m).de -= rxn.ratelaw
				self.trial.getmol(m).numdest += 1
			for m in rxn.products:
				if m in rxn.reactants:
					print("\twarning: found species %s as a product but also a reactant of rxn '%s'" % (m, rxn.reaction))
					continue
				self.trial.getmol(m).de += rxn.ratelaw
				self.trial.getmol(m).numform += 1
			if params['verbosity'] > 0: pbar.update(i)
		if params['verbosity'] > 0: pbar.finish()
	
	def runinitialrxnsanitychecks(self):
		"""
		Runs a variety of sanity checks on the reaction network and species list.
		
		Tests include:
		- check the number of formation/destruction routes for each species
		- check element/mass balance of all reactions
		- check charge balance of all reactions
		- check accretion of each neutral species
		- check thermal desorption of each surface species
		- check all dissociation/ionization/desorption/accretion/migration reactions for a single reactant
		- check that most of the other reaction types have exactly two reactants
		- check that all electron recombination rxns involve an electron
		- check that all cations are involved in at least one electron recombination reaction
		- check that all neutrals are involved in a dissociation/ionization reaction
		- check that all species in the 'order' list exist
		"""
		###
		# check for duplicate reactions
		if not params['duplicatereactionaction'] == "nocheck":
			if params['verbosity'] > 1: print("\tchecking for duplicate reactions..")
			for i,rxn in enumerate(self.reactions):
				rtypestr = "RTYPE=%s" % rxn.rtype
				duprxns = gg_lib_cfilter.cfilter(
					["rtype", "reactants", "products"],
					[rxn.rtype, rxn.reactants, rxn.products],
					self.reactions[:i])
				if len(duprxns):
					# build useful message
					msg = "found a duplicate reaction:\n"
					msg += "\tcurrent  : %s (line %s)\n" % (rxn.rstring, rxn.linenum)
					msg += "\tmatch(es):\n"
					for dupr in duprxns:
						msg += "\t           %s (line %s)\n" % (dupr.rstring, dupr.linenum)
					# determine action to take
					if params['duplicatereactionaction'] in ["override", "ignore"]:
						msg += "ERROR: You have requested to override or ignore duplicate"
						msg += " reactions, but the duplicates were not caught in time."
						msg += " This was probably caused by a slight discrepancy in the"
						msg += " formatting of the reactions. You should check this issue"
						msg += " closely and re-run the trial manually. Sorry!"
						raise Exception(msg)
					elif params['duplicatereactionaction'] == "append":
						msg += "\taction: added it"
					elif params['duplicatereactionaction'] == "error":
						raise Exception(msg)
					if params['warn_duplicaterxns'] and (params['verbosity'] > 1):
						print(msg)
		###
		# create list of only molecular species
		particles = ["G0", "G1m", "E1m"]
		###
		# check number of formation/destruction routes for each species
		if params['warn_missingroutes'] and (params['verbosity'] > 1): print("\tchecking # of formation/destruction routes..")
		for s in self.trial.molecules.tolist():
			if s.name in particles: continue # skip particles
			if (s.numform==0) and (s.numdest==0):
				print("warning: species %s has no non-zero formation/destruction routes" % (s.name), end=' ')
			elif (s.numform == 0):
				if params['warn_missingroutes']:
					print("warning: species %s has no formation route!" % s.name)
			elif (s.numdest == 0):
				if params['warn_missingroutes']:
					print("warning: species %s has no destruction route!" % s.name)
		###
		# check element/mass balance of all reactions
		if params['verbosity'] > 1: print("\tchecking mass/elemental conservation for all reactions")
		badrxns = []
		for rxn in self.reactions:
			mass_lhs = 0
			mass_rhs = 0
			for s in rxn.reactants:
				mass_lhs += self.trial.getmol(s).mass
			for s in rxn.products:
				mass_rhs += self.trial.getmol(s).mass
			if abs(mass_lhs - mass_rhs) > 0.001*amu:
				badrxns.append(rxn)
		if len(badrxns) > 0:
			for rxn in badrxns:
				print("\t%s (linenum %s)" % (rxn.reaction, rxn.linenum))
			raise Exception("ERROR! found a mass imbalance for the following reaction: see list above")
		###
		# check charge balance of all reactions
		if params['verbosity'] > 1: print("\tchecking charge conservation for all reactions")
		badrxns = []
		for rxn in self.reactions:
			charge_lhs = 0
			charge_rhs = 0
			for s in rxn.reactants:
				charge_lhs += self.trial.getmol(s).charge
			for s in rxn.products:
				charge_rhs += self.trial.getmol(s).charge
			if abs(charge_lhs - charge_rhs):
				badrxns.append(rxn)
		if len(badrxns) > 0:
			for rxn in badrxns:
				print("\t%s (linenum %s)" % (rxn.reaction, rxn.linenum))
			raise Exception("ERROR! found a charge imbalance for the following reaction: see list above")
		###
		# check accretion of each neutral species (rtype=99)
		if params['usesurfchem']:
			if params['verbosity'] > 1:
				print("\tchecking that all species have an accretion pathway (rtype=99)")
			accretingspecies = self.getallreactantsfromrtype(99)
			accretedspecies = self.getallproductssfromrtype(99)
			involvedspecies = set(accretingspecies + accretedspecies)
			for m in self.trial.molecules.molecules:
				if m.name in particles: continue # skip particles
				if (m.charge == 0) and (not m in involvedspecies):
					print("warning: species %s is not involved in any accretion pathway" % m.name)
		###
		# check thermal desorption of each surface species (rtype=15)
		if params['usesurfchem']:
			if params['verbosity'] > 1: print("\tchecking that all species have an accretion pathway (rtype=15)")
			desorbingspecies = self.getallreactantsfromrtype(15)
			desorbedspecies = self.getallproductssfromrtype(15)
			involvedspecies = set(desorbingspecies + desorbedspecies)
			for m in self.trial.molecules.molecules:
				if m.name in particles: continue # skip particles
				if (m.charge == 0) and (not m in involvedspecies):
					print("warning: species %s is not involved in any thermal desorption pathway" % m.name)
		###
		# check that all dissociation/ionization/desorption/accretion/migration reactions contain a single reactant
		# (rtype=[1,13,15,16,17,18,19,20,99])
		if params['verbosity'] > 1: print("\tchecking that all dissociation/ionization/desorption/accretion/migration reactions (rtype=[1,13,15,16,17,18,19,20,99]) have exactly one reactant")
		if params['usesurfchem']:
			rxnstocheck = []
			badrxns = []
			for rtype in [1,13,15,16,17,18,19,20,99]:
				rxnstocheck += self.filter('rtype', rtype)
			for rxn in rxnstocheck:
				if not len(rxn.reactants)==1:
					badrxns.append(rxn)
			if len(badrxns) > 0:
				for rxn in badrxns:
					print("\t%s (linenum %s)" % (rxn.reaction, rxn.linenum))
				raise Exception("ERROR! found the reaction(s) not containing exactly one reactant: see list above")
		###
		# check that certain reaction types contain exactly two reactants
		#  (rtype=[2,3,4,5,6,7,8,9,10,11,12,14])
		if params['verbosity'] > 1: print("\tchecking that all specific reaction types (rtype=[2-12,14]) have exactly two reactants")
		if params['usesurfchem']:
			rxnstocheck = []
			badrxns = []
			for rtype in [2,3,4,5,6,7,8,9,10,11,12,14]:
				rxnstocheck += self.filter('rtype', rtype)
			for rxn in rxnstocheck:
				if not len(rxn.reactants)==2:
					badrxns.append(rxn)
			if len(badrxns) > 0:
				for rxn in badrxns:
					print("\t%s (linenum %s)" % (rxn.reaction, rxn.linenum))
				raise Exception("ERROR! found the reaction(s) not containing exactly two reactants: see list above")
		###
		# check that all electron recombination reactions (rtype=[9,10]) involve an electron and cation
		if params['verbosity'] > 1: print("\tchecking that all electron recombination reactions are in the correct format")
		badrxns = []
		rxnstocheck = []
		for rtype in [9,10]:
			rxnstocheck += self.filter('rtype', rtype)
		for rxn in rxnstocheck:
			if not 'E1m' in rxn.reactants:
				badrxns.append(rxn)
		if len(badrxns) > 0:
			for rxn in badrxns:
				print("\t%s (linenum %s)" % (rxn.reaction, rxn.linenum))
			raise Exception("ERROR! found the recombination reaction(s) not involving an electron: see list above")
		###
		# check that all cations are involved in some sort of electron recombination reaction (rtype=[9,10])
		if params['verbosity'] > 1: print("\tchecking that all cations are involved in at least one gas-phase electron recombination rxn (rtype = 9, 10)")
		involvedspecies = []
		for rtype in [9,10]:
			involvedspecies += self.getallreactantsfromrtype(rtype)
		involvedspecies = set(involvedspecies)
		for m in self.trial.molecules.molecules:
			if m.charge==1 and (not m in involvedspecies):
				print("WARNING! could not find species %s involved in any electron recombination reactions!" % m.name)
				#raise Exception("ERROR! could not find species %s involved in any electron recombination reactions!" % s.name)
		###
		# check that all neutrals can be photodissociated/photoionized
		if params['verbosity'] > 1: print("\tchecking that all neutrals can be photodissociated/photoionized")
		reactantsfromrtype1 = self.getallreactantsfromrtype(1)
		reactantsfromrtype13 = self.getallreactantsfromrtype(13)
		photoinvolvedgasphasespecies = set(reactantsfromrtype1+reactantsfromrtype13)
		reactantsfromrtype17 = self.getallreactantsfromrtype(17)
		reactantsfromrtype18 = self.getallreactantsfromrtype(18)
		reactantsfromrtype19 = self.getallreactantsfromrtype(19)
		reactantsfromrtype20 = self.getallreactantsfromrtype(20)
		photoinvolvedgrainspecies = set(reactantsfromrtype17+reactantsfromrtype18+reactantsfromrtype19+reactantsfromrtype20)
		for m in self.trial.molecules.molecules:
			if m.name in particles: continue # skip particles
			if m.numatoms == 1: continue # skip atoms
			if m.charge==0 and (not m.name[0]=='g') and (not m in photoinvolvedgasphasespecies):
				print("WARNING! could not find species %s involved in any photoionization/photodissociation reactions!" % m.name)
			elif m.charge==0 and (m.name[0]=='g') and params['usesurfchem'] and (not m in photoinvolvedgrainspecies):
				print("WARNING! could not find species %s involved in any photoionization/photodissociation reactions!" % m.name)
		print("WARNING! 'check that all neutrals can be photodissociated/photoionized' is not finished")
		###
		# check that order species exist(s)
		if params['verbosity'] > 1: print("\tchecking that order species exist(s)")
		if params['log_order'] and (not params['log_order_allspecies']):
			if len(params['log_order_species']) > 0:
				for s in params['log_order_species']:
					if not s in self.trial.molecules.get_names():
						raise NameError('%s was requested for order info, but it does not seem to be present in the network' % s)
			else:
				print("warning: log_order_species appears to be an empty list; this is not a fatal error, but")
				print("\tthe logging of order info will be disabled by forcing log_order=False")
				params['log_order'] = False
		###
		# check that all grain species have a binding energy
		if (params['verbosity'] > 1): print("\tchecking all grain species for binding energies and heats of formation..")
		for s in self.trial.molecules.tolist():
			if not ("g" == s.name[0] and params['warn_missingsurfprops']): continue
			if (not s.bindingenergy) and (not s.heatformation or s.heatformation <= -999):
				print("WARNING: %s seems to be completely missing from the surfprop file!" % s.name)
			elif not s.bindingenergy:
				print("WARNING: %s is missing a E_bind!" % s.name)
			elif not s.heatformation or s.heatformation <= -999:
				print("WARNING: %s has a suspicious heat of formation!" % s.name)
	
	def runfinalrxnsanitychecks(self):
		"""
		Runs a variety of sanity checks on the reaction network and species list
		AFTER the time-dependent densities have been solved.
		
		Tests include:
		- check that all neutral molecules have a reaction involving the Top 5 cations across the model
		"""
		###
		# check number of formation/destruction routes for each species
		if params['warn_missingroutes'] and (params['verbosity'] > 1): print("\tchecking all ion-molecule reactions..")
		# generate list of cations
		allCations = []
		for m in self.trial.molecules.tolist():
			if m.charge == 1: allCations.append(m)
		#print "\nallCations are %s" % allCations
		# make note of the sum of the cations
		chargeSums = []
		for it in range(self.trial.physgrid.length):
			chargeSums.append(sum(list([m.fabunds[it] for m in allCations])))
		#print "\nchargeSums are %s" % chargeSums
		# identify which ones dominate in abundance..
		#e1m = self.trial.getmol('E1m')
		importantCations = []
		for it in range(self.trial.physgrid.length):
			for m in allCations:
				#if (not m in importantCations) and (m.fabunds[it] > e1m.fabunds[it]*1e-6):
				if (not m in importantCations) and (m.fabunds[it] > chargeSums[it]*1e-4):
					importantCations.append(m)
		#print "\nimportantCations are:"
		#for m in sorted(importantCations, key=lambda x: max(x.fabunds), reverse=True):
		#	print "\t%s" % m.name
		# identify potentially-important hydrogen donors
		importantProtonDonors = [m for m in importantCations if m.stoi['hydrogen']]
		#print "\nimportantProtonDonors are %s" % importantProtonDonors
		print("\n\nWARNING: network.runfinalrxnsanitychecks() is not complete!!!")
	
	def filter(self, attr, val, reactions=None):
		"""
		Provides a method to retrieve a list of all reactions that match
		an attribute with the desired property.
		
		:param attr: name of the property to match
		:param val: value of the property to match
		:type attr: str
		:type val: any (that is appropriate)
		"""
		if reactions is None:
			reactions = np.asarray(self.reactions)
		return gg_lib_cfilter.cfilter(attr, val, reactions)
	
	def sort(self, attr):
		# sort either by a single attribute..
		if isinstance(attr, str):
			self.reactions.sort(key=lambda rxn: getattr(rxn, attr))
		# or sort according to the list, in reverse so that first one takes priority
		elif isinstance(attr, list):
			for a in attr:
				reverse = a[0] == "-"
				if reverse: a = a[1:]
				self.reactions.sort(key=lambda rxn: getattr(rxn, a), reverse=reverse)
		return self.reactions
	
	def print_properties(self, prefix=''):
		"""
		Prints a few stats about the object (filename, size, etc..).
		"""
		print("filename: %s" % (self.fname))
		print("found %s reactions" % len(self.reactions))
		print("special reactions are: %s" % self.specrxns)
	
	def print_reactions(self, limit=None, randomize=False, prefix=''):
		"""
		Loops through the all the reactions, and prints their properties
		based on the method reaction.print_properties(). See that for more
		detail.
		
		:param limit: (optional) the number of printings to stop at
		:param randomize: (optional) whether to randomize the order before printing
		:param prefix: (optional) strings/characters to prepend the printings
		:type limit: int
		:type randomize: bool
		:type prefix: str
		"""
		if params['verbosity'] > 0: print("printing reactions")
		if randomize:
			random.shuffle(self.reactions)
		else:
			self.reactions.sort(key=lambda x: x.numTOT)
		
		num_printed = 0
		for rxn in self.reactions:
			if limit and (num_printed > limit):
				break
			print("%s[" % prefix)
			rxn.print_properties(prefix=prefix+'\t')
			print("%s]" % prefix)
			num_printed += 1
	
	def reorder_by_linenum(self):
		"""
		Reorders the list of reactions, based on the line-number within
		the network file.
		"""
		if params['verbosity'] > 0:
			print("sorting the reactions based on their linenum")
		self.reactions = sorted(self.reactions, key=lambda x: x.linenum)
	
	def purge_unused_reactions(self):
		"""
		Removes reactions with zero-valued rates.
		"""
		for rxn in reversed(self.reactions):
			if not rxn.ratelaw: self.reactions.remove(rxn)
	
	def print_rate_laws(self, return_results=False):
		"""
		Loops through the all the reactions, and prints their rate laws.
		"""
		if params['verbosity'] > 0: print("printing rate laws")
		
		printed = ""
		for rxn in self.reactions:
			to_print = "(%s) %s: %s" % (rxn.linenum, rxn.reaction, rxn.ratelaw)
			printed += "%s\n" % to_print
			print(to_print)
		if return_results: return printed
	
	def getallreactantsfromrtype(self, rtype, nonzerokonly=True):
		"""
		Returns list of species which are involved as reactants in reactions
		of a desired rtype.
		
		:param rtype: the desired rtype to match
		:type rtype: int
		"""
		# initialize container to hold all reactants
		reactants = []
		for rxn in self.filter('rtype', rtype):
			try:
				if nonzerokonly and rxn.ratelaw == 0:
					continue
			except AttributeError:
				pass
			for s in rxn.reactants: reactants.append(self.trial.getmol(s))
		return reactants
	
	def getallproductssfromrtype(self, rtype, nonzerokonly=True):
		"""
		Returns list of species which are involved as products in reactions
		of a desired rtype.
		
		:param rtype: the desired rtype to match
		:type rtype: int
		"""
		# initialize container to hold all reactants
		products = []
		for rxn in self.filter('rtype', rtype):
			try:
				if nonzerokonly and rxn.ratelaw == 0:
					continue
			except AttributeError:
				pass
			for s in rxn.products: products.append(self.trial.getmol(s))
		return products
	




class reaction(object):
	"""
	Provides a class for describing a reaction. It holds information
	about the reactants, products, and kinetics, and encapsulate all
	the information that may be found within a astrochemical model's
	rate file, as well as some additional information to help with
	bookkeeping.
	"""
	
	# define dictionary such that getk[num] is same as getkfromrtype<num>(appropriatearguments)
	getrate = {
		0  : getratefromrtype0,
		1  : getratefromrtype1,
		2  : getratefromrtype2,
		3  : getratefromrtype3,
		4  : getratefromrtype4,
		5  : getratefromrtype5,
		6  : getratefromrtype6,
		7  : getratefromrtype7,
		8  : getratefromrtype8,
		9  : getratefromrtype9,
		10 : getratefromrtype10,
		11 : getratefromrtype11,
		12 : getratefromrtype12,
		13 : getratefromrtype13,
		14 : getratefromrtype14,
		15 : getratefromrtype15,
		16 : getratefromrtype16,
		17 : getratefromrtype17,
		18 : getratefromrtype18,
		19 : getratefromrtype19,
		20 : getratefromrtype20,
		80 : getratefromrtype80,
		97 : getratefromrtype97,
		98 : getratefromrtype98,
		99 : getratefromrtype99
	}
	
	def __init__(self, parent):
		self.parent = parent
		self.trial = parent.trial
		self.errors = False
		self.comment = None
		
		self.linenum = 0 # see network.load_file()
		self.numTOT = 0 # see network.load_file()
		self.numRTYPE = 0 # see network.load_file()
		
		self.reaction = "" # see reaction.load_jcl()
		self.reactants = [] # see reaction.load_jcl()
		self.products = [] # see reaction.load_jcl()
		
		self.kinetics = "" # see network.set_kinetics()
		self.rtype = None # see network.set_kinetics()
		self.k1, self.k2, self.k3 = 0, 0, 0 # see network.set_kinetics()
		self.br = 0 # see network.set_brs()
		
		self.ratelaw = None # see network.set_ratelaws() and reaction.set_ratelaw()
	
	def __unicode__(self):
		return u"%s" % self.reaction
	def __str__(self):
		return "reaction(%s,%s)" % (self.reaction, self.kinetics)
	
	def load_jcl(self, rstring):
		"""
		Instantiates the basic rate information that may be found in
		the format from Jacob C. Laas's file format, based on the standard
		OSU gas/grain astrochemical rate file.
		
		:param line: the line entry of a JCL-formatted rate file
		:type line: str
		"""
		# check to make sure line looks like: "reactants -> products RTYPE..."
		match = re.match('.* -> .*RTYPE.*', rstring)
		if match is None:
			print("\n\nERROR: Line entry is not properly formatted!")
			print("Please make sure it looks like:")
			print("reactant1 [+ reactant2 + ..] -> product1 [+ product2 + ..] RTYPE=XX k1=...")
			print("\nsee line #%s of file" % (self.linenum, self.parent.fname))
			print("line:", line)
			raise SyntaxError("see message above")
		
		# report line contents
		if params['verbosity'] > 2:
			print("found a reaction entry: %s" % rstring)
		
		# split by RTYPE, so that first group is reaction and second group is kinetics
		groups = rstring.split('RTYPE')
		# must add RTYPE back in
		groups[1] = "RTYPE" + groups[1]
		
		# identify reaction as first group
		reaction = groups[0].strip(' \t\n\r')
		# check against excluded species
		for entry in params['ignored_species']:
			if re.search(entry, reaction):
				self.errors = True
				return
		# split into reactants vs products
		#	reactants are first group, left of arraow, and further separated by +'s
		reactants = groups[0].split(' -> ')[0].split(' + ')
		#	products are same but right of arrow
		products = groups[0].split(' -> ')[1].split(' + ')
		
		# identify kinetics-related information as second group
		kinetics = groups[1].split()
		
		# fix character formatting
		index = 0
		for item in reactants:
			reactants[index] = cleaner_raw_molecular_formula(item)
			index += 1
		index = 0
		for item in products:
			products[index] = cleaner_raw_molecular_formula(item)
			index += 1
		
		# create/populate reaction dictionary with basic information
		self.reaction = reaction
		self.reactants = sorted(reactants)
		self.products = sorted(products)
		self.kinetics = kinetics
	
	def set_ratelaw(self):
		"""
		Sets the reaction's rate law, based on the lookup dictionary
		reaction.getrate{rtype => getratefromrtypeXX}.
		"""
		try:
			self.ratelaw = self.getrate[self.rtype](self)
		except:
			print("\nthere was an error when setting the ratelaw: %s" % self)
			e = sys.exc_info()[1]
			raise
	
	def print_properties(self, prefix="", title=""):
		"""
		Pretty-prints all the current attributes of the reaction, so long
		as they are of a normal type (i.e. not a callable function). If
		they are not a standard python object, then it is also noted at
		the right-hand side of the printing.
		"""
		if not title == "": print(prefix+title)
		if params['verbosity'] > 0: print(prefix+"reaction properties:")
		ignoredProperties = ['errors', 'kinetics', 'parent']
		knownAttr = vars(self)
		for k,v in list(knownAttr.items()):
			if isinstance(v, normal_types) and (not k in ignoredProperties):
				print("%s%s -> %s" % (prefix,k,v))
			elif k in ignoredProperties:
				pass
			else:
				print("odd (%s): %s -> %s" % (type(v),k,v))




class molecules(object):
	"""
	Provides a class for maintaining a list of molecules.
	"""
	def __init__(self, parent, network=None):
		self.parent = parent
		self.trial = parent.trial
		self.molecules = []
		if network is not None:
			self.load_from_network(network)
		
	
	def tolist(self):
		"""
		Simply returns the list of molecules.
		"""
		return self.molecules
	
	def get_names(self):
		"""
		Returns a list of the molecules contained, but only their namestrings.
		"""
		return [s.name for s in self.molecules]
	
	def get_molecule(self, name):
		"""
		Provides a look-up function, which returns the molecule whose
		name matches the argument string.
		
		:param name: name of the molecule to get
		:type name: str
		:returns: the molecule
		:rtype: molecule
		"""
		if not "nametomol" in dir(self):
			self.nametomol = {}
			for s in self.molecules:
				self.nametomol[s.name] = s
		if not name in list(self.nametomol.keys()):
			raise AttributeError("could not find the molecule named: %s" % name)
		else:
			return self.nametomol[name]
	
	def load_from_network(self, network):
		"""
		Create list of all species according to a network.
		
		Note that the attribute 'var' is also added to each entry.
		
		:param network: the network object to reference
		:type network: network
		"""
		if params['verbosity'] > 0: print("loading molecules from the network %s" % network)
		
		# initialize list to hold each species
		all_species = []
		
		# walk through all reactions, and append each species to the list
		for rxn in network.reactions:
			for s in rxn.reactants: all_species.append(s)
			for s in rxn.products:  all_species.append(s)
		
		# remove duplicate entries
		all_species = np.unique(all_species).tolist()
		
		# sort in alphabetical order for readability
		all_species.sort(key=lambda s: s.lower())
		
		for i,s in enumerate(all_species):
			if params['verbosity'] > 2.5: print("processing species %s" % s)
			m = molecule(s)
			concvar = "conc" + s
			m.var = ggdvar(concvar) # also a symbolic variable of its concentration
			self.molecules.append(m)
			#forget() # a hack in an attempt to reduce the rings' assumption
	
	def set_initial_abundances(self, fname, adjfname=None):
		"""
		Reads all the initial concentrations for each species from a file, and
		then sets a default value for those which are not listed.
		
		The file contents should simply be a space-delimited file as "SPECIES VALUE".
		
		:param fname: the filename containing initial abundances, which has the following shape:
			# this can be a comment
			SPECIES1		X1
			..				..
		:type fname: str
		"""
		if params['verbosity'] > 0: print("setting initial abundances")
		
		# initialize dict to contain initial values
		abundances = {}
		# open the initial abundance file
		try:
			file = open(fname, 'r')
		except IOError:
			raise IOError("Tried to load the initial abudance file, but %s isn't a file!" % fname)
		
		# loop through file
		linenum = 0
		for line in file:
			linenum += 1
			# strip comments
			line = re.sub(r'#.*', r'', line)
			# skip blank lines
			if re.match(r'^\s*$', line): continue
			
			# split by whitespace
			s,conc0 = line.split()
			# fix +/- symbols
			s = s.replace("+", 'p')
			s = s.replace("-", 'm')
			
			# warn about unknown species
			if params['warn_unknownspecies'] and (not s in self.get_names()):
				print("warning: did not recognize species %s in %s, line %s" % (s, fname, linenum))
			
			# append to dict
			abundances[s] = float(conc0)
		file.close()
		
		# set default initial abundances for all species
		for s in self.molecules:
			foundInitAbund = s.name in list(abundances.keys())
			if foundInitAbund and (abundances[s.name] > params['defaultinitialabund']):
				s.fabund0 = abundances[s.name]
			else:
				s.fabund0 = params['defaultinitialabund']
			s.density = s.fabund0 * params['initialdensity']
		
		# load the "patch" if appropriate
		if (adjfname is not None) and (os.path.isfile(adjfname)):
			try:
				file = open(adjfname, 'r')
			except IOError:
				raise IOError("Tried to load the patch file for the initial abundances! (%s)" % adjfname)
			
			# loop through file
			linenum = 0
			for line in file:
				linenum += 1
				# strip comments
				line = re.sub(r'#.*', r'', line)
				# skip blank lines
				if re.match(r'^\s*$', line): continue
				
				# split by whitespace
				adjmol,adjabund = line.split()
				# fix +/- symbols
				adjmol = adjmol.replace("+", 'p')
				adjmol = adjmol.replace("-", 'm')
				
				# warn about unknown species
				if params['warn_unknownspecies'] and (not adjmol in self.get_names()):
					print("warning: did not recognize species %s in %s, line %s" % (adjmol, adjfname, linenum))
				else:
					# do something with the value here...
					s = self.get_molecule(adjmol)
					try:
						if adjabund[0] == "+":
							s.fabund0 += float(adjabund[1:])
							s.density = s.fabund0 * params['initialdensity']
						elif adjabund[0] == "-":
							s.fabund0 -= float(adjabund[1:])
							s.density = s.fabund0 * params['initialdensity']
						elif adjabund[0] == "*":
							s.fabund0 *= float(adjabund[1:])
							s.density = s.fabund0 * params['initialdensity']
						else:
							s.fabund0 = float(adjabund)
							s.density = s.fabund0 * params['initialdensity']
					except:
						print("there was a problem adjusting the initial abundance for: %s -> %s" % (adjmol, adjabund))
				
			file.close()
		
		# set for atomic/molecular hydrogen, if they are specified
		if params['Hfa0'] and params['H2fa0']:
			self.get_molecule('H').fabund0 = params['Hfa0']
			self.get_molecule('H').density = params['Hfa0'] * params['initialdensity']
			self.get_molecule('H2').fabund0 = params['H2fa0']
			self.get_molecule('H2').density = params['H2fa0'] * params['initialdensity']
		elif params['Hfa0']:
			params['H2fa0'] = (1-params['Hfa0'])/2.0
			self.get_molecule('H').fabund0 = params['Hfa0']
			self.get_molecule('H').density = params['Hfa0'] * params['initialdensity']
			self.get_molecule('H2').fabund0 = params['H2fa0']
			self.get_molecule('H2').density = params['H2fa0'] * params['initialdensity']
		elif params['H2fa0']:
			params['Hfa0'] = 1 - 2*params['H2fa0']
			self.get_molecule('H').fabund0 = params['Hfa0']
			self.get_molecule('H').density = params['Hfa0'] * params['initialdensity']
			self.get_molecule('H2').fabund0 = params['H2fa0']
			self.get_molecule('H2').density = params['H2fa0'] * params['initialdensity']
		totH = float(2.0*self.get_molecule('H2').fabund0 + self.get_molecule('H').fabund0)
		if not totH == 1:
			print("WARNING: initial hydrogen fractional abundances are not unity(!): %s" % totH)
		
		# set for grains, based on gastodustratio
		self.get_molecule('G0').fabund0 = gastodustratio**-1
		self.get_molecule('G0').density = gastodustratio**-1 * params['initialdensity']
		
		# set for electrons, based on sum of cations
		sumofposcharge = 0
		for s in self.molecules:
			if s.charge > 0:
				sumofposcharge += s.charge * s.density
		self.get_molecule('E1m').density = float(sumofposcharge)
		self.get_molecule('E1m').fabund0 = float(sumofposcharge) / params['initialdensity']
		
		## push initial fractional abundances to array
		#for s in self.molecules:
		#	s.fabunds = [s.fabund0]
	
	def set_surface_properties(self, fname):
		"""
		Reads all the surface species' info from an input file.
		
		:param fname: the filename containing initial abundances, which has the following shape:
			# this can be a comment
			Species		E_bind		E_diff		QTunBar (K)		dHf(kcal/mol)
			X			X			X			X				X		# another comment
		:type fname: str
		"""
		if params['verbosity'] > 0: print("setting surface properties")
		
		# open the data file
		try:
			file = open(fname, 'r')
		except IOError:
			raise IOError("Tried to load the surf info file, but %s isn't a file!" % fname)
		
		# initialize container and T_dust
		hosp = {}
		if params['useseparatedusttemp']:
			Td = params['initialdusttemp']
		else:
			Td = T
		
		known_grain_species = [n for n in self.get_names() if n[0]=="g"]
		
		# loop through file
		linenum = 0
		for line in file:
			linenum += 1
			
			if line[:7] == "Species": continue # skip header line
			# strip comments and skip blank lines
			line = re.sub(r'#.*', r'', line)
			if re.match(r'^\s*$', line): continue
			
			# split the line by whitespace
			properties = line.split()
			
			# determines species
			s = str(properties[0])
			
			# warn about unknown species
			if params['warn_unknownspecies'] and (not s in known_grain_species):
				print("WARNING: did not recognize species %s in %s, line %s" % (s, fname, linenum))
			
			# initialize container
			hosp[s] = {}
			
			# appends values to the HoH
			hosp[s]['bindingenergy'] = float(properties[1])
			# set the proper diffusion energy, depending on whether there's a value
			if float(properties[2]): hosp[s]['diffbarr'] = float(properties[2])
			else: hosp[s]['diffbarr'] = float(properties[1]) * params['diffusionbindingratio']
			hosp[s]['tunnelbarr'] = float(properties[3])
			hosp[s]['heatformation'] = float(properties[4])
		
		# first note binding energy for H2O, so that loop is not doubled
		be_h2o = hosp['gH2O']['bindingenergy']
		
		surface_species = list(hosp.keys())
		for s in self.molecules:
			try:
				s.bindingenergy = hosp[s.name]['bindingenergy']
				s.diffbarr = hosp[s.name]['diffbarr']
				s.tunnelbarr = hosp[s.name]['tunnelbarr']
				s.heatformation = hosp[s.name]['heatformation']
			except KeyError:
				s.bindingenergy = 0
				s.diffbarr = 0
				s.tunnelbarr = 0
				s.heatformation = 0
			# sets zero and large binding energies to that of water's
			if (s.bindingenergy > be_h2o):
				if (params['verbosity'] > 1) and (s.name[0]=='g'): print("limiting the large binding energy for species %s" % s.name)
				s.bindingenergy = be_h2o
			#elif (s.bindingenergy == 0):
			#	if (params['verbosity'] > 1) and (s.name[0]=='g'): print "fixing the zero-valued binding energy for species %s" % s.name
			#	s.bindingenergy = be_h2o
			# set characteristic vibrational frequency
			s.vibfreq = sqrt(2 * k_b * params['grainsitedensity'] * s.bindingenergy / pi**2 / s.mass)
			## set sticking coefficients for grains and light molecules
			#grains = [
			#	"G0",
			#	"G1m"
			#]
			#masscutoff = 2.5 * amu
			#if (hop[s]['mass'] < masscutoff) or (s in grains):
			#	hop[s]['stickcoeff'] = 0
			#else:
			#	hop[s]['stickcoeff'] = stickingprobability
	
	def reorder_by_reactions(self):
		"""
		Reorders the list of molecules, based on the number of their
		non-zero rxns.
		"""
		if params['verbosity'] > 0:
			print("sorting the list of species based on their # of non-zero rxns")
		self.molecules.sort(key=lambda x: ((x.numform + x.numdest), x.name))
	
	def print_properties(self, limit=None, randomize=False, prefix=''):
		"""
		Pretty-prints all the properties for the molecules contained within.
		See molecule.print_properties() for more details on what is printed.
		
		:param limit: (optional) the number of printings to stop at
		:param randomize: (optional) whether to randomize the order before printing
		:param prefix: (optional) strings/characters to prepend the printings
		:type limit: int
		:type randomize: bool
		:type prefix: str
		"""
		print("found %s species" % len(self.molecules))
		
		if randomize:
			random.shuffle(self.molecules)
		else:
			self.molecules.sort(key=lambda x: x.name)
		
		num_printed = 0
		for s in self.molecules:
			if limit and (num_printed > limit):
				break
			print("%s[" % prefix)
			s.print_properties(prefix=prefix+'\t')
			print("%s]" % prefix)
			num_printed += 1
	
	def reorder_by_name(self):
		"""
		Reorders the list of molecules, based on their namestring.
		"""
		if params['verbosity'] > 0:
			print("sorting the list of species based on their name")
		self.molecules.sort(key=lambda x: x.name)
	
	def purge_unused_species(self):
		"""
		Removes molecules with zero destruction/formation routes.
		"""
		for m in reversed(self.molecules):
			if not (m.numform and m.numdest): self.molecules.remove(m)
	
	def print_des(self, return_results=False):
		"""
		Loops through the all the molecules, and prints their DEs.
		"""
		if params['verbosity'] > 0: print("printing DEs")
		
		printed = ""
		for mol in self.molecules:
			to_print = "%s: %s" % (mol.name, mol.de)
			printed += "%s\n" % to_print
			#print to_print
		if return_results: return printed




class molecule(object):
	"""
	Provides a class for describing a molecule. Each instance will have
	a set of attributes that directly refer to its known properties.
	"""
	
	# define elemental names
	elements = {
		"H"		:	'hydrogen'	,
		"He"	:	'helium'	,
		"C"		:	'carbon'	,
		"N"		:	'nitrogen'	,
		"O"		:	'oxygen'	,
		"F"		:	'fluorine'	,
		"Na"	:	'sodium'	,
		"Mg"	:	'magnesium'	,
		"Si"	:	'silicon'	,
		"P"		:	'phos'		,
		"S"		:	'sulfur'	,
		"Cl"	:	'chlorine'	,
		"Fe"	:	'iron'
	}
	# define atomic masses
	# source: CRC Handbook of Chemistry and Physics, 95th edition (2014-2015)
	# note that masses refer to specific isotopes (not the population-averaged values!)
	atomicmass = {
		"electron"  :  0.00054857990946 * amu,
		"hydrogen"  :  1.00782503223 * amu,
		"helium" 	:  4.00260325413 * amu,
		"carbon"  	: 12.0 * amu,
		"nitrogen"  : 14.00307400443 * amu,
		"oxygen"  	: 15.99491461957 * amu,
		"fluorine"  : 18.9984031627 * amu,
		"sodium" 	: 22.9897692820 * amu,
		"magnesium" : 23.985041697 * amu,
		"silicon" 	: 27.9769265346 * amu,
		"phos"  	: 30.9737619984 * amu,
		"sulfur"  	: 31.9720711744 * amu,
		"chlorine" 	: 34.96885268 * amu,
		"iron" 		: 55.9349373 * amu,
		"grain"  	: dustmass
	}
	
	def __init__(self, name):
		self.name = name
		self.var = None # see molecules.load_from_network()
		self.charge = 0 # see molecule.set_mol_charge()
		self.stoi = {} # see molecule.set_elemental_stoichiometry()
		self.numatoms = 0 # see molecule.set_elemental_stoichiometry()
		self.mass = 0 # see molecule.set_mol_mass()
		self.velpersqrtT = 0 # see molecule.set_mol_velocity()
		
		self.fabund0 = 0 # see molecules.set_initial_abundances()
		self.fabunds = [] # see molecules.set_initial_abundances()
		
		self.bindingenergy = 0 # see molecules.set_surface_properties()
		self.diffbarr = 0 # see molecules.set_surface_properties()
		self.tunnelbarr = 0 # see molecules.set_surface_properties()
		self.heatformation = -999.99 # see molecules.set_surface_properties()
		self.vibfreq = 0 # see molecules.set_surface_properties()
		
		self.de = 0 # see molecules.init_des()
		self.numform = 0 # see molecules.init_des()
		self.numdest = 0 # see molecules.init_des()
		
		self.set_mol_charge()
		self.set_elemental_stoichiometry()
		self.set_mol_mass()
		self.set_mol_velocity()
	
	def __str__(self):
		return self.name
	def __unicode__(self):
		return u"%s" % self.name
	def __repr__(self):
		return "molecule(%s)" % self.name
	
	def set_mol_charge(self):
		"""
		Sets molecular charge for all species.
		"""
		if params['verbosity'] > 2.5: print("setting its charge")
		# try to identify at digit + p/m
		match = re.match(r'.*(\d)([pm])$', self.name)
		# if there's a match, there's a charge..
		if match:
			#print matchobj.groups()
			if match.groups()[1] == "p":
				self.charge = 1 * int(match.groups()[0])
			elif match.groups()[1] == "m":
				self.charge = -1 * int(match.groups()[0])
			else:
				raise SyntaxError("found something perplexing when identifying charge:", match.groups())
		# otherwise there's zero charge
		else: self.charge = 0
	
	def set_elemental_stoichiometry(self):
		"""
		Sets stoichiometry of elemental composition for all species.
		"""
		if params['verbosity'] > 2.5: print("setting its stoichiometry")
		self.stoi = {}
		for short,elem in list(self.elements.items()):
			self.stoi[elem] = 0
		
		if self.name not in ['E1m','G0','G1m']:
			# deep copy species to formula string
			formula = copy.deepcopy(self.name)
			# drop charges at end of formula
			formula = re.sub(r"\d[pm]$", r'', formula)
			# drop grain-surface designation
			formula = formula.lstrip('g')
			# split to either Capital[lowercase] or Digits
			tokens = re.findall(r'[A-Z][a-z]*|\d+', formula)
			# walk through tokens in reverse
			tokens.reverse()
			multiplier = 0
			for t in tokens:
				# if there's a digit, use it to multiply the next token
				if re.match(r'\d+', t): multiplier = int(t)
				else:
					elem = self.elements[t]
					# add multiplier to sum and reset multiplier
					if multiplier:
						self.stoi[elem] += multiplier
						multiplier = 0
					# just add mass to sum
					else:
						self.stoi[elem] += 1
			for elem,stoi in list(self.stoi.items()):
				self.numatoms += stoi
	
	def set_mol_mass(self):
		"""
		Sets molecular weight for all species.
		"""
		if params['verbosity'] > 2.5: print("setting its mass")
		# variable to contain the sum of atomic masses
		mass = 0
		if self.name == "E1m":
			mass = self.atomicmass['electron']
		elif self.name in ["G0","G1m"]:
			mass = self.atomicmass['grain']
		else:
			for elem,stoi in list(self.stoi.items()):
				mass += self.atomicmass[elem]*stoi
		self.mass = mass
	
	def set_mol_velocity(self):
		"""
		Sets molecular thermal velocity for all species.
		
		Note that 'thermal velocity' means the velocity per sqrt(T)
		(i.e. velpersqrtT*sqrt(T) is the velocity).
		"""
		if params['verbosity'] > 2.5: print("setting its molecular velocity")
		self.velpersqrtT = sqrt(8/pi * k_b / self.mass)
	
	def get_tex_name(self):
		"""
		Returns the mhchem LaTeX package-compatible molecular formula for
		the species.
		
		Note that the format adheres specifically to the 'mhchem' package
		which would also need to be installed on the system.
		
		:returns: the latex-compatible namestring
		:rtype: str
		"""
		if params['verbosity'] > 2.5: print("setting its tex string")
		
		texstring = self.name.replace("E",'e').replace("G","grain").rstrip("0")
		
		# strip grain-surface designation
		suffix = ""
		if self.name[0] == "g":
			suffix = "{}_{surf}"
			texstring = texstring.lstrip('g')
		
		# strip positive charge
		texstring = re.sub(r"\d[pm]$", r'', texstring)
		if self.charge == 1:
			texstring += "^+"
		elif self.charge == -1:
			texstring += "^-"
		
		texstring += suffix
		texstring = "\ce{%s}" % texstring
		
		return texstring
	
	def get_enhanced_name(self):
		"""
		Returns the species' molecular formula for containing slightly
		enhanced formatting (e.g. for gnuplot with enhanced font).
		
		:returns: the latex-compatible namestring
		:rtype: str
		"""
		if params['verbosity'] > 2.5: print("setting its tex string")
		
		namestring = self.name.replace("E",'e').replace("G","grain")
		
		# strip grain-surface designation
		suffix = ""
		if self.name[0] == "g":
			suffix = "{_{ice}}"
			namestring = namestring.lstrip('g')
		
		# strip positive charge
		namestring = re.sub(r"\d[pm]$", r'', namestring)
		if self.charge == 1:
			namestring += "^+"
		elif self.charge == -1:
			namestring += "^-"
		
		# subscript all stoichiometry numbers
		namestring = re.sub(r"([a-zA-Z])(\d+)", r"\1_\2", namestring)
		
		if namestring[-1] in "123456789":
			namestring += "%s" % suffix.replace("ice",",ice")
		else:
			namestring += suffix
		
		return namestring
	
	def print_properties(self, prefix=""):
		"""
		Pretty-prints all the known attributes of the molecule, with the
		exception of:
			'fabunds' - too long of a list to be useful as a pretty-print
		"""
		ignoredProperties = ['fabunds']
		knownAttr = vars(self)
		for k,v in list(knownAttr.items()):
			if isinstance(v, normal_types) and (not k in ignoredProperties):
				print("%s%s -> %s" % (prefix,k,v))
			elif k in ignoredProperties:
				pass
			else:
				print("%s%s -> %s (odd: %s)" % (prefix,k,v,type(v)))




class physgrid(object):
	"""
	Provides a class for containing a list of iterations to march
	through.
	
	Note that the printed representation has been forced to look like
	a list of strings.
	"""
	def __init__(self, parent):
		self.parent = parent
		self.trial = parent.trial
		self.length = 0 # see physgrid.set_times()
		self.times = [] # see physgrid.set_times()
		self.densities = [] # see physgrid.set_densities()
		self.getdens = None # see physgrid.set_densities()
		self.extinctions = [] # see physgrid.set_av()
		self.getav = None # see physgrid.set_av()
		self.temperatures = [] # see physgrid.set_temperatures()
		self.gettemp = None # see physgrid.set_temperatures()
		self.last_time = 0
	
	def set_times(self):
		"""
		Sets the array of time points (unit: s), and defines how many
		iterations there will be for the trial.
		"""
		if params['verbosity'] > 0: print("setting the time points (dtditer=%s)" % params['dtditer'])
		
		if params['dtditer'] == "linear":
			"""
			Defines the array ranging from zero, spaced evenly by dyear.
			"""
			finals = params['finalyear'] * yr
			ds = params['dyear'] * yr
			self.times = srange(0, finals, ds)
		elif params['dtditer'] == "log":
			"""
			Defines the array ranging from dyear to finalyear, spaced
			in a logarithmic fashion.
			"""
			log_first = log10(params['dyear']*yr)
			log_last = log10(params['finalyear']*yr)
			n = int(params['numberofiterations'])
			self.times = [0] + np.logspace(log_first, log_last, n).tolist()
		elif params['dtditer'] == "gg08st1":
			"""
			Defines the time points explicitly used in Stage 1 of the
			GG08 model.
			
			Note that there is no analytical function describing these
			times, but rather the times were explicitly copied and defined
			here.
			"""
			# set explicit points
			times = [0,10,11.55,13.34,15.4,17.78,20.54,23.71,27.38,31.62,36.52,42.17,
				48.7,56.23,64.94,74.99,86.6,100,115.5,133.4,154,177.8,205.4,237.1,273.8,
				316.2,365.2,421.7,487,562.3,649.4,749.9,866,1000,1155,1334,1540,1778,
				2054,2371,2738,3162,3652,4217,4870,5623,6494,7499,8660,10000,11550,
				13340,15400,17780,20540,23710,27380,31620,36520,42170,48700,56230,64940,
				74990,86600,100000,115500,133400,154000,177800,205400,237100,273800,
				316200,365200,421700,487000,562300,649400,749900,866000,938000
			]
			self.times = [x * yr for x in times]
		elif params['dtditer'] == "gg08st2":
			raise NotImplementedError("the gg08 stage 2 time points have not been set yet, but the 'log' option works nearly the same..")
		
		self.length = len(self.times)
		self.times = np.asarray(self.times)
	
	def set_densities(self):
		"""
		Sets the array of time points (unit: cm**-3).
		"""
		if params['verbosity'] > 0: print("setting the densities (ddensitydt=%s)" % params['ddensitydt'])
		
		if (params['ddensitydt'] == 0) or (params['ddensitydt'] == "gg08st2"):
			"""
			Uses a static value of density for all iterations.
			"""
			for i in range(self.length):
				self.densities.append(params['initialdensity'])
		elif params['ddensitydt'] == 1:
			"""
			Uses a linear progression of densities across all iterations.
			
			Note that the algorithm is:
			density(iter) = iter * slope + offset, where
				offset = initialdensity
				scale = (final density - initial density ) / (number of iterations)
			"""
			try:
				float(params['densitychangebegins'])
			except TypeError:
				params['densitychangebegins'] = self.times[1]/float(yr)
			# determine scaling
			numiters = 0
			for it in range(self.length):
				if (self.times[it]/float(yr)) < params['densitychangebegins']:
					pass
				else:
					numiters += 1
			# note the 'minus one' is needed so that the final iteration is correct
			scale = (params['finaldensity'] - params['initialdensity']) / float(numiters)
			offset = params['initialdensity']
			# apply density
			for it in range(self.length):
				if (self.times[it]/float(yr)) < params['densitychangebegins']:
					self.densities.append(params['initialdensity'])
				else:
					density = (it-(self.length-numiters)) * scale + offset
					self.densities.append(density)
		elif params['ddensitydt'] == 2:
			"""
			Uses a power-law progression of densities.
			
			Note that the algorithm is:
			density(iter) = iter^2 * slope + offset, where
				offset = initialdensity
				scale = (final density - initial density ) / (number of iterations)^2
			"""
			try:
				float(params['densitychangebegins'])
			except TypeError:
				params['densitychangebegins'] = self.times[1]/float(yr)
			# determine scaling
			numiters = 0
			for it in range(self.length):
				if (self.times[it]/float(yr)) < params['densitychangebegins']:
					pass
				else:
					numiters += 1
			# note the 'minus one' is needed so that the final iteration is correct
			scale = (params['finaldensity'] - params['initialdensity']) / float((numiters-1)**2)
			offset = params['initialdensity']
			# apply density
			for it in range(self.length):
				if (self.times[it]/float(yr)) < params['densitychangebegins']:
					self.densities.append(params['initialdensity'])
				else:
					density = ( it-(self.length-numiters) )**2 * scale + offset
					self.densities.append(density)
		elif params['ddensitydt'] == "gg08st1":
			r"""
			Uses the density points explicitly used in Stage 1 (modified
			collapse) of the GG08 model across all iterations.
			"""
			# define the densities
			densities = [3000,3000,3000,3000,3000,3000,3000,3000,3000,3000,3000,3000,
				3000,3000,3000,3000,3000,3000,3000,3000,3000,3000,3000,3000,3000,3000,
				3000,3000,3000,3000,3000,3000,3000,3000,3000,3000,3000,3000,3000,3000,
				3000,3000,3000,3000,3000,3000,3000,3000,3001,3001,3001,3001,3002,3002,
				3003,3004,3005,3006,3009,3011,3015,3020,3027,3036,3048,3064,3086,3115,
				3154,3209,3283,3387,3533,3743,4055,4539,5338,6796,9950,19540,99250,
				10000000
			]
			# apply density
			for it in range(self.length):
				self.densities.append(densities[it])
				#nt = densities[index] + 1e-3
				#brac1 = nt**4 / initialdensity
				#brac1 = brac1**(1/3)
				#brac2 = (nt/initialdensity)**(1/3)
				#brac2 = (brac2-1) * 24 * pi * grav * amu * initialdensity
				#brac2 = sqrt(brac2)
				#nt = magcollcoeff * brac1 * brac2
		
		self.densities = np.asarray(self.densities)
		self.den_splrep = scipy.interpolate.splrep(self.times, self.densities)
		def den_splev(x):
			if isinstance(x, tuple): x = float(x[0])
			return scipy.interpolate.splev(x, self.den_splrep, ext=3)
		self.getdens = den_splev
	
	def set_av(self):
		"""
		Defines the visual extinction for the iterations. This depends
		how the densities scale with respect to the initial density and
		visual extinction.
		
		Note that the algorithm is:
		new_av = initial_av * (new_density / initial_density)**(2/3)
		"""
		if params['verbosity'] > 0: print("setting the visual extinctions")
		self.extinctions = params['av0'] * (self.densities/params['initialdensity'])**(2.0/3.0)
		
		self.av_splrep = scipy.interpolate.splrep(self.times, self.extinctions)
		def av_splev(x):
			if isinstance(x, tuple): x = float(x[0])
			return scipy.interpolate.splev(x, self.av_splrep, ext=3)
		self.getav = av_splev
	
	def set_temperatures(self):
		"""
		Sets the array of temperatures (unit: K).
		"""
		if params['verbosity'] > 0: print("setting the temperatures (dtempdt=%s)" % params['dtempdt'])
		
		if (params['dtempdt'] == 0) or (params['dtempdt'] == "gg08st1"):
			"""
			Uses a static value of temperature for all iterations.
			"""
			# set global switch to zero (in case it is referenced elsewhere)
			params['dtempdt'] = 0
			# set temperature for all iterations
			for it in range(self.length):
				self.temperatures.append(params['initialgastemp'])
		elif params['dtempdt'] == 1:
			"""
			Uses a linear progression of temperatures across all iterations
			
			Note that the algorithm is:
			temp(iter) = iter * slope + offset, where
				offset = initialgastemp
				scale = (final temp - initial temp ) / (number of iterations)
			"""
			# determine scaling
			# note the 'minus one' is needed so that the final iteration is correct
			scale = (params['finalgastemp'] - params['initialgastemp']) / float( self.length - 1 )
			offset = params['initialgastemp']
			# set temperature for all iterations
			x = 0
			for it in range(self.length):
				temp = x * scale + offset
				self.temperatures.append(float(temp))
				x += 1
		elif (params['dtempdt'] == 2) or (params['dtempdt'] == "gg08st2"):
			"""
			Defines temperature points in a fashion akin to that used
			in Stage 2 (gradual warm-up) of the GG08 model across all
			iterations.
			
			Note that the algorithm is:
			temp(iter) = iter^2 * slope + offset, where
				offset = initialgastemp
				scale = (final temp - initial temp ) / (number of iterations)^2
			"""
			try:
				float(params['finaltemptimescale'])
			except TypeError:
				params['finaltemptimescale'] = self.times[-1]/float(yr)
			numiters = 0
			for it in range(self.length):
				if (self.times[it]/float(yr)) >= params['finaltemptimescale']:
					break
				else:
					numiters += 1
			scale = (params['finalgastemp'] - params['initialgastemp']) * numiters**-2
			offset = params['initialgastemp']
			# set temperature for all iterations
			for it in range(self.length):
				temp = it**2 * scale + offset
				if (self.times[it]/float(yr)) >= params['finaltemptimescale']:
					self.temperatures.append(params['finalgastemp'])
				else:
					self.temperatures.append(temp)
		
		self.temperatures = np.asarray(self.temperatures)
		self.temp_splrep = scipy.interpolate.splrep(self.times, self.temperatures)
		def temp_splev(x):
			if isinstance(x, tuple): x = float(x[0])
			return scipy.interpolate.splev(x, self.temp_splrep, ext=3)
		self.gettemp = temp_splev
	
	def print_iterations(self, limit=None, prefix=''):
		if not limit:
			limit = self.length
		print("%s  IT  time (s)   t (yr)   n (cm⁻³)    Aᵥ   T (K)" % prefix)
		for it in range(self.length):
			if it > limit: break
			print("%s%4s  %8.2e  %8.2e  %8.2e  %5.2f  %5.1f" % (
				prefix,
				it,
				self.times[it],
				self.times[it]/float(yr),
				self.densities[it],
				self.extinctions[it],
				self.temperatures[it]))
	
	def runphyssanitychecks(self):
		r"""
		Runs a battery of tests to check that the defined physical system is sane.
		
		Tests include:
		- a check that the order times match exactly (optional) the iteration times
		"""
		trial = self.trial
		###
		# check that order times match exactly the iteration times
		if (params['log_order']) and (not params['log_order_alltimes']) and (not params['log_order_timestouse']=='iterations'):
			params['log_order_notexacttimes'] = False
			if (len(params['log_order_times']) > 0):
				for i,t in enumerate(params['log_order_times']):
					# convert first to seconds
					t *= yr
					params['log_order_times'][i] = t
					# set a flag to use the method declared via log_order_timestouse
					if (not t in self.times):
						params['log_order_notexacttimes'] = True
			else:
				print("warning: log_order_times appears to be an empty list; this is not a fatal error, but")
				print("\twill disable logging the order info by setting log_order=False")
				params['log_order'] = False
			if (params['log_order_notexacttimes']==True):
				print("will use the", params['log_order_timestouse'], "method for calculating order info")
		if params['log_order_timestouse']=='iterations':
			bad_entries = []
			for it in params['log_order_times']:
				if not isinstance(it, int_types):
					raise TypeError("if log_order_timestouse=='iterations', you must input log_order_times as integers, not %s" % type(it))
				if not it in xrange(trial.physgrid.length):
					bad_entries.append(it)
			if len(bad_entries) > 0:
				bad_entries = sorted(bad_entries, reverse=True)
				for it in bad_entries:
					del params['log_order_times'][params['log_order_times'].index(it)]
		###
		# check temperature range validity
		###
		# make sure first iteration is t=0
		###
		# make sure density doesn't change if quickrun
		#if params['quickrun'] and params['ddensitydt']:
		#	raise NotImplementedError("the quickrun solver does not support ddensitydt=%s" % params['ddensitydt'])
		
		return True

class solution(object):
	"""
	Provides a class for containing things related to the ODE solution.
	"""
	def __init__(self, parent):
		self.parent = parent
		self.has_run = False
		self.has_finished = False
		self.was_loaded = False
		self.plots = []

#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# DESCRIPTION
#	All calculations of physical constants and values should go here.
#
# standard library
from __future__ import print_function
from __future__ import absolute_import
import os
import sys
# third-party
# local
if not os.path.dirname(os.path.abspath(__file__)) in sys.path:
	sys.path.append(os.path.dirname(os.path.abspath(__file__)))
from gg_lib_sys import *
from gg_lib_math import *


########
# misc #
########

# intialize symbolic variables for physical properties
T = ggdvar('T')
gasdensity = ggdvar('gasdensity')
av = ggdvar('av')
#av = av0

###
# general physical constants
#
# boltzmann constant (1.3806488e-23 in units: J/K)
# ref: CRC Handbook of Chem & Phys, 95th Ed
k_b = 1.3806488e-16
#
# atomic mass unit (1.66053886e-27 in unit: kg)
amu = 1.66053886e-24
#
# planck's constant (1.054571726e-34 in units: J*s)
# ref: CRC Handbook of Chem & Phys, 95th Ed
hbar = 1.054571726e-27

# length (unit: s) of year
yr = 31556925.9936

# gravitational constant (6.67384e-11 in units: m^3 kg^-1 s^-2)
grav = 6.67384e-8

# electron charge (unit: coulombs)
echarge = 1.60217657e-19




###############
# grain rates #
###############

###
# determine dust properties
#	see gg_osu_2008v1.f:194 for details
#
# determine an individual grain's mass
dustmass = 4/3.0 * pi * params['dustmassdensity'] * params['dustradius']**3
#
# determine the number density ratio of total gas-to-dust
# GG08: 1.762e-12
gastodustratio = dustmass / float(params['dusttogasmassratio']) / amu
#
# determine the limiting density for a monolayer
limitingdensity = params['monolayerlimit'] * params['grainsites'] / gastodustratio
#
# determine the grain's collisional cross-section
graincrosssection = pi * params['dustradius']**2
#
# determine a grain's surface area
#	*note* depends on porosity!!
#	for a smooth sphere, SA = 4 * pi * radius^2
#	-> 1.26e-9 cm^2 for a smooth 0.1 um radius sphere
#grainsurfacearea =




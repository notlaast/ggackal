# DESCRIPTION
#	This file should contain the initial fractional abundances
#	of all species. The reference species should be defined in
#	"gg_in_params.yml".

# highly depleted abundances for a dense core
# Ref: McElroy et al., A&A 550 (2013) - UMIST 2012
H           0.3333333334
H2          0.3333333333
He          9.0e-2
O           3.2e-4
C1+         1.4e-4
N           7.5e-5
S1+         1.0e-5
Na1+        2.0e-9
Si1+        8.0e-9
Mg1+        7.0e-9
Cl1+        4.0e-9
Fe1+        3.0e-9
P1+         3.0e-9
F           2.0e-8

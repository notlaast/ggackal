#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# TODO
#
# DESCRIPTION
#	This file should contain all the pretty-printers and data reformatters.
#
# standard library
from operator import itemgetter
import collections
import re
# third-party
import numpy as np
# local


list_types = (list, tuple, dict)




def printsep():
	"""
	Prints a horizontal line of ='s.
	
	OUTPUT:
	============================================================
	"""
	print('=' * 60)




def cleaner_raw_molecular_formula(s):
	# replace symbol for charges: "+/- -> p/m"
	s = s.replace("+", 'p')
	s = s.replace("-", 'm')
	# whitespace padding
	s = s.strip()
	return s



def getsimptext(s):
	# add literals to _,%
	s = s.replace("_", "\_")
	s = s.replace("%", "\%")
	# & -> and
	s = s.replace("&", "and")
	# greek letters
	s = s.replace("α", "$\\alpha$")
	return s


def getmatchingstringindex(s, l):
	for i,item in enumerate(l):
		if s in item:
			return i




def nx_to_latex(
	graph,
	get_positions_from_set=True,
	layout="graphviz",
	loops_allowed=True,
	directed=True,
	weighted=True,
	units="cm",
	scale=10,
	vertex_shape="rectangle, rounded corners", # or "circle"
	vertex_color="red",
	vertex_fill="yellow",
	vertex_font="\\bfseries",
	label_shape="rectangle",
	label_color="blue",
	label_fill="white",
	label_font="\\tiny",
	label_centered=True,
	label_sloped=True,
	edge_weight=1,
	edge_linewidth=5,
	edge_color="black"):
	
	import networkx as nx
	
	# define edge angles for multiple edges
	# (i.e. straight, then bent to alternate directions and increasing angles)
	edge_angles = {
		1: "bend left=0",
		2: "bend right=15",
		3: "bend left=15",
		4: "bend right=30",
		5: "bend left=30",
		6: "bend right=45",
		7: "bend left=45",
		8: "bend right=60",
		9: "bend left=60",
		10: "bend right=75",
		11: "bend left=75"}
	
	# initialize latex code
	latex = ""
	latex += \
	r'''
	\documentclass{article}
	\usepackage{tikz}
	\usepackage[a0paper]{geometry}

	\pagestyle{empty}
	\usepackage[utf8]{inputenc}
	\usepackage[T1]{fontenc}
	\usepackage{amsmath}
	\usepackage{amssymb}
	\usepackage{amsfonts}
	\usepackage{tkz-graph}
	\usepackage[version=3]{mhchem}
	
	\usetikzlibrary{arrows}
	\usetikzlibrary{decorations.pathmorphing}
	\usetikzlibrary{backgrounds}
	\usetikzlibrary{positioning}
	\usetikzlibrary{fit}
	'''
	
	# define styles
	latex += "\\tikzset{VertexStyle/.style = {\n"
	latex += "  shape = %s,\n" % vertex_shape
	latex += "  text = %s,\n" % vertex_color
	latex += "  fill = %s,\n" % vertex_fill
	latex += "  font = %s}}\n" % vertex_font
	latex += "\\tikzset{LabelStyle/.style = {draw,\n"
	latex += "  shape = %s,\n" % label_shape
	latex += "  text = %s,\n" % label_color
	latex += "  fill = %s,\n" % label_fill
	if label_centered:
		latex += "  anchor = south,\n"
	if label_sloped:
		latex += "  sloped,\n"
	latex += "  font = %s}}\n" % label_font
	latex += "\\tikzset{EdgeStyle/.append style = {\n"
	if directed:
		latex += "  ->}}\n"
	else:
		latex += "  -}}\n"
	if layout == "graphviz":
		unit_length = "%s%s" % (scale/10.0, units)
	else:
		unit_length = "%s%s" % (scale, units)
	
	# define graph itself
	latex += "\n\\begin{document}\n"
	
	# convert list to dict
	if not isinstance(graph, list):
		graph = [("",graph)]
	
	# get positions of vertices from combined set if desired
	if get_positions_from_set:
		# make graph with all nodes
		mastergraph = nx.MultiDiGraph()
		for i,g in graph:
			mastergraph.add_nodes_from(g.nodes(data=True))
			mastergraph.add_edges_from(g.edges())
		# get positions
		if layout == "circular":
			pos = nx.circular_layout(mastergraph)
			scale /= 5.0
		elif layout == "random":
			pos = nx.random_layout(mastergraph)
			scale /= 5.0
		elif layout == "shell":
			pos = nx.shell_layout(mastergraph)
			scale /= 10.0
		elif layout == "spring":
			pos = nx.spring_layout(mastergraph)
			scale /= 5.0
		elif layout == "spectral":
			pos = nx.spectral_layout(mastergraph)
			scale /= 5.0
		elif layout == "fruchterman_reingold":
			pos = nx.fruchterman_reingold_layout(mastergraph)
			scale /= 5.0
		elif layout == "graphviz":
			pos = nx.graphviz_layout(mastergraph)
			scale /= 100.0
		# determine shift key species to origin (0,0)
		origin = (0,0)
		for n in mastergraph.nodes(data=True):
			print(n)
			if "center" in list(n[1].keys()) and n[1]["center"]:
				origin = list(pos[n[0]])
				origin[0] *= scale
				origin[1] *= scale
				break
		if origin == (0,0):
			pos_x = []
			pos_y = []
			for k,coordinates in list(pos.items()):
				pos_x.append(coordinates[0])
				pos_y.append(coordinates[1])
			origin = (np.mean(pos_x)*scale, np.mean(pos_y)*scale)
		pos_x = []
		pos_y = []
		# shift all positions and collect them
		for k,coordinates in list(pos.items()):
			new_x = pos[k][0]*scale - origin[0]
			new_y = pos[k][1]*scale - origin[1]
			pos[k] = (new_x, new_y)
			pos_x.append(new_x)
			pos_y.append(new_y)
		legend_pos = (min(pos_y), max(pos_x))
	
	for glabel,g in graph:
		if not get_positions_from_set:
			if layout == "circular":
				pos = nx.circular_layout(g)
			elif layout == "random":
				pos = nx.random_layout(g)
			elif layout == "shell":
				pos = nx.shell_layout(g)
			elif layout == "spring":
				pos = nx.spring_layout(g)
			elif layout == "spectral":
				pos = nx.spectral_layout(g)
			elif layout == "fruchterman_reingold":
				pos = nx.fruchterman_reingold_layout(g)
			elif layout == "graphviz":
				pos = nx.graphviz_layout(g)
				scale /= 100.0
			# determine shift key species to origin (0,0)
			origin = (0,0)
			for n in mastergraph.nodes(data=True):
				print(n)
				if "center" in list(n[1].keys()) and n[1]["center"]:
					origin = list(pos[n[0]])
					origin[0] *= scale
					origin[1] *= scale
					break
			if origin == (0,0):
				pos_x = []
				pos_y = []
				for k,coordinates in list(pos.items()):
					pos_x.append(coordinates[0])
					pos_y.append(coordinates[1])
				origin = (np.mean(pos_x)*scale, np.mean(pos_y)*scale)
			pos_x = []
			pos_y = []
			# shift all positions and collect them
			for k,coordinates in list(pos.items()):
				new_x = pos[k][0]*scale - origin[0]
				new_y = pos[k][1]*scale - origin[1]
				pos[k] = (new_x, new_y)
				pos_x.append(new_x)
				pos_y.append(new_y)
			legend_pos = (max(pos_y), max(pos_x))
		
		latex += "\n\\begin{tikzpicture}[overlay]\n"
		
		# add legend
		latex += "  \\node at (%s,%s)[black]{%s};\n" % (legend_pos[0], legend_pos[1], glabel)
		
		vertices = g.nodes(data=True)
		for i,v in enumerate(vertices):
			vname = v[0]
			vlabel = "%s" % vname
			vstyle = ""
			if "label" in list(v[1].keys()):
				vlabel = v[1]["label"]
			if "style" in list(v[1].keys()):
				vstyle += "%s" % (v[1]["style"])
			latex += "  \\node[VertexStyle]"
			latex += " (%s)" % (vname)
			latex += " at (%.3f, %.3f)" % (pos[vname][0], pos[vname][1])
			if not vstyle == "":
				latex += "[%s]" % (vstyle)
			latex += " {%s};\n" % (vlabel)
		
		edges = g.edges(data=True)
		num_multiedges_plotted = {}
		latex += "  \n"
		for e in edges:
			a = e[0]
			b = e[1]
			label = ""
			color = edge_color
			weight = edge_weight
			if "label" in list(e[2].keys()):
				label = e[2]["label"]
			if "weight" in list(e[2].keys()):
				weight = e[2]["weight"]
			if "color" in list(e[2].keys()):
				color = e[2]["color"]
			edgestyle = color
			if g.number_of_edges(a, b) > 11:
				raise NotImplementedError("the bending angles need fixed for high degeneracy multiedges!!!")
			if g.number_of_edges(a, b) > 1:
				print("found multiple edges between %s & %s!" % (a, b))
				try:
					num_multiedges_plotted['-'.join([a,b])] += 1
				except KeyError:
					num_multiedges_plotted['-'.join([a,b])] = 1
				angle = edge_angles[num_multiedges_plotted['-'.join([a,b])]]
				edgestyle += ",%s" % (angle)
			if not weighted:
				latex += "  \\Edge[label = %s](%s)(%s);\n" % (label, a, b)
			else:
				latex += "  \\draw[line width=%.1fpt]" % (weight*edge_linewidth)
				latex += " (%s)" % (a)
				latex += " edge[EdgeStyle,%s]" % (edgestyle)
				if not label == "":
					latex += " node[LabelStyle]{%s}" % (label)
				else:
					latex += " node{}"
				latex += " (%s);\n" % (b)
		latex += "\\end{tikzpicture}\n"
		if len(graph) > 1:
			latex += "\n\\newpage\n"
	
	# finish the latex code
	latex += "\\end{document}"
	
	latex = latex.replace('\t','')
	latex = latex.replace('%','\%')
	return latex




def network_to_deluxetable(
	network, title="", columns=[],
	singlespacing=False, # only works if \usepackage{setspace}
	padding_rows=None, # methoxy table of constants used '1.01'
	padding_columns=None, # methoxy table of constants used '1.5mm'
	fontsize="scriptsize", # any latex-compatible font size.. just drop the slashes (i.e. \scriptsize -> scriptsize)
	tablewidth=None,
	tabletype="deluxetable", # see note below about compatibility
	datalimit=None,
	label=None,
	commentreftofootnote=True):
	"""
	Creates a bare minimum deluxetable based on all the reactions contained
	in a network. This means one should create/process/prune/trim/blah using
	a new network object.
	
	Only basic features/layouts of a deluxetable are supported. This means
	that one should probably do a bit post-processing of the output prior
	to publication.
	
	Note that the deluxetable only available for emulateapj & apj packages,
	and the longtable & sidewaystable are useful for the a&a package.
	"""
	### check certain inputs
	allowedtables = ["deluxetable","longtable","sidewaystable","table"]
	if (tabletype is not None) and (not tabletype in allowedtables):
		raise SyntaxError("only the following table types (or None for default 'table') are allowed: %s" % allowedtables)
	elif tabletype is None:
		tabletype = "table*"
	
	if (tabletype == "deluxetable") and commentreftofootnote:
		print("warning: deluxetables with a footer bibliography aren't supported! will disable the footer bib")
		commentreftofootnote = False
	
	### parse columns/formatting
	colforms = []
	for ic,c in enumerate(columns):
		if isinstance(c, tuple):
			colforms.append(c[1])
			columns[ic] = c[0]
		else:
			colforms.append("str")
	
	### generate header
	header = "{"
	# spacings
	if singlespacing:
		header += "\\singlespacing\n"
	if padding_rows is not None:
		header += "\\renewcommand{\\arraystretch}{%s}\n" % padding_rows
	if padding_columns is not None:
		header += "\\renewcommand{\\tabcolsep}{%s}\n" % padding_rows
	# table environment
	if tabletype == "deluxetable":
		header += "\\begin{deluxetable*}"
	elif tabletype == "longtable":
		header += "\\longtab[1]{\n\\begin{longtable}"
	else:
		header += "\\begin{%s}\\centering\n" % tabletype
		header += "\\caption{%s}\n" % title
		if label is not None:
			header += "\\label{%s}\n" % label
		#header += "\\tabletypesize{\\%s}\n" % fontsize
		header += "\\begin{tabular}"
	# table column alignment
	header += "{"
	for c in columns:
		header += "l"
	header += "}\n"
	# misc options
	if tablewidth is not None:
		header += "\\tablewidth{%s}\n" % tablewidth
	if tabletype == "deluxetable":
		header += "\\tabletypesize{\\%s}\n" % fontsize
	# caption & label
	if tabletype in ["deluxetable","longtable"]:
		if tabletype == "deluxetable":
			caption = "tablecaption"
		else:
			caption = "caption"
		header += "\\%s{%s" % (caption, title)
		if label is not None:
			header += " \\label{%s}" % label
		if tabletype == "deluxetable":
			spacer = ""
		else:
			spacer = " \\\\"
		header += "}%s\n" % spacer
	# column headers
	if tabletype == "deluxetable":
		header += "\\tablehead{\\colhead{"
		header += "%s" % "} & \\colhead{".join(columns)
		header += "}}\n\\startdata"
	else:
		header += "\\hline\n\\hline\n"
		header += "%s" % " & ".join(columns)
		header += " \\\\\n\\hline"
	if tabletype == "longtable":
		header += "\n\\endfirsthead\n\\caption{%s (cont'd)} \\\\\n\\hline\n" % title
		header += "%s" % " & ".join(columns)
		header += " \\\\\n\\hline\n\\endhead\n\\hline\n\\endfoot\n\\hline\n\\endlastfoot"
	
	### populate data, line by line
	data = ""
	refList = []
	for ir,rxn in enumerate(network.reactions):
		if (datalimit is not None) and (ir >= datalimit):
			break
		for ic,c in enumerate(columns):
			format = colforms[ic]
			### get/process property
			if (c == "reaction") and (format == "str"): # replace reaction with nicer format
				prop = ""
				# add reactants
				rtex = []
				for r in rxn.reactants:
					mtex = network.molecules.get_molecule(r).get_tex_name()
					rtex.append(mtex)
				prop += " + ".join(rtex)
				# add extra tex species
				if rxn.extra_species_tex[0] is not None:
					prop += rxn.extra_species_tex[0]
				# add arrow
				prop += " \ce{->} "
				# add products
				ptex = []
				for p in rxn.products:
					mtex = network.molecules.get_molecule(p).get_tex_name()
					ptex.append(mtex)
				prop += " + ".join(ptex)
				# add extra tex species
				if rxn.extra_species_tex[1] is not None:
					prop += rxn.extra_species_tex[1]
			elif (c == "comment") and commentreftofootnote and ("ref:" in getattr(rxn, c)):
				comment = getattr(rxn, c)
				prop = comment
				for match in re.findall("ref:[a-zA-Z0-9]+", comment):
					refTxt = match.replace("ref:", "")
					refIdx = getmatchingstringindex(refTxt, refList)
					if refIdx is None:
						refNum = "(%s)" % (len(refList) + 1)
						refList.append("%s~\\citet{%s}" % (refNum, refTxt))
					else:
						refNum = "(%s)" % (refIdx + 1)
					prop = prop.replace("ref:%s" % refTxt, refNum)
			else: # all the rest of the properties...
				prop = getattr(rxn, c)
			### format/write the property
			if format == "exp":
				data += "%.2e & " % prop
			elif format == "gen":
				data += "%g & " % prop
			elif format == "math":
				data += "$%s$ & " % prop
			elif format == "mhchem":
				data += "\\ce{%s} & " % prop
			elif format == "simptext":
				data += "%s & " % getsimptext(prop)
			else:	# "str" in other words..
				data += "%s & " % prop
		data = data[:-2] # chop the extra "& "
		data += "\\\\\n"
	if tabletype == "deluxetable":
		data = data[:-3] # chop several extra newlines
	else:
		data = data[:-1] # chop final newline
	
	if tabletype == "deluxetable":
		footer = "\\enddata\n"
		if commentreftofootnote and len(refList):
			raise NotImplementedError("cannot yet add a reflist to the deluxetable!")
		footer += "\\end{deluxetable*}}"
	elif tabletype == "longtable":
		footer = "\\end{longtable}\n"
		if commentreftofootnote and len(refList):
			footer += "\\tablebib{\n"
			footer += "; ".join(refList)
			footer += ".\n}\n"
		footer += "}}"
	else:
		footer = "\\hline\n\\end{tabular}\n"
		if commentreftofootnote and len(refList):
			footer += "\\tablebib{\n"
			footer += "; ".join(refList)
			footer += ".\n}\n"
		footer += "\\end{%s}}" % tabletype
	
	return "%s\n%s\n%s" % (header, data, footer)




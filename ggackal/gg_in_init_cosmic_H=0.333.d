# DESCRIPTION
#	This file should contain the initial fractional abundances
#	of all species. The reference species should be defined in
#	"gg_in_params.yml".

# listed here are values for cosmic standard abundances
# Ref: Przybilla et al., ApJ 688 (2008) - cosmic standard
# Ref: Esteban et al., MONRAS 355 (2004) - orion gas+dust
# Ref: Asplund et al., Annu.Rev.Astron.Astrophys. 47 (2009) - solar
H           0.3333333334
H2          0.3333333333
He          9.55e-2 # P08
O           5.7544e-4 # P08
C1+         2.0893e-4 # P08
N           5.7544e-5 # P08
S1+         1.66e-5 # E04
Na1+        1.74e-6 # A09
Si1+        3.1623e-5 # P08
Mg1+        3.6308e-5 # P08
Cl1+        2.88e-7 # E04
Fe1+        2.7542e-5 # P08
P1+         2.57e-7 # A09
F           3.63e-8 # A09

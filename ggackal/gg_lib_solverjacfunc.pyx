# cython: language_level=3, boundscheck=False, cdivision=True
from __main__ import ggtrial
from __main__ import params
from cytoolz import juxt
import numpy as np
cimport numpy as np

DTYPE = np.float
ctypedef np.float_t DTYPE_t

def jacfunc(np.ndarray[DTYPE_t, ndim=1] y, float t, float args):
	cdef int I = ggtrial.solution.jacval.shape[0]
	cdef int i, j
	
	#y.clip(min=params['defaultinitialabund'], max=0.5)
	y[-3] = ggtrial.physgrid.gettemp(t + args)
	y[-2] = ggtrial.physgrid.getdens(t + args)
	y[-1] = ggtrial.physgrid.getav(t + args)
	
	for i in range(I):
		ggtrial.solution.jacval[i] = juxt(ggtrial.solution.jac[i])(y)
	return ggtrial.solution.jacval

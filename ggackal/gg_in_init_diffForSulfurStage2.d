# DESCRIPTION
#	This file contains fractional abundances meant to "patch"
#	the initial abundances used as input for Stage 2.

# listed here are values for cosmic standard abundances
# Ref: (N12) Neufeld et al. 2012, A&A, 542, L6
# Ref: (N15) Neufeld et al. 2015, A&A, 577, A49
HS          4e-9  # N15
H2S         5e-10 # N15
CS          1e-9  # N15
SO          2.5e-10 # N15
# corrections to elements (yes, they are small)
C1+         -1.0e-9
O           -2.5e-10
S1+         -5.75e-10

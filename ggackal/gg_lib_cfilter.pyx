# cython: language_level=3, boundscheck=False, cdivision=True
import sys
if sys.version_info[0] == 3:
	izip = zip
else:
	from itertools import izip

def cfilter(attr, val, reactions):
	"""
	Provides a method to retrieve a list of all reactions that match
	an attribute with the desired property.
	
	:param attr: name of the property to match
	:param val: value of the property to match
	:param reactions: reaction list to check against
	:type attr: str
	:type val: any (that is appropriate)
	:type reactions: list
	"""
	cdef int I = len(reactions)
	cdef int i
	
	if not len(reactions):
		return []

	if isinstance(attr, list):
		found = list(reactions)
		if not len(attr) == len(val):
			raise SyntaxError("attr and val must be of the same length!")
		for a,v in izip(attr, val):
			found = cfilter(a, v, found)
	else:
		found = []
		for i in range(I):
			rxnprop = getattr(reactions[i], attr)
			# matching list against list
			if isinstance(rxnprop, list) and isinstance(val, list) and (val == rxnprop):
				found.append(reactions[i])
			# matching item within list
			elif isinstance(rxnprop, list) and (val in rxnprop):
				found.append(reactions[i])
			# matching int against int
			elif isinstance(rxnprop, int) and int(val) == rxnprop:
				found.append(reactions[i])
			# matching string within string
			elif not isinstance(rxnprop, list) and isinstance(val, list):
				if val[0] in rxnprop:
					found.append(reactions[i])
			# matching string against string
			elif val == rxnprop:
				found.append(reactions[i])
	return found

# NOTES
# - this has been significantly modified in the following ways, from that of GWWH08/Laas2011
#	O (and its dependents) has been updated according to He2014 to have a much higher binding-energy
#	many sulfur species have been added to reflect changes in the network
#	the binding energy of NH3 (and its dependents) has been lowered to match the rest of the literature (i.e. GH06/GWWH08/Laas2011 were exceptions)
#
Species           E_bind  E_diff  QTunBar (K)   dHf(kcal/mol)
gH                 450       225  3.0E+01        +51.63    # Herma, Ebfac=50%
gH2                430       215        0           0.0    # Herma, Ebfac=50%
gHe                100         0  1.2e-01           0.0    # T&H(p248), dEb=WT(Ar)
gC                 800         0  7.5e-04       +169.98    # TA H2Oice, dEb=WT(H2O), dHf from Schuurman+ 2004
gN                 800         0  7.5e-04       +112.53    # TA H2Oice, dEb=WT(H2O), dHf from Schuurman+ 2004
gO                 1764        0  7.5e-04        +58.99    # He2014; was 800
gNa                11800       0        0        +25.69    # Hase pol=23.6E-24
gMg                5300        0        0        +34.87    # Hase pol=10.6E-24cm+3
gSi                2700        0        0        +106.6    # Hase pol=5.4(-24)cm+3
gP                 1100        0        0        +75.62    #
gS                 1100        0        0        +65.66    # TA H2Oice
gCl                3000        0        0        +28.99    # Wakelam's suggestion
gFe                4200        0        0        +99.30    # Hase pol=8.4E-24
gCH                925         0        0       +142.00    # C+(1/4)*(CH4-C), dHf confirmed by Etim+ 2016
gCH2               1050        0        0        +93.35    # C+(2/4)*(CH4-C)
gCH3               1175        0        0        +34.82    # C+(3/4)*(CH4-C)
gCH4               1300        0        0        -17.89    # Hermas suggestion
gNH                1533        0        0        +85.92    # N+(1/3)*(NH3-N), dHf from Schuurman+ 2004
gNH2               2267        0        0        +45.50    # (NH3+NH)/2
gNH3               3000        0        0          -9.3    # MD2014, dHf from Schuurman+ 2004
gOH                2850        0        0         +8.85    # H2O/2, dHf from Schuurman+ 2004
gH2O               5700        0        0        -57.10    # Hermas suggestion, dHf from Schuurman+ 2004
gO2                930         0        0           0.0    # Jing2012
gO3                1833        0        0        +34.10    # Jing2012
gHO2               4614        0        0         +0.50    # O+OH
gH2O2              5700        0        0        -32.53    # OH+OH
gNaH               12250       0        0        +29.70    # Na+H
gNaOH              14650       0        0        -47.27    # Na+OH
gNaCN              13400       0        0         +22.5    # Na+CN, dHf from Etim+ 2017
gFeH               4650        0        0        +117.2    # Fe+H, dHf from Riley&Merz 2007
gFeS               5300        0        0         +83.8    # Fe+S, dHf from Riley&Merz 2007
gMgH               5750        0        0        +40.40    # Mg+H
gMgH2              6200        0        0         -18.2    # MgH+H, dHf from Pozzo&Alfe 2008
gSiH               3150        0        0        +90.02    # Si+H
gSiH2              3600        0        0        +62.75    # SiH+H
gSiH3              4050        0        0        +35.47    # SiH2+H
gSiH4              4500        0        0         +8.20    # SiH3+H
gSiC               3500        0        0        +177.1    # Si+C, dHf from Etim+ 2017
gSiCH              3625        0        0       -999.99    # Si+CH
gSiCH2             3750        0        0       -999.99    # Si+CH2
gSiCH3             3875        0        0       -999.99    # Si+CH3
gSiC2              4300        0        0        +152.0    # SiC+C, dHf from Etim+ 2017
gSiC2H             4837        0        0       -999.99    # Si+C2H
gSiC2H2            5287        0        0       -999.99    # Si+HC2H
gSiC3              5100        0        0        +228.8    # Si+C3, dHf from Etim+ 2017
gSiC3H             5637        0        0       -999.99    # Si+C3H
gSiC4              5900        0        0        +216.9    # SiC+C3, dHf from Etim+ 2017
gSiN               3500        0        0        +89.00    # Si+N
gHNSi              4233        0        0       -999.99    # Si+NH
gSiNC              4300        0        0       -999.99    # Si + CN
gSiO               4464        0        0        -24.00    # Si+O
gSiO2              6228        0        0        -73.00    # Si+O+O
gH2SiO             5364        0        0        -999.9    # 2*SiH + SiO - 2*Si
gSiS               3800        0        0         +25.3    # Si+S, dHf from Cioslowski2000
gPH                1550        0        0        +60.60    # P + H
gPH2               2000        0        0        +30.10    # P + 2*H
gPN                1900        0        0        +25.04    # P + N
gPO                2864        0        0         -5.63    # P + O
gHCP               2025        0        0        +35.83    # CH + CP - C
gHPO               3314        0        0        -12.70    # PO + H
gS2                550         0        0         +50.2    # S*0.5 (i.e. O vs O2), dHf from Steudel+ 2003   were: 550
gS3                1100        0        0         +49.7    # S2 + (n-2)*S2/2, dHf from Steudel+ 2003             1650
gS4                1650        0        0         +47.8    # S2 + (n-2)*S2/2, dHf from Steudel+ 2003             1925
gS5                2200        0        0         +36.0    # S2 + (n-2)*S2/2, dHf from Steudel+ 2003             2200
gS6                2750        0        0         +31.2    # S2 + (n-2)*S2/2, dHf from Steudel+ 2003             2475
gS7                3300        0        0         +31.0    # S2 + (n-2)*S2/2, dHf from Steudel+ 2003             2750
gS8                3850        0        0         +25.1    # S2 + (n-2)*S2/2, dHf from McBride+ 2002             3025
gHS                1372        0        0         +33.9    # H2S/2, dHf from McBride+ 2002
gH2S               2743        0        0         -4.23    # Collings, dHf from McBride+ 2002
gH2SO4             0           0        0        -171.8    # ?, dHf from McBride+ 2002
gHSO               3314        0        0           0.0    # SO+H, dHf from average of Luke & McLean 1985
gS2H               1000        0        0         +22.0    # S2+H, dHf from Griller+ 1990 (empirical estimate!)
gH2S2              3193        0        0          +3.8    # H2S+H, dHf from Cheng & Hung 1996
gS2O               2314        0        0         -12.9    # S2+O, dHf from McBride+ 2002
gCH2SH             2096        0        0         +37.7    # CH3SH-H, dHf from Ruscic&Berkowitz1992
gCH3S              3250        0        0         +29.7    # H2CS+H, dHf from Resende&Ornellas2003
gCH3SH             2546        0        0         -5.46    # CH3+HS, dHf from JANAF table
gCS3               1900        0        0        -999.9    # C+S3
gClH               3450        0        0        -22.06    # Cl + H
gClC               3800        0        0       +120.00    # Cl + C
gClO               4764        0        0        +24.19    # Cl + O
gCP                1900        0        0       +107.53    # C + P
gCCP               2700        0        0        +153.1    # C2 + P, dHf from Etim+ 2016
gC3P               3500        0        0        +174.7    # C2P + C, dHf from Etim+ 2016
gC4P               4300        0        0        +199.1    # C3P + C, dHf from Etim+ 2016
gCH2PH             2600        0        0        -67.40    # CH2 + PH
gHCCP              3150        0        0       -999.99    # CCP + H
gCO                1150        0        0        -27.20    # Hermas suggestion, dHf from Schuurman+ 2004
gCO2               2575        0        0        -93.97    # Collings, dHf from Schuurman+ 2004
gHCO               1600        0        0        +10.40    # CO+H
gHOC               3650        0        0        +10.40    # C+OH
gH2CO              2050        0        0        -27.70    # HCO+H
gHCOH              4634        0        0        -27.70    # CH2OH-H
gCH2OH             5084        0        0         -4.10    # CH3OH-H, dHf from Cioslowski+ 2000
gCH3O              2500        0        0         +4.10    # H2CO+H, dHf from Cioslowski+ 2000
gCH3OH             5534        0        0        -48.00    # Collings
gN2                1000        0        0           0.0    # Hermas suggestion
gN2H               1450        0        0        +76.20    # N2+H
gN2H2              3067        0        0        +50.90    # NH2+N
gNO                2564        0        0        +21.58    # N+O
gHNO               3014        0        0        +23.80    # H+N+O
gHON               3650        0        0        +23.80    # N+OH
gNO2               4328        0        0         +7.91    # N+O+O
gN2O               3364        0        0        +19.61    # N+N+O
gNS                1900        0        0        +63.00    # N+S
gNH2CN             3867        0        0        +33.67    # NH2+CN, dHf from KIDA (Burcat)
gSO                2864        0        0         +1.12    # S+O, dHf from McBride+ 2002
gSO2               3405        0        0        -70.33    # Collings, dHf from McBride+ 2002
gSO3               5169        0        0         -93.3    # SO2+O, dHf from McBride+ 2002
gCN                1600        0        0        +106.4    # C+N, dHf from Etim+ 2016
gHCN               2050        0        0        +32.30    # H+CN
gHNC               2050        0        0         +46.5    # HCN, dHf from Etim+ 2017
gCNH2              2500        0        0        +55.70    # HCN+H
gH2CN              2500        0        0        +55.70    # HCN+H
gCS                1900        0        0         +72.4    # C+S, dHf from Etim+ 2016
gCS2               3800        0        0         +27.7    # 2*CS, dHf from McBride+ 2002
gCSSH              4250        0        0           0.0    # CS2+H
gHCSSH             3722        0        0           0.0    # HCS+HS
gHCOSH             2972        0        0         -30.0    # HCO+HS, dHf from Benson 1978
gHCS               2350        0        0        +71.70    # CS+H
gH2CS              2800        0        0        +24.30    # HCS+H, dHf from Benson 1978
gC2S               1075        0        0       +141.31    # C+C+S, dHf from Etim+ 2016
gC3S               1875        0        0       +136.29    # C2S+C, dHf from Etim+ 2016
gC4S               2675        0        0       +196.96    # C3S+C, dHf from Etim+ 2016
gC5S               3475        0        0       +190.68    # C4S+C, dHf from Etim+ 2016
gHC2S              1525        0        0       -999.99    # C2S + H
gHC3S              2325        0        0       -999.99    # C3S + H
gOCN               3364        0        0         +30.6    # O+C+N, dHf from Schuurman+ 2004
gOCN1-             3364        0        0         -52.8    # O+C+N, dHf from Etim+ 2017
gHNCO              3814        0        0        -33.36    # OCN+H, dHf from Etim+ 2017
gHOCN              3814        0        0          -4.4    # OCN+H, dHf from Etim+ 2017
gOCS               2888        0        0        -33.90    # Collings, dHf from McBride+ 2002
gOCS2              3988        0        0       -999.99    # OCS + S
gHSCN              2972        0        0         +38.3    # HS + CN, dHf from Etim+ 2017
gHNCS              3433        0        0         +25.0    # NH + CS, dHf from Etim+ 2017
gHCNS              2825        0        0         +59.4    # CH + NS, dHf from Etim+ 2017
gNH2CS             4167        0        0       -999.99    # NH2 + CS
gNH2CHS            4617        0        0       -999.99    # NH2 + HCS
gNH2CH2SH          4688        0        0       -999.99    # NH2 + CH2 + HS
gC2                1600        0        0       +194.43    # 2*C, dHf from Etim+ 2016
gC3                2400        0        0       +244.79    # 3*C, dHf from Etim+ 2016
gC4                3200        0        0       +261.33    # 4*C, dHf from Etim+ 2016
gC5                4000        0        0       +305.82    # 5*C, dHf from Etim+ 2016
gC6                4800        0        0       +310.38    # 6*C, dHf from Etim+ 2016
gC7                5600        0        0       +356.50    # 7*C, dHf from Etim+ 2016
gC8                6400        0        0       +363.74    # 8*C, dHf from Etim+ 2016
gC9                7200        0        0       +373.33    # 9*C
gC10               8000        0        0       +420.89    # 10*C
gC2H               2137        0        0        +148.0    # C2H2 - H, dHf from Etim+ 2016
gHC2H              2587        0        0        +54.19    # Collings, dHf confirmed by Etim+ 2016
gH2C2H             3037        0        0        +71.00    # C2H2 + H
gH2C2H2            3487        0        0        +12.54    # C2H2 + 2*H
gCH3CH2            3937        0        0        +28.40    # C2H2 + 3*H
gCH3CH2CHO         3825        0        0        -45.00    # CH3CHO + CH2
gCH3CH3            4387        0        0        -20.04    # C2H5 + H
gC3H               2937        0        0        +173.6    # C2H + C, dHf from Etim+ 2016
gC3H2              3387        0        0       +152.59    # C3H + H, dHf from Etim+ 2016
gC3H3              3837        0        0        +81.00    # C3H + 2*H
gC3H4              4287        0        0        +44.32    # C3H + 3*H
gC3H5              4737        0        0        +40.90    # C3H + 4*H
gC3H6              5187        0        0         +4.88    # C3H + 5*H
gC3H7              5637        0        0        +22.00    # C3H + 6*H
gC3H8              6087        0        0        -25.02    # C3H + 7*H
gC4H               3737        0        0        +194.8    # C3H + C, dHf from Etim+ 2016
gHC4H              4187        0        0       +110.69    # C4H + H, dHf from Etim+ 2016
gC4H3              4637        0        0       +110.66    # C4H + 2*H
gC4H4              5087        0        0        +70.40    # C4H + 3*H
gC4H5              5537        0        0        +73.00    # C4H + 4*H
gC4H6              5987        0        0        +39.48    # C4H + 5*H
gC5H               4537        0        0        +224.7    # C4H + C, dHf from Etim+ 2016
gC5H2              4987        0        0       +185.28    # C5H + H, dHf from Etim+ 2016
gC5H3              5437        0        0       +130.52    # C5H + 2*H
gC5H4              5887        0        0       +115.00    # C5H + 3*H
gCH3C4H            5887        0        0       +101.00    # C5H4
gC6H               5337        0        0        +247.6    # C5H + C, dHf from Etim+ 2016
gC6H2              5787        0        0       +161.68    # C6H + H, dHf from Etim+ 2016
gC6H3              6237        0        0       +161.38    # C6H + 2*H
gC6H4              6687        0        0       +128.60    # C6H + 3*H
gC6H6              7587        0        0        +19.82    # C6H + 5*H
gC7H               6137        0        0        +279.7    # C6H + C, dHf from Etim+ 2016
gC7H2              6587        0        0       +226.69    # C7H + H, dHf from Etim+ 2016
gC7H3              7037        0        0       +182.50    # C7H + 2*H
gC7H4              7487        0        0       +135.00    # C7H + 3*H
gCH3C6H            7487        0        0       +135.00    # C7H4
gC8H               6937        0        0        +304.5    # C7H + C, dHf from Etim+ 2016
gC8H2              7387        0        0       +208.13    # C8H + H
gC8H3              7837        0        0       +212.93    # C8H + 2*H
gC8H4              8287        0        0       +200.00    # C8H + 3*H
gC9H               7737        0        0       +329.11    # C8H + C
gC9H2              8187        0        0       +277.49    # C9H + H
gC9H3              8637        0        0       +234.08    # C9H + 2*H
gC9H4              9087        0        0       +200.00    # C9H + 3*H
gC2O               1950        0        0        +91.37    # CO + C, dHf from Etim+ 2016
gC3O               2750        0        0        +75.33    # C2O + C, dHf from Etim+ 2016
gHC2O              2400        0        0        +42.40    # C2O + H
gCH2CO             2200        0        0        -20.85    # CH2 + CO
gHC3O              3200        0        0        +46.23    # C3O + H
gcH2C3O            3650        0        0        +23.00    # C3O + 2*H
gNHNO              3814        0        0       -999.99    # HNO + N
gNH2NO             4264        0        0       -999.99    # NHNO + H
gC2N               2400        0        0        +163.5    # CN + C, dHf from Etim+ 2016
gC3N               3200        0        0        +173.7    # CN + 2*C, dHf from Etim+ 2016
gC4N               4000        0        0        +210.5    # CN + 3*C, dHf from Etim+ 2016
gC5N               4800        0        0        +231.7    # CN + 4*C, dHf from Etim+ 2016
gC7N               6400        0        0        +287.7    # CN + 6*C, dHf from Etim+ 2016
gC9N               8000        0        0        +354.0    # CN + 8*C
gHCNH              2458        0        0        +55.70    # CH + NH
gCH2NH             2583        0        0        +26.00    # CH2 + NH
gCH3NH             2708        0        0        +43.60    # CH3 + NH
gCH2NH2            3317        0        0        +38.00    # CH2 + NH2
gCH3NH2            3442        0        0         -5.50    # CH3 + NH2
gHC2N              3780        0        0       +122.65    # CH2CN - H, dHf confirmed by Etim+ 2016
gHC3N              4580        0        0        +84.63    # HC2N + C, dHf confirmed by Etim+ 2016
gHC5N              6180        0        0        +140.6    # HC2N + C*3, dHf from Etim+ 2016
gHC7N              7780        0        0        +191.8    # HC2N + C*5, dHf from Etim+ 2016
gHC9N              9380        0        0        +242.9    # HC2N + C*7, dHf from Etim+ 2016
gC2NCH             4580        0        0        +84.60    # HC3N
gHC2NC             4580        0        0        +84.60    # HC3N
gHNC3              4580        0        0        +84.60    # HC3N
gH2C2N             4230        0        0        +57.61    # CH3CN - H, dHf from Etim+ 2016
gCH3CN             4680        0        0        +17.70    # Collings
gHC3NH             5030        0        0        +152.6    # HC3N + H, dHf from Etim+ 2016
gC3H3N             5480        0        0        +42.95    # HC3N + 2*H
gH4C3N             5930        0        0       -999.99    # HC3N + 3*H
gH5C3N             6380        0        0        +12.30    # HC3N + 4*H
gH2C5N             6630        0        0        +185.3    # HC5N + H, dHf from Etim+ 2016
gH3C5N             7080        0        0       -999.99    # HC5N + 2*H
gH2C7N             8230        0        0        +226.7    # HC7N + H, dHf from Etim+ 2016
gH3C7N             8680        0        0       -999.99    # HC7N + 2*H
gH2C9N             9830        0        0       -999.99    # HC9N + H
gH3C9N             10280       0        0       -999.99    # HC9N + 2*H
gCH3C3N            6280        0        0        +81.00    # C3H3N + C
gCH3C5N            7880        0        0       -999.99    # H3C5N + C
gCH3C7N            9480        0        0       -999.99    # H3C7N + C
gNH2OH             5117        0        0        -10.00    # NH2 + OH
gNH2CHO            3867        0        0        -44.50    # NH2 + HCO
gNH2NH             3800        0        0        +58.20    # NH + NH2
gHNOH              4383        0        0        +27.29    # NH + OH
gNH2NH2            4533        0        0        +22.80    # 2*NH2
gNH2OCH3           4767        0        0       -999.99    # NH2 + CH3O
gNH2CH2OH          7351        0        0       -999.99    # NH2 + CH2OH
gNH2CO             3417        0        0         -3.60    # NH2 + CO
gHNCHO             3133        0        0        +19.53    # NH + HCO
gHNCONH            4217        0        0       -999.99    # CO + 2*NH
gNH2CONH           4950        0        0       -999.99    # NH2 + CO + NH
gNH2CONH2          5683        0        0        -56.29    # 2*NH2 + CO
gHNCOOH            6653        0        0       -999.99    # NH + HCOOH - H
gNH2COOH           7387        0        0       -999.99    # HCOOH - H + NH2
gCH3ONH            4033        0        0       -999.99    # NH + CH3O
gHNCH2OH           6617        0        0       -999.99    # NH + CH2OH
gCH3OCONH          5183        0        0       -999.99    # CH3O + CO + NH
gHNCOCH2OH         7767        0        0       -999.99    # CH2OH + CO + NH
gCH3OCONH2         5917        0        0       -101.60    # CH3O + CO + NH2
gNH2COCH2OH        8501        0        0       -999.99    # NH2 + CO + CH2OH
gHNCOCHO           4283        0        0       -999.99    # NH + CO + HCO
gNH2COCHO          5017        0        0       -999.99    # NH2 + CO + HCO
gCH3CONH           3858        0        0       -999.99    # CH3 + CO + NH
gCH3CONH2          4592        0        0        -56.96    # CH3 + CO + NH2
gCOOH              5120        0        0        -43.91    # HCOOH - H
gHCOOH             5570        0        0        -90.49    # Collings
gCH3COOH           6295        0        0       -103.44    # COOH + CH3
gHCOCOOH           6720        0        0       -999.99    # HCO + COOH
gHOCOOH            7970        0        0       -999.99    # OH + COOH
gCH3OCOOH          7620        0        0       -145.08    # CH3O + COOH
gCH2OHCOOH         10204       0        0       -139.34    # CH2OH + COOH
gCH3OOCH3          5000        0        0        -30.00    # 2*CH3O
gCH3OCH2OH         7584        0        0       -999.99    # CH3O + CH2OH
gHOCH2CH2OH        10168       0        0        -94.26    # 2*CH2OH
gCH3OOH            5350        0        0        -31.31    # CH3O + OH
gHOCH2OH           7934        0        0        +14.79    # OH + CH2OH
gCH3CO             2325        0        0         -2.87    # CH3 + CO
gCH3CHO            2775        0        0        -40.80    # CH3 + HCO
gCH3OCO            3650        0        0        -39.90    # CH3O + CO
gHOCH2CO           6234        0        0        -39.20    # CH2OH + CO
gHCOOCH3           4100        0        0        -86.60    # HCO + CH3O
gCH2OHCHO          6684        0        0        -77.63    # CH2OH + HCO
gCH3OCH3           3675        0        0        -43.99    # CH3O + CH3
gCH3CH2OH          6259        0        0        -56.23    # CH3 + CH2OH
gCH3COCH3          3500        0        0        -52.00    # CH3 + CO + CH3
gCH3COOCH3         4825        0        0        -98.00    # CH3 + CO + CH3O
gCH3COCH2OH        7409        0        0       -999.99    # CH3 + CO + CH2OH
gCH3OCOOCH3        6150        0        0       -999.99    # CH3O + CO + CH3O
gCH3OCOCH2OH       8734        0        0       -133.10    # CH3O + CO + CH2OH
gHOCH2COCH2OH      11318       0        0       -999.99    # CH2OH + CO + CH2OH
gCH3OCOCHO         5250        0        0       -999.99    # CH3O + CO + HCO
gHOCH2COCHO        7834        0        0       -999.99    # CH2OH + CO + HCO
gCOCHO             2750        0        0       -999.99    # CO + HCO
gCH3COCHO          3925        0        0        -64.80    # CH3 + CO + HCO
gCHOCHO            3200        0        0        -50.67    # 2*HCO
gHCOCOCHO          4350        0        0       -999.99    # HCO + CO + HCO

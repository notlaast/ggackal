#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# DESCRIPTION
#	All subroutines specific to file logs should go here.
#
# standard library
from __future__ import absolute_import
import os
import sys
import shutil
from operator import itemgetter
if sys.version_info[0] == 3:
	import pickle
else:
	import cPickle as pickle
import socket
# third-party
import scipy
# local
if not os.path.dirname(os.path.abspath(__file__)) in sys.path:
	sys.path.append(os.path.dirname(os.path.abspath(__file__)))
from gg_lib_sys import *
from gg_lib_physics import *
from gg_lib_reac import *




# define wrapper for final general logs
def runinitiallogs():
	r"""
	Runs the initial logs.
	
	That is:
	1. creates the output directory;
	2. copies the input files; and,
	3. touches a dummy file denoting that the trial has not yet finished.
	"""
	if params['verbosity'] > 0: print("running the initial logs")
	if params['verbosity'] > 1: print("saving the logs to %s" % params['log_dir'])
	
	init_log_dir()
	
	# copy the input files
	try:
		outputfile_param_link = os.path.join(
			params['log_fulldir'], 'gg_in_params_%s.yml' % params['trialname'])
		os.symlink('gg_in_params.yml', outputfile_param_link)
	except:
		pass
	outputfile_param = os.path.join(
		params['log_fulldir'], "gg_in_params.yml")
	try:
		shutil.copy(params["paramfname"], outputfile_param)
	except:
		pass
	outputfile_network = os.path.join(
		params['log_fulldir'], os.path.basename(params['networkfname']))
	try:
		shutil.copy(params['networkfname'], outputfile_network)
	except:
		raise
	outputfile_initialabund = os.path.join(
		params['log_fulldir'], os.path.basename(params['initialabundfname']))
	try:
		shutil.copy(params['initialabundfname'], outputfile_initialabund)
	except:
		raise
	if params['initadjfname'] is not None:
		outputfile_initadj = os.path.join(
			params['log_fulldir'], os.path.basename(params['initadjfname']))
		try:
			shutil.copy(params['initadjfname'], outputfile_initadj)
		except:
			raise
	outputfile_surfspecies = os.path.join(
		params['log_fulldir'], os.path.basename(params['surfspeciesfname']))
	try:
		shutil.copy(params['surfspeciesfname'], outputfile_surfspecies)
	except:
		raise
	outputfile_ssdata = os.path.join(
		params['log_fulldir'], os.path.basename(params['ssdatafname']))
	try:
		shutil.copy(params['ssdatafname'], outputfile_ssdata)
	except:
		raise
	
	# touch an empty temp file for warning user that run has not finished
	outputfile_notfinished = os.path.join(params['log_fulldir'], "RUN_HAS_NOT_FINISHED")
	with open(outputfile_notfinished, 'a'):
		os.utime(outputfile_notfinished, None)


# define wrapper for final general logs
def runfinallogs(trial):
	r"""
	Runs the final logs.
	
	That is, it:
	1. removes the dummy file denoting that the trial has not yet finished;
	2. invokes (when desired) the individual logging routines; and,
	3. removes the output directory is nothing was logged.
	"""
	if params['verbosity'] > 0: print("running the final logs")
	
	outputfile_notfinished = os.path.join(params['log_fulldir'], "RUN_HAS_NOT_FINISHED")
	if os.path.isfile(outputfile_notfinished):
		os.remove(outputfile_notfinished)
	
	# process the order info if necessary
	if params['log_reactions']:
		if params['verbosity'] > 0: print("logging the reactions that were used..")
		logtofile_reactions(trial)
	
	# process the order info if necessary
	if params['log_molprop']:
		if params['verbosity'] > 0: print("logging the molecules and their properties..")
		logtofile_molprop(trial)
	
	# process the order info if necessary
	if params['log_finalabund']:
		if params['verbosity'] > 0: print("logging the final abundance information..")
		logtofile_finalabund(trial)
	
	# process the order info if necessary
	if params['log_fullabund']:
		if params['verbosity'] > 0: print("logging the full abundance information for each species..")
		logtofile_fullabund(trial)
	
	# process the order info if necessary
	if params['log_order']:
		if params['verbosity'] > 0: print("processing the requested default ORDER information..")
		orderinfo = doorder(trial)
		if params['verbosity'] > 0: print("logging the default ORDER information to file..")
		logtofile_order(trial, orderinfo)
	
	if os.path.isdir(params['log_fulldir']) and os.listdir(params['log_fulldir'])==[]:
		print("it seems the log directory is empty; it will thus be removed")
		os.rmdir(params['log_fulldir'])
	
	if params['verbosity'] > 0: printsep()




def sendemail_completed():
	r"""
	Sends an e-mail informing the user that the trial has finished.
	
	NOTE:
	- This requires that a postfix mail server has been installed on the system!
	"""
	if params['verbosity'] > 0: 'will send an e-mail to note the completion of this trial'
	
	import smtplib
	
	hostname = socket.getfqdn()
	fromaddr = "ggackal-user@%s" % hostname
	toaddrs  = params['log_email_recipient']
	
	br = '\r\n'
	msgtext = 'The following GGackal trial has completed: '+ params['trialname'] + br
	msgtext += br + 'Stats:' + br
	msgtext += '\ttime: '+ str(datetime.datetime.now()) + br
	msgtext += '\tdirectory: '
	if log_level > 0:
		msgtext += params['log_fulldir'] + br
	else:
		msgtext += 'NONE' + br
	
	# build header (structure is important here!)
	msg = "From: %s" % fromaddr
	msg += "\nTo: %s" % toaddrs
	msg += "\nSubject: GGackal trial complete"
	
	msg += msgtext
	
	smtpserver = 'localhost'
	
	# send mail
	try:
		server = smtplib.SMTP(smtpserver)
		server.set_debuglevel(0)
		server.sendmail(fromaddr, toaddrs, msg)
		server.quit()
		if self.debugging:
			print("an email was successfully sent to %s" % toaddrs)
	except socket.gaierror:
		e = sys.exc_info()[1]
		print("ERROR: there appears to be no local mail server!")
		print("\terror was: %s" % e)
		print("\tto fix this, install postfix, which is a send-only local service")
		print("\t(optionally) see also https://realpython.com/python-send-email/")
	except smtplib.SMTPRecipientsRefused:
		e = sys.exc_info()[1]
		print("ERROR: there was a problem with the local mail server!")
		print("\terror was: %s" % e)
		print("\tto fix this, check /etc/postfix/main.cf.. maybe you need lines like:")
		print("\t\tmynetworks = 127.0.0.0/8 [::ffff:127.0.0.0]/104 [::1]/128")
	except UserWarning as e:
		print(e)
	except:
		e = sys.exc_info()[1]
		print("ERROR: there was an unexpected problem sending an e-mail!")
		print("\terror was: %s" % e)




def logtofile_reactions(trial):
	r"""
	Logs the reaction info.
	
	OUTPUT:
	- a file named 'ggo_reactions.d' that contains all the info known about each reaction
	"""
	outputfile = os.path.join(params['log_fulldir'], "ggo_reactions.d")
	keystoskip = ['kinetics', "trial", "parent"]
	with open(outputfile, 'w') as f:
		for rxn in sorted(trial.network.reactions, key=lambda x: x.numTOT):
			f.write("{\n")
			for k in sorted(dir(rxn)):
				if k in keystoskip: continue
				if not k in rxn.__dict__: continue
				f.write("\t%s => %s\n" % (k, getattr(rxn, k)))
			f.write("}\n\n")


def logtofile_molprop(trial):
	r"""
	Logs the molecular properties (except its DE, abundances, and up-to-date density)..
	
	OUTPUT:
	- a file named 'ggo_molprop.d' that contains all the info known molecular properties
	"""
	outputfile = os.path.join(params['log_fulldir'], "ggo_molprop.d")
	keystoskip = ['de', 'fabunds', 'density']
	with open(outputfile, 'w') as f:
		for s in sorted(trial.molecules.get_names()):
			f.write("{\n")
			f.write("\tspecies: %s\n" % s)
			for k in sorted(dir(trial.getmol(s))):
				if k in keystoskip: continue
				if not k in trial.getmol(s).__dict__: continue
				f.write("\t\t%s => %s\n" % (k, getattr(trial.getmol(s), k)))
			f.write("}\n\n")


def logtofile_finalabund(trial):
	r"""
	Logs the final fractional abundances of each species.
	
	OUTPUT:
	- a file named 'ggo_fullabund.d' that contains the time evolution of all species
	"""
	outputfile = os.path.join(params['log_fulldir'], "ggo_tail.d")
	descriptor = "FINAL OUTPUT AS PER log_finalabund==True"
	it = len(trial.physgrid.times) - 1
	writeabunds(trial, it=it, fname=outputfile, descriptor=descriptor)


def logtofile_fullabund(trial):
	r"""
	Logs the fractional abundances of each species.
	
	OUTPUT:
	- a file named 'ggo_fullabund.d' that contains the time evolution of all species
	"""
	outputfile = os.path.join(params['log_fulldir'], "ggo_fullabund.obj")
	allfabunds = {}
	for s in sorted(trial.molecules.get_names()):
		allfabunds[s] = trial.getmol(s).fabunds
	allfabunds['times'] = trial.physgrid.times
	with open(outputfile, 'wb') as handle:
		pickle.dump(allfabunds, handle, protocol=2)


def logtofile_order(trial, orderinfo):
	r"""
	Logs the order info.
	
	OUTPUT:
	- a file named 'ggo_order.d' that contains the order info
	"""
	outputfile = os.path.join(params['log_fulldir'], "ggo_order.d")
	with open(outputfile, 'w') as f:
		if params['verbosity'] > 1: print("\twriting to output file now")
		f.write("="*50 + "\n")
		for it in orderinfo['iterations']:
			# write header
			iheader = "ORDER INFO FOR IT = %s" % it
			iheader += ", time = " + str(int(trial.physgrid.times[it]/float(yr))) + " yr"
			iheader += ", density = " + str(int(trial.physgrid.densities[it])) + " cm-1"
			iheader += "\n"
			f.write(iheader)
			# loop through molecules
			for s in orderinfo['species']:
				# write molecular info
				f.write("-"*10+"\n")
				molheader = "species: " + s
				molheader += ", frac: " + "{0:E}".format(float(trial.getmol(s).fabunds[it]))
				molheader += "\n"
				f.write(molheader)
				# loop through reactions
				formationlines = []
				sum_form = orderinfo['formtot'][s][it]
				for r in orderinfo['formrxns'][s]:
					rxn = r.reaction
					line = str(r.numTOT) + ": {:<40}".format(rxn)
					# print contribution
					rate = getreactionrate(trial, it, r) * r.products.count(s)
					line += "{0:E}".format(float(rate))
					# print rate
					line += "\n"
					formationlines.append((rate,line))
				# sort by rates, then output to file
				formationlines = sorted(formationlines, key=itemgetter(0), reverse=True)
				for r,l in formationlines:
					if r/float(sum_form) < params['log_order_mincontrib']: break
					f.write(l)
				# do it all again for destruction routes..
				destructionlines = []
				sum_dest = orderinfo['desttot'][s][it]
				for r in orderinfo['destrxns'][s]:
					rxn = r.reaction
					line = str(r.numTOT) + ": {:<40}".format(rxn)
					rate = - getreactionrate(trial, it, r) * r.reactants.count(s)
					line += " "*15 + "{0:E}".format(float(rate))
					line += "\n"
					destructionlines.append((rate,line))
				destructionlines = sorted(destructionlines, key=itemgetter(0))
				for r,l in destructionlines:
					if r/float(sum_dest) < params['log_order_mincontrib']: break
					f.write(l)
				sumline = " "*35 + "sum:   " + "{0:E}".format(float(sum_form)) + " s-1"+ "   {0:E}".format(float(sum_dest)) + " s-1\n"
				f.write(sumline)
			f.write("="*75+"\n")


def writeabunds(trial, it=None, t=None, fname=None, descriptor=None):
	if (not it) and (not t):
		raise SyntaxError("You must specify either an iteration or the time (unit: yr) to use as output!")
	if it and t:
		print("warning: you requested both an iteration and a time, but this routine will only print the iteration..")
	if not fname:
		print("warning: you did not specify the name of the output file! will only return the results..")
	
	if fname:
		try:
			fileout = open(fname, 'w')
		except IOError:
			raise IOError("Could not open the file %s for writing out the abundances!" % fname)
	
	towrite = []
	
	# write out some sort of header info, such as:
	towrite.append("# This file was automatically generated as per the request of a user.\n")
	towrite.append("# Trial information:\n")
	towrite.append("#\ttrialname: %s\n" % params['trialname'])
	towrite.append("#\ttrial's original timestamp: %s\n" % ggstarttime)
	towrite.append("#\tfile generation timestamp: %s\n" % str(datetime.datetime.now()))
	towrite.append("#\titeration: %s\n" % it)
	towrite.append("#\ttime (yr): %s\n" % t)
	if descriptor:
		towrite.append("# Description:\n")
		towrite.append("#\t%s\n" % descriptor)
	
	years = [s/float(yr) for s in trial.physgrid.times]
	
	abunds = {}
	for s in sorted(trial.molecules.get_names()):
		sstring = s.replace("p", '+').replace('m',"-")
		if it:
			abunds[sstring] = trial.getmol(s).fabunds[it]
		else:
			fabundsplrep = scipy.interpolate.splrep([sec/float(yr) for sec in trial.physgrid.times], trial.getmol(s).fabunds)
			abunds[sstring] = scipy.interpolate.splev(t, fabundsplrep)
	
	if sys.version_info[0] == 3:
		abunds = sorted( ((v,k) for k,v in abunds.items()), reverse=True)
	else:
		abunds = sorted( ((v,k) for k,v in abunds.iteritems()), reverse=True)
	for abund,s in abunds:
		towrite.append("%-15s %s\n" % (s,abund))
	
	if not fname:
		return towrite
	else:
		for l in towrite:
			fileout.write(l)
		fileout.close()


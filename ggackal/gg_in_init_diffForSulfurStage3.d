                                         # DESCRIPTION
#	This file contains fractional abundances meant to "patch"
#	the initial abundances used as input for Stage 2.

# listed here are values for cosmic standard abundances
# Ref: (T95b) Turner 1995, ApJ, 455, 556-573
# Ref: (T96a) Turner 1996, ApJ, 461, 246-264
# Ref: (T96b) Turner 1996, ApJ, 468, 694-721
H2S         6e-9    # T96b
CS          4e-9    # T96a
HCS1+       2e-10   # T96a
H2CS        3e-9    # T96b
SO          4e-8    # T95b
SO1+        1e-9    # T96b
# corrections to elements (yes, they are small)
S1+         -6e-8

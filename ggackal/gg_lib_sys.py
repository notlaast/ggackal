#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# DESCRIPTION
#	All system-related routines and pre-processors should go here. This file
#	should also be the first to be loaded and executed.
#
# standard library
from __future__ import absolute_import
import os
import sys
import datetime
from pdb import set_trace
import subprocess
import tempfile
import resource
import types
import re
# third-party
import yaml
import pyximport; pyximport.install()
# local
if not os.path.dirname(os.path.abspath(__file__)) in sys.path:
	sys.path.append(os.path.dirname(os.path.abspath(__file__)))
from gg_lib_formatter import *

if sys.version_info[0] == 3:
	unicode = str

# load input params
if os.path.isfile(os.path.join(os.getcwd(), 'gg_in_params.yml')):
	print("it appears the parameter input file is found at '%s'" % os.getcwd())
	print("will use this one instead of the package one..")
	params_fh = open(os.path.join(os.getcwd(), 'gg_in_params.yml'), 'r')
else:
	params_fh = open(os.path.join(os.path.dirname(os.path.abspath(__file__)), 'gg_in_params.yml'), 'r')
params = yaml.load(params_fh)
for k,v in list(params.items()):
	if isinstance(v, str):
		if "#" in v:
			raise SyntaxError("input parameter '%s' contains a # symbol!" % k)
		if v.lower() == "none": params[k] = None
		else:
			match = re.match(r'[\d\.]+[e][-]*[\d]+', v)
			if match: params[k] = float(v)
params["paramfname"] = params_fh.name

# set model dir
params['model_dir'] = os.path.dirname(os.path.abspath(__file__))

# for getting date and time of startup
ggstarttime = str(datetime.datetime.now().replace(microsecond=0))


# define a name for the code, which gets referenced many times
codename = "GGackal"

# for referencing various object types elsewhere
list_types = (list, tuple, dict)
int_types = (int,)
float_types = (float,)
number_types = int_types + float_types
string_types = (str, unicode)
other_types = (bool,)
normal_types = string_types + list_types + number_types + other_types
function_type = types.FunctionType


def toshell(shellcommand):
	r"""
	Defines a pipe via the subprocess module to send a shell command.
	
	INPUTS:
	- ``shellcommand`` -- any command that could be run directly from command line
	
	EXAMPLES:
	- toshell('reset') -- resets the terminal screen
	- toshell('ls -l') -- lists files in the working directory
	
	TODO:
	- do something with the return values of spawned process (success vs failure?)
	"""
	return subprocess.Popen(shellcommand,shell=True,stdout=subprocess.PIPE).communicate()[0]

##########
# immediately reset the shell so that terminal scrollback is cleared
toshell('reset')





def showintro():
	r"""
	This is a wrapper for calling the routines that print the introductory texts.
	"""
	showreadme()
	showusage()




class gg_mem_object(object):
	"""
	Defines an object that is used to keep track of memory usage for the current
	trial.
	
	EXAMPLES:
	- gg14_memory_usage.get()
	
	NOTE:
	- gg14_memory_usage eventually has many custom properties defined throughout the run
	"""
	def __init__(self):
		self.init = self.get()
	
	def get(self):
		"""
		Returns a floating point value of the current memory usage.
		"""
		rusage_denom = 1024.
		return resource.getrusage(resource.RUSAGE_SELF).ru_maxrss / float(rusage_denom)
	
	def setfinal(self):
		self.final = self.get()

gg14_memory_usage = gg_mem_object()




def which(cmd):
	"""
	Simply provides a nicer wrapper for checking for the path/presence
	of an executable in the system path.
	"""
	import distutils.spawn
	return distutils.spawn.find_executable(cmd)




def init_log_dir():
	if params['log_dir'] == ".":
		params['log_fulldir'] = "."
		return
	
	log_dir = params['log_dir']
	print(log_dir)
	if not os.path.isdir(log_dir):
		os.mkdir(log_dir)
		if not os.path.isdir(log_dir):
			raise IOError("could not create the output directory: %s" % log_dir)
	
	log_trialdir = "%s_%s" % (params['trialname'], ggstarttime.replace('-', '').replace(' ', '_').replace(':',''))
	params['log_fulldir'] = os.path.join(log_dir, log_trialdir)
	
	if not os.path.isdir(params['log_fulldir']):
		os.mkdir(params['log_fulldir'])
		if not os.path.isdir(params['log_fulldir']):
			raise IOError("could not create the output directory: %s" % params['log_fulldir'])




def run_latex(filename, engine="latex", format=None):
	r"""
	Processes a file using latex.
	"""
	# process inputs
	if format is None:
		if engine == "latex":
			format = "dvi"
		elif engine == "pdflatex":
			format = "pdf"
	# process paths/filenames
	olddir = os.getcwd()
	basename = os.path.splitext(os.path.basename(filename))[0]
	# process file
	try:
		os.chdir("/tmp")
		os.system("`which %s` %s" % (engine, filename))
	except:
		print("there was a problem calling %s!" % engine)
	# do file conversion if necessary
	try:
		if (engine == "latex") and (format == "dvi"):
			pass
		elif (engine == "latex") and (format == "ps"):
			os.system("`which dvips` %s" % basename)
		elif (engine == "latex") and (format == "pdf"):
			os.system("`which dvipdf` %s" % basename)
		elif (engine == "latex") and (format == "png"):
			os.system("`which convert` %s.dvi %s.png" % (basename, basename))
		elif (engine == "pdflatex") and (format == "pdf"):
			pass
		elif (engine == "pdflatex") and (format == "png"):
			os.system("`which convert` %s.pdf %s.png" % (basename, basename))
	except:
		print("there was a problem converting the format!")
	# return to previous dir and then output file
	os.chdir(olddir)
	output = "%s.%s" % (basename, format)
	return output




def runsnake(command, globals=None, locals=None):
	r"""
	Loads the RunSnakeRun graphical profiler
	
	Note that `runsnake` requires the program ``runsnake``. On Ubuntu,
	this can be done with:
		> sudo apt-get install python-profiler python-wxgtk2.8
		> sudo pip install RunSnakeRun
	See the ``runsnake`` website for instructions for other platforms.
	
	`runsnake` further assumes that the system wide Python is
	installed in ``/usr/bin/python``.
	
	see also:
		- `The runsnake website <http://www.vrplumber.com/programming/runsnakerun/>`
		- ``%prun``
	
	:param command: the desired command to run, including the parentheses
	:type command: str
	"""
	if not isinstance(command, str):
		raise SyntaxError("the desired command should be a string")
	import cProfile
	if params['log_level'] > 0:
		init_log_dir()
		fd, tmpfile = tempfile.mkstemp(prefix='profile_rsr_', dir=params['log_fulldir'])
	else:
		fd, tmpfile = tempfile.mkstemp()
	os.close(fd)
	print("profiling command '%s' to file '%s' and launching it with 'runsnakerun'.." % (command, tmpfile))
	cProfile.run(command.lstrip().rstrip(), filename=tmpfile)
	try:
		os.system("/usr/bin/python -E `which runsnake` %s &" % tmpfile)
	except:
		print("there was a problem calling runsnake!")

def snakeviz(command):
	r"""
	Loads the SnakeViz graphical profiler
	
	Note that `runsnake` requires the program ``runsnake``. On Ubuntu,
	this can be done with:
		> sudo pip install snakeviz
	
	:param command: the desired command to run, including the parentheses
	:type command: str
	"""
	if not isinstance(command, str):
		raise SyntaxError("the desired command should be a string")
	import cProfile
	fd, tmpfile = tempfile.mkstemp()
	os.close(fd)
	print("profiling command '%s' to file '%s' and launching it with 'snakeviz'.." % (command, tmpfile))
	cProfile.run(command.lstrip().rstrip(), filename=tmpfile)
	try:
		os.system("snakeviz %s &" % tmpfile)
	except:
		print("there was a problem calling snakeviz!")

def kcachegrind(command):
	r"""
	Loads the valgrind-based KCacheGrind profiler
	
	:param command: the desired command to run, including the parentheses
	:type command: str
	"""
	if not isinstance(command, str):
		raise SyntaxError("the desired command should be a string")
	import hotshot
	fd, tmpfile = tempfile.mkstemp()
	os.close(fd)
	print("profiling command '%s' to file '%s' and launching it with 'kcachegrind'.." % (command, tmpfile))
	prof = hotshot.Profile(tmpfile, lineevents=1)
	prof.run(command)
	prof.close()
	try:
		os.system("hotshot2calltree -o %s.cachegrind.out.00 %s" % (tmpfile, tmpfile))
	except:
		print("there was a problem calling hotshot2calltree (from kcachegrind-converters)! see http://fperez.org/py4science/profiling/ for tips")
	os.system("kcachegrind %s.cachegrind.out.00 &" % tmpfile)




def showreadme(readmefile="../README.md"):
	r"""
	Prints out the project's readme file.
	
	INPUTS:
	- ``readmefile`` -- file that serves as the readme text (default: "README.md")
	"""
	printsep()
	if not os.path.exists(readmefile):
		readmefile = os.path.join(os.path.dirname(os.path.abspath(__file__)), readmefile)
	try:
		f = open(readmefile, 'r')
	except:
		print('Error: could not locate the readme file at "./%s"; skipping it.' % readmefile)
	else:
		print("printing the contents of the readme:\n")
		print(f.read())
		f.close()
	printsep()




def showusage():
	r"""
	Prints out a brief usage message for the code.
	"""
	print("You probably want to run this program using\n\t>./gg_main.py run")
	print("\nIf you run into trouble, try the following functions:")
	print("\tshowglobals() or print<type>(<ggobject>)")
	print("\tor manually insert 'debughere()' within the code to start PDB")
	printsep()




def showgpuwarn():
	r"""
	Prints out warning message about using GPUs
	
	NOTE:
	
	WARNING:
	
	TODO:
	- update information above to reflect its actual implementation..
	"""
	print("warning: will attempt to use the GPU for various solver functions")
	print("Note the following dependencies:")
	print("\thardware: a Cuda-enabled GPU")
	print("\tpython package: PyCUDA")
	print("\tpython package: Theano (if computejac == True)")
	printsep()




def debughere():
	r"""
	Provides a simple callable function for debugging the code.
	
	This routine first prints a message that you made it to the point you were hoping for. It then defines routines that allow one to view the global/local variables of the python namespace, and finally invokes PDB, the Python Debugger.
	
	WARNING:
	- The scope of locals is not what you expect, and is thus pointless in its current state.
	
	TODO:
	- fix the point above!
	"""
	def printglobals():
		# report all global variables
		print("all globals are:")
		for name in list(globals().keys()):
			print(name)
	def printlocals():
		# report local variables
		print("all locals are:")
		for name in list(locals().keys()):
			print(name)
	# print message and begin debugger
	print("You made it here!")
	print("Try running !printglobals() or !printlocals()")
	print("Will run the Python Debugger (pdb) now..")
	# begin the pythyon debugger
	set_trace()




# DESCRIPTION
#	This file should contain the initial fractional abundances
#	of all species. The reference species should be defined in
#	"gg_in_params.yml".

# F- and G-star photospheric abundances
# Ref: Sofia & Meyer, ApJ 554 (2001)
H           0.3333333334
H2          0.3333333333
He          0.1		# guess
O           4.45e-4
C1+         3.58e-4
N           7.5e-5	# guess
S1+         1.2e-5	# guess (O * 1/38)
Na1+        6e-7	# guess
Si1+        3.99e-5
Mg1+        4.27e-5
Cl1+        4e-8	# guess
Fe1+        2.79e-5
P1+         3e-8	# guess
F           3e-8	# guess

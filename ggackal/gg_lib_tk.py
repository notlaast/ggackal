#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# TODO
# - find all Exception / NotImplementedError and fix them
# - look into pyqtgraph's LabeledGraph example (not new from v0.9.10 ?!?)
#
# DESCRIPTION
#	Provides a class and methods for browsing the results of a
#	trial from GGackal.
#
# standard library
from __future__ import absolute_import
import os
import sys
from operator import itemgetter
import tempfile
import types
import re
from functools import partial
# third-party
if sys.version_info[0] == 3:
	try:
		import tkinter as tk
	except ImportError:
		msg = "Could not import tk! It's usually installed by default for any "
		msg += "python distribution.. anyway.."
		if 'anaconda' in str(sys.version).lower():
			msg += "\n\tTry doing something like 'conda install tk'\n"
		elif sys.platform == 'darwin':
			msg += "\n\tTry doing something like 'sudo port install py-tkinter'\n"
		else:
			msg += "\n\tTry doing something like 'sudo apt-get install python-tk'\n"
		raise ImportError(msg)
else:
	try:
		import Tkinter as tk
	except ImportError:
		msg = "Could not import tk! It's usually installed by default for any "
		msg += "python distribution.. anyway.."
		if 'anaconda' in str(sys.version).lower():
			msg += "\n\tTry doing something like 'conda install tk'\n"
		elif sys.platform == 'darwin':
			msg += "\n\tTry doing something like 'sudo port install py37-tkinter'\n"
		else:
			msg += "\n\tTry doing something like 'sudo apt-get install python3-tk'\n"
		raise ImportError(msg)
import numpy as np
import matplotlib.pyplot as plt
# local
if not os.path.dirname(os.path.abspath(__file__)) in sys.path:
	sys.path.append(os.path.dirname(os.path.abspath(__file__)))
from gg_lib_sys import *
from gg_lib_math import *
from gg_lib_physics import *
from gg_lib_reac import *
import gg_lib_plot
from gg_lib_plot import *
from gg_lib_logs import *
import gg_lib_formatter

if sys.version_info[0] == 3:
	from importlib import reload

#toshell('reset')
scaledownbydensity = False

###
# species list
def showspecieslist(parent, species):
	"""
	Loads a window containing all species in a list.
	"""
	trial = parent.trial
	###
	# determine masses and make species list
	def getSortTuple(species):
		mol = trial.getmol(species)
		if species[0] == 'g':
			mod_string = "%sg" % species[1:]
		else:
			mod_string = species
		return (mol.mass, mol.charge, mod_string)
	specieslist = []
	for s in species:
		specieslist.append(s)
	if parent.sortbypeakfabundvar.get()==1:
		specieslist = sorted(
			specieslist,
			key=lambda x: max(trial.getmol(x).fabunds),
			reverse=True)
	else:
		specieslist = sorted(specieslist, key=getSortTuple, reverse=False)
	###
	# set window properties
	win_specieslist = tk.Toplevel()
	win_specieslist.title("%s gui (%s): Species List" % (codename, trial.trialname))
	icon = tk.PhotoImage(file = os.path.join(os.path.dirname(__file__), 'grain.png'))
	win_specieslist.tk.call('wm', 'iconphoto', win_specieslist._w, icon)
	win_specieslist.bind("<Escape>", partial(close_window, window=win_specieslist))
	###
	# make time plot
	def plotspeciesselected(tkevent=False):
		r"""
		Creates a time plot of all selected species.
		"""
		if parent.forcepyqtvar.get()==1:
			params["plot_lib"] = "pyqtgraph"
		elif parent.switchBackToGnuplot:
			params["plot_lib"] = "gnuplot"
		import gg_lib_plot
		reload(gg_lib_plot)
		selectedspecies = [specieslist[int(item)] for item in specieslistbox.curselection()]
		print("will plot:", selectedspecies)
		altfabundfname = parent.altfabundentry.get()
		if altfabundfname and os.path.isdir(altfabundfname):
			print("warning: tried to load an alternative set of fabunds, but entry looks like a log directory.. will try to select its fabunds file")
			altfabundfname += "/ggo_fullabund.obj"
			parent.altfabundentry.delete(0, tk.END)
			parent.altfabundentry.insert(0, altfabundfname)
		if altfabundfname and (not os.path.isfile(altfabundfname)):
			print("warning: tried to load an alternative set of fabunds, but", altfabundfname, "isn't a file!")
			altfabundfname = ""
		elif altfabundfname and (not altfabundfname[-3:] == "obj"):
			print("warning: the alternative set of fabunds,", altfabundfname, ", must end with the '.obj' file extension")
			altfabundfname = ""
		width = parent.widthstring.get()
		if not len(width):
			width = 630
		height = parent.heightstring.get()
		if not len(height):
			height = 390
		saveplot = False
		if parent.saveplotvar.get() == 1:
			saveplot = "pdf"
		savehistory = bool(parent.savehistoryvar.get())
		xstart = parent.xstartstring.get()
		xstop = parent.xstopstring.get()
		ystart = parent.ystartstring.get()
		ystop = parent.ystopstring.get()
		plotsolutions(
			trial,
			los=selectedspecies, altfabundfname=altfabundfname,
			xstart=xstart, xstop=xstop,
			ystart=ystart, ystop=ystop,
			width=width, height=height,
			saveplot=saveplot, savehistory=savehistory)
	timeplotbutton = tk.Button(win_specieslist,
		text="Show Time Plots",
		command=plotspeciesselected)
	timeplotbutton.pack(fill=tk.X)
	###
	# browse species (for only the TOP species in the list)
	def viewspecies(tkevent=False):
		selectedspecies = [specieslist[int(item)] for item in specieslistbox.curselection()]
		browsespecies(parent, selectedspecies[0])
	speciesbrowserbutton = tk.Button(win_specieslist,
		text="Browse Species",
		command=viewspecies)
	speciesbrowserbutton.pack(fill=tk.X)
	###
	# make connectivity graph
	def graphselected(tkevent=False):
		r"""
		Creates a graph showing the direct connectivity of all species.
		"""
		import networkx as nx
		selectedspecies = [specieslist[int(item)] for item in specieslistbox.curselection()]
		print("selected species are", selectedspecies)
		# first get all reactions involving each species as reactant and then product
		allrxns = []
		for s in selectedspecies:
			allrxns += trial.network.filter('reactants', s)
			allrxns += trial.network.filter('products', s)
		allrxns = list(set(allrxns))
		# check all these reactions for intersections
		connectedrxns = []
		for i,s1 in enumerate(selectedspecies):
			for s2 in selectedspecies[i+1:]:
				connectedrxns += trial.network.filter(
					['reactants','reactants'], [s1, s2], allrxns)
				connectedrxns += trial.network.filter(
					['products','products'], [s1, s2], allrxns)
				connectedrxns += trial.network.filter(
					['reactants','products'], [s1, s2], allrxns)
				connectedrxns += trial.network.filter(
					['products','reactants'], [s1, s2], allrxns)
		connectedrxns = list(set(connectedrxns))
		if not len(connectedrxns):
			print("found no reactions in common!")
			return
		# define graph
		g = nx.MultiDiGraph()
		for rxn in connectedrxns:
			# add nodes for each reaction
			rxn_node = rxn.reaction.split('->')[0].strip()
			rxn_node = rxn_node.replace('(','').replace(')','')
			rxn_label = ' + '.join([trial.getmol(s).get_tex_name() for s in rxn.reactants])
			if rxn.extra_species_tex[0]:
				rxn_label += rxn.extra_species_tex[0]
			rxn_label += " $\\rightarrow$ "
			rxn_label += ' + '.join([trial.getmol(s).get_tex_name() for s in rxn.products])
			if rxn.extra_species_tex[1]:
				rxn_label += rxn.extra_species_tex[1]
			g.add_node(rxn_node, label=rxn_label,
				style="font=\\ttfamily,opacity=0.2,text opacity=1,fill=black!5")
			# add nodes & edges for each reactant
			for s in rxn.reactants:
				if not s in selectedspecies:
					continue
				a_node = s
				a_label = trial.getmol(s).get_tex_name()
				g.add_node(a_node, label=a_label)
				g.add_edge(a_node, rxn_node, color="red")
			for s in rxn.products:
				if not s in selectedspecies:
					continue
				b_node = s
				b_label = trial.getmol(s).get_tex_name()
				g.add_node(b_node, label=b_label)
				g.add_edge(rxn_node, b_node, color="blue")
		reload(gg_lib_formatter)
		latex = gg_lib_formatter.nx_to_latex(
			graph=g, layout="graphviz", directed=nx.is_directed(g),
			scale=5, edge_linewidth=1)
		reload(gg_lib_plot)
		gg_lib_plot.show_graph(latex=latex, show_plot=False, nohup_latex=True, autocrop_latex=True)
		browserxns(parent, connectedrxns)
	connectivitybutton = tk.Button(win_specieslist,
		text="Show Connectivity",
		command=graphselected)
	connectivitybutton.pack(fill=tk.X)
	###
	# create scrollbar and listbox for species list
	speciesscrollbar = tk.Scrollbar(win_specieslist)
	speciesscrollbar.pack(side=tk.RIGHT, fill=tk.Y)
	specieslistbox = tk.Listbox(
		win_specieslist,
		selectmode=tk.EXTENDED,
		exportselection=0,
		height=20)
	for s in specieslist:
		mol = trial.getmol(s)
		mass = int(round(mol.mass/float(amu)))
		string = "%3g: %s" % (mass, s)
		specieslistbox.insert(tk.END, string)
	specieslistbox.pack(fill=tk.BOTH, expand=tk.YES)
	specieslistbox.config(yscrollcommand=speciesscrollbar.set)
	speciesscrollbar.config(command=specieslistbox.yview)
	###
	# show window
	win_specieslist.mainloop()

###
# species browser
def browsespecies(parent, species):
	r"""
	Creates a window which lets one view various properties of a single species.
	"""
	trial = parent.trial
	###
	# set window properties
	win_speciesbrowser = tk.Toplevel()
	win_speciesbrowser.title("%s gui (%s): Browsing %s" % (codename, trial.trialname, species))
	icon = tk.PhotoImage(file = os.path.join(os.path.dirname(__file__), 'grain.png'))
	win_speciesbrowser.tk.call('wm', 'iconphoto', win_speciesbrowser._w, icon)
	win_speciesbrowser.bind("<Escape>", partial(close_window, window=win_speciesbrowser))
	###
	# define text area where all reports are pushed to
	textbox = tk.Text(
		win_speciesbrowser,
		height=50, width=100,
		exportselection=0)
	textbox.pack(side=tk.LEFT, fill=tk.BOTH, expand=tk.YES)
	textscrollbar = tk.Scrollbar(win_speciesbrowser)
	textscrollbar.pack(side=tk.LEFT, fill=tk.Y)
	textbox.config(yscrollcommand=textscrollbar.set)
	textscrollbar.config(command=textbox.yview)
	###
	# define iteration list
	iterscrollbar = tk.Scrollbar(win_speciesbrowser)
	iterscrollbar.pack(side=tk.RIGHT, fill=tk.Y)
	iterlistbox = tk.Listbox(
		win_speciesbrowser,
		selectmode=tk.EXTENDED,
		exportselection=0,
		height=30,
		width=42)
	for it,t in enumerate(trial.physgrid.times):
		thisyear = t/float(yr)
		string = (" IT=%-3s" % it)
		string += ("  YR=%3.1e" % thisyear)
		string += ("  TEMP=%s" % trial.physgrid.temperatures[it])
		string += "  DENSITY=" + ("%3.1e" % trial.physgrid.densities[it])
		iterlistbox.insert(tk.END,string)
	iterlistbox.pack(side=tk.RIGHT, fill=tk.BOTH, expand=tk.YES)
	iterlistbox.config(yscrollcommand=iterscrollbar.set)
	iterscrollbar.config(command=iterlistbox.yview)
	###
	# molecular properties lister
	def showmolecularproperties(tkevent=False):
		textbox.delete(1.0,tk.END)
		for key in sorted(dir(trial.getmol(species))):
			if key in ["de","fabunds"]:
				continue
			if key not in trial.getmol(species).__dict__:
				continue
			val = getattr(trial.getmol(species), key)
			line = ""
			line += str(key) + " => " + str(val) + "\n"
			textbox.insert(tk.END, line)
	tk.Button(
		win_speciesbrowser,
		text="Show Mol Properties",
		command=showmolecularproperties).pack(fill=tk.X, pady=5)
	###
	# view full abundance info
	def showfullabundance(tkevent=False):
		textbox.delete(1.0, tk.END)
		textbox.insert(tk.END, "\nfull abundance:\n")
		for it,t in enumerate(trial.physgrid.times):
			thisyear = t/float(yr)
			line = ""
			line += ("%s" % it) + ":\t"
			line += ("%4.3e" % thisyear) + " yr\t\t"
			line += ("%5.4e" % trial.getmol(species).fabunds[it])
			line += "\n"
			textbox.insert(tk.END, line)
	tk.Button(win_speciesbrowser,
		text="Show Full Abundance",
		command=showfullabundance).pack(fill=tk.X,pady=5)
	###
	# view peak abundance info
	def showpeakabundance(tkevent=False):
		textbox.delete(1.0,tk.END)
		toprint = "\npeak abundance info\n\n"
		fullabunds = trial.getmol(species).fabunds
		peakabund = max(fullabunds)
		peakindex = fullabunds.index(peakabund)
		toprint += ("iteration: %s\n" % peakindex)
		peakyear = trial.physgrid.times[peakindex]/float(yr)
		toprint += ("time (yr): %4.3e\n" % peakyear)
		peakdensity = trial.physgrid.densities[peakindex]
		toprint += ("density (cm^-3): %.3e\n" % peakdensity)
		peaktemp = trial.physgrid.temperatures[peakindex]
		toprint += ("temperature (K): %.3e\n" % peaktemp)
		toprint += ("frac. abund.: %.4e\n" % peakabund)
		textbox.insert(tk.END, toprint)
	tk.Button(
		win_speciesbrowser,
		text="Show Peak Abundance",
		command=showpeakabundance).pack(fill=tk.X,pady=5)
	###
	# order browser button
	def orderspeciesselected(tkevent=False):
		r"""
		Shows the order information for the selected iterations.

		TODO:
		- ?
		"""
		textbox.delete(1.0,tk.END)
		textbox.insert(tk.END, "Will now show all order info for '%s' for iterations:\n" % species)
		if scaledownbydensity:
			textbox.insert(tk.END, "Note that all rates will be scaled down (i.e. divided-by) the gas density, for matching exactly with GG08 ORDER results..\n")
		# get selected iterations
		selectediters = [int(item) for item in iterlistbox.curselection()]
		if not len(selectediters):
			#return
			#selectediters = params['log_order_times']
			iterlength = trial.physgrid.length
			selectediters = [
				1,
				int(round(iterlength*1/5.0)),
				int(round(iterlength*2/5.0)),
				int(round(iterlength*3/5.0)),
				int(round(iterlength*4/5.0)),
				iterlength-1]
		print("selected species is", species)
		print("selected iterations are", selectediters)
		# update text area with header
		for it in selectediters:
			textbox.insert(tk.END, str(it)+" ")
		textbox.insert(tk.END, "\n\n")
		textbox.insert(tk.END, "="*80+"\n\n")
		# get order info
		orderinfo = doorder(trial, log_it=selectediters, log_s=[species])
		for it in orderinfo['iterations']:
			itheader = ""
			itheader = "ORDER INFO FOR IT = " + str(it)
			itheader += ", time = " + str(int(trial.physgrid.times[it]/float(yr))) + " yr"
			itheader += ", density = " + str(int(trial.physgrid.densities[it])) + " cm-1"
			itheader += ", av = " + str(int(trial.physgrid.extinctions[it]))
			itheader += "\n"
			itheader += "fractional abundance: " + "{0:E}".format(float(trial.getmol(species).fabunds[it]))
			itheader += "\n" + "-"*10 + "\n\n"
			textbox.insert(tk.END, itheader)
			formtot = float(orderinfo['formtot'][species][it])
			desttot = float(orderinfo['desttot'][species][it])
			if scaledownbydensity:
				density = float(trial.physgrid.densities[it])
				formtot /= density
				desttot /= density
			# loop through formation reactions
			linecontainer = []
			for rxnidx,rxn in enumerate(orderinfo['formrxns'][species]):
				rate = getreactionrate(trial, it, rxn) * rxn.products.count(species)
				if scaledownbydensity: rate /= density
				ratefr = rate/formtot
				if (ratefr > params['log_order_mincontrib']):
					# establish entry for reaction
					rxnline = ""
					rxnline += "%s:   " % rxn.numTOT
					rxnline += "{:<40}".format(rxn.reaction)
					rxnline += "{0:>10E}".format(float(rate))
					rxnline += " ({0:.1%})".format(float(ratefr))
					rxnline += "\n"
					# append tuple (ratefr, line entry)
					linecontainer.append((ratefr,rxnline))
			# sort list of tuples by ratefr
			linecontainer = sorted(linecontainer, key=itemgetter(0), reverse=True)
			# loop through list and append to output
			for ratefr,rxnline in linecontainer:
				textbox.insert(tk.END, rxnline)
			# loop through destruction reactions
			linecontainer = []
			for rxnidx,rxn in enumerate(orderinfo['destrxns'][species]):
				rate = -getreactionrate(trial, it, rxn) * rxn.reactants.count(species)
				if scaledownbydensity: rate /= density
				ratefr = rate/desttot
				if (ratefr > params['log_order_mincontrib']):
					# establish entry for reaction
					rxnline = ""
					rxnline += "%s:   " % rxn.numTOT
					rxnline += "{:<63}".format(rxn.reaction)
					rxnline += "{0:>10E}".format(float(rate))
					rxnline += " ({0:.1%})".format(float(ratefr))
					rxnline += "\n"
					# append tuple (ratefr, line entry)
					linecontainer.append((ratefr, rxnline))
			# sort list of tuples by ratefr
			linecontainer = sorted(linecontainer,key=itemgetter(0),reverse=True)
			# loop through list and append to output
			for ratefr,rxnline in linecontainer:
				textbox.insert(tk.END, rxnline)
			# write sum line
			sumline = "\n" + " "*40 + "sum:    {:>10}".format(float(formtot))
			sumline += " "*6 + "{:>10}\n".format(desttot)
			textbox.insert(tk.END, sumline)
			# write net line
			netline = "\n" + " "*55 + "net:   {:>10}".format(float(formtot+desttot))
			netline += "\n\n"
			textbox.insert(tk.END, netline)
			textbox.insert(tk.END, "="*80+"\n")
	tk.Button(win_speciesbrowser,
		text="Show Order",
		command=orderspeciesselected).pack(fill=tk.X,pady=5)
	###
	# view basic connectivity graph
	def graphselected(tkevent=False):
		r"""
		Creates graphs of the species' dominant reactions for each iteration.

		TODO:
		- Create GIF across selected iterations
		- Use consistent node locations for each species
		"""
		import networkx as nx
		# get selected iterations
		selectediters = [int(item) for item in iterlistbox.curselection()]
		if not len(selectediters):
			#return
			#selectediters = params['log_order_times']
			iterlength = trial.physgrid.length
			selectediters = [
				1,
				int(round(iterlength*1/5.0)),
				int(round(iterlength*2/5.0)),
				int(round(iterlength*3/5.0)),
				int(round(iterlength*4/5.0)),
				iterlength-1]
		print("selected species and iterations are:", species, ",", selectediters)
		# get order info
		orderinfo = doorder(trial, log_it=selectediters, log_s=[species])
		print("order dict has the keys", list(orderinfo.keys()))
		### more advanced graph, based on order info with primary formation/destruction routes
		# create graph dict, which points to graphs[species][iteration]
		graphs = []
		# define cutoff for rate
		rcutoff = 0.005
		for it in orderinfo['iterations']:
			# finally define the graph itself, and initialize containers
			g = nx.MultiDiGraph()
			print("working on species", species, "and iter", it)
			# define pertinent kinetics info
			formtot = float(orderinfo['formtot'][species][it])
			desttot = float(orderinfo['desttot'][species][it])
			# loop through formation/destruction reactions and add to the graph
			for rxn in orderinfo['formrxns'][species]:
				rate = getreactionrate(trial, it, rxn) * rxn.products.count(species)
				ratefr = rate/formtot
				if (ratefr > rcutoff):
					print("\tfound rxn %s: %s, with ratefr %s" % (rxn.numTOT, rxn.reaction, ratefr))
					a_node = rxn.reaction.split('->')[0].strip()
					a_node = a_node.replace('(','').replace(')','')
					a_label = ' + '.join([trial.getmol(s).get_tex_name() for s in rxn.reactants])
					if rxn.extra_species_tex[0]:
						a_label += rxn.extra_species_tex[0]
					#g.add_node(a_node, label=a_label)
					rxn_label = ' + '.join([trial.getmol(s).get_tex_name() for s in rxn.reactants])
					if rxn.extra_species_tex[0]:
						rxn_label += rxn.extra_species_tex[0]
					rxn_label += " $\\rightarrow$ "
					rxn_label += ' + '.join([trial.getmol(s).get_tex_name() for s in rxn.products])
					if rxn.extra_species_tex[1]:
						rxn_label += rxn.extra_species_tex[1]
					g.add_node(a_node, label=rxn_label, style="opacity=0.5,text opacity=1")
					b_node = species
					b_label = trial.getmol(species).get_tex_name()
					g.add_node(b_node, label=b_label, center=True)
					label = "({0:.1%})".format(float(ratefr))
					#xlabel = ""
					#byproducts = [s for s in rxn.products if not s == species]
					#if len(byproducts):
					#	xlabel = ' + '.join(byproducts)
					#if rxn.extra_species_tex[1] is not None:
					#	xlabel += rxn.extra_species_tex[1].strip().lstrip("+ ")
					#if not xlabel == "":
					#	label += " (+ %s)" % (xlabel)
					g.add_edge(a_node, b_node, label=label, color="blue", weight=ratefr)
			for rxn in orderinfo['destrxns'][species]:
				rate = -getreactionrate(trial, it, rxn) * rxn.reactants.count(species)
				ratefr = rate/desttot
				if (ratefr > rcutoff):
					print("\tfound rxn %s: %s, with ratefr %s" % (rxn.numTOT, rxn.reaction, ratefr))
					a_node = species
					a_label = trial.getmol(species).get_tex_name()
					g.add_node(a_node, label=a_label)
					b_node = ' + '.join(rxn.products)
					b_label = ' + '.join([trial.getmol(s).get_tex_name() for s in rxn.products])
					#g.add_node(b_node, label=b_label)
					rxn_label = ' + '.join([trial.getmol(s).get_tex_name() for s in rxn.reactants])
					if rxn.extra_species_tex[0]:
						rxn_label += rxn.extra_species_tex[0]
					rxn_label += " $\\rightarrow$ "
					rxn_label += ' + '.join([trial.getmol(s).get_tex_name() for s in rxn.products])
					if rxn.extra_species_tex[1]:
						rxn_label += rxn.extra_species_tex[1]
					g.add_node(b_node, label=rxn_label, style="opacity=0.5,text opacity=1")
					label = "{0:.1%}".format(float(ratefr))
					#xlabel = ""
					#byproducts = [s for s in rxn.reactants if not s == species]
					#if len(byproducts):
					#	xlabel = ' + '.join(byproducts)
					#if rxn.extra_species_tex[0] is not None:
					#	xlabel += rxn.extra_species_tex[0].strip().lstrip("+ ")
					#if not xlabel == "":
					#	label += " (+ %s)" % (xlabel)
					g.add_edge(a_node, b_node, label=label, color="red", weight=ratefr)
			# check latex
			reload(gg_lib_formatter)
			latex = gg_lib_formatter.nx_to_latex(graph=g, layout="graphviz")
			glabel = "%.2e yr" % float(trial.physgrid.times[it]/float(yr))
			graphs.append((glabel, g))
		reload(gg_lib_plot)
		gg_lib_plot.show_graph(graph=graphs, show_plot=False, nohup_latex=True)
	tk.Button(
		win_speciesbrowser,
		text="Graph Order",
		command=graphselected).pack(fill=tk.X,pady=5)
	###
	# show window
	win_speciesbrowser.mainloop()


###
# reaction browser
def browserxns(parent, rxns):
	r"""
	Loads a window listing the matched reactions.
	"""
	trial = parent.trial
	###
	# set window properties
	win_rxnbrowser = tk.Toplevel()
	win_rxnbrowser.title("%s gui (%s): Reaction Browser" % (codename, trial.trialname))
	icon = tk.PhotoImage(file = os.path.join(os.path.dirname(__file__), 'grain.png'))
	win_rxnbrowser.tk.call('wm', 'iconphoto', win_rxnbrowser._w, icon)
	win_rxnbrowser.bind("<Escape>", partial(close_window, window=win_rxnbrowser))
	###
	# define text area where all reports are pushed to
	textboxframe = tk.Frame(win_rxnbrowser)
	textboxframe.pack(side=tk.TOP, fill=tk.BOTH, expand=tk.YES)
	textbox = tk.Text(
		textboxframe,
		height=45, width=80,
		exportselection=0)
	textbox.pack(side=tk.LEFT, fill=tk.BOTH, expand=tk.YES)
	textscrollbar = tk.Scrollbar(textboxframe)
	textscrollbar.pack(side=tk.LEFT, fill=tk.Y)
	textbox.config(yscrollcommand=textscrollbar.set)
	textscrollbar.config(command=textbox.yview)
	###
	# sort and populate textbox
	rxnlist = sorted(rxns, key=lambda x: x.numTOT)
	textbox.insert(tk.END, "="*75 + "\n")
	for r in rxnlist:
		rxnheader = ""
		rxnheader += "numTOT = %s\n" % r.numTOT
		textbox.insert(tk.END, rxnheader)
		keystoskip = ["numTOT", "ff", 'kinetics', "trial", "parent"]
		for k in sorted(dir(r)):
			if k in keystoskip:
				continue
			if k not in r.__dict__:
				continue
			v = getattr(r, k)
			line = "\t%s => " % k
			if isinstance(v, str):
				line += v + "\n"
			elif isinstance(v, list_types):
				line += "\n"
				for item in v:
					line += "\t\t" + str(item) + "\n"
			elif isinstance(v, number_types):
				line += str(v) + "\n"
			elif is_Expression(v):
				line += str(v) + "\n"
			else:
				line += str(type(v)) + "\n"
			textbox.insert(tk.END, line)
		textbox.insert(tk.END, "="*75 + "\n")
	# add reaction rate plotting routine & button
	def plotreactionrates(tkevent=False):
		r"""
		Creates a time plot for the rates of each reaction across time.
		"""
		if parent.forcepyqtvar.get()==1:
			params["plot_lib"] = "pyqtgraph"
		elif parent.switchBackToGnuplot:
			params["plot_lib"] = "gnuplot"
		import gg_lib_plot
		reload(gg_lib_plot)
		xdict = {'time (yr)': np.asarray([t/float(yr) for t in trial.physgrid.times])}
		ydict = {}
		for rxn in rxns:
			y = [getreactionrate(trial, it, rxn) for it in range(trial.physgrid.length)]
			ydict[rxn.numTOT] = np.asarray(y)
		genericplot(trial, xdict, ydict, scale='loglog')
	tk.Button(
		win_rxnbrowser,
		text='Plot Reaction Rates',
		command=plotreactionrates).pack(side=tk.TOP, fill=tk.X)

	# show window
	win_rxnbrowser.mainloop()

###
# generic text viewer
def showgeneralviewer(texttoshow, wintitle="Text Viewer", height=45, width=150):
	r"""
	Used for showing arbitrary text.
	"""
	# set window properties
	win_textviewer = tk.Toplevel()
	win_textviewer.title(wintitle)
	icon = tk.PhotoImage(file = os.path.join(os.path.dirname(__file__), 'grain.png'))
	win_textviewer.tk.call('wm', 'iconphoto', win_textviewer._w, icon)
	win_textviewer.bind("<Escape>", partial(close_window, window=win_textviewer))
	# determine text width
	width1 = len(texttoshow[0])
	width2 = len(texttoshow[1])
	if max(width1,width2)+1 < 150:
		width = max(width1,width2)+1
	if not all(entry[-1]=='\n' for entry in texttoshow):
		text = '\n'.join(texttoshow)
	else:
		text = ''.join(texttoshow)
	# create text box and scrollbars
	textbox = tk.Text(
		win_textviewer,
		height=height, width=width,
		exportselection=0,
		wrap=tk.NONE)
	textyscrollbar = tk.Scrollbar(win_textviewer, orient=tk.VERTICAL)
	textyscrollbar.pack(side=tk.RIGHT, fill=tk.Y)
	textxscrollbar = tk.Scrollbar(win_textviewer, orient=tk.HORIZONTAL)
	textxscrollbar.pack(side=tk.BOTTOM, fill=tk.X)
	textbox.config(xscrollcommand=textxscrollbar.set)
	textbox.config(yscrollcommand=textyscrollbar.set)
	textxscrollbar.config(command=textbox.xview)
	textyscrollbar.config(command=textbox.yview)
	# fill text box with text
	textbox.insert(tk.END, text)
	textbox.pack(fill=tk.BOTH, expand=tk.YES)

###
# close daughter objects
def close_window(event=None, window=None):
	if window is not None:
		window.destroy()

###
# expandable/collapsable frame
class toggledFrame(tk.Frame):
	def __init__(self, parent, text='', **options):
		tk.Frame.__init__(self, parent, **options)
		self.show = tk.IntVar()
		self.show.set(0)
		self.titleFrame = tk.Frame(self)
		self.titleFrame.pack(fill=tk.X, expand=1)
		tk.Label(self.titleFrame, text=text).pack(side=tk.LEFT, fill=tk.X, expand=1)
		self.toggleButton = tk.Checkbutton(
			self.titleFrame, width=2, text='+', indicatoron=False,
			command=self.toggle, variable=self.show)
		self.toggleButton.pack(side=tk.LEFT)
		self.subFrame = tk.Frame(self, relief=tk.SUNKEN, borderwidth=1)
	
	def toggle(self):
		if bool(self.show.get()):
			self.subFrame.pack(fill=tk.X, expand=1)
			self.toggleButton.configure(text='-')
		else:
			self.subFrame.forget()
			self.toggleButton.configure(text='+')

###############
# main window #
###############

class ggtkgui(tk.Tk):

	def __init__(self, trial, *args, **kwargs):
		tk.Tk.__init__(self, *args, **kwargs)
		self.trial = trial
		if params["plot_lib"] == "gnuplot":
			self.switchBackToGnuplot = True
		else:
			self.switchBackToGnuplot = False
		self.title("%s gui (%s)" % (codename, self.trial.trialname))
		self.icon = tk.PhotoImage(file = os.path.join(os.path.dirname(__file__), 'grain.png'))
		self.tk.call('wm', 'iconphoto', self._w, self.icon)
		self.bind("<Escape>", partial(close_window, window=self))
		self.setupUI()

	def setupUI(self):

		# horizontal line
		line = tk.Frame(self, height=1, bg="black")
		line.pack(fill=tk.X, side=tk.TOP)

		self.specieslisterframe = toggledFrame(self, "species lister")
		self.specieslisterframe.pack(side=tk.TOP)
		self.specieslisterfiltersframe = tk.Frame(self.specieslisterframe.subFrame)
		self.specieslisterfiltersframe.pack(side=tk.LEFT)
		self.specieslisterbuttonsframe = tk.Frame(self.specieslisterframe.subFrame)
		self.specieslisterbuttonsframe.pack(side=tk.RIGHT)

		self.nameframe = tk.Frame(self.specieslisterfiltersframe)
		self.nameframe.pack(side=tk.TOP)
		tk.Label(self.nameframe, text="   Name Contains the String(s) ").pack(side=tk.LEFT)
		self.namestring = tk.Entry(self.nameframe)
		self.namestring.config(width=7)
		self.namestring.insert(0,'S')
		self.namestring.bind("<Return>", self.showmatchingnames)
		self.namestring.pack(side=tk.LEFT)
		self.namestring2 = tk.Entry(self.nameframe)
		self.namestring2.config(width=10)
		self.namestring2.insert(0,'-Si')
		self.namestring2.bind("<Return>", self.showmatchingnames)
		self.namestring2.pack(side=tk.LEFT)
		tk.Button(
			self.nameframe,
			text='Go!',
			command=self.showmatchingnames).pack(side=tk.LEFT, padx=5, pady=5)

		self.fabundframe = tk.Frame(self.specieslisterfiltersframe)
		self.fabundframe.pack(side=tk.TOP)
		tk.Label(self.fabundframe, text="Min Peak Fractional Abundance ").pack(side=tk.LEFT)
		self.cutoff = tk.Entry(self.fabundframe)
		self.cutoff.config(width=15)
		self.cutoff.insert(0,'1e-12')
		self.cutoff.bind("<Return>", self.showpeakedfabunds)
		self.cutoff.pack(side=tk.LEFT)
		tk.Button(
			self.fabundframe,
			text='Go!',
			command=self.showpeakedfabunds).pack(side=tk.LEFT, padx=5, pady=5)

		self.combinefilterlistvar = tk.IntVar()
		self.combinefilterlistbutton = tk.Checkbutton(
			self.specieslisterbuttonsframe,
			text="Combine filters?",
			variable=self.combinefilterlistvar)
		self.combinefilterlistbutton.pack(side=tk.TOP, padx=3, pady=3)

		self.sortbypeakfabundvar = tk.IntVar()
		self.sortbypeakfabundbutton = tk.Checkbutton(
			self.specieslisterbuttonsframe,
			text="Sort by peak abund?",
			variable=self.sortbypeakfabundvar)
		self.sortbypeakfabundbutton.pack(side=tk.TOP, padx=3, pady=3)

		self.forcepyqtvar = tk.IntVar()
		self.forcepyqtbutton = tk.Checkbutton(
			self.specieslisterbuttonsframe,
			text="Force PyQt?",
			variable=self.forcepyqtvar,
			command=self.forcePyQTwasToggled)
		self.forcepyqtbutton.pack(side=tk.TOP, padx=3, pady=3)

		# horizontal line
		line = tk.Frame(self, height=1, bg="black")
		line.pack(fill=tk.X,side=tk.TOP)

		self.reactionlisterframe = toggledFrame(self, "reaction lister")
		self.reactionlisterframe.pack(side=tk.TOP)
		self.rxnnumtotframe = tk.Frame(self.reactionlisterframe.subFrame)
		self.rxnnumtotframe.pack(side=tk.TOP)
		self.rxnkeyvalframe = tk.Frame(self.reactionlisterframe.subFrame)
		self.rxnkeyvalframe.pack(side=tk.TOP)
		
		tk.Label(self.rxnnumtotframe, text="Show reactions by list/range of numTOT ").pack(side=tk.LEFT)
		self.rxnnumtot = tk.Entry(self.rxnnumtotframe)
		self.rxnnumtot.insert(0,'0330,1-12')
		self.rxnnumtot.bind("<Return>", self.showreactionsbynumtot)
		self.rxnnumtot.pack(side=tk.LEFT)
		tk.Button(
			self.rxnnumtotframe,
			text='Go!',
			command=self.showreactionsbynumtot).pack(side=tk.LEFT, padx=5, pady=5)
		
		tk.Label(self.rxnkeyvalframe, text="Get rxns by key/val matching ").pack(side=tk.LEFT)
		self.rxnkey = tk.Entry(self.rxnkeyvalframe)
		self.rxnkey.config(width=12)
		self.rxnkey.insert(0,'products')
		self.rxnkey.bind("<Return>", self.filterreactions)
		self.rxnkey.pack(side=tk.LEFT)
		self.rxnval = tk.Entry(self.rxnkeyvalframe)
		self.rxnval.config(width=15)
		self.rxnval.insert(0,'gH2')
		self.rxnval.bind("<Return>", self.filterreactions)
		self.rxnval.pack(side=tk.LEFT)
		tk.Button(
			self.rxnkeyvalframe,
			text='Go!',
			command=self.filterreactions).pack(side=tk.LEFT, padx=5, pady=5)

		# horizontal line
		line = tk.Frame(self, height=1, bg="black")
		line.pack(fill=tk.X, side=tk.TOP)

		self.altfabundframe = tk.Frame(self)
		self.altfabundframe.pack(side=tk.TOP)
		tk.Label(self.altfabundframe, text=" Alt. F.Abund. File (prev trial) ").pack(side=tk.LEFT)
		self.altfabundentry = tk.Entry(self.altfabundframe)
		self.altfabundentry.config(width=45)
		self.altfabundentry.insert(0,'')
		self.altfabundentry.bind("<Return>", self.selectaltfabunds)
		self.altfabundentry.pack(side=tk.LEFT)
		tk.Button(
			self.altfabundframe,
			text='Choose',
			command=self.selectaltfabunds).pack(side=tk.LEFT, padx=5, pady=5)

		# horizontal line
		line = tk.Frame(self, height=1, bg="black")
		line.pack(fill=tk.X, side=tk.TOP)

		self.sinkexplorerframe = toggledFrame(self, "sink explorer")
		self.sinkexplorerframe.pack(side=tk.TOP)
		self.sinkviewframe = tk.Frame(self.sinkexplorerframe.subFrame)
		self.sinkviewframe.pack(side=tk.TOP)
		self.sinkplotframe = tk.Frame(self.sinkexplorerframe.subFrame)
		self.sinkplotframe.pack(side=tk.TOP)
		self.sinkplot2frame = tk.Frame(self.sinkexplorerframe.subFrame)
		self.sinkplot2frame.pack(side=tk.TOP)
		
		tk.Label(self.sinkviewframe, text="Table of Elem Sinks (elem, iteration, numlimit, fraclimit) ").pack(side=tk.LEFT)
		self.sinkventry_elem = tk.Entry(self.sinkviewframe)
		self.sinkventry_elem.config(width=4)
		self.sinkventry_elem.insert(0,'S')
		self.sinkventry_elem.bind("<Return>", self.viewsinks)
		self.sinkventry_elem.pack(side=tk.LEFT)
		self.sinkventry_it = tk.Entry(self.sinkviewframe)
		self.sinkventry_it.config(width=5)
		self.sinkventry_it.insert(0,'1')
		self.sinkventry_it.bind("<Return>", self.viewsinks)
		self.sinkventry_it.pack(side=tk.LEFT)
		self.sinkventry_numlimit = tk.Entry(self.sinkviewframe)
		self.sinkventry_numlimit.config(width=5)
		self.sinkventry_numlimit.insert(0,'10')
		self.sinkventry_numlimit.bind("<Return>", self.viewsinks)
		self.sinkventry_numlimit.pack(side=tk.LEFT)
		self.sinkventry_fraclimit = tk.Entry(self.sinkviewframe)
		self.sinkventry_fraclimit.config(width=5)
		self.sinkventry_fraclimit.insert(0,'')
		self.sinkventry_fraclimit.bind("<Return>", self.viewsinks)
		self.sinkventry_fraclimit.pack(side=tk.LEFT)
		tk.Button(
			self.sinkviewframe,
			text='Go!',
			command=self.viewsinks).pack(side=tk.LEFT, padx=5, pady=5)

		tk.Label(self.sinkplotframe, text="Plot Elem Sinks (elem, iterations, fraclimit) ").pack(side=tk.LEFT)
		self.sinkpentry_elem = tk.Entry(self.sinkplotframe)
		self.sinkpentry_elem.config(width=3)
		self.sinkpentry_elem.insert(0,'S')
		self.sinkpentry_elem.bind("<Return>", self.plotsinks)
		self.sinkpentry_elem.pack(side=tk.LEFT)
		self.sinkpentry_it = tk.Entry(self.sinkplotframe)
		self.sinkpentry_it.config(width=20)
		self.sinkpentry_it.insert(0,'1,')
		self.sinkpentry_it.bind("<Return>", self.plotsinks)
		self.sinkpentry_it.pack(side=tk.LEFT)
		self.sinkpentry_fraclimit = tk.Entry(self.sinkplotframe)
		self.sinkpentry_fraclimit.config(width=5)
		self.sinkpentry_fraclimit.insert(0,'')
		self.sinkpentry_fraclimit.bind("<Return>", self.plotsinks)
		self.sinkpentry_fraclimit.pack(side=tk.LEFT)
		tk.Button(
			self.sinkplotframe,
			text='Frac',
			command=self.plotsinks).pack(side=tk.LEFT, padx=5, pady=5)
		tk.Button(
			self.sinkplotframe,
			text='T',
			command=partial(self.plotsinks, frac=False)).pack(side=tk.LEFT, padx=2, pady=2)

		tk.Label(self.sinkplot2frame, text="Plot Sinks #2 (elem, year) ").pack(side=tk.LEFT)
		self.sinkpentry_yr = tk.Entry(self.sinkplot2frame)
		self.sinkpentry_yr.config(width=20)
		self.sinkpentry_yr.insert(0,'')
		self.sinkpentry_yr.bind("<Return>", self.plotsinks2)
		self.sinkpentry_yr.pack(side=tk.LEFT)
		tk.Button(
			self.sinkplot2frame,
			text='Pie Chart',
			command=self.plotsinks2).pack(side=tk.LEFT, padx=5, pady=5)

		# horizontal line
		line = tk.Frame(self, height=1, bg="black")
		line.pack(fill=tk.X, side=tk.TOP)

		self.fabundsviewerframe = toggledFrame(self, "frac. abunds")
		self.fabundsviewerframe.pack(side=tk.TOP)
		self.viewabundsframe = tk.Frame(self.fabundsviewerframe.subFrame)
		self.viewabundsframe.pack(side=tk.TOP)
		self.saveabundsfnameframe = tk.Frame(self.fabundsviewerframe.subFrame)
		self.saveabundsfnameframe.pack(side=tk.TOP)
		self.saveabundsdescframe = tk.Frame(self.fabundsviewerframe.subFrame)
		self.saveabundsdescframe.pack(side=tk.TOP)
		
		tk.Label(self.viewabundsframe, text="View all F.Abunds. (iteration, year) ").pack(side=tk.LEFT)
		self.abunditstring = tk.Entry(self.viewabundsframe)
		self.abunditstring.config(width=5)
		self.abunditstring.insert(0,'')
		self.abunditstring.bind("<Return>", self.viewabunds)
		self.abunditstring.pack(side=tk.LEFT)
		self.abundtstring = tk.Entry(self.viewabundsframe)
		self.abundtstring.config(width=7)
		self.abundtstring.insert(0,'1e5')
		self.abundtstring.bind("<Return>", self.viewabunds)
		self.abundtstring.pack(side=tk.LEFT)
		tk.Button(
			self.viewabundsframe,
			text='View',
			command=self.viewabunds).pack(side=tk.LEFT, padx=5, pady=5)

		tk.Label(self.saveabundsfnameframe, text="Filename for saving ").pack(side=tk.LEFT)
		self.abundfilestring = tk.Entry(self.saveabundsfnameframe)
		self.abundfilestring.config(width=30)
		self.abundfilestring.insert(0, 'ggo_tail_dummy.d')
		self.abundfilestring.bind("<Return>", self.saveabunds)
		self.abundfilestring.pack(side=tk.LEFT)
		tk.Button(
			self.saveabundsfnameframe,
			text='Choose',
			command=self.selectfabundssavefile).pack(side=tk.LEFT, padx=5, pady=5)
		tk.Button(
			self.saveabundsfnameframe,
			text='Save',
			command=self.saveabunds).pack(side=tk.LEFT)
		
		tk.Label(self.saveabundsdescframe, text="(optional) description").pack(side=tk.LEFT)
		self.abunddescstring = tk.Entry(self.saveabundsdescframe)
		self.abunddescstring.config(width=47)
		self.abunddescstring.insert(0, '')
		self.abunddescstring.bind("<Return>", self.saveabunds)
		self.abunddescstring.pack(side=tk.LEFT, pady=5)

		# horizontal line
		line = tk.Frame(self, height=1, bg="black")
		line.pack(fill=tk.X, side=tk.TOP)

		self.infoframe = tk.Frame(self)
		self.infoframe.pack(side=tk.TOP)
		tk.Button(
			self.infoframe,
			text='View Iteration Info',
			command=self.showiterinfo).pack(side=tk.LEFT, padx=10, pady=10)
		tk.Button(
			self.infoframe,
			text='Iter vs Time',
			command=self.plotitervstime).pack(side=tk.LEFT, padx=10, pady=10)
		tk.Button(
			self.infoframe,
			text='Time vs Temp/Density',
			command=self.plottimevsdt).pack(side=tk.LEFT, padx=10, pady=10)

		# horizontal line
		line = tk.Frame(self, height=1, bg="black")
		line.pack(fill=tk.X, side=tk.TOP)

		self.plotoptionsframe = toggledFrame(self, "plot options")
		self.plotoptionsframe.pack(side=tk.TOP)
		self.saveframe = tk.Frame(self.plotoptionsframe.subFrame)
		self.saveframe.pack(side=tk.TOP)
		self.plotrangeframe = tk.Frame(self.plotoptionsframe.subFrame)
		self.plotrangeframe.pack(side=tk.TOP)
		self.plotkeywordframe = tk.Frame(self.plotoptionsframe.subFrame)
		self.plotkeywordframe.pack(side=tk.TOP)
		
		tk.Label(self.saveframe, text="Size of plot in px (width, height)").pack(side=tk.LEFT)
		self.widthstring = tk.Entry(self.saveframe)
		self.widthstring.config(width=7)
		self.widthstring.pack(side=tk.LEFT)
		self.heightstring = tk.Entry(self.saveframe)
		self.heightstring.config(width=7)
		self.heightstring.pack(side=tk.LEFT)
		line = tk.Frame(self.saveframe, height=1, bg="black")
		line.pack(fill=tk.Y, side=tk.LEFT, padx=3)
		label = tk.Label(self.saveframe, text="Save..")
		label.pack(side=tk.LEFT, padx=3, pady=3)
		self.saveplotvar = tk.IntVar()
		self.saveplotbutton = tk.Checkbutton(
			self.saveframe,
			text="plot?",
			variable=self.saveplotvar)
		self.saveplotbutton.pack(side=tk.TOP, padx=3, pady=3)
		self.savehistoryvar = tk.IntVar()
		self.savehistorybutton = tk.Checkbutton(
			self.saveframe,
			text="history?",
			variable=self.savehistoryvar)
		self.savehistorybutton.pack(side=tk.TOP, padx=3, pady=3)

		tk.Label(self.plotrangeframe, text="X Range (start, stop)").pack(side=tk.LEFT)
		self.xstartstring = tk.Entry(self.plotrangeframe)
		self.xstartstring.config(width=6)
		self.xstartstring.pack(side=tk.LEFT, pady=5)
		self.xstopstring = tk.Entry(self.plotrangeframe)
		self.xstopstring.config(width=6)
		self.xstopstring.pack(side=tk.LEFT)
		line = tk.Frame(self.plotrangeframe, height=1, bg="black")
		line.pack(fill=tk.Y, side=tk.LEFT, padx=3)
		self.ystartstring = tk.Entry(self.plotrangeframe)
		self.ystartstring.config(width=6)
		self.ystartstring.pack(side=tk.LEFT)
		self.ystopstring = tk.Entry(self.plotrangeframe)
		self.ystopstring.config(width=6)
		self.ystopstring.pack(side=tk.LEFT)
		tk.Label(self.plotrangeframe, text="Y Range (start, stop)").pack(side=tk.LEFT)
		
		tk.Label(self.plotkeywordframe, text="keywords").pack(side=tk.LEFT)
		self.plotkeywordstring = tk.Entry(self.plotkeywordframe)
		self.plotkeywordstring.config(width=45)
		self.plotkeywordstring.pack(side=tk.LEFT, pady=5)
		

		# horizontal line
		line = tk.Frame(self, height=1, bg="black")
		line.pack(fill=tk.X, side=tk.TOP)

		self.comparetwoframe = tk.Frame(self)
		self.comparetwoframe.pack(side=tk.TOP)
		tk.Label(self.comparetwoframe, text="Plot one species as fraction of the other (numerator, denominator) ").pack(side=tk.LEFT)
		self.compNum = tk.Entry(self.comparetwoframe)
		self.compNum.config(width=15)
		self.compNum.insert(0,'gH2S')
		self.compNum.bind("<Return>", self.comparetwo)
		self.compNum.pack(side=tk.LEFT)
		self.compDenom = tk.Entry(self.comparetwoframe)
		self.compDenom.config(width=8)
		self.compDenom.insert(0,'gH2O')
		self.compDenom.bind("<Return>", self.comparetwo)
		self.compDenom.pack(side=tk.LEFT)
		tk.Button(
			self.comparetwoframe,
			text='Go!',
			command=self.comparetwo).pack(side=tk.LEFT, padx=5, pady=5)

		# horizontal line
		line = tk.Frame(self, height=1, bg="black")
		line.pack(fill=tk.X, side=tk.TOP)

		self.sulfurplotsframe = toggledFrame(self, "sulfur plots")
		self.sulfurplotsframe.pack(side=tk.TOP)
		self.sulfurframe = tk.Frame(self.sulfurplotsframe.subFrame)
		self.sulfurframe.pack(side=tk.TOP)
		self.sulfurframe2 = tk.Frame(self.sulfurplotsframe.subFrame)
		self.sulfurframe2.pack(side=tk.TOP)
		self.sulfurframe3 = tk.Frame(self.sulfurplotsframe.subFrame)
		self.sulfurframe3.pack(side=tk.TOP)
		
		tk.Button(
			self.sulfurframe,
			text='Plot S: triatomics',
			command=self.plotsulfur_triatomicsgasandice).pack(side=tk.LEFT, padx=10, pady=10)
		tk.Button(
			self.sulfurframe,
			text='Plot S: HCNS fam',
			command=self.plotsulfur_HCNS).pack(side=tk.LEFT, padx=10, pady=10)
		tk.Button(
			self.sulfurframe,
			text='Plot S: CH3SH vs H2S',
			command=self.plotsulfur_CH3SH).pack(side=tk.LEFT, padx=10, pady=10)
		
		tk.Button(
			self.sulfurframe2,
			text='Plot S (IRAS 16293)',
			command=self.plotsulfur_md2016).pack(side=tk.LEFT, padx=10, pady=10)
		tk.Button(
			self.sulfurframe2,
			text="Plot S (L1544)",
			command=self.plotsulfur_silvia).pack(side=tk.LEFT, padx=10, pady=10)
		tk.Button(
			self.sulfurframe2,
			text="Plot S (L183)",
			command=self.plotsulfur_vale).pack(side=tk.LEFT, padx=10, pady=10)
		
		tk.Button(
			self.sulfurframe3,
			text='Plot S (diffuse)',
			command=self.plotsulfur_diffuse).pack(side=tk.LEFT, padx=10, pady=10)
		tk.Button(
			self.sulfurframe3,
			text='Plot S (transl.)',
			command=self.plotsulfur_translucent).pack(side=tk.LEFT, padx=10, pady=10)
		tk.Button(
			self.sulfurframe3,
			text='Plot S (dense)',
			command=self.plotsulfur_dense).pack(side=tk.LEFT, padx=10, pady=10)
		
		# horizontal line
		line = tk.Frame(self, height=1, bg="black")
		line.pack(fill=tk.X, side=tk.TOP)

	# Name Contains the String..
	def getmatchingnames(self, listtocheck=[]):
		r"""
		Returns all species which contain the literal string.
		"""
		if len(listtocheck) == 0:
			listtocheck = self.trial.molecules.get_names()
		nametomatch = self.namestring.get()
		speciesfound = []
		for s in listtocheck:
			if  nametomatch in s:
				speciesfound.append(s)
		nametomatch2 = self.namestring2.get()
		if not nametomatch2=="":
			templist = list(speciesfound)
			speciesfound = []
			nametomatch2 = nametomatch2.split(' ')
			exclusions = []
			inclusions = []
			for s in nametomatch2:
				if s[0] == "-":
					exclusions.append(s[1:])
				else:
					inclusions.append(s)
			for s in templist:
				exclusionPassed = not len(exclusions) or all(not e in s for e in exclusions)
				inclusionPassed = not len(inclusions) or all(i in s for i in inclusions)
				if exclusionPassed and inclusionPassed:
					speciesfound.append(s)
		if params['verbosity'] > 2:
			print("species containing the string %s are:" % nametomatch)
			for m in speciesfound:
				print("\t%s" % m.name)
		return speciesfound
	def showmatchingnames(self, tkevent=False):
		r"""
		Shows all species returned by getmatchingnames().
		"""
		speciesfound = self.getmatchingnames()
		if self.combinefilterlistvar.get() == 1:
			speciesfound = self.getpeakedfabunds(speciesfound)
		showspecieslist(self, speciesfound)
	
	def forcePyQTwasToggled(self, tkevent=False):
		pass
		#print "'Force PyQT' was toggled..."

	# Min Peak Fractional Abundance
	def getpeakedfabunds(self, listtocheck=[]):
		r"""
		Returns all species which have a peak abundance above the defined threshold.
		"""
		if len(listtocheck)==0:
			listtocheck = self.trial.molecules.get_names()
		minfabund = self.cutoff.get()
		speciesfound = []
		for s in listtocheck:
			if max(self.trial.getmol(s).fabunds) > float(minfabund):
				speciesfound.append(s)
		if params['verbosity'] > 2:
			print("species that peak at fabund > %s are:" % minfabund)
			for m in speciesfound:
				print("\t%s" % m.name)
		return speciesfound
	def showpeakedfabunds(self, tkevent=False):
		r"""
		Shows list of species filtered by getpeakedfabunds().
		"""
		speciesfound = self.getpeakedfabunds()
		if self.combinefilterlistvar.get()==1:
			speciesfound = self.getmatchingnames(speciesfound)
		showspecieslist(self, speciesfound)

	# reaction explorer (list/range)
	def showreactionsbynumtot(self, tkevent=False):
		r"""
		Shows all reactions which match the desired numTOT.
		"""
		numtots = self.rxnnumtot.get().split(",")
		# process numbers
		numlist = []
		for num in numtots:
			if len(num)==0:
				continue
			elif re.match('^\d{,4}-\d{,4}$', num):
				start,end = num.split("-")
				start = int(start)
				end = int(end)
				if (start < 1) or (end > 9999):
					raise ValueError("your desired value(s) must be integers in the range [1..10000]")
				else:
					for i in range(start, end+1):
						numlist.append(str(i).zfill(4))
			elif isinstance(num, str) and (len(num) <= 4):
				if int(num) < 1e4:
					numlist.append(str(num).zfill(4))
			else:
				raise ValueError("your desired value(s) must be integers in the range [1..10000]")
		if numlist == ['0000'] or len(numlist) == 0:
			raise ValueError("you didn't input any values of numTOT!")
		# populate list of reactions
		rxnlist = []
		for rxn in self.trial.network.reactions:
			if rxn.numTOT in numlist: rxnlist.append(rxn)
		if params['verbosity'] > 2:
			print("found reactions matching the requested numTOTs:")
			for r in rxnlist:
				print("\t%s" % r.reaction)
		browserxns(self, rxnlist)

	# reaction explorer (filter via getallreactions())
	def filterreactions(self, tkevent=False):
		r"""
		Shows all reactions which have the requested property.
		"""
		keystomatch = self.rxnkey.get()
		valstomatch = self.rxnval.get().replace("+", "p")
		if (valstomatch[0] == "[") and (valstomatch[-1] == "]"):
			raise NotImplementedError("not ready, but this should be a simple fix..")
		rxnlist = self.trial.network.filter(keystomatch, valstomatch)
		if params['verbosity'] > 2:
			print("found reactions matching the requested properties:")
			for r in rxnlist:
				print("\t%s" % r.reaction)
		browserxns(self, rxnlist)

	# alternative fabund chooser
	def selectaltfabunds(self, tkevent=False):
		r"""
		Lets one choose a *.obj file containing abundances from a previous trial.
		"""
		if sys.version_info[0] == 3:
			import tkinter.filedialog
			altfabundfname = tkinter.filedialog.askopenfilename()
		else:
			import tkFileDialog
			altfabundfname = tkFileDialog.askopenfilename()
		self.altfabundentry.delete(0, tk.END)
		self.altfabundentry.insert(0, altfabundfname)

	# view elemental sinks
	def viewsinks(self, tkevent=False):
		r"""
		Shows a table listing the elemental sinks for a given element.
		"""
		elem = self.sinkventry_elem.get()
		if not elem:
			print("Error: you didn't input an element to view")
			return
		it = self.sinkventry_it.get()
		if not it=='':
			it = int(it)
		else:
			print("warning: you didn't select an iteration, and will thus use the final one")
			it = len(self.trial.physgrid.times[-1])
		numlimit = self.sinkventry_numlimit.get()
		if not numlimit=='':
			numlimit = int(numlimit)
		else:
			numlimit = None
		fraclimit = self.sinkventry_fraclimit.get()
		if not fraclimit=='':
			fraclimit = float(fraclimit)
		else:
			fraclimit = None
		sink_text = getelementalsinks(
			self.trial, elem, it,
			numlimit=numlimit,
			fraclimit=fraclimit,
			getresult='text')
		wintitle = "%s gui (%s): Elem. Sinks for %s at it=%s" % (codename, self.trial.trialname, elem, it)
		height=20
		if (numlimit >= 10) and (numlimit <= 30):
			height=numlimit+1
		showgeneralviewer(sink_text, wintitle=wintitle, height=height)

	# plot elemental sinks
	def plotsinks(self, tkevent=False, frac=True):
		r"""
		Creates a stacked histogram plot of the elemental sinks for a given element.
		"""
		### for testing new features
		reload(gg_lib_plot)
		useFilledCurves = True
		useMultiplot = False
		useLinkedX2 = False
		useX2tics = False
		plotoptions = {}
		if "=" in self.plotkeywordstring.get():
			for opt in self.plotkeywordstring.get().split():
				k,v = opt.split("=")
				plotoptions[k] = v
		# make sure gnuplot is enabled, and then initialize it and the tempfile
		if not params['plot_lib']:
			print("Error: sorry, but a plot of elemental sinks is not available without Gnuplot")
			return
		if params['plot_lib']=="pyqtgraph":
			print("WARNING: this is going to use gnuplot anyway, until a version with pyqtgraph is implemented")
		import Gnuplot
		Gnuplot.GnuplotOpts.gnuplot_command = '/usr/bin/gnuplot --persist'
		#gplot = Gnuplot.Gnuplot()
		gplot = GnuplotPlus()
		width = self.widthstring.get()
		if not len(width):
			width = 640
		height = self.heightstring.get()
		if not len(height):
			height = 480
		gplot('set term wxt size %s,%s' % (width,height))
		f = tempfile.NamedTemporaryFile(mode='w', delete=False)
		filename = f.name
		if params['verbosity'] > 1: print("sink plot data file is %s" % filename)
		# assimilate all the requested data
		elem = self.sinkpentry_elem.get()
		if not elem:
			print("Error: you didn't input an element to view")
			return
		itentry = self.sinkpentry_it.get().split(",")
		itlist = []
		if self.sinkpentry_it.get() == '':
			print("warning: you didn't select an iteration, and will plot five evenly-spaced ones:")
			iterlength = self.trial.physgrid.length
			itlist = [
				1,
				int(round(iterlength*1/5.0)),
				int(round(iterlength*2/5.0)),
				int(round(iterlength*3/5.0)),
				int(round(iterlength*4/5.0)),
				iterlength-1]
		elif (len(itentry) == 1) and (not '-' in itentry[0]):
			iterlength = self.trial.physgrid.length
			itlist = [1]
			numIntermediates = int(itentry[0]) + 1
			for i in range(1, numIntermediates):
				newIt = int(round(iterlength * i/float(numIntermediates))) - 1
				if newIt == 0:
					continue
				if newIt == itlist[-1]:
					continue
				if itlist[-1] == iterlength-1:
					continue
				itlist += [newIt]
			itlist += [iterlength-1]
			print(itlist)
		else:
			for it in itentry:
				if len(it)==0:
					continue
				elif re.match('^\d+-\d+$',it):
					start,end = it.split("-")
					start = int(start)
					end = int(end)
					if start>end:
						print("warning: a reversed range does not work..")
						continue
					for k in range(start,end+1):
						if k > len(self.trial.physgrid.times):
							print("warning: you seem to have requested an iteration number that exceeds the range of the currently-loaded trial (", k,")")
						else:
							itlist.append(k)
				elif isinstance(it, str) and (int(it) <= len(self.trial.physgrid.times)):
					itlist.append(int(it))
				else:
					print("warning: you seem to have requested an iteration number that exceeds the range of the currently-loaded trial (", i,")")
		fraclimit = self.sinkpentry_fraclimit.get()
		if not fraclimit=='':
			fraclimit = float(fraclimit)
		else:
			fraclimit = 0.01	# sets default limit to 1%
		sink_species = []
		sink_dict = {}
		for it in itlist:
			species,sink_dict[it] = getelementalsinks(
				self.trial, elem, it,
				numlimit=None,
				fraclimit=None,
				getresult='dict')
			for s in species:
				if sink_dict[it][s]['ftot'] > fraclimit:
					if (not s in sink_species): sink_species.append(s)
				else:
					break
		# set plot options
		title = ('set title "Elemental Sinks of %s"' % elem)
		gplot(title)
		gplot('set key outside')
		if useMultiplot: # a test for adding a floating time axis
			gplot('set multiplot')
			gplot('set size 1,0.8')
			gplot('set origin 0,0.2')
		if frac and useFilledCurves:
			gplot('set format x "10^{%T}"')
			gplot('set xlabel "Time (yr)"')
		else:
			gplot('set xlabel "Iteration Number"')
		gplot('set xtics nomirror rotate by -60 scale 1 font ",6"')
		if not frac:
			gplot('set yrange [0:]')
			gplot('set ylabel "total fractional abundance"')
		else:
			gplot('set yrange [0:100]')
			gplot('set ylabel "% of total"')
		gplot('set grid y')
		gplot('set border 3')
		if useFilledCurves:
			gplot('set logscale x')
		else:
			gplot('set style data histograms')
			gplot('set style histogram rowstacked')
			gplot('set style fill solid border -1')
			gplot('set boxwidth 1')
		if "palette" in list(plotoptions.keys()):
			colormap = gg_lib_plot.getColormap(palette=plotoptions["palette"], format="html", num=len(sink_species))
		else:
			colormap = getColormap(palette="nipy_spectral", format="html", num=len(sink_species))
		# set up all the lines to write to file
		plotlines = []
		headerline = "iteration\t"
		for s in sink_species:
			namestring = self.trial.getmol(s).get_enhanced_name()
			headerline += '"%s"\t' % namestring
		plotlines.append(headerline)
		x2tics = []
		for it in itlist:
			plotline = str(it)+"\t"
			if useX2tics and (itlist.index(it) % 2 == 0):
				x2tics.append('"%.3e"' % (self.trial.physgrid.times[it]/float(yr)))
				#x2tics.append('%.2e' % (self.trial.physgrid.times[it-1]/yr))
			for s in sink_species:
				if not frac:
					plotline += "%.7e\t" % sink_dict[it][s]['fabund']
				else:
					plotline += "%.7e\t" % sink_dict[it][s]['ftot']
			plotline += "%.1f" % float(self.trial.physgrid.times[it]/float(yr))
			plotlines.append(plotline)
		# write the info to file
		for l in plotlines:
			f.write('%s\n' % l)
		f.close()
		if useLinkedX2:
			# define a linked x2
			gplot('times = "%s"' % " ".join(x2tics))
			gplot('gettimes(n) = word(times, n)')
			gplot('reversedtimes(w) = sum [i=1:words(times)] (word(times,i) eq w) ? i : 0')
			gplot('set link x2 via gettimes(x) inverse reversedtimes(x)')
		if useX2tics:
			# define the second axis' tics
			x2tics = ', '.join(x2tics)
			gplot('set x2tics (%s)' % x2tics)
			gplot('set x2tics rotate by -60 right scale 1 font ",8"')
		# build the final plot command
		plotcommand = ""
		if not frac:
			finalcolumn = len(sink_species)+1
			plotcommand = 'plot for [i=2:%s] "%s" u (column(i)) t column(i)' % (finalcolumn, filename)
		else:
			finalcolumn = len(sink_species)+1
			if useFilledCurves:
				plotcommand = "plot "
				for i in reversed(list(range(2,finalcolumn+1))):
					plotcommand += '\\\n "%s" u' % (filename)
					plotcommand += ' %s:(100.*sum [col=2:%g] column(col)) t column(%g)' % (finalcolumn+1, i, i)
					plotcommand += ' w filledcurves x1 lc rgb "%s", ' % colormap[i-2]
				plotcommand = plotcommand[:-2]
			else:
				plotcommand = 'plot for [i=2:%s] "%s" u (100.*column(i)) t column(i)' % (finalcolumn, filename)
		# now try the plot
		gplot(plotcommand)
		if bool(self.saveplotvar.get()):
			f2 = tempfile.NamedTemporaryFile(mode='w', delete=False)
			filename2 = f2.name
			print("saving plot to %s" % filename2)
			#termstring = 'set terminal postscript eps size %s,%s' % (int(width)/100.0,int(height)/100.0)
			#termstring += "font 'Helvetica,20' enhanced color linewidth 2"
			termstring = 'set terminal pdfcairo size %s,%s' % (int(width)/100.0,int(height)/100.0)
			termstring += " font 'Helvetica,14' enhanced color linewidth 2"
			gplot(termstring)
			gplot('set output "%s"' % filename2)
			gplot('set xtics nomirror rotate by -60 scale 1 font ",14"')
			gplot(plotcommand)
			gplot('unset output')
		if bool(self.savehistoryvar.get()):
			f4 = tempfile.NamedTemporaryFile(mode='w', delete=False)
			print("saving history to %s" % f4.name)
			for c in gplot.history:
				f4.write("%s\n" % c)
				if params['verbosity'] > 2: print(c)
			f4.close()
		if useMultiplot:
			gplot('set size 1,0.15')
			gplot('set origin 0,0')
			gplot('set xtics nomirror rotate by -60 scale 1 font ",6"')
			gplot('unset title')
			gplot('unset x2tics')
			gplot('plot "%s" using ($%g):($%g) with lines' % (filename, finalcolumn+1, finalcolumn+2))
			gplot('unset multiplot')
	def plotsinks2(self, tkevent=False):
		r"""
		Creates a pie chart of the elemental sinks for a given element, at a specific time.

		Note: much of this is simply duplicated/trimmed from self.plotsinks()..
		"""
		# make sure gnuplot is enabled, and then initialize it and the tempfile
		if not params['plot_lib']:
			print("Error: sorry, but a plot of elemental sinks is not available without Gnuplot")
			return
		if params['plot_lib']=="pyqtgraph":
			print("WARNING: this is going to use gnuplot anyway, until a version with pyqtgraph is implemented")
		import Gnuplot
		Gnuplot.GnuplotOpts.gnuplot_command = '/usr/bin/gnuplot --persist'
		#gplot = Gnuplot.Gnuplot()
		gplot = GnuplotPlus()
		gplot('set term wxt')
		if params['verbosity'] > 1: print("sink plot data file is %s" % filename)
		# assimilate all the requested data
		elem = self.sinkpentry_elem.get()
		if not elem:
			print("Error: you didn't input an element to view")
			return
		t = self.sinkpentry_yr.get()
		if not t=='':
			t = float(t)
		else:
			print("Error: you didn't input a time (yr) to view")
			return
		fraclimit = self.sinkpentry_fraclimit.get()
		if not fraclimit=='':
			fraclimit = float(fraclimit)
		else:
			fraclimit = 0.01	# sets default limit to 1%
		sink_species = []
		sink_dict = {}
		species,sink_dict = getelementalsinks(
			self.trial, elem, t=t,
			numlimit=None,
			fraclimit=None,
			getresult='dict')
		for s in species:
			if sink_dict[s]['ftot'] > fraclimit:
				if (not s in sink_species): sink_species.append(s)
			else:
				break
		#colormap = getColormap(palette="Dark2", format="html", num=len(sink_species))
		colormap = getColormap(lib="cmocean", palette="thermal", format="html", num=len(sink_species))
		#colormap = getColormap(lib="cmocean", palette="ice", format="html", num=len(sink_species))
		# set plot options
		title = ('set title "Elemental Sinks of %s"' % elem)
		gplot(title)
		# for circle & legend positions
		gplot('centerX=0.35')
		gplot('centerY=0.5')
		gplot('radius=0.25')
		gplot('yposmin = -0.5*radius')
		gplot('yposmax = 1.25*radius')
		gplot('xpos = 1.5*radius')
		gplot('ypos(i) = yposmax - i*(yposmax-yposmin)/(%.1f)' % len(sink_species))
		# modify ranges for room for legend
		gplot('set size ratio -1')
		gplot('set style fill solid 1.0 border -1')
		gplot('set angle degrees')
		gplot('set xrange [-radius:2*radius]')
		gplot('set yrange [-radius:radius]')
		gplot('unset border')
		gplot('unset tics')
		gplot('unset key')
		curTot = 0
		for i,s in enumerate(reversed(sink_species)):
			ftot = sink_dict[s]['ftot']
			# add arc
			cmd = "set object %s circle at screen centerX,centerY size screen radius" % (i+1)
			cmd += ' arc [%.1f : %.1f] fillcolor rgb "%s"' % (curTot, curTot+ftot*360, colormap[i])
			gplot(cmd)
			# add label
			gplot('set label %d center "%s - %.1f%%" at xpos,ypos(%d) tc rgb "%s"' % (i+1, s, ftot*100, i+1, colormap[i]))
			# increment the running total
			curTot += ftot*360
		gplot('plot 2 with lines lc rgb "white"')
		if bool(self.saveplotvar.get()):
			print("WARNING: the pie chart cannot be directly saved to file! try saving history..")
		if bool(self.savehistoryvar.get()):
			f4 = tempfile.NamedTemporaryFile(mode='w', delete=False)
			print("saving history to %s" % f4.name)
			for c in gplot.history:
				f4.write("%s\n" % c)
				if params['verbosity'] > 2: print(c)
			f4.close()

	# view all fractional abundances
	def viewabunds(self, tkevent=False):
		r"""
		Simply processes gui'd entries for running writeabunds() and viewing the full abundances.
		"""
		it = self.abunditstring.get()
		if it: it=int(it)
		t = self.abundtstring.get()
		if t: t=float(t)
		results = writeabunds(self.trial, it=it, t=t, fname=None)
		wintitle = "%s gui (%s): Fractional Abundances" % (codename, self.trial.trialname)
		showgeneralviewer(results, wintitle=wintitle)

	# save all fractional abundances
	def selectfabundssavefile(self, tkevent=False):
		r"""
		Provides a file selection dialog for choosing a new save file.
		"""
		if sys.version_info[0] == 3:
			import tkinter.filedialog
			fabundfname = tkinter.filedialog.asksaveasfilename()
		else:
			import tkFileDialog
			fabundfname = tkFileDialog.asksaveasfilename()
		self.abundfilestring.delete(0, tk.END)
		self.abundfilestring.insert(0, fabundfname)
	def saveabunds(self, tkevent=False):
		r"""
		Simply processes gui'd entries for running writeabunds().
		"""
		it = self.abunditstring.get()
		if it: it=int(it)
		t = self.abundtstring.get()
		if t: t=float(t)
		fname = self.abundfilestring.get()
		if not fname:
			print("error: you did not specify a file!")
			return
		descriptor = self.abunddescstring.get()
		writeabunds(self.trial, it=it, t=t, fname=fname, descriptor=descriptor)
		print("wrote fractional abundances from it=%s/t=%s to %s" % (it, t, fname))

	# view iteration info
	def showiterinfo(self, tkevent=False):
		r"""
		Shows a table listing all the physical properties of each iteration.
		"""
		texttoshow = ["iter"]
		# populate header
		for key in ["times", "temperatures", "densities", "extinctions"]:
			if key == 'times': key += " (yr)"
			texttoshow[0] += '{:^15}'.format(key)
		# add lines with values
		for it,t in enumerate(self.trial.physgrid.times):
			line = '{:>4}'.format(str(it))
			for key in ["times", "temperatures", "densities", "extinctions"]:
				val = getattr(self.trial.physgrid, key)[it]
				if key == 'times': val /= yr
				line += '{:^15.3e}'.format(float(val))
			texttoshow.append(line)
		wintitle = "%s gui (%s): Iteration Info" % (codename, self.trial.trialname)
		showgeneralviewer(texttoshow, wintitle=wintitle)

	# view iteration info
	def plotitervstime(self, tkevent=False):
		r"""
		Plots the time (yr) vs iteration number
		"""
		xdict = {}
		xarray = []
		ydict = {}
		yarray = []
		for it,t in enumerate(self.trial.physgrid.times):
			xarray.append(it)
			yarray.append(self.trial.physgrid.times[it]/float(yr))
		xdict['iteration'] = xarray
		ydict['time (yr)'] = yarray
		if params['dtditer']=="linear":
			scale = 'linear'
		else:
			scale = 'logy'
		genericgnuplot(self.trial, xdict, ydict, scale=scale)

	# view iteration info
	def plottimevsdt(self, tkevent=False):
		r"""
		Plots the density (cm^-1) and temperature (K) vs time (yr)
		"""
		xdict = {}
		ydict = {}
		ydict2 = {}
		xdict['time (yr)'] = self.trial.physgrid.times
		ydict['density (cm^{-3})'] = self.trial.physgrid.densities
		ydict2['temperature (K)'] = self.trial.physgrid.temperatures
		genericgnuplot(self.trial, xdict, ydict, ydict2=ydict2, scale='loglog')

	### special sulfur-specific routines
	def comparetwo(self, tkevent=False):
		r"""
		Creates a time plot of one species wrt another.
		"""
		if self.forcepyqtvar.get()==1:
			raise NotImplementedError("pyqtgraph cannot yet do comparisons to other species or reference values")
		import gg_lib_plot
		reload(gg_lib_plot)
		selectedspecies = [n.strip() for n in self.compNum.get().split(',')]
		refspecies = str(self.compDenom.get())
		altfabundfname = self.altfabundentry.get()
		if altfabundfname and os.path.isdir(altfabundfname):
			print("warning: tried to load an alternative set of fabunds, but entry looks like a log directory.. will try to select its fabunds file")
			altfabundfname += "/ggo_fullabund.obj"
			self.altfabundentry.delete(0, tk.END)
			self.altfabundentry.insert(0, altfabundfname)
		if altfabundfname and (not os.path.isfile(altfabundfname)):
			print("warning: tried to load an alternative set of fabunds, but", altfabundfname, "isn't a file!")
			altfabundfname = ""
		elif altfabundfname and (not altfabundfname[-3:] == "obj"):
			print("warning: the alternative set of fabunds,", altfabundfname, ", must end with the '.obj' file extension")
			altfabundfname = ""
		width = self.widthstring.get()
		if not len(width):
			width = 630
		height = self.heightstring.get()
		if not len(height):
			height = 390
		saveplot = False
		if self.saveplotvar.get() == 1:
			saveplot = "pdf"
		savehistory = bool(self.savehistoryvar.get())
		xstart = self.xstartstring.get()
		xstop = self.xstopstring.get()
		ystart = self.ystartstring.get()
		ystop = self.ystopstring.get()
		plotsolutions(
			self.trial,
			los=selectedspecies, altfabundfname=altfabundfname,
			refspecies=refspecies,
			xstart=xstart, xstop=xstop,
			ystart=ystart, ystop=ystop,
			width=width, height=height,
			saveplot=saveplot, savehistory=savehistory)
		if self.forcepyqtvar.get()==1:
			params["plot_lib"] = old_plot_lib


	def plotsulfur_triatomicsgasandice(self, tkevent=False):
		"""
		Plots the three triatomics OCS, H2S, & SO2 each as a fraction of H2O (ice) and CO (gas).
		Ref: Ferrante et al. 2008 Apj 449
		"""
		if self.forcepyqtvar.get()==1:
			raise NotImplementedError("pyqtgraph cannot yet do comparisons to other species or reference values")
		import gg_lib_plot
		reload(gg_lib_plot)
		altfabundfname = self.altfabundentry.get()
		if altfabundfname and os.path.isdir(altfabundfname):
			print("warning: tried to load an alternative set of fabunds, but entry looks like a log directory.. will try to select its fabunds file")
			altfabundfname += "/ggo_fullabund.obj"
			self.altfabundentry.delete(0, tk.END)
			self.altfabundentry.insert(0, altfabundfname)
		if altfabundfname and (not os.path.isfile(altfabundfname)):
			print("warning: tried to load an alternative set of fabunds, but '%s' isn't a file!" % altfabundfname)
			altfabundfname = ""
		elif altfabundfname and (not altfabundfname[-3:] == "obj"):
			print("warning: the alternative set of fabunds '%s' must end with the '.obj' file extension" % altfabundfname)
			altfabundfname = ""
		ocs_refs = [ # actually, mostly from Palumbo et al. 1997
			(4e-4, "W33A"),
			(1e-3, "AFGL 989"),
			(5.5e-4, "Mon R2 IRS2"),
			(8.8e-4, "AFGL 961E"),
			(5.3e-3, "AFGL 490"),
			(7.2e-4, "NGC 2024 IRS 2"),
			(1.9e-3, "OMC 2 IRS 3"),
			(1.9e-3, "Elias 16"),
			(0.05e-2, "NGC 7538 IRS 9")] # Ferrante et al. 2008
		plotsolutions(self.trial, los=["gOCS"], altfabundfname=altfabundfname, refspecies='gH2O', refvalues=ocs_refs)
		#ocs_refs = [ # all from Palumbo et al. 1997
		#	(5e-2, "W33A"),
		#	(8e-3, "AFGL 989"),
		#	(6.5e-3, "Mon R2 IRS2"),
		#	(1e-2, "AFGL 961E"),
		#	(2.6e-2, "AFGL 490"),
		#	(8e-3, "NGC 2024 IRS 2"),
		#	(1.6e-2, "OMC 2 IRS 3"),
		#	(8e-3, "Elias 16")]
		#plotsolutions(self.trial, los=["gOCS"], altfabundfname=altfabundfname, refspecies='gCO', refvalues=ocs_refs)
		h2s_refs = [
			(0.2e-2, "W33A"),
			(0.5e-2, "Mon R2 IRS2")]
		plotsolutions(self.trial, los=["gH2S"], altfabundfname=altfabundfname, refspecies='gH2O', refvalues=h2s_refs)
		so2_refs = [
			(0.31e-2, "W33A"),
			(0.8e-2, "NGC 7538 IRS 1"),
			(0.5e-2, "NGC 7538 IRS 9")]
		plotsolutions(self.trial, los=["gSO2"], altfabundfname=altfabundfname, refspecies='gH2O', refvalues=so2_refs)
		#ocs_refs = [
		#	(0.02e-2, "L1234N"),
		#	(0.5e-2, "Orion")]
		#plotsolutions(self.trial, los=["OCS"], altfabundfname=altfabundfname, refspecies='CO', refvalues=ocs_refs)
		#h2s_refs = [
		#	(0.01e-2, "L1234N"),
		#	(1.0e-2, "Orion")]
		#plotsolutions(self.trial, los=["H2S"], altfabundfname=altfabundfname, refspecies='CO', refvalues=h2s_refs)
		#so2_refs = [
		#	(0.005e-2, "L1234N"),
		#	(0.6e-2, "Orion")]
		#plotsolutions(self.trial, los=["SO2"], altfabundfname=altfabundfname, refspecies='CO', refvalues=so2_refs)
		#plotsolutions(self.trial, los=["OCS"], altfabundfname=altfabundfname, refspecies='H2O', refvalues=[(0.5e-2, "Hale-Bopp")])
		#plotsolutions(self.trial, los=["H2S"], altfabundfname=altfabundfname, refspecies='H2O', refvalues=[(1.0e-2, "Hale-Bopp")])
		#plotsolutions(self.trial, los=["SO2"], altfabundfname=altfabundfname, refspecies='H2O', refvalues=[(0.6e-2, "Hale-Bopp")])

	def plotsulfur_HCNS(self, tkevent=False):
		"""
		blah
		"""
		if self.forcepyqtvar.get()==1:
			raise NotImplementedError("pyqtgraph cannot yet do comparisons to other species or reference values")
		import gg_lib_plot
		reload(gg_lib_plot)
		altfabundfname = self.altfabundentry.get()
		if altfabundfname and os.path.isdir(altfabundfname):
			print("warning: tried to load an alternative set of fabunds, but entry looks like a log directory.. will try to select its fabunds file")
			altfabundfname += "/ggo_fullabund.obj"
			self.altfabundentry.delete(0, tk.END)
			self.altfabundentry.insert(0, altfabundfname)
		if altfabundfname and (not os.path.isfile(altfabundfname)):
			print("warning: tried to load an alternative set of fabunds, but '%s' isn't a file!" % altfabundfname)
			altfabundfname = ""
		elif altfabundfname and (not altfabundfname[-3:] == "obj"):
			print("warning: the alternative set of fabunds '%s' must end with the '.obj' file extension" % altfabundfname)
			altfabundfname = ""
		width = self.widthstring.get()
		if not len(width):
			width = 630
		height = self.heightstring.get()
		if not len(height):
			height = 390
		saveplot = False
		if self.saveplotvar.get() == 1:
			saveplot = "pdf"
		savehistory = bool(self.savehistoryvar.get())
		xstart = self.xstartstring.get()
		xstop = self.xstopstring.get()
		ystart = self.ystartstring.get()
		ystop = self.ystopstring.get()
		HCNS_refs = [ # actually, mostly from Palumbo et al. 1997
			(4.15e-12, "HNCS"),
			(3e-12, "HSCN")]
		plotsolutions(
			self.trial, los=["HNCS", "HSCN", "HCNS", "gHNCS", "gHSCN", "gHCNS"],
			altfabundfname=altfabundfname, refvalues=HCNS_refs,
			xstart=xstart, xstop=xstop,
			ystart=ystart, ystop=ystop,
			width=width, height=height,
			saveplot=saveplot, savehistory=savehistory)

	def plotsulfur_CH3SH(self, tkevent=False):
		"""
		Plots the CH3SH vs H2S and compares to Calmonte et al. 2016
		"""
		if self.forcepyqtvar.get()==1:
			raise NotImplementedError("pyqtgraph cannot yet do comparisons to other species or reference values")
		import gg_lib_plot
		reload(gg_lib_plot)
		altfabundfname = self.altfabundentry.get()
		if altfabundfname and os.path.isdir(altfabundfname):
			print("warning: tried to load an alternative set of fabunds, but entry looks like a log directory.. will try to select its fabunds file")
			altfabundfname += "/ggo_fullabund.obj"
			self.altfabundentry.delete(0, tk.END)
			self.altfabundentry.insert(0, altfabundfname)
		if altfabundfname and (not os.path.isfile(altfabundfname)):
			print("warning: tried to load an alternative set of fabunds, but '%s' isn't a file!" % altfabundfname)
			altfabundfname = ""
		elif altfabundfname and (not altfabundfname[-3:] == "obj"):
			print("warning: the alternative set of fabunds '%s' must end with the '.obj' file extension" % altfabundfname)
			altfabundfname = ""
		width = self.widthstring.get()
		if not len(width):
			width = 630
		height = self.heightstring.get()
		if not len(height):
			height = 390
		saveplot = False
		if self.saveplotvar.get() == 1:
			saveplot = "pdf"
		savehistory = bool(self.savehistoryvar.get())
		xstart = self.xstartstring.get()
		xstop = self.xstopstring.get()
		ystart = self.ystartstring.get()
		ystop = self.ystopstring.get()
		plotsolutions(
			self.trial, los=["gCH3SH"],
			altfabundfname=altfabundfname, refspecies="gH2S",
			xstart=xstart, xstop=xstop,
			ystart=ystart, ystop=ystop,
			width=width, height=height,
			saveplot=saveplot, savehistory=savehistory)

	def plotsulfur_md2016(self, tkevent=False):
		"""
		Plots a number of gas-phase species wrt H2S, and shows the reference
		ratio for IRAS 16293-2422 reported by Martin-Domenech et al. 2016.
		Ref: Calmonte et al. 2016
		"""
		if self.forcepyqtvar.get()==1:
			raise NotImplementedError("pyqtgraph cannot yet do comparisons to other species or reference values")
		import gg_lib_plot
		reload(gg_lib_plot)
		altfabundfname = self.altfabundentry.get()
		if altfabundfname and os.path.isdir(altfabundfname):
			print("warning: tried to load an alternative set of fabunds, but entry looks like a log directory.. will try to select its fabunds file")
			altfabundfname += "/ggo_fullabund.obj"
			self.altfabundentry.delete(0, tk.END)
			self.altfabundentry.insert(0, altfabundfname)
		if altfabundfname and (not os.path.isfile(altfabundfname)):
			print("warning: tried to load an alternative set of fabunds, but '%s' isn't a file!" % altfabundfname)
			altfabundfname = ""
		elif altfabundfname and (not altfabundfname[-3:] == "obj"):
			print("warning: the alternative set of fabunds '%s' must end with the '.obj' file extension" % altfabundfname)
			altfabundfname = ""
		width = self.widthstring.get()
		if not len(width):
			width = 630
		height = self.heightstring.get()
		if not len(height):
			height = 390
		saveplot = False
		if self.saveplotvar.get() == 1:
			saveplot = "pdf"
		savehistory = bool(self.savehistoryvar.get())
		xstart = self.xstartstring.get()
		xstop = self.xstopstring.get()
		ystart = self.ystartstring.get()
		ystop = self.ystopstring.get()
		plotsolutions(
			self.trial, los=["S2"], altfabundfname=altfabundfname,
			refspecies='H2S', refvalues=[(0.55, "M-D 2016")],
			xstart=xstart, xstop=xstop,
			ystart=ystart, ystop=ystop,
			width=width, height=height,
			saveplot=saveplot, savehistory=savehistory)
		plotsolutions(
			self.trial, los=["S2H"], altfabundfname=altfabundfname,
			refspecies='H2S', refvalues=[(2e-2, "M-D 2016")],
			xstart=xstart, xstop=xstop,
			ystart=ystart, ystop=ystop,
			width=width, height=height,
			saveplot=saveplot, savehistory=savehistory)
		plotsolutions(
			self.trial, los=["H2S2"], altfabundfname=altfabundfname,
			refspecies='H2S', refvalues=[(1.5e-2, "M-D 2016")],
			xstart=xstart, xstop=xstop,
			ystart=ystart, ystop=ystop,
			width=width, height=height,
			saveplot=saveplot, savehistory=savehistory)

	def plotsulfur_silvia(self, tkevent=False):
		"""
		Plots the species from Silvia's list of S-bearing molecules toward L1544,
		along with their abundances.

		Note that the ordering of the list of reference values is important, so
		that their colors match accordingly..
		"""
		if self.forcepyqtvar.get()==1:
			raise NotImplementedError("pyqtgraph cannot yet do comparisons to other species or reference values")
		import gg_lib_plot
		reload(gg_lib_plot)
		altfabundfname = self.altfabundentry.get()
		if altfabundfname and os.path.isdir(altfabundfname):
			print("warning: tried to load an alternative set of fabunds, but entry looks like a log directory.. will try to select its fabunds file")
			altfabundfname += "/ggo_fullabund.obj"
			self.altfabundentry.delete(0, tk.END)
			self.altfabundentry.insert(0, altfabundfname)
		if altfabundfname and (not os.path.isfile(altfabundfname)):
			print("warning: tried to load an alternative set of fabunds, but '%s' isn't a file!" % altfabundfname)
			altfabundfname = ""
		elif altfabundfname and (not altfabundfname[-3:] == "obj"):
			print("warning: the alternative set of fabunds '%s' must end with the '.obj' file extension" % altfabundfname)
			altfabundfname = ""
		width = self.widthstring.get()
		if not len(width):
			width = 630
		height = self.heightstring.get()
		if not len(height):
			height = 390
		saveplot = False
		if self.saveplotvar.get() == 1:
			saveplot = "pdf"
		savehistory = bool(self.savehistoryvar.get())
		xstart = self.xstartstring.get()
		xstop = self.xstopstring.get()
		ystart = self.ystartstring.get()
		ystop = self.ystopstring.get()
		plotsolutions(
			self.trial,
			los=["CS", "C2S", "H2CS", "OCS", "SO", "SO2", "HCS1p"],
			altfabundfname=altfabundfname,
			refvalues=[
				(1.7e-11, "CS (L1544)"),
				(4.5e-12, "C_2S (L1544)"),
				(2.8e-12, "H_2CS (L1544)"),
				(1.3e-11, "OCS (L1544)"),
				(1.7e-11, "SO (L1544)"),
				(1.15e-11, "SO_2 (L1544)"),
				(1.9e-12, "HCS^+ (L1544)")],
			xstart=xstart, xstop=xstop,
			ystart=ystart, ystop=ystop,
			width=width, height=height,
			saveplot=saveplot, savehistory=savehistory)

	def plotsulfur_vale(self, tkevent=False):
		"""
		Plots the species from Valerio's list of S-bearing molecules toward L183,
		along with their abundances.

		Note that the ordering of the list of reference values is important, so
		that their colors match accordingly..
		"""
		if self.forcepyqtvar.get()==1:
			raise NotImplementedError("pyqtgraph cannot yet do comparisons to other species or reference values")
		import gg_lib_plot
		reload(gg_lib_plot)
		altfabundfname = self.altfabundentry.get()
		if altfabundfname and os.path.isdir(altfabundfname):
			print("warning: tried to load an alternative set of fabunds, but entry looks like a log directory.. will try to select its fabunds file")
			altfabundfname += "/ggo_fullabund.obj"
			self.altfabundentry.delete(0, tk.END)
			self.altfabundentry.insert(0, altfabundfname)
		if altfabundfname and (not os.path.isfile(altfabundfname)):
			print("warning: tried to load an alternative set of fabunds, but '%s' isn't a file!" % altfabundfname)
			altfabundfname = ""
		elif altfabundfname and (not altfabundfname[-3:] == "obj"):
			print("warning: the alternative set of fabunds '%s' must end with the '.obj' file extension" % altfabundfname)
			altfabundfname = ""
		width = self.widthstring.get()
		if not len(width):
			width = 630
		height = self.heightstring.get()
		if not len(height):
			height = 390
		saveplot = False
		if self.saveplotvar.get() == 1:
			saveplot = "pdf"
		savehistory = bool(self.savehistoryvar.get())
		xstart = self.xstartstring.get()
		xstop = self.xstopstring.get()
		ystart = self.ystartstring.get()
		ystop = self.ystopstring.get()
		plotsolutions(
			self.trial,
			los=["HCS1p", "SO", "SO2", "OCS", "H2CS"],
			altfabundfname=altfabundfname,
			refvalues=[
				(2.0e-12, "HCS^+ (L183)"),
				(1.6e-9, "SO (L183)"),
				(5.1e-11, "SO_2 (L183)"),
				(1.3e-10, "OCS (L183)"),
				(2.9e-11, "H_2CS (L183)")],
			xstart=xstart, xstop=xstop,
			ystart=ystart, ystop=ystop,
			width=width, height=height,
			saveplot=saveplot, savehistory=savehistory)
	
	def plotsulfur_diffuse(self, tkevent=False):
		"""
		Plots the species from the manuscript's summary table.

		Note that the ordering of the list of reference values is important, so
		that their colors match accordingly..
		"""
		if self.forcepyqtvar.get()==1:
			raise NotImplementedError("pyqtgraph cannot yet do comparisons to other species or reference values")
		import gg_lib_plot
		reload(gg_lib_plot)
		altfabundfname = self.altfabundentry.get()
		if altfabundfname and os.path.isdir(altfabundfname):
			print("warning: tried to load an alternative set of fabunds, but entry looks like a log directory.. will try to select its fabunds file")
			altfabundfname += "/ggo_fullabund.obj"
			self.altfabundentry.delete(0, tk.END)
			self.altfabundentry.insert(0, altfabundfname)
		if altfabundfname and (not os.path.isfile(altfabundfname)):
			print("warning: tried to load an alternative set of fabunds, but '%s' isn't a file!" % altfabundfname)
			altfabundfname = ""
		elif altfabundfname and (not altfabundfname[-3:] == "obj"):
			print("warning: the alternative set of fabunds '%s' must end with the '.obj' file extension" % altfabundfname)
			altfabundfname = ""
		width = self.widthstring.get()
		if not len(width):
			width = 630
		height = self.heightstring.get()
		if not len(height):
			height = 390
		saveplot = False
		if self.saveplotvar.get() == 1:
			saveplot = "pdf"
		savehistory = bool(self.savehistoryvar.get())
		xstart = self.xstartstring.get()
		xstop = self.xstopstring.get()
		ystart = self.ystartstring.get()
		ystop = self.ystopstring.get()
		plotsolutions(
			self.trial,
			los=["HS", "HS1p", "H2S", "CS", "SO"],
			altfabundfname=altfabundfname,
			refvalues=[
				("9e-10 -- 8e-9", "HS (N15)"),
				("<4e-9", "HS^+ (N12)"),
				("3e-10 -- 6.3e-10", "H_2S (N15)"),
				("2e-10 -- 3e-9", "CS (N15)"),
				("1e-10 -- 5e-10", "SO (N15)")],
			xstart=xstart, xstop=xstop,
			ystart=ystart, ystop=ystop,
			width=width, height=height,
			saveplot=saveplot, savehistory=savehistory)
	
	def plotsulfur_translucent(self, tkevent=False):
		"""
		Plots the species from the manuscript's summary table.

		Note that the ordering of the list of reference values is important, so
		that their colors match accordingly..
		"""
		if self.forcepyqtvar.get()==1:
			raise NotImplementedError("pyqtgraph cannot yet do comparisons to other species or reference values")
		import gg_lib_plot
		reload(gg_lib_plot)
		altfabundfname = self.altfabundentry.get()
		if altfabundfname and os.path.isdir(altfabundfname):
			print("warning: tried to load an alternative set of fabunds, but entry looks like a log directory.. will try to select its fabunds file")
			altfabundfname += "/ggo_fullabund.obj"
			self.altfabundentry.delete(0, tk.END)
			self.altfabundentry.insert(0, altfabundfname)
		if altfabundfname and (not os.path.isfile(altfabundfname)):
			print("warning: tried to load an alternative set of fabunds, but '%s' isn't a file!" % altfabundfname)
			altfabundfname = ""
		elif altfabundfname and (not altfabundfname[-3:] == "obj"):
			print("warning: the alternative set of fabunds '%s' must end with the '.obj' file extension" % altfabundfname)
			altfabundfname = ""
		width = self.widthstring.get()
		if not len(width):
			width = 630
		height = self.heightstring.get()
		if not len(height):
			height = 390
		saveplot = False
		if self.saveplotvar.get() == 1:
			saveplot = "pdf"
		savehistory = bool(self.savehistoryvar.get())
		xstart = self.xstartstring.get()
		xstop = self.xstopstring.get()
		ystart = self.ystartstring.get()
		ystop = self.ystopstring.get()
		plotsolutions(
			self.trial,
			los=["H2S",
				"CS",
				"HCS1p",
				"H2CS",
				"C2S",
				"SO",
				"SO1p",
				"SO2",
				"OCS"],
			altfabundfname=altfabundfname,
			refvalues=[
				("3e-9 -- 3e-8", "H_2S (T96b)"),
				("6e-10 -- 8e-9", "CS (T96a)"),
				("8e-11 -- 4e-10", "HCS^+ (T96a)"),
				("<1e-8", "H_2CS (T96b)"),
				(1.6e-9, "C_2S (T98)"),
				("3.3e-9--3.5e-8", "SO (T95)"),
				("8e-10 -- 6e-9", "SO^+ (T96b)"),
				("<9e-9", "SO_2 (T95b)"),
				("<1.5e-9", "OCS (T96b)")],
			xstart=xstart, xstop=xstop,
			ystart=ystart, ystop=ystop,
			width=width, height=height,
			saveplot=saveplot, savehistory=savehistory)
	
	def plotsulfur_dense(self, tkevent=False):
		"""
		Plots the species from the manuscript's summary table.

		Note that the ordering of the list of reference values is important, so
		that their colors match accordingly..
		"""
		if self.forcepyqtvar.get()==1:
			raise NotImplementedError("pyqtgraph cannot yet do comparisons to other species or reference values")
		import gg_lib_plot
		reload(gg_lib_plot)
		altfabundfname = self.altfabundentry.get()
		if altfabundfname and os.path.isdir(altfabundfname):
			print("warning: tried to load an alternative set of fabunds, but entry looks like a log directory.. will try to select its fabunds file")
			altfabundfname += "/ggo_fullabund.obj"
			self.altfabundentry.delete(0, tk.END)
			self.altfabundentry.insert(0, altfabundfname)
		if altfabundfname and (not os.path.isfile(altfabundfname)):
			print("warning: tried to load an alternative set of fabunds, but '%s' isn't a file!" % altfabundfname)
			altfabundfname = ""
		elif altfabundfname and (not altfabundfname[-3:] == "obj"):
			print("warning: the alternative set of fabunds '%s' must end with the '.obj' file extension" % altfabundfname)
			altfabundfname = ""
		width = self.widthstring.get()
		if not len(width):
			width = 630
		height = self.heightstring.get()
		if not len(height):
			height = 390
		saveplot = False
		if self.saveplotvar.get() == 1:
			saveplot = "pdf"
		savehistory = bool(self.savehistoryvar.get())
		xstart = self.xstartstring.get()
		xstop = self.xstopstring.get()
		ystart = self.ystartstring.get()
		ystop = self.ystopstring.get()
		plotsolutions(
			self.trial,
			los=["H2S", "CS", "HCS1p", "H2CS", "C2S", "NS", "SO", "SO2", "OCS"],
			altfabundfname=altfabundfname,
			refvalues=[
				(7e-10, "H_2S (M89)"),
				(6.5e-9, "CS (G16)"),
				(2.5e-10, "HCS^+ (G16)"),
				(2.6e-9, "H_2CS (G16)"),
				(5e-9, "C_2S (G16)"),
				(4e-10, "NS (M94)"),
				(1e-8, "SO (L06)"),
				(1e-10, "SO_2 (C11)"),
				(1e-9, "OCS (M87)")],
			xstart=xstart, xstop=xstop,
			ystart=ystart, ystop=ystop,
			width=width, height=height,
			saveplot=saveplot, savehistory=savehistory)

###
# display main window
if (__name__ == '__main__') and ("ggtrial" in list(globals().keys())):
	win_master = ggtkgui(ggtrial)
	win_master.mainloop()


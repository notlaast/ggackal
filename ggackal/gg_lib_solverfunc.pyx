# cython: language_level=3, boundscheck=False, cdivision=True
from __main__ import ggtrial
from __main__ import params
from cytoolz import juxt
from libc.math cimport fmax
import numpy as np
cimport numpy as np

DTYPE = np.float
ctypedef np.float_t DTYPE_t

cdef float minratecoeff = 1.0e-50
cdef float val

cdef float _getffabovethreshold(de, np.ndarray[DTYPE_t, ndim=1] v):
	val = de(v)
	if abs(val) > minratecoeff:
		return val
	else:
		return 0

def func(np.ndarray[DTYPE_t, ndim=1] y, float t, float args):
	cdef int I = ggtrial.solution.desval.shape[0]
	cdef int i
	cdef double[:] desval = np.empty(I, dtype=float)
	
	#y.clip(min=params['defaultinitialabund'], max=0.5)
	y[-3] = ggtrial.physgrid.gettemp(t + args)
	y[-2] = ggtrial.physgrid.getdens(t + args)
	y[-1] = ggtrial.physgrid.getav(t + args)
	if params['minratecoeff']:
		for i in range(I):
			desval[i] = _getffabovethreshold(ggtrial.solution.des[i], y)
		return np.asarray(desval)
	else:
		for i in range(I):
			ggtrial.solution.desval[i] = ggtrial.solution.des[i](y)
		return ggtrial.solution.desval

# DESCRIPTION
#	This file should contain the initial fractional abundances
#	of all species. The reference species should be defined in
#	"gg_in_params.yml".

# highly depleted abundances for a dense core
# Ref: McElroy et al., A&A 550 (2013) - UMIST 2012
H           0.3333333334
H2          0.3333333333
He          9.0e-2 # 95% cosmic
O           3.2e-4 # 56%
C1+         1.4e-4 # 67%
N           7.5e-5 # 130%
S1+         8.0e-8 # 0.5%
Na1+        2.0e-9 # 0.1%
Si1+        8.0e-9 # 0.02%
Mg1+        7.0e-9 # 0.02%
Cl1+        4.0e-9 # 1.4%
Fe1+        3.0e-9 # 0.01%
P1+         3.0e-9 # 1.2%
F           2.0e-8 # 55%

# DESCRIPTION
#	This file should contain the initial fractional abundances
#	of all species. The reference species should be defined in
#	"gg_in_params.yml".

# listed here are values for diffuse solar abundances
# Ref: Asplund et al., Annu.Rev.Astron.Astrophys. 47 (2009)
H           0.3333333334
H2          0.3333333333
He          8.51E-02
O           4.90E-04
C1+         2.69E-04
N           6.76E-05
S1+         1.32E-05
Na1+        1.74E-06
Si1+        3.24E-05
Mg1+        3.98E-05
Cl1+        3.16E-07
Fe1+        3.16E-05
P1+         2.57E-07
F           3.63E-08

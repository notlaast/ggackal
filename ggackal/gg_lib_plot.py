#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# DESCRIPTION
#	All plotting/graphing subroutines should go here.
#
# TODO
# - move the plotsinks() function from Tk to here
# - implement a version of plotsinks() for pyqtgraph
#
# standard library
from __future__ import absolute_import
import os
import sys
import tempfile
import copy
import subprocess
import random
if sys.version_info[0] == 3:
	import pickle
else:
	import cPickle as pickle
# third-party
from scipy import sparse
import numpy as np
import matplotlib.pyplot as plt
# local
if not os.path.dirname(os.path.abspath(__file__)) in sys.path:
	sys.path.append(os.path.dirname(os.path.abspath(__file__)))
from gg_lib_sys import *
from gg_lib_physics import *
from gg_lib_formatter import *
# others (conditionally)
if params['plot_jacadjacency']:
	try:
		from PIL import Image
	except ImportError:
		msg = "Could not import PIL!"
		if sys.platform == 'darwin':
			msg += "\n\tTry doing something like 'sudo port install py-pil'\n"
			msg += "\nIf you tried this and still have trouble, see https://stackoverflow.com/a/11368029"
		else:
			msg += "\n\tTry doing something like 'sudo pip install PIL'\n"
		raise ImportError(msg)

if params['plot_lib'] == "gnuplot":
	try:
		import Gnuplot
		class GnuplotPlus(object):
			"""
			Provides a subclassed version of the gnuplot-py interface,
			except that the gnuplot process object now also keeps track
			of its call history, so that one may recall it and use it
			to, for example, save to a script for future use.
			"""
			def __init__(self, **opts):
				self.gnuplot = Gnuplot.Gnuplot(**opts)
				self.history = []
			def __call__(self, s):
				"""
				Pushes the command to history, and then calls process.
				"""
				self.history.append(s)
				self.gnuplot(s)
				
	except ImportError:
		# print warning message
		s = ""
		s += "\nwarning: You do not appear to have the gnuplot-py module installed.\n"
		s += "\tYou can install it through the python package manager via\n"
		s += "\t>sudo pip install gnuplot-py\n"
		s += "\tor (if using py3)"
		s += "\t>sudo pip install git+https://github.com/oblalex/gnuplot.py-py3k.git\n"
		s += "Will disable gnuplot for now, but you can install it for next time..\n"
		print(s)
		# then just skip it and use the built-in plotting routines
		global plot_usegnuplot
		plot_usegnuplot = False
elif params['plot_lib'] == "pyqtgraph":
	try:
		import pyqtgraph as pg
		from pyqtgraph import siScale
	except ImportError as e:
		msg = "Could not import pyqtgraph!"
		msg += "\n\tTry doing something like 'sudo pip install pyqtgraph'\n"
		raise ImportError(msg)

if sys.version_info[0] == 3:
	from importlib import reload




def getColormap(
	lib="matplotlib",
	palette="nipy_spectral",
	format="rgb",
	num=0,
	lowerLimit=0.0,
	upperLimit=1.0):
	# see https://lisacharlotterost.github.io/2016/04/22/Colors-for-DataVis/
	# do something with https://xkcd.com/color/rgb/
	def hex_to_rgbtuples(hexcolors):
		return [[ord(c) for c in h[1:].decode('hex')] for h in hexcolors]
	misc = [
		"cielab256", # https://stackoverflow.com/questions/33295120/how-to-generate-gif-256-colors-palette/33295456
		"coloralphabet26", # https://graphicdesign.stackexchange.com/a/3815
		"gilbertson", # https://graphicdesign.stackexchange.com/a/3686
		]
	if palette in misc:
		if palette == "cielab256":
			if num > 256:
				raise RuntimeError("the cielab256 palette only supports a maximum of 256 colors: %g" % num)
			hexcolors = ["#B88183", "#922329", "#5A0007", "#D7BFC2", "#D86A78", "#FF8A9A", "#3B000A",
				"#E20027", "#943A4D", "#5B4E51", "#B05B6F", "#FEB2C6", "#D83D66", "#895563", "#FF1A59",
				"#FFDBE5", "#CC0744", "#CB7E98", "#997D87", "#6A3A4C", "#FF2F80", "#6B002C", "#A74571",
				"#C6005A", "#FF5DA7", "#300018", "#B894A6", "#FF90C9", "#7C6571", "#A30059", "#DA007C",
				"#5B113C", "#402334", "#D157A0", "#DDB6D0", "#885578", "#962B75", "#A97399", "#D20096",
				"#E773CE", "#AA5199", "#E704C4", "#6B3A64", "#FFA0F2", "#6F0062", "#B903AA", "#C895C5",
				"#FF34FF", "#320033", "#DBD5DD", "#EEC3FF", "#BC23FF", "#671190", "#201625", "#F5E1FF",
				"#BC65E9", "#D790FF", "#72418F", "#4A3B53", "#9556BD", "#B4A8BD", "#7900D7", "#A079BF",
				"#958A9F", "#837393", "#64547B", "#3A2465", "#353339", "#BCB1E5", "#9F94F0", "#9695C5",
				"#0000A6", "#000035", "#636375", "#00005F", "#97979E", "#7A7BFF", "#3C3E6E", "#6367A9",
				"#494B5A", "#3B5DFF", "#C8D0F6", "#6D80BA", "#8FB0FF", "#0045D2", "#7A87A1", "#324E72",
				"#00489C", "#0060CD", "#789EC9", "#012C58", "#99ADC0", "#001325", "#DDEFFF", "#59738A",
				"#0086ED", "#75797C", "#BDC9D2", "#3E89BE", "#8CD0FF", "#0AA3F7", "#6B94AA", "#29607C",
				"#404E55", "#006FA6", "#013349", "#0AA6D8", "#658188", "#5EBCD1", "#456D75", "#0089A3",
				"#B5F4FF", "#02525F", "#1CE6FF", "#001C1E", "#203B3C", "#A3C8C9", "#00A6AA", "#00C6C8",
				"#006A66", "#518A87", "#E4FFFC", "#66E1D3", "#004D43", "#809693", "#15A08A", "#00846F",
				"#00C2A0", "#00FECF", "#78AFA1", "#02684E", "#C2FFED", "#47675D", "#00D891", "#004B28",
				"#8ADBB4", "#0CBD66", "#549E79", "#1A3A2A", "#6C8F7D", "#008941", "#63FFAC", "#1BE177",
				"#006C31", "#B5D6C3", "#3D4F44", "#4B8160", "#66796D", "#71BB8C", "#04F757", "#001E09",
				"#D2DCD5", "#00B433", "#9FB2A4", "#003109", "#A3F3AB", "#456648", "#51A058", "#83A485",
				"#7ED379", "#D1F7CE", "#A1C299", "#061203", "#1E6E00", "#5EFF03", "#55813B", "#3B9700",
				"#4FC601", "#1B4400", "#C2FF99", "#788D66", "#868E7E", "#83AB58", "#374527", "#98D058",
				"#C6DC99", "#A4E804", "#76912F", "#8BB400", "#34362D", "#4C6001", "#DFFB71", "#6A714A",
				"#222800", "#6B7900", "#3A3F00", "#BEC459", "#FEFFE6", "#A3A489", "#9FA064", "#FFFF00",
				"#61615A", "#FFFFFE", "#9B9700", "#CFCDAC", "#797868", "#575329", "#FFF69F", "#8D8546",
				"#F4D749", "#7E6405", "#1D1702", "#CCAA35", "#CCB87C", "#453C23", "#513A01", "#FFB500",
				"#A77500", "#D68E01", "#B79762", "#7A4900", "#372101", "#886F4C", "#A45B02", "#E7AB63",
				"#FAD09F", "#C0B9B2", "#938A81", "#A38469", "#D16100", "#A76F42", "#5B4534", "#5B3213",
				"#CA834E", "#FF913F", "#953F00", "#D0AC94", "#7D5A44", "#BE4700", "#FDE8DC", "#772600",
				"#A05837", "#EA8B66", "#391406", "#FF6832", "#C86240", "#29201D", "#B77B68", "#806C66",
				"#FFAA92", "#89412E", "#E83000", "#A88C85", "#F7C9BF", "#643127", "#E98176", "#7B4F4B",
				"#1E0200", "#9C6966", "#BF5650", "#BA0900", "#FF4A46", "#F4ABAA", "#000000", "#452C2C",
				"#C8A1A1"]
			random.shuffle(hexcolors) # to ensure a spread of colors...
			rgb_tuples = hex_to_rgbtuples(hexcolors)[:num]
		elif palette == "coloralphabet26":
			if num > 26:
				raise RuntimeError("the coloralphabet26 palette only supports a maximum of 26 colors: %g" % num)
			rgb_tuples = [[240,163,255],[0,117,220],[153,63,0],[76,0,92],[25,25,25],[0,92,49],
				[43,206,72],[255,204,153],[128,128,128],[148,255,181],[143,124,0],[157,204,0],
				[194,0,136],[0,51,128],[255,164,5],[255,168,187],[66,102,0],[255,0,16],[94,241,242],
				[0,153,143],[224,255,102],[116,10,255],[153,0,0],[255,255,128],[255,255,0],[255,80,5]]
			rgb_tuples = rgb_tuples[:num]
		elif palette == "gilbertson":
			if num > 24:
				raise RuntimeError("the gilbertson palette only supports a maximum of 24 colors: %g" % num)
			rgb_tuples = [[255,0,0], [228,228,0], [0,255,0], [0,255,255], [176,176,255], [255,0,255],
				[228,228,228], [176,0,0], [186,186,0], [0,176,0], [0,176,176], [132,132,255], [176,0,176],
				[186,186,186], [135,0,0], [135,135,0], [0,135,0], [0,135,135], [73,73,255], [135,0,135],
				[135,135,135], [85,0,0], [84,84,0], [0,85,0], [0,85,85], [0,0,255], [85,0,85], [84,84,84]]
			rgb_tuples = rgb_tuples[:num]
	elif lib == "matplotlib":
		cm = plt.cm
		# define palettes that contain white colors, so they can be avoided (sort of..)
		whiteTop = [
			"Greys", "Purples", "Blues", "Greens", "Oranges", "Reds",
			"YlOrBr", "YlOrRd", "OrRd", "PuRd", "RdPu", "BuPu",
			"GnBu", "PuBu", "YlGnBu", "PuBuGn", "BuGn", "YlGn",
			"gist_gray", "gray", "bone", "pink",
			"hot", "afmhot", "gist_heat",
			"ocean", "gist_earth", "terrain", "gist_stern",
			"gnuplot2", "CMRmap", "cubehelix",
			"gist_ncar"]
		whiteCenter = [
			"PiYG", "PRGn", "BrBG", "PuOr", "RdGy", "RdBu",
			"RdYlBu", "RdYlGn", "bwr"]
		whiteBottom = [
			"binary", "gist_yarg"]
		if palette in whiteTop:
			upperLimit = 0.9
		elif palette in whiteBottom:
			lowerLimit = 0.1
		elif palette in whiteCenter:
			print("WARNING:")
			print("\tyou are using a color palette (%s) that will likely yield a white color!" % palette)
			print("\tyou might want to avoid these (%s)" % whiteCenter)
			print("\tsee https://matplotlib.org/users/colormaps.html for alternatives")
		try:
			colormap = getattr(cm, "%s" % palette)
		except AttributeError:
			print("WARNING: you tried to request a color palette from matplotlib that doesn't appear to exist!")
			print("\tsee https://matplotlib.org/users/colormaps.html for alternatives")
			return getColormap(format=format, num=num)
		rgb_tuples = [colormap(i)[:-1] for i in np.linspace(lowerLimit, upperLimit, num)]
		rgb_tuples = [[int(c*255) for c in rgb] for rgb in rgb_tuples]
	elif lib == "cmocean":
		# http://matplotlib.org/cmocean/
		try:
			from cmocean import cm, tools
		except ImportError:
			print("WARNING: could not import cmocean! try 'pip install cmocean' next time.. returning the default for now..")
			return getColormap(format=format, num=num)
		try:
			colormap = getattr(cm, "%s" % palette)
		except AttributeError:
			print("WARNING: you tried to request a color palette from cmocean that doesn't appear to exist!")
			print("\tsee http://matplotlib.org/cmocean/ for alternatives")
			return getColormap(format=format, num=num)
		rgb_dict = tools.get_dict(colormap, N=num)
		rgb_tuples = []
		for i in range(num):
			rgb_tuples.append((rgb_dict["red"][i][1], rgb_dict["green"][i][1], rgb_dict["blue"][i][1]))
		rgb_tuples = [[int(c*255) for c in rgb] for rgb in rgb_tuples]
	if format == "rgb":
		return rgb_tuples
	elif format == "html":
		return ["#"+''.join(["%0.2X" % c for c in rgb]).lower() for rgb in rgb_tuples]
	else:
		return None


def plotsparsity(m):
	r"""
	Makes a plot to view "adjacency graph" of rate network's Jacobian.
	
	First the dense matrix is converted to a sparse format, and then to a binary representation, so you can visualize which entries are non-zero.
	
	INPUTS:
	- ``m`` -- a dense matrix (shape: two-dimensional array)
	"""
	if params['verbosity'] > 0: print("will show the Jacobian's adjacency")
	
	if not params['plot_lib'] == "matplotlib":
		print("warning: you selected gnuplot or pyqtgraph for plots, but using matplotlib for the sparse plotter")
	
	if params['verbosity'] > 0: print("\tconverting to sparse matrix type..")
	dokmat = sparse.dok_matrix(m)
	for key,val in list(dokmat.items()):
		if val==dummyfunc: del dokmat[key]
	coomat = dokmat.tocoo()
	
	if params['verbosity'] > 0: print("\tdefining the plot..")
	from plt import figure
	fig = figure()
	ax = fig.add_subplot(111, axisbg='black')
	ax.plot(coomat.col, coomat.row, 's', color='white', ms=3)
	ax.set_xlim(0, coomat.shape[1])
	ax.set_ylim(0, coomat.shape[0])
	ax.invert_yaxis()
	ax.set_aspect('equal')
	ax.set_xticks([])
	ax.set_yticks([])
	
	if params['verbosity'] > 0: print("\tconverting to PNG and displaying it..")
	plotfile = tempfile.NamedTemporaryFile(mode='w', delete=False, suffix='.png').name
	fig.savefig(plotfile)
	image = Image.open(plotfile)
	image.show()




def plotsolutions(
	trial,
	s="", los=[],
	altfabundfname='',
	refspecies="", refvalues=[],
	xstart="", xstop="",
	ystart="", ystop="",
	width=None, height=None,
	saveplot=False, savehistory=False):
	r"""
	Wrapper for the solution plotters, calling the appropriate plotting routine that is preferred (gnuplot vs built-in)
	
	INPUTS:
	- ``s`` -- a string of which species is to be plotted
	- ``los`` -- list of species (type: string) to be plotted
	- ``altfabundfname`` -- a string containing the name of a previous trial's saved fractional abundances
	- ``refspecies`` -- the name of an species to use for normalizing the fractional abundance (instead of density)
	- ``refvalues`` -- a list[float] of refvalues to show on the plot (values can alternatively be tuples(float,str) for controlling their names in the legend)
	"""
	if (not s) and (not los):
		raise SyntaxError("you did not appear to give a proper argument of *what* to plot!")
	
	# loads/performs a sanity check on the altfabunds file if one is provided
	altfabunds = ''
	if altfabundfname:
		try:
			altfabundshandle = open(altfabundfname, 'r')
			altfabunds = pickle.load(altfabundshandle)
		except IOError:
			print("warning: could not load the alternative fabunds from file", altfabundsfname)
			altfabunds = ""
		except UnicodeDecodeError:	# this can happen when loading an old trial from a different version of python
			if not altfabundshandle.closed:
				altfabundshandle.close()
			if sys.version_info[0] == 3:
				altfabundshandle = open(altfabundfname, 'rb')
				altfabunds = pickle.load(altfabundshandle, encoding='latin1')
		finally:
			if (not altfabunds == "") and (not isinstance(altfabunds, dict)):
				print("warning: the alternative fabunds were loaded, but they do not look like a dictionary")
				altfabunds = ""
			elif not 'times' in list(altfabunds.keys()):
				print("warning: the alternative fabunds were loaded, but they do not contain a reference in time")
				altfabunds = ""
			elif params['verbosity'] > 1:
				print("will use the provided alternative fabunds file..")
	
	if params['plot_lib'] == "gnuplot":
		return gnuplotsolutions(
			trial, s=s, los=los, altfabunds=altfabunds,
			refspecies=refspecies, refvalues=refvalues,
			xstart=xstart, xstop=xstop, ystart=ystart, ystop=ystop,
			width=width, height=height, saveplot=saveplot, savehistory=savehistory)
	elif params['plot_lib'] == "pyqtgraph":
		if savehistory is not None:
			print("WARNING: pyqtgraph does not support saving the command history!")
		return pgplotsolutions(
			trial, s=s, los=los, altfabunds=altfabunds,
			refspecies=refspecies, refvalues=refvalues,
			xstart=xstart, xstop=xstop, ystart=ystart, ystop=ystop,
			width=width, height=height, saveplot=saveplot)
	else:
		return line2dplotsolutions(trial, s=s, los=los)

def line2dplotsolutions(
	trial,
	s="", los=[]):
	r"""
	Uses the built-in library sage.plot.line.line2d to plot time evolutions.
	
	See ``plotsolutions`` documentation for more details.
	"""
	raise NotImplementedError("line2dplotsolutions still relies on sage.. switch to matplotlib or ")
	
	if altfabunds:
		raise NotImplementedError("unfortunately line2dplotsolutions() is not ready for alternative fabunds")
	
	# create plot via points (fastest built-in routine.. maybe gnuplot is better)
	from sage.plot.line import line2d
	
	if (len(los) > 0):
		# define color palette for each plot
		R = rainbow(len(los))
		i = 0
		stoR = {}
		for s in los:
			stoR[s] = R[i]
			i = i+1
		# do plots
		theplot = sum([line2d(list(zip(times, allmolprop[s]['fabunds'])), legend_label=s, color=stoR[s], scale="loglog") for s in los])
	elif s:
		# create comma-separated list of x,y points
		toplot = list(zip(times,allmolprop[s]['fabunds']))
		theplot = line2d(toplot,legend_label=s,scale="loglog")
	else:
		raise SyntaxError("you did not appear to give a proper argument of *what* to plot!")
	
	# show plot
	theplot.show()

def gnuplotsolutions(
	trial,
	s="", los=[],
	altfabunds=None,
	refspecies="", refvalues=[],
	xstart="", xstop="",
	ystart="", ystop="",
	width=630, height=390,
	saveplot=False, savehistory=False):
	r"""
	Uses the gnuplot to plot time evolutions.
	
	See ``plotsolutions`` documentation for more details.
	Note: If you run into 'symbol lookup' errors, try using the latest version of gnuplot (v5),
	and preferably a version compiled by yourself on your own system (i.e. not a pre-packaged version).
	"""
	Gnuplot.GnuplotOpts.gnuplot_command = '/usr/bin/gnuplot --persist'
	#gplot = Gnuplot.Gnuplot()
	gplot = GnuplotPlus()
	gplot('set term wxt')
	gplot('set for [i=1:5] linetype i dt i')
	if los:
		# initialize temp file
		f = tempfile.NamedTemporaryFile(mode='w',delete=False)
		filename1 = f.name
		lines = []
		# write x values
		for x in trial.physgrid.times: lines.append(str(x/float(yr)))
		# create space-separated list of x,y1,y2,y3,y4.. points
		plotcommand = "plot "
		col = 2
		if altfabunds:
			f2 = tempfile.NamedTemporaryFile(mode='w',delete=False)
			filename2 = f2.name
			lines2 = []
			for x in altfabunds['times']: lines2.append("%s" % float(x/float(yr)))
			col2 = 2
		colormap = getColormap(palette="nipy_spectral", format="html", num=len(los))
		# write out y values
		for isp,s in enumerate(los):
			namestring = trial.getmol(s).get_enhanced_name()
			if not refspecies == "":
				namestring += " / %s" % trial.getmol(refspecies).get_enhanced_name()
			for iy,y in enumerate(trial.getmol(s).fabunds):
				if not refspecies == "":
					y /= trial.getmol(refspecies).fabunds[iy]
				if float(y) > params['defaultinitialabund']:
					lines[iy] += ", " + "{0:E}".format(float(y))
				else:
					lines[iy] += ", -"
			plotcommand += '\\\n "%s" using 1:%g title "%s" w l dt 1 lw 2 lc rgb "%s", ' % (filename1, col, namestring, colormap[isp])
			col += 1
			if altfabunds and (s in list(altfabunds.keys())):
				for i,y in enumerate(altfabunds[s]):
					if not refspecies == "":
						y /= altfabunds[refspecies][iy]
					lines2[i] += " " + "{0:E}".format(float(y)) + ","
				plotcommand += '\\\n "%s" using 1:%g notitle w l dt 3 lt 0.5 lw 1.7 lc rgb "%s", ' % (filename2, col2, colormap[isp])
				col2 += 1
		if len(refvalues):
			colormap_refs = getColormap(palette="nipy_spectral", format="html", num=len(refvalues))
			for ir,r in enumerate(refvalues):
				if isinstance(r, tuple):
					if isinstance(r[0], float):
						plotcommand += '\\\n %s title "%s" w l dt 4 lt 2 lw 1.7 lc rgb "%s", ' % (r[0], r[1], colormap_refs[ir])
					elif isinstance(r[0], str) and (r[0][0] == "<"):
						v = r[0][1:]
						plotcommand += '\\\n %s title "%s" w l dt "." lt 2 lw 1.7 lc rgb "%s", ' % (v, r[1], colormap_refs[ir])
						#plotcommand += '%s title "%s" w points pt "v" lw 1.2 lc rgb "%s", ' % (v, r[1], colormap_refs[ir])
					elif isinstance(r[0], str) and ("--" in r[0]):
						v_low, v_up = r[0].split("--")
						v_low = v_low.strip()
						v_up = v_up.strip()
						plotcommand += '\\\n %s title "%s" w l dt 2 lt 2 lw 1.7 lc rgb "%s", ' % (v_up, r[1], colormap_refs[ir])
						plotcommand += '%s notitle w l dt 2 lt 2 lw 1.7 lc rgb "%s", ' % (v_low, colormap_refs[ir])
					else:
						try:
							plotcommand += '\\\n %s title "%s" w l dt 4 lt 2 lw 1.7 lc rgb "%s", ' % (float(r[0]), r[1], colormap_refs[ir])
						except:
							print("could not interpret the active refvalue: %s" % r[0])
				else:
					plotcommand += '\\\n %s title "ref: %s" w l dt 1 lt 2 lw 1.7 lc rgb "%s", ' % (r, r, colormap_refs[ir])
		# write to temp file
		for l in lines:
			f.write('%s\n' % l)
		f.close()
		if altfabunds:
			for l in lines2:
				f2.write('%s\n' % l)
			f2.close()
		# do plots
		gplot('set term wxt 0 enhanced lw 1.5 font "Univers-Bold" size %s,%s' % (width,height))
		gplot('set logscale xy')
		gplot('set offset graph 0.05, graph 0.05, graph 0.05, graph 0.05')
		gplot('set format x "10^{%T}"')
		gplot('set xlabel "Time (yr)"')
		gplot('set xtics in nomirror')
		gplot('set xrange [%s:%s]' % (xstart, xstop))
		gplot('set format y "10^{%T}"')
		gplot('set ylabel "Fractional Abundance (X_i/X_{hyd})"')
		gplot('set yrange [%s:%s]' % (ystart, ystop))
		gplot('set key outside')
		gplot('set border lw 1.5')
		plotcommand = plotcommand[:-2]
		if params['verbosity'] > 2: print("gnuplot command is:\n\t%s" % (plotcommand))
		gplot(plotcommand)
		if saveplot:
			f3 = tempfile.NamedTemporaryFile(mode='w', delete=False)
			filename3 = f3.name
			print("saving plot to %s" % filename3)
			f3.close()
			#termstring = 'set terminal postscript eps size %s,%s' % (int(width)/100.0,int(height)/100.0)
			#termstring += "font 'Helvetica,20' enhanced color linewidth 2"
			termstring = 'set terminal pdfcairo size %s,%s' % (int(width)/100.0,int(height)/100.0)
			termstring += " font 'Helvetica,14' enhanced color linewidth 2"
			gplot(termstring)
			gplot('set output "%s"' % filename3)
			gplot('set xtics nomirror rotate by -60 scale 1 font ",14"')
			gplot(plotcommand)
			gplot('unset output')
		if savehistory:
			f4 = tempfile.NamedTemporaryFile(mode='w', delete=False)
			print("saving history to %s" % f4.name)
			for c in gplot.history:
				f4.write("%s\n" % c)
				if params['verbosity'] > 2: print(c)
			f4.close()
	else:
		raise SyntaxError("you did not appear to give a proper argument of *what* to plot!")
	return gplot

def pgplotsolutions(
	trial,
	s="", los=[],
	altfabunds=None,
	refspecies="", refvalues=[],
	xstart=None, xstop=None,
	ystart=None, ystop=None,
	width=None, height=None,
	saveplot=False):
	r"""
	Uses the pyqtgraph to plot time evolutions.
	
	See ``plotsolutions`` documentation for more details.
	"""
	if not refspecies == "":
		raise NotImplementedError
	if len(refvalues):
		raise NotImplementedError
	if any([xstart, xstop, ystart, ystop]):
		print("warning: you requested ranges, but this interactive plot ignores it")
	if saveplot:
		raise NotImplementedError
	import gg_lib_qt
	reload(gg_lib_qt)
	qApp = QtGui.QApplication.instance()
	if qApp is None:
		qApp = QtGui.QApplication(sys.argv)
	
	# invert the color scheme to 'black-on-white'
	pg.setConfigOption('background', 'w')
	pg.setConfigOption('foreground', 'k')
	
	# set up the window
	title = "time plots from trial %s" % trial.trialname
	labels = {'left': "n(i) / n(total H)", 'bottom': "time (yr)"}
	win = gg_lib_qt.BasicAbundPlotter(trial, title=title, labels=labels, scale="loglog")
	trial.solution.plots.append(win)
	# define the x axis
	idx0 = 0
	if trial.physgrid.times[0] == 0:
		idx0 = 1
	times = np.asarray(trial.physgrid.times[idx0:]).copy()
	times /= yr
	times = np.log(times)/np.log(10.0)
	# move the single species to a list (for convenience)
	if not s == "":
		los += [s]
	RGB_tuples = getColormap(num=len(los))
	# loop through each species and add their plots
	for i,s in enumerate(los):
		fabunds = np.asarray(trial.getmol(s).fabunds[idx0:]).copy()
		mask = fabunds > 0
		fabunds = np.log(fabunds)/np.log(10.0)
		pen = pg.mkPen(RGB_tuples[i])
		pargs = (times[mask], fabunds[mask])
		pkwargs = {'name':s, 'pen':pen}
		win.addPlot(pargs=pargs, pkwargs=pkwargs)
		if altfabunds:
			try:
				afabunds = np.asarray(altfabunds[s][idx0:]).copy()
				amask = afabunds > 0
				afabunds = np.log(afabunds)/np.log(10.0)
				apen = pg.mkPen(RGB_tuples[i], style=QtCore.Qt.DashDotLine)
				alts = "%s (alt)" % s
				pargs = (times[amask], afabunds[amask])
				pkwargs = {'name':alts, 'pen':apen}
				win.addPlot(skipLegend=True, pargs=pargs, pkwargs=pkwargs)
			except KeyError:
				print("warning: species %s was not found in the altfabunds" % s)
	# activate/update QtGui
	QtGui.QApplication.instance().exec_()




def genericplot(trial, xdict, ydict, ydict2=None, scale='linear', scale2='linear'):
	r"""
	Uses the gnuplot to plot time evolutions.
	
	INPUTS:
	- ``xdict`` -- a dictionary containing a single key/val pair, where
			the key is simply the string to use for the x-axis, and
			the val is a list of x-values to serve as the x-axis
	- ``ydict`` -- a dictionary containing key/val pairs, where
			the key are strings to serve as the label for each set of values to plot,
			and each val is a list of y-values
			Note: the length of the y-values must all match the x-axis
	"""
	if params['plot_lib'] == "gnuplot":
		genericgnuplot(trial=trial, xdict=xdict, ydict=ydict, ydict2=ydict2, scale=scale, scale2=scale2)
	elif params['plot_lib'] == "pyqtgraph":
		genericpgplot(trial=trial, xdict=xdict, ydict=ydict, ydict2=ydict2, scale=scale, scale2=scale2)
	else:
		raise NotImplementedError("matplotlib doesn't have a generic plotter, sorry!")

def genericgnuplot(trial, xdict, ydict, ydict2=None, scale='linear', scale2='linear'):
	r"""
	Uses the gnuplot for generic plots of xy dictionaries. See genericplot
	for more details.
	"""
	Gnuplot.GnuplotOpts.gnuplot_command = '/usr/bin/gnuplot --persist'
	#gplot = Gnuplot.Gnuplot()
	gplot = GnuplotPlus()
	gplot('set term wxt')
	# initialize temp file
	f = tempfile.NamedTemporaryFile(mode='w',delete=False)
	filename1 = f.name
	plotlines = []
	# prepare x-axis
	if not len(list(xdict.keys())) == 1:
		raise ValueError("it appears that you tried to input more than one x-axis: %s" % list(xdict.keys()))
	xlabel = list(xdict.keys())[0]
	xlabel_string = '{}'.format(xlabel)
	for xval in xdict[xlabel]:
		plotlines.append(str(xval))
	# append y-values as space-separated y1,y2,y3,y4.. points
	plotcommand = "plot "
	loi = list(ydict.keys())
	col = 2
	for item in loi:
		if not len(ydict[item]) == len(xdict[xlabel]):
			raise ValueError("the length of the data series does not appear to match the x-axis' length")
		for i,yval in enumerate(ydict[item]):
			plotlines[i] += ", " + str(yval)
		plotcommand += '\\\n "' + filename1 + '" using 1:' + str(col) + ' title "' + item + '" with lines, '
		col += 1
	if ydict2:
		loi2 = list(ydict2.keys())
		for item in loi2:
			if not len(ydict2[item]) == len(xdict[xlabel]):
				raise ValueError("the length of the second data series does not appear to match the x-axis' length")
			for i,yval in enumerate(ydict2[item]):
				plotlines[i] += ", " + str(yval)
			plotcommand += '\\\n "' + filename1 + '" using 1:' + str(col) + ' title "' + item + '" with lines axes x1y2, '
			col += 1
	# write to temp file
	for l in plotlines:
		f.write('%s\n' % l)
	f.close()
	# do plots
	if scale=='log' or scale=='logx':
		gplot('set logscale x')
		gplot('set format x "10^{%T}"')
	elif scale=='logy':
		gplot('set logscale y')
		gplot('set format y "10^{%T}"')
	elif scale=='loglog':
		gplot('set logscale xy')
		gplot('set format x "10^{%T}"')
		gplot('set format y "10^{%T}"')
	if scale2=='logy':
		gplot('set log y2')
		gplot('set format y2 "10^{%T}"')
	else:
		gplot('unset log y2')
	gplot('set offset graph 0.05, graph 0.05, graph 0.05, graph 0.05')
	gplot('set xlabel "'+xlabel_string+'"')
	if len(loi)==1:
		gplot('set ylabel "'+loi[0]+'"')
	if ydict2:
		gplot('set ytics nomirror')
		gplot('set y2tics')
		if len(loi2):
			gplot('set y2label "'+loi2[0]+'"')
	gplot('set key outside')
	plotcommand = plotcommand[:-2]
	if params['verbosity'] > 1: print("gnuplot command is:\n\t", plotcommand)
	gplot(plotcommand)
	return gplot

def genericpgplot(trial, xdict, ydict, ydict2=None, scale='linear', scale2='linear'):
	r"""
	Uses pyqtgraph for generic plots of xy dictionaries. See genericplot
	for more details.
	"""
	import gg_lib_qt
	reload(gg_lib_qt)
	qApp = QtGui.QApplication.instance()
	if qApp is None:
		qApp = QtGui.QApplication(sys.argv)
	
	if ydict2:
		raise NotImplementedError("genericpgplot accepts only one y-axis still")
	# define the x axis
	xnames = list(xdict.keys())
	if len(xnames) > 1:
		raise NotImplementedError("genericpgplot accepts only one x-axis still")
	x = np.asarray(xdict[xnames[0]]).copy()
	idx0 = 0
	if x[0] == 0:
		idx0 = 1
	x = x[idx0:]
	if scale=='log' or scale=='logx' or scale=='loglog':
		x = x[x > 0]
		x = np.log(x)/np.log(10.0)
	# set up the window
	pg.setConfigOptions(antialias=True)
	title = "generic plot"
	labels = {'left': "arb", 'bottom': xnames[0]}
	win = gg_lib_qt.GenericPlotter(trial, title=title, labels=labels, scale=scale)
	trial.solution.plots.append(win)
	RGB_tuples = getColormap(num=len(list(ydict.keys())))
	# loop through each species and add their plots
	for i,k in enumerate(ydict.keys()):
		y = np.asarray(ydict[k][idx0:])
		mask = y > 0
		y = y[mask]
		if scale=='logy' or scale=='loglog':
			y = np.log(y)/np.log(10.0)
		pen = pg.mkPen(RGB_tuples[i])
		win.addPlot(x[mask], y, name=k, pen=pen)
	QtGui.QApplication.instance().exec_()




def old_maketkzgraph(texstring):
	r"""
	Creates a tex file from a string, and compiles it.
	
	INPUTS:
	- ``texstring`` -- a texstring
	
	OUTPUT:
	- a string containing the filename of the output files
	"""
	raise NotImplementedError("maketkzgraph doesn't yet work since dumping sage")
	# convert input to string if it is not already..
	if type(texstring) == type(latex('blah')):
		texstring = str(texstring)
	
	# define header and footers
	TIKZHEADER = \
	r'''\documentclass{article}
	\usepackage{amsmath}
	\usepackage{amssymb}
	\usepackage{amsfonts}
	\usepackage{graphicx}
	\usepackage{mathrsfs}
	\pagestyle{empty}
	\usepackage[utf8]{inputenc}
	\usepackage[T1]{fontenc}
	\usepackage[paperwidth=15in, paperheight=15in, top=1.5in, left=1in]{geometry}
	
	\usepackage{tikz}
	\usepackage{tkz-graph}
	\usepackage{tkz-berge}
	\usetikzlibrary{arrows,shapes}
	\newcommand{\ZZ}{\Bold{Z}}
	\newcommand{\NN}{\Bold{N}}
	\newcommand{\RR}{\Bold{R}}
	\newcommand{\CC}{\Bold{C}}
	\newcommand{\QQ}{\Bold{Q}}
	\newcommand{\QQbar}{\overline{\QQ}}
	\newcommand{\GF}[1]{\Bold{F}_{#1}}
	\newcommand{\Zp}[1]{\ZZ_{#1}}
	\newcommand{\Qp}[1]{\QQ_{#1}}
	\newcommand{\Zmod}[1]{\ZZ/#1\ZZ}
	\newcommand{\CDF}{\Bold{C}}
	\newcommand{\CIF}{\Bold{C}}
	\newcommand{\CLF}{\Bold{C}}
	\newcommand{\RDF}{\Bold{R}}
	\newcommand{\RIF}{\Bold{I} \Bold{R}}
	\newcommand{\RLF}{\Bold{R}}
	\newcommand{\CFF}{\Bold{CFF}}
	\newcommand{\Bold}[1]{\mathbf{#1}}
	
	\usepackage[version=3]{mhchem}
	
	\begin{document}
	\begin{center}
	
	'''
	
	TIKZFOOTER = \
	r'''
	\end{center}
	\end{document}
	'''
	
	# write output to temporary file
	f = tempfile.NamedTemporaryFile(mode='w',delete=False,suffix='.tex')
	filename = f.name
	f.write('%s' % TIKZHEADER.replace('\t',''))
	f.write('%s' % texstring)
	f.write('%s' % TIKZFOOTER.replace('\t',''))
	f.close()
	
	# run pdflatex on file
	from sage.misc.latex import _run_latex_
	suffix = run_latex(filename, engine='pdflatex', png=True)
	outputfile = filename
	pdfout = outputfile.replace(".tex", ".pdf")
	pngout = outputfile.replace(".tex", ".png")
	if os.path.isfile(pngout):
		outputfile = pngout
	elif os.path.isfile(pdfout):
		outputfile = pdfout
	else:
		"warning: there does not seem to be a PDF or PNG output file.."
	
	return outputfile




def old_rungraphtikzdemo():
	r"""
	Runs a basic demo of a tikz-based graph, which should demonstrate most all
	the available options.
	"""
	raise NotImplementedError("rungraphtikzdemo doesn't yet work since dumping sage")
	var('x y u w')
	G = Graph(loops=True)
	for i in range(5):
		for j in range(i+1, 5):
			G.add_edge((i, j), label=(x^i*y^j).expand())
	G.add_edge((0,0), label=sin(u))
	G.add_edge((4,4), label=w^5)
	G.set_pos(G.layout_circular())
	G.set_latex_options(
		units='in',
		graphic_size=(8,8),
		margins=(1,2,2,1),
		scale=0.5,
		vertex_color='0.8',
		vertex_colors={1:'aqua', 3:'y', 4:'#0000FF'},
		vertex_fill_color='blue',
		vertex_fill_colors={1:'green', 3:'b', 4:'#FF00FF'},
		vertex_label_color='brown',
		vertex_label_colors={0:'g',1:'purple',2:'#007F00'},
		vertex_shape='diamond',
		vertex_shapes={1:'rectangle', 2:'sphere', 3:'sphere', 4:'circle'},
		vertex_size=0.3,
		vertex_sizes={0:1.0, 2:0.3, 4:1.0},
		vertex_label_placements = {2:(0.6, 180), 4:(0,45)},
		edge_color='purple',
		edge_colors={(0,2):'g',(3,4):'red'},
		edge_fills=True,
		edge_fill_color='green',
		edge_label_colors={(2,3):'y',(0,4):'blue'},
		edge_thickness=0.05,
		edge_thicknesses={(3,4):0.2, (0,4):0.02},
		edge_labels=True,
		edge_label_sloped=True,
		edge_label_slopes={(0,3):False, (2,4):False},
		edge_label_placement=0.50,
		edge_label_placements={(0,4):'above', (2,3):'left', (0,0):'above', (4,4):'below'},
		loop_placement=(2.0, 'NO'),
		loop_placements={4:(8.0, 'EA')}
		)
	print(latex(G))
	filename = maketkzgraph(latex(G))
	G = GraphicsFile(filename)
	G.launch_viewer()





def old_rundirectedgraphtest():
	r"""
	Runs a minimal demo of a tikz-based directed graph with multiple edges.
	"""
	raise NotImplementedError("rundirectedgraphtest doesn't yet work since dumping sage")
	mygraph = DiGraph(multiedges=True)
	mygraph.add_edge('A','B','thinnest')
	mygraph.add_edge('A','B','blah1')
	mygraph.add_edge('A','B','thickest')
	mygraph.add_edge('A','B','blah2')
	mygraph.add_edge('A','C','blah')
	mygraph.add_edge('B','C','blah')
	mygraph.add_edge('C','B')
	mygraph.set_latex_options(
		units='in',
		graphic_size=(6,6),
		edge_thickness=0.01,
		edge_labels=True,
		edge_thicknesses={('A','B'):[0.01,0.025,0.05,0.025]},
		)
	print(latex(mygraph))
	#view(latex(mygraph))
	
	filename = maketkzgraph(latex(mygraph))
	G = GraphicsFile(filename)
	G.launch_viewer()




def old_runstumpgraphtest():
	r"""
	Runs a demo of a complex tikz-based directed graph containing multiple edges.
	"""
	raise NotImplementedError("runstumpgraphtest doesn't yet work since dumping sage")
	G = DiGraph(multiedges=True)
	G.add_edges(
		[('Binary trees', 'Binary trees', 'Left border symmetry'),
		 ('Binary trees', 'Binary trees', 'Left-right symmetry'),
		 ('Binary trees', 'Dyck paths', "recursive map '1 L 0 R'"),
		 ('Binary trees', 'Dyck paths', "recursive map 'L 1 R 0' (Tamari)"),
		 ('Dyck paths', 'Binary trees', "recursive map '1 L 0 R'"),
		 ('Dyck paths', 'Integer partitions', 'to partition'),
		 ('Dyck paths', 'Permutations', 'to 132 avoiding permutation'),
		 ('Dyck paths', 'Permutations', 'to 312 avoiding permutation'),
		 ('Dyck paths', 'Permutations', 'to 321 avoiding permutation'),
		 ('Dyck paths', 'Permutations', 'to non-crossing permutation'),
		 ('Integer partitions', 'Integer partitions', 'conjugate'),
		 ('Integer partitions', 'Standard tableaux', 'initial_tableau'),
		 ('Perfect Matchings', 'Permutations', 'to permutation'),
		 ('Permutations', 'Binary trees', 'Binary search tree (left to right)'),
		 ('Permutations', 'Binary trees', 'Increasing tree'),
		 ('Permutations', 'Integer compositions', 'descent composition'),
		 ('Permutations', 'Integer partitions', 'Robinson-Schensted tableau shape'),
		 ('Permutations', 'Permutations', 'complement'),
		 ('Permutations', 'Permutations', 'inverse'),
		 ('Permutations', 'Permutations', 'reverse'),
		 ('Set partitions', 'Integer partitions', 'shape'),
		 ('Set partitions', 'Permutations', 'to permutation'),
		 ('Standard tableaux', 'Integer partitions', 'shape'),
		 ('Standard tableaux', 'Permutations', 'reading word permutation')]
		)
	G.set_latex_options(
		graphic_size=(25,25),
		edge_labels=True,
		)
	#print latex(G)
	filename = maketkzgraph(latex(G))
	G = GraphicsFile(filename)
	G.launch_viewer()


def runweightednetworkxdemo(
	show_latex=True, show_plot=True, nohup_latex=False):
	
	import networkx as nx
	
	G = nx.DiGraph()
	
	G.add_edge('a', 'b', weight=0.6, label="a to b")
	G.add_edge('a', 'c', weight=0.2, label="a to c")
	G.add_edge('a', 'd', weight=0.3, label="a to d")
	G.add_edge('c', 'd', weight=0.1, label="c to d")
	G.add_edge('c', 'e', weight=0.7, label="c to e")
	G.add_edge('c', 'f', weight=0.9, label="c to f")
	
	ehuge = [(u,v) for (u,v,d) in G.edges(data=True) if d['weight'] >=0.9]
	emid  = [(u,v) for (u,v,d) in G.edges(data=True) if 0.2 < d['weight'] < 0.9]
	etiny = [(u,v) for (u,v,d) in G.edges(data=True) if d['weight'] <=0.2]
	
	# positions for all nodes
	pos = nx.spring_layout(G)
	pos = nx.graphviz_layout(G)
	
	# nodes
	nx.draw_networkx_nodes(G, pos, node_size=700)
	
	# edges
	nx.draw_networkx_edges(G,pos,edgelist=ehuge, width=10)
	nx.draw_networkx_edges(G,pos,edgelist=emid, width=6,alpha=0.5,edge_color='b',style='dashed')
	nx.draw_networkx_edges(G,pos,edgelist=etiny, width=1)
	
	# labels
	nx.draw_networkx_labels(G, pos, font_size=20, font_family='sans-serif')
	
	if show_latex:
		# get latex code, process it, and view it
		latex = nx_to_latex(graph=G)
		f = tempfile.NamedTemporaryFile(mode='w',delete=False,suffix='.tex')
		filename = f.name
		f.write(latex)
		f.close()
		# run tex on it
		filename = run_latex(f.name, engine="pdflatex", format="pdf")
		cmd = "`which okular` /tmp/%s" % filename
		if nohup_latex:
			subprocess.call(cmd.split())
		else:
			os.system(cmd)
	
	if show_plot:
		plt.axis('off')
		plt.show() # display


def show_graph(
	graph=None, latex=None,
	show_latex=True, show_plot=True, nohup_latex=False,
	autocrop_latex=True):
	
	import networkx as nx
	
	if show_latex:
		if graph and (latex is None):
			# get latex code
			latex = nx_to_latex(graph)
		if not latex:
			return
		print(latex)
		# process it
		f = tempfile.NamedTemporaryFile(mode='w', delete=False, suffix='.tex')
		filename = f.name
		f.write(latex)
		f.close()
		filename = run_latex(f.name, engine="pdflatex", format="pdf")
		olddir = os.getcwd()
		if autocrop_latex:
			os.chdir("/tmp")
			os.system("`which pdfcrop2` --mode auto %s %s" % (filename, filename))
			os.chdir(olddir)
		# view it
		if nohup_latex:
			cmd = "okular /tmp/%s" % filename
			subprocess.Popen(cmd, shell=True)
		else:
			os.system("`which okular` /tmp/%s" % filename)
	
	if show_plot:
		if isinstance(graph, list):
			msg = "WARNING: you requested to use matplotlib for multiple graphs,"
			msg += " but this is not explicitly supported.. will only plot the"
			msg += " first one.."
			print(msg)
			graph = graph[0]
		# get graph stuff
		pos = nx.graphviz_layout(graph)
		edges = graph.edges(data=True)
		nx.draw_networkx_nodes(graph, pos, node_size=700)
		nx.draw_networkx_edges(graph, pos, edgelist=edges, width=1)
		nx.draw_networkx_labels(graph, pos, font_size=20, font_family='sans-serif')
		plt.axis('off')
		plt.show() # display


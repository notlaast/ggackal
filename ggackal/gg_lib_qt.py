#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# TODO
#
# DESCRIPTION
#	Provides classes for browsing the results of a trial from
#	ggackal using PyQt4-based interfaces.
#
# standard library
# third-party
import numpy as np
import pyqtgraph as pg
from pyqtgraph.Qt import QtGui, QtCore, QtSvg
# local

class BasicAbundPlotter(QtGui.QWidget):
	"""
	Provides a basic widget that contains a pyqtgraph plot.	In addition,
	a number of keyboard/mouse shortcuts are also available.
	"""
	def __init__(
		self,
		trial=None,
		title=None, # str
		labels=None, # dict like {‘left’:'blah', ‘bottom’:'blah', ‘right’:'blah', and/or ‘top’:'blah'}
		scale=None):
		super(self.__class__, self).__init__()
		
		self.trial = trial
		self.title = title
		self.labels = labels
		self.scale = scale
		
		self.plots = []
		self.plotOptions = []
		
		self.shortcutQuit = QtGui.QShortcut(QtGui.QKeySequence("Esc"), self)
		self.shortcutQuit.activated.connect(self.close)
		
		self.initUI()
		self.initPlot()
		self.show()
	
	
	def initUI(self):
		self.layout = QtGui.QVBoxLayout()
		self.setLayout(self.layout)
		self.resize(1000, 600)
	
	
	def initPlot(self):
		
		pg.setConfigOptions(antialias=True)
		
		self.pw = pg.PlotWidget(title=self.title, labels=self.labels)
		self.layout.addWidget(self.pw)
		
		self.plot = self.pw.getPlotItem()
		self.vb = self.plot.getViewBox()
		#self.legend = self.plot.addLegend()
		self.legend = LegendItem(size=None, offset=(30, 30))
		self.legend.setParentItem(self.vb)
		
		# axis scaling
		if (self.scale == "log") or (self.scale == "logx"):
			self.plot.setLogMode(x=True)
		elif self.scale == "logy":
			self.plot.setLogMode(y=True)
		elif self.scale == "loglog":
			self.plot.setLogMode(x=True, y=True)
		
		# signals
		self.mouseLabeldot = pg.TextItem(text="", anchor=(0.5,0.5), fill=(0,0,0,0))
		self.mouseLabeltext = pg.TextItem(text="", anchor=(0,1), fill=(0,0,0,0))
		self.plot.addItem(self.mouseLabeldot)
		self.plot.addItem(self.mouseLabeltext)
		self.mouseLabeldot.setZValue(999)
		self.mouseLabeltext.setZValue(999)
		self.sigMouseMove = pg.SignalProxy(
			self.plot.scene().sigMouseMoved,
			rateLimit=10,
			slot=self.mousePosition)
		self.plotLabels = []
		self.sigMouseClick = pg.SignalProxy(
			self.plot.scene().sigMouseClicked,
			rateLimit=10,
			slot=self.mouseClicked)
	
	
	def mousePosition(self, mouseEvent):
		mousePos = self.vb.mapSceneToView(mouseEvent[0])
		mouseX, mouseY = mousePos.x(), mousePos.y()
		modifier = QtGui.QApplication.keyboardModifiers()
		if modifier == QtCore.Qt.ShiftModifier:
			if not self.plots:
				return
			Xidx = np.abs(self.plots[0].xData - mouseX).argmin()
			mouseXdiscrete = self.plots[0].xData[Xidx]
			plotDistances = []
			for i,p in enumerate(self.plots):
				try:
					distance = np.abs(mouseY - p.yData[p.xData.searchsorted(mouseXdiscrete)])
					plotDistances.append((distance, i))
				except IndexError:
					pass
			nearestPlotIdx = sorted(plotDistances)[0][1]
			nearestPlot = self.plots[nearestPlotIdx]
			nearestName = nearestPlot.name()
			nearestX = nearestPlot.xData[nearestPlot.xData.searchsorted(mouseXdiscrete)]
			nearestY = nearestPlot.yData[nearestPlot.xData.searchsorted(mouseXdiscrete)]
			htmlText = "<div style='text-align:left'><span style='font-size: 14pt'>"
			htmlText += "<span style='color:green'>%s: %.1e</span>"
			htmlText += "</span></div>"
			self.mouseLabeldot.setPos(nearestX, nearestY)
			self.mouseLabeldot.setText("*")
			self.mouseLabeltext.setPos(nearestX, nearestY)
			self.mouseLabeltext.setHtml(htmlText % (nearestName, 10**nearestY))
		elif modifier == QtCore.Qt.ControlModifier:
			self.mouseLabeldot.setPos(mouseX, mouseY)
			self.mouseLabeldot.setText("*")
			self.mouseLabeltext.setPos(mouseX, mouseY)
			self.mouseLabeltext.setText("%.1e\n%.1e" % (10**mouseX, 10**mouseY))
		else:
			self.mouseLabeldot.setPos(0,0)
			self.mouseLabeldot.setText("")
			self.mouseLabeltext.setPos(0,0)
			self.mouseLabeltext.setText("")
	
	
	def mouseClicked(self, mouseEvent):
		mousePos = self.vb.mapSceneToView(mouseEvent[0].scenePos())
		mouseX, mouseY = mousePos.x(), mousePos.y()
		modifier = QtGui.QApplication.keyboardModifiers()
		if modifier == QtCore.Qt.ShiftModifier:
			if not self.plots:
				return
			labelDot = pg.TextItem(text="*", anchor=(0.5,0.5))
			labelDot.setPos(self.mouseLabeltext.pos())
			self.plot.addItem(labelDot)
			labelText = pg.TextItem(text="", anchor=(0,1), fill=(0,0,0,0))
			labelText.setPos(self.mouseLabeltext.pos())
			labelText.setHtml(self.mouseLabeltext.textItem.toHtml())
			self.plot.addItem(labelText)
			self.plotLabels.append((labelDot,labelText))
		elif modifier == QtCore.Qt.ControlModifier:
			labelDot = pg.TextItem(text="*", anchor=(0.5,0.5))
			labelDot.setPos(self.mouseLabeltext.pos())
			self.plot.addItem(labelDot)
			labelText = pg.TextItem(text="", anchor=(0,1), fill=(0,0,0,0))
			labelText.setPos(self.mouseLabeltext.pos())
			labelText.setText("%.1e\n%.1e" % (10**mouseX, 10**mouseY))
			self.plot.addItem(labelText)
			self.plotLabels.append((labelDot,labelText))
	
	
	def addPlot(self, **kwargs):
		pargs = kwargs['pargs']
		pkwargs = kwargs['pkwargs']
		skipLegend = False
		if 'skipLegend' in list(kwargs.keys()):
			skipLegend = kwargs['skipLegend']
		
		plot = pg.PlotCurveItem(*pargs, **pkwargs)
		self.plot.addItem(plot)
		self.plots.append(plot)
		self.plotOptions.append(kwargs)
		if not skipLegend:
			self.legend.addItem(plot, plot.name())
	
	def resetLegend(self):
		self.legend.scene().removeItem(self.legend)
		self.legend = self.plot.addLegend()
		for i,p in enumerate(self.plots):
			hasSkipLegendKey = 'skipLegend' in list(self.plotOptions[i].keys())
			toSkipLegend = hasSkipLegendKey and self.plotOptions[i]['skipLegend']
			if (not hasSkipLegendKey) or (not toSkipLegend):
				self.legend.addItem(p, p.name())


class GenericPlotter(QtGui.QWidget):
	"""
	Provides a basic widget that contains a pyqtgraph plot.	In addition,
	a number of keyboard/mouse shortcuts are also available.
	"""
	def __init__(
		self,
		trial=None,
		title=None, # str
		labels=None, # dict like {‘left’:'blah', ‘bottom’:'blah', ‘right’:'blah', and/or ‘top’:'blah'}
		scale=None):
		super(self.__class__, self).__init__()
		
		self.trial = trial
		self.title = title
		self.labels = labels
		self.scale = scale
		
		self.plots = []
		
		self.shortcutQuit = QtGui.QShortcut(QtGui.QKeySequence("Esc"), self)
		self.shortcutQuit.activated.connect(self.close)
		
		self.initUI()
		self.initPlot()
		self.show()
	
	
	def initUI(self):
		"""
		Defines layout of GUI, since there's no *.ui file defined for this.
		"""
		self.layout = QtGui.QVBoxLayout()
		self.setLayout(self.layout)
		self.resize(1000, 600)
	
	
	def initPlot(self):
		"""
		Sets up plot..
		"""
		pg.setConfigOptions(antialias=True)
		
		self.pw = pg.PlotWidget(title=self.title, labels=self.labels)
		self.layout.addWidget(self.pw)
		
		self.plot = self.pw.getPlotItem()
		self.vb = self.plot.getViewBox()
		self.legend = self.plot.addLegend()
		
		# axis scaling
		if (self.scale == "log") or (self.scale == "logx"):
			self.plot.setLogMode(x=True)
		elif self.scale == "logy":
			self.plot.setLogMode(y=True)
		elif self.scale == "loglog":
			self.plot.setLogMode(x=True, y=True)
		
		# objects/signals for interacting with the mouse
		self.mouseLabeldot = pg.TextItem(text="", anchor=(0.5,0.5), fill=(0,0,0,0))
		self.mouseLabeltext = pg.TextItem(text="", anchor=(0,1), fill=(0,0,0,0))
		self.plot.addItem(self.mouseLabeldot)
		self.plot.addItem(self.mouseLabeltext)
		self.mouseLabeldot.setZValue(999)
		self.mouseLabeltext.setZValue(999)
		self.sigMouseMove = pg.SignalProxy(
			self.plot.scene().sigMouseMoved,
			rateLimit=10,
			slot=self.mousePosition)
		self.plotLabels = []
		self.sigMouseClick = pg.SignalProxy(
			self.plot.scene().sigMouseClicked,
			rateLimit=10,
			slot=self.mouseClicked)
	
	
	def addPlot(self, *args, **kwargs):
		"""
		Used to add plots to the GUI.
		"""
		plot = pg.PlotCurveItem(*args, **kwargs)
		self.plot.addItem(plot)
		self.plots.append(plot)
	
	
	def mousePosition(self, mouseEvent):
		"""
		Called each time the mouse is moved above the plot.
		"""
		mousePos = self.vb.mapSceneToView(mouseEvent[0])
		mouseX, mouseY = mousePos.x(), mousePos.y()
		modifier = QtGui.QApplication.keyboardModifiers()
		if modifier == QtCore.Qt.ShiftModifier:
			if not self.plots:
				return
			Xidx = np.abs(self.plots[0].xData - mouseX).argmin()
			mouseXdiscrete = self.plots[0].xData[Xidx]
			plotDistances = []
			for i,p in enumerate(self.plots):
				try:
					distance = np.abs(mouseY - p.yData[p.xData.searchsorted(mouseXdiscrete)])
					plotDistances.append((distance, i))
				except IndexError:
					pass
			nearestPlotIdx = sorted(plotDistances)[0][1]
			nearestPlot = self.plots[nearestPlotIdx]
			nearestName = nearestPlot.name()
			nearestX = nearestPlot.xData[nearestPlot.xData.searchsorted(mouseXdiscrete)]
			nearestY = nearestPlot.yData[nearestPlot.xData.searchsorted(mouseXdiscrete)]
			htmlText = "<div style='text-align:left'><span style='font-size: 14pt'>"
			htmlText += "<span style='color:green'>%s: %.1e</span>"
			htmlText += "</span></div>"
			self.mouseLabeldot.setPos(nearestX, nearestY)
			self.mouseLabeldot.setText("*")
			self.mouseLabeltext.setPos(nearestX, nearestY)
			self.mouseLabeltext.setHtml(htmlText % (nearestName, 10**nearestY))
		elif modifier == QtCore.Qt.ControlModifier:
			self.mouseLabeldot.setPos(mouseX, mouseY)
			self.mouseLabeldot.setText("*")
			self.mouseLabeltext.setPos(mouseX, mouseY)
			self.mouseLabeltext.setText("%.1e\n%.1e" % (10**mouseX, 10**mouseY))
		else:
			self.mouseLabeldot.setPos(0,0)
			self.mouseLabeldot.setText("")
			self.mouseLabeltext.setPos(0,0)
			self.mouseLabeltext.setText("")
	
	
	def mouseClicked(self, mouseEvent):
		"""
		Called each time the mouse clicks the plot.
		"""
		mousePos = self.vb.mapSceneToView(mouseEvent[0].scenePos())
		mouseX, mouseY = mousePos.x(), mousePos.y()
		modifier = QtGui.QApplication.keyboardModifiers()
		if modifier == QtCore.Qt.ShiftModifier:
			if not self.plots:
				return
			labelDot = pg.TextItem(text="*", anchor=(0.5,0.5))
			labelDot.setPos(self.mouseLabeltext.pos())
			self.plot.addItem(labelDot)
			labelText = pg.TextItem(text="", anchor=(0,1), fill=(0,0,0,0))
			labelText.setPos(self.mouseLabeltext.pos())
			labelText.setHtml(self.mouseLabeltext.textItem.toHtml())
			self.plot.addItem(labelText)
			self.plotLabels.append((labelDot,labelText))
		elif modifier == QtCore.Qt.ControlModifier:
			labelDot = pg.TextItem(text="*", anchor=(0.5,0.5))
			labelDot.setPos(self.mouseLabeltext.pos())
			self.plot.addItem(labelDot)
			labelText = pg.TextItem(text="", anchor=(0,1), fill=(0,0,0,0))
			labelText.setPos(self.mouseLabeltext.pos())
			labelText.setText("%.1e\n%.1e" % (10**mouseX, 10**mouseY))
			self.plot.addItem(labelText)
			self.plotLabels.append((labelDot,labelText))


class LegendItem(pg.LegendItem):
	"""
	Provides a sub-classed LegendItem from PyQtGraph which handles the
	widths correctly. This way, the width does not constantly grow as
	the legend is updated with items.
	
	Note that this simple serves as a bug fix.
	"""
	def __init__(self, **opts):
		super(self.__class__, self).__init__(**opts)
	
	def updateSize(self):
		if self.size is not None:
			return
		
		height = 0
		width = 0
		for sample, label in self.items:
			height += max(sample.height(), label.height()) + 3
			width = max(width, sample.width(), label.width())
		self.setGeometry(0, 0, width+25, height)
	
	def paint(self, p, *args):
		p.setPen(pg.mkPen(QtGui.QColor(pg.Colors[pg.CONFIG_OPTIONS['background']]).setAlpha(100)))
		p.setBrush(pg.mkBrush(QtGui.QColor(pg.Colors[pg.CONFIG_OPTIONS['foreground']]).setAlpha(100)))
		p.drawRect(self.boundingRect())


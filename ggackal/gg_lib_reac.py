#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# TODO
# - finish moving the general routines to the OO-based class definitions
#	getallvarsatiteration -> solution
#	getreactionrate -> solution
#	getelementalsinks -> solution
#
# DESCRIPTION
#	All subroutines specific to the chemical reaction network should go here.
#	This includes and should be limited to:
#		- reaction network initialization
#		- abundances/densities
#		- rate law definitions
#		- ODE definitions
#	Thus they should essentially be limited to manipulating the information
#	contained in the gg_in_init.d and gg_in_network.d input files.
#
# standard library
from __future__ import absolute_import
from __future__ import print_function
import os
import sys
import math
import re
# third-party
import numpy as np
from progressbar import *
import scipy
# local
if not os.path.dirname(os.path.abspath(__file__)) in sys.path:
	sys.path.append(os.path.dirname(os.path.abspath(__file__)))
from gg_lib_sys import *
from gg_lib_physics import *
from gg_lib_math import *




def getjac(trial):
	r"""
	Returns a jacobian based on a list of functions and variables
	
	INPUTS:
	- ``trial`` -- the ggtrial object
	
	OUTPUT:
	a 2D-array containing the jacobian
	
	TODO:
	- describe shape of jacobian
	"""
	if params['verbosity'] > 0: print("computing the Jacobian")
	nrow = len(trial.solution.des)
	ncol = len(trial.solution.dvars)
	jac = []
	if params['verbosity'] > 0:
		# define progress bar
		pbwidgets = [Percentage(), Bar('>')]
		pbar = ProgressBar(widgets=pbwidgets, maxval=nrow).start()
	# create dictionary to note which column each variable belongs to
	getcol = {}
	for i,v in enumerate(trial.solution.dvars):
		getcol[v] = i
	# loop through functions and add partial derivatives of each contained variable
	for i,f in enumerate(trial.solution.des):
		row = [dummyfunc for j in range(ncol)]
		if is_Expression(f) and mathlib == "sympy":
			for v in f.atoms(sympy.Symbol):
				if v in [T, gasdensity, av]: continue
				row[getcol[v]] = sympy.lambdify(trial.solution.dvars, sympy.diff(f, v), 'numpy')
		elif is_Expression(f) and mathlib == "symengine":
			for v in f.atoms(symengine.Symbol):
				if v in [T, gasdensity, av]: continue
				row[getcol[v]] = symengine.Lambdify(trial.solution.dvars, [symengine.diff(f, v)])
		elif is_Expression(f) and mathlib == "theano":
			raise Exception("the jacobian cannot be prepared using the theano library yet!")
		jac.append(np.asarray(row))
		if params['verbosity'] > 0: pbar.update(i)
	if params['verbosity'] > 0: pbar.finish()
	return jac




def doorder(trial, log_it=[], log_s=[]):
	r"""
	Analyzes the model solution for reaction rate information and outputs to file.
	
	INPUTS:
	- ``logit`` -- (optional) list of iterations
	- ``logs`` -- (optional) list of species of interest
	
	OUTPUT:
	a dictionary of order information, with the keys:
		'iterations' - list of iterations to be analyzed
		'species' - list of species to be analyzed
		'vars' - list of dependent variables used
		'densities' - dict of densities/values, with keys as each iteration
		'formrxns' - dict (with each key in species) for list of formation reactions
		'formtot' - dict (with first key in species) containing tot form rate (with 2nd key as iter)
		'destrxns' - dict (with each key in species) for all destruction reactions
		'desttot' - dict (with first key in species) containing tot dest rate (with 2nd key as iter)
	
	NOTES:
	- each reaction now has a 'ff' key that points to a callable fast_float that takes densities
	
	TODO:
	- describe shape of jacobian
	"""
	##
	# initialize containers
	orderinfo = {}
	if log_it:
		orderinfo['iterations'] = log_it
	else:
		orderinfo['iterations'] = arange(len(params['log_order_times']))
		print("no iterations were specified, so tried to make a list of all:", orderinfo['iterations'])
	orderinfo['densities'] = {}
	orderinfo['formrxns'] = {}
	orderinfo['formtot'] = {}
	orderinfo['destrxns'] = {}
	orderinfo['desttot'] = {}
	##
	# populate list with fractional abundances at each requested time/iteration
	#	if this was called from the gui (and thus iterations match exactly)
	if log_it or params['log_order_timestouse']=='iterations':
		if params['verbosity'] > 2: print("will now process the selected species/iterations", orderinfo['iterations'])
		for it in orderinfo['iterations']:
			orderinfo['densities'][it] = getallvarsatiteration(trial, it)
	#	if only specific times, determine exact times/iterations
	elif (not params['log_order_alltimes']):
		raise NotImplementedError("cannot doorder for specific times yet")
		if params['verbosity'] > 2: print("\tmaking sense of the requested parameter times..", end=' ')
		# if times are not exact, interpolate or round..
		if params['log_order_notexacttimes'] and (not params['log_it']) and (not params['log_order_timestouse']=='iterations'):
			if (params['log_order_timestouse'] == 'rounded'):
				if params['verbosity'] > 2: print("by rounding up to the next iteration")
				# get iterations
				stoppedat = 0
				for i,t in enumerate(params['log_order_times']):
					for it,seconds in enumerate(trial.physgrid.times[stoppedat:]):
						if seconds > t:
							orderinfo['iterations'][i] = it
							stoppedat = it
							break
				# note densities
				for it in orderinfo['iterations']:
					orderinfo['densities'][it] = getallvarsatiteration(trial, it)
			elif (params['log_order_timestouse'] == 'interpolated'):
				if params['verbosity'] > 2: print("by interpolating densities between iterations")
				raise NotImplementedError('not ready yet, sorry..')
	#	otherwise just loop through iteration times and append
	else:
		raise NotImplementedError("cannot doorder for all times yet")
		if params['verbosity'] > 0: print("warning: you requested all times, so this may take awhile!")
		orderinfo['iterations'] = list(range(len(trial.physgrid.times)))[1:]
		for it in orderinfo['iterations']:
			orderinfo['densities'][it] = getallvarsatiteration(trial, it)
		raise NotImplementedError('not ready yet, sorry..')
	##
	# populate list of reactions involving each species
	if (not log_s):
		if params['log_order_allspecies']:
			if params['verbosity'] > 0: print("warning: you requested all species, so this may take awhile!")
			log_s = trial.molecules.get_names()
		else:
			log_s = log_order_species
	if params['verbosity'] > 1: print("\tparsing all reaction information for each species..")
	orderinfo['species'] = log_s
	for s in log_s:
		orderinfo['formrxns'][s] = []
		orderinfo['formtot'][s] = {}
		orderinfo['destrxns'][s] = []
		orderinfo['desttot'][s] = {}
		for rxn in trial.network.reactions:
			if (s in rxn.products) and (not s in rxn.reactants):
				orderinfo['formrxns'][s].append(rxn)
			if (s in rxn.reactants) and (not s in rxn.products):
				orderinfo['destrxns'][s].append(rxn)
		if params['verbosity'] > 1:
			print("\t\t", s, "has", len(orderinfo['formrxns'][s]), "formation routes", end=' ')
			print("and", len(orderinfo['destrxns'][s]), "destruction routes")
	##
	# walk through each density and collect total rates
	for it in orderinfo['iterations']:
		# loop through molecules
		for s in orderinfo['species']:
			# loop through reactions
			sum_form = 0
			for rxn in orderinfo['formrxns'][s]:
				sum_form += getreactionrate(trial, it, rxn) * rxn.products.count(s)
			orderinfo['formtot'][s][it] = sum_form
			sum_dest = 0
			for rxn in orderinfo['destrxns'][s]:
				sum_dest -= getreactionrate(trial, it, rxn) * rxn.reactants.count(s)
			orderinfo['desttot'][s][it] = sum_dest
	
	return orderinfo




def getallvarsatiteration(trial, it=None, t=None):
	r"""
	Returns a list of values that can be used for to evaluate the rate equations
	for a particular iteration/time.
	
	INPUTS:
	- ``it`` -- iteration requested
	- ``t`` -- (not implemented yet) time in yrs, to be interpolated
	
	OUTPUT:
	- a list of values of densities for each species, as well as the iteration's physical properties
	"""
	if it is not None:
		totaldensity = trial.physgrid.densities[it]
		densities = []
		for s in trial.molecules.molecules:
			d = s.fabunds[it] * totaldensity
			densities.append(d)
		densities.append(trial.physgrid.temperatures[it])
		densities.append(trial.physgrid.densities[it])
		densities.append(trial.physgrid.extinctions[it])
		return densities
	elif t:
		raise NotImplementedError("cannot getallvarsatiteration for a specific time yet")
	else:
		raise SyntaxError("You did not seem to input an iteration or time for desired dvar values.")




def getreactionrate(trial, it, rxn):
	r"""
	Returns the rate of the requested reaction at a particular interation.
	
	INPUTS:
	- ``it`` -- the iteration number at which to be evaluated
	- ``rxn`` -- the reaction to be evaluated
	
	NOTE:
	Note that the reaction is to be the full entry that can be found within the
	allreactions list.
	"""
	densities = getallvarsatiteration(trial, it)
	try:
		rate = rxn.ff(densities)
	except AttributeError:
		# define fast_float of the reaction, if it doesn't exist already
		dvars = []
		for s in trial.molecules.molecules:
			dvars.append(s.var)
		dvars += [T, gasdensity, av]
		if mathlib == "sympy":
			if is_Expression(rxn.ratelaw):
				rxn.ff = sympy.lambdify(dvars, rxn.ratelaw, 'numpy')
			else:
				rxn.ff = dummyfunc
		elif mathlib == "symengine":
			if is_Expression(rxn.ratelaw):
				rxn.ff = symengine.Lambdify(dvars, [rxn.ratelaw])
			else:
				rxn.ff = dummyfunc
		elif mathlib == "theano":
			raise NotImplementedError("getreactionrate doesn't work with theano yet")
			rxn.ff = lambda x: rxn.ratelaw.eval(dict(izip(dvars, x)))
		rate = rxn.ff(densities)
	return float(rate)




def getelementalsinks(trial, element, it=0, t=0, numlimit=None, fraclimit=0, getresult='print'):
	r"""
	Analyzes the chemical network to determine where most of a particular element
	can be found, and returns the result in a variety of optional forms.
	
	INPUTS:
	- ``element`` -- the element of interest, as a string
	- ``it`` -- the iteration number
	- ``numlimit`` -- (optional) the number of entries to limit the result
	- ``fraclimit`` -- (optional) the minimum fractional contribution to limit to
	- ``getresult`` -- the form in which the result should be returned, with options:
		`print` -- (default) only prints the result directly to the terminal
		`dict` -- returns a dictionary containing the results
		`text` -- returns a list of strings containing the results
	
	OUTPUT:
	- a list of species, and both their fractional abundance and what fraction
	  of the element can be found in the species
	"""
	if (not it in range(len(trial.physgrid.times))) and (not t):
		print("ERROR: you requested a non-existent iteration (nor time)")
		return
	# process abunds for each species
	elements = {
		"H"		:	'hydrogen'	,
		"He"	:	'helium'	,
		"C"		:	'carbon'	,
		"N"		:	'nitrogen'	,
		"O"		:	'oxygen'	,
		"F"		:	'fluorine'	,
		"Na"	:	'sodium'	,
		"Mg"	:	'magnesium'	,
		"Si"	:	'silicon'	,
		"P"		:	'phos'		,
		"S"		:	'sulfur'	,
		"Cl"	:	'chlorine'	,
		"Fe"	:	'iron'
	}
	if not element in list(elements.keys()):
		print("ERROR: you requested an unknown element", element)
		return
	sink_tot = 0
	sink_dict = {}
	for s in trial.molecules.get_names():
		if not trial.getmol(s).stoi[elements[element]]==0:
			sink_dict[s] = {}
			# find elemental stoichiometry of species
			stoichiometry = trial.getmol(s).stoi[elements[element]]
			sink_dict[s]['stoichiometry'] = stoichiometry
			if params['verbosity'] > 2: print("found %s, which contains %s x %s" % (s, element, stoichiometry))
			# find fractional abundance at the requested iteration
			if it:
				sink_dict[s]['fabund'] = trial.getmol(s).fabunds[it]
			else:
				fabundsplrep = scipy.interpolate.splrep([sec/float(yr) for sec in trial.physgrid.times], trial.getmol(s).fabunds)
				sink_dict[s]['fabund'] = scipy.interpolate.splev(t, fabundsplrep)
			totfabund = sink_dict[s]['fabund']*stoichiometry
			sink_tot += totfabund
			sink_dict[s]['tot'] = totfabund
	if params['verbosity'] > 1: print("sink_tot was %s" % sink_tot)
	# sort by abundance
	sink_species = []
	i = 0
	if not numlimit:
		numlimit = len(list(sink_dict.keys()))
	if not fraclimit:
		fraclimit = 0
	for s in sorted(sink_dict, key=lambda k:sink_dict[k]['tot'], reverse=True):
		i += 1
		sink_dict[s]['ftot'] = sink_dict[s]['tot']/sink_tot
		if (i > numlimit) or (sink_dict[s]['ftot'] < fraclimit):
			break
		sink_species.append(s)
	# process results
	if not getresult=='dict':
		if getresult=='text':
			result = []
		header = "{:<10}\t".format('species') + "{:<12}\t".format('fabund') + "{:<12}\t".format('elmnt frxn')
		if getresult=='text':
			result.append(header)
		else:
			print(header)
		for s in sink_species:
			string = ''
			string += "{:<10}\t".format(s)
			string += "{0:<12E}\t".format(float(sink_dict[s]['fabund']))
			string += "{0:<12E}\t".format(float(sink_dict[s]['ftot']))
			if getresult=='text':
				result.append(string)
			else:
				print(string)
		if getresult=='text':
			return result
	else:
		return sink_species,sink_dict




def findupdatedcomment(oldrxn, newfile):
	#print "\nworking on rxn %s" % oldrxn
	newfilehandle = open(newfile, 'r')
	for line in newfilehandle:
		if oldrxn.rstring in line:
			#print "\tfound a matching rxn: %s" % line.replace("\t\t\t\t","")
			restring = r"%s, (.*)$" % oldrxn.comment
			rematch = re.search(restring, line, re.VERBOSE)
			if rematch:
				#print "\tmatch: %s" % rematch.group(1)
				newfilehandle.close()
				return str(rematch.group(1)).strip()
	newfilehandle.close()
	return None




###########
# for returning all the rate coefficients
###########
###
# DESCRIPTION:
#	sets the expression for the rate coefficients according to their reaction type
# ARGUMENTS:
#	k1, k2, ...
# RETURNS:
#	the reaction rate as a function of all relevant variables
#
###
#  0	Gas-grain interaction, electron-grain recombination (old version)
#		A  +  B  +  grain  ->  C  +  grain
#	or	A+  +  grain-  ->  A  +  grain
#	or	A+  +  grain-  ->  A  +  B  +  grain
def getratefromrtype0(rxn):
	ggtrial = rxn.trial
	dummyh2rxns = ['H + H + G0 -> H2 + G0', 'H + H + G1- -> H2 + G1-']
	K = rxn.k1 * (T / 300.0)**rxn.k2 * gastodustratio
	if (not K) and params['warn_zeroratecoeff']:
		rxn.print_properties(title="received a zero-valued rate coefficient for:")
	rate = K * rxn.br
	for r in rxn.reactants:
		rate *= ggtrial.getmol(r).var
	if rxn.reaction in dummyh2rxns:
		rate /= ggtrial.getmol('H').var
	return rate

###
#  1	Cosmic-ray ionization (direct)
#		A  +  cr  ->  A+  +  e-
def getratefromrtype1(rxn):
	ggtrial = rxn.trial
	K = rxn.k1 * params['ionrate']
	if (not K) and params['warn_zeroratecoeff']:
		rxn.print_properties(title="received a zero-valued rate coefficient for:")
	rate = K * rxn.br * ggtrial.getmol(rxn.reactants[0]).var # checked against OSU2008 ORDER
	return rate

###
#  2	Ion-molecule reactions and charge-exchange reactions
#		A+  +  B  ->  A  +  B+
#	or	A+  +  B  ->  C+  +  D
def getratefromrtype2(rxn):
	ggtrial = rxn.trial
	K = rxn.k1 * (T / 300.0)**rxn.k2 * exp(-rxn.k3 / T)
	if (not K) and params['warn_zeroratecoeff']:
		rxn.print_properties(title="received a zero-valued rate coefficient for:")
	rate = K * rxn.br * ggtrial.getmol(rxn.reactants[0]).var * ggtrial.getmol(rxn.reactants[1]).var # checked against OSU2008 ORDER
	return rate

###
#  3	Anion-neutral reactions
#		A-  +  B  ->  A  +  B-
#	or	A-  +  B  ->  C  +  e-
def getratefromrtype3(rxn):
	ggtrial = rxn.trial
	K = rxn.k1 * (T / 300.0)**rxn.k2 * exp(-rxn.k3 / T)
	if (not K) and params['warn_zeroratecoeff']:
		rxn.print_properties(title="received a zero-valued rate coefficient for:")
	rate = K * rxn.br * ggtrial.getmol(rxn.reactants[0]).var * ggtrial.getmol(rxn.reactants[1]).var
	return rate

###
#  4	Ion-neutral radiative association
#		A+  +  B  ->  C+  +  photon
def getratefromrtype4(rxn):
	ggtrial = rxn.trial
	K = rxn.k1 * (T / 300.0)**rxn.k2 * exp(-rxn.k3 / T)
	if (not K) and params['warn_zeroratecoeff']:
		rxn.print_properties(title="received a zero-valued rate coefficient for:")
	rate = K * rxn.br * ggtrial.getmol(rxn.reactants[0]).var * ggtrial.getmol(rxn.reactants[1]).var # checked against OSU2008 ORDER
	return rate

###
#  5	Anion-neutral electron ejection
#		A-  +  B  ->  C  +  e-
def getratefromrtype5(rxn):
	ggtrial = rxn.trial
	K = rxn.k1 * (T / 300.0)**rxn.k2 * exp(-rxn.k3 / T)
	if (not K) and params['warn_zeroratecoeff']:
		rxn.print_properties(title="received a zero-valued rate coefficient for:")
	rate = K * rxn.br * ggtrial.getmol(rxn.reactants[0]).var * ggtrial.getmol(rxn.reactants[1]).var # checked against OSU2008 ORDER
	return rate

###
#  6	Neutral-neutral reaction producing cation + electron
#		Only one entry in gg08: O + CH -> HCO+ + e-
def getratefromrtype6(rxn):
	ggtrial = rxn.trial
	K = rxn.k1 * (T / 300.0)**rxn.k2 * exp(-rxn.k3 / T)
	if (not K) and params['warn_zeroratecoeff']:
		rxn.print_properties(title="received a zero-valued rate coefficient for:")
	rate = K * rxn.br * ggtrial.getmol(rxn.reactants[0]).var * ggtrial.getmol(rxn.reactants[1]).var
	return rate

###
#  7	Neutral-neutral gas-phase reaction
#		A  +  B  ->  C  +  D
def getratefromrtype7(rxn):
	ggtrial = rxn.trial
	K = rxn.k1 * (T / 300.0)**rxn.k2 * exp(-rxn.k3 / T)
	if (not K) and params['warn_zeroratecoeff']:
		rxn.print_properties(title="received a zero-valued rate coefficient for:")
	rate = K * rxn.br * ggtrial.getmol(rxn.reactants[0]).var * ggtrial.getmol(rxn.reactants[1]).var # checked against OSU2008 ORDER
	return rate

###
#  8	Neutral-neutral radiative association
#		A  +  B  ->  C  +  photon
def getratefromrtype8(rxn):
	ggtrial = rxn.trial
	K = rxn.k1 * (T / 300.0)**rxn.k2 * exp(-rxn.k3 / T)
	if (not K) and params['warn_zeroratecoeff']:
		rxn.print_properties(title="received a zero-valued rate coefficient for:")
	rate = K * rxn.br * ggtrial.getmol(rxn.reactants[0]).var * ggtrial.getmol(rxn.reactants[1]).var # checked against OSU2008 ORDER
	return rate

###
#  9	Dissociative electron recombination
#		A+  +  e-  ->  B  +  C
def getratefromrtype9(rxn):
	ggtrial = rxn.trial
	K = rxn.k1 * (T / 300.0)**rxn.k2 * exp(-rxn.k3 / T)
	if (not K) and params['warn_zeroratecoeff']:
		rxn.print_properties(title="received a zero-valued rate coefficient for:")
	rate = K * rxn.br * ggtrial.getmol(rxn.reactants[0]).var * ggtrial.getmol(rxn.reactants[1]).var
	return rate

###
# 10	Radiative recombination (non-dissociative)
#		atom+  +  e-  ->  atom
def getratefromrtype10(rxn):
	ggtrial = rxn.trial
	K = rxn.k1 * (T / 300.0)**rxn.k2 * exp(-rxn.k3 / T)
	if (not K) and params['warn_zeroratecoeff']:
		rxn.print_properties(title="received a zero-valued rate coefficient for:")
	rate = K * rxn.br * ggtrial.getmol(rxn.reactants[0]).var * ggtrial.getmol(rxn.reactants[1]).var # checked against OSU2008 ORDER ***POSSIBLY A TYPO IN THEIR CODE***
	return rate

###
# 11	Coulombic Recombination
#		A+  +  B-  ->  A  +  B
#	or	A+  +  B-  ->  C  +  D
def getratefromrtype11(rxn):
	ggtrial = rxn.trial
	K = rxn.k1 * (T / 300.0)**rxn.k2 * exp(-rxn.k3 / T)
	if (not K) and params['warn_zeroratecoeff']:
		rxn.print_properties(title="received a zero-valued rate coefficient for:")
	rate = K * rxn.br * ggtrial.getmol(rxn.reactants[0]).var * ggtrial.getmol(rxn.reactants[1]).var # checked against OSU2008 ORDER
	return rate

###
# 12	Electron attachment
#		atom  +  e-  ->  atom-
def getratefromrtype12(rxn):
	ggtrial = rxn.trial
	K = rxn.k1 * (T / 300.0)**rxn.k2 * exp(-rxn.k3 / T)
	if (not K) and params['warn_zeroratecoeff']:
		rxn.print_properties(title="received a zero-valued rate coefficient for:")
	rate = K * rxn.br * ggtrial.getmol(rxn.reactants[0]).var * ggtrial.getmol(rxn.reactants[1]).var # checked against OSU2008 ORDER
	return rate

###
# 13	Photoionization and photodissociation
#		A  +  photon  ->  A+  +  e-
#	or	A  +  photon  ->  B  +  C
def getratefromrtype13(rxn):
	ggtrial = rxn.trial
	K = rxn.k1 * params['uvgasscalefac']
	if params['preferE2form'] and rxn.k2:
		K *= expint(2, -rxn.k2*av)
	else:
		K *= exp(-rxn.k3*av)
	if (not K) and params['warn_zeroratecoeff']:
		rxn.print_properties(title="received a zero-valued rate coefficient for:")
	rate = K * rxn.br * ggtrial.getmol(rxn.reactants[0]).var # checked against OSU2008 ORDER
	return rate

###
# 14	Grain-surface chemical reactions
#		gA  +  gB  ->  gC (+ gD)
#		gA  +  gB  ->  C (+ D)
def getratefromrtype14(rxn):
	ggtrial = rxn.trial
	if params['useseparatedusttemp']:
		Td = params['initialdusttemp']
	else:
		Td = T
	if params['usesurfchem'] and params['usertype14']:
		if params['verbosity'] > 1:
			print("working on the rtype=14 reaction: %s (numTOT=%s)" % (rxn.reaction, rxn.numTOT))
		reactant1 = ggtrial.getmol(rxn.reactants[0])
		reactant2 = ggtrial.getmol(rxn.reactants[1])
		product1 = ggtrial.getmol(rxn.products[0])
		thermdiff1 = reactant1.vibfreq * exp(-reactant1.diffbarr / float(Td)) / params['grainsites']
		thermdiff2 = reactant2.vibfreq * exp(-reactant2.diffbarr / float(Td)) / params['grainsites']
		
		###
		# define scaling for reaction barriers
		barr = 1
		if params['usegrainqm']:
			# add the rectangular barrier approximation
			if set(rxn.reactants).intersection(["gH", "gH2"]):
				classical_expfac = rxn.k3 / float(Td)
				reduced_mass = reactant1.mass * reactant2.mass / (reactant1.mass + reactant2.mass)
				qm_expfac = 2 * params['grainbarrwidth'] / hbar * sqrt(2 * reduced_mass * k_b * rxn.k3)
				total_expfac = sqrt(qm_expfac**2 + classical_expfac**2)
				barr = exp(-total_expfac)
		else:
			barr = exp(-rxn.k3 / Td)
		
		###
		# define quantum rates for diffusing
		if params['usemodh']:
			# RQ1(I)=DEB(I)*BOLTZ/4.0D+0/HBAR/TNS
			qmA_diffrate1 = reactant1.tunnelbarr * k_b / 4.0 / hbar / params['grainsites']
			qmA_diffrate2 = reactant2.tunnelbarr * k_b / 4.0 / hbar / params['grainsites']
			# RQ2(I)=CHF(I)/TNS*EXP(-2.0D+0*ACM/HBAR*SQRT(2.0D+0*AMU*SMA*BOLTZ*EB(I)))
			qmB_diffrate1 = reactant1.vibfreq / params['grainsites'] * exp(-2*params['grainbarrwidth']/hbar*sqrt(2*reactant1.mass*k_b*reactant1.diffbarr))
			qmB_diffrate2 = reactant2.vibfreq / params['grainsites'] * exp(-2*params['grainbarrwidth']/hbar*sqrt(2*reactant2.mass*k_b*reactant2.diffbarr))
			if params['usemodh'] == 1:
				if not rxn.reactants[0] == "gH":
					qmA_diffrate1 = 0
					qmB_diffrate1 = 0
				if not rxn.reactants[1] == "gH":
					qmA_diffrate2 = 0
					qmB_diffrate2 = 0
			elif params['usemodh'] == 2:
				if not set(["gH", "gH2"]).intersection([rxn.reactants[0]]):
					qmA_diffrate1 = 0
					qmB_diffrate1 = 0
				if not set(["gH", "gH2"]).intersection([rxn.reactants[1]]):
					qmA_diffrate2 = 0
					qmB_diffrate2 = 0
			elif params['usemodh'] > 2:
				raise NotImplementedError("usemodh > 2 is not supported")
		
		R_diff1 = sqrt(thermdiff1**2 + qmA_diffrate1**2 + qmB_diffrate1**2)
		R_diff2 = sqrt(thermdiff2**2 + qmA_diffrate2**2 + qmB_diffrate2**2)
		K = (R_diff1 + R_diff2)
		
		###
		#	account for branching and thermodynamics
		effscale = rxn.br
		#
		# determine amount of branching
		nevap = 0
		if product1.name[0] == 'g':
			otherrxns = ggtrial.network.filter('reactants', rxn.reactants, reactions=ggtrial.network.rtype14rxns)
			for evaprxn in ggtrial.network.filter('products', [p.lstrip('g') for p in rxn.products], reactions=otherrxns):
				if evaprxn.k1 > 0: nevap += 1
		# calculate heat of formation for reaction
		sum_heatformation = reactant1.heatformation + reactant2.heatformation
		for p in rxn.products:
			sum_heatformation -= ggtrial.getmol(p).heatformation
		sum_heatformation *= -503.217 # convert from kcal/mol to K
		#sum_heatformation += rxn.k3 # account for activation energy <- wrong!
		# determine the limiting binding energy
		sum_binde = product1.bindingenergy
		if len(rxn.products) > 1: sum_binde = max(sum_binde, ggtrial.getmol(rxn.products[1]).bindingenergy)
		if len(rxn.products) == 3: sum_binde = max(sum_binde, ggtrial.getmol(rxn.products[2]).bindingenergy)
		# find the number of atoms for reactant1
		numatoms = min(reactant1.numatoms, reactant2.numatoms)
		# determine the effective "partition function"
		try:
			partfunc = 1 - (sum_binde / float(sum_heatformation))
		except ZeroDivisionError:
			partfunc = 1
		if numatoms == 2:
			partfunc = partfunc**(3*numatoms - 5)
		elif numatoms > 3:
			partfunc = partfunc**(3*numatoms - 6)
		partfunc *= params['rrkcoeffa']
		evfrac = partfunc / float((1+partfunc))
		# fix bad cases
		badflag = 0
		if reactant1.heatformation <= -999:
			evfrac = 0
			badflag += 1
		if reactant2.heatformation <= -999:
			evfrac = 0
			badflag += 1
		if product1.heatformation <= -999:
			evfrac = 0
			badflag += 1
		#if len(rxn.products) > 1:
		#	evfrac = 0
		#	badflag += len(rxn.products)-1
		# set evfrac/nevap/dhfsum to limits when they are excessive
		if evfrac > 1:
			evfrac = 1
		elif evfrac < 0:
			evfrac = 0
		if nevap == 0:
			evfrac = 0
		if sum_heatformation > 0:
			evfrac = 0
		# rescale XJ as scaling factor also for the EVFRAC
		effscale *= evfrac
		
		if not rxn.products[0][0] == 'g':
			effscale *= 0
		
		rate = rxn.k1 * effscale * K * barr * reactant1.var * reactant2.var
		
		if rxn.k1 and ("S" in "".join(rxn.reactants)) and not ("Si" in "".join(rxn.reactants)) and rxn.products[0][0]=="g":
			try:
				float(rate)
			except (TypeError, RuntimeError):
				pass
			else:
				print("found a bad sulfur reaction! %s" % rxn.reaction)
				print("\trate was %s" % rate)
				print("\tbarr was %s" % barr)
				print("\tnevap was %s" % nevap)
				print("\tsum_heatformation = %s" % sum_heatformation)
				print("\tsum_binde = %s" % sum_binde)
				print("\tpartfunc was %s" % partfunc)
				print("\tevfrac was %s" % evfrac)
				print("\tbadflag was %s" % badflag)
				for m in rxn.reactants + rxn.products:
					print("\t%s has heatformation %s" % (m, ggtrial.getmol(m).heatformation))
		
		
	else:
		rate = 0
	## for checking the rates of a few reactions (if usegrainqm==False)
	#if rxn.reaction == 'gH + gC -> gCH':
	#	print "would use the rate for '%s' as: %s" % (rxn.reaction, rate)
	#	replacements = {
	#		T: 10,
	#		gasdensity: 3e3,
	#		ggtrial.getmol('gC').var: 1e-40,
	#		ggtrial.getmol('gH').var: 1e-40}
	#	print "initial rate should be: %s and should give ~4.32e-72*gasdensity" % rate.subs_dict(replacements).n(8, real=True)
	#if rxn.reaction == 'gC + gC -> gC2':
	#	print "would use the rate for '%s' as: %s" % (rxn.reaction, rate)
	#	replacements = {
	#		T: 10,
	#		gasdensity: 2e4,
	#	ggtrial.getmol('gC').var: 1e-40}
	#	print "initial rate should be: %s and should give ~8.35e-80" % rate.subs_dict(replacements).n(8, real=True)
	return rate

###
# 15	Thermal evaporation
#		gA (evap)  ->  A
def getratefromrtype15(rxn):
	ggtrial = rxn.trial
	if params['useseparatedusttemp']:
		Td = params['initialdusttemp']
	else:
		Td = T
	if params['usesurfchem'] or params['usersurfaccrdes']:
		reactant = ggtrial.getmol(rxn.reactants[0])
		K = rxn.k1 * rxn.br * reactant.vibfreq * exp(-reactant.bindingenergy/float(Td))
		if (not K) and params['warn_zeroratecoeff']:
			rxn.print_properties(title="received a zero-valued rate coefficient for:")
	else:
		K = 0
	rate = K * ggtrial.getmol(rxn.reactants[0]).var # checked against OSU2008 ORDER
	#if rxn['reaction'] == 'gC (evap) -> C':
	#	print "found gC evaporation"
	#	print "would use rate as", rate.simplify()
	#	print "initial rate should be:", rate(T=10,gasdensity=2e4,concgC=allmolprop['gC']['density']).n()/2e4, "and should give ~2.34e-63"
	#	raise Exception
	#if rxn['reaction'] == 'gO (evap) -> O':
	#	print "zeroed O desorption (thermal)"
	#	rate = 0
	return rate

###
# 16	Cosmic Ray induced general desorption
#		gA  +  cr (evap)  ->  A
def getratefromrtype16(rxn):
	ggtrial = rxn.trial
	if params['useseparatedusttemp']:
		Td = params['initialdusttemp']
	else:
		Td = T
	if params['usesurfchem'] and params['usersurfphotochem']:
		reactant = ggtrial.getmol(rxn.reactants[0])
		vibfreq = reactant.vibfreq
		binde = reactant.bindingenergy
		K = rxn.k1 * rxn.br * vibfreq * exp(-binde / params['grainpeaktemp']) * params['grainironencounterrate'] * params['grainheatingduration']
		K *= (params['dustradius'] / 1e-5)**2		# scale the rate for alternative dust radii
		if (not K) and params['warn_zeroratecoeff']:
			rxn.print_properties(title="received a zero-valued rate coefficient for:")
	else:
		K = 0
	rate = K * ggtrial.getmol(rxn.reactants[0]).var # checked against OSU2008 ORDER
	#if rxn['reaction'] == 'gC + cr (evap) -> C':
	#	print "found gC CR-induced evaporation"
	#	print "would use rate as", rate.simplify()
	#	print "initial rate should be:", rate(T=10,gasdensity=2e4,concgC=allmolprop['gC']['density']).n()/2e4, "and should give ~4.24e-52"
	#	raise Exception
	return rate

###
# 17 	Photodissociation by cosmic rays (direct)
#		gA  +  cr (PD)  ->  gB  +  gC
def getratefromrtype17(rxn):
	ggtrial = rxn.trial
	if params['useseparatedusttemp']:
		Td = params['initialdusttemp']
	else:
		Td = T
	if params['usesurfchem'] and params['usersurfphotochem']:
		K = rxn.k1 * rxn.br * params['ionrate'] * params['crscalefac17']
		# cap rate at monolayerlimit
		# IF (Y(JSP1(J)).GT.MONLAY) XK(J)=XK(J)*MONLAY/Y(JSP1(J))
			# MONLAY = LAYERS*TNS/GTODN
			#	==> monolayerlimit * grainsites / gastodustratio
			# Y(K) = density
			#	==> rxnprop['reactants'][0]
		#speciesdensity = allmolprop[rxnprop['reactants'][0]]['var']
		#K *= limitingdensity/speciesdensity * erf(speciesdensity/limitingdensity) # why did I try this once?
		if (not K) and params['warn_zeroratecoeff']:
			rxn.print_properties(title="received a zero-valued rate coefficient for:")
	else:
		K = 0
	rate = K * ggtrial.getmol(rxn.reactants[0]).var # checked against OSU2008 ORDER
	return rate

###
# 18 	Photodissociation by cosmic rays (indirect)
#		gA  +  cr (ind-PD)  ->  gA+  +  e-  ->  gB  +  gC
def getratefromrtype18(rxn):
	ggtrial = rxn.trial
	if params['useseparatedusttemp']:
		Td = params['initialdusttemp']
	else:
		Td = T
	if params['usesurfchem'] and params['usersurfphotochem']:
		K = rxn.k1 * rxn.br * params['ionrate'] * params['crscalefac18']
		#speciesdensity = allmolprop[rxnprop['reactants'][0]]['var']
		#K *= limitingdensity/speciesdensity * erf(speciesdensity/limitingdensity) # why did I try this once?
		if (not K) and params['warn_zeroratecoeff']:
			rxn.print_properties(title="received a zero-valued rate coefficient for:")
	else:
		K = 0
	rate = K * ggtrial.getmol(rxn.reactants[0]).var
	return rate

###
# 19	Photodissociation by background photons (direct)
#		gA  +  photon (PD)  ->  gB  +  gC
def getratefromrtype19(rxn):
	ggtrial = rxn.trial
	if params['useseparatedusttemp']:
		Td = params['initialdusttemp']
	else:
		Td = T
	if params['usesurfchem'] and params['usersurfphotochem']:
		K = rxn.k1 * rxn.br * params['uvgrainscalefac']
		if params['preferE2form'] and rxn.k2:
			K *= expint(2, -rxn.k2*av)
		else:
			K *= exp(-rxn.k3*av)
		#speciesdensity = allmolprop[rxnprop['reactants'][0]]['var']
		#K *= limitingdensity/speciesdensity * erf(speciesdensity/limitingdensity) # why did I try this once?
		if (not K) and params['warn_zeroratecoeff']:
			rxn.print_properties(title="received a zero-valued rate coefficient for:")
	else:
		K = 0
	rate = K * ggtrial.getmol(rxn.reactants[0]).var # checked against OSU2008 ORDER
	return rate

###
# 20	Photodissociation by background photons (indirect)
#		gA  +  photon (ind-PD)  ->  gA+  +  e-  ->  gB  +  gC
def getratefromrtype20(rxn):
	ggtrial = rxn.trial
	if params['usesurfchem'] and params['usersurfphotochem']:
		K = rxn.k1 * rxn.br * params['uvgrainscalefac']
		if params['preferE2form'] and rxn.k2:
			K *= expint(2, -rxn.k2*av)
		else:
			K *= exp(-rxn.k3*av)
		#speciesdensity = allmolprop[rxnprop['reactants'][0]]['var']
		#K *= limitingdensity/speciesdensity * erf(speciesdensity/limitingdensity) # why did I try this once?
		if (not K) and params['warn_zeroratecoeff']:
			rxn.print_properties(title="received a zero-valued rate coefficient for:")
	else:
		K = 0
	rate = K * ggtrial.getmol(rxn.reactants[0]).var # checked against OSU2008 ORDER
	return rate

###
# 80	Photodesorption (direct)
#		gA  +  photon (PD)  ->  A
def getratefromrtype80(rxn):
	ggtrial = rxn.trial
	if params['usesurfchem'] and params['usersurfphotochem'] and params['usersurfphotodes']:
		K = rxn.k1 * rxn.br * params['uvgrainscalefac'] * params['fuvrate']
		if params['preferE2form'] and rxn.k2:
			K *= expint(2, -rxn.k2*av)
		else:
			K *= exp(-rxn.k3*av)
		if (not K) and params['warn_zeroratecoeff']:
			rxn.print_properties(title="received a zero-valued rate coefficient for:")
	else:
		K = 0
	rate = K * ggtrial.getmol(rxn.reactants[0]).var # checked against OSU2008 ORDER
	return rate

###
# 97	Surface -> mantle migration
def getratefromrtype97(rxn):
	ggtrial = rxn.trial
	if params['useseparatedusttemp']:
		Td = params['initialdusttemp']
	else:
		Td = T
	if params['usesurfchem'] and params['usesurfmantle']:
		K = 0	# fix this!!
	else:
		K = 0
	rate = K * rxn.br * ggtrial.getmol(rxn.reactants[0]).var
	return rate

###
# 98	Mantle -> surface migration
def getratefromrtype98(rxn):
	ggtrial = rxn.trial
	if params['useseparatedusttemp']:
		Td = params['initialdusttemp']
	else:
		Td = T
	if params['usesurfchem'] and params['usesurfmantle']:
		K = 0	# fix this!!
	else:
		K = 0
	rate = K * rxn.br * ggtrial.getmol(rxn.reactants[0]).var
	return rate

###
# 99	Grain surface accretion
#		A (accr)  ->  gA
def getratefromrtype99(rxn):
	ggtrial = rxn.trial
	if params['useseparatedusttemp']:
		Td = params['initialdusttemp']
	else:
		Td = T
	if params['usesurfchem'] or params['usersurfaccrdes']:
		reactant = ggtrial.getmol(rxn.reactants[0])
		velpersqrtT = reactant.velpersqrtT
		collrate = graincrosssection * velpersqrtT * sqrt(T) * gasdensity / float(gastodustratio)
		final_stick_prob = params['stickingprobability']
		if params['stickingprobability_dependsonbinde']:
			final_stick_prob *= exp(- Td / float(reactant.bindingenergy))
		K = rxn.k1 * rxn.br * rxn.br * collrate * final_stick_prob
		if (not K) and params['warn_zeroratecoeff']:
			rxn.print_properties(title="received a zero-valued rate coefficient for:")
	else:
		K = 0
	rate = K * ggtrial.getmol(rxn.reactants[0]).var # checked against OSU2008 ORDER
	#if rxn['reaction'] == 'C (accr) -> gC':
	#	print "found C accretion"
	#	print "would use rate as", rate.simplify()
	#	print "initial rate should be:", rate(T=10,gasdensity=2e4,concC=allmolprop['C']['density']).n()/2e4, "and should give ~5.51e-54"
	#	raise Exception
	return rate


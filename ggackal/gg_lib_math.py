#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# DESCRIPTION
#	This file should contain all the subroutines related purely to
#	number-crunching. Thus only mathematical functions and interfaces/solvers
#	should go here. They should return single items..
#
# standard library
from __future__ import absolute_import
import os
import sys
# third-party
import numpy as np
import sympy
import symengine
#import theano
# local
if not os.path.dirname(os.path.abspath(__file__)) in sys.path:
	sys.path.append(os.path.dirname(os.path.abspath(__file__)))
from gg_lib_sys import *
from gg_lib_dummyfunc import dummyfunc

mathlib = "symengine"

# misc
pi = np.pi
eulconst = 0.577215664901533
if mathlib == "sympy":
	sqrt = sympy.sqrt
	log = sympy.log
	exp = sympy.exp
	erf = sympy.erf
	expint = sympy.expint
elif mathlib == "symengine":
	sqrt = symengine.sqrt
	log = symengine.log
	exp = symengine.exp
	def erf(x):
		return symengine.tanh(log(2)*sqrt(pi)*x)
	def ei_small(x):
		# note: this is only appropriate (i.e. to <10%) for x < 0.8
		# ref: Numerical Recipes (3rd Ed.), Eq. 6.3.10
		val = eulconst + log(x)
		for n in range(1,6):
			val += x**n / float(n) / sympy.factorial(n)
		return val
	def ei_big(x):
		# note: this is only appropriate (i.e. to <10%) for x > 1.2
		# ref: Numerical Recipes (3rd Ed.), Eq. 6.3.11
		val = 1
		for n in range(1,5):
			val += n/float(x)
		return val * exp(x)/float(x)
	def expint(n, x):
		if n == 0:
			raise NotImplementedError("expint does not support n=0!")
		elif n == 1:
			return -(0.5*ei_small(-x) + 0.5*ei_big(-x))
		else:
			return exp(-x) - x*expint(n-1, x)
	if params['preferE2form']:
		raise NotImplementedError("the E2-based photo rates appear to be unstable with this symengine hack!")
elif mathlib == "theano":
	sqrt = theano.tensor.sqrt
	log = theano.tensor.log
	exp = theano.tensor.exp
	erf = theano.tensor.erf
	if params['preferE2form']:
		raise NotImplementedError("theano does not support the E2-based photo rates!")




###
## integrator checks
#
# for the scipy ode methods
scipyodemethods = ['vode','lsoda']
if (params['intlib']=="vode") and (not params['iter_maxnum']):
	print("warning: you did not set a maximum number of internal integrator steps, but this integrator")
	print("         method may fail if not allowed to try many (n >> 500) internal timesteps; setting a")
	print("         large number (iter_maxnum=5000) now..")
	params['iter_maxnum'] = 5000
# for the GSL ode methods
gslodemethods = [
	'rk2','rk4','rkf45','rkck','rk8pd',
	'rk1imp','rk2imp','rk4imp',
	'bsimp',
	'msadams','msbdf',
	'gear1','gear2']
if (params['intlib']=='bsimp') and not params['computejac']:
	print("this integration method requires jacobian; will exit now")
	raise NotImplementedError("see message above, sorry")
# for error tolerances
if params['iter_atol']==0: params['iter_atol']=None
if params['iter_rtol']==0: params['iter_rtol']=None
# for troublesome iterations
prevjacfunc = None




def log10(x):
	"""
	log10(x) -> base-10 logarithm
	"""
	return float(log(x) / log(10.0))




def cdf(x):
	"""
	Returns the value of the cumulative distribution function (CDF).
	"""
	return 1/2.0*(1+erf(x/sqrt(2)))




def ggdvar(*args):
	r"""
	Returns the symbolic dependent variable of choice.
	
	This is useful for being a single place where all symbolic variables can be defined, so that it is easier for one to change their definitions, and thus the library to be used for symbolics.
	
	INPUTS:
	- ``*args`` -- any arguments which would be sent directly to e.g. var()
	
	OUTPUT:
	The symbolic variable of preferred type.
	
	EXAMPLES:
	- ggdvar('x y z')
	- ggdvar('a, b')
	- ggdvar('T')
	
	NOTE:
	- one must control the backends by commenting out the alternative options..
	
	TODO:
	- possibly make it possible to set a switch in gg_in_params.py for preferred library
	"""
#	return var(*args, domain='real')
#	return SR.var(*args, domain=RR) # slightly faster (by avoiding globals)
#	return SR.var(*args) # avoid domains, because sage is an asshole right now
	if mathlib == "sympy":
		return sympy.Symbol(*args)
	elif mathlib == "symengine":
		return symengine.Symbol(*args)
	elif mathlib == "theano":
		return theano.tensor.dscalar(*args)




def is_Expression(v):
	"""
	Return `True` if any of the arguments are symbolic.
	"""
	if mathlib in ["sympy", 'symengine'] and 'is_Add' in dir(v):
		return True
	elif mathlib == "theano" and 'var' in dir(v):
		return True
	else:
		return False




def contains_nan(y):
	"""
	Return `True` if any of the items are NaN.
	"""
	if not type(y).__module__ == np.__name__:
		y = np.asarray(y)
	if np.isnan(y).any():
		return True
	else:
		False


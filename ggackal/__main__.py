#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# standard library
from __future__ import absolute_import
import sys
import os
import argparse
# third-party
pass
# local
if not os.path.dirname(os.path.abspath(__file__)) in sys.path:
	sys.path.append(os.path.dirname(os.path.abspath(__file__)))
from gg_main import *


showintro()

gg14_memory_usage.postintro = gg14_memory_usage.get()
if params['verbosity'] > 0: print("post-intro memory usage is", gg14_memory_usage.postintro)

# define arguments
parser = argparse.ArgumentParser()
subparsers = parser.add_subparsers()
### general (optional) flags
parser.add_argument(
	"-d", "--debug", action='store_true', default=False,
	help="whether to add extra print messages to the terminal")
parser.add_argument("--check", action='store_true', help="performs a check-only run")
parser.add_argument("--profile", action='store_true', help="profiles a run")
parser.add_argument("--ipython", action='store_true', help="activates ipython (actually, only instructs HOW to do so!)")
parser.add_argument("--forcegui", action='store_true', help="whether to override the input parameter and show the GUI after all")
### subcommands
# run new trial
p_run = subparsers.add_parser("run", help="performs a normal trial run")
p_run.set_defaults(command="run")
# load old trial
p_load = subparsers.add_parser("load", help="loads an old trial")
p_load.add_argument("OLDDIR", nargs=1, help="directory containing an old trial")
p_load.set_defaults(command="load")
# compare two networks
p_comp = subparsers.add_parser("comp", help="compare two network files and prepare a report about it")
p_comp.add_argument("NETWORK1", nargs=1, help="first network for comparison")
p_comp.add_argument("NETWORK2", nargs=1, help="second network for comparison")
p_comp.set_defaults(command="comp")
p_comp.add_argument("--reportrtype", help="(optional) whether to print in terminal any difference in included rxn types")
p_comp.add_argument("--reportuniquerxns", action='store_true', help="(optional) whether to print in terminal all the unique reactions from each network")
p_comp.add_argument("--reportuniquemols", action='store_true', help="(optional) whether to print in terminal all the unique molecules from each network")
p_comp.add_argument("--reportlatex", help="(optional) the output file for a latex report")

# parse arguments and perform actions
args = parser.parse_args()
if args.debug:
	print("args were: %s" % args)
params['checkonly'] = args.check
if not "command" in args: # in py2, isn't automatically added to Namespace
	args.command = None
if args.ipython:
	print("\nyou should try running:")
	print("ipython -i gg_main.py -- [args]")
elif args.command == "run":
	if args.profile:
		runsnake('rungg()')
	else:
		rungg()
	if args.check and (not params['log_dir'] == "."):
		shutil.rmtree(params['log_fulldir']) # clean up output directory on clean exit
elif args.command == "load":
	if args.profile:
		runsnake('loadprevgg(%s, forcegui=%s)' % (args.OLDDIR[0], args.forcegui))
	else:
		loadprevgg(args.OLDDIR[0], forcegui=args.forcegui)
elif args.command == "comp":
	if not os.path.isfile(args.NETWORK1[0]):
		raise IOError("the first network file '%s' does not appear to be a file!" % args.NETWORK1[0])
	elif not os.path.isfile(args.NETWORK2[1]):
		raise IOError("the second network file '%s' does not appear to be a file!" % args.NETWORK2[0])
	compare_networks(
		fname1=args.NETWORK1[0], fname2=args.NETWORK2[1],
		doreport_rtypecomp=args.reportrtype,
		doreport_printuniqrxns=args.reportuniquerxns,
		doreport_printuniqmols=args.reportuniquemols,
		doreport_latex=args.reportlatex)
else:
	print("you must specify an action ().. here's the help message (also available via -h/--help):")
	parser.print_help()

sys.exit()
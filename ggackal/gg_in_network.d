H + H + G0 -> H2 + G0									RTYPE=0 K1=4.95E-17 K2=5.00E-01 K3=0.0 # gg08_rnum=1
H + H + G1- -> H2 + G1-									RTYPE=0 K1=4.95E-17 K2=5.00E-01 K3=0.0 # gg08_rnum=2
E1- + G0 -> G1-											RTYPE=0 K1=6.90E-15 K2=5.00E-01 K3=0.0 # gg08_rnum=3
C1+ + G1- -> C + G0										RTYPE=0 K1=4.90E-17 K2=5.00E-01 K3=0.0 # gg08_rnum=4
Fe1+ + G1- -> Fe + G0									RTYPE=0 K1=2.30E-17 K2=5.00E-01 K3=0.0 # gg08_rnum=5
H1+ + G1- -> H + G0										RTYPE=0 K1=1.70E-16 K2=5.00E-01 K3=0.0 # gg08_rnum=6
He1+ + G1- -> He + G0									RTYPE=0 K1=8.50E-17 K2=5.00E-01 K3=0.0 # gg08_rnum=7
Mg1+ + G1- -> Mg + G0									RTYPE=0 K1=3.70E-17 K2=5.00E-01 K3=0.0 # gg08_rnum=8
N1+ + G1- -> N + G0										RTYPE=0 K1=4.70E-17 K2=5.00E-01 K3=0.0 # gg08_rnum=9
Na1+ + G1- -> Na + G0									RTYPE=0 K1=3.60E-17 K2=5.00E-01 K3=0.0 # gg08_rnum=10
O1+ + G1- -> O + G0										RTYPE=0 K1=4.40E-17 K2=5.00E-01 K3=0.0 # gg08_rnum=11
S1+ + G1- -> S + G0										RTYPE=0 K1=3.00E-17 K2=5.00E-01 K3=0.0 # gg08_rnum=12
Si1+ + G1- -> Si + G0									RTYPE=0 K1=3.30E-17 K2=5.00E-01 K3=0.0 # gg08_rnum=13
H31+ + G1- -> H2 + H + G0								RTYPE=0 K1=1.00E-16 K2=5.00E-01 K3=0.0 # gg08_rnum=14
HCO1+ + G1- -> H + CO + G0								RTYPE=0 K1=3.10E-17 K2=5.00E-01 K3=0.0 # gg08_rnum=15
C -> C1+ + E1-											RTYPE=1 K1=1.02E+03 K2=0.0 K3=0.0 # gg08_rnum=16
HC2H -> HC2H1+ + E1-									RTYPE=1 K1=1.31E+03 K2=0.0 K3=0.0 # gg08_rnum=17
H2C2H2 -> H2C2H21+ + E1-								RTYPE=1 K1=7.80E+02 K2=0.0 K3=0.0 # gg08_rnum=18
CH3CH2OH -> CH3CH2OH1+ + E1-							RTYPE=1 K1=2.74E+03 K2=0.0 K3=0.0 # gg08_rnum=19
C3H4 -> C3H41+ + E1-									RTYPE=1 K1=5.30E+03 K2=0.0 K3=0.0 # gg08_rnum=20
HC4H -> HC4H1+ + E1-									RTYPE=1 K1=1.12E+03 K2=0.0 K3=0.0 # gg08_rnum=21
CH2 -> CH21+ + E1-										RTYPE=1 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=22
CH2CO -> CH2CO1+ + E1-									RTYPE=1 K1=1.22E+03 K2=0.0 K3=0.0 # gg08_rnum=23
CH3 -> CH31+ + E1-										RTYPE=1 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=24
CH3CHO -> CH3CHO1+ + E1-								RTYPE=1 K1=1.12E+03 K2=0.0 K3=0.0 # gg08_rnum=25
CH3CN -> CH3CN1+ + E1-									RTYPE=1 K1=2.24E+03 K2=0.0 K3=0.0 # gg08_rnum=26
CH3NH2 -> CH3NH21+ + E1-								RTYPE=1 K1=1.12E+03 K2=0.0 K3=0.0 # gg08_rnum=27
CH3OCH3 -> CH3OCH31+ + E1-								RTYPE=1 K1=1.12E+03 K2=0.0 K3=0.0 # gg08_rnum=28
CH3OH -> CH2OH1+ + H + E1-								RTYPE=1 K1=4.95E+01 K2=0.0 K3=0.0 # gg08_rnum=29
CH3OH -> CH3OH1+ + E1-									RTYPE=1 K1=1.44E+03 K2=0.0 K3=0.0 # gg08_rnum=30
CH3OH -> CH3O1+ + H + E1-								RTYPE=1 K1=4.95E+01 K2=0.0 K3=0.0 # gg08_rnum=31
Cl -> Cl1+ + E1-										RTYPE=1 K1=3.00E+03 K2=0.0 K3=0.0 # gg08_rnum=32
CO -> CO1+ + E1-										RTYPE=1 K1=3.00E+00 K2=0.0 K3=0.0 # gg08_rnum=33
Fe -> Fe1+ + E1-										RTYPE=1 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=34
H -> H1+ + E1-											RTYPE=1 K1=4.60E-01 K2=0.0 K3=0.0 # gg08_rnum=35
H2 -> H1+ + H + E1-										RTYPE=1 K1=2.20E-02 K2=0.0 K3=0.0 # gg08_rnum=36
H2 -> H21+ + E1-										RTYPE=1 K1=9.30E-01 K2=0.0 K3=0.0 # gg08_rnum=37
H2S -> H2S1+ + E1-										RTYPE=1 K1=1.70E+03 K2=0.0 K3=0.0 # gg08_rnum=38
HCO -> HCO1+ + E1-										RTYPE=1 K1=1.17E+03 K2=0.0 K3=0.0 # gg08_rnum=39
HCOOH -> HCOOH1+ + E1-									RTYPE=1 K1=6.50E+02 K2=0.0 K3=0.0 # gg08_rnum=40
HCS -> HCS1+ + E1-										RTYPE=1 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=41
He -> He1+ + E1-										RTYPE=1 K1=5.00E-01 K2=0.0 K3=0.0 # gg08_rnum=42
HNO -> HNO1+ + E1-										RTYPE=1 K1=1.00E+03 K2=0.0 K3=0.0 # gg08_rnum=43
Mg -> Mg1+ + E1-										RTYPE=1 K1=1.13E+02 K2=0.0 K3=0.0 # gg08_rnum=44
N -> N1+ + E1-											RTYPE=1 K1=2.10E+00 K2=0.0 K3=0.0 # gg08_rnum=45
Na -> Na1+ + E1-										RTYPE=1 K1=1.70E+01 K2=0.0 K3=0.0 # gg08_rnum=46
NH2 -> NH21+ + E1-										RTYPE=1 K1=6.50E+02 K2=0.0 K3=0.0 # gg08_rnum=47
NH3 -> NH31+ + E1-										RTYPE=1 K1=5.75E+02 K2=0.0 K3=0.0 # gg08_rnum=48
NO -> NO1+ + E1-										RTYPE=1 K1=4.94E+02 K2=0.0 K3=0.0 # gg08_rnum=49
O -> O1+ + E1-											RTYPE=1 K1=2.80E+00 K2=0.0 K3=0.0 # gg08_rnum=50
O2 -> O21+ + E1-										RTYPE=1 K1=1.17E+02 K2=0.0 K3=0.0 # gg08_rnum=51
OCS -> OCS1+ + E1-										RTYPE=1 K1=1.44E+03 K2=0.0 K3=0.0 # gg08_rnum=52
P -> P1+ + E1-											RTYPE=1 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=53
S -> S1+ + E1-											RTYPE=1 K1=9.60E+02 K2=0.0 K3=0.0 # gg08_rnum=54
Si -> Si1+ + E1-										RTYPE=1 K1=4.23E+03 K2=0.0 K3=0.0 # gg08_rnum=55
C10 -> C9 + C											RTYPE=1 K1=1.00E+03 K2=0.0 K3=0.0 # gg08_rnum=56
C2 -> C + C												RTYPE=1 K1=2.37E+02 K2=0.0 K3=0.0 # gg08_rnum=57
C2H -> C2 + H											RTYPE=1 K1=5.00E+03 K2=0.0 K3=0.0 # gg08_rnum=58
HC2H -> C2H + H											RTYPE=1 K1=5.15E+03 K2=0.0 K3=0.0 # gg08_rnum=59
H2C2H -> HC2H + H										RTYPE=1 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=60
H2C2H2 -> HC2H + H2										RTYPE=1 K1=3.70E+03 K2=0.0 K3=0.0 # gg08_rnum=61
CH3CH2 -> H2C2H2 + H									RTYPE=1 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=62
CH3CH2CHO -> CH3CH2 + HCO								RTYPE=1 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=63
CH3CH2OH -> CH3CH2 + OH									RTYPE=1 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=64
CH3CH2OH -> CH3 + CH2OH									RTYPE=1 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=65
CH3CH3 -> H2C2H2 + H2									RTYPE=1 K1=3.00E+03 K2=0.0 K3=0.0 # gg08_rnum=66
C2N -> C + CN											RTYPE=1 K1=1.00E+03 K2=0.0 K3=0.0 # gg08_rnum=67
C2O -> CO + C											RTYPE=1 K1=7.50E+02 K2=0.0 K3=0.0 # gg08_rnum=68
C2O -> C2 + O											RTYPE=1 K1=7.50E+02 K2=0.0 K3=0.0 # gg08_rnum=69
C2S -> CS + C											RTYPE=1 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=70
C3 -> C2 + C											RTYPE=1 K1=1.12E+03 K2=0.0 K3=0.0 # gg08_rnum=71
C3H -> C3 + H											RTYPE=1 K1=1.00E+04 K2=0.0 K3=0.0 # gg08_rnum=72
C3H2 -> C3H + H											RTYPE=1 K1=1.00E+04 K2=0.0 K3=0.0 # gg08_rnum=73
C3H3 -> C3H2 + H										RTYPE=1 K1=1.00E+04 K2=0.0 K3=0.0 # gg08_rnum=74
C3H3N -> H2C2H + CN										RTYPE=1 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=75
C3H4 -> C3H3 + H										RTYPE=1 K1=3.28E+03 K2=0.0 K3=0.0 # gg08_rnum=76
C3N -> C2 + CN											RTYPE=1 K1=1.75E+03 K2=0.0 K3=0.0 # gg08_rnum=77
C3O -> C2 + CO											RTYPE=1 K1=6.60E+03 K2=0.0 K3=0.0 # gg08_rnum=78
C3P -> CCP + C											RTYPE=1 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=79
C3S -> C2 + CS											RTYPE=1 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=80
C4 -> C3 + C											RTYPE=1 K1=1.00E+03 K2=0.0 K3=0.0 # gg08_rnum=81
C4H -> C4 + H											RTYPE=1 K1=5.00E+03 K2=0.0 K3=0.0 # gg08_rnum=82
HC4H -> C2H + C2H										RTYPE=1 K1=1.73E+03 K2=0.0 K3=0.0 # gg08_rnum=83
HC4H -> C4H + H											RTYPE=1 K1=1.73E+03 K2=0.0 K3=0.0 # gg08_rnum=84
C4H3 -> HC4H + H										RTYPE=1 K1=1.00E+04 K2=0.0 K3=0.0 # gg08_rnum=85
C4H4 -> HC4H + H2										RTYPE=1 K1=7.50E+03 K2=0.0 K3=0.0 # gg08_rnum=86
C4N -> C3 + CN											RTYPE=1 K1=1.00E+03 K2=0.0 K3=0.0 # gg08_rnum=87
C4P -> C3P + C											RTYPE=1 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=88
C4S -> C3 + CS											RTYPE=1 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=89
C5 -> C4 + C											RTYPE=1 K1=1.00E+03 K2=0.0 K3=0.0 # gg08_rnum=90
C5H -> C5 + H											RTYPE=1 K1=5.00E+03 K2=0.0 K3=0.0 # gg08_rnum=91
C5H2 -> C5H + H											RTYPE=1 K1=1.75E+03 K2=0.0 K3=0.0 # gg08_rnum=92
C5H3 -> C5H2 + H										RTYPE=1 K1=1.00E+04 K2=0.0 K3=0.0 # gg08_rnum=93
C5H4 -> C5H2 + H2										RTYPE=1 K1=7.50E+03 K2=0.0 K3=0.0 # gg08_rnum=94
C5N -> C4 + CN											RTYPE=1 K1=1.75E+03 K2=0.0 K3=0.0 # gg08_rnum=95
C6 -> C5 + C											RTYPE=1 K1=1.00E+03 K2=0.0 K3=0.0 # gg08_rnum=96
C6H -> C6 + H											RTYPE=1 K1=5.00E+03 K2=0.0 K3=0.0 # gg08_rnum=97
C6H2 -> C6H + H											RTYPE=1 K1=1.75E+03 K2=0.0 K3=0.0 # gg08_rnum=98
C6H3 -> C6H2 + H										RTYPE=1 K1=1.00E+04 K2=0.0 K3=0.0 # gg08_rnum=99
C6H4 -> C6H2 + H2										RTYPE=1 K1=7.50E+03 K2=0.0 K3=0.0 # gg08_rnum=100
C6H6 -> C6H4 + H2										RTYPE=1 K1=3.00E+03 K2=0.0 K3=0.0 # gg08_rnum=101
C7 -> C6 + C											RTYPE=1 K1=1.00E+03 K2=0.0 K3=0.0 # gg08_rnum=102
C7H -> C7 + H											RTYPE=1 K1=5.00E+03 K2=0.0 K3=0.0 # gg08_rnum=103
C7H2 -> C7H + H											RTYPE=1 K1=1.75E+03 K2=0.0 K3=0.0 # gg08_rnum=104
C7H3 -> C7H2 + H										RTYPE=1 K1=1.00E+04 K2=0.0 K3=0.0 # gg08_rnum=105
C7H4 -> C7H2 + H2										RTYPE=1 K1=7.50E+03 K2=0.0 K3=0.0 # gg08_rnum=106
C7N -> C6 + CN											RTYPE=1 K1=1.75E+03 K2=0.0 K3=0.0 # gg08_rnum=107
C8 -> C7 + C											RTYPE=1 K1=1.00E+03 K2=0.0 K3=0.0 # gg08_rnum=108
C8H -> C8 + H											RTYPE=1 K1=5.00E+03 K2=0.0 K3=0.0 # gg08_rnum=109
C8H2 -> C8H + H											RTYPE=1 K1=1.75E+03 K2=0.0 K3=0.0 # gg08_rnum=110
C8H3 -> C8H2 + H										RTYPE=1 K1=1.00E+04 K2=0.0 K3=0.0 # gg08_rnum=111
C8H4 -> C8H2 + H2										RTYPE=1 K1=7.50E+03 K2=0.0 K3=0.0 # gg08_rnum=112
C9 -> C8 + C											RTYPE=1 K1=1.00E+03 K2=0.0 K3=0.0 # gg08_rnum=113
C9H -> C9 + H											RTYPE=1 K1=5.00E+03 K2=0.0 K3=0.0 # gg08_rnum=114
C9H2 -> C9H + H											RTYPE=1 K1=1.75E+03 K2=0.0 K3=0.0 # gg08_rnum=115
C9H3 -> C9H2 + H										RTYPE=1 K1=1.00E+04 K2=0.0 K3=0.0 # gg08_rnum=116
C9H4 -> C9H2 + H2										RTYPE=1 K1=7.50E+03 K2=0.0 K3=0.0 # gg08_rnum=117
C9N -> C8 + CN											RTYPE=1 K1=1.75E+03 K2=0.0 K3=0.0 # gg08_rnum=118
ClC -> C + Cl											RTYPE=1 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=119
CCP -> C2 + P											RTYPE=1 K1=7.50E+02 K2=0.0 K3=0.0 # gg08_rnum=120
CCP -> CP + C											RTYPE=1 K1=7.50E+02 K2=0.0 K3=0.0 # gg08_rnum=121
CH -> C + H												RTYPE=1 K1=7.30E+02 K2=0.0 K3=0.0 # gg08_rnum=122
CH1+ -> C + H1+											RTYPE=1 K1=1.76E+02 K2=0.0 K3=0.0 # gg08_rnum=123
CH2 -> CH + H											RTYPE=1 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=124
H2C2N -> CH2 + CN										RTYPE=1 K1=5.00E+03 K2=0.0 K3=0.0 # gg08_rnum=125
CH2CO -> CH2 + CO										RTYPE=1 K1=9.15E+02 K2=0.0 K3=0.0 # gg08_rnum=126
CH2NH -> CH2 + NH										RTYPE=1 K1=4.98E+03 K2=0.0 K3=0.0 # gg08_rnum=127
CH2NH2 -> CH2 + NH2										RTYPE=1 K1=9.50E+03 K2=0.0 K3=0.0 # gg08_rnum=128
CH2OH -> CH2 + OH										RTYPE=1 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=129
CH2PH -> CH2 + PH										RTYPE=1 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=130
CH3 -> CH2 + H											RTYPE=1 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=131
CH3C3N -> CH3 + C3N										RTYPE=1 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=132
CH3C4H -> CH3 + C4H										RTYPE=1 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=133
CH3C5N -> CH3 + C5N										RTYPE=1 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=134
CH3C6H -> CH3 + C6H										RTYPE=1 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=135
CH3C7N -> CH3 + C7N										RTYPE=1 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=136
CH3CHO -> CH3 + HCO										RTYPE=1 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=137
CH3CN -> CH3 + CN										RTYPE=1 K1=4.76E+03 K2=0.0 K3=0.0 # gg08_rnum=138
CH3CO -> CH3 + CO										RTYPE=1 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=139
CH3NH -> CH2NH + H										RTYPE=1 K1=9.50E+03 K2=0.0 K3=0.0 # gg08_rnum=140
CH3NH -> CH3 + NH										RTYPE=1 K1=1.00E+04 K2=0.0 K3=0.0 # gg08_rnum=141
CH3NH2 -> CH3 + NH2										RTYPE=1 K1=1.00E+04 K2=0.0 K3=0.0 # gg08_rnum=142
CH3O -> CH3 + O											RTYPE=1 K1=3.00E+03 K2=0.0 K3=0.0 # gg08_rnum=143
CH3OCH3 -> CH3O + CH3									RTYPE=1 K1=1.72E+03 K2=0.0 K3=0.0 # gg08_rnum=144
CH3OH -> CH3 + OH										RTYPE=1 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=145
CH3OH -> CH2OH + H										RTYPE=1 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=146
CH3OH -> CH3O + H										RTYPE=1 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=147
CH4 -> CH2 + H2											RTYPE=1 K1=2.34E+03 K2=0.0 K3=0.0 # gg08_rnum=148
HCNH -> CH + NH											RTYPE=1 K1=1.00E+03 K2=0.0 K3=0.0 # gg08_rnum=149
HCOH -> CH + OH											RTYPE=1 K1=0.0 K2=0.0 K3=0.0 # gg08_rnum=150
ClO -> Cl + O											RTYPE=1 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=151
CN -> C + N												RTYPE=1 K1=1.06E+04 K2=0.0 K3=0.0 # gg08_rnum=152
CO -> C + O												RTYPE=1 K1=5.00E+00 K2=0.0 K3=0.0 # gg08_rnum=153
CO2 -> CO + O											RTYPE=1 K1=1.71E+03 K2=0.0 K3=0.0 # gg08_rnum=154
COCHO -> HCO + CO										RTYPE=1 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=155
COOH -> CO + OH											RTYPE=1 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=156
CP -> C + P												RTYPE=1 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=157
CS -> C + S												RTYPE=1 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=158
FeH -> Fe + H											RTYPE=1 K1=1.00E+03 K2=0.0 K3=0.0 # gg08_rnum=159
H2 -> H + H												RTYPE=1 K1=1.00E-01 K2=0.0 K3=0.0 # gg08_rnum=160
H2 -> H1+ + H1-											RTYPE=1 K1=3.00E-04 K2=0.0 K3=0.0 # gg08_rnum=161
HC3NH -> HC2H + CN										RTYPE=1 K1=3.00E+03 K2=0.0 K3=0.0 # gg08_rnum=162
cH2C3O -> HC2H + CO										RTYPE=1 K1=1.80E+03 K2=0.0 K3=0.0 # gg08_rnum=163
H2C5N -> HC4H + CN										RTYPE=1 K1=3.00E+03 K2=0.0 K3=0.0 # gg08_rnum=164
H2C7N -> C6H2 + CN										RTYPE=1 K1=3.00E+03 K2=0.0 K3=0.0 # gg08_rnum=165
H2C9N -> C8H2 + CN										RTYPE=1 K1=3.00E+03 K2=0.0 K3=0.0 # gg08_rnum=166
H2CN -> HCN + H											RTYPE=1 K1=1.00E+03 K2=0.0 K3=0.0 # gg08_rnum=167
H2CO -> HCO + H											RTYPE=1 K1=1.33E+03 K2=0.0 K3=0.0 # gg08_rnum=168
H2CO -> CO + H2											RTYPE=1 K1=1.33E+03 K2=0.0 K3=0.0 # gg08_rnum=169
H2CS -> HCS + H											RTYPE=1 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=170
H2O -> OH + H											RTYPE=1 K1=9.70E+02 K2=0.0 K3=0.0 # gg08_rnum=171
H2O2 -> OH + OH											RTYPE=1 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=172
H2S -> H2 + S											RTYPE=1 K1=5.15E+03 K2=0.0 K3=0.0 # gg08_rnum=173
H2S2 -> HS + HS											RTYPE=1 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=174
H2SiO -> SiO + H2										RTYPE=1 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=175
H3C5N -> H2C2H + C3N									RTYPE=1 K1=3.00E+03 K2=0.0 K3=0.0 # gg08_rnum=176
H3C7N -> H2C2H + C5N									RTYPE=1 K1=3.00E+03 K2=0.0 K3=0.0 # gg08_rnum=177
H3C9N -> H2C2H + C7N									RTYPE=1 K1=3.00E+03 K2=0.0 K3=0.0 # gg08_rnum=178
H4C3N -> H2C2H2 + CN									RTYPE=1 K1=3.00E+03 K2=0.0 K3=0.0 # gg08_rnum=179
H5C3N -> CH3CH2 + CN									RTYPE=1 K1=3.00E+03 K2=0.0 K3=0.0 # gg08_rnum=180
HC2NC -> C2H + CN										RTYPE=1 K1=3.45E+03 K2=0.0 K3=0.0 # gg08_rnum=181
HC2O -> CO + CH											RTYPE=1 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=182
HC3N -> C2H + CN										RTYPE=1 K1=1.72E+03 K2=0.0 K3=0.0 # gg08_rnum=183
HC3O -> CO + C2H										RTYPE=1 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=184
HC5N -> C4H + CN										RTYPE=1 K1=1.75E+03 K2=0.0 K3=0.0 # gg08_rnum=185
HC7N -> C6H + CN										RTYPE=1 K1=1.75E+03 K2=0.0 K3=0.0 # gg08_rnum=186
HC9N -> C8H + CN										RTYPE=1 K1=1.75E+03 K2=0.0 K3=0.0 # gg08_rnum=187
HC2N -> C2N + H											RTYPE=1 K1=1.00E+04 K2=0.0 K3=0.0 # gg08_rnum=188
HCCP -> CCP + H											RTYPE=1 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=189
ClH -> H + Cl											RTYPE=1 K1=6.10E+02 K2=0.0 K3=0.0 # gg08_rnum=190
HCN -> CN + H											RTYPE=1 K1=3.12E+03 K2=0.0 K3=0.0 # gg08_rnum=191
C2NCH -> C2H + CN										RTYPE=1 K1=3.45E+03 K2=0.0 K3=0.0 # gg08_rnum=192
HCO -> CO + H											RTYPE=1 K1=4.21E+02 K2=0.0 K3=0.0 # gg08_rnum=193
HCOOCH3 -> HCO + CH3O									RTYPE=1 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=194
HCOOH -> HCO + OH										RTYPE=1 K1=2.49E+02 K2=0.0 K3=0.0 # gg08_rnum=195
HCP -> CP + H											RTYPE=1 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=196
HCS -> CH + S											RTYPE=1 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=197
HCS -> CS + H											RTYPE=1 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=198
SiCH -> CH + Si											RTYPE=1 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=199
CH3COCHO -> CH3CO + HCO									RTYPE=1 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=200
CH3COCH3 -> CH3CO + CH3									RTYPE=1 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=201
CH3CONH -> CH3CO + NH									RTYPE=1 K1=1.00E+04 K2=0.0 K3=0.0 # gg08_rnum=202
CH3CONH -> CH3 + HNCO									RTYPE=1 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=203
CH3COOH -> CH3 + COOH									RTYPE=1 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=204
CH3COOH -> CH3CO + OH									RTYPE=1 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=205
CH3CONH2 -> CH3 + NH2CO									RTYPE=1 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=206
CH3CONH2 -> CH3CO + NH2									RTYPE=1 K1=1.00E+04 K2=0.0 K3=0.0 # gg08_rnum=207
CH3COOCH3 -> CH3CO + CH3O								RTYPE=1 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=208
CH3COOCH3 -> CH3 + CH3OCO								RTYPE=1 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=209
CH3COCH2OH -> CH3 + HOCH2CO								RTYPE=1 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=210
CH3COCH2OH -> CH3CO + CH2OH								RTYPE=1 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=211
HNC -> CN + H											RTYPE=1 K1=3.00E+03 K2=0.0 K3=0.0 # gg08_rnum=212
HNC3 -> C2H + CN										RTYPE=1 K1=3.45E+03 K2=0.0 K3=0.0 # gg08_rnum=213
HNCHO -> NH + HCO										RTYPE=1 K1=1.00E+04 K2=0.0 K3=0.0 # gg08_rnum=214
HNCO -> NH + CO											RTYPE=1 K1=6.00E+03 K2=0.0 K3=0.0 # gg08_rnum=215
HNCOCHO -> HNCO + HCO									RTYPE=1 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=216
HNCOCHO -> NH + COCHO									RTYPE=1 K1=1.00E+04 K2=0.0 K3=0.0 # gg08_rnum=217
HNCONH -> NH + HNCO										RTYPE=1 K1=1.00E+04 K2=0.0 K3=0.0 # gg08_rnum=218
HNCOOH -> COOH + NH										RTYPE=1 K1=1.00E+04 K2=0.0 K3=0.0 # gg08_rnum=219
HNCOOH -> OH + HNCO										RTYPE=1 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=220
HNO -> NH + O											RTYPE=1 K1=1.00E+03 K2=0.0 K3=0.0 # gg08_rnum=221
HNO -> NO + H											RTYPE=1 K1=1.00E+03 K2=0.0 K3=0.0 # gg08_rnum=222
HNOH -> NH + OH											RTYPE=1 K1=1.00E+03 K2=0.0 K3=0.0 # gg08_rnum=223
HNSi -> SiN + H											RTYPE=1 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=224
CH3ONH -> NH + CH3O										RTYPE=1 K1=1.00E+03 K2=0.0 K3=0.0 # gg08_rnum=225
HNCH2OH -> NH + CH2OH									RTYPE=1 K1=1.00E+04 K2=0.0 K3=0.0 # gg08_rnum=226
HOC -> CO + H											RTYPE=1 K1=0.0 K2=0.0 K3=0.0 # gg08_rnum=227
HOCOOH -> OH + COOH										RTYPE=1 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=228
HPO -> PO + H											RTYPE=1 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=229
HS -> H + S												RTYPE=1 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=230
S2H -> HS + S											RTYPE=1 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=231
HCOCOCHO -> HCO + COCHO									RTYPE=1 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=232
MgH -> Mg + H											RTYPE=1 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=233
MgH2 -> MgH + H											RTYPE=1 K1=3.00E+03 K2=0.0 K3=0.0 # gg08_rnum=234
N2 -> N + N												RTYPE=1 K1=5.00E+00 K2=0.0 K3=0.0 # gg08_rnum=235
N2H2 -> NH + NH											RTYPE=1 K1=2.00E+02 K2=0.0 K3=0.0 # gg08_rnum=236
N2O -> NO + N											RTYPE=1 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=237
NaH -> Na + H											RTYPE=1 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=238
NaOH -> Na + OH											RTYPE=1 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=239
NH -> N + H												RTYPE=1 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=240
NH2 -> NH + H											RTYPE=1 K1=8.00E+01 K2=0.0 K3=0.0 # gg08_rnum=241
NH2CHO -> NH2 + HCO										RTYPE=1 K1=3.00E+03 K2=0.0 K3=0.0 # gg08_rnum=242
NH2CN -> NH2 + CN										RTYPE=1 K1=9.50E+03 K2=0.0 K3=0.0 # gg08_rnum=243
NH2OH -> NH2 + OH										RTYPE=1 K1=3.00E+03 K2=0.0 K3=0.0 # gg08_rnum=244
NH3 -> NH + H2											RTYPE=1 K1=5.40E+02 K2=0.0 K3=0.0 # gg08_rnum=245
NH3 -> NH2 + H											RTYPE=1 K1=1.32E+03 K2=0.0 K3=0.0 # gg08_rnum=246
NO -> N + O												RTYPE=1 K1=4.82E+02 K2=0.0 K3=0.0 # gg08_rnum=247
NO2 -> NO + O											RTYPE=1 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=248
NS -> N + S												RTYPE=1 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=249
O2 -> O + O												RTYPE=1 K1=7.50E+02 K2=0.0 K3=0.0 # gg08_rnum=250
HO2 -> O + OH											RTYPE=1 K1=7.50E+02 K2=0.0 K3=0.0 # gg08_rnum=251
HO2 -> O2 + H											RTYPE=1 K1=7.50E+02 K2=0.0 K3=0.0 # gg08_rnum=252
O3 -> O2 + O											RTYPE=1 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=253
OCN -> CN + O											RTYPE=1 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=254
OCS -> CO + S											RTYPE=1 K1=5.35E+03 K2=0.0 K3=0.0 # gg08_rnum=255
OH -> O + H												RTYPE=1 K1=5.10E+02 K2=0.0 K3=0.0 # gg08_rnum=256
CHOCHO -> HCO + HCO										RTYPE=1 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=257
HCOCOOH -> HCO + COOH									RTYPE=1 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=258
HCOCOOH -> COCHO + OH									RTYPE=1 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=259
PH -> P + H												RTYPE=1 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=260
PH2 -> PH + H											RTYPE=1 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=261
PN -> P + N												RTYPE=1 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=262
PO -> P + O												RTYPE=1 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=263
NH2CO -> NH2 + CO										RTYPE=1 K1=1.00E+04 K2=0.0 K3=0.0 # gg08_rnum=264
NH2COCHO -> NH2 + COCHO									RTYPE=1 K1=1.00E+04 K2=0.0 K3=0.0 # gg08_rnum=265
NH2COCHO -> NH2CO + HCO									RTYPE=1 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=266
NH2CONH -> NH2 + HNCO									RTYPE=1 K1=1.00E+04 K2=0.0 K3=0.0 # gg08_rnum=267
NH2CONH -> NH2CO + NH									RTYPE=1 K1=1.00E+04 K2=0.0 K3=0.0 # gg08_rnum=268
NH2COOH -> NH2CO + OH									RTYPE=1 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=269
NH2COOH -> NH2 + COOH									RTYPE=1 K1=1.00E+04 K2=0.0 K3=0.0 # gg08_rnum=270
NH2CONH2 -> NH2 + NH2CO									RTYPE=1 K1=1.00E+04 K2=0.0 K3=0.0 # gg08_rnum=271
NH2NH -> NH + NH2										RTYPE=1 K1=2.00E+02 K2=0.0 K3=0.0 # gg08_rnum=272
NH2NH2 -> NH2 + NH2										RTYPE=1 K1=2.00E+02 K2=0.0 K3=0.0 # gg08_rnum=273
NH2OCH3 -> NH2 + CH3O									RTYPE=1 K1=1.00E+03 K2=0.0 K3=0.0 # gg08_rnum=274
NH2CH2OH -> NH2 + CH2OH									RTYPE=1 K1=1.00E+04 K2=0.0 K3=0.0 # gg08_rnum=275
S2 -> S + S												RTYPE=1 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=276
SiC -> Si + C											RTYPE=1 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=277
SiC2 -> SiC + C											RTYPE=1 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=278
SiC2H -> SiC2 + H										RTYPE=1 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=279
SiC2H2 -> SiC2 + H2										RTYPE=1 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=280
cSiC3 -> SiC2 + C										RTYPE=1 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=281
SiC3H -> cSiC3 + H										RTYPE=1 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=282
SiC4 -> SiC2 + C2										RTYPE=1 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=283
SiCH2 -> SiC + H2										RTYPE=1 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=284
SiCH3 -> SiCH2 + H										RTYPE=1 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=285
SiH -> Si + H											RTYPE=1 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=286
SiH2 -> SiH + H											RTYPE=1 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=287
SiH3 -> SiH2 + H										RTYPE=1 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=288
SiH4 -> SiH2 + H2										RTYPE=1 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=289
SiN -> Si + N											RTYPE=1 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=290
SiNC -> Si + CN											RTYPE=1 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=291
SiO -> Si + O											RTYPE=1 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=292
SiO2 -> SiO + O											RTYPE=1 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=293
SiS -> Si + S											RTYPE=1 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=294
SO -> S + O												RTYPE=1 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=295
SO2 -> SO + O											RTYPE=1 K1=1.88E+03 K2=0.0 K3=0.0 # gg08_rnum=296
CH3OCO -> CH3O + CO										RTYPE=1 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=297
CH3OCOCHO -> CH3O + COCHO								RTYPE=1 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=298
CH3OCOCHO -> CH3OCO + HCO								RTYPE=1 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=299
CH3OCONH -> CH3O + HNCO									RTYPE=1 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=300
CH3OCONH -> CH3OCO + NH									RTYPE=1 K1=1.00E+04 K2=0.0 K3=0.0 # gg08_rnum=301
CH3OCOOH -> CH3O + COOH									RTYPE=1 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=302
CH3OCOOH -> CH3OCO + OH									RTYPE=1 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=303
CH3OCONH2 -> NH2 + CH3OCO								RTYPE=1 K1=1.00E+04 K2=0.0 K3=0.0 # gg08_rnum=304
CH3OCONH2 -> NH2CO + CH3O								RTYPE=1 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=305
CH3OCOOCH3 -> CH3O + CH3OCO								RTYPE=1 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=306
CH3OCOCH2OH -> CH3O + HOCH2CO							RTYPE=1 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=307
CH3OCOCH2OH -> CH3OCO + CH2OH							RTYPE=1 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=308
CH3OOH -> OH + CH3O										RTYPE=1 K1=1.00E+03 K2=0.0 K3=0.0 # gg08_rnum=309
CH3OOCH3 -> CH3O + CH3O									RTYPE=1 K1=1.00E+03 K2=0.0 K3=0.0 # gg08_rnum=310
CH3OCH2OH -> CH3O + CH2OH								RTYPE=1 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=311
CH2OHCHO -> CH2OH + HCO									RTYPE=1 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=312
HOCH2CO -> CH2OH + CO									RTYPE=1 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=313
HOCH2COCHO -> HOCH2CO + HCO								RTYPE=1 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=314
HOCH2COCHO -> CH2OH + COCHO								RTYPE=1 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=315
HNCOCH2OH -> HOCH2CO + NH								RTYPE=1 K1=1.00E+04 K2=0.0 K3=0.0 # gg08_rnum=316
HNCOCH2OH -> CH2OH + HNCO								RTYPE=1 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=317
CH2OHCOOH -> HOCH2CO + OH								RTYPE=1 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=318
CH2OHCOOH -> CH2OH + COOH								RTYPE=1 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=319
NH2COCH2OH -> NH2 + HOCH2CO								RTYPE=1 K1=1.00E+04 K2=0.0 K3=0.0 # gg08_rnum=320
NH2COCH2OH -> NH2CO + CH2OH								RTYPE=1 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=321
HOCH2COCH2OH -> CH2OH + HOCH2CO							RTYPE=1 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=322
HOCH2OH -> OH + CH2OH									RTYPE=1 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=323
HOCH2CH2OH -> CH2OH + CH2OH								RTYPE=1 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=324
C1+ + C2H -> C31+ + H									RTYPE=2 K1=2.60E-09 K2=-0.5 K3=0.0 # gg08_rnum=325
C1+ + HC2H -> C3H1+ + H									RTYPE=2 K1=2.70E-09 K2=0.0 K3=0.0 # gg08_rnum=326
C1+ + H2C2H -> C3H1+ + H2								RTYPE=2 K1=6.70E-10 K2=-0.5 K3=0.0 # gg08_rnum=327
C1+ + H2C2H -> H2C2H1+ + C								RTYPE=2 K1=6.70E-10 K2=-0.5 K3=0.0 # gg08_rnum=328
C1+ + H2C2H -> C3H21+ + H								RTYPE=2 K1=6.70E-10 K2=-0.5 K3=0.0 # gg08_rnum=329
C1+ + H2C2H2 -> C3H1+ + H2 + H							RTYPE=2 K1=8.50E-11 K2=0.0 K3=0.0 # gg08_rnum=330
C1+ + H2C2H2 -> H2C2H1+ + CH							RTYPE=2 K1=8.50E-11 K2=0.0 K3=0.0 # gg08_rnum=331
C1+ + H2C2H2 -> C3H21+ + H2								RTYPE=2 K1=5.10E-10 K2=0.0 K3=0.0 # gg08_rnum=332
C1+ + H2C2H2 -> H2C2H21+ + C							RTYPE=2 K1=1.70E-10 K2=0.0 K3=0.0 # gg08_rnum=333
C1+ + H2C2H2 -> C3H31+ + H								RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=334
C1+ + CH3CH2 -> C3H31+ + H2								RTYPE=2 K1=6.65E-10 K2=-0.5 K3=0.0 # gg08_rnum=335
C1+ + CH3CH2 -> CH3CH21+ + C							RTYPE=2 K1=6.65E-10 K2=-0.5 K3=0.0 # gg08_rnum=336
C1+ + CH3CH2CHO -> CH3CH2CHO1+ + C						RTYPE=2 K1=3.44E-09 K2=-0.5 K3=0.0 # gg08_rnum=337
C1+ + CH3CH2OH -> CH3CH2O1+ + CH						RTYPE=2 K1=7.07E-10 K2=-0.5 K3=0.0 # gg08_rnum=338
C1+ + CH3CH2OH -> CH3CH2OH1+ + C						RTYPE=2 K1=7.07E-10 K2=-0.5 K3=0.0 # gg08_rnum=339
C1+ + CH3CH2OH -> CH2OH1+ + H2C2H						RTYPE=2 K1=7.07E-10 K2=-0.5 K3=0.0 # gg08_rnum=340
C1+ + CH3CH3 -> H2C2H1+ + CH3							RTYPE=2 K1=5.10E-10 K2=0.0 K3=0.0 # gg08_rnum=341
C1+ + CH3CH3 -> CH3CH21+ + CH							RTYPE=2 K1=1.70E-10 K2=0.0 K3=0.0 # gg08_rnum=342
C1+ + CH3CH3 -> C3H31+ + H2 + H							RTYPE=2 K1=8.50E-10 K2=0.0 K3=0.0 # gg08_rnum=343
C1+ + CH3CH3 -> HC2H1+ + CH4							RTYPE=2 K1=1.70E-10 K2=0.0 K3=0.0 # gg08_rnum=344
C1+ + C2O -> C2O1+ + C									RTYPE=2 K1=3.93E-09 K2=-0.5 K3=0.0 # gg08_rnum=345
C1+ + C2S -> C2S1+ + C									RTYPE=2 K1=1.72E-09 K2=-0.5 K3=0.0 # gg08_rnum=346
C1+ + C2S -> C31+ + S									RTYPE=2 K1=1.72E-09 K2=-0.5 K3=0.0 # gg08_rnum=347
C1+ + C3H -> C41+ + H									RTYPE=2 K1=9.50E-09 K2=-0.5 K3=0.0 # gg08_rnum=348
C1+ + C3H2 -> C4H1+ + H									RTYPE=2 K1=2.10E-09 K2=-0.5 K3=0.0 # gg08_rnum=349
C1+ + C3H2 -> C41+ + H2									RTYPE=2 K1=2.10E-09 K2=-0.5 K3=0.0 # gg08_rnum=350
C1+ + C3H3 -> C4H1+ + H2								RTYPE=2 K1=1.70E-09 K2=-0.5 K3=0.0 # gg08_rnum=351
C1+ + C3H3 -> C3H31+ + C								RTYPE=2 K1=1.70E-09 K2=-0.5 K3=0.0 # gg08_rnum=352
C1+ + C3H3 -> HC4H1+ + H								RTYPE=2 K1=1.70E-09 K2=-0.5 K3=0.0 # gg08_rnum=353
C1+ + C3H3N -> C3H3N1+ + C								RTYPE=2 K1=4.80E-09 K2=-0.5 K3=0.0 # gg08_rnum=354
C1+ + C3H4 -> HC4H1+ + H2								RTYPE=2 K1=3.00E-10 K2=-0.5 K3=0.0 # gg08_rnum=355
C1+ + C3H4 -> C3H41+ + C								RTYPE=2 K1=3.00E-10 K2=-0.5 K3=0.0 # gg08_rnum=356
C1+ + C3H4 -> HC2H1+ + HC2H								RTYPE=2 K1=1.00E-10 K2=-0.5 K3=0.0 # gg08_rnum=357
C1+ + C3H4 -> H2C2H1+ + C2H								RTYPE=2 K1=1.00E-10 K2=-0.5 K3=0.0 # gg08_rnum=358
C1+ + C3H4 -> C3H31+ + CH								RTYPE=2 K1=2.00E-10 K2=-0.5 K3=0.0 # gg08_rnum=359
C1+ + C3O -> C3O1+ + C									RTYPE=2 K1=2.34E-09 K2=-0.5 K3=0.0 # gg08_rnum=360
C1+ + C3O -> C41+ + O									RTYPE=2 K1=2.34E-09 K2=-0.5 K3=0.0 # gg08_rnum=361
C1+ + C3O -> C31+ + CO									RTYPE=2 K1=2.34E-09 K2=-0.5 K3=0.0 # gg08_rnum=362
C1+ + C3P -> C41+ + P									RTYPE=2 K1=1.22E-09 K2=-0.5 K3=0.0 # gg08_rnum=363
C1+ + C3S -> C41+ + S									RTYPE=2 K1=2.36E-09 K2=-0.5 K3=0.0 # gg08_rnum=364
C1+ + C3S -> C3S1+ + C									RTYPE=2 K1=2.36E-09 K2=-0.5 K3=0.0 # gg08_rnum=365
C1+ + C4H -> C51+ + H									RTYPE=2 K1=2.70E-09 K2=-0.5 K3=0.0 # gg08_rnum=366
C1+ + HC4H -> HC4H1+ + C								RTYPE=2 K1=1.40E-09 K2=0.0 K3=0.0 # gg08_rnum=367
C1+ + HC4H -> C5H1+ + H									RTYPE=2 K1=1.50E-09 K2=0.0 K3=0.0 # gg08_rnum=368
C1+ + HC4H -> C3H1+ + C2H								RTYPE=2 K1=1.45E-10 K2=0.0 K3=0.0 # gg08_rnum=369
C1+ + HC4H -> C51+ + H2									RTYPE=2 K1=2.00E-09 K2=0.0 K3=0.0 # gg08_rnum=370
C1+ + C4H3 -> C5H21+ + H								RTYPE=2 K1=3.10E-10 K2=-0.5 K3=0.0 # gg08_rnum=371
C1+ + C4H3 -> C5H1+ + H2								RTYPE=2 K1=3.10E-10 K2=-0.5 K3=0.0 # gg08_rnum=372
C1+ + C4H3 -> C4H31+ + C								RTYPE=2 K1=3.10E-10 K2=-0.5 K3=0.0 # gg08_rnum=373
C1+ + C4H3 -> C3H21+ + C2H								RTYPE=2 K1=3.10E-10 K2=-0.5 K3=0.0 # gg08_rnum=374
C1+ + C4H4 -> C5H31+ + H								RTYPE=2 K1=2.48E-10 K2=-0.5 K3=0.0 # gg08_rnum=375
C1+ + C4H4 -> C4H41+ + C								RTYPE=2 K1=2.48E-10 K2=-0.5 K3=0.0 # gg08_rnum=376
C1+ + C4P -> C51+ + P									RTYPE=2 K1=6.01E-10 K2=-0.5 K3=0.0 # gg08_rnum=377
C1+ + C4S -> C4S1+ + C									RTYPE=2 K1=1.80E-09 K2=-0.5 K3=0.0 # gg08_rnum=378
C1+ + C4S -> C51+ + S									RTYPE=2 K1=1.80E-09 K2=-0.5 K3=0.0 # gg08_rnum=379
C1+ + C5H -> C61+ + H									RTYPE=2 K1=1.25E-08 K2=-0.5 K3=0.0 # gg08_rnum=380
C1+ + C5H2 -> C61+ + H2									RTYPE=2 K1=1.53E-09 K2=-0.5 K3=0.0 # gg08_rnum=381
C1+ + C5H2 -> C6H1+ + H									RTYPE=2 K1=1.53E-09 K2=-0.5 K3=0.0 # gg08_rnum=382
C1+ + C5H4 -> C5H41+ + C								RTYPE=2 K1=4.74E-10 K2=-0.5 K3=0.0 # gg08_rnum=383
C1+ + C5H4 -> C6H31+ + H								RTYPE=2 K1=4.74E-10 K2=-0.5 K3=0.0 # gg08_rnum=384
C1+ + C6H -> C71+ + H									RTYPE=2 K1=1.43E-08 K2=-0.5 K3=0.0 # gg08_rnum=385
C1+ + C6H2 -> C71+ + H2									RTYPE=2 K1=1.20E-09 K2=0.0 K3=0.0 # gg08_rnum=386
C1+ + C6H2 -> C7H1+ + H									RTYPE=2 K1=1.20E-09 K2=0.0 K3=0.0 # gg08_rnum=387
C1+ + C6H4 -> C6H41+ + C								RTYPE=2 K1=2.40E-10 K2=-0.5 K3=0.0 # gg08_rnum=388
C1+ + C6H4 -> C7H31+ + H								RTYPE=2 K1=2.40E-10 K2=-0.5 K3=0.0 # gg08_rnum=389
C1+ + C7H -> C81+ + H									RTYPE=2 K1=1.27E-08 K2=-0.5 K3=0.0 # gg08_rnum=390
C1+ + C7H2 -> C8H1+ + H									RTYPE=2 K1=1.50E-09 K2=-0.5 K3=0.0 # gg08_rnum=391
C1+ + C7H2 -> C81+ + H2									RTYPE=2 K1=1.50E-09 K2=-0.5 K3=0.0 # gg08_rnum=392
C1+ + C7H4 -> C7H41+ + C								RTYPE=2 K1=4.65E-10 K2=-0.5 K3=0.0 # gg08_rnum=393
C1+ + C7H4 -> C8H31+ + H								RTYPE=2 K1=4.65E-10 K2=-0.5 K3=0.0 # gg08_rnum=394
C1+ + C8H -> C91+ + H									RTYPE=2 K1=1.40E-08 K2=-0.5 K3=0.0 # gg08_rnum=395
C1+ + C8H2 -> C91+ + H2									RTYPE=2 K1=1.20E-09 K2=0.0 K3=0.0 # gg08_rnum=396
C1+ + C8H2 -> C9H1+ + H									RTYPE=2 K1=1.20E-09 K2=0.0 K3=0.0 # gg08_rnum=397
C1+ + C8H4 -> C8H41+ + C								RTYPE=2 K1=2.36E-10 K2=-0.5 K3=0.0 # gg08_rnum=398
C1+ + C8H4 -> C9H31+ + H								RTYPE=2 K1=2.36E-10 K2=-0.5 K3=0.0 # gg08_rnum=399
C1+ + C9H -> C101+ + H									RTYPE=2 K1=1.31E-08 K2=-0.5 K3=0.0 # gg08_rnum=400
C1+ + C9H2 -> C101+ + H2								RTYPE=2 K1=2.95E-09 K2=-0.5 K3=0.0 # gg08_rnum=401
C1+ + C9H4 -> C9H41+ + C								RTYPE=2 K1=9.18E-10 K2=-0.5 K3=0.0 # gg08_rnum=402
C1+ + ClC -> ClC1+ + C									RTYPE=2 K1=1.93E-09 K2=-0.5 K3=0.0 # gg08_rnum=403
C1+ + CCP -> CP1+ + C2									RTYPE=2 K1=1.46E-09 K2=-0.5 K3=0.0 # gg08_rnum=404
C1+ + CCP -> CCP1+ + C									RTYPE=2 K1=1.46E-09 K2=-0.5 K3=0.0 # gg08_rnum=405
C1+ + CH -> C21+ + H									RTYPE=2 K1=2.70E-09 K2=-0.5 K3=0.0 # gg08_rnum=406
C1+ + CH -> CH1+ + C									RTYPE=2 K1=2.70E-09 K2=-0.5 K3=0.0 # gg08_rnum=407
C1+ + CH2 -> C2H1+ + H									RTYPE=2 K1=4.34E-10 K2=-0.5 K3=0.0 # gg08_rnum=408
C1+ + CH2 -> CH21+ + C									RTYPE=2 K1=4.34E-10 K2=-0.5 K3=0.0 # gg08_rnum=409
C1+ + H2C2N -> H2C2N1+ + C								RTYPE=2 K1=2.04E-09 K2=-0.5 K3=0.0 # gg08_rnum=410
C1+ + CH2CO -> CH2CO1+ + C								RTYPE=2 K1=1.80E-09 K2=-0.5 K3=0.0 # gg08_rnum=411
C1+ + CH2NH -> H2C2N1+ + H								RTYPE=2 K1=2.70E-09 K2=-0.5 K3=0.0 # gg08_rnum=412
C1+ + CH2OH -> CH21+ + HCO								RTYPE=2 K1=1.61E-09 K2=-0.5 K3=0.0 # gg08_rnum=413
C1+ + CH2PH -> HCCP1+ + H2								RTYPE=2 K1=6.28E-10 K2=-0.5 K3=0.0 # gg08_rnum=414
C1+ + CH3 -> HC2H1+ + H									RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=415
C1+ + CH3 -> CH31+ + C									RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=416
C1+ + CH3 -> C2H1+ + H2									RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=417
C1+ + CH3C3N -> C4H31+ + CN								RTYPE=2 K1=2.90E-09 K2=-0.5 K3=0.0 # gg08_rnum=418
C1+ + CH3C3N -> H2C2H1+ + C3N							RTYPE=2 K1=2.90E-09 K2=-0.5 K3=0.0 # gg08_rnum=419
C1+ + CH3C4H -> C6H31+ + H								RTYPE=2 K1=4.93E-10 K2=-0.5 K3=0.0 # gg08_rnum=420
C1+ + CH3C4H -> C5H31+ + CH								RTYPE=2 K1=4.93E-10 K2=-0.5 K3=0.0 # gg08_rnum=421
C1+ + CH3C4H -> C6H21+ + H2								RTYPE=2 K1=4.93E-10 K2=-0.5 K3=0.0 # gg08_rnum=422
C1+ + CH3C5N -> C6H31+ + CN								RTYPE=2 K1=6.24E-09 K2=-0.5 K3=0.0 # gg08_rnum=423
C1+ + CH3C6H -> C7H31+ + CH								RTYPE=2 K1=9.00E-10 K2=-0.5 K3=0.0 # gg08_rnum=424
C1+ + CH3C6H -> C8H21+ + H2								RTYPE=2 K1=9.00E-10 K2=-0.5 K3=0.0 # gg08_rnum=425
C1+ + CH3C7N -> C8H31+ + CN								RTYPE=2 K1=6.44E-09 K2=-0.5 K3=0.0 # gg08_rnum=426
C1+ + CH3CHO -> CH3CHO1+ + C							RTYPE=2 K1=1.70E-09 K2=-0.5 K3=0.0 # gg08_rnum=427
C1+ + CH3CHO -> CH3CO1+ + CH							RTYPE=2 K1=1.70E-09 K2=-0.5 K3=0.0 # gg08_rnum=428
C1+ + CH3CN -> HC2NCH1+ + H								RTYPE=2 K1=2.49E-09 K2=-0.5 K3=0.0 # gg08_rnum=429
C1+ + CH3CN -> H2C2H1+ + CN								RTYPE=2 K1=2.49E-09 K2=-0.5 K3=0.0 # gg08_rnum=430
C1+ + CH3CO -> CH3CO1+ + C								RTYPE=2 K1=2.37E-09 K2=-0.5 K3=0.0 # gg08_rnum=431
C1+ + CH3NH -> CH3NH1+ + C								RTYPE=2 K1=1.51E-09 K2=-0.5 K3=0.0 # gg08_rnum=432
C1+ + CH3NH2 -> CH3NH21+ + C							RTYPE=2 K1=1.10E-09 K2=-0.5 K3=0.0 # gg08_rnum=433
C1+ + CH3NH2 -> CH3NH1+ + CH							RTYPE=2 K1=6.20E-10 K2=-0.5 K3=0.0 # gg08_rnum=434
C1+ + CH3O -> CH3O1+ + C								RTYPE=2 K1=2.46E-10 K2=-0.5 K3=0.0 # gg08_rnum=435
C1+ + CH3OCH3 -> CH3OCH31+ + C							RTYPE=2 K1=1.60E-09 K2=-0.5 K3=0.0 # gg08_rnum=436
C1+ + CH3OH -> CH31+ + HCO								RTYPE=2 K1=6.90E-10 K2=-0.5 K3=0.0 # gg08_rnum=437
C1+ + CH3OH -> CH3O1+ + CH								RTYPE=2 K1=6.90E-10 K2=-0.5 K3=0.0 # gg08_rnum=438
C1+ + CH3OH -> CH3OH1+ + C								RTYPE=2 K1=8.10E-10 K2=-0.5 K3=0.0 # gg08_rnum=439
C1+ + CH4 -> HC2H1+ + H2								RTYPE=2 K1=5.00E-10 K2=0.0 K3=0.0 # gg08_rnum=440
C1+ + CH4 -> H2C2H1+ + H								RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=441
C1+ + ClO -> ClO1+ + C									RTYPE=2 K1=3.65E-09 K2=-0.5 K3=0.0 # gg08_rnum=442
C1+ + CO2 -> CO1+ + CO									RTYPE=2 K1=1.10E-09 K2=0.0 K3=0.0 # gg08_rnum=443
C1+ + COCHO -> COCHO1+ + C								RTYPE=2 K1=1.05E-09 K2=-0.5 K3=0.0 # gg08_rnum=444
C1+ + COOH -> COOH1+ + C								RTYPE=2 K1=3.69E-09 K2=-0.5 K3=0.0 # gg08_rnum=445
C1+ + CP -> CP1+ + C									RTYPE=2 K1=2.58E-09 K2=-0.5 K3=0.0 # gg08_rnum=446
C1+ + Fe -> Fe1+ + C									RTYPE=2 K1=2.60E-09 K2=0.0 K3=0.0 # gg08_rnum=447
C1+ + H2CO -> HCO1+ + CH								RTYPE=2 K1=6.50E-10 K2=-0.5 K3=0.0 # gg08_rnum=448
C1+ + H2CO -> H2CO1+ + C								RTYPE=2 K1=9.60E-10 K2=-0.5 K3=0.0 # gg08_rnum=449
C1+ + H2CO -> CH21+ + CO								RTYPE=2 K1=1.50E-09 K2=-0.5 K3=0.0 # gg08_rnum=450
C1+ + H2CS -> CH21+ + CS								RTYPE=2 K1=2.10E-09 K2=-0.5 K3=0.0 # gg08_rnum=451
C1+ + H2O -> HCO1+ + H									RTYPE=2 K1=8.90E-10 K2=-0.5 K3=0.0 # gg08_rnum=452
C1+ + H2O -> HOC1+ + H									RTYPE=2 K1=1.80E-09 K2=-0.5 K3=0.0 # gg08_rnum=453
C1+ + H2S -> H2S1+ + C									RTYPE=2 K1=3.20E-10 K2=-0.5 K3=0.0 # gg08_rnum=454
C1+ + H2S -> HCS1+ + H									RTYPE=2 K1=9.50E-10 K2=-0.5 K3=0.0 # gg08_rnum=455
C1+ + H2SiO -> H2SiO1+ + C								RTYPE=2 K1=2.51E-09 K2=-0.5 K3=0.0 # gg08_rnum=456
C1+ + HC2NC -> C2H1+ + C2N								RTYPE=2 K1=1.40E-09 K2=-0.5 K3=0.0 # gg08_rnum=457
C1+ + HC2NC -> CNC1+ + C2H								RTYPE=2 K1=1.40E-09 K2=-0.5 K3=0.0 # gg08_rnum=458
C1+ + HC2NC -> C4N1+ + H								RTYPE=2 K1=1.40E-09 K2=-0.5 K3=0.0 # gg08_rnum=459
C1+ + HC2NC -> NC3H1+ + C								RTYPE=2 K1=1.40E-09 K2=-0.5 K3=0.0 # gg08_rnum=460
C1+ + HC2NC -> C3H1+ + CN								RTYPE=2 K1=1.40E-09 K2=-0.5 K3=0.0 # gg08_rnum=461
C1+ + HC3N -> C31+ + HCN								RTYPE=2 K1=5.50E-10 K2=-0.5 K3=0.0 # gg08_rnum=462
C1+ + HC3N -> C3H1+ + CN								RTYPE=2 K1=7.70E-09 K2=-0.5 K3=0.0 # gg08_rnum=463
C1+ + HC3N -> C4N1+ + H									RTYPE=2 K1=2.53E-09 K2=-0.5 K3=0.0 # gg08_rnum=464
C1+ + HC5N -> C5HN1+ + C								RTYPE=2 K1=6.20E-09 K2=-0.5 K3=0.0 # gg08_rnum=465
C1+ + HC5N -> C5H1+ + CN								RTYPE=2 K1=6.20E-09 K2=-0.5 K3=0.0 # gg08_rnum=466
C1+ + HC7N -> C7H1+ + CN								RTYPE=2 K1=1.30E-08 K2=-0.5 K3=0.0 # gg08_rnum=467
C1+ + HC9N -> C9H1+ + CN								RTYPE=2 K1=1.34E-08 K2=-0.5 K3=0.0 # gg08_rnum=468
C1+ + HCCP -> CCP1+ + CH								RTYPE=2 K1=5.00E-10 K2=0.0 K3=0.0 # gg08_rnum=469
C1+ + HCCP -> CP1+ + C2H								RTYPE=2 K1=5.00E-10 K2=0.0 K3=0.0 # gg08_rnum=470
C1+ + ClH -> ClC1+ + H									RTYPE=2 K1=3.30E-09 K2=-0.5 K3=0.0 # gg08_rnum=471
C1+ + HCN -> CNC1+ + H									RTYPE=2 K1=4.75E-09 K2=-0.5 K3=0.0 # gg08_rnum=472
C1+ + HCN -> C2N1+ + H									RTYPE=2 K1=4.75E-09 K2=-0.5 K3=0.0 # gg08_rnum=473
C1+ + C2NCH -> C2H1+ + C2N								RTYPE=2 K1=4.80E-09 K2=-0.5 K3=0.0 # gg08_rnum=474
C1+ + C2NCH -> HCN1+ + C3								RTYPE=2 K1=4.80E-09 K2=-0.5 K3=0.0 # gg08_rnum=475
C1+ + C2NCH -> C4N1+ + H								RTYPE=2 K1=4.80E-09 K2=-0.5 K3=0.0 # gg08_rnum=476
C1+ + C2NCH -> C2N1+ + C2H								RTYPE=2 K1=4.80E-09 K2=-0.5 K3=0.0 # gg08_rnum=477
C1+ + C2NCH -> C3N1+ + CH								RTYPE=2 K1=4.80E-09 K2=-0.5 K3=0.0 # gg08_rnum=478
C1+ + HCO -> CH1+ + CO									RTYPE=2 K1=6.70E-10 K2=-0.5 K3=0.0 # gg08_rnum=479
C1+ + HCO -> HCO1+ + C									RTYPE=2 K1=6.70E-10 K2=-0.5 K3=0.0 # gg08_rnum=480
C1+ + HCOOCH3 -> HCOOCH31+ + C							RTYPE=2 K1=2.17E-09 K2=-0.5 K3=0.0 # gg08_rnum=481
C1+ + HCOOH -> HCOOH1+ + C								RTYPE=2 K1=1.44E-09 K2=-0.5 K3=0.0 # gg08_rnum=482
C1+ + HCP -> HCP1+ + C									RTYPE=2 K1=5.85E-10 K2=-0.5 K3=0.0 # gg08_rnum=483
C1+ + HCP -> CCP1+ + H									RTYPE=2 K1=5.85E-10 K2=-0.5 K3=0.0 # gg08_rnum=484
C1+ + SiCH -> SiC21+ + H								RTYPE=2 K1=3.01E-09 K2=-0.5 K3=0.0 # gg08_rnum=485
C1+ + CH3COCHO -> CH3COCHO1+ + C						RTYPE=2 K1=4.12E-09 K2=-0.5 K3=0.0 # gg08_rnum=486
C1+ + CH3COCH3 -> CH3COCH31+ + C						RTYPE=2 K1=2.86E-09 K2=-0.5 K3=0.0 # gg08_rnum=487
C1+ + CH3CONH -> CH3CONH1+ + C							RTYPE=2 K1=3.78E-09 K2=-0.5 K3=0.0 # gg08_rnum=488
C1+ + CH3COOH -> CH3COOH1+ + C							RTYPE=2 K1=1.72E-09 K2=-0.5 K3=0.0 # gg08_rnum=489
C1+ + CH3CONH2 -> CH3CONH21+ + C						RTYPE=2 K1=3.75E-09 K2=-0.5 K3=0.0 # gg08_rnum=490
C1+ + CH3COOCH3 -> CH3COOCH31+ + C						RTYPE=2 K1=1.74E-09 K2=-0.5 K3=0.0 # gg08_rnum=491
C1+ + CH3COCH2OH -> CH3COCH2OH1+ + C					RTYPE=2 K1=3.03E-09 K2=-0.5 K3=0.0 # gg08_rnum=492
C1+ + HNC -> C2N1+ + H									RTYPE=2 K1=8.60E-09 K2=-0.5 K3=0.0 # gg08_rnum=493
C1+ + HNC3 -> C3N1+ + CH								RTYPE=2 K1=1.55E-09 K2=-0.5 K3=0.0 # gg08_rnum=494
C1+ + HNC3 -> C4N1+ + H									RTYPE=2 K1=1.55E-09 K2=-0.5 K3=0.0 # gg08_rnum=495
C1+ + HNC3 -> C31+ + HNC								RTYPE=2 K1=1.55E-09 K2=-0.5 K3=0.0 # gg08_rnum=496
C1+ + HNC3 -> HNC1+ + C3								RTYPE=2 K1=1.55E-09 K2=-0.5 K3=0.0 # gg08_rnum=497
C1+ + HNCHO -> HNCHO1+ + C								RTYPE=2 K1=2.09E-09 K2=-0.5 K3=0.0 # gg08_rnum=498
C1+ + HNCO -> HNCO1+ + C								RTYPE=2 K1=2.16E-09 K2=-0.5 K3=0.0 # gg08_rnum=499
C1+ + HNCOCHO -> HNCOCHO1+ + C							RTYPE=2 K1=1.01E-09 K2=-0.5 K3=0.0 # gg08_rnum=500
C1+ + HNCONH -> NHCONH1+ + C							RTYPE=2 K1=3.94E-09 K2=-0.5 K3=0.0 # gg08_rnum=501
C1+ + HNCOOH -> HNCOOH1+ + C							RTYPE=2 K1=2.03E-09 K2=-0.5 K3=0.0 # gg08_rnum=502
C1+ + HNOH -> HNOH1+ + C								RTYPE=2 K1=2.14E-09 K2=-0.5 K3=0.0 # gg08_rnum=503
C1+ + HNSi -> SiNC1+ + H								RTYPE=2 K1=4.79E-10 K2=-0.5 K3=0.0 # gg08_rnum=504
C1+ + CH3ONH -> CH3ONH1+ + C							RTYPE=2 K1=1.52E-09 K2=-0.5 K3=0.0 # gg08_rnum=505
C1+ + HNCH2OH -> CH2OHNH1+ + C							RTYPE=2 K1=1.52E-09 K2=-0.5 K3=0.0 # gg08_rnum=506
C1+ + HOCOOH -> HOCOOH1+ + C							RTYPE=2 K1=2.01E-09 K2=-0.5 K3=0.0 # gg08_rnum=507
C1+ + HPO -> HPO1+ + C									RTYPE=2 K1=6.90E-09 K2=-0.5 K3=0.0 # gg08_rnum=508
C1+ + HS -> CS1+ + H									RTYPE=2 K1=2.40E-09 K2=-0.5 K3=0.0 # gg08_rnum=509
C1+ + HCOCOCHO -> HCOCOCHO1+ + C						RTYPE=2 K1=1.99E-09 K2=-0.5 K3=0.0 # gg08_rnum=510
C1+ + Mg -> Mg1+ + C									RTYPE=2 K1=1.10E-09 K2=0.0 K3=0.0 # gg08_rnum=511
C1+ + MgH -> Mg1+ + CH									RTYPE=2 K1=4.10E-09 K2=-0.5 K3=0.0 # gg08_rnum=512
C1+ + Na -> Na1+ + C									RTYPE=2 K1=1.10E-09 K2=0.0 K3=0.0 # gg08_rnum=513
C1+ + NaH -> Na1+ + CH									RTYPE=2 K1=2.20E-08 K2=-0.5 K3=0.0 # gg08_rnum=514
C1+ + NaOH -> HCO1+ + Na								RTYPE=2 K1=6.40E-09 K2=-0.5 K3=0.0 # gg08_rnum=515
C1+ + NH -> CN1+ + H									RTYPE=2 K1=4.60E-09 K2=-0.5 K3=0.0 # gg08_rnum=516
C1+ + NH2 -> HCN1+ + H									RTYPE=2 K1=2.70E-09 K2=-0.5 K3=0.0 # gg08_rnum=517
C1+ + NH2CHO -> H2CO1+ + HCN							RTYPE=2 K1=1.85E-09 K2=-0.5 K3=0.0 # gg08_rnum=518
C1+ + NH2CHO -> HCN1+ + H2CO							RTYPE=2 K1=1.85E-09 K2=-0.5 K3=0.0 # gg08_rnum=519
C1+ + NH2CHO -> H2C2N1+ + OH							RTYPE=2 K1=1.85E-09 K2=-0.5 K3=0.0 # gg08_rnum=520
C1+ + NH2CHO -> CH3CN1+ + O								RTYPE=2 K1=1.85E-09 K2=-0.5 K3=0.0 # gg08_rnum=521
C1+ + NH2CHO -> CH3CO1+ + N								RTYPE=2 K1=1.85E-09 K2=-0.5 K3=0.0 # gg08_rnum=522
C1+ + NH2CHO -> CH3O1+ + CN								RTYPE=2 K1=1.85E-09 K2=-0.5 K3=0.0 # gg08_rnum=523
C1+ + NH2OH -> NH2OH1+ + C								RTYPE=2 K1=6.23E-10 K2=-0.5 K3=0.0 # gg08_rnum=524
C1+ + NH3 -> HCN1+ + H2									RTYPE=2 K1=1.08E-10 K2=-0.5 K3=0.0 # gg08_rnum=525
C1+ + NH3 -> H2CN1+ + H									RTYPE=2 K1=1.36E-09 K2=-0.5 K3=0.0 # gg08_rnum=526
C1+ + NH3 -> NH31+ + C									RTYPE=2 K1=6.80E-10 K2=-0.5 K3=0.0 # gg08_rnum=527
C1+ + NO -> NO1+ + C									RTYPE=2 K1=4.80E-10 K2=-0.5 K3=0.0 # gg08_rnum=528
C1+ + NS -> NS1+ + C									RTYPE=2 K1=2.70E-09 K2=-0.5 K3=0.0 # gg08_rnum=529
C1+ + NS -> CS1+ + N									RTYPE=2 K1=2.70E-09 K2=-0.5 K3=0.0 # gg08_rnum=530
C1+ + O2 -> CO1+ + O									RTYPE=2 K1=7.50E-10 K2=0.0 K3=0.0 # gg08_rnum=531
C1+ + O2 -> O1+ + CO									RTYPE=2 K1=4.10E-10 K2=0.0 K3=0.0 # gg08_rnum=532
C1+ + OCN -> CO1+ + CN									RTYPE=2 K1=1.90E-09 K2=-0.5 K3=0.0 # gg08_rnum=533
C1+ + OCS -> CS1+ + CO									RTYPE=2 K1=1.60E-09 K2=-0.5 K3=0.0 # gg08_rnum=534
C1+ + OCS -> OCS1+ + C									RTYPE=2 K1=4.00E-10 K2=-0.5 K3=0.0 # gg08_rnum=535
C1+ + OH -> CO1+ + H									RTYPE=2 K1=2.90E-09 K2=-3.33E-01 K3=0.0 # gg08_rnum=536
C1+ + CHOCHO -> CHOCHO1+ + C							RTYPE=2 K1=7.22E-10 K2=0.0 K3=0.0 # gg08_rnum=537
C1+ + HCOCOOH -> HCOCOOH1+ + C							RTYPE=2 K1=9.98E-10 K2=-0.5 K3=0.0 # gg08_rnum=538
C1+ + P -> P1+ + C										RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=539
C1+ + PH -> PH1+ + C									RTYPE=2 K1=1.99E-09 K2=-0.5 K3=0.0 # gg08_rnum=540
C1+ + PO -> PO1+ + C									RTYPE=2 K1=5.58E-09 K2=-0.5 K3=0.0 # gg08_rnum=541
C1+ + NH2CO -> NH2CO1+ + C								RTYPE=2 K1=2.09E-09 K2=-0.5 K3=0.0 # gg08_rnum=542
C1+ + NH2COCHO -> NH2COCHO1+ + C						RTYPE=2 K1=9.93E-10 K2=-0.5 K3=0.0 # gg08_rnum=543
C1+ + NH2CONH -> NH2CONH1+ + C							RTYPE=2 K1=3.88E-09 K2=-0.5 K3=0.0 # gg08_rnum=544
C1+ + NH2COOH -> NH2COOH1+ + C							RTYPE=2 K1=1.99E-09 K2=-0.5 K3=0.0 # gg08_rnum=545
C1+ + NH2CONH2 -> NH2CONH21+ + C						RTYPE=2 K1=3.85E-09 K2=-0.5 K3=0.0 # gg08_rnum=546
C1+ + NH2NH -> NH2NH1+ + C								RTYPE=2 K1=1.99E-09 K2=-0.5 K3=0.0 # gg08_rnum=547
C1+ + NH2NH2 -> NH2NH21+ + C							RTYPE=2 K1=1.99E-09 K2=-0.5 K3=0.0 # gg08_rnum=548
C1+ + NH2OCH3 -> NH2OCH31+ + C							RTYPE=2 K1=2.01E-09 K2=-0.5 K3=0.0 # gg08_rnum=549
C1+ + NH2CH2OH -> NH2CH2OH1+ + C						RTYPE=2 K1=1.51E-09 K2=-0.5 K3=0.0 # gg08_rnum=550
C1+ + S -> S1+ + C										RTYPE=2 K1=1.50E-09 K2=0.0 K3=0.0 # gg08_rnum=551
C1+ + Si -> Si1+ + C									RTYPE=2 K1=2.10E-09 K2=0.0 K3=0.0 # gg08_rnum=552
C1+ + SiC -> SiC1+ + C									RTYPE=2 K1=2.56E-09 K2=-0.5 K3=0.0 # gg08_rnum=553
C1+ + SiC -> Si1+ + C2									RTYPE=2 K1=2.56E-09 K2=-0.5 K3=0.0 # gg08_rnum=554
C1+ + SiC2 -> SiC21+ + C								RTYPE=2 K1=2.97E-09 K2=-0.5 K3=0.0 # gg08_rnum=555
C1+ + SiC2H -> SiC2H1+ + C								RTYPE=2 K1=8.70E-10 K2=-0.5 K3=0.0 # gg08_rnum=556
C1+ + SiC2H -> SiC31+ + H								RTYPE=2 K1=8.70E-10 K2=-0.5 K3=0.0 # gg08_rnum=557
C1+ + SiC2H2 -> SiC3H1+ + H								RTYPE=2 K1=3.09E-09 K2=-0.5 K3=0.0 # gg08_rnum=558
C1+ + cSiC3 -> SiC31+ + C								RTYPE=2 K1=2.44E-09 K2=-0.5 K3=0.0 # gg08_rnum=559
C1+ + SiC3H -> SiC41+ + H								RTYPE=2 K1=3.65E-09 K2=-0.5 K3=0.0 # gg08_rnum=560
C1+ + SiCH2 -> SiCH21+ + C								RTYPE=2 K1=6.33E-10 K2=-0.5 K3=0.0 # gg08_rnum=561
C1+ + SiCH2 -> SiC2H1+ + H								RTYPE=2 K1=6.33E-10 K2=-0.5 K3=0.0 # gg08_rnum=562
C1+ + SiCH2 -> SiC21+ + H2								RTYPE=2 K1=6.33E-10 K2=-0.5 K3=0.0 # gg08_rnum=563
C1+ + SiCH3 -> SiCH31+ + C								RTYPE=2 K1=6.33E-10 K2=-0.5 K3=0.0 # gg08_rnum=564
C1+ + SiCH3 -> SiC2H1+ + H2								RTYPE=2 K1=6.33E-10 K2=-0.5 K3=0.0 # gg08_rnum=565
C1+ + SiCH3 -> SiC2H21+ + H								RTYPE=2 K1=6.33E-10 K2=-0.5 K3=0.0 # gg08_rnum=566
C1+ + SiH -> SiC1+ + H									RTYPE=2 K1=3.78E-10 K2=-0.5 K3=0.0 # gg08_rnum=567
C1+ + SiH2 -> SiCH1+ + H								RTYPE=2 K1=7.93E-11 K2=-0.5 K3=0.0 # gg08_rnum=568
C1+ + SiH2 -> SiH21+ + C								RTYPE=2 K1=7.93E-11 K2=-0.5 K3=0.0 # gg08_rnum=569
C1+ + SiH2 -> SiC1+ + H2								RTYPE=2 K1=7.93E-11 K2=-0.5 K3=0.0 # gg08_rnum=570
C1+ + SiH3 -> SiCH21+ + H								RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=571
C1+ + SiH3 -> SiH31+ + C								RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=572
C1+ + SiH3 -> SiCH1+ + H2								RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=573
C1+ + SiH4 -> SiCH21+ + H2								RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=574
C1+ + SiH4 -> SiCH31+ + H								RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=575
C1+ + SiN -> SiN1+ + C									RTYPE=2 K1=3.46E-09 K2=-0.5 K3=0.0 # gg08_rnum=576
C1+ + SiN -> SiC1+ + N									RTYPE=2 K1=3.46E-09 K2=-0.5 K3=0.0 # gg08_rnum=577
C1+ + SiO -> Si1+ + CO									RTYPE=2 K1=4.60E-09 K2=-0.5 K3=0.0 # gg08_rnum=578
C1+ + SiS -> SiS1+ + C									RTYPE=2 K1=2.50E-09 K2=-0.5 K3=0.0 # gg08_rnum=579
C1+ + SiS -> SiC1+ + S									RTYPE=2 K1=2.50E-09 K2=-0.5 K3=0.0 # gg08_rnum=580
C1+ + SO -> CO1+ + S									RTYPE=2 K1=1.10E-09 K2=-0.5 K3=0.0 # gg08_rnum=581
C1+ + SO -> CS1+ + O									RTYPE=2 K1=1.10E-09 K2=-0.5 K3=0.0 # gg08_rnum=582
C1+ + SO -> SO1+ + C									RTYPE=2 K1=1.10E-09 K2=-0.5 K3=0.0 # gg08_rnum=583
C1+ + SO -> S1+ + CO									RTYPE=2 K1=1.10E-09 K2=-0.5 K3=0.0 # gg08_rnum=584
C1+ + SO2 -> SO1+ + CO									RTYPE=2 K1=2.00E-09 K2=-0.5 K3=0.0 # gg08_rnum=585
C1+ + CH3OCO -> CH3OCO1+ + C							RTYPE=2 K1=1.77E-09 K2=-0.5 K3=0.0 # gg08_rnum=586
C1+ + CH3OCOCHO -> CH3OCOCHO1+ + C						RTYPE=2 K1=9.76E-10 K2=-0.5 K3=0.0 # gg08_rnum=587
C1+ + CH3OCONH -> CH3OCONH1+ + C						RTYPE=2 K1=1.97E-09 K2=-0.5 K3=0.0 # gg08_rnum=588
C1+ + CH3OCOOH -> CH3OCOOH1+ + C						RTYPE=2 K1=1.95E-09 K2=-0.5 K3=0.0 # gg08_rnum=589
C1+ + CH3OCONH2 -> CH3OCONH21+ + C						RTYPE=2 K1=1.96E-09 K2=-0.5 K3=0.0 # gg08_rnum=590
C1+ + CH3OCOOCH3 -> CH3OCOOCH31+ + C					RTYPE=2 K1=1.74E-10 K2=-0.5 K3=0.0 # gg08_rnum=591
C1+ + CH3OCOCH2OH -> CH3OCOCH2OH1+ + C					RTYPE=2 K1=3.09E-09 K2=-0.5 K3=0.0 # gg08_rnum=592
C1+ + CH3OOH -> CH3OOH1+ + C							RTYPE=2 K1=1.72E-09 K2=-0.5 K3=0.0 # gg08_rnum=593
C1+ + CH3OOCH3 -> CH3OOCH31+ + C						RTYPE=2 K1=1.47E-09 K2=-0.5 K3=0.0 # gg08_rnum=594
C1+ + CH3OCH2OH -> CH3OCH2OH1+ + C						RTYPE=2 K1=1.47E-09 K2=-0.5 K3=0.0 # gg08_rnum=595
C1+ + CH2OHCHO -> CH2OHCHO1+ + C						RTYPE=2 K1=2.70E-09 K2=-0.5 K3=0.0 # gg08_rnum=596
C1+ + HOCH2CO -> HOCH2CO1+ + C							RTYPE=2 K1=2.72E-09 K2=-0.5 K3=0.0 # gg08_rnum=597
C1+ + HOCH2COCHO -> HOCH2COCHO1+ + C					RTYPE=2 K1=9.76E-10 K2=-0.5 K3=0.0 # gg08_rnum=598
C1+ + HNCOCH2OH -> HNCOCH2OH1+ + C						RTYPE=2 K1=1.97E-09 K2=-0.5 K3=0.0 # gg08_rnum=599
C1+ + CH2OHCOOH -> CH2OHCOOH1+ + C						RTYPE=2 K1=1.76E-09 K2=-0.5 K3=0.0 # gg08_rnum=600
C1+ + NH2COCH2OH -> NH2COCH2OH1+ + C					RTYPE=2 K1=1.96E-09 K2=-0.5 K3=0.0 # gg08_rnum=601
C1+ + HOCH2COCH2OH -> HOCH2COCH2OH1+ + C				RTYPE=2 K1=1.74E-09 K2=-0.5 K3=0.0 # gg08_rnum=602
C1+ + HOCH2OH -> HOCH2OH1+ + C							RTYPE=2 K1=3.04E-09 K2=-0.5 K3=0.0 # gg08_rnum=603
C1+ + HOCH2CH2OH -> HOCH2CH2OH1+ + C					RTYPE=2 K1=7.60E-10 K2=0.0 K3=0.0 # gg08_rnum=604
C21+ + C -> C1+ + C2									RTYPE=2 K1=1.10E-10 K2=0.0 K3=0.0 # gg08_rnum=605
C21+ + C2 -> C31+ + C									RTYPE=2 K1=8.70E-10 K2=0.0 K3=0.0 # gg08_rnum=606
C21+ + HC2H -> C4H1+ + H								RTYPE=2 K1=1.70E-09 K2=0.0 K3=0.0 # gg08_rnum=607
C21+ + CH -> C31+ + H									RTYPE=2 K1=3.20E-10 K2=0.0 K3=0.0 # gg08_rnum=608
C21+ + CH -> CH1+ + C2									RTYPE=2 K1=3.20E-10 K2=0.0 K3=0.0 # gg08_rnum=609
C21+ + CH2 -> C3H1+ + H									RTYPE=2 K1=4.50E-10 K2=0.0 K3=0.0 # gg08_rnum=610
C21+ + CH2 -> CH21+ + C2								RTYPE=2 K1=4.50E-10 K2=0.0 K3=0.0 # gg08_rnum=611
C21+ + CH4 -> C3H1+ + H2 + H							RTYPE=2 K1=1.96E-10 K2=0.0 K3=0.0 # gg08_rnum=612
C21+ + CH4 -> C3H21+ + H2								RTYPE=2 K1=5.74E-10 K2=0.0 K3=0.0 # gg08_rnum=613
C21+ + CH4 -> C2H1+ + CH3								RTYPE=2 K1=2.38E-10 K2=0.0 K3=0.0 # gg08_rnum=614
C21+ + CH4 -> C3H31+ + H								RTYPE=2 K1=2.10E-10 K2=0.0 K3=0.0 # gg08_rnum=615
C21+ + CH4 -> HC2H1+ + CH2								RTYPE=2 K1=1.82E-10 K2=0.0 K3=0.0 # gg08_rnum=616
C21+ + H2 -> C2H1+ + H									RTYPE=2 K1=1.10E-09 K2=0.0 K3=0.0 # gg08_rnum=617
C21+ + H2O -> C2H1+ + OH								RTYPE=2 K1=4.40E-10 K2=0.0 K3=0.0 # gg08_rnum=618
C21+ + H2O -> C2OH1+ + H								RTYPE=2 K1=4.40E-10 K2=0.0 K3=0.0 # gg08_rnum=619
C21+ + HCN -> C3H1+ + N									RTYPE=2 K1=7.80E-10 K2=0.0 K3=0.0 # gg08_rnum=620
C21+ + HCN -> C3N1+ + H									RTYPE=2 K1=1.60E-09 K2=0.0 K3=0.0 # gg08_rnum=621
C21+ + HCO -> C2H1+ + CO								RTYPE=2 K1=3.80E-10 K2=0.0 K3=0.0 # gg08_rnum=622
C21+ + HCO -> HCO1+ + C2								RTYPE=2 K1=3.80E-10 K2=0.0 K3=0.0 # gg08_rnum=623
C21+ + N -> C1+ + CN									RTYPE=2 K1=4.00E-11 K2=0.0 K3=0.0 # gg08_rnum=624
C21+ + NH -> C2N1+ + H									RTYPE=2 K1=3.30E-10 K2=0.0 K3=0.0 # gg08_rnum=625
C21+ + NH -> C2H1+ + N									RTYPE=2 K1=3.30E-10 K2=0.0 K3=0.0 # gg08_rnum=626
C21+ + NH2 -> HC2N1+ + H								RTYPE=2 K1=4.60E-10 K2=0.0 K3=0.0 # gg08_rnum=627
C21+ + NH2 -> NH21+ + C2								RTYPE=2 K1=4.60E-10 K2=0.0 K3=0.0 # gg08_rnum=628
C21+ + NO -> NO1+ + C2									RTYPE=2 K1=3.40E-10 K2=0.0 K3=0.0 # gg08_rnum=629
C21+ + O -> CO1+ + C									RTYPE=2 K1=3.10E-10 K2=0.0 K3=0.0 # gg08_rnum=630
C21+ + O2 -> CO1+ + CO									RTYPE=2 K1=8.00E-10 K2=0.0 K3=0.0 # gg08_rnum=631
C21+ + S -> CS1+ + C									RTYPE=2 K1=5.80E-10 K2=0.0 K3=0.0 # gg08_rnum=632
C21+ + S -> S1+ + C2									RTYPE=2 K1=5.80E-10 K2=0.0 K3=0.0 # gg08_rnum=633
C2H1+ + C -> C31+ + H									RTYPE=2 K1=1.10E-09 K2=0.0 K3=0.0 # gg08_rnum=634
C2H1+ + HC2H -> HC4H1+ + H								RTYPE=2 K1=1.70E-09 K2=0.0 K3=0.0 # gg08_rnum=635
C2H1+ + CH -> C3H1+ + H									RTYPE=2 K1=3.20E-10 K2=0.0 K3=0.0 # gg08_rnum=636
C2H1+ + CH -> CH21+ + C2								RTYPE=2 K1=3.20E-10 K2=0.0 K3=0.0 # gg08_rnum=637
C2H1+ + CH2 -> C3H21+ + H								RTYPE=2 K1=4.40E-10 K2=0.0 K3=0.0 # gg08_rnum=638
C2H1+ + CH2 -> CH31+ + C2								RTYPE=2 K1=4.40E-10 K2=0.0 K3=0.0 # gg08_rnum=639
C2H1+ + CH4 -> C3H41+ + H								RTYPE=2 K1=1.32E-10 K2=0.0 K3=0.0 # gg08_rnum=640
C2H1+ + CH4 -> HC2H1+ + CH3								RTYPE=2 K1=3.74E-10 K2=0.0 K3=0.0 # gg08_rnum=641
C2H1+ + CH4 -> C3H31+ + H2								RTYPE=2 K1=3.74E-10 K2=0.0 K3=0.0 # gg08_rnum=642
C2H1+ + CN -> C3N1+ + H									RTYPE=2 K1=9.10E-10 K2=0.0 K3=0.0 # gg08_rnum=643
C2H1+ + CO2 -> C2OH1+ + CO								RTYPE=2 K1=9.40E-10 K2=0.0 K3=0.0 # gg08_rnum=644
C2H1+ + H2 -> HC2H1+ + H								RTYPE=2 K1=1.10E-09 K2=0.0 K3=0.0 # gg08_rnum=645
C2H1+ + H2CO -> CH2OH1+ + C2							RTYPE=2 K1=1.10E-09 K2=0.0 K3=0.0 # gg08_rnum=646
C2H1+ + H2O -> CH2CO1+ + H								RTYPE=2 K1=8.70E-10 K2=0.0 K3=0.0 # gg08_rnum=647
C2H1+ + HCN -> H2CN1+ + C2								RTYPE=2 K1=9.50E-10 K2=0.0 K3=0.0 # gg08_rnum=648
C2H1+ + HCN -> NC3H1+ + H								RTYPE=2 K1=1.80E-09 K2=0.0 K3=0.0 # gg08_rnum=649
C2H1+ + HCN -> HC2H1+ + CN								RTYPE=2 K1=1.40E-09 K2=0.0 K3=0.0 # gg08_rnum=650
C2H1+ + HCO -> HC2H1+ + CO								RTYPE=2 K1=7.60E-10 K2=0.0 K3=0.0 # gg08_rnum=651
C2H1+ + HNC -> H2CN1+ + C2								RTYPE=2 K1=1.40E-09 K2=0.0 K3=0.0 # gg08_rnum=652
C2H1+ + N -> C2N1+ + H									RTYPE=2 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=653
C2H1+ + N -> CH1+ + CN									RTYPE=2 K1=9.00E-11 K2=0.0 K3=0.0 # gg08_rnum=654
C2H1+ + NH2 -> H2C2N1+ + H								RTYPE=2 K1=4.60E-10 K2=0.0 K3=0.0 # gg08_rnum=655
C2H1+ + NH2 -> NH31+ + C2								RTYPE=2 K1=4.60E-10 K2=0.0 K3=0.0 # gg08_rnum=656
C2H1+ + NH3 -> H2C2N1+ + H2								RTYPE=2 K1=5.50E-10 K2=0.0 K3=0.0 # gg08_rnum=657
C2H1+ + NH3 -> NH41+ + C2								RTYPE=2 K1=5.50E-10 K2=0.0 K3=0.0 # gg08_rnum=658
C2H1+ + NO -> NO1+ + C2H								RTYPE=2 K1=1.20E-10 K2=0.0 K3=0.0 # gg08_rnum=659
C2H1+ + O -> HCO1+ + C									RTYPE=2 K1=3.30E-10 K2=0.0 K3=0.0 # gg08_rnum=660
C2H1+ + S -> S1+ + C2H									RTYPE=2 K1=1.20E-09 K2=0.0 K3=0.0 # gg08_rnum=661
HC2H1+ + C -> C3H1+ + H									RTYPE=2 K1=1.10E-09 K2=0.0 K3=0.0 # gg08_rnum=662
HC2H1+ + C -> HC2H + C1+								RTYPE=2 K1=1.10E-09 K2=0.0 K3=0.0 # gg08_rnum=663
HC2H1+ + C -> C31+ + H2									RTYPE=2 K1=1.10E-09 K2=0.0 K3=0.0 # gg08_rnum=664
HC2H1+ + C2H -> HC4H1+ + H								RTYPE=2 K1=2.10E-09 K2=-0.5 K3=0.0 # gg08_rnum=665
HC2H1+ + HC2H -> HC4H1+ + H2							RTYPE=2 K1=5.20E-10 K2=0.0 K3=0.0 # gg08_rnum=666
HC2H1+ + HC2H -> C4H31+ + H								RTYPE=2 K1=8.80E-10 K2=0.0 K3=0.0 # gg08_rnum=667
HC2H1+ + H2C2H -> H2C2H1+ + HC2H						RTYPE=2 K1=5.30E-10 K2=-0.5 K3=0.0 # gg08_rnum=668
HC2H1+ + H2C2H -> C3H31+ + CH2							RTYPE=2 K1=5.30E-10 K2=-0.5 K3=0.0 # gg08_rnum=669
HC2H1+ + H2C2H -> C4H31+ + H2							RTYPE=2 K1=5.30E-10 K2=-0.5 K3=0.0 # gg08_rnum=670
HC2H1+ + H2C2H2 -> H2C2H21+ + HC2H						RTYPE=2 K1=4.10E-10 K2=0.0 K3=0.0 # gg08_rnum=671
HC2H1+ + H2C2H2 -> C3H31+ + CH3							RTYPE=2 K1=5.20E-10 K2=0.0 K3=0.0 # gg08_rnum=672
HC2H1+ + H2C2H2 -> C4H51+ + H							RTYPE=2 K1=2.80E-10 K2=0.0 K3=0.0 # gg08_rnum=673
HC2H1+ + C3H -> C5H1+ + H2								RTYPE=2 K1=3.64E-09 K2=-0.5 K3=0.0 # gg08_rnum=674
HC2H1+ + C3H -> C5H21+ + H								RTYPE=2 K1=3.64E-09 K2=-0.5 K3=0.0 # gg08_rnum=675
HC2H1+ + C3H2 -> C5H31+ + H								RTYPE=2 K1=2.24E-09 K2=-0.5 K3=0.0 # gg08_rnum=676
HC2H1+ + C3H2 -> C5H21+ + H2							RTYPE=2 K1=1.12E-10 K2=-0.5 K3=0.0 # gg08_rnum=677
HC2H1+ + C3H3 -> C3H31+ + HC2H							RTYPE=2 K1=1.31E-09 K2=-0.5 K3=0.0 # gg08_rnum=678
HC2H1+ + C3H3 -> C5H31+ + H2							RTYPE=2 K1=1.31E-09 K2=-0.5 K3=0.0 # gg08_rnum=679
HC2H1+ + C3H3 -> C5H41+ + H								RTYPE=2 K1=1.31E-09 K2=-0.5 K3=0.0 # gg08_rnum=680
HC2H1+ + C3H4 -> C3H41+ + HC2H							RTYPE=2 K1=2.54E-10 K2=-0.5 K3=0.0 # gg08_rnum=681
HC2H1+ + C3H4 -> C5H41+ + H2							RTYPE=2 K1=2.54E-10 K2=-0.5 K3=0.0 # gg08_rnum=682
HC2H1+ + C3H4 -> C5H51+ + H								RTYPE=2 K1=2.54E-10 K2=-0.5 K3=0.0 # gg08_rnum=683
HC2H1+ + C4H -> C6H21+ + H								RTYPE=2 K1=1.00E-09 K2=-0.5 K3=0.0 # gg08_rnum=684
HC2H1+ + C4H -> C6H1+ + H2								RTYPE=2 K1=1.00E-09 K2=-0.5 K3=0.0 # gg08_rnum=685
HC2H1+ + HC4H -> HC4H1+ + HC2H							RTYPE=2 K1=1.30E-09 K2=0.0 K3=0.0 # gg08_rnum=686
HC2H1+ + HC4H -> C6H21+ + H2							RTYPE=2 K1=1.00E-17 K2=0.0 K3=0.0 # gg08_rnum=687
HC2H1+ + HC4H -> C6H31+ + H								RTYPE=2 K1=1.40E-10 K2=0.0 K3=0.0 # gg08_rnum=688
HC2H1+ + C5H -> C7H21+ + H								RTYPE=2 K1=4.62E-09 K2=-0.5 K3=0.0 # gg08_rnum=689
HC2H1+ + C5H -> C7H1+ + H2								RTYPE=2 K1=4.62E-09 K2=-0.5 K3=0.0 # gg08_rnum=690
HC2H1+ + C5H2 -> C5H21+ + HC2H							RTYPE=2 K1=7.55E-10 K2=-0.5 K3=0.0 # gg08_rnum=691
HC2H1+ + C5H2 -> C7H21+ + H2							RTYPE=2 K1=7.55E-10 K2=-0.5 K3=0.0 # gg08_rnum=692
HC2H1+ + C5H2 -> C7H31+ + H								RTYPE=2 K1=7.55E-10 K2=-0.5 K3=0.0 # gg08_rnum=693
HC2H1+ + C6H -> C8H1+ + H2								RTYPE=2 K1=5.03E-09 K2=-0.5 K3=0.0 # gg08_rnum=694
HC2H1+ + C6H -> C8H21+ + H								RTYPE=2 K1=5.03E-09 K2=-0.5 K3=0.0 # gg08_rnum=695
HC2H1+ + C6H2 -> C6H21+ + HC2H							RTYPE=2 K1=5.00E-10 K2=0.0 K3=0.0 # gg08_rnum=696
HC2H1+ + C6H2 -> C8H21+ + H2							RTYPE=2 K1=5.00E-10 K2=0.0 K3=0.0 # gg08_rnum=697
HC2H1+ + C6H2 -> C8H31+ + H								RTYPE=2 K1=5.00E-10 K2=0.0 K3=0.0 # gg08_rnum=698
HC2H1+ + C7H -> C9H1+ + H2								RTYPE=2 K1=4.63E-09 K2=-0.5 K3=0.0 # gg08_rnum=699
HC2H1+ + C7H -> C9H21+ + H								RTYPE=2 K1=4.63E-09 K2=-0.5 K3=0.0 # gg08_rnum=700
HC2H1+ + C7H2 -> C7H21+ + HC2H							RTYPE=2 K1=7.23E-10 K2=-0.5 K3=0.0 # gg08_rnum=701
HC2H1+ + C7H2 -> C9H21+ + H2							RTYPE=2 K1=7.23E-10 K2=-0.5 K3=0.0 # gg08_rnum=702
HC2H1+ + C7H2 -> C9H31+ + H								RTYPE=2 K1=7.23E-10 K2=-0.5 K3=0.0 # gg08_rnum=703
HC2H1+ + CH -> C3H21+ + H								RTYPE=2 K1=4.60E-09 K2=-0.5 K3=0.0 # gg08_rnum=704
HC2H1+ + CH2 -> C3H31+ + H								RTYPE=2 K1=7.32E-10 K2=-0.5 K3=0.0 # gg08_rnum=705
HC2H1+ + CH4 -> C3H51+ + H								RTYPE=2 K1=8.00E-10 K2=0.0 K3=0.0 # gg08_rnum=706
HC2H1+ + CH4 -> C3H41+ + H2								RTYPE=2 K1=2.00E-10 K2=0.0 K3=0.0 # gg08_rnum=707
HC2H1+ + CN -> NC3H1+ + H								RTYPE=2 K1=3.70E-09 K2=-0.5 K3=0.0 # gg08_rnum=708
HC2H1+ + Fe -> Fe1+ + HC2H								RTYPE=2 K1=2.00E-09 K2=0.0 K3=0.0 # gg08_rnum=709
HC2H1+ + H -> H2C2H1+									RTYPE=2 K1=7.00E-15 K2=-1.50E+00 K3=0.0 # gg08_rnum=710
HC2H1+ + H2 -> H2C2H1+ + H								RTYPE=2 K1=0.0 K2=0.0 K3=0.0 # gg08_rnum=711
HC2H1+ + H2CO -> H2CO1+ + HC2H							RTYPE=2 K1=1.60E-09 K2=-0.5 K3=0.0 # gg08_rnum=712
HC2H1+ + H2S -> H2S1+ + HC2H							RTYPE=2 K1=9.80E-10 K2=-0.5 K3=0.0 # gg08_rnum=713
HC2H1+ + H2S -> H2C2H1+ + HS							RTYPE=2 K1=2.00E-11 K2=-0.5 K3=0.0 # gg08_rnum=714
HC2H1+ + HCN -> HC3NH1+ + H								RTYPE=2 K1=1.33E-10 K2=0.0 K3=0.0 # gg08_rnum=715
HC2H1+ + HCO -> HCO1+ + HC2H							RTYPE=2 K1=5.70E-10 K2=-0.5 K3=0.0 # gg08_rnum=716
HC2H1+ + HCO -> H2C2H1+ + CO							RTYPE=2 K1=4.30E-10 K2=-0.5 K3=0.0 # gg08_rnum=717
HC2H1+ + HNC -> H2CN1+ + C2H							RTYPE=2 K1=2.60E-10 K2=0.0 K3=0.0 # gg08_rnum=718
HC2H1+ + HNC -> HC3NH1+ + H								RTYPE=2 K1=1.30E-10 K2=0.0 K3=0.0 # gg08_rnum=719
HC2H1+ + Mg -> Mg1+ + HC2H								RTYPE=2 K1=3.00E-09 K2=0.0 K3=0.0 # gg08_rnum=720
HC2H1+ + N -> HC2N1+ + H								RTYPE=2 K1=1.50E-10 K2=0.0 K3=0.0 # gg08_rnum=721
HC2H1+ + N -> CH1+ + HCN								RTYPE=2 K1=2.50E-11 K2=0.0 K3=0.0 # gg08_rnum=722
HC2H1+ + N -> C2N1+ + H2								RTYPE=2 K1=7.50E-11 K2=0.0 K3=0.0 # gg08_rnum=723
HC2H1+ + Na -> Na1+ + HC2H								RTYPE=2 K1=2.70E-09 K2=0.0 K3=0.0 # gg08_rnum=724
HC2H1+ + NH -> H2C2N1+ + H								RTYPE=2 K1=3.90E-09 K2=-0.5 K3=0.0 # gg08_rnum=725
HC2H1+ + NH2 -> CH3CN1+ + H								RTYPE=2 K1=1.10E-09 K2=-0.5 K3=0.0 # gg08_rnum=726
HC2H1+ + NH2 -> NH31+ + C2H								RTYPE=2 K1=1.10E-09 K2=-0.5 K3=0.0 # gg08_rnum=727
HC2H1+ + NH3 -> NH31+ + HC2H							RTYPE=2 K1=1.20E-09 K2=-0.5 K3=0.0 # gg08_rnum=728
HC2H1+ + NH3 -> NH41+ + C2H								RTYPE=2 K1=5.50E-10 K2=-0.5 K3=0.0 # gg08_rnum=729
HC2H1+ + NO -> NO1+ + HC2H								RTYPE=2 K1=3.80E-10 K2=-0.5 K3=0.0 # gg08_rnum=730
HC2H1+ + O -> HOC1+ + CH								RTYPE=2 K1=5.00E-11 K2=0.0 K3=0.0 # gg08_rnum=731
HC2H1+ + O -> C2OH1+ + H								RTYPE=2 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=732
HC2H1+ + O -> HCO1+ + CH								RTYPE=2 K1=5.00E-11 K2=0.0 K3=0.0 # gg08_rnum=733
HC2H1+ + O2 -> HCO1+ + HCO								RTYPE=2 K1=9.80E-13 K2=0.0 K3=0.0 # gg08_rnum=734
HC2H1+ + OH -> CH2CO1+ + H								RTYPE=2 K1=4.80E-09 K2=-0.5 K3=0.0 # gg08_rnum=735
HC2H1+ + P -> HCCP1+ + H								RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=736
HC2H1+ + Si -> SiC21+ + H2								RTYPE=2 K1=2.00E-10 K2=0.0 K3=0.0 # gg08_rnum=737
HC2H1+ + Si -> SiC2H1+ + H								RTYPE=2 K1=2.00E-10 K2=0.0 K3=0.0 # gg08_rnum=738
HC2H1+ + Si -> Si1+ + HC2H								RTYPE=2 K1=1.70E-09 K2=0.0 K3=0.0 # gg08_rnum=739
H2C2H1+ + C -> C3H1+ + H2								RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=740
H2C2H1+ + C -> C3H21+ + H								RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=741
H2C2H1+ + C2H -> C4H31+ + H								RTYPE=2 K1=6.80E-10 K2=-0.5 K3=0.0 # gg08_rnum=742
H2C2H1+ + C2H -> HC2H1+ + HC2H							RTYPE=2 K1=6.80E-10 K2=-0.5 K3=0.0 # gg08_rnum=743
H2C2H1+ + C2H -> HC4H1+ + H2							RTYPE=2 K1=6.80E-10 K2=-0.5 K3=0.0 # gg08_rnum=744
H2C2H1+ + HC2H -> C4H31+ + H2							RTYPE=2 K1=7.20E-10 K2=0.0 K3=0.0 # gg08_rnum=745
H2C2H1+ + H2C2H -> C3H31+ + CH3							RTYPE=2 K1=5.30E-10 K2=-0.5 K3=0.0 # gg08_rnum=746
H2C2H1+ + H2C2H -> C4H51+ + H							RTYPE=2 K1=5.30E-10 K2=-0.5 K3=0.0 # gg08_rnum=747
H2C2H1+ + H2C2H2 -> CH3CH21+ + HC2H						RTYPE=2 K1=9.30E-10 K2=0.0 K3=0.0 # gg08_rnum=748
H2C2H1+ + C3H -> C5H21+ + H2							RTYPE=2 K1=2.40E-09 K2=-0.5 K3=0.0 # gg08_rnum=749
H2C2H1+ + C3H -> C5H31+ + H								RTYPE=2 K1=2.40E-09 K2=-0.5 K3=0.0 # gg08_rnum=750
H2C2H1+ + C3H -> C3H21+ + HC2H							RTYPE=2 K1=2.40E-09 K2=-0.5 K3=0.0 # gg08_rnum=751
H2C2H1+ + C3H2 -> C5H41+ + H							RTYPE=2 K1=1.11E-09 K2=-0.5 K3=0.0 # gg08_rnum=752
H2C2H1+ + C3H2 -> C3H31+ + HC2H							RTYPE=2 K1=7.44E-10 K2=-0.5 K3=0.0 # gg08_rnum=753
H2C2H1+ + C3H2 -> C5H31+ + H2							RTYPE=2 K1=1.11E-09 K2=-0.5 K3=0.0 # gg08_rnum=754
H2C2H1+ + C3H3 -> C5H41+ + H2							RTYPE=2 K1=1.29E-09 K2=-0.5 K3=0.0 # gg08_rnum=755
H2C2H1+ + C3H3 -> C5H51+ + H							RTYPE=2 K1=1.29E-09 K2=-0.5 K3=0.0 # gg08_rnum=756
H2C2H1+ + C3H3 -> C3H41+ + HC2H							RTYPE=2 K1=1.29E-09 K2=-0.5 K3=0.0 # gg08_rnum=757
H2C2H1+ + C3H4 -> C3H51+ + HC2H							RTYPE=2 K1=3.77E-10 K2=-0.5 K3=0.0 # gg08_rnum=758
H2C2H1+ + C3H4 -> C5H51+ + H2							RTYPE=2 K1=3.77E-10 K2=-0.5 K3=0.0 # gg08_rnum=759
H2C2H1+ + C4H -> C6H31+ + H								RTYPE=2 K1=6.60E-10 K2=-0.5 K3=0.0 # gg08_rnum=760
H2C2H1+ + C4H -> HC4H1+ + HC2H							RTYPE=2 K1=6.60E-10 K2=-0.5 K3=0.0 # gg08_rnum=761
H2C2H1+ + C4H -> C6H21+ + H2							RTYPE=2 K1=6.60E-10 K2=-0.5 K3=0.0 # gg08_rnum=762
H2C2H1+ + HC4H -> C4H31+ + HC2H							RTYPE=2 K1=3.00E-10 K2=0.0 K3=0.0 # gg08_rnum=763
H2C2H1+ + HC4H -> C6H31+ + H2							RTYPE=2 K1=3.00E-10 K2=0.0 K3=0.0 # gg08_rnum=764
H2C2H1+ + HC4H -> C6H41+ + H							RTYPE=2 K1=3.00E-10 K2=0.0 K3=0.0 # gg08_rnum=765
H2C2H1+ + C5H -> C5H21+ + HC2H							RTYPE=2 K1=3.04E-09 K2=-0.5 K3=0.0 # gg08_rnum=766
H2C2H1+ + C5H -> C7H21+ + H2							RTYPE=2 K1=3.04E-09 K2=-0.5 K3=0.0 # gg08_rnum=767
H2C2H1+ + C5H -> C7H31+ + H								RTYPE=2 K1=3.04E-09 K2=-0.5 K3=0.0 # gg08_rnum=768
H2C2H1+ + C5H2 -> C7H31+ + H2							RTYPE=2 K1=7.45E-10 K2=-0.5 K3=0.0 # gg08_rnum=769
H2C2H1+ + C5H2 -> C7H41+ + H							RTYPE=2 K1=7.45E-10 K2=-0.5 K3=0.0 # gg08_rnum=770
H2C2H1+ + C5H2 -> C5H31+ + HC2H							RTYPE=2 K1=7.45E-10 K2=-0.5 K3=0.0 # gg08_rnum=771
H2C2H1+ + C6H -> C8H21+ + H2							RTYPE=2 K1=3.34E-09 K2=-0.5 K3=0.0 # gg08_rnum=772
H2C2H1+ + C6H -> C8H31+ + H								RTYPE=2 K1=3.34E-09 K2=-0.5 K3=0.0 # gg08_rnum=773
H2C2H1+ + C6H -> C6H21+ + HC2H							RTYPE=2 K1=3.34E-09 K2=-0.5 K3=0.0 # gg08_rnum=774
H2C2H1+ + C6H2 -> C8H31+ + H2							RTYPE=2 K1=3.00E-10 K2=0.0 K3=0.0 # gg08_rnum=775
H2C2H1+ + C6H2 -> C8H41+ + H							RTYPE=2 K1=3.00E-10 K2=0.0 K3=0.0 # gg08_rnum=776
H2C2H1+ + C6H2 -> C6H31+ + HC2H							RTYPE=2 K1=3.00E-10 K2=0.0 K3=0.0 # gg08_rnum=777
H2C2H1+ + C6H6 -> C6H71+ + HC2H							RTYPE=2 K1=1.60E-09 K2=0.0 K3=0.0 # gg08_rnum=778
H2C2H1+ + C7H -> C9H31+ + H								RTYPE=2 K1=3.04E-09 K2=-0.5 K3=0.0 # gg08_rnum=779
H2C2H1+ + C7H -> C7H21+ + HC2H							RTYPE=2 K1=3.04E-09 K2=-0.5 K3=0.0 # gg08_rnum=780
H2C2H1+ + C7H -> C9H21+ + H2							RTYPE=2 K1=3.04E-09 K2=-0.5 K3=0.0 # gg08_rnum=781
H2C2H1+ + C7H2 -> C9H41+ + H							RTYPE=2 K1=6.79E-10 K2=-0.5 K3=0.0 # gg08_rnum=782
H2C2H1+ + C7H2 -> C7H31+ + HC2H							RTYPE=2 K1=6.79E-10 K2=-0.5 K3=0.0 # gg08_rnum=783
H2C2H1+ + C7H2 -> C9H31+ + H2							RTYPE=2 K1=6.79E-10 K2=-0.5 K3=0.0 # gg08_rnum=784
H2C2H1+ + CH4 -> C3H51+ + H2							RTYPE=2 K1=2.20E-10 K2=0.0 K3=0.0 # gg08_rnum=785
H2C2H1+ + H -> HC2H1+ + H2								RTYPE=2 K1=6.80E-11 K2=0.0 K3=0.0 # gg08_rnum=786
H2C2H1+ + H2O -> H3O1+ + HC2H							RTYPE=2 K1=2.20E-09 K2=-0.5 K3=0.0 # gg08_rnum=787
H2C2H1+ + H2S -> H3S1+ + HC2H							RTYPE=2 K1=9.70E-10 K2=-0.5 K3=0.0 # gg08_rnum=788
H2C2H1+ + HC3N -> HC3NH1+ + HC2H						RTYPE=2 K1=8.12E-09 K2=-0.5 K3=0.0 # gg08_rnum=789
H2C2H1+ + HCN -> H2CN1+ + HC2H							RTYPE=2 K1=7.40E-09 K2=-0.5 K3=0.0 # gg08_rnum=790
H2C2H1+ + HNC -> H2CN1+ + HC2H							RTYPE=2 K1=6.74E-09 K2=-0.5 K3=0.0 # gg08_rnum=791
H2C2H1+ + N -> HC2N1+ + H2								RTYPE=2 K1=2.20E-10 K2=0.0 K3=0.0 # gg08_rnum=792
H2C2H1+ + NH3 -> NH41+ + HC2H							RTYPE=2 K1=1.80E-09 K2=-0.5 K3=0.0 # gg08_rnum=793
H2C2H1+ + O -> HCO1+ + CH2								RTYPE=2 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=794
H2C2H1+ + O -> CH2CO1+ + H								RTYPE=2 K1=8.50E-11 K2=0.0 K3=0.0 # gg08_rnum=795
H2C2H1+ + O -> CH31+ + CO								RTYPE=2 K1=5.00E-12 K2=0.0 K3=0.0 # gg08_rnum=796
H2C2H1+ + O -> C2OH1+ + H2								RTYPE=2 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=797
H2C2H1+ + S -> HC2S1+ + H2								RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=798
H2C2H1+ + Si -> SiC2H1+ + H2							RTYPE=2 K1=2.00E-10 K2=0.0 K3=0.0 # gg08_rnum=799
H2C2H1+ + Si -> SiC2H21+ + H							RTYPE=2 K1=2.00E-10 K2=0.0 K3=0.0 # gg08_rnum=800
H2C2H21+ + C -> C3H21+ + H2								RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=801
H2C2H21+ + C -> C3H31+ + H								RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=802
H2C2H21+ + C2H -> C4H31+ + H2							RTYPE=2 K1=1.00E-09 K2=-0.5 K3=0.0 # gg08_rnum=803
H2C2H21+ + C2H -> C3H31+ + CH2							RTYPE=2 K1=1.00E-09 K2=-0.5 K3=0.0 # gg08_rnum=804
H2C2H21+ + HC2H -> C4H51+ + H							RTYPE=2 K1=1.60E-10 K2=0.0 K3=0.0 # gg08_rnum=805
H2C2H21+ + HC2H -> C3H31+ + CH3							RTYPE=2 K1=6.30E-10 K2=0.0 K3=0.0 # gg08_rnum=806
H2C2H21+ + H2C2H -> C4H51+ + H2							RTYPE=2 K1=5.20E-10 K2=-0.5 K3=0.0 # gg08_rnum=807
H2C2H21+ + H2C2H -> H2C2H1+ + H2C2H2					RTYPE=2 K1=5.20E-10 K2=-0.5 K3=0.0 # gg08_rnum=808
H2C2H21+ + H2C2H -> CH3CH21+ + HC2H						RTYPE=2 K1=5.20E-10 K2=-0.5 K3=0.0 # gg08_rnum=809
H2C2H21+ + H2C2H2 -> C4H71+ + H							RTYPE=2 K1=7.20E-11 K2=0.0 K3=0.0 # gg08_rnum=810
H2C2H21+ + H2C2H2 -> C3H51+ + CH3						RTYPE=2 K1=7.30E-10 K2=0.0 K3=0.0 # gg08_rnum=811
H2C2H21+ + C3H -> HC4H1+ + CH3							RTYPE=2 K1=5.34E-09 K2=-0.5 K3=0.0 # gg08_rnum=812
H2C2H21+ + C3H -> C5H41+ + H							RTYPE=2 K1=1.78E-09 K2=-0.5 K3=0.0 # gg08_rnum=813
H2C2H21+ + C3H2 -> C5H51+ + H							RTYPE=2 K1=8.20E-10 K2=-0.5 K3=0.0 # gg08_rnum=814
H2C2H21+ + C3H2 -> C4H31+ + CH3							RTYPE=2 K1=2.46E-09 K2=-0.5 K3=0.0 # gg08_rnum=815
H2C2H21+ + C3H3 -> C5H51+ + H2							RTYPE=2 K1=1.29E-09 K2=-0.5 K3=0.0 # gg08_rnum=816
H2C2H21+ + C3H3 -> C3H31+ + H2C2H2						RTYPE=2 K1=1.29E-09 K2=-0.5 K3=0.0 # gg08_rnum=817
H2C2H21+ + C3H3 -> C4H31+ + CH4							RTYPE=2 K1=1.29E-09 K2=-0.5 K3=0.0 # gg08_rnum=818
H2C2H21+ + C3H4 -> C4H51+ + CH3							RTYPE=2 K1=7.45E-10 K2=-0.5 K3=0.0 # gg08_rnum=819
H2C2H21+ + C4H -> C5H21+ + CH3							RTYPE=2 K1=1.47E-09 K2=-0.5 K3=0.0 # gg08_rnum=820
H2C2H21+ + C4H -> C6H41+ + H							RTYPE=2 K1=4.89E-10 K2=-0.5 K3=0.0 # gg08_rnum=821
H2C2H21+ + HC4H -> C6H41+ + H2							RTYPE=2 K1=5.00E-10 K2=0.0 K3=0.0 # gg08_rnum=822
H2C2H21+ + HC4H -> C5H31+ + CH3							RTYPE=2 K1=5.00E-10 K2=0.0 K3=0.0 # gg08_rnum=823
H2C2H21+ + C5H -> C7H41+ + H							RTYPE=2 K1=2.25E-09 K2=-0.5 K3=0.0 # gg08_rnum=824
H2C2H21+ + C5H -> C6H21+ + CH3							RTYPE=2 K1=6.76E-09 K2=-0.5 K3=0.0 # gg08_rnum=825
H2C2H21+ + C5H2 -> C7H51+ + H							RTYPE=2 K1=5.52E-10 K2=-0.5 K3=0.0 # gg08_rnum=826
H2C2H21+ + C5H2 -> C6H31+ + CH3							RTYPE=2 K1=1.66E-09 K2=-0.5 K3=0.0 # gg08_rnum=827
H2C2H21+ + C6H -> C8H41+ + H							RTYPE=2 K1=2.55E-09 K2=-0.5 K3=0.0 # gg08_rnum=828
H2C2H21+ + C6H -> C7H21+ + CH3							RTYPE=2 K1=7.65E-09 K2=-0.5 K3=0.0 # gg08_rnum=829
H2C2H21+ + C6H2 -> C8H41+ + H2							RTYPE=2 K1=5.00E-10 K2=0.0 K3=0.0 # gg08_rnum=830
H2C2H21+ + C6H2 -> C7H31+ + CH3							RTYPE=2 K1=5.00E-10 K2=0.0 K3=0.0 # gg08_rnum=831
H2C2H21+ + C7H -> C9H41+ + H							RTYPE=2 K1=2.25E-09 K2=-0.5 K3=0.0 # gg08_rnum=832
H2C2H21+ + C7H -> C8H21+ + CH3							RTYPE=2 K1=6.75E-09 K2=-0.5 K3=0.0 # gg08_rnum=833
H2C2H21+ + C7H2 -> C9H41+ + H2							RTYPE=2 K1=5.27E-10 K2=-0.5 K3=0.0 # gg08_rnum=834
H2C2H21+ + C7H2 -> C8H31+ + CH3							RTYPE=2 K1=1.58E-09 K2=-0.5 K3=0.0 # gg08_rnum=835
H2C2H21+ + H -> H2C2H1+ + H2							RTYPE=2 K1=3.00E-10 K2=0.0 K3=0.0 # gg08_rnum=836
H2C2H21+ + HC3N -> HC3NH1+ + H2C2H						RTYPE=2 K1=8.03E-09 K2=-0.5 K3=0.0 # gg08_rnum=837
H2C2H21+ + N -> CH3CN1+ + H								RTYPE=2 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=838
H2C2H21+ + N -> H2C2N1+ + H2							RTYPE=2 K1=2.50E-10 K2=0.0 K3=0.0 # gg08_rnum=839
H2C2H21+ + NH3 -> NH41+ + H2C2H							RTYPE=2 K1=8.75E-10 K2=-0.5 K3=0.0 # gg08_rnum=840
H2C2H21+ + NH3 -> NH31+ + H2C2H2						RTYPE=2 K1=8.75E-10 K2=-0.5 K3=0.0 # gg08_rnum=841
H2C2H21+ + O -> CH3CO1+ + H								RTYPE=2 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=842
H2C2H21+ + O -> HCO1+ + CH3								RTYPE=2 K1=8.40E-11 K2=0.0 K3=0.0 # gg08_rnum=843
H2C2H21+ + O -> CH31+ + HCO								RTYPE=2 K1=1.08E-10 K2=0.0 K3=0.0 # gg08_rnum=844
H2C2H21+ + S -> HC2S1+ + H2 + H							RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=845
H2C2H21+ + Si -> SiC2H21+ + H2							RTYPE=2 K1=2.00E-10 K2=0.0 K3=0.0 # gg08_rnum=846
H2C2H21+ + Si -> SiC2H31+ + H							RTYPE=2 K1=2.00E-10 K2=0.0 K3=0.0 # gg08_rnum=847
CH3CH21+ + C -> C3H31+ + H2								RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=848
CH3CH21+ + C -> C3H41+ + H								RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=849
CH3CH21+ + HC2H -> C3H31+ + CH4							RTYPE=2 K1=6.80E-11 K2=0.0 K3=0.0 # gg08_rnum=850
CH3CH21+ + HC2H -> C4H51+ + H2							RTYPE=2 K1=1.20E-10 K2=0.0 K3=0.0 # gg08_rnum=851
CH3CH21+ + H2C2H2 -> C3H51+ + CH4						RTYPE=2 K1=3.90E-10 K2=0.0 K3=0.0 # gg08_rnum=852
CH3CH21+ + H -> H2C2H21+ + H2							RTYPE=2 K1=1.00E-11 K2=0.0 K3=0.0 # gg08_rnum=853
CH3CH21+ + H2CO -> CH2OH1+ + H2C2H2						RTYPE=2 K1=2.35E-09 K2=-0.5 K3=0.0 # gg08_rnum=854
CH3CH21+ + H2O -> H3O1+ + H2C2H2						RTYPE=2 K1=2.15E-09 K2=-0.5 K3=0.0 # gg08_rnum=855
CH3CH21+ + H2S -> H3S1+ + H2C2H2						RTYPE=2 K1=9.50E-10 K2=-0.5 K3=0.0 # gg08_rnum=856
CH3CH21+ + HCN -> H2CN1+ + H2C2H2						RTYPE=2 K1=7.31E-09 K2=-0.5 K3=0.0 # gg08_rnum=857
CH3CH21+ + HNC -> H2CN1+ + H2C2H2						RTYPE=2 K1=6.62E-09 K2=-0.5 K3=0.0 # gg08_rnum=858
CH3CH21+ + NH3 -> NH41+ + H2C2H2						RTYPE=2 K1=1.74E-09 K2=-0.5 K3=0.0 # gg08_rnum=859
CH3CH21+ + O -> HCO1+ + CH4								RTYPE=2 K1=1.00E-11 K2=0.0 K3=0.0 # gg08_rnum=860
CH3CH21+ + O -> CH3CHO1+ + H							RTYPE=2 K1=1.00E-11 K2=0.0 K3=0.0 # gg08_rnum=861
CH3CH31+ + H -> CH3CH21+ + H2							RTYPE=2 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=862
C2N1+ + HC2H -> H2CN1+ + C3								RTYPE=2 K1=1.30E-10 K2=0.0 K3=0.0 # gg08_rnum=863
C2N1+ + HC2H -> C3H1+ + HCN								RTYPE=2 K1=1.50E-09 K2=0.0 K3=0.0 # gg08_rnum=864
C2N1+ + CH4 -> H2C2H1+ + HCN							RTYPE=2 K1=4.20E-10 K2=0.0 K3=0.0 # gg08_rnum=865
C2N1+ + CH4 -> HC3NH1+ + H2								RTYPE=2 K1=2.10E-10 K2=0.0 K3=0.0 # gg08_rnum=866
C2N1+ + CH4 -> H2CN1+ + HC2H							RTYPE=2 K1=7.00E-11 K2=0.0 K3=0.0 # gg08_rnum=867
C2N1+ + H2 -> H2CN1+ + C								RTYPE=2 K1=8.10E-10 K2=0.0 K3=0.0 # gg08_rnum=868
C2N1+ + H2O -> HCO1+ + HCN								RTYPE=2 K1=1.50E-09 K2=0.0 K3=0.0 # gg08_rnum=869
C2N1+ + H2O -> H2CN1+ + CO								RTYPE=2 K1=1.30E-10 K2=0.0 K3=0.0 # gg08_rnum=870
C2N1+ + H2S -> HCS1+ + HCN								RTYPE=2 K1=1.20E-09 K2=0.0 K3=0.0 # gg08_rnum=871
C2N1+ + NH3 -> H2CN1+ + HCN								RTYPE=2 K1=1.90E-09 K2=0.0 K3=0.0 # gg08_rnum=872
NCCN1+ + H -> C2H1+ + N2								RTYPE=2 K1=1.20E-10 K2=0.0 K3=0.0 # gg08_rnum=873
NCCN1+ + H -> HNC1+ + CN								RTYPE=2 K1=5.00E-10 K2=0.0 K3=0.0 # gg08_rnum=874
C31+ + HC2H -> C5H1+ + H								RTYPE=2 K1=6.00E-10 K2=0.0 K3=0.0 # gg08_rnum=875
C31+ + H -> C3H1+										RTYPE=2 K1=7.00E-16 K2=-1.50E+00 K3=0.0 # gg08_rnum=876
C31+ + H2 -> C3H1+ + H									RTYPE=2 K1=2.40E-10 K2=0.0 K3=0.0 # gg08_rnum=877
C3H1+ + HC2H -> C5H21+ + H								RTYPE=2 K1=3.00E-10 K2=0.0 K3=0.0 # gg08_rnum=878
C3H1+ + H2C2H2 -> C5H31+ + H2							RTYPE=2 K1=5.00E-11 K2=0.0 K3=0.0 # gg08_rnum=879
C3H1+ + H2C2H2 -> C3H31+ + HC2H							RTYPE=2 K1=9.00E-10 K2=0.0 K3=0.0 # gg08_rnum=880
C3H1+ + HC4H -> C5H1+ + HC2H							RTYPE=2 K1=1.20E-10 K2=0.0 K3=0.0 # gg08_rnum=881
C3H1+ + HC4H -> C5H21+ + C2H							RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=882
C3H1+ + CH4 -> H2C2H1+ + HC2H							RTYPE=2 K1=6.12E-10 K2=0.0 K3=0.0 # gg08_rnum=883
C3H1+ + CH4 -> C4H31+ + H2								RTYPE=2 K1=5.50E-11 K2=0.0 K3=0.0 # gg08_rnum=884
C3H1+ + CO2 -> HC3O1+ + CO								RTYPE=2 K1=2.00E-12 K2=0.0 K3=0.0 # gg08_rnum=885
C3H1+ + H -> C3H21+										RTYPE=2 K1=2.00E-14 K2=-1.50E+00 K3=0.0 # gg08_rnum=886
C3H1+ + H2 -> C3H21+ + H								RTYPE=2 K1=1.00E-12 K2=0.0 K3=0.0 # gg08_rnum=887
C3H1+ + H2O -> H2C2H1+ + CO								RTYPE=2 K1=6.87E-10 K2=-0.5 K3=0.0 # gg08_rnum=888
C3H1+ + H2O -> HC3O1+ + H2								RTYPE=2 K1=6.87E-10 K2=-0.5 K3=0.0 # gg08_rnum=889
C3H1+ + H2O -> HCO1+ + HC2H								RTYPE=2 K1=6.87E-10 K2=-0.5 K3=0.0 # gg08_rnum=890
C3H1+ + H2S -> H2C2H1+ + CS								RTYPE=2 K1=2.98E-10 K2=-0.5 K3=0.0 # gg08_rnum=891
C3H1+ + H2S -> HCS1+ + HC2H								RTYPE=2 K1=2.98E-10 K2=-0.5 K3=0.0 # gg08_rnum=892
C3H1+ + H2S -> HC3S1+ + H2								RTYPE=2 K1=2.98E-10 K2=-0.5 K3=0.0 # gg08_rnum=893
C3H1+ + NH3 -> NH41+ + C3								RTYPE=2 K1=5.57E-10 K2=-0.5 K3=0.0 # gg08_rnum=894
C3H1+ + NH3 -> H2NC1+ + HC2H							RTYPE=2 K1=5.57E-10 K2=-0.5 K3=0.0 # gg08_rnum=895
C3H1+ + NH3 -> NH31+ + C3H								RTYPE=2 K1=5.57E-10 K2=-0.5 K3=0.0 # gg08_rnum=896
C3H1+ + NO -> NO1+ + C3H								RTYPE=2 K1=1.73E-10 K2=-0.5 K3=0.0 # gg08_rnum=897
C3H1+ + NO -> HC2N1+ + CO								RTYPE=2 K1=1.73E-10 K2=-0.5 K3=0.0 # gg08_rnum=898
C3H1+ + O2 -> HCO1+ + C2O								RTYPE=2 K1=1.50E-11 K2=0.0 K3=0.0 # gg08_rnum=899
C3H1+ + O2 -> HC3O1+ + O								RTYPE=2 K1=7.50E-12 K2=0.0 K3=0.0 # gg08_rnum=900
C3H1+ + O2 -> C2OH1+ + CO								RTYPE=2 K1=2.50E-12 K2=0.0 K3=0.0 # gg08_rnum=901
C3H1+ + OCS -> HC3O1+ + CS								RTYPE=2 K1=4.57E-10 K2=-0.5 K3=0.0 # gg08_rnum=902
C3H1+ + OCS -> HC3S1+ + CO								RTYPE=2 K1=4.57E-10 K2=-0.5 K3=0.0 # gg08_rnum=903
C3H1+ + OCS -> CS1+ + C3O + H							RTYPE=2 K1=4.57E-10 K2=-0.5 K3=0.0 # gg08_rnum=904
C3H1+ + Si -> SiC31+ + H								RTYPE=2 K1=2.00E-10 K2=0.0 K3=0.0 # gg08_rnum=905
C3H21+ + C -> C4H1+ + H									RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=906
C3H21+ + C2H -> C5H21+ + H								RTYPE=2 K1=1.89E-09 K2=-0.5 K3=0.0 # gg08_rnum=907
C3H21+ + HC2H -> C5H31+ + H								RTYPE=2 K1=5.50E-10 K2=0.0 K3=0.0 # gg08_rnum=908
C3H21+ + H2C2H -> C5H41+ + H							RTYPE=2 K1=7.30E-10 K2=-0.5 K3=0.0 # gg08_rnum=909
C3H21+ + H2C2H -> C5H31+ + H2							RTYPE=2 K1=7.30E-10 K2=-0.5 K3=0.0 # gg08_rnum=910
C3H21+ + H2C2H2 -> C5H51+ + H							RTYPE=2 K1=3.00E-10 K2=0.0 K3=0.0 # gg08_rnum=911
C3H21+ + H2C2H2 -> C4H31+ + CH3							RTYPE=2 K1=3.00E-10 K2=0.0 K3=0.0 # gg08_rnum=912
C3H21+ + C3H -> C6H21+ + H								RTYPE=2 K1=6.57E-09 K2=-0.5 K3=0.0 # gg08_rnum=913
C3H21+ + C3H2 -> C6H31+ + H								RTYPE=2 K1=1.51E-09 K2=-0.5 K3=0.0 # gg08_rnum=914
C3H21+ + C3H2 -> C6H21+ + H2							RTYPE=2 K1=1.51E-09 K2=-0.5 K3=0.0 # gg08_rnum=915
C3H21+ + C3H3 -> C6H41+ + H								RTYPE=2 K1=3.54E-09 K2=-0.5 K3=0.0 # gg08_rnum=916
C3H21+ + C3H4 -> C6H51+ + H								RTYPE=2 K1=1.37E-10 K2=-0.5 K3=0.0 # gg08_rnum=917
C3H21+ + C3H4 -> C4H31+ + H2C2H							RTYPE=2 K1=8.23E-11 K2=-0.5 K3=0.0 # gg08_rnum=918
C3H21+ + C3H4 -> C4H41+ + HC2H							RTYPE=2 K1=2.81E-10 K2=-0.5 K3=0.0 # gg08_rnum=919
C3H21+ + C3H4 -> HC4H1+ + H2C2H2						RTYPE=2 K1=6.17E-11 K2=-0.5 K3=0.0 # gg08_rnum=920
C3H21+ + C3H4 -> C5H31+ + CH3							RTYPE=2 K1=1.24E-10 K2=-0.5 K3=0.0 # gg08_rnum=921
C3H21+ + C4H -> C7H1+ + H2								RTYPE=2 K1=8.90E-10 K2=-0.5 K3=0.0 # gg08_rnum=922
C3H21+ + C4H -> C7H21+ + H								RTYPE=2 K1=8.90E-10 K2=-0.5 K3=0.0 # gg08_rnum=923
C3H21+ + HC4H -> C7H31+ + H								RTYPE=2 K1=3.00E-10 K2=0.0 K3=0.0 # gg08_rnum=924
C3H21+ + HC4H -> C7H21+ + H2							RTYPE=2 K1=3.00E-10 K2=0.0 K3=0.0 # gg08_rnum=925
C3H21+ + C5H -> C8H21+ + H								RTYPE=2 K1=4.08E-09 K2=-0.5 K3=0.0 # gg08_rnum=926
C3H21+ + C5H -> C8H1+ + H2								RTYPE=2 K1=4.08E-09 K2=-0.5 K3=0.0 # gg08_rnum=927
C3H21+ + C5H2 -> C8H21+ + H2							RTYPE=2 K1=1.00E-09 K2=-0.5 K3=0.0 # gg08_rnum=928
C3H21+ + C5H2 -> C8H31+ + H								RTYPE=2 K1=1.00E-09 K2=-0.5 K3=0.0 # gg08_rnum=929
C3H21+ + C6H -> C9H1+ + H2								RTYPE=2 K1=4.59E-09 K2=-0.5 K3=0.0 # gg08_rnum=930
C3H21+ + C6H -> C9H21+ + H								RTYPE=2 K1=4.59E-09 K2=-0.5 K3=0.0 # gg08_rnum=931
C3H21+ + C6H2 -> C9H21+ + H2							RTYPE=2 K1=3.00E-10 K2=0.0 K3=0.0 # gg08_rnum=932
C3H21+ + C6H2 -> C9H31+ + H								RTYPE=2 K1=3.00E-10 K2=0.0 K3=0.0 # gg08_rnum=933
C3H21+ + CH4 -> C4H51+ + H								RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=934
C3H21+ + H -> C3H31+									RTYPE=2 K1=4.00E-15 K2=-1.50E+00 K3=0.0 # gg08_rnum=935
C3H21+ + H -> C3H1+ + H2								RTYPE=2 K1=6.00E-11 K2=0.0 K3=0.0 # gg08_rnum=936
C3H21+ + N -> NC3H1+ + H								RTYPE=2 K1=2.50E-10 K2=0.0 K3=0.0 # gg08_rnum=937
C3H21+ + O -> HCO1+ + C2H								RTYPE=2 K1=2.00E-10 K2=0.0 K3=0.0 # gg08_rnum=938
C3H21+ + P -> PC3H1+ + H								RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=939
C3H21+ + S -> HC3S1+ + H								RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=940
C3H21+ + Si -> SiC31+ + H2								RTYPE=2 K1=2.00E-10 K2=0.0 K3=0.0 # gg08_rnum=941
C3H21+ + Si -> SiC3H1+ + H								RTYPE=2 K1=2.00E-10 K2=0.0 K3=0.0 # gg08_rnum=942
C3H31+ + C -> C4H1+ + H2								RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=943
C3H31+ + C -> HC4H1+ + H								RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=944
C3H31+ + C2H -> C5H21+ + H2								RTYPE=2 K1=9.40E-10 K2=-0.5 K3=0.0 # gg08_rnum=945
C3H31+ + C2H -> C5H31+ + H								RTYPE=2 K1=9.40E-10 K2=-0.5 K3=0.0 # gg08_rnum=946
C3H31+ + HC2H -> C5H31+ + H2							RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=947
C3H31+ + H2C2H -> C5H41+ + H2							RTYPE=2 K1=1.46E-09 K2=-0.5 K3=0.0 # gg08_rnum=948
C3H31+ + H2C2H2 -> C5H51+ + H2							RTYPE=2 K1=1.10E-09 K2=0.0 K3=0.0 # gg08_rnum=949
C3H31+ + C3H -> C6H21+ + H2								RTYPE=2 K1=3.27E-09 K2=-0.5 K3=0.0 # gg08_rnum=950
C3H31+ + C3H -> C6H31+ + H								RTYPE=2 K1=3.27E-09 K2=-0.5 K3=0.0 # gg08_rnum=951
C3H31+ + C3H2 -> C6H41+ + H								RTYPE=2 K1=1.50E-09 K2=-0.5 K3=0.0 # gg08_rnum=952
C3H31+ + C3H2 -> C6H31+ + H2							RTYPE=2 K1=1.50E-09 K2=-0.5 K3=0.0 # gg08_rnum=953
C3H31+ + C3H3 -> C6H41+ + H2							RTYPE=2 K1=1.76E-09 K2=-0.5 K3=0.0 # gg08_rnum=954
C3H31+ + C3H3 -> C6H51+ + H								RTYPE=2 K1=1.76E-09 K2=-0.5 K3=0.0 # gg08_rnum=955
C3H31+ + C3H4 -> C6H51+ + H2							RTYPE=2 K1=6.81E-10 K2=-0.5 K3=0.0 # gg08_rnum=956
C3H31+ + C4H -> C7H21+ + H2								RTYPE=2 K1=8.85E-10 K2=-0.5 K3=0.0 # gg08_rnum=957
C3H31+ + C4H -> C7H31+ + H								RTYPE=2 K1=8.85E-10 K2=-0.5 K3=0.0 # gg08_rnum=958
C3H31+ + HC4H -> C7H31+ + H2							RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=959
C3H31+ + C5H -> C8H31+ + H								RTYPE=2 K1=4.05E-09 K2=-0.5 K3=0.0 # gg08_rnum=960
C3H31+ + C5H -> C8H21+ + H2								RTYPE=2 K1=4.05E-09 K2=-0.5 K3=0.0 # gg08_rnum=961
C3H31+ + C5H2 -> C8H41+ + H								RTYPE=2 K1=9.91E-10 K2=-0.5 K3=0.0 # gg08_rnum=962
C3H31+ + C5H2 -> C8H31+ + H2							RTYPE=2 K1=9.91E-10 K2=-0.5 K3=0.0 # gg08_rnum=963
C3H31+ + C6H -> C9H21+ + H2								RTYPE=2 K1=4.55E-09 K2=-0.5 K3=0.0 # gg08_rnum=964
C3H31+ + C6H -> C9H31+ + H								RTYPE=2 K1=4.55E-09 K2=-0.5 K3=0.0 # gg08_rnum=965
C3H31+ + C6H2 -> C9H31+ + H2							RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=966
C3H31+ + CH4 -> C4H51+ + H2								RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=967
C3H31+ + O -> H2C2H1+ + CO								RTYPE=2 K1=4.50E-11 K2=0.0 K3=0.0 # gg08_rnum=968
C3H31+ + O -> HCO1+ + HC2H								RTYPE=2 K1=2.00E-10 K2=0.0 K3=0.0 # gg08_rnum=969
C3H31+ + S -> HC3S1+ + H2								RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=970
C3H31+ + Si -> SiC3H21+ + H								RTYPE=2 K1=2.00E-10 K2=0.0 K3=0.0 # gg08_rnum=971
C3H31+ + Si -> SiC3H1+ + H2								RTYPE=2 K1=2.00E-10 K2=0.0 K3=0.0 # gg08_rnum=972
C3H41+ + C -> HC4H1+ + H2								RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=973
C3H41+ + C -> C4H31+ + H								RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=974
C3H41+ + HC2H -> C5H51+ + H								RTYPE=2 K1=4.90E-10 K2=0.0 K3=0.0 # gg08_rnum=975
C3H41+ + C3H4 -> C6H71+ + H								RTYPE=2 K1=7.50E-10 K2=0.0 K3=0.0 # gg08_rnum=976
C3H41+ + H -> C3H31+ + H2								RTYPE=2 K1=3.00E-10 K2=0.0 K3=0.0 # gg08_rnum=977
C3H41+ + N -> HC3NH1+ + H2								RTYPE=2 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=978
C3H41+ + N -> C3H3N1+ + H								RTYPE=2 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=979
C3H41+ + O -> HCO1+ + H2C2H								RTYPE=2 K1=2.00E-10 K2=0.0 K3=0.0 # gg08_rnum=980
C3H51+ + C -> C4H31+ + H2								RTYPE=2 K1=2.00E-09 K2=0.0 K3=0.0 # gg08_rnum=981
C3H51+ + C6H6 -> C6H71+ + C3H4							RTYPE=2 K1=1.15E-10 K2=0.0 K3=0.0 # gg08_rnum=982
C3H51+ + N -> C3H3N1+ + H2								RTYPE=2 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=983
C3H51+ + N -> H2C2H21+ + HCN							RTYPE=2 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=984
C3H51+ + O -> HCO1+ + H2C2H2							RTYPE=2 K1=2.00E-10 K2=0.0 K3=0.0 # gg08_rnum=985
NC3H1+ + H2 -> HC3NH1+ + H								RTYPE=2 K1=5.00E-12 K2=0.0 K3=0.0 # gg08_rnum=986
NC3H1+ + H2 -> HC2H1+ + HCN								RTYPE=2 K1=2.00E-12 K2=0.0 K3=0.0 # gg08_rnum=987
C3N1+ + H2 -> NC3H1+ + H								RTYPE=2 K1=1.50E-09 K2=0.0 K3=0.0 # gg08_rnum=988
C41+ + H2 -> C4H1+ + H									RTYPE=2 K1=1.30E-10 K2=0.0 K3=0.0 # gg08_rnum=989
C4H1+ + C -> C51+ + H									RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=990
C4H1+ + C2H -> C61+ + H2								RTYPE=2 K1=9.03E-10 K2=-0.5 K3=0.0 # gg08_rnum=991
C4H1+ + C2H -> C6H1+ + H								RTYPE=2 K1=9.03E-10 K2=-0.5 K3=0.0 # gg08_rnum=992
C4H1+ + HC2H -> C6H21+ + H								RTYPE=2 K1=1.50E-09 K2=0.0 K3=0.0 # gg08_rnum=993
C4H1+ + H2C2H -> C6H31+ + H								RTYPE=2 K1=1.39E-09 K2=-0.5 K3=0.0 # gg08_rnum=994
C4H1+ + H2C2H2 -> C6H41+ + H							RTYPE=2 K1=7.50E-10 K2=0.0 K3=0.0 # gg08_rnum=995
C4H1+ + H2C2H2 -> C4H31+ + HC2H							RTYPE=2 K1=7.50E-10 K2=0.0 K3=0.0 # gg08_rnum=996
C4H1+ + C3H -> C7H1+ + H								RTYPE=2 K1=3.10E-09 K2=-0.5 K3=0.0 # gg08_rnum=997
C4H1+ + C3H -> C71+ + H2								RTYPE=2 K1=3.10E-09 K2=-0.5 K3=0.0 # gg08_rnum=998
C4H1+ + C3H2 -> C7H21+ + H								RTYPE=2 K1=2.85E-09 K2=-0.5 K3=0.0 # gg08_rnum=999
C4H1+ + C3H3 -> C7H31+ + H								RTYPE=2 K1=3.33E-09 K2=-0.5 K3=0.0 # gg08_rnum=1000
C4H1+ + C3H4 -> C7H41+ + H								RTYPE=2 K1=6.45E-10 K2=-0.5 K3=0.0 # gg08_rnum=1001
C4H1+ + C4H -> HC4H1+ + C4								RTYPE=2 K1=8.35E-10 K2=-0.5 K3=0.0 # gg08_rnum=1002
C4H1+ + C4H -> C8H1+ + H								RTYPE=2 K1=8.35E-10 K2=-0.5 K3=0.0 # gg08_rnum=1003
C4H1+ + HC4H -> C8H21+ + H								RTYPE=2 K1=1.60E-09 K2=0.0 K3=0.0 # gg08_rnum=1004
C4H1+ + C5H -> C9H1+ + H								RTYPE=2 K1=7.57E-09 K2=-0.5 K3=0.0 # gg08_rnum=1005
C4H1+ + C5H2 -> C9H21+ + H								RTYPE=2 K1=1.85E-09 K2=-0.5 K3=0.0 # gg08_rnum=1006
C4H1+ + CH4 -> C5H31+ + H2								RTYPE=2 K1=1.10E-09 K2=0.0 K3=0.0 # gg08_rnum=1007
C4H1+ + Fe -> Fe1+ + C4H								RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=1008
C4H1+ + H -> HC4H1+										RTYPE=2 K1=6.00E-14 K2=-1.50E+00 K3=0.0 # gg08_rnum=1009
C4H1+ + H2 -> HC4H1+ + H								RTYPE=2 K1=1.65E-10 K2=0.0 K3=0.0 # gg08_rnum=1010
C4H1+ + Mg -> Mg1+ + C4H								RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=1011
C4H1+ + Na -> Na1+ + C4H								RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=1012
C4H1+ + O -> HCO1+ + C3									RTYPE=2 K1=2.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1013
HC4H1+ + C -> C51+ + H2									RTYPE=2 K1=5.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1014
HC4H1+ + C -> C5H1+ + H									RTYPE=2 K1=5.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1015
HC4H1+ + C2H -> C6H21+ + H								RTYPE=2 K1=1.80E-09 K2=-0.5 K3=0.0 # gg08_rnum=1016
HC4H1+ + HC2H -> C6H31+ + H								RTYPE=2 K1=7.00E-12 K2=0.0 K3=0.0 # gg08_rnum=1017
HC4H1+ + H2C2H -> C6H31+ + H2							RTYPE=2 K1=2.78E-10 K2=-0.5 K3=0.0 # gg08_rnum=1018
HC4H1+ + H2C2H -> C6H41+ + H							RTYPE=2 K1=1.11E-09 K2=-0.5 K3=0.0 # gg08_rnum=1019
HC4H1+ + H2C2H2 -> C6H41+ + H2							RTYPE=2 K1=2.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1020
HC4H1+ + H2C2H2 -> C6H51+ + H							RTYPE=2 K1=8.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1021
HC4H1+ + C3H -> C7H21+ + H								RTYPE=2 K1=6.17E-09 K2=-0.5 K3=0.0 # gg08_rnum=1022
HC4H1+ + C3H2 -> C7H31+ + H								RTYPE=2 K1=2.84E-09 K2=-0.5 K3=0.0 # gg08_rnum=1023
HC4H1+ + C3H3 -> C7H31+ + H2							RTYPE=2 K1=6.63E-10 K2=-0.5 K3=0.0 # gg08_rnum=1024
HC4H1+ + C3H3 -> C7H41+ + H								RTYPE=2 K1=2.65E-09 K2=-0.5 K3=0.0 # gg08_rnum=1025
HC4H1+ + C3H4 -> C7H51+ + H								RTYPE=2 K1=6.42E-10 K2=-0.5 K3=0.0 # gg08_rnum=1026
HC4H1+ + C4H -> C8H21+ + H								RTYPE=2 K1=1.66E-09 K2=-0.5 K3=0.0 # gg08_rnum=1027
HC4H1+ + HC4H -> C8H21+ + H2							RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=1028
HC4H1+ + HC4H -> C6H21+ + HC2H							RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=1029
HC4H1+ + C5H -> C9H21+ + H								RTYPE=2 K1=7.53E-09 K2=-0.5 K3=0.0 # gg08_rnum=1030
HC4H1+ + C5H2 -> C9H31+ + H								RTYPE=2 K1=1.84E-09 K2=-0.5 K3=0.0 # gg08_rnum=1031
HC4H1+ + CH4 -> C5H51+ + H								RTYPE=2 K1=8.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1032
HC4H1+ + CH4 -> C5H41+ + H2								RTYPE=2 K1=2.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1033
HC4H1+ + H -> C4H31+									RTYPE=2 K1=7.00E-11 K2=-1.00E-01 K3=0.0 # gg08_rnum=1034
HC4H1+ + N -> HC4N1+ + H								RTYPE=2 K1=1.00E-18 K2=0.0 K3=0.0 # gg08_rnum=1035
HC4H1+ + O -> C3H21+ + CO								RTYPE=2 K1=1.10E-10 K2=0.0 K3=0.0 # gg08_rnum=1036
HC4H1+ + O -> HCO1+ + C3H								RTYPE=2 K1=2.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1037
HC4H1+ + O -> HC4O1+ + H								RTYPE=2 K1=1.35E-10 K2=0.0 K3=0.0 # gg08_rnum=1038
HC4H1+ + P -> PC4H1+ + H								RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=1039
HC4H1+ + S -> HC4S1+ + H								RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=1040
HC4H1+ + Si -> SiC41+ + H2								RTYPE=2 K1=2.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1041
HC4H1+ + Si -> SiC4H1+ + H								RTYPE=2 K1=2.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1042
C4H31+ + C -> C5H1+ + H2								RTYPE=2 K1=5.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1043
C4H31+ + C -> C5H21+ + H								RTYPE=2 K1=5.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1044
C4H31+ + C2H -> C6H31+ + H								RTYPE=2 K1=1.79E-09 K2=-0.5 K3=0.0 # gg08_rnum=1045
C4H31+ + H2C2H -> C6H41+ + H2							RTYPE=2 K1=6.93E-10 K2=-0.5 K3=0.0 # gg08_rnum=1046
C4H31+ + H2C2H -> C6H51+ + H							RTYPE=2 K1=6.93E-10 K2=-0.5 K3=0.0 # gg08_rnum=1047
C4H31+ + C3H -> C7H31+ + H								RTYPE=2 K1=6.14E-09 K2=-0.5 K3=0.0 # gg08_rnum=1048
C4H31+ + C3H2 -> C7H41+ + H								RTYPE=2 K1=2.82E-09 K2=-0.5 K3=0.0 # gg08_rnum=1049
C4H31+ + C3H3 -> C7H51+ + H								RTYPE=2 K1=1.65E-09 K2=-0.5 K3=0.0 # gg08_rnum=1050
C4H31+ + C3H3 -> C7H41+ + H2							RTYPE=2 K1=1.65E-09 K2=-0.5 K3=0.0 # gg08_rnum=1051
C4H31+ + C3H4 -> C5H51+ + HC2H							RTYPE=2 K1=6.39E-10 K2=-0.5 K3=0.0 # gg08_rnum=1052
C4H31+ + C4H -> C8H31+ + H								RTYPE=2 K1=1.65E-09 K2=-0.5 K3=0.0 # gg08_rnum=1053
C4H31+ + HC4H -> C8H41+ + H								RTYPE=2 K1=5.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1054
C4H31+ + HC4H -> C6H31+ + HC2H							RTYPE=2 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1055
C4H31+ + C5H -> C9H31+ + H								RTYPE=2 K1=7.49E-09 K2=-0.5 K3=0.0 # gg08_rnum=1056
C4H31+ + C5H2 -> C9H31+ + H2							RTYPE=2 K1=9.17E-10 K2=-0.5 K3=0.0 # gg08_rnum=1057
C4H31+ + C5H2 -> C9H41+ + H								RTYPE=2 K1=9.17E-10 K2=-0.5 K3=0.0 # gg08_rnum=1058
C4H31+ + CH4 -> C5H51+ + H2								RTYPE=2 K1=5.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1059
C4H31+ + H -> C4H41+									RTYPE=2 K1=6.00E-14 K2=-7.00E-01 K3=0.0 # gg08_rnum=1060
C4H31+ + N -> H2C4N1+ + H								RTYPE=2 K1=1.00E-18 K2=0.0 K3=0.0 # gg08_rnum=1061
C4H31+ + O -> HCO1+ + C3H2								RTYPE=2 K1=5.00E-11 K2=0.0 K3=0.0 # gg08_rnum=1062
C4H31+ + S -> HC4S1+ + H2								RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=1063
C4H41+ + HC2H -> C6H41+ + H2							RTYPE=2 K1=1.20E-11 K2=0.0 K3=0.0 # gg08_rnum=1064
C4H41+ + HC2H -> C6H51+ + H								RTYPE=2 K1=9.00E-11 K2=0.0 K3=0.0 # gg08_rnum=1065
C4H41+ + HC4H -> C6H41+ + HC2H							RTYPE=2 K1=7.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1066
C4H4N1+ + C -> C5H3N1+ + H								RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=1067
C4H51+ + N -> C4H4N1+ + H								RTYPE=2 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1068
C4N1+ + HC2H -> C5H1+ + HCN								RTYPE=2 K1=8.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1069
C4N1+ + CH4 -> C5H2N1+ + H2								RTYPE=2 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1070
C4N1+ + H2 -> C3H1+ + HCN								RTYPE=2 K1=2.20E-11 K2=0.0 K3=0.0 # gg08_rnum=1071
C4N1+ + H2O -> HC3NH1+ + CO								RTYPE=2 K1=7.50E-10 K2=0.0 K3=0.0 # gg08_rnum=1072
C4N1+ + H2O -> HCO1+ + HC3N								RTYPE=2 K1=7.50E-10 K2=0.0 K3=0.0 # gg08_rnum=1073
C51+ + H2 -> C5H1+ + H									RTYPE=2 K1=6.20E-10 K2=0.0 K3=0.0 # gg08_rnum=1074
C5H1+ + C -> C61+ + H									RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=1075
C5H1+ + H2 -> C5H21+ + H								RTYPE=2 K1=1.00E-17 K2=0.0 K3=0.0 # gg08_rnum=1076
C5H1+ + N -> C5N1+ + H									RTYPE=2 K1=2.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1077
C5H1+ + O -> HCO1+ + C4									RTYPE=2 K1=2.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1078
C5H21+ + C -> C61+ + H2									RTYPE=2 K1=5.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1079
C5H21+ + C -> C6H1+ + H									RTYPE=2 K1=5.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1080
C5H21+ + C2H -> C7H21+ + H								RTYPE=2 K1=1.74E-09 K2=-0.5 K3=0.0 # gg08_rnum=1081
C5H21+ + HC2H -> C7H21+ + H2							RTYPE=2 K1=3.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1082
C5H21+ + HC2H -> C7H31+ + H								RTYPE=2 K1=7.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1083
C5H21+ + H2C2H -> C7H41+ + H							RTYPE=2 K1=4.47E-10 K2=-0.5 K3=0.0 # gg08_rnum=1084
C5H21+ + H2C2H -> C7H31+ + H2							RTYPE=2 K1=8.93E-10 K2=-0.5 K3=0.0 # gg08_rnum=1085
C5H21+ + H2C2H2 -> C7H41+ + H2							RTYPE=2 K1=5.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1086
C5H21+ + H2C2H2 -> C7H51+ + H							RTYPE=2 K1=5.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1087
C5H21+ + C3H -> C8H21+ + H								RTYPE=2 K1=5.91E-09 K2=-0.5 K3=0.0 # gg08_rnum=1088
C5H21+ + C3H2 -> C8H21+ + H2							RTYPE=2 K1=9.10E-10 K2=-0.5 K3=0.0 # gg08_rnum=1089
C5H21+ + C3H2 -> C8H31+ + H								RTYPE=2 K1=1.81E-09 K2=-0.5 K3=0.0 # gg08_rnum=1090
C5H21+ + C3H3 -> C8H31+ + H2							RTYPE=2 K1=1.59E-09 K2=-0.5 K3=0.0 # gg08_rnum=1091
C5H21+ + C3H3 -> C8H41+ + H								RTYPE=2 K1=1.59E-09 K2=-0.5 K3=0.0 # gg08_rnum=1092
C5H21+ + C3H4 -> C8H41+ + H2							RTYPE=2 K1=6.14E-10 K2=-0.5 K3=0.0 # gg08_rnum=1093
C5H21+ + C4H -> C9H21+ + H								RTYPE=2 K1=1.58E-09 K2=-0.5 K3=0.0 # gg08_rnum=1094
C5H21+ + HC4H -> C9H31+ + H								RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=1095
C5H21+ + HC4H -> C7H31+ + C2H							RTYPE=2 K1=6.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1096
C5H21+ + CH4 -> C6H51+ + H								RTYPE=2 K1=8.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1097
C5H21+ + CH4 -> C6H41+ + H2								RTYPE=2 K1=2.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1098
C5H21+ + N -> C5HN1+ + H								RTYPE=2 K1=2.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1099
C5H21+ + O -> HCO1+ + C4H								RTYPE=2 K1=2.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1100
C5H31+ + C -> C6H1+ + H2								RTYPE=2 K1=5.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1101
C5H31+ + C -> C6H21+ + H								RTYPE=2 K1=5.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1102
C5H31+ + N -> C5H2N1+ + H								RTYPE=2 K1=2.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1103
C5H31+ + O -> HCO1+ + HC4H								RTYPE=2 K1=2.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1104
C5H41+ + N -> C5H3N1+ + H								RTYPE=2 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1105
C5H51+ + N -> C5H3N1+ + H2								RTYPE=2 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1106
C5HN1+ + H2 -> C5H2N1+ + H								RTYPE=2 K1=5.00E-12 K2=0.0 K3=0.0 # gg08_rnum=1107
C5N1+ + H2 -> C5HN1+ + H								RTYPE=2 K1=1.50E-09 K2=0.0 K3=0.0 # gg08_rnum=1108
C61+ + H2 -> C6H1+ + H									RTYPE=2 K1=5.40E-11 K2=0.0 K3=0.0 # gg08_rnum=1109
C6H1+ + C -> C71+ + H									RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=1110
C6H1+ + H2 -> C6H21+ + H								RTYPE=2 K1=1.30E-12 K2=0.0 K3=0.0 # gg08_rnum=1111
C6H1+ + O -> HCO1+ + C5									RTYPE=2 K1=2.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1112
C6H21+ + C -> C71+ + H2									RTYPE=2 K1=5.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1113
C6H21+ + C -> C7H1+ + H									RTYPE=2 K1=5.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1114
C6H21+ + C2H -> C8H21+ + H								RTYPE=2 K1=1.70E-09 K2=-0.5 K3=0.0 # gg08_rnum=1115
C6H21+ + H2C2H -> C8H41+ + H							RTYPE=2 K1=4.36E-10 K2=-0.5 K3=0.0 # gg08_rnum=1116
C6H21+ + H2C2H -> C8H31+ + H2							RTYPE=2 K1=8.72E-10 K2=-0.5 K3=0.0 # gg08_rnum=1117
C6H21+ + H2C2H2 -> C8H41+ + H2							RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=1118
C6H21+ + C3H -> C9H21+ + H								RTYPE=2 K1=5.73E-09 K2=-0.5 K3=0.0 # gg08_rnum=1119
C6H21+ + C3H2 -> C9H31+ + H								RTYPE=2 K1=1.76E-09 K2=-0.5 K3=0.0 # gg08_rnum=1120
C6H21+ + C3H2 -> C9H21+ + H2							RTYPE=2 K1=8.80E-10 K2=-0.5 K3=0.0 # gg08_rnum=1121
C6H21+ + C3H3 -> C9H31+ + H2							RTYPE=2 K1=1.54E-09 K2=-0.5 K3=0.0 # gg08_rnum=1122
C6H21+ + C3H3 -> C9H41+ + H								RTYPE=2 K1=1.54E-09 K2=-0.5 K3=0.0 # gg08_rnum=1123
C6H21+ + C3H4 -> C9H41+ + H2							RTYPE=2 K1=5.94E-10 K2=-0.5 K3=0.0 # gg08_rnum=1124
C6H21+ + CH4 -> C7H41+ + H2								RTYPE=2 K1=2.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1125
C6H21+ + CH4 -> C7H51+ + H								RTYPE=2 K1=8.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1126
C6H21+ + O -> HCO1+ + C5H								RTYPE=2 K1=2.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1127
C6H31+ + C -> C7H1+ + H2								RTYPE=2 K1=5.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1128
C6H31+ + C -> C7H21+ + H								RTYPE=2 K1=5.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1129
C6H31+ + O -> HCO1+ + C5H2								RTYPE=2 K1=2.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1130
C6H41+ + H -> C6H51+									RTYPE=2 K1=3.00E-15 K2=-1.50E+00 K3=0.0 # gg08_rnum=1131
C6H4N1+ + C -> C7H3N1+ + H								RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=1132
C6H51+ + H2C2H2 -> C6H71+ + HC2H						RTYPE=2 K1=8.50E-11 K2=0.0 K3=0.0 # gg08_rnum=1133
C6H51+ + H2 -> C6H71+									RTYPE=2 K1=6.00E-11 K2=0.0 K3=0.0 # gg08_rnum=1134
C6H51+ + N -> C6H4N1+ + H								RTYPE=2 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1135
C6H51+ + O -> C5H51+ + CO								RTYPE=2 K1=6.00E-11 K2=0.0 K3=0.0 # gg08_rnum=1136
C6H51+ + O2 -> C4H51+ + CO + CO							RTYPE=2 K1=4.90E-11 K2=0.0 K3=0.0 # gg08_rnum=1137
C71+ + H2 -> C7H1+ + H									RTYPE=2 K1=1.90E-10 K2=0.0 K3=0.0 # gg08_rnum=1138
C7H1+ + C -> C81+ + H									RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=1139
C7H1+ + H2 -> C7H21+ + H								RTYPE=2 K1=1.00E-17 K2=0.0 K3=0.0 # gg08_rnum=1140
C7H1+ + N -> C7N1+ + H									RTYPE=2 K1=2.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1141
C7H1+ + O -> HCO1+ + C6									RTYPE=2 K1=2.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1142
C7H21+ + C -> C81+ + H2									RTYPE=2 K1=5.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1143
C7H21+ + C -> C8H1+ + H									RTYPE=2 K1=5.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1144
C7H21+ + C2H -> C9H21+ + H								RTYPE=2 K1=1.67E-09 K2=-0.5 K3=0.0 # gg08_rnum=1145
C7H21+ + HC2H -> C9H21+ + H2							RTYPE=2 K1=3.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1146
C7H21+ + HC2H -> C9H31+ + H								RTYPE=2 K1=7.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1147
C7H21+ + H2C2H -> C9H41+ + H							RTYPE=2 K1=4.27E-10 K2=-0.5 K3=0.0 # gg08_rnum=1148
C7H21+ + H2C2H -> C9H31+ + H2							RTYPE=2 K1=8.53E-10 K2=-0.5 K3=0.0 # gg08_rnum=1149
C7H21+ + H2C2H2 -> C9H41+ + H2							RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=1150
C7H21+ + CH4 -> C8H41+ + H2								RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=1151
C7H21+ + N -> C7HN1+ + H								RTYPE=2 K1=2.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1152
C7H21+ + O -> HCO1+ + C6H								RTYPE=2 K1=2.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1153
C7H31+ + C -> C8H21+ + H								RTYPE=2 K1=5.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1154
C7H31+ + C -> C8H1+ + H2								RTYPE=2 K1=5.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1155
C7H31+ + N -> C7H2N1+ + H								RTYPE=2 K1=2.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1156
C7H31+ + O -> HCO1+ + C6H2								RTYPE=2 K1=2.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1157
C7H41+ + N -> C7H3N1+ + H								RTYPE=2 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1158
C7H51+ + N -> C7H3N1+ + H2								RTYPE=2 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1159
C7HN1+ + H2 -> C7H2N1+ + H								RTYPE=2 K1=5.00E-12 K2=0.0 K3=0.0 # gg08_rnum=1160
C7N1+ + H2 -> C7HN1+ + H								RTYPE=2 K1=1.50E-09 K2=0.0 K3=0.0 # gg08_rnum=1161
C81+ + H2 -> C8H1+ + H									RTYPE=2 K1=4.70E-12 K2=0.0 K3=0.0 # gg08_rnum=1162
C8H1+ + H2 -> C8H21+ + H								RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=1163
C8H21+ + C -> C91+ + H2									RTYPE=2 K1=5.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1164
C8H21+ + C -> C9H1+ + H									RTYPE=2 K1=5.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1165
C8H21+ + CH4 -> C9H41+ + H2								RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=1166
C8H21+ + O -> HCO1+ + C7H								RTYPE=2 K1=2.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1167
C8H31+ + C -> C9H1+ + H2								RTYPE=2 K1=5.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1168
C8H31+ + C -> C9H21+ + H								RTYPE=2 K1=5.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1169
C8H31+ + O -> HCO1+ + C7H2								RTYPE=2 K1=2.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1170
C8H4N1+ + C -> C9H3N1+ + H								RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=1171
C91+ + H2 -> C9H1+ + H									RTYPE=2 K1=4.10E-11 K2=0.0 K3=0.0 # gg08_rnum=1172
C9H1+ + C -> C101+ + H									RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=1173
C9H1+ + H2 -> C9H21+ + H								RTYPE=2 K1=1.00E-17 K2=0.0 K3=0.0 # gg08_rnum=1174
C9H1+ + N -> C9N1+ + H									RTYPE=2 K1=2.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1175
C9H1+ + O -> HCO1+ + C8									RTYPE=2 K1=2.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1176
C9H21+ + C -> C101+ + H2								RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=1177
C9H21+ + N -> C9HN1+ + H								RTYPE=2 K1=2.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1178
C9H21+ + O -> HCO1+ + C8H								RTYPE=2 K1=2.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1179
C9H31+ + N -> C9H2N1+ + H								RTYPE=2 K1=2.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1180
C9H31+ + O -> HCO1+ + C8H2								RTYPE=2 K1=2.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1181
C9H41+ + N -> C9H3N1+ + H								RTYPE=2 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1182
C9HN1+ + H2 -> C9H2N1+ + H								RTYPE=2 K1=5.00E-12 K2=0.0 K3=0.0 # gg08_rnum=1183
C9N1+ + H2 -> C9HN1+ + H								RTYPE=2 K1=1.50E-09 K2=0.0 K3=0.0 # gg08_rnum=1184
CH1+ + C -> C21+ + H									RTYPE=2 K1=1.20E-09 K2=0.0 K3=0.0 # gg08_rnum=1185
CH1+ + C2 -> C31+ + H									RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=1186
CH1+ + C2H -> C31+ + H2									RTYPE=2 K1=9.80E-10 K2=0.0 K3=0.0 # gg08_rnum=1187
CH1+ + HC2H -> C3H21+ + H								RTYPE=2 K1=2.40E-09 K2=0.0 K3=0.0 # gg08_rnum=1188
CH1+ + CH -> C21+ + H2									RTYPE=2 K1=7.40E-10 K2=0.0 K3=0.0 # gg08_rnum=1189
CH1+ + CH2 -> C2H1+ + H2								RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=1190
CH1+ + CH3OH -> H2CO1+ + CH3							RTYPE=2 K1=1.45E-09 K2=0.0 K3=0.0 # gg08_rnum=1191
CH1+ + CH3OH -> CH3O1+ + CH2							RTYPE=2 K1=2.90E-10 K2=0.0 K3=0.0 # gg08_rnum=1192
CH1+ + CH3OH -> CH3OH21+ + C							RTYPE=2 K1=1.16E-09 K2=0.0 K3=0.0 # gg08_rnum=1193
CH1+ + CH4 -> HC2H1+ + H2 + H							RTYPE=2 K1=1.40E-10 K2=0.0 K3=0.0 # gg08_rnum=1194
CH1+ + CH4 -> H2C2H1+ + H2								RTYPE=2 K1=1.10E-09 K2=0.0 K3=0.0 # gg08_rnum=1195
CH1+ + CH4 -> H2C2H21+ + H								RTYPE=2 K1=6.50E-11 K2=0.0 K3=0.0 # gg08_rnum=1196
CH1+ + CN -> C2N1+ + H									RTYPE=2 K1=1.10E-09 K2=0.0 K3=0.0 # gg08_rnum=1197
CH1+ + CN -> CNC1+ + H									RTYPE=2 K1=5.50E-10 K2=0.0 K3=0.0 # gg08_rnum=1198
CH1+ + CO2 -> HCO1+ + CO								RTYPE=2 K1=1.60E-09 K2=0.0 K3=0.0 # gg08_rnum=1199
CH1+ + Fe -> Fe1+ + CH									RTYPE=2 K1=2.60E-10 K2=0.0 K3=0.0 # gg08_rnum=1200
CH1+ + H -> C1+ + H2									RTYPE=2 K1=7.50E-10 K2=0.0 K3=0.0 # gg08_rnum=1201
CH1+ + H2 -> CH21+ + H									RTYPE=2 K1=1.20E-09 K2=0.0 K3=0.0 # gg08_rnum=1202
CH1+ + H2CO -> CH2OH1+ + C								RTYPE=2 K1=9.60E-10 K2=0.0 K3=0.0 # gg08_rnum=1203
CH1+ + H2CO -> HCO1+ + CH2								RTYPE=2 K1=9.60E-10 K2=0.0 K3=0.0 # gg08_rnum=1204
CH1+ + H2CO -> CH31+ + CO								RTYPE=2 K1=9.60E-10 K2=0.0 K3=0.0 # gg08_rnum=1205
CH1+ + H2O -> HCO1+ + H2								RTYPE=2 K1=2.90E-09 K2=0.0 K3=0.0 # gg08_rnum=1206
CH1+ + H2O -> H2CO1+ + H								RTYPE=2 K1=5.80E-10 K2=0.0 K3=0.0 # gg08_rnum=1207
CH1+ + H2O -> H3O1+ + C									RTYPE=2 K1=5.80E-10 K2=0.0 K3=0.0 # gg08_rnum=1208
CH1+ + H2S -> H3S1+ + C									RTYPE=2 K1=6.30E-10 K2=0.0 K3=0.0 # gg08_rnum=1209
CH1+ + H2S -> HCS1+ + H2								RTYPE=2 K1=6.60E-10 K2=0.0 K3=0.0 # gg08_rnum=1210
CH1+ + HCN -> C2N1+ + H2								RTYPE=2 K1=3.60E-10 K2=0.0 K3=0.0 # gg08_rnum=1211
CH1+ + HCN -> HC2N1+ + H								RTYPE=2 K1=2.40E-10 K2=0.0 K3=0.0 # gg08_rnum=1212
CH1+ + HCN -> H2CN1+ + C								RTYPE=2 K1=2.40E-09 K2=0.0 K3=0.0 # gg08_rnum=1213
CH1+ + HCO -> HCO1+ + CH								RTYPE=2 K1=4.60E-10 K2=0.0 K3=0.0 # gg08_rnum=1214
CH1+ + HCO -> CH21+ + CO								RTYPE=2 K1=4.60E-10 K2=0.0 K3=0.0 # gg08_rnum=1215
CH1+ + HNC -> H2CN1+ + C								RTYPE=2 K1=1.80E-09 K2=0.0 K3=0.0 # gg08_rnum=1216
CH1+ + Mg -> Mg1+ + CH									RTYPE=2 K1=3.60E-10 K2=0.0 K3=0.0 # gg08_rnum=1217
CH1+ + N -> CN1+ + H									RTYPE=2 K1=1.90E-10 K2=0.0 K3=0.0 # gg08_rnum=1218
CH1+ + Na -> Na1+ + CH									RTYPE=2 K1=3.50E-10 K2=0.0 K3=0.0 # gg08_rnum=1219
CH1+ + NH -> CN1+ + H2									RTYPE=2 K1=7.60E-10 K2=0.0 K3=0.0 # gg08_rnum=1220
CH1+ + NH2 -> HCN1+ + H2								RTYPE=2 K1=1.10E-09 K2=0.0 K3=0.0 # gg08_rnum=1221
CH1+ + NH3 -> H2NC1+ + H2								RTYPE=2 K1=1.84E-09 K2=0.0 K3=0.0 # gg08_rnum=1222
CH1+ + NH3 -> NH31+ + CH								RTYPE=2 K1=4.59E-10 K2=0.0 K3=0.0 # gg08_rnum=1223
CH1+ + NH3 -> NH41+ + C									RTYPE=2 K1=4.05E-10 K2=0.0 K3=0.0 # gg08_rnum=1224
CH1+ + NO -> NO1+ + CH									RTYPE=2 K1=7.60E-10 K2=0.0 K3=0.0 # gg08_rnum=1225
CH1+ + O -> CO1+ + H									RTYPE=2 K1=3.50E-10 K2=0.0 K3=0.0 # gg08_rnum=1226
CH1+ + O2 -> CO1+ + OH									RTYPE=2 K1=1.00E-11 K2=0.0 K3=0.0 # gg08_rnum=1227
CH1+ + O2 -> HCO1+ + O									RTYPE=2 K1=9.70E-10 K2=0.0 K3=0.0 # gg08_rnum=1228
CH1+ + O2 -> HCO + O1+									RTYPE=2 K1=1.00E-11 K2=0.0 K3=0.0 # gg08_rnum=1229
CH1+ + OCS -> HOCS1+ + C								RTYPE=2 K1=8.55E-10 K2=0.0 K3=0.0 # gg08_rnum=1230
CH1+ + OCS -> HCS1+ + CO								RTYPE=2 K1=1.05E-09 K2=0.0 K3=0.0 # gg08_rnum=1231
CH1+ + OH -> CO1+ + H2									RTYPE=2 K1=7.50E-10 K2=0.0 K3=0.0 # gg08_rnum=1232
CH1+ + S -> CS1+ + H									RTYPE=2 K1=4.70E-10 K2=0.0 K3=0.0 # gg08_rnum=1233
CH1+ + S -> HS1+ + C									RTYPE=2 K1=4.70E-10 K2=0.0 K3=0.0 # gg08_rnum=1234
CH1+ + S -> S1+ + CH									RTYPE=2 K1=4.70E-10 K2=0.0 K3=0.0 # gg08_rnum=1235
CH1+ + Si -> Si1+ + CH									RTYPE=2 K1=2.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1236
CH21+ + C -> C2H1+ + H									RTYPE=2 K1=1.20E-09 K2=0.0 K3=0.0 # gg08_rnum=1237
CH21+ + C2 -> C3H1+ + H									RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=1238
CH21+ + C2H -> C3H21+ + H								RTYPE=2 K1=9.50E-10 K2=0.0 K3=0.0 # gg08_rnum=1239
CH21+ + CH -> HC2H1+ + H								RTYPE=2 K1=7.20E-10 K2=0.0 K3=0.0 # gg08_rnum=1240
CH21+ + CH2 -> H2C2H1+ + H								RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=1241
CH21+ + CH4 -> CH3CH21+ + H								RTYPE=2 K1=3.60E-10 K2=0.0 K3=0.0 # gg08_rnum=1242
CH21+ + CH4 -> H2C2H21+ + H2							RTYPE=2 K1=9.10E-10 K2=0.0 K3=0.0 # gg08_rnum=1243
CH21+ + CO2 -> H2CO1+ + CO								RTYPE=2 K1=1.20E-09 K2=0.0 K3=0.0 # gg08_rnum=1244
CH21+ + H2 -> CH31+ + H									RTYPE=2 K1=1.20E-09 K2=0.0 K3=0.0 # gg08_rnum=1245
CH21+ + H2CO -> CH3CO1+ + H								RTYPE=2 K1=3.30E-10 K2=0.0 K3=0.0 # gg08_rnum=1246
CH21+ + H2CO -> HCO1+ + CH3								RTYPE=2 K1=2.80E-09 K2=0.0 K3=0.0 # gg08_rnum=1247
CH21+ + H2CO -> CH2CO1+ + H2							RTYPE=2 K1=1.65E-10 K2=0.0 K3=0.0 # gg08_rnum=1248
CH21+ + H2O -> CH2OH1+ + H								RTYPE=2 K1=1.20E-09 K2=0.0 K3=0.0 # gg08_rnum=1249
CH21+ + H2S -> HCS1+ + H2 + H							RTYPE=2 K1=1.10E-09 K2=0.0 K3=0.0 # gg08_rnum=1250
CH21+ + H2S -> H3CS1+ + H								RTYPE=2 K1=1.60E-09 K2=0.0 K3=0.0 # gg08_rnum=1251
CH21+ + HCN -> H2C2N1+ + H								RTYPE=2 K1=1.80E-09 K2=0.0 K3=0.0 # gg08_rnum=1252
CH21+ + HCO -> CH31+ + CO								RTYPE=2 K1=4.50E-10 K2=0.0 K3=0.0 # gg08_rnum=1253
CH21+ + HCO -> CH2CO1+ + H								RTYPE=2 K1=4.50E-10 K2=0.0 K3=0.0 # gg08_rnum=1254
CH21+ + N -> HCN1+ + H									RTYPE=2 K1=2.20E-10 K2=0.0 K3=0.0 # gg08_rnum=1255
CH21+ + NH -> H2CN1+ + H								RTYPE=2 K1=7.50E-10 K2=0.0 K3=0.0 # gg08_rnum=1256
CH21+ + NH2 -> H2CN1+ + H2								RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=1257
CH21+ + NH3 -> NH41+ + CH								RTYPE=2 K1=1.30E-09 K2=0.0 K3=0.0 # gg08_rnum=1258
CH21+ + NH3 -> CH3NH1+ + H								RTYPE=2 K1=1.50E-09 K2=0.0 K3=0.0 # gg08_rnum=1259
CH21+ + NO -> NO1+ + CH2								RTYPE=2 K1=4.20E-10 K2=0.0 K3=0.0 # gg08_rnum=1260
CH21+ + O -> HCO1+ + H									RTYPE=2 K1=7.50E-10 K2=0.0 K3=0.0 # gg08_rnum=1261
CH21+ + O2 -> HCO1+ + OH								RTYPE=2 K1=9.10E-10 K2=0.0 K3=0.0 # gg08_rnum=1262
CH21+ + O2 -> HOCO1+ + H								RTYPE=2 K1=4.70E-10 K2=0.0 K3=0.0 # gg08_rnum=1263
CH21+ + OCS -> H2CS1+ + CO								RTYPE=2 K1=7.20E-10 K2=0.0 K3=0.0 # gg08_rnum=1264
CH21+ + OCS -> HCS1+ + HCO								RTYPE=2 K1=1.08E-09 K2=0.0 K3=0.0 # gg08_rnum=1265
CH21+ + OH -> H2CO1+ + H								RTYPE=2 K1=7.40E-10 K2=0.0 K3=0.0 # gg08_rnum=1266
CH21+ + S -> HCS1+ + H									RTYPE=2 K1=1.40E-09 K2=0.0 K3=0.0 # gg08_rnum=1267
CH2OH1+ + CH -> CH21+ + H2CO							RTYPE=2 K1=4.40E-09 K2=-0.5 K3=0.0 # gg08_rnum=1268
CH2OH1+ + H2CO -> CH2OHOCH21+							RTYPE=2 K1=8.10E-15 K2=-3.00E+00 K3=0.0 # gg08_rnum=1269
CH2OH1+ + HNC -> H2CN1+ + H2CO							RTYPE=2 K1=6.52E-09 K2=-0.5 K3=0.0 # gg08_rnum=1270
CH2OH1+ + Na -> Na1+ + H2CO + H							RTYPE=2 K1=2.60E-09 K2=0.0 K3=0.0 # gg08_rnum=1271
CH2OH1+ + NH2 -> NH31+ + H2CO							RTYPE=2 K1=2.20E-09 K2=-0.5 K3=0.0 # gg08_rnum=1272
CH2OH1+ + NH3 -> NH41+ + H2CO							RTYPE=2 K1=1.70E-09 K2=-0.5 K3=0.0 # gg08_rnum=1273
SiCH21+ + C -> SiC2H1+ + H								RTYPE=2 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1274
SiCH21+ + C -> Si1+ + HC2H								RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=1275
SiCH21+ + C -> SiC21+ + H2								RTYPE=2 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1276
SiCH21+ + N -> Si1+ + HCN + H							RTYPE=2 K1=7.60E-10 K2=0.0 K3=0.0 # gg08_rnum=1277
SiCH21+ + O -> Si1+ + H2CO								RTYPE=2 K1=6.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1278
SiCH21+ + O -> HCO1+ + SiH								RTYPE=2 K1=3.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1279
SiCH21+ + S -> Si1+ + H2CS								RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=1280
CH31+ + C -> C2H1+ + H2									RTYPE=2 K1=1.20E-09 K2=0.0 K3=0.0 # gg08_rnum=1281
CH31+ + C -> HC2H1+ + H									RTYPE=2 K1=1.20E-09 K2=0.0 K3=0.0 # gg08_rnum=1282
CH31+ + C2 -> C3H1+ + H2								RTYPE=2 K1=9.90E-10 K2=0.0 K3=0.0 # gg08_rnum=1283
CH31+ + C2H -> C3H21+ + H2								RTYPE=2 K1=1.20E-09 K2=-0.5 K3=0.0 # gg08_rnum=1284
CH31+ + C2H -> C3H31+ + H								RTYPE=2 K1=1.20E-09 K2=-0.5 K3=0.0 # gg08_rnum=1285
CH31+ + HC2H -> C3H31+ + H2								RTYPE=2 K1=1.20E-09 K2=0.0 K3=0.0 # gg08_rnum=1286
CH31+ + H2C2H -> C3H31+ + H + H2						RTYPE=2 K1=1.90E-10 K2=-0.5 K3=0.0 # gg08_rnum=1287
CH31+ + H2C2H -> C3H41+ + H2							RTYPE=2 K1=1.10E-09 K2=-0.5 K3=0.0 # gg08_rnum=1288
CH31+ + H2C2H -> H2C2H1+ + CH3							RTYPE=2 K1=5.60E-10 K2=-0.5 K3=0.0 # gg08_rnum=1289
CH31+ + H2C2H2 -> C3H31+ + H2 + H2						RTYPE=2 K1=4.60E-11 K2=0.0 K3=0.0 # gg08_rnum=1290
CH31+ + H2C2H2 -> C3H51+ + H2							RTYPE=2 K1=5.20E-10 K2=0.0 K3=0.0 # gg08_rnum=1291
CH31+ + H2C2H2 -> H2C2H1+ + CH4							RTYPE=2 K1=3.50E-10 K2=0.0 K3=0.0 # gg08_rnum=1292
CH31+ + C3H -> C3H1+ + CH3								RTYPE=2 K1=4.35E-09 K2=-0.5 K3=0.0 # gg08_rnum=1293
CH31+ + C3H -> HC4H1+ + H2								RTYPE=2 K1=4.35E-09 K2=-0.5 K3=0.0 # gg08_rnum=1294
CH31+ + C3H2 -> C4H31+ + H2								RTYPE=2 K1=4.02E-09 K2=-0.5 K3=0.0 # gg08_rnum=1295
CH31+ + C3H3 -> C4H51+ + H								RTYPE=2 K1=4.71E-09 K2=-0.5 K3=0.0 # gg08_rnum=1296
CH31+ + C4H -> C5H21+ + H2								RTYPE=2 K1=2.44E-09 K2=-0.5 K3=0.0 # gg08_rnum=1297
CH31+ + HC4H -> C3H31+ + HC2H							RTYPE=2 K1=1.20E-09 K2=0.0 K3=0.0 # gg08_rnum=1298
CH31+ + HC4H -> C5H31+ + H2								RTYPE=2 K1=1.30E-10 K2=0.0 K3=0.0 # gg08_rnum=1299
CH31+ + C5H -> C6H21+ + H2								RTYPE=2 K1=1.14E-08 K2=-0.5 K3=0.0 # gg08_rnum=1300
CH31+ + C5H2 -> C6H31+ + H2								RTYPE=2 K1=2.79E-09 K2=-0.5 K3=0.0 # gg08_rnum=1301
CH31+ + C6H -> C7H21+ + H2								RTYPE=2 K1=1.30E-08 K2=-0.5 K3=0.0 # gg08_rnum=1302
CH31+ + C6H2 -> C7H31+ + H2								RTYPE=2 K1=1.20E-09 K2=0.0 K3=0.0 # gg08_rnum=1303
CH31+ + C7H -> C8H21+ + H2								RTYPE=2 K1=1.16E-08 K2=-0.5 K3=0.0 # gg08_rnum=1304
CH31+ + C7H2 -> C8H31+ + H2								RTYPE=2 K1=2.71E-09 K2=-0.5 K3=0.0 # gg08_rnum=1305
CH31+ + C8H -> C9H21+ + H2								RTYPE=2 K1=1.27E-08 K2=-0.5 K3=0.0 # gg08_rnum=1306
CH31+ + C8H2 -> C9H31+ + H2								RTYPE=2 K1=1.20E-09 K2=0.0 K3=0.0 # gg08_rnum=1307
CH31+ + CH -> HC2H1+ + H2								RTYPE=2 K1=5.10E-09 K2=-0.5 K3=0.0 # gg08_rnum=1308
CH31+ + CH2 -> H2C2H1+ + H2								RTYPE=2 K1=8.21E-10 K2=-0.5 K3=0.0 # gg08_rnum=1309
CH31+ + CH3OH -> CH3O1+ + CH4							RTYPE=2 K1=2.10E-09 K2=-0.5 K3=0.0 # gg08_rnum=1310
CH31+ + CH4 -> CH3CH21+ + H2							RTYPE=2 K1=9.60E-10 K2=0.0 K3=0.0 # gg08_rnum=1311
CH31+ + CN -> H2C2N1+ + H								RTYPE=2 K1=4.30E-09 K2=-0.5 K3=0.0 # gg08_rnum=1312
CH31+ + Fe -> Fe1+ + CH3								RTYPE=2 K1=2.40E-09 K2=0.0 K3=0.0 # gg08_rnum=1313
CH31+ + H2CO -> HCO1+ + CH4								RTYPE=2 K1=2.90E-09 K2=-0.5 K3=0.0 # gg08_rnum=1314
CH31+ + H2S -> H3CS1+ + H2								RTYPE=2 K1=1.20E-09 K2=-0.5 K3=0.0 # gg08_rnum=1315
CH31+ + HC2NC -> C3H31+ + HCN							RTYPE=2 K1=6.32E-09 K2=-0.5 K3=0.0 # gg08_rnum=1316
CH31+ + HC3N -> C3H31+ + HCN							RTYPE=2 K1=1.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=1317
CH31+ + ClH -> ClCH21+ + H2								RTYPE=2 K1=3.04E-09 K2=-0.5 K3=0.0 # gg08_rnum=1318
CH31+ + C2NCH -> C3H31+ + HCN							RTYPE=2 K1=2.20E-08 K2=-0.5 K3=0.0 # gg08_rnum=1319
CH31+ + HCO -> CH41+ + CO								RTYPE=2 K1=6.10E-10 K2=-0.5 K3=0.0 # gg08_rnum=1320
CH31+ + HCO -> HCO1+ + CH3								RTYPE=2 K1=6.10E-10 K2=-0.5 K3=0.0 # gg08_rnum=1321
CH31+ + HNC3 -> C3H31+ + HNC							RTYPE=2 K1=5.67E-09 K2=-0.5 K3=0.0 # gg08_rnum=1322
CH31+ + HS -> H2CS1+ + H2								RTYPE=2 K1=2.20E-09 K2=-0.5 K3=0.0 # gg08_rnum=1323
CH31+ + Mg -> Mg1+ + CH3								RTYPE=2 K1=3.50E-09 K2=0.0 K3=0.0 # gg08_rnum=1324
CH31+ + N -> H2NC1+ + H									RTYPE=2 K1=6.70E-11 K2=0.0 K3=0.0 # gg08_rnum=1325
CH31+ + N -> HCN1+ + H2									RTYPE=2 K1=6.70E-11 K2=0.0 K3=0.0 # gg08_rnum=1326
CH31+ + Na -> Na1+ + CH3								RTYPE=2 K1=3.40E-09 K2=0.0 K3=0.0 # gg08_rnum=1327
CH31+ + NH -> H2CN1+ + H2								RTYPE=2 K1=4.40E-09 K2=-0.5 K3=0.0 # gg08_rnum=1328
CH31+ + NH2 -> CH3NH1+ + H								RTYPE=2 K1=2.60E-09 K2=-0.5 K3=0.0 # gg08_rnum=1329
CH31+ + NH2CHO -> CH3NH21+ + HCO						RTYPE=2 K1=1.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=1330
CH31+ + NH3 -> CH3NH1+ + H2								RTYPE=2 K1=1.72E-09 K2=-0.5 K3=0.0 # gg08_rnum=1331
CH31+ + NH3 -> NH41+ + CH2								RTYPE=2 K1=3.00E-10 K2=-0.5 K3=0.0 # gg08_rnum=1332
CH31+ + NO -> NO1+ + CH3								RTYPE=2 K1=4.44E-10 K2=-0.5 K3=0.0 # gg08_rnum=1333
CH31+ + O -> H31+ + CO									RTYPE=2 K1=8.80E-11 K2=0.0 K3=0.0 # gg08_rnum=1334
CH31+ + O -> HCO1+ + H2									RTYPE=2 K1=2.05E-10 K2=0.0 K3=0.0 # gg08_rnum=1335
CH31+ + O -> HOC1+ + H2									RTYPE=2 K1=2.05E-10 K2=0.0 K3=0.0 # gg08_rnum=1336
CH31+ + O -> H2CO1+ + H									RTYPE=2 K1=1.00E-15 K2=0.0 K3=0.0 # gg08_rnum=1337
CH31+ + O2 -> CH3O1+ + O								RTYPE=2 K1=5.00E-12 K2=0.0 K3=0.0 # gg08_rnum=1338
CH31+ + OCS -> H3CS1+ + CO								RTYPE=2 K1=1.90E-09 K2=-0.5 K3=0.0 # gg08_rnum=1339
CH31+ + OH -> H2CO1+ + H2								RTYPE=2 K1=5.40E-09 K2=-0.5 K3=0.0 # gg08_rnum=1340
CH31+ + P -> PCH21+ + H									RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=1341
CH31+ + S -> HCS1+ + H2									RTYPE=2 K1=1.40E-09 K2=0.0 K3=0.0 # gg08_rnum=1342
CH31+ + Si -> SiCH1+ + H2								RTYPE=2 K1=2.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1343
CH31+ + Si -> SiCH21+ + H								RTYPE=2 K1=9.70E-10 K2=0.0 K3=0.0 # gg08_rnum=1344
CH31+ + Si -> Si1+ + CH3								RTYPE=2 K1=9.70E-10 K2=0.0 K3=0.0 # gg08_rnum=1345
CH31+ + SiH -> SiCH21+ + H2								RTYPE=2 K1=3.50E-10 K2=-0.5 K3=0.0 # gg08_rnum=1346
CH31+ + SO -> HOCS1+ + H2								RTYPE=2 K1=4.20E-09 K2=-0.5 K3=0.0 # gg08_rnum=1347
CH3CN1+ + CO -> HCO1+ + H2C2N							RTYPE=2 K1=2.00E-09 K2=0.0 K3=0.0 # gg08_rnum=1348
CH3O1+ + CH -> CH21+ + H2CO								RTYPE=2 K1=4.40E-09 K2=-0.5 K3=0.0 # gg08_rnum=1349
CH3O1+ + H2CO -> CH2OHOCH21+							RTYPE=2 K1=8.10E-15 K2=-3.00E+00 K3=0.0 # gg08_rnum=1350
CH3O1+ + HNC -> H2CN1+ + H2CO							RTYPE=2 K1=6.52E-09 K2=-0.5 K3=0.0 # gg08_rnum=1351
CH3O1+ + Na -> Na1+ + H2CO + H							RTYPE=2 K1=2.60E-09 K2=0.0 K3=0.0 # gg08_rnum=1352
CH3O1+ + NH2 -> NH31+ + H2CO							RTYPE=2 K1=2.20E-09 K2=-0.5 K3=0.0 # gg08_rnum=1353
CH3O1+ + NH3 -> NH41+ + H2CO							RTYPE=2 K1=1.70E-09 K2=-0.5 K3=0.0 # gg08_rnum=1354
CH3OH21+ + CH3OH -> CH3OCH41+ + H2O						RTYPE=2 K1=1.00E-10 K2=-1.00E+00 K3=0.0 # gg08_rnum=1355
CH3OH21+ + H2CO -> HCOOCH3H1+ + H2						RTYPE=2 K1=0.0 K2=0.0 K3=0.0 # gg08_rnum=1356
CH41+ + HC2H -> HC2H1+ + CH4							RTYPE=2 K1=1.10E-09 K2=0.0 K3=0.0 # gg08_rnum=1357
CH41+ + HC2H -> H2C2H1+ + CH3							RTYPE=2 K1=1.40E-09 K2=0.0 K3=0.0 # gg08_rnum=1358
CH41+ + HC2H -> C3H31+ + H2 + H							RTYPE=2 K1=1.25E-10 K2=0.0 K3=0.0 # gg08_rnum=1359
CH41+ + H2C2H2 -> H2C2H21+ + CH4						RTYPE=2 K1=1.38E-09 K2=0.0 K3=0.0 # gg08_rnum=1360
CH41+ + H2C2H2 -> CH3CH21+ + CH3						RTYPE=2 K1=4.23E-10 K2=0.0 K3=0.0 # gg08_rnum=1361
CH41+ + CH3OH -> CH3OH1+ + CH4							RTYPE=2 K1=1.80E-09 K2=0.0 K3=0.0 # gg08_rnum=1362
CH41+ + CH3OH -> CH3OH21+ + CH3							RTYPE=2 K1=1.20E-09 K2=0.0 K3=0.0 # gg08_rnum=1363
CH41+ + CH4 -> CH51+ + CH3								RTYPE=2 K1=1.50E-09 K2=0.0 K3=0.0 # gg08_rnum=1364
CH41+ + CO -> HCO1+ + CH3								RTYPE=2 K1=1.40E-09 K2=0.0 K3=0.0 # gg08_rnum=1365
CH41+ + CO2 -> HOCO1+ + CH3								RTYPE=2 K1=1.20E-09 K2=0.0 K3=0.0 # gg08_rnum=1366
CH41+ + H -> CH31+ + H2									RTYPE=2 K1=1.00E-11 K2=0.0 K3=0.0 # gg08_rnum=1367
CH41+ + H2 -> CH51+ + H									RTYPE=2 K1=3.50E-11 K2=0.0 K3=0.0 # gg08_rnum=1368
CH41+ + H2CO -> CH2OH1+ + CH3							RTYPE=2 K1=1.98E-09 K2=0.0 K3=0.0 # gg08_rnum=1369
CH41+ + H2CO -> H2CO1+ + CH4							RTYPE=2 K1=1.62E-09 K2=0.0 K3=0.0 # gg08_rnum=1370
CH41+ + H2O -> H3O1+ + CH3								RTYPE=2 K1=2.50E-09 K2=0.0 K3=0.0 # gg08_rnum=1371
CH41+ + H2S -> H2S1+ + CH4								RTYPE=2 K1=1.60E-09 K2=0.0 K3=0.0 # gg08_rnum=1372
CH41+ + H2S -> H3S1+ + CH3								RTYPE=2 K1=9.50E-10 K2=0.0 K3=0.0 # gg08_rnum=1373
CH41+ + NH3 -> CH51+ + NH2								RTYPE=2 K1=3.00E-11 K2=0.0 K3=0.0 # gg08_rnum=1374
CH41+ + NH3 -> NH31+ + CH4								RTYPE=2 K1=6.90E-10 K2=0.0 K3=0.0 # gg08_rnum=1375
CH41+ + NH3 -> NH41+ + CH3								RTYPE=2 K1=6.60E-10 K2=0.0 K3=0.0 # gg08_rnum=1376
CH41+ + O -> CH31+ + OH									RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=1377
CH41+ + O2 -> O21+ + CH4								RTYPE=2 K1=4.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1378
CH41+ + OCS -> OCS1+ + CH4								RTYPE=2 K1=4.20E-10 K2=0.0 K3=0.0 # gg08_rnum=1379
CH41+ + OCS -> HOCS1+ + CH3								RTYPE=2 K1=9.80E-10 K2=0.0 K3=0.0 # gg08_rnum=1380
CH51+ + C -> CH1+ + CH4									RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=1381
CH51+ + C -> H2C2H1+ + H2								RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=1382
CH51+ + C -> H2C2H21+ + H								RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=1383
CH51+ + C2 -> C2H1+ + CH4								RTYPE=2 K1=9.50E-10 K2=0.0 K3=0.0 # gg08_rnum=1384
CH51+ + C2H -> HC2H1+ + CH4								RTYPE=2 K1=2.30E-09 K2=-0.5 K3=0.0 # gg08_rnum=1385
CH51+ + HC2H -> H2C2H1+ + CH4							RTYPE=2 K1=1.60E-09 K2=0.0 K3=0.0 # gg08_rnum=1386
CH51+ + C6H6 -> C6H71+ + CH4							RTYPE=2 K1=2.00E-09 K2=0.0 K3=0.0 # gg08_rnum=1387
CH51+ + CH -> CH21+ + CH4								RTYPE=2 K1=4.90E-09 K2=-0.5 K3=0.0 # gg08_rnum=1388
CH51+ + CH2 -> CH31+ + CH4								RTYPE=2 K1=7.97E-10 K2=-0.5 K3=0.0 # gg08_rnum=1389
CH51+ + CO -> HCO1+ + CH4								RTYPE=2 K1=3.16E-10 K2=-0.5 K3=0.0 # gg08_rnum=1390
CH51+ + H -> CH41+ + H2									RTYPE=2 K1=1.50E-10 K2=0.0 K3=4.81E+02 # gg08_rnum=1391
CH51+ + H2CO -> CH2OH1+ + CH4							RTYPE=2 K1=2.70E-09 K2=-0.5 K3=0.0 # gg08_rnum=1392
CH51+ + H2O -> H3O1+ + CH4								RTYPE=2 K1=2.40E-09 K2=-0.5 K3=0.0 # gg08_rnum=1393
CH51+ + HC3N -> HC3NH1+ + CH4							RTYPE=2 K1=9.55E-09 K2=-0.5 K3=0.0 # gg08_rnum=1394
CH51+ + HCN -> H2CN1+ + CH4								RTYPE=2 K1=8.50E-09 K2=-0.5 K3=0.0 # gg08_rnum=1395
CH51+ + HCO -> H2CO1+ + CH4								RTYPE=2 K1=1.20E-09 K2=-0.5 K3=0.0 # gg08_rnum=1396
CH51+ + HNC -> H2CN1+ + CH4								RTYPE=2 K1=7.67E-09 K2=-0.5 K3=0.0 # gg08_rnum=1397
CH51+ + Mg -> Mg1+ + CH4 + H							RTYPE=2 K1=1.40E-09 K2=0.0 K3=0.0 # gg08_rnum=1398
CH51+ + NH -> NH21+ + CH4								RTYPE=2 K1=4.20E-09 K2=-0.5 K3=0.0 # gg08_rnum=1399
CH51+ + NH2 -> NH31+ + CH4								RTYPE=2 K1=2.50E-09 K2=-0.5 K3=0.0 # gg08_rnum=1400
CH51+ + NH3 -> NH41+ + CH4								RTYPE=2 K1=2.00E-09 K2=-0.5 K3=0.0 # gg08_rnum=1401
CH51+ + O -> H3O1+ + CH2								RTYPE=2 K1=2.20E-10 K2=0.0 K3=0.0 # gg08_rnum=1402
CH51+ + O -> CH3O1+ + H2								RTYPE=2 K1=4.40E-12 K2=0.0 K3=0.0 # gg08_rnum=1403
CH51+ + OH -> H2O1+ + CH4								RTYPE=2 K1=5.20E-09 K2=-0.5 K3=0.0 # gg08_rnum=1404
CH51+ + S -> HS1+ + CH4									RTYPE=2 K1=1.30E-09 K2=0.0 K3=0.0 # gg08_rnum=1405
CH51+ + Si -> SiCH41+ + H								RTYPE=2 K1=2.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1406
CH51+ + Si -> SiCH31+ + H2								RTYPE=2 K1=2.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1407
SiCH1+ + H2 -> SiCH21+ + H								RTYPE=2 K1=1.50E-09 K2=0.0 K3=0.0 # gg08_rnum=1408
Cl1+ + H2 -> ClH1+ + H									RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=1409
Cl1+ + O2 -> O21+ + Cl									RTYPE=2 K1=4.60E-10 K2=0.0 K3=0.0 # gg08_rnum=1410
CN1+ + C -> C1+ + CN									RTYPE=2 K1=1.10E-10 K2=0.0 K3=0.0 # gg08_rnum=1411
CN1+ + C2 -> C21+ + CN									RTYPE=2 K1=8.50E-10 K2=0.0 K3=0.0 # gg08_rnum=1412
CN1+ + C2H -> C2H1+ + CN								RTYPE=2 K1=8.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1413
CN1+ + CH -> CH1+ + CN									RTYPE=2 K1=6.40E-10 K2=0.0 K3=0.0 # gg08_rnum=1414
CN1+ + CH2 -> CH21+ + CN								RTYPE=2 K1=8.80E-10 K2=0.0 K3=0.0 # gg08_rnum=1415
CN1+ + CO -> CO1+ + CN									RTYPE=2 K1=6.30E-10 K2=0.0 K3=0.0 # gg08_rnum=1416
CN1+ + CO2 -> OCN1+ + CO								RTYPE=2 K1=2.25E-10 K2=0.0 K3=0.0 # gg08_rnum=1417
CN1+ + CO2 -> C2O1+ + NO								RTYPE=2 K1=2.25E-10 K2=0.0 K3=0.0 # gg08_rnum=1418
CN1+ + CO2 -> CO21+ + CN								RTYPE=2 K1=4.50E-10 K2=0.0 K3=0.0 # gg08_rnum=1419
CN1+ + H -> H1+ + CN									RTYPE=2 K1=6.40E-10 K2=0.0 K3=0.0 # gg08_rnum=1420
CN1+ + H2 -> HCN1+ + H									RTYPE=2 K1=7.50E-10 K2=0.0 K3=0.0 # gg08_rnum=1421
CN1+ + H2 -> HNC1+ + H									RTYPE=2 K1=7.50E-10 K2=0.0 K3=0.0 # gg08_rnum=1422
CN1+ + H2CO -> HCO1+ + HCN								RTYPE=2 K1=5.20E-10 K2=0.0 K3=0.0 # gg08_rnum=1423
CN1+ + H2CO -> H2CO1+ + CN								RTYPE=2 K1=5.20E-10 K2=0.0 K3=0.0 # gg08_rnum=1424
CN1+ + H2O -> HCO1+ + NH								RTYPE=2 K1=1.60E-10 K2=0.0 K3=0.0 # gg08_rnum=1425
CN1+ + H2O -> H2NC1+ + O								RTYPE=2 K1=4.80E-10 K2=0.0 K3=0.0 # gg08_rnum=1426
CN1+ + H2O -> HNCO1+ + H								RTYPE=2 K1=6.40E-10 K2=0.0 K3=0.0 # gg08_rnum=1427
CN1+ + H2O -> HCN1+ + OH								RTYPE=2 K1=1.60E-09 K2=0.0 K3=0.0 # gg08_rnum=1428
CN1+ + HCN -> NCCN1+ + H								RTYPE=2 K1=3.15E-10 K2=0.0 K3=0.0 # gg08_rnum=1429
CN1+ + HCN -> HCN1+ + CN								RTYPE=2 K1=2.40E-09 K2=0.0 K3=0.0 # gg08_rnum=1430
CN1+ + HCO -> HCO1+ + CN								RTYPE=2 K1=3.70E-10 K2=0.0 K3=0.0 # gg08_rnum=1431
CN1+ + HCO -> HCN1+ + CO								RTYPE=2 K1=3.70E-10 K2=0.0 K3=0.0 # gg08_rnum=1432
CN1+ + NH -> NH1+ + CN									RTYPE=2 K1=6.50E-10 K2=0.0 K3=0.0 # gg08_rnum=1433
CN1+ + NH2 -> NH21+ + CN								RTYPE=2 K1=9.10E-10 K2=0.0 K3=0.0 # gg08_rnum=1434
CN1+ + NO -> OCN1+ + N									RTYPE=2 K1=1.90E-10 K2=0.0 K3=0.0 # gg08_rnum=1435
CN1+ + NO -> NO1+ + CN									RTYPE=2 K1=8.10E-10 K2=0.0 K3=0.0 # gg08_rnum=1436
CN1+ + O -> O1+ + CN									RTYPE=2 K1=6.50E-11 K2=0.0 K3=0.0 # gg08_rnum=1437
CN1+ + O2 -> OCN1+ + O									RTYPE=2 K1=8.60E-11 K2=0.0 K3=0.0 # gg08_rnum=1438
CN1+ + O2 -> NO1+ + CO									RTYPE=2 K1=8.60E-11 K2=0.0 K3=0.0 # gg08_rnum=1439
CN1+ + O2 -> O21+ + CN									RTYPE=2 K1=7.80E-10 K2=0.0 K3=0.0 # gg08_rnum=1440
CN1+ + OH -> OH1+ + CN									RTYPE=2 K1=6.40E-10 K2=0.0 K3=0.0 # gg08_rnum=1441
CN1+ + S -> S1+ + CN									RTYPE=2 K1=1.10E-09 K2=0.0 K3=0.0 # gg08_rnum=1442
CNC1+ + HC2H -> H2CN1+ + C3								RTYPE=2 K1=5.60E-11 K2=0.0 K3=0.0 # gg08_rnum=1443
CNC1+ + HC2H -> C3H1+ + HCN								RTYPE=2 K1=6.40E-10 K2=0.0 K3=0.0 # gg08_rnum=1444
CNC1+ + H2O -> HCO1+ + HCN								RTYPE=2 K1=6.40E-11 K2=0.0 K3=0.0 # gg08_rnum=1445
CNC1+ + NH3 -> H2CN1+ + HCN								RTYPE=2 K1=1.90E-09 K2=0.0 K3=0.0 # gg08_rnum=1446
CO1+ + C -> C1+ + CO									RTYPE=2 K1=1.10E-10 K2=0.0 K3=0.0 # gg08_rnum=1447
CO1+ + C2 -> C21+ + CO									RTYPE=2 K1=8.40E-10 K2=0.0 K3=0.0 # gg08_rnum=1448
CO1+ + C2H -> C2H1+ + CO								RTYPE=2 K1=3.90E-10 K2=0.0 K3=0.0 # gg08_rnum=1449
CO1+ + C2H -> HCO1+ + C2								RTYPE=2 K1=3.90E-10 K2=0.0 K3=0.0 # gg08_rnum=1450
CO1+ + CH -> CH1+ + CO									RTYPE=2 K1=3.20E-10 K2=0.0 K3=0.0 # gg08_rnum=1451
CO1+ + CH -> HCO1+ + C									RTYPE=2 K1=3.20E-10 K2=0.0 K3=0.0 # gg08_rnum=1452
CO1+ + CH2 -> CH21+ + CO								RTYPE=2 K1=4.30E-10 K2=0.0 K3=0.0 # gg08_rnum=1453
CO1+ + CH2 -> HCO1+ + CH								RTYPE=2 K1=4.30E-10 K2=0.0 K3=0.0 # gg08_rnum=1454
CO1+ + CH4 -> CH41+ + CO								RTYPE=2 K1=7.93E-10 K2=0.0 K3=0.0 # gg08_rnum=1455
CO1+ + CH4 -> CH3CO1+ + H								RTYPE=2 K1=5.20E-11 K2=0.0 K3=0.0 # gg08_rnum=1456
CO1+ + CH4 -> HCO1+ + CH3								RTYPE=2 K1=4.55E-10 K2=0.0 K3=0.0 # gg08_rnum=1457
CO1+ + CO2 -> CO21+ + CO								RTYPE=2 K1=1.10E-09 K2=0.0 K3=0.0 # gg08_rnum=1458
CO1+ + H -> H1+ + CO									RTYPE=2 K1=4.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1459
CO1+ + H2 -> HOC1+ + H									RTYPE=2 K1=7.50E-10 K2=0.0 K3=0.0 # gg08_rnum=1460
CO1+ + H2 -> HCO1+ + H									RTYPE=2 K1=7.50E-10 K2=0.0 K3=0.0 # gg08_rnum=1461
CO1+ + H2CO -> H2CO1+ + CO								RTYPE=2 K1=1.40E-09 K2=0.0 K3=0.0 # gg08_rnum=1462
CO1+ + H2CO -> HCO1+ + HCO								RTYPE=2 K1=1.70E-09 K2=0.0 K3=0.0 # gg08_rnum=1463
CO1+ + H2O -> HCO1+ + OH								RTYPE=2 K1=8.80E-10 K2=0.0 K3=0.0 # gg08_rnum=1464
CO1+ + H2O -> H2O1+ + CO								RTYPE=2 K1=1.70E-09 K2=0.0 K3=0.0 # gg08_rnum=1465
CO1+ + H2S -> H2S1+ + CO								RTYPE=2 K1=2.44E-09 K2=0.0 K3=0.0 # gg08_rnum=1466
CO1+ + H2S -> HCO1+ + HS								RTYPE=2 K1=1.56E-10 K2=0.0 K3=0.0 # gg08_rnum=1467
CO1+ + HCN -> HCN1+ + CO								RTYPE=2 K1=3.40E-10 K2=0.0 K3=0.0 # gg08_rnum=1468
CO1+ + HCO -> HCO1+ + CO								RTYPE=2 K1=7.40E-10 K2=0.0 K3=0.0 # gg08_rnum=1469
CO1+ + N -> NO1+ + C									RTYPE=2 K1=8.10E-11 K2=0.0 K3=0.0 # gg08_rnum=1470
CO1+ + NH -> NH1+ + CO									RTYPE=2 K1=3.20E-10 K2=0.0 K3=0.0 # gg08_rnum=1471
CO1+ + NH -> HCO1+ + N									RTYPE=2 K1=3.20E-10 K2=0.0 K3=0.0 # gg08_rnum=1472
CO1+ + NH2 -> HCO1+ + NH								RTYPE=2 K1=4.50E-10 K2=0.0 K3=0.0 # gg08_rnum=1473
CO1+ + NH2 -> NH21+ + CO								RTYPE=2 K1=4.50E-10 K2=0.0 K3=0.0 # gg08_rnum=1474
CO1+ + NH3 -> HCO1+ + NH2								RTYPE=2 K1=4.12E-11 K2=0.0 K3=0.0 # gg08_rnum=1475
CO1+ + NH3 -> NH31+ + CO								RTYPE=2 K1=2.02E-09 K2=0.0 K3=0.0 # gg08_rnum=1476
CO1+ + NO -> NO1+ + CO									RTYPE=2 K1=3.30E-10 K2=0.0 K3=0.0 # gg08_rnum=1477
CO1+ + O -> O1+ + CO									RTYPE=2 K1=1.40E-10 K2=0.0 K3=0.0 # gg08_rnum=1478
CO1+ + O2 -> O21+ + CO									RTYPE=2 K1=1.20E-10 K2=0.0 K3=0.0 # gg08_rnum=1479
CO1+ + OH -> OH1+ + CO									RTYPE=2 K1=3.10E-10 K2=0.0 K3=0.0 # gg08_rnum=1480
CO1+ + OH -> HCO1+ + O									RTYPE=2 K1=3.10E-10 K2=0.0 K3=0.0 # gg08_rnum=1481
CO1+ + S -> S1+ + CO									RTYPE=2 K1=1.10E-09 K2=0.0 K3=0.0 # gg08_rnum=1482
CO1+ + SO2 -> SO1+ + CO2								RTYPE=2 K1=1.70E-09 K2=0.0 K3=0.0 # gg08_rnum=1483
CO21+ + CH4 -> HOCO1+ + CH3								RTYPE=2 K1=5.50E-10 K2=0.0 K3=0.0 # gg08_rnum=1484
CO21+ + CH4 -> CH41+ + CO2								RTYPE=2 K1=5.50E-10 K2=0.0 K3=0.0 # gg08_rnum=1485
CO21+ + H -> H1+ + CO2									RTYPE=2 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1486
CO21+ + H -> HCO1+ + O									RTYPE=2 K1=2.90E-10 K2=0.0 K3=0.0 # gg08_rnum=1487
CO21+ + H2 -> HOCO1+ + H								RTYPE=2 K1=8.70E-10 K2=0.0 K3=0.0 # gg08_rnum=1488
CO21+ + H2O -> H2O1+ + CO2								RTYPE=2 K1=2.04E-09 K2=0.0 K3=0.0 # gg08_rnum=1489
CO21+ + H2O -> HOCO1+ + OH								RTYPE=2 K1=7.56E-10 K2=0.0 K3=0.0 # gg08_rnum=1490
CO21+ + H2S -> H2S1+ + CO2								RTYPE=2 K1=1.40E-09 K2=0.0 K3=0.0 # gg08_rnum=1491
CO21+ + NH3 -> NH31+ + CO2								RTYPE=2 K1=1.98E-09 K2=0.0 K3=0.0 # gg08_rnum=1492
CO21+ + NO -> NO1+ + CO2								RTYPE=2 K1=1.20E-10 K2=0.0 K3=0.0 # gg08_rnum=1493
CO21+ + O -> O1+ + CO2									RTYPE=2 K1=9.62E-11 K2=0.0 K3=0.0 # gg08_rnum=1494
CO21+ + O -> O21+ + CO									RTYPE=2 K1=1.64E-10 K2=0.0 K3=0.0 # gg08_rnum=1495
CO21+ + O2 -> O21+ + CO2								RTYPE=2 K1=5.00E-11 K2=0.0 K3=0.0 # gg08_rnum=1496
CP1+ + H2 -> HCP1+ + H									RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=1497
CP1+ + O -> P1+ + CO									RTYPE=2 K1=2.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1498
CS1+ + C -> C1+ + CS									RTYPE=2 K1=1.60E-09 K2=0.0 K3=0.0 # gg08_rnum=1499
CS1+ + CH4 -> HCS1+ + CH3								RTYPE=2 K1=5.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1500
CS1+ + Fe -> Fe1+ + CS									RTYPE=2 K1=1.70E-10 K2=0.0 K3=0.0 # gg08_rnum=1501
CS1+ + H2 -> HCS1+ + H									RTYPE=2 K1=4.80E-10 K2=0.0 K3=0.0 # gg08_rnum=1502
CS1+ + Mg -> Mg1+ + CS									RTYPE=2 K1=2.70E-10 K2=0.0 K3=0.0 # gg08_rnum=1503
CS1+ + Na -> Na1+ + CS									RTYPE=2 K1=2.30E-10 K2=0.0 K3=0.0 # gg08_rnum=1504
CS1+ + O -> CO1+ + S									RTYPE=2 K1=6.00E-11 K2=0.0 K3=0.0 # gg08_rnum=1505
CS1+ + O2 -> OCS1+ + O									RTYPE=2 K1=1.30E-10 K2=0.0 K3=0.0 # gg08_rnum=1506
CS1+ + Si -> Si1+ + CS									RTYPE=2 K1=1.50E-10 K2=0.0 K3=0.0 # gg08_rnum=1507
Fe1+ + Na -> Na1+ + Fe									RTYPE=2 K1=1.00E-11 K2=0.0 K3=0.0 # gg08_rnum=1508
H1+ + C2 -> C21+ + H									RTYPE=2 K1=3.10E-09 K2=0.0 K3=0.0 # gg08_rnum=1509
H1+ + C2H -> C2H1+ + H									RTYPE=2 K1=3.70E-09 K2=-0.5 K3=0.0 # gg08_rnum=1510
H1+ + C2H -> C21+ + H2									RTYPE=2 K1=3.70E-09 K2=-0.5 K3=0.0 # gg08_rnum=1511
H1+ + HC2H -> C2H1+ + H2								RTYPE=2 K1=2.00E-09 K2=0.0 K3=0.0 # gg08_rnum=1512
H1+ + HC2H -> HC2H1+ + H								RTYPE=2 K1=2.00E-09 K2=0.0 K3=0.0 # gg08_rnum=1513
H1+ + H2C2H -> HC2H1+ + H2								RTYPE=2 K1=3.00E-09 K2=-0.5 K3=0.0 # gg08_rnum=1514
H1+ + H2C2H -> H2C2H1+ + H								RTYPE=2 K1=3.00E-09 K2=-0.5 K3=0.0 # gg08_rnum=1515
H1+ + H2C2H2 -> H2C2H1+ + H2							RTYPE=2 K1=2.00E-09 K2=0.0 K3=0.0 # gg08_rnum=1516
H1+ + H2C2H2 -> H2C2H21+ + H							RTYPE=2 K1=2.00E-09 K2=0.0 K3=0.0 # gg08_rnum=1517
H1+ + CH3CH2 -> CH3CH21+ + H							RTYPE=2 K1=1.97E-09 K2=-0.5 K3=0.0 # gg08_rnum=1518
H1+ + CH3CH2 -> H2C2H21+ + H2							RTYPE=2 K1=1.97E-09 K2=-0.5 K3=0.0 # gg08_rnum=1519
H1+ + CH3CH2CHO -> CH3CH2CHO1+ + H						RTYPE=2 K1=1.09E-08 K2=-0.5 K3=0.0 # gg08_rnum=1520
H1+ + CH3CH2OH -> CH3CH2O1+ + H2						RTYPE=2 K1=3.30E-09 K2=-0.5 K3=0.0 # gg08_rnum=1521
H1+ + CH3CH2OH -> CH3CH2OH1+ + H						RTYPE=2 K1=3.30E-09 K2=-0.5 K3=0.0 # gg08_rnum=1522
H1+ + CH3CH3 -> H2C2H1+ + H2 + H2						RTYPE=2 K1=1.30E-09 K2=0.0 K3=0.0 # gg08_rnum=1523
H1+ + CH3CH3 -> H2C2H21+ + H2 + H						RTYPE=2 K1=1.30E-09 K2=0.0 K3=0.0 # gg08_rnum=1524
H1+ + CH3CH3 -> CH3CH21+ + H2							RTYPE=2 K1=1.30E-09 K2=0.0 K3=0.0 # gg08_rnum=1525
H1+ + C2N -> C2N1+ + H									RTYPE=2 K1=5.56E-09 K2=-0.5 K3=0.0 # gg08_rnum=1526
H1+ + C2O -> C2O1+ + H									RTYPE=2 K1=1.20E-08 K2=-0.5 K3=0.0 # gg08_rnum=1527
H1+ + C2S -> C2S1+ + H									RTYPE=2 K1=1.09E-08 K2=-0.5 K3=0.0 # gg08_rnum=1528
H1+ + C3 -> C31+ + H									RTYPE=2 K1=4.00E-09 K2=0.0 K3=0.0 # gg08_rnum=1529
H1+ + C3H -> C31+ + H2									RTYPE=2 K1=1.40E-08 K2=-0.5 K3=0.0 # gg08_rnum=1530
H1+ + C3H -> C3H1+ + H									RTYPE=2 K1=1.40E-08 K2=-0.5 K3=0.0 # gg08_rnum=1531
H1+ + C3H2 -> C3H1+ + H2								RTYPE=2 K1=6.50E-09 K2=-0.5 K3=0.0 # gg08_rnum=1532
H1+ + C3H2 -> C3H21+ + H								RTYPE=2 K1=6.50E-09 K2=-0.5 K3=0.0 # gg08_rnum=1533
H1+ + C3H3 -> C3H21+ + H2								RTYPE=2 K1=7.80E-09 K2=-0.5 K3=0.0 # gg08_rnum=1534
H1+ + C3H3 -> C3H31+ + H								RTYPE=2 K1=7.80E-09 K2=-0.5 K3=0.0 # gg08_rnum=1535
H1+ + C3H3N -> C3H3N1+ + H								RTYPE=2 K1=7.50E-09 K2=-0.5 K3=0.0 # gg08_rnum=1536
H1+ + C3H3N -> HC3NH1+ + H2								RTYPE=2 K1=7.50E-09 K2=-0.5 K3=0.0 # gg08_rnum=1537
H1+ + C3H4 -> C3H41+ + H								RTYPE=2 K1=1.50E-09 K2=-0.5 K3=0.0 # gg08_rnum=1538
H1+ + C3H4 -> C3H31+ + H2								RTYPE=2 K1=1.50E-09 K2=-0.5 K3=0.0 # gg08_rnum=1539
H1+ + C3O -> C3O1+ + H									RTYPE=2 K1=2.21E-08 K2=-0.5 K3=0.0 # gg08_rnum=1540
H1+ + C3S -> C3S1+ + H									RTYPE=2 K1=1.52E-08 K2=-0.5 K3=0.0 # gg08_rnum=1541
H1+ + C4 -> C41+ + H									RTYPE=2 K1=4.00E-09 K2=0.0 K3=0.0 # gg08_rnum=1542
H1+ + C4H -> C41+ + H2									RTYPE=2 K1=4.20E-09 K2=-0.5 K3=0.0 # gg08_rnum=1543
H1+ + C4H -> C4H1+ + H									RTYPE=2 K1=4.20E-09 K2=-0.5 K3=0.0 # gg08_rnum=1544
H1+ + HC4H -> C4H1+ + H2								RTYPE=2 K1=2.00E-09 K2=0.0 K3=0.0 # gg08_rnum=1545
H1+ + HC4H -> HC4H1+ + H								RTYPE=2 K1=2.00E-09 K2=0.0 K3=0.0 # gg08_rnum=1546
H1+ + C4H3 -> C4H31+ + H								RTYPE=2 K1=2.00E-09 K2=-0.5 K3=0.0 # gg08_rnum=1547
H1+ + C4H3 -> HC4H1+ + H2								RTYPE=2 K1=2.00E-09 K2=-0.5 K3=0.0 # gg08_rnum=1548
H1+ + C4H4 -> C4H41+ + H								RTYPE=2 K1=7.81E-10 K2=-0.5 K3=0.0 # gg08_rnum=1549
H1+ + C4H4 -> C4H31+ + H2								RTYPE=2 K1=7.81E-10 K2=-0.5 K3=0.0 # gg08_rnum=1550
H1+ + C4P -> C4P1+ + H									RTYPE=2 K1=1.94E-09 K2=-0.5 K3=0.0 # gg08_rnum=1551
H1+ + C4S -> C4S1+ + H									RTYPE=2 K1=1.17E-08 K2=-0.5 K3=0.0 # gg08_rnum=1552
H1+ + C5 -> C51+ + H									RTYPE=2 K1=4.00E-09 K2=0.0 K3=0.0 # gg08_rnum=1553
H1+ + C5H -> C51+ + H2									RTYPE=2 K1=1.98E-08 K2=-0.5 K3=0.0 # gg08_rnum=1554
H1+ + C5H -> C5H1+ + H									RTYPE=2 K1=1.98E-08 K2=-0.5 K3=0.0 # gg08_rnum=1555
H1+ + C5H2 -> C5H21+ + H								RTYPE=2 K1=4.87E-09 K2=-0.5 K3=0.0 # gg08_rnum=1556
H1+ + C5H2 -> C5H1+ + H2								RTYPE=2 K1=4.87E-09 K2=-0.5 K3=0.0 # gg08_rnum=1557
H1+ + C5H4 -> C5H31+ + H2								RTYPE=2 K1=1.52E-09 K2=-0.5 K3=0.0 # gg08_rnum=1558
H1+ + C5H4 -> C5H41+ + H								RTYPE=2 K1=1.52E-09 K2=-0.5 K3=0.0 # gg08_rnum=1559
H1+ + C6 -> C61+ + H									RTYPE=2 K1=4.00E-09 K2=0.0 K3=0.0 # gg08_rnum=1560
H1+ + C6H -> C61+ + H2									RTYPE=2 K1=2.30E-08 K2=-0.5 K3=0.0 # gg08_rnum=1561
H1+ + C6H -> C6H1+ + H									RTYPE=2 K1=2.30E-08 K2=-0.5 K3=0.0 # gg08_rnum=1562
H1+ + C6H2 -> C6H1+ + H2								RTYPE=2 K1=2.00E-09 K2=0.0 K3=0.0 # gg08_rnum=1563
H1+ + C6H2 -> C6H21+ + H								RTYPE=2 K1=2.00E-09 K2=0.0 K3=0.0 # gg08_rnum=1564
H1+ + C6H4 -> C6H41+ + H								RTYPE=2 K1=7.78E-10 K2=-0.5 K3=0.0 # gg08_rnum=1565
H1+ + C6H4 -> C6H31+ + H2								RTYPE=2 K1=7.78E-10 K2=-0.5 K3=0.0 # gg08_rnum=1566
H1+ + C7 -> C71+ + H									RTYPE=2 K1=4.00E-09 K2=0.0 K3=0.0 # gg08_rnum=1567
H1+ + C7H -> C71+ + H2									RTYPE=2 K1=2.07E-08 K2=-0.5 K3=0.0 # gg08_rnum=1568
H1+ + C7H -> C7H1+ + H									RTYPE=2 K1=2.07E-08 K2=-0.5 K3=0.0 # gg08_rnum=1569
H1+ + C7H2 -> C7H21+ + H								RTYPE=2 K1=4.86E-09 K2=-0.5 K3=0.0 # gg08_rnum=1570
H1+ + C7H2 -> C7H1+ + H2								RTYPE=2 K1=4.86E-09 K2=-0.5 K3=0.0 # gg08_rnum=1571
H1+ + C7H4 -> C7H41+ + H								RTYPE=2 K1=1.52E-09 K2=-0.5 K3=0.0 # gg08_rnum=1572
H1+ + C7H4 -> C7H31+ + H2								RTYPE=2 K1=1.52E-09 K2=-0.5 K3=0.0 # gg08_rnum=1573
H1+ + C8 -> C81+ + H									RTYPE=2 K1=4.00E-09 K2=0.0 K3=0.0 # gg08_rnum=1574
H1+ + C8H -> C81+ + H2									RTYPE=2 K1=2.30E-08 K2=-0.5 K3=0.0 # gg08_rnum=1575
H1+ + C8H -> C8H1+ + H									RTYPE=2 K1=2.30E-08 K2=-0.5 K3=0.0 # gg08_rnum=1576
H1+ + C8H2 -> C8H21+ + H								RTYPE=2 K1=2.00E-09 K2=0.0 K3=0.0 # gg08_rnum=1577
H1+ + C8H2 -> C8H1+ + H2								RTYPE=2 K1=2.00E-09 K2=0.0 K3=0.0 # gg08_rnum=1578
H1+ + C8H4 -> C8H41+ + H								RTYPE=2 K1=7.79E-10 K2=-0.5 K3=0.0 # gg08_rnum=1579
H1+ + C8H4 -> C8H31+ + H2								RTYPE=2 K1=7.79E-10 K2=-0.5 K3=0.0 # gg08_rnum=1580
H1+ + C9 -> C91+ + H									RTYPE=2 K1=4.00E-09 K2=0.0 K3=0.0 # gg08_rnum=1581
H1+ + C9H -> C9H1+ + H									RTYPE=2 K1=2.16E-08 K2=-0.5 K3=0.0 # gg08_rnum=1582
H1+ + C9H -> C91+ + H2									RTYPE=2 K1=2.16E-08 K2=-0.5 K3=0.0 # gg08_rnum=1583
H1+ + C9H2 -> C9H21+ + H								RTYPE=2 K1=4.85E-09 K2=-0.5 K3=0.0 # gg08_rnum=1584
H1+ + C9H2 -> C9H1+ + H2								RTYPE=2 K1=4.85E-09 K2=-0.5 K3=0.0 # gg08_rnum=1585
H1+ + C9H4 -> C9H41+ + H								RTYPE=2 K1=1.52E-09 K2=-0.5 K3=0.0 # gg08_rnum=1586
H1+ + C9H4 -> C9H31+ + H2								RTYPE=2 K1=1.52E-09 K2=-0.5 K3=0.0 # gg08_rnum=1587
H1+ + CCP -> CCP1+ + H									RTYPE=2 K1=9.23E-09 K2=-0.5 K3=0.0 # gg08_rnum=1588
H1+ + CH -> CH1+ + H									RTYPE=2 K1=1.40E-08 K2=-0.5 K3=0.0 # gg08_rnum=1589
H1+ + CH2 -> CH21+ + H									RTYPE=2 K1=1.14E-09 K2=-0.5 K3=0.0 # gg08_rnum=1590
H1+ + CH2 -> CH1+ + H2									RTYPE=2 K1=1.14E-09 K2=-0.5 K3=0.0 # gg08_rnum=1591
H1+ + H2C2N -> H2C2N1+ + H								RTYPE=2 K1=6.26E-09 K2=-0.5 K3=0.0 # gg08_rnum=1592
H1+ + CH2CO -> CH2CO1+ + H								RTYPE=2 K1=5.60E-09 K2=-0.5 K3=0.0 # gg08_rnum=1593
H1+ + CH2NH -> H2CN1+ + H2								RTYPE=2 K1=7.90E-09 K2=-0.5 K3=0.0 # gg08_rnum=1594
H1+ + CH2PH -> CH2PH1+ + H								RTYPE=2 K1=1.95E-09 K2=-0.5 K3=0.0 # gg08_rnum=1595
H1+ + CH3 -> CH31+ + H									RTYPE=2 K1=3.40E-09 K2=0.0 K3=0.0 # gg08_rnum=1596
H1+ + CH3C3N -> CH31+ + HC3N							RTYPE=2 K1=1.85E-08 K2=-0.5 K3=0.0 # gg08_rnum=1597
H1+ + CH3C4H -> C5H41+ + H								RTYPE=2 K1=2.36E-09 K2=-0.5 K3=0.0 # gg08_rnum=1598
H1+ + CH3C4H -> C5H31+ + H2								RTYPE=2 K1=2.36E-09 K2=-0.5 K3=0.0 # gg08_rnum=1599
H1+ + CH3C5N -> CH31+ + HC5N							RTYPE=2 K1=2.03E-08 K2=-0.5 K3=0.0 # gg08_rnum=1600
H1+ + CH3C6H -> C7H31+ + H2								RTYPE=2 K1=2.92E-09 K2=-0.5 K3=0.0 # gg08_rnum=1601
H1+ + CH3C6H -> C7H41+ + H								RTYPE=2 K1=2.92E-09 K2=-0.5 K3=0.0 # gg08_rnum=1602
H1+ + CH3C7N -> CH31+ + HC7N							RTYPE=2 K1=2.12E-08 K2=-0.5 K3=0.0 # gg08_rnum=1603
H1+ + CH3CHO -> CH3CHO1+ + H							RTYPE=2 K1=5.00E-09 K2=-0.5 K3=0.0 # gg08_rnum=1604
H1+ + CH3CHO -> CH3CO1+ + H2							RTYPE=2 K1=5.00E-09 K2=-0.5 K3=0.0 # gg08_rnum=1605
H1+ + CH3CN -> H2C2N1+ + H2								RTYPE=2 K1=7.70E-09 K2=-0.5 K3=0.0 # gg08_rnum=1606
H1+ + CH3CN -> CH3CN1+ + H								RTYPE=2 K1=7.70E-09 K2=-0.5 K3=0.0 # gg08_rnum=1607
H1+ + CH3NH2 -> CH3NH21+ + H							RTYPE=2 K1=2.60E-09 K2=-0.5 K3=0.0 # gg08_rnum=1608
H1+ + CH3NH2 -> CH3NH1+ + H2							RTYPE=2 K1=2.60E-09 K2=-0.5 K3=0.0 # gg08_rnum=1609
H1+ + CH3OCH3 -> CH3OCH31+ + H							RTYPE=2 K1=2.50E-09 K2=-0.5 K3=0.0 # gg08_rnum=1610
H1+ + CH3OCH3 -> CH3CH2O1+ + H2							RTYPE=2 K1=2.50E-09 K2=-0.5 K3=0.0 # gg08_rnum=1611
H1+ + CH3OH -> CH3OH1+ + H								RTYPE=2 K1=3.30E-09 K2=-0.5 K3=0.0 # gg08_rnum=1612
H1+ + CH3OH -> CH3O1+ + H2								RTYPE=2 K1=3.30E-09 K2=-0.5 K3=0.0 # gg08_rnum=1613
H1+ + CH4 -> CH31+ + H2									RTYPE=2 K1=2.30E-09 K2=0.0 K3=0.0 # gg08_rnum=1614
H1+ + CH4 -> CH41+ + H									RTYPE=2 K1=1.50E-09 K2=0.0 K3=0.0 # gg08_rnum=1615
H1+ + CO2 -> HCO1+ + O									RTYPE=2 K1=3.00E-09 K2=0.0 K3=0.0 # gg08_rnum=1616
H1+ + CP -> CP1+ + H									RTYPE=2 K1=7.96E-09 K2=-0.5 K3=0.0 # gg08_rnum=1617
H1+ + CS -> CS1+ + H									RTYPE=2 K1=1.80E-08 K2=-0.5 K3=0.0 # gg08_rnum=1618
H1+ + Fe -> Fe1+ + H									RTYPE=2 K1=7.40E-09 K2=0.0 K3=0.0 # gg08_rnum=1619
H1+ + H2CO -> H2CO1+ + H								RTYPE=2 K1=4.60E-09 K2=-0.5 K3=0.0 # gg08_rnum=1620
H1+ + H2CO -> HCO1+ + H2								RTYPE=2 K1=4.60E-09 K2=-0.5 K3=0.0 # gg08_rnum=1621
H1+ + H2CS -> H2CS1+ + H								RTYPE=2 K1=6.40E-09 K2=-0.5 K3=0.0 # gg08_rnum=1622
H1+ + H2O -> H2O1+ + H									RTYPE=2 K1=7.30E-09 K2=-0.5 K3=0.0 # gg08_rnum=1623
H1+ + H2S -> H2S1+ + H									RTYPE=2 K1=3.80E-09 K2=-0.5 K3=0.0 # gg08_rnum=1624
H1+ + H2S2 -> H2S21+ + H								RTYPE=2 K1=4.67E-09 K2=-0.5 K3=0.0 # gg08_rnum=1625
H1+ + H2SiO -> H2SiO1+ + H								RTYPE=2 K1=3.91E-09 K2=-0.5 K3=0.0 # gg08_rnum=1626
H1+ + H2SiO -> HSiO1+ + H2								RTYPE=2 K1=3.91E-09 K2=-0.5 K3=0.0 # gg08_rnum=1627
H1+ + H5C3N -> C3H4N1+ + H2								RTYPE=2 K1=1.57E-08 K2=-0.5 K3=0.0 # gg08_rnum=1628
H1+ + HC2NC -> C3N1+ + H2								RTYPE=2 K1=7.23E-09 K2=-0.5 K3=0.0 # gg08_rnum=1629
H1+ + HC2NC -> H2C2N1+ + C								RTYPE=2 K1=7.23E-09 K2=-0.5 K3=0.0 # gg08_rnum=1630
H1+ + HC2NC -> HC2H1+ + CN								RTYPE=2 K1=7.23E-09 K2=-0.5 K3=0.0 # gg08_rnum=1631
H1+ + HC3N -> NC3H1+ + H								RTYPE=2 K1=3.40E-08 K2=-0.5 K3=0.0 # gg08_rnum=1632
H1+ + HC5N -> C5HN1+ + H								RTYPE=2 K1=4.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=1633
H1+ + HC7N -> C7HN1+ + H								RTYPE=2 K1=4.25E-08 K2=-0.5 K3=0.0 # gg08_rnum=1634
H1+ + HC9N -> C9HN1+ + H								RTYPE=2 K1=4.44E-08 K2=-0.5 K3=0.0 # gg08_rnum=1635
H1+ + HCCP -> HCCP1+ + H								RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=1636
H1+ + ClH -> ClH1+ + H									RTYPE=2 K1=1.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=1637
H1+ + HCN -> HCN1+ + H									RTYPE=2 K1=2.78E-08 K2=-0.5 K3=0.0 # gg08_rnum=1638
H1+ + C2NCH -> HC2N1+ + CH								RTYPE=2 K1=1.90E-08 K2=-0.5 K3=0.0 # gg08_rnum=1639
H1+ + C2NCH -> HCN1+ + C2H								RTYPE=2 K1=1.90E-08 K2=-0.5 K3=0.0 # gg08_rnum=1640
H1+ + C2NCH -> C2H1+ + HCN								RTYPE=2 K1=1.90E-08 K2=-0.5 K3=0.0 # gg08_rnum=1641
H1+ + C2NCH -> C3N1+ + H2								RTYPE=2 K1=1.90E-08 K2=-0.5 K3=0.0 # gg08_rnum=1642
H1+ + HCO -> HCO1+ + H									RTYPE=2 K1=1.30E-09 K2=-0.5 K3=0.0 # gg08_rnum=1643
H1+ + HCO -> CO1+ + H2									RTYPE=2 K1=1.30E-09 K2=-0.5 K3=0.0 # gg08_rnum=1644
H1+ + HCO -> H21+ + CO									RTYPE=2 K1=1.30E-09 K2=-0.5 K3=0.0 # gg08_rnum=1645
H1+ + HCOOCH3 -> HCOOCH31+ + H							RTYPE=2 K1=6.90E-09 K2=-0.5 K3=0.0 # gg08_rnum=1646
H1+ + HCOOH -> HCOOH1+ + H								RTYPE=2 K1=2.80E-09 K2=-0.5 K3=0.0 # gg08_rnum=1647
H1+ + HCOOH -> HOCO1+ + H2								RTYPE=2 K1=2.80E-09 K2=-0.5 K3=0.0 # gg08_rnum=1648
H1+ + HCP -> HCP1+ + H									RTYPE=2 K1=3.61E-09 K2=-0.5 K3=0.0 # gg08_rnum=1649
H1+ + HCS -> CS1+ + H2									RTYPE=2 K1=1.85E-08 K2=-0.5 K3=0.0 # gg08_rnum=1650
H1+ + SiCH -> SiC1+ + H2								RTYPE=2 K1=4.63E-09 K2=-0.5 K3=0.0 # gg08_rnum=1651
H1+ + SiCH -> SiCH1+ + H								RTYPE=2 K1=4.63E-09 K2=-0.5 K3=0.0 # gg08_rnum=1652
H1+ + HNC -> H1+ + HCN									RTYPE=2 K1=2.51E-08 K2=-0.5 K3=0.0 # gg08_rnum=1653
H1+ + HNC3 -> H1+ + HC3N								RTYPE=2 K1=3.23E-09 K2=-0.5 K3=0.0 # gg08_rnum=1654
H1+ + HNC3 -> NC3H1+ + H								RTYPE=2 K1=3.23E-09 K2=-0.5 K3=0.0 # gg08_rnum=1655
H1+ + HNC3 -> C3H1+ + NH								RTYPE=2 K1=3.23E-09 K2=-0.5 K3=0.0 # gg08_rnum=1656
H1+ + HNC3 -> C3N1+ + H2								RTYPE=2 K1=3.23E-09 K2=-0.5 K3=0.0 # gg08_rnum=1657
H1+ + HNC3 -> HNC1+ + C2H								RTYPE=2 K1=3.23E-09 K2=-0.5 K3=0.0 # gg08_rnum=1658
H1+ + HNC3 -> C2H1+ + HNC								RTYPE=2 K1=3.23E-09 K2=-0.5 K3=0.0 # gg08_rnum=1659
H1+ + HNCO -> NH21+ + CO								RTYPE=2 K1=1.48E-08 K2=-0.5 K3=0.0 # gg08_rnum=1660
H1+ + HNO -> NO1+ + H2									RTYPE=2 K1=6.70E-09 K2=-0.5 K3=0.0 # gg08_rnum=1661
H1+ + HNSi -> SiN1+ + H2								RTYPE=2 K1=7.40E-10 K2=-0.5 K3=0.0 # gg08_rnum=1662
H1+ + HNSi -> HNSi1+ + H								RTYPE=2 K1=7.40E-10 K2=-0.5 K3=0.0 # gg08_rnum=1663
H1+ + HPO -> HPO1+ + H									RTYPE=2 K1=2.15E-08 K2=-0.5 K3=0.0 # gg08_rnum=1664
H1+ + HS -> HS1+ + H									RTYPE=2 K1=3.50E-09 K2=-0.5 K3=0.0 # gg08_rnum=1665
H1+ + HS -> S1+ + H2									RTYPE=2 K1=3.50E-09 K2=-0.5 K3=0.0 # gg08_rnum=1666
H1+ + S2H -> S2H1+ + H									RTYPE=2 K1=7.79E-09 K2=-0.5 K3=0.0 # gg08_rnum=1667
H1+ + Mg -> Mg1+ + H									RTYPE=2 K1=1.10E-09 K2=0.0 K3=0.0 # gg08_rnum=1668
H1+ + MgH -> Mg1+ + H2									RTYPE=2 K1=1.20E-08 K2=-0.5 K3=0.0 # gg08_rnum=1669
H1+ + N2H2 -> N2H1+ + H2								RTYPE=2 K1=2.00E-09 K2=0.0 K3=0.0 # gg08_rnum=1670
H1+ + Na -> Na1+ + H									RTYPE=2 K1=1.20E-09 K2=0.0 K3=0.0 # gg08_rnum=1671
H1+ + NaH -> Na1+ + H2									RTYPE=2 K1=6.30E-08 K2=-0.5 K3=0.0 # gg08_rnum=1672
H1+ + NaOH -> Na1+ + H2O								RTYPE=2 K1=2.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=1673
H1+ + NH -> NH1+ + H									RTYPE=2 K1=1.20E-08 K2=-0.5 K3=0.0 # gg08_rnum=1674
H1+ + NH2 -> NH21+ + H									RTYPE=2 K1=7.30E-09 K2=-0.5 K3=0.0 # gg08_rnum=1675
H1+ + NH2CHO -> H2NC1+ + H2O							RTYPE=2 K1=8.62E-09 K2=-0.5 K3=0.0 # gg08_rnum=1676
H1+ + NH2CHO -> NH21+ + H2CO							RTYPE=2 K1=8.62E-09 K2=-0.5 K3=0.0 # gg08_rnum=1677
H1+ + NH2CHO -> NH41+ + CO								RTYPE=2 K1=8.62E-09 K2=-0.5 K3=0.0 # gg08_rnum=1678
H1+ + NH2CHO -> HCO1+ + NH3								RTYPE=2 K1=8.62E-09 K2=-0.5 K3=0.0 # gg08_rnum=1679
H1+ + NH2CN -> HNC1+ + NH2								RTYPE=2 K1=8.35E-09 K2=-0.5 K3=0.0 # gg08_rnum=1680
H1+ + NH2CN -> NH21+ + HNC								RTYPE=2 K1=8.35E-09 K2=-0.5 K3=0.0 # gg08_rnum=1681
H1+ + NH3 -> NH31+ + H									RTYPE=2 K1=5.80E-09 K2=-0.5 K3=0.0 # gg08_rnum=1682
H1+ + NO -> NO1+ + H									RTYPE=2 K1=1.40E-09 K2=-0.5 K3=0.0 # gg08_rnum=1683
H1+ + NS -> NS1+ + H									RTYPE=2 K1=1.70E-08 K2=-0.5 K3=0.0 # gg08_rnum=1684
H1+ + O -> O1+ + H										RTYPE=2 K1=7.00E-10 K2=0.0 K3=2.32E+02 # gg08_rnum=1685
H1+ + O2 -> O21+ + H									RTYPE=2 K1=1.20E-09 K2=0.0 K3=0.0 # gg08_rnum=1686
H1+ + OCS -> HS1+ + CO									RTYPE=2 K1=6.50E-09 K2=-0.5 K3=0.0 # gg08_rnum=1687
H1+ + OH -> OH1+ + H									RTYPE=2 K1=1.60E-08 K2=-0.5 K3=0.0 # gg08_rnum=1688
H1+ + P -> P1+ + H										RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=1689
H1+ + PH -> PH1+ + H									RTYPE=2 K1=5.94E-09 K2=-0.5 K3=0.0 # gg08_rnum=1690
H1+ + PH2 -> PH21+ + H									RTYPE=2 K1=1.96E-09 K2=-0.5 K3=0.0 # gg08_rnum=1691
H1+ + PN -> PN1+ + H									RTYPE=2 K1=2.54E-08 K2=-0.5 K3=0.0 # gg08_rnum=1692
H1+ + PO -> PO1+ + H									RTYPE=2 K1=1.74E-08 K2=-0.5 K3=0.0 # gg08_rnum=1693
H1+ + S -> S1+ + H										RTYPE=2 K1=1.30E-09 K2=0.0 K3=0.0 # gg08_rnum=1694
H1+ + S2 -> S21+ + H									RTYPE=2 K1=3.00E-09 K2=0.0 K3=0.0 # gg08_rnum=1695
H1+ + Si -> Si1+ + H									RTYPE=2 K1=9.90E-10 K2=0.0 K3=0.0 # gg08_rnum=1696
H1+ + SiC -> SiC1+ + H									RTYPE=2 K1=1.57E-08 K2=-0.5 K3=0.0 # gg08_rnum=1697
H1+ + SiC2 -> SiC21+ + H								RTYPE=2 K1=9.32E-09 K2=-0.5 K3=0.0 # gg08_rnum=1698
H1+ + SiC2H -> SiC21+ + H2								RTYPE=2 K1=2.73E-09 K2=-0.5 K3=0.0 # gg08_rnum=1699
H1+ + SiC2H -> SiC2H1+ + H								RTYPE=2 K1=2.73E-09 K2=-0.5 K3=0.0 # gg08_rnum=1700
H1+ + SiC2H2 -> SiC2H1+ + H2							RTYPE=2 K1=4.87E-09 K2=-0.5 K3=0.0 # gg08_rnum=1701
H1+ + SiC2H2 -> SiC2H21+ + H							RTYPE=2 K1=4.87E-09 K2=-0.5 K3=0.0 # gg08_rnum=1702
H1+ + cSiC3 -> SiC31+ + H								RTYPE=2 K1=7.79E-09 K2=-0.5 K3=0.0 # gg08_rnum=1703
H1+ + SiC3H -> SiC31+ + H2								RTYPE=2 K1=5.85E-09 K2=-0.5 K3=0.0 # gg08_rnum=1704
H1+ + SiC3H -> SiC3H1+ + H								RTYPE=2 K1=5.85E-09 K2=-0.5 K3=0.0 # gg08_rnum=1705
H1+ + SiC4 -> SiC41+ + H								RTYPE=2 K1=2.45E-08 K2=-0.5 K3=0.0 # gg08_rnum=1706
H1+ + SiCH2 -> SiCH21+ + H								RTYPE=2 K1=2.93E-09 K2=-0.5 K3=0.0 # gg08_rnum=1707
H1+ + SiCH2 -> SiCH1+ + H2								RTYPE=2 K1=2.93E-09 K2=-0.5 K3=0.0 # gg08_rnum=1708
H1+ + SiCH3 -> SiCH21+ + H2								RTYPE=2 K1=2.93E-09 K2=-0.5 K3=0.0 # gg08_rnum=1709
H1+ + SiCH3 -> SiCH31+ + H								RTYPE=2 K1=2.93E-09 K2=-0.5 K3=0.0 # gg08_rnum=1710
H1+ + SiH -> Si1+ + H2									RTYPE=2 K1=5.60E-10 K2=-0.5 K3=0.0 # gg08_rnum=1711
H1+ + SiH -> SiH1+ + H									RTYPE=2 K1=5.60E-10 K2=-0.5 K3=0.0 # gg08_rnum=1712
H1+ + SiH2 -> SiH21+ + H								RTYPE=2 K1=3.54E-10 K2=-0.5 K3=0.0 # gg08_rnum=1713
H1+ + SiH2 -> SiH1+ + H2								RTYPE=2 K1=3.54E-10 K2=-0.5 K3=0.0 # gg08_rnum=1714
H1+ + SiH3 -> SiH31+ + H								RTYPE=2 K1=1.50E-09 K2=0.0 K3=0.0 # gg08_rnum=1715
H1+ + SiH3 -> SiH21+ + H2								RTYPE=2 K1=1.50E-09 K2=0.0 K3=0.0 # gg08_rnum=1716
H1+ + SiH4 -> SiH31+ + H2								RTYPE=2 K1=1.50E-09 K2=0.0 K3=0.0 # gg08_rnum=1717
H1+ + SiH4 -> SiH41+ + H								RTYPE=2 K1=1.50E-09 K2=0.0 K3=0.0 # gg08_rnum=1718
H1+ + SiN -> SiN1+ + H									RTYPE=2 K1=2.13E-08 K2=-0.5 K3=0.0 # gg08_rnum=1719
H1+ + SiNC -> SiNC1+ + H								RTYPE=2 K1=7.92E-09 K2=-0.5 K3=0.0 # gg08_rnum=1720
H1+ + SiO -> SiO1+ + H									RTYPE=2 K1=2.90E-08 K2=-0.5 K3=0.0 # gg08_rnum=1721
H1+ + SiS -> SiS1+ + H									RTYPE=2 K1=1.60E-08 K2=-0.5 K3=0.0 # gg08_rnum=1722
H1+ + SO -> SO1+ + H									RTYPE=2 K1=1.40E-08 K2=-0.5 K3=0.0 # gg08_rnum=1723
H21+ + C -> CH1+ + H									RTYPE=2 K1=2.40E-09 K2=0.0 K3=0.0 # gg08_rnum=1724
H21+ + C2 -> C2H1+ + H									RTYPE=2 K1=1.10E-09 K2=0.0 K3=0.0 # gg08_rnum=1725
H21+ + C2 -> C21+ + H2									RTYPE=2 K1=1.10E-09 K2=0.0 K3=0.0 # gg08_rnum=1726
H21+ + C2H -> C2H1+ + H2								RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=1727
H21+ + C2H -> HC2H1+ + H								RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=1728
H21+ + HC2H -> H2C2H1+ + H								RTYPE=2 K1=4.80E-10 K2=0.0 K3=0.0 # gg08_rnum=1729
H21+ + HC2H -> HC2H1+ + H2								RTYPE=2 K1=4.80E-09 K2=0.0 K3=0.0 # gg08_rnum=1730
H21+ + H2C2H2 -> H2C2H21+ + H2							RTYPE=2 K1=2.21E-09 K2=0.0 K3=0.0 # gg08_rnum=1731
H21+ + H2C2H2 -> HC2H1+ + H2 + H2						RTYPE=2 K1=8.82E-10 K2=0.0 K3=0.0 # gg08_rnum=1732
H21+ + H2C2H2 -> H2C2H1+ + H2 + H						RTYPE=2 K1=1.81E-09 K2=0.0 K3=0.0 # gg08_rnum=1733
H21+ + CH -> CH21+ + H									RTYPE=2 K1=7.10E-10 K2=0.0 K3=0.0 # gg08_rnum=1734
H21+ + CH -> CH1+ + H2									RTYPE=2 K1=7.10E-10 K2=0.0 K3=0.0 # gg08_rnum=1735
H21+ + CH2 -> CH21+ + H2								RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=1736
H21+ + CH2 -> CH31+ + H									RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=1737
H21+ + CH4 -> CH41+ + H2								RTYPE=2 K1=1.40E-09 K2=0.0 K3=0.0 # gg08_rnum=1738
H21+ + CH4 -> CH51+ + H									RTYPE=2 K1=1.10E-10 K2=0.0 K3=0.0 # gg08_rnum=1739
H21+ + CH4 -> CH31+ + H + H2							RTYPE=2 K1=2.30E-09 K2=0.0 K3=0.0 # gg08_rnum=1740
H21+ + CN -> CN1+ + H2									RTYPE=2 K1=1.20E-09 K2=0.0 K3=0.0 # gg08_rnum=1741
H21+ + CN -> HCN1+ + H									RTYPE=2 K1=1.20E-09 K2=0.0 K3=0.0 # gg08_rnum=1742
H21+ + CO -> CO1+ + H2									RTYPE=2 K1=6.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1743
H21+ + CO -> HCO1+ + H									RTYPE=2 K1=2.20E-09 K2=0.0 K3=0.0 # gg08_rnum=1744
H21+ + CO2 -> CO21+ + H2								RTYPE=2 K1=1.40E-09 K2=0.0 K3=0.0 # gg08_rnum=1745
H21+ + CO2 -> HOCO1+ + H								RTYPE=2 K1=2.35E-09 K2=0.0 K3=0.0 # gg08_rnum=1746
H21+ + CO2 -> CO1+ + H2O								RTYPE=2 K1=1.40E-09 K2=0.0 K3=0.0 # gg08_rnum=1747
H21+ + H -> H1+ + H2									RTYPE=2 K1=6.40E-10 K2=0.0 K3=0.0 # gg08_rnum=1748
H21+ + H2 -> H31+ + H									RTYPE=2 K1=2.10E-09 K2=0.0 K3=0.0 # gg08_rnum=1749
H21+ + H2CO -> HCO1+ + H + H2							RTYPE=2 K1=1.40E-09 K2=0.0 K3=0.0 # gg08_rnum=1750
H21+ + H2CO -> H2CO1+ + H2								RTYPE=2 K1=1.40E-09 K2=0.0 K3=0.0 # gg08_rnum=1751
H21+ + H2O -> H2O1+ + H2								RTYPE=2 K1=3.90E-09 K2=0.0 K3=0.0 # gg08_rnum=1752
H21+ + H2O -> H3O1+ + H									RTYPE=2 K1=3.40E-09 K2=0.0 K3=0.0 # gg08_rnum=1753
H21+ + H2S -> HS1+ + H + H2								RTYPE=2 K1=8.60E-10 K2=0.0 K3=0.0 # gg08_rnum=1754
H21+ + H2S -> H2S1+ + H2								RTYPE=2 K1=2.70E-09 K2=0.0 K3=0.0 # gg08_rnum=1755
H21+ + H2S -> S1+ + H2 + H2								RTYPE=2 K1=7.70E-10 K2=0.0 K3=0.0 # gg08_rnum=1756
H21+ + HCN -> HCN1+ + H2								RTYPE=2 K1=2.70E-09 K2=0.0 K3=0.0 # gg08_rnum=1757
H21+ + HCO -> H31+ + CO									RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=1758
H21+ + HCO -> HCO1+ + H2								RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=1759
H21+ + N -> NH1+ + H									RTYPE=2 K1=1.90E-09 K2=0.0 K3=0.0 # gg08_rnum=1760
H21+ + N2 -> N2H1+ + H									RTYPE=2 K1=2.00E-09 K2=0.0 K3=0.0 # gg08_rnum=1761
H21+ + NH -> NH21+ + H									RTYPE=2 K1=7.60E-10 K2=0.0 K3=0.0 # gg08_rnum=1762
H21+ + NH -> NH1+ + H2									RTYPE=2 K1=7.60E-10 K2=0.0 K3=0.0 # gg08_rnum=1763
H21+ + NH2 -> NH21+ + H2								RTYPE=2 K1=2.10E-09 K2=0.0 K3=0.0 # gg08_rnum=1764
H21+ + NH3 -> NH31+ + H2								RTYPE=2 K1=5.70E-09 K2=0.0 K3=0.0 # gg08_rnum=1765
H21+ + NO -> NO1+ + H2									RTYPE=2 K1=1.10E-09 K2=0.0 K3=0.0 # gg08_rnum=1766
H21+ + NO -> HNO1+ + H									RTYPE=2 K1=1.10E-09 K2=0.0 K3=0.0 # gg08_rnum=1767
H21+ + O -> OH1+ + H									RTYPE=2 K1=1.50E-09 K2=0.0 K3=0.0 # gg08_rnum=1768
H21+ + O2 -> O21+ + H2									RTYPE=2 K1=8.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1769
H21+ + O2 -> O2H1+ + H									RTYPE=2 K1=1.90E-09 K2=0.0 K3=0.0 # gg08_rnum=1770
H21+ + OH -> OH1+ + H2									RTYPE=2 K1=7.60E-10 K2=0.0 K3=0.0 # gg08_rnum=1771
H21+ + OH -> H2O1+ + H									RTYPE=2 K1=7.60E-10 K2=0.0 K3=0.0 # gg08_rnum=1772
H2C4N1+ + H2 -> CH3C3N1+ + H							RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=1773
ClH21+ + CO -> HCO1+ + ClH								RTYPE=2 K1=7.80E-10 K2=0.0 K3=0.0 # gg08_rnum=1774
ClH21+ + H2O -> H3O1+ + ClH								RTYPE=2 K1=2.00E-09 K2=0.0 K3=0.0 # gg08_rnum=1775
H2CN1+ + CH -> CH21+ + HCN								RTYPE=2 K1=4.50E-09 K2=-0.5 K3=0.0 # gg08_rnum=1776
H2CN1+ + CH2 -> CH31+ + HNC								RTYPE=2 K1=3.61E-10 K2=-0.5 K3=0.0 # gg08_rnum=1777
H2CN1+ + CH2 -> CH31+ + HCN								RTYPE=2 K1=3.61E-10 K2=-0.5 K3=0.0 # gg08_rnum=1778
H2CN1+ + H2CO -> CH2OH1+ + HCN							RTYPE=2 K1=2.37E-09 K2=-0.5 K3=0.0 # gg08_rnum=1779
H2CN1+ + Na -> Na1+ + HNC + H							RTYPE=2 K1=1.35E-09 K2=0.0 K3=0.0 # gg08_rnum=1780
H2CN1+ + Na -> Na1+ + HCN + H							RTYPE=2 K1=2.70E-09 K2=0.0 K3=0.0 # gg08_rnum=1781
H2CN1+ + NH2 -> NH31+ + HNC								RTYPE=2 K1=1.11E-09 K2=-0.5 K3=0.0 # gg08_rnum=1782
H2CN1+ + NH2 -> NH31+ + HCN								RTYPE=2 K1=1.11E-09 K2=-0.5 K3=0.0 # gg08_rnum=1783
H2CN1+ + NH3 -> NH41+ + HCN								RTYPE=2 K1=8.75E-10 K2=-0.5 K3=0.0 # gg08_rnum=1784
H2CN1+ + NH3 -> NH41+ + HNC								RTYPE=2 K1=8.75E-10 K2=-0.5 K3=0.0 # gg08_rnum=1785
H2CN1+ + Si -> SiNC1+ + H2								RTYPE=2 K1=5.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1786
H2CO1+ + C2 -> C2H1+ + HCO								RTYPE=2 K1=8.20E-10 K2=0.0 K3=0.0 # gg08_rnum=1787
H2CO1+ + C2H -> HC2H1+ + HCO							RTYPE=2 K1=7.70E-10 K2=0.0 K3=0.0 # gg08_rnum=1788
H2CO1+ + CH -> CH21+ + HCO								RTYPE=2 K1=3.10E-10 K2=0.0 K3=0.0 # gg08_rnum=1789
H2CO1+ + CH -> CH1+ + H2CO								RTYPE=2 K1=3.10E-10 K2=0.0 K3=0.0 # gg08_rnum=1790
H2CO1+ + CH2 -> CH21+ + H2CO							RTYPE=2 K1=4.30E-10 K2=0.0 K3=0.0 # gg08_rnum=1791
H2CO1+ + CH2 -> CH31+ + HCO								RTYPE=2 K1=4.30E-10 K2=0.0 K3=0.0 # gg08_rnum=1792
H2CO1+ + CH4 -> CH2OH1+ + CH3							RTYPE=2 K1=1.10E-10 K2=0.0 K3=0.0 # gg08_rnum=1793
H2CO1+ + Fe -> Fe1+ + H2CO								RTYPE=2 K1=1.90E-09 K2=0.0 K3=0.0 # gg08_rnum=1794
H2CO1+ + H2CO -> CH2OH1+ + HCO							RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=1795
H2CO1+ + H2O -> H3O1+ + HCO								RTYPE=2 K1=2.60E-09 K2=0.0 K3=0.0 # gg08_rnum=1796
H2CO1+ + HCN -> H2CN1+ + HCO							RTYPE=2 K1=1.40E-09 K2=0.0 K3=0.0 # gg08_rnum=1797
H2CO1+ + HCO -> HCO1+ + H2CO							RTYPE=2 K1=3.20E-09 K2=0.0 K3=0.0 # gg08_rnum=1798
H2CO1+ + HCO -> CH2OH1+ + CO							RTYPE=2 K1=3.60E-10 K2=0.0 K3=0.0 # gg08_rnum=1799
H2CO1+ + HNC -> H2CN1+ + HCO							RTYPE=2 K1=1.40E-09 K2=0.0 K3=0.0 # gg08_rnum=1800
H2CO1+ + Mg -> Mg1+ + H2CO								RTYPE=2 K1=2.90E-09 K2=0.0 K3=0.0 # gg08_rnum=1801
H2CO1+ + Na -> Na1+ + H2CO								RTYPE=2 K1=2.60E-09 K2=0.0 K3=0.0 # gg08_rnum=1802
H2CO1+ + NH -> CH2OH1+ + N								RTYPE=2 K1=6.40E-10 K2=0.0 K3=0.0 # gg08_rnum=1803
H2CO1+ + NH2 -> NH31+ + HCO								RTYPE=2 K1=8.80E-10 K2=0.0 K3=0.0 # gg08_rnum=1804
H2CO1+ + NH3 -> NH41+ + HCO								RTYPE=2 K1=1.30E-09 K2=0.0 K3=0.0 # gg08_rnum=1805
H2CO1+ + NH3 -> NH31+ + H2CO							RTYPE=2 K1=4.30E-10 K2=0.0 K3=0.0 # gg08_rnum=1806
H2CO1+ + NO -> NO1+ + H2CO								RTYPE=2 K1=7.80E-10 K2=0.0 K3=0.0 # gg08_rnum=1807
H2CO1+ + O2 -> HCO1+ + HO2								RTYPE=2 K1=7.70E-11 K2=0.0 K3=0.0 # gg08_rnum=1808
H2CO1+ + S -> S1+ + H2CO								RTYPE=2 K1=5.50E-10 K2=0.0 K3=0.0 # gg08_rnum=1809
H2CO1+ + S -> HS1+ + HCO								RTYPE=2 K1=5.50E-10 K2=0.0 K3=0.0 # gg08_rnum=1810
H2CO1+ + Si -> Si1+ + H2CO								RTYPE=2 K1=1.60E-09 K2=0.0 K3=0.0 # gg08_rnum=1811
H2NC1+ + Si -> SiNC1+ + H2								RTYPE=2 K1=5.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1812
H2O1+ + C -> CH1+ + OH									RTYPE=2 K1=1.10E-09 K2=0.0 K3=0.0 # gg08_rnum=1813
H2O1+ + C2 -> C2H1+ + OH								RTYPE=2 K1=4.70E-10 K2=0.0 K3=0.0 # gg08_rnum=1814
H2O1+ + C2 -> C21+ + H2O								RTYPE=2 K1=4.70E-10 K2=0.0 K3=0.0 # gg08_rnum=1815
H2O1+ + C2H -> HC2H1+ + OH								RTYPE=2 K1=4.40E-10 K2=0.0 K3=0.0 # gg08_rnum=1816
H2O1+ + C2H -> C2H1+ + H2O								RTYPE=2 K1=4.40E-10 K2=0.0 K3=0.0 # gg08_rnum=1817
H2O1+ + CH -> CH1+ + H2O								RTYPE=2 K1=3.40E-10 K2=0.0 K3=0.0 # gg08_rnum=1818
H2O1+ + CH -> CH21+ + OH								RTYPE=2 K1=3.40E-10 K2=0.0 K3=0.0 # gg08_rnum=1819
H2O1+ + CH2 -> CH31+ + OH								RTYPE=2 K1=4.70E-10 K2=0.0 K3=0.0 # gg08_rnum=1820
H2O1+ + CH2 -> CH21+ + H2O								RTYPE=2 K1=4.70E-10 K2=0.0 K3=0.0 # gg08_rnum=1821
H2O1+ + CH4 -> H3O1+ + CH3								RTYPE=2 K1=1.30E-09 K2=0.0 K3=0.0 # gg08_rnum=1822
H2O1+ + CO -> HCO1+ + OH								RTYPE=2 K1=5.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1823
H2O1+ + Fe -> Fe1+ + H2O								RTYPE=2 K1=1.50E-09 K2=0.0 K3=0.0 # gg08_rnum=1824
H2O1+ + H2 -> H3O1+ + H									RTYPE=2 K1=6.10E-10 K2=0.0 K3=0.0 # gg08_rnum=1825
H2O1+ + H2CO -> CH2OH1+ + OH							RTYPE=2 K1=6.60E-10 K2=0.0 K3=0.0 # gg08_rnum=1826
H2O1+ + H2CO -> H2CO1+ + H2O							RTYPE=2 K1=1.40E-09 K2=0.0 K3=0.0 # gg08_rnum=1827
H2O1+ + H2O -> H3O1+ + OH								RTYPE=2 K1=2.10E-09 K2=0.0 K3=0.0 # gg08_rnum=1828
H2O1+ + H2S -> H3S1+ + OH								RTYPE=2 K1=7.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1829
H2O1+ + H2S -> H2S1+ + H2O								RTYPE=2 K1=8.90E-10 K2=0.0 K3=0.0 # gg08_rnum=1830
H2O1+ + H2S -> H3O1+ + HS								RTYPE=2 K1=5.90E-10 K2=0.0 K3=0.0 # gg08_rnum=1831
H2O1+ + HCN -> H2CN1+ + OH								RTYPE=2 K1=2.10E-09 K2=0.0 K3=0.0 # gg08_rnum=1832
H2O1+ + HCO -> H2CO1+ + OH								RTYPE=2 K1=2.80E-10 K2=0.0 K3=0.0 # gg08_rnum=1833
H2O1+ + HCO -> H3O1+ + CO								RTYPE=2 K1=2.80E-10 K2=0.0 K3=0.0 # gg08_rnum=1834
H2O1+ + HCO -> HCO1+ + H2O								RTYPE=2 K1=2.80E-10 K2=0.0 K3=0.0 # gg08_rnum=1835
H2O1+ + HNC -> H2CN1+ + OH								RTYPE=2 K1=1.10E-09 K2=0.0 K3=0.0 # gg08_rnum=1836
H2O1+ + Mg -> Mg1+ + H2O								RTYPE=2 K1=2.20E-09 K2=0.0 K3=0.0 # gg08_rnum=1837
H2O1+ + N -> HNO1+ + H									RTYPE=2 K1=1.90E-10 K2=0.0 K3=0.0 # gg08_rnum=1838
H2O1+ + Na -> Na1+ + H2O								RTYPE=2 K1=2.70E-09 K2=0.0 K3=0.0 # gg08_rnum=1839
H2O1+ + NH -> H3O1+ + N									RTYPE=2 K1=7.10E-10 K2=0.0 K3=0.0 # gg08_rnum=1840
H2O1+ + NH2 -> NH21+ + H2O								RTYPE=2 K1=4.90E-10 K2=0.0 K3=0.0 # gg08_rnum=1841
H2O1+ + NH2 -> NH31+ + OH								RTYPE=2 K1=4.90E-10 K2=0.0 K3=0.0 # gg08_rnum=1842
H2O1+ + NH3 -> NH31+ + H2O								RTYPE=2 K1=2.20E-09 K2=0.0 K3=0.0 # gg08_rnum=1843
H2O1+ + NH3 -> NH41+ + OH								RTYPE=2 K1=9.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1844
H2O1+ + NO -> NO1+ + H2O								RTYPE=2 K1=1.20E-09 K2=0.0 K3=0.0 # gg08_rnum=1845
H2O1+ + O -> O21+ + H2									RTYPE=2 K1=4.00E-11 K2=0.0 K3=0.0 # gg08_rnum=1846
H2O1+ + O2 -> O21+ + H2O								RTYPE=2 K1=4.30E-10 K2=0.0 K3=0.0 # gg08_rnum=1847
H2O1+ + OH -> H3O1+ + O									RTYPE=2 K1=6.90E-10 K2=0.0 K3=0.0 # gg08_rnum=1848
H2O1+ + S -> HS1+ + OH									RTYPE=2 K1=4.30E-10 K2=0.0 K3=0.0 # gg08_rnum=1849
H2O1+ + S -> HSO1+ + H									RTYPE=2 K1=4.30E-10 K2=0.0 K3=0.0 # gg08_rnum=1850
H2O1+ + S -> S1+ + H2O									RTYPE=2 K1=4.30E-10 K2=0.0 K3=0.0 # gg08_rnum=1851
H2O1+ + Si -> Si1+ + H2O								RTYPE=2 K1=3.00E-09 K2=0.0 K3=0.0 # gg08_rnum=1852
H2S1+ + C -> HCS1+ + H									RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=1853
H2S1+ + Fe -> Fe1+ + H2S								RTYPE=2 K1=1.80E-09 K2=0.0 K3=0.0 # gg08_rnum=1854
H2S1+ + H -> HS1+ + H2									RTYPE=2 K1=2.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1855
H2S1+ + H2O -> H3O1+ + HS								RTYPE=2 K1=7.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1856
H2S1+ + H2S -> H3S1+ + HS								RTYPE=2 K1=5.80E-10 K2=0.0 K3=0.0 # gg08_rnum=1857
H2S1+ + HCO -> HCO1+ + H2S								RTYPE=2 K1=7.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1858
H2S1+ + Mg -> Mg1+ + H2S								RTYPE=2 K1=2.80E-09 K2=0.0 K3=0.0 # gg08_rnum=1859
H2S1+ + N -> NS1+ + H2									RTYPE=2 K1=7.90E-10 K2=0.0 K3=0.0 # gg08_rnum=1860
H2S1+ + Na -> Na1+ + H2S								RTYPE=2 K1=2.50E-09 K2=0.0 K3=0.0 # gg08_rnum=1861
H2S1+ + NH3 -> NH41+ + HS								RTYPE=2 K1=1.30E-09 K2=0.0 K3=0.0 # gg08_rnum=1862
H2S1+ + NH3 -> NH31+ + H2S								RTYPE=2 K1=5.60E-10 K2=0.0 K3=0.0 # gg08_rnum=1863
H2S1+ + NO -> NO1+ + H2S								RTYPE=2 K1=3.70E-10 K2=0.0 K3=0.0 # gg08_rnum=1864
H2S1+ + O -> HS1+ + OH									RTYPE=2 K1=3.10E-10 K2=0.0 K3=0.0 # gg08_rnum=1865
H2S1+ + O -> SO1+ + H2									RTYPE=2 K1=3.10E-10 K2=0.0 K3=0.0 # gg08_rnum=1866
H2S1+ + S -> S1+ + H2S									RTYPE=2 K1=1.10E-09 K2=0.0 K3=0.0 # gg08_rnum=1867
H2S1+ + Si -> Si1+ + H2S								RTYPE=2 K1=1.60E-09 K2=0.0 K3=0.0 # gg08_rnum=1868
H31+ + C -> CH1+ + H2									RTYPE=2 K1=2.00E-09 K2=0.0 K3=0.0 # gg08_rnum=1869
H31+ + C2 -> C2H1+ + H2									RTYPE=2 K1=1.80E-09 K2=0.0 K3=0.0 # gg08_rnum=1870
H31+ + C2H -> HC2H1+ + H2								RTYPE=2 K1=4.50E-09 K2=-0.5 K3=0.0 # gg08_rnum=1871
H31+ + HC2H -> H2C2H1+ + H2								RTYPE=2 K1=3.50E-09 K2=0.0 K3=0.0 # gg08_rnum=1872
H31+ + H2C2H -> H2C2H21+ + H2							RTYPE=2 K1=3.50E-09 K2=-0.5 K3=0.0 # gg08_rnum=1873
H31+ + H2C2H2 -> H2C2H1+ + H2 + H2						RTYPE=2 K1=1.20E-10 K2=0.0 K3=0.0 # gg08_rnum=1874
H31+ + H2C2H2 -> CH3CH21+ + H2							RTYPE=2 K1=1.90E-09 K2=0.0 K3=0.0 # gg08_rnum=1875
H31+ + CH3CH2 -> CH3CH31+ + H2							RTYPE=2 K1=2.34E-09 K2=-0.5 K3=0.0 # gg08_rnum=1876
H31+ + CH3CH2CHO -> CH3CH2CHOH1+ + H2					RTYPE=2 K1=6.41E-09 K2=-0.5 K3=0.0 # gg08_rnum=1877
H31+ + CH3CH2OH -> CH3CH21+ + H2O + H2					RTYPE=2 K1=1.30E-09 K2=-0.5 K3=0.0 # gg08_rnum=1878
H31+ + CH3CH2OH -> CH3CH2OH21+ + H2						RTYPE=2 K1=1.30E-09 K2=-0.5 K3=0.0 # gg08_rnum=1879
H31+ + CH3CH2OH -> H3O1+ + H2C2H2 + H2					RTYPE=2 K1=1.30E-09 K2=-0.5 K3=0.0 # gg08_rnum=1880
H31+ + CH3CH3 -> CH3CH21+ + H2 + H2						RTYPE=2 K1=3.37E-09 K2=0.0 K3=0.0 # gg08_rnum=1881
H31+ + C2N -> HC2N1+ + H2								RTYPE=2 K1=3.29E-09 K2=-0.5 K3=0.0 # gg08_rnum=1882
H31+ + C2O -> C2OH1+ + H2								RTYPE=2 K1=7.12E-09 K2=-0.5 K3=0.0 # gg08_rnum=1883
H31+ + C2S -> HC2S1+ + H2								RTYPE=2 K1=6.41E-09 K2=-0.5 K3=0.0 # gg08_rnum=1884
H31+ + C3 -> C3H1+ + H2									RTYPE=2 K1=2.00E-09 K2=0.0 K3=0.0 # gg08_rnum=1885
H31+ + C3H -> C3H21+ + H2								RTYPE=2 K1=1.70E-08 K2=-0.5 K3=0.0 # gg08_rnum=1886
H31+ + C3H2 -> C3H31+ + H2								RTYPE=2 K1=5.16E-09 K2=-0.5 K3=0.0 # gg08_rnum=1887
H31+ + C3H3 -> C3H41+ + H2								RTYPE=2 K1=9.30E-09 K2=-0.5 K3=0.0 # gg08_rnum=1888
H31+ + C3H3N -> C3H4N1+ + H2							RTYPE=2 K1=8.90E-09 K2=-0.5 K3=0.0 # gg08_rnum=1889
H31+ + C3H4 -> C3H31+ + H2 + H2							RTYPE=2 K1=1.80E-10 K2=-0.5 K3=0.0 # gg08_rnum=1890
H31+ + C3H4 -> C3H51+ + H2								RTYPE=2 K1=1.80E-09 K2=-0.5 K3=0.0 # gg08_rnum=1891
H31+ + C3N -> NC3H1+ + H2								RTYPE=2 K1=1.20E-08 K2=-0.5 K3=0.0 # gg08_rnum=1892
H31+ + C3O -> HC3O1+ + H2								RTYPE=2 K1=1.30E-08 K2=-0.5 K3=0.0 # gg08_rnum=1893
H31+ + C3P -> PC3H1+ + H2								RTYPE=2 K1=2.28E-09 K2=-0.5 K3=0.0 # gg08_rnum=1894
H31+ + C3S -> HC3S1+ + H2								RTYPE=2 K1=8.89E-09 K2=-0.5 K3=0.0 # gg08_rnum=1895
H31+ + C4 -> C4H1+ + H2									RTYPE=2 K1=2.00E-09 K2=0.0 K3=0.0 # gg08_rnum=1896
H31+ + C4H -> HC4H1+ + H2								RTYPE=2 K1=4.90E-09 K2=-0.5 K3=0.0 # gg08_rnum=1897
H31+ + HC4H -> C4H31+ + H2								RTYPE=2 K1=2.00E-09 K2=0.0 K3=0.0 # gg08_rnum=1898
H31+ + C4H3 -> C4H41+ + H2								RTYPE=2 K1=2.30E-09 K2=-0.5 K3=0.0 # gg08_rnum=1899
H31+ + C4H4 -> C4H31+ + H2 + H2							RTYPE=2 K1=4.60E-10 K2=-0.5 K3=0.0 # gg08_rnum=1900
H31+ + C4H4 -> C4H51+ + H2								RTYPE=2 K1=4.60E-10 K2=-0.5 K3=0.0 # gg08_rnum=1901
H31+ + C4P -> PC4H1+ + H2								RTYPE=2 K1=1.14E-09 K2=-0.5 K3=0.0 # gg08_rnum=1902
H31+ + C4S -> HC4S1+ + H2								RTYPE=2 K1=6.82E-09 K2=-0.5 K3=0.0 # gg08_rnum=1903
H31+ + C5 -> C5H1+ + H2									RTYPE=2 K1=2.00E-09 K2=0.0 K3=0.0 # gg08_rnum=1904
H31+ + C5H -> C5H21+ + H2								RTYPE=2 K1=2.33E-08 K2=-0.5 K3=0.0 # gg08_rnum=1905
H31+ + C5H2 -> C5H31+ + H2								RTYPE=2 K1=5.71E-09 K2=-0.5 K3=0.0 # gg08_rnum=1906
H31+ + C5H4 -> C5H31+ + H2 + H2							RTYPE=2 K1=8.91E-10 K2=-0.5 K3=0.0 # gg08_rnum=1907
H31+ + C5H4 -> C5H51+ + H2								RTYPE=2 K1=8.91E-10 K2=-0.5 K3=0.0 # gg08_rnum=1908
H31+ + C5N -> C5HN1+ + H2								RTYPE=2 K1=1.45E-08 K2=-0.5 K3=0.0 # gg08_rnum=1909
H31+ + C6 -> C6H1+ + H2									RTYPE=2 K1=2.00E-09 K2=0.0 K3=0.0 # gg08_rnum=1910
H31+ + C6H -> C6H21+ + H2								RTYPE=2 K1=2.69E-08 K2=-0.5 K3=0.0 # gg08_rnum=1911
H31+ + C6H2 -> C6H31+ + H2								RTYPE=2 K1=2.00E-09 K2=0.0 K3=0.0 # gg08_rnum=1912
H31+ + C6H4 -> C6H31+ + H2 + H2							RTYPE=2 K1=4.55E-10 K2=-0.5 K3=0.0 # gg08_rnum=1913
H31+ + C6H4 -> C6H51+ + H2								RTYPE=2 K1=4.55E-10 K2=-0.5 K3=0.0 # gg08_rnum=1914
H31+ + C6H6 -> C6H71+ + H2								RTYPE=2 K1=3.90E-09 K2=0.0 K3=0.0 # gg08_rnum=1915
H31+ + C7 -> C7H1+ + H2									RTYPE=2 K1=2.00E-09 K2=0.0 K3=0.0 # gg08_rnum=1916
H31+ + C7H -> C7H21+ + H2								RTYPE=2 K1=2.42E-08 K2=-0.5 K3=0.0 # gg08_rnum=1917
H31+ + C7H2 -> C7H31+ + H2								RTYPE=2 K1=5.68E-09 K2=-0.5 K3=0.0 # gg08_rnum=1918
H31+ + C7H4 -> C7H51+ + H2								RTYPE=2 K1=8.88E-10 K2=-0.5 K3=0.0 # gg08_rnum=1919
H31+ + C7H4 -> C7H31+ + H2 + H2							RTYPE=2 K1=8.88E-10 K2=-0.5 K3=0.0 # gg08_rnum=1920
H31+ + C7N -> C7HN1+ + H2								RTYPE=2 K1=1.61E-08 K2=-0.5 K3=0.0 # gg08_rnum=1921
H31+ + C8 -> C8H1+ + H2									RTYPE=2 K1=2.00E-09 K2=0.0 K3=0.0 # gg08_rnum=1922
H31+ + C8H -> C8H21+ + H2								RTYPE=2 K1=2.68E-08 K2=-0.5 K3=0.0 # gg08_rnum=1923
H31+ + C8H2 -> C8H31+ + H2								RTYPE=2 K1=2.00E-09 K2=0.0 K3=0.0 # gg08_rnum=1924
H31+ + C8H4 -> C8H51+ + H2								RTYPE=2 K1=4.53E-10 K2=-0.5 K3=0.0 # gg08_rnum=1925
H31+ + C8H4 -> C8H31+ + H2 + H2							RTYPE=2 K1=4.53E-10 K2=-0.5 K3=0.0 # gg08_rnum=1926
H31+ + C9 -> C9H1+ + H2									RTYPE=2 K1=2.00E-09 K2=0.0 K3=0.0 # gg08_rnum=1927
H31+ + C9H -> C9H21+ + H2								RTYPE=2 K1=2.52E-08 K2=-0.5 K3=0.0 # gg08_rnum=1928
H31+ + C9H2 -> C9H31+ + H2								RTYPE=2 K1=5.65E-09 K2=-0.5 K3=0.0 # gg08_rnum=1929
H31+ + C9H4 -> C9H31+ + H2 + H2							RTYPE=2 K1=8.83E-10 K2=-0.5 K3=0.0 # gg08_rnum=1930
H31+ + C9H4 -> C9H51+ + H2								RTYPE=2 K1=8.83E-10 K2=-0.5 K3=0.0 # gg08_rnum=1931
H31+ + C9N -> C9HN1+ + H2								RTYPE=2 K1=1.76E-08 K2=-0.5 K3=0.0 # gg08_rnum=1932
H31+ + CCP -> HCCP1+ + H2								RTYPE=2 K1=5.42E-09 K2=-0.5 K3=0.0 # gg08_rnum=1933
H31+ + CH -> CH21+ + H2									RTYPE=2 K1=8.50E-09 K2=-0.5 K3=0.0 # gg08_rnum=1934
H31+ + CH2 -> CH31+ + H2								RTYPE=2 K1=1.40E-09 K2=-0.5 K3=0.0 # gg08_rnum=1935
H31+ + H2C2N -> CH3CN1+ + H2							RTYPE=2 K1=3.70E-09 K2=-0.5 K3=0.0 # gg08_rnum=1936
H31+ + CH2CO -> CH3CO1+ + H2							RTYPE=2 K1=3.30E-09 K2=-0.5 K3=0.0 # gg08_rnum=1937
H31+ + CH2NH -> CH3NH1+ + H2							RTYPE=2 K1=4.70E-09 K2=-0.5 K3=0.0 # gg08_rnum=1938
H31+ + CH2OH -> CH3OH1+ + H2							RTYPE=2 K1=3.00E-09 K2=-0.5 K3=0.0 # gg08_rnum=1939
H31+ + CH2PH -> CH2PH21+ + H2							RTYPE=2 K1=1.15E-09 K2=-0.5 K3=0.0 # gg08_rnum=1940
H31+ + CH3 -> CH41+ + H2								RTYPE=2 K1=2.10E-09 K2=0.0 K3=0.0 # gg08_rnum=1941
H31+ + CH3C3N -> C4H4N1+ + H2							RTYPE=2 K1=1.08E-08 K2=-0.5 K3=0.0 # gg08_rnum=1942
H31+ + CH3C4H -> C5H51+ + H2							RTYPE=2 K1=2.76E-09 K2=-0.5 K3=0.0 # gg08_rnum=1943
H31+ + CH3C5N -> C6H4N1+ + H2							RTYPE=2 K1=1.19E-08 K2=-0.5 K3=0.0 # gg08_rnum=1944
H31+ + CH3C6H -> C7H51+ + H2							RTYPE=2 K1=3.40E-09 K2=-0.5 K3=0.0 # gg08_rnum=1945
H31+ + CH3C7N -> C8H4N1+ + H2							RTYPE=2 K1=1.24E-08 K2=-0.5 K3=0.0 # gg08_rnum=1946
H31+ + CH3CHO -> CH3CH2O1+ + H2							RTYPE=2 K1=6.20E-09 K2=-0.5 K3=0.0 # gg08_rnum=1947
H31+ + CH3CN -> CH3CNH1+ + H2							RTYPE=2 K1=9.10E-09 K2=-0.5 K3=0.0 # gg08_rnum=1948
H31+ + CH3CO -> CH3CHO1+ + H2							RTYPE=2 K1=4.47E-09 K2=-0.5 K3=0.0 # gg08_rnum=1949
H31+ + CH3NH -> CH3NH21+ + H2							RTYPE=2 K1=2.62E-09 K2=-0.5 K3=0.0 # gg08_rnum=1950
H31+ + CH3NH2 -> CH3NH31+ + H2							RTYPE=2 K1=3.10E-09 K2=-0.5 K3=0.0 # gg08_rnum=1951
H31+ + CH3O -> CH3OH1+ + H2								RTYPE=2 K1=4.58E-10 K2=-0.5 K3=0.0 # gg08_rnum=1952
H31+ + CH3OCH3 -> CH3OCH41+ + H2						RTYPE=2 K1=3.00E-09 K2=-0.5 K3=0.0 # gg08_rnum=1953
H31+ + CH3OH -> CH3O1+ + H2 + H2						RTYPE=2 K1=1.12E-09 K2=-0.5 K3=0.0 # gg08_rnum=1954
H31+ + CH3OH -> CH3OH21+ + H2							RTYPE=2 K1=1.04E-09 K2=-0.5 K3=0.0 # gg08_rnum=1955
H31+ + CH3OH -> CH31+ + H2O + H2						RTYPE=2 K1=1.80E-09 K2=-0.5 K3=0.0 # gg08_rnum=1956
H31+ + CH4 -> CH51+ + H2								RTYPE=2 K1=2.40E-09 K2=0.0 K3=0.0 # gg08_rnum=1957
H31+ + Cl -> ClH1+ + H2									RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=1958
H31+ + CN -> HCN1+ + H2									RTYPE=2 K1=8.10E-09 K2=-0.5 K3=0.0 # gg08_rnum=1959
H31+ + CO -> HCO1+ + H2									RTYPE=2 K1=1.61E-09 K2=0.0 K3=0.0 # gg08_rnum=1960
H31+ + CO -> HOC1+ + H2									RTYPE=2 K1=9.44E-11 K2=0.0 K3=0.0 # gg08_rnum=1961
H31+ + CO2 -> HOCO1+ + H2								RTYPE=2 K1=1.90E-09 K2=0.0 K3=0.0 # gg08_rnum=1962
H31+ + COCHO -> CHOCHO1+ + H2							RTYPE=2 K1=1.94E-09 K2=-0.5 K3=0.0 # gg08_rnum=1963
H31+ + COOH -> HCOOH1+ + H2								RTYPE=2 K1=6.74E-09 K2=-0.5 K3=0.0 # gg08_rnum=1964
H31+ + CP -> HCP1+ + H2									RTYPE=2 K1=4.70E-09 K2=-0.5 K3=0.0 # gg08_rnum=1965
H31+ + CS -> HCS1+ + H2									RTYPE=2 K1=1.10E-08 K2=-0.5 K3=0.0 # gg08_rnum=1966
H31+ + Fe -> Fe1+ + H + H2								RTYPE=2 K1=4.90E-09 K2=0.0 K3=0.0 # gg08_rnum=1967
H31+ + H2CO -> CH2OH1+ + H2								RTYPE=2 K1=5.50E-09 K2=-0.5 K3=0.0 # gg08_rnum=1968
H31+ + H2CS -> H3CS1+ + H2								RTYPE=2 K1=3.80E-09 K2=-0.5 K3=0.0 # gg08_rnum=1969
H31+ + H2O -> H3O1+ + H2								RTYPE=2 K1=4.50E-09 K2=-0.5 K3=0.0 # gg08_rnum=1970
H31+ + H2S -> H3S1+ + H2								RTYPE=2 K1=2.30E-09 K2=-0.5 K3=0.0 # gg08_rnum=1971
H31+ + H2S2 -> H3S21+ + H2								RTYPE=2 K1=2.74E-09 K2=-0.5 K3=0.0 # gg08_rnum=1972
H31+ + H2SiO -> H3SiO1+ + H2							RTYPE=2 K1=4.61E-09 K2=-0.5 K3=0.0 # gg08_rnum=1973
H31+ + H5C3N -> C3H4N1+ + H2 + H2						RTYPE=2 K1=9.22E-09 K2=-0.5 K3=0.0 # gg08_rnum=1974
H31+ + HC2NC -> HC2NCH1+ + H2							RTYPE=2 K1=1.28E-08 K2=-0.5 K3=0.0 # gg08_rnum=1975
H31+ + HC3N -> HC3NH1+ + H2								RTYPE=2 K1=2.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=1976
H31+ + HC5N -> C5H2N1+ + H2								RTYPE=2 K1=2.30E-08 K2=-0.5 K3=0.0 # gg08_rnum=1977
H31+ + HC7N -> C7H2N1+ + H2								RTYPE=2 K1=2.48E-08 K2=-0.5 K3=0.0 # gg08_rnum=1978
H31+ + HC9N -> C9H2N1+ + H2								RTYPE=2 K1=2.59E-08 K2=-0.5 K3=0.0 # gg08_rnum=1979
H31+ + HCCP -> PC2H21+ + H2								RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=1980
H31+ + ClH -> ClH21+ + H2								RTYPE=2 K1=5.93E-09 K2=-0.5 K3=0.0 # gg08_rnum=1981
H31+ + HCN -> H2CN1+ + H2								RTYPE=2 K1=1.70E-08 K2=-0.5 K3=0.0 # gg08_rnum=1982
H31+ + C2NCH -> HC2NCH1+ + H2							RTYPE=2 K1=4.40E-08 K2=-0.5 K3=0.0 # gg08_rnum=1983
H31+ + HCO -> H2CO1+ + H2								RTYPE=2 K1=2.30E-09 K2=-0.5 K3=0.0 # gg08_rnum=1984
H31+ + HCOOCH3 -> HCOOCH3H1+ + H2						RTYPE=2 K1=4.05E-09 K2=-0.5 K3=0.0 # gg08_rnum=1985
H31+ + HCOOH -> H3O1+ + CO + H2							RTYPE=2 K1=9.70E-10 K2=-0.5 K3=0.0 # gg08_rnum=1986
H31+ + HCOOH -> HCO1+ + H2O + H2						RTYPE=2 K1=2.30E-09 K2=-0.5 K3=0.0 # gg08_rnum=1987
H31+ + HCP -> PCH21+ + H2								RTYPE=2 K1=2.13E-09 K2=-0.5 K3=0.0 # gg08_rnum=1988
H31+ + HCS -> H2CS1+ + H2								RTYPE=2 K1=1.09E-08 K2=-0.5 K3=0.0 # gg08_rnum=1989
H31+ + SiCH -> SiCH21+ + H2								RTYPE=2 K1=5.47E-09 K2=-0.5 K3=0.0 # gg08_rnum=1990
H31+ + CH3COCHO -> CH3COCHOH1+ + H2						RTYPE=2 K1=7.99E-09 K2=-0.5 K3=0.0 # gg08_rnum=1991
H31+ + CH3COCH3 -> CH3COCH41+ + H2						RTYPE=2 K1=5.52E-09 K2=-0.5 K3=0.0 # gg08_rnum=1992
H31+ + CH3CONH -> CH3CONH21+ + H2						RTYPE=2 K1=7.23E-09 K2=-0.5 K3=0.0 # gg08_rnum=1993
H31+ + CH3COOH -> CH3COOH21+ + H2						RTYPE=2 K1=3.33E-09 K2=-0.5 K3=0.0 # gg08_rnum=1994
H31+ + CH3CONH2 -> CH3CONH2H1+ + H2						RTYPE=2 K1=7.21E-09 K2=-0.5 K3=0.0 # gg08_rnum=1995
H31+ + CH3COOCH3 -> CH3COOCH3H1+ + H2					RTYPE=2 K1=3.40E-09 K2=-0.5 K3=0.0 # gg08_rnum=1996
H31+ + CH3COCH2OH -> CH3COCH2OHH1+ + H2					RTYPE=2 K1=5.91E-09 K2=-0.5 K3=0.0 # gg08_rnum=1997
H31+ + HNC -> H2CN1+ + H2								RTYPE=2 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=1998
H31+ + HNC3 -> HC3NH1+ + H2								RTYPE=2 K1=1.14E-08 K2=-0.5 K3=0.0 # gg08_rnum=1999
H31+ + HNCHO -> NH2CHO1+ + H2							RTYPE=2 K1=3.89E-09 K2=-0.5 K3=0.0 # gg08_rnum=2000
H31+ + HNCO -> HNCHO1+ + H2								RTYPE=2 K1=3.92E-09 K2=-0.5 K3=0.0 # gg08_rnum=2001
H31+ + HNCOCHO -> NH2COCHO1+ + H2						RTYPE=2 K1=1.92E-09 K2=-0.5 K3=0.0 # gg08_rnum=2002
H31+ + HNCONH -> NH2CONH1+ + H2							RTYPE=2 K1=7.41E-09 K2=-0.5 K3=0.0 # gg08_rnum=2003
H31+ + HNCOOH -> NH2COOH1+ + H2							RTYPE=2 K1=3.85E-09 K2=-0.5 K3=0.0 # gg08_rnum=2004
H31+ + HNO -> H2NO1+ + H2								RTYPE=2 K1=4.00E-09 K2=-0.5 K3=0.0 # gg08_rnum=2005
H31+ + HNOH -> NH2OH1+ + H2								RTYPE=2 K1=3.91E-09 K2=-0.5 K3=0.0 # gg08_rnum=2006
H31+ + HNSi -> SiNH21+ + H2								RTYPE=2 K1=8.74E-10 K2=-0.5 K3=0.0 # gg08_rnum=2007
H31+ + CH3ONH -> NH2OCH31+ + H2							RTYPE=2 K1=2.89E-09 K2=-0.5 K3=0.0 # gg08_rnum=2008
H31+ + HNCH2OH -> NH2CH2OH1+ + H2						RTYPE=2 K1=2.89E-09 K2=-0.5 K3=0.0 # gg08_rnum=2009
H31+ + HOCOOH -> HOCOOH21+ + H2							RTYPE=2 K1=3.84E-09 K2=-0.5 K3=0.0 # gg08_rnum=2010
H31+ + HPO -> H2PO1+ + H2								RTYPE=2 K1=1.27E-08 K2=-0.5 K3=0.0 # gg08_rnum=2011
H31+ + HS -> H2S1+ + H2									RTYPE=2 K1=4.20E-09 K2=-0.5 K3=0.0 # gg08_rnum=2012
H31+ + S2H -> H2S21+ + H2								RTYPE=2 K1=4.56E-09 K2=-0.5 K3=0.0 # gg08_rnum=2013
H31+ + HCOCOCHO -> HCOCOCHOH1+ + H2						RTYPE=2 K1=3.83E-09 K2=-0.5 K3=0.0 # gg08_rnum=2014
H31+ + Mg -> Mg1+ + H + H2								RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=2015
H31+ + MgH -> Mg1+ + H2 + H2							RTYPE=2 K1=7.20E-09 K2=-0.5 K3=0.0 # gg08_rnum=2016
H31+ + N -> NH21+ + H									RTYPE=2 K1=1.00E-17 K2=0.0 K3=0.0 # gg08_rnum=2017
H31+ + N2 -> N2H1+ + H2									RTYPE=2 K1=1.70E-09 K2=0.0 K3=0.0 # gg08_rnum=2018
H31+ + N2H2 -> N2H1+ + H2 + H2							RTYPE=2 K1=2.00E-09 K2=0.0 K3=0.0 # gg08_rnum=2019
H31+ + Na -> Na1+ + H + H2								RTYPE=2 K1=1.10E-09 K2=0.0 K3=0.0 # gg08_rnum=2020
H31+ + NaH -> Na1+ + H2 + H2							RTYPE=2 K1=3.80E-08 K2=-0.5 K3=0.0 # gg08_rnum=2021
H31+ + NaOH -> NaH2O1+ + H2								RTYPE=2 K1=1.20E-08 K2=-0.5 K3=0.0 # gg08_rnum=2022
H31+ + NH -> NH21+ + H2									RTYPE=2 K1=7.50E-09 K2=-0.5 K3=0.0 # gg08_rnum=2023
H31+ + NH2 -> NH31+ + H2								RTYPE=2 K1=4.50E-09 K2=-0.5 K3=0.0 # gg08_rnum=2024
H31+ + NH2CHO -> NH2CHOH1+ + H2							RTYPE=2 K1=2.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=2025
H31+ + NH2CN -> NH2CNH1+ + H2							RTYPE=2 K1=9.86E-09 K2=-0.5 K3=0.0 # gg08_rnum=2026
H31+ + NH2OH -> NH3OH1+ + H2							RTYPE=2 K1=1.16E-09 K2=-0.5 K3=0.0 # gg08_rnum=2027
H31+ + NH3 -> NH41+ + H2								RTYPE=2 K1=3.60E-09 K2=-0.5 K3=0.0 # gg08_rnum=2028
H31+ + NO -> HNO1+ + H2									RTYPE=2 K1=8.50E-10 K2=-0.5 K3=0.0 # gg08_rnum=2029
H31+ + NO2 -> NO1+ + H2 + OH							RTYPE=2 K1=7.28E-10 K2=-0.5 K3=0.0 # gg08_rnum=2030
H31+ + NS -> HNS1+ + H2									RTYPE=2 K1=9.90E-09 K2=-0.5 K3=0.0 # gg08_rnum=2031
H31+ + O -> OH1+ + H2									RTYPE=2 K1=8.00E-10 K2=0.0 K3=0.0 # gg08_rnum=2032
H31+ + O2 -> O2H1+ + H2									RTYPE=2 K1=6.40E-10 K2=0.0 K3=0.0 # gg08_rnum=2033
H31+ + OCS -> HOCS1+ + H2								RTYPE=2 K1=3.80E-09 K2=-0.5 K3=0.0 # gg08_rnum=2034
H31+ + OH -> H2O1+ + H2									RTYPE=2 K1=9.50E-09 K2=-0.5 K3=0.0 # gg08_rnum=2035
H31+ + CHOCHO -> CHOCHOH1+ + H2							RTYPE=2 K1=1.37E-09 K2=0.0 K3=0.0 # gg08_rnum=2036
H31+ + HCOCOOH -> HCOCOOH21+ + H2						RTYPE=2 K1=1.92E-09 K2=-0.5 K3=0.0 # gg08_rnum=2037
H31+ + P -> PH1+ + H2									RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=2038
H31+ + PH -> PH21+ + H2									RTYPE=2 K1=3.54E-09 K2=-0.5 K3=0.0 # gg08_rnum=2039
H31+ + PH2 -> PH31+ + H2								RTYPE=2 K1=1.17E-09 K2=-0.5 K3=0.0 # gg08_rnum=2040
H31+ + PN -> HPN1+ + H2									RTYPE=2 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=2041
H31+ + PO -> HPO1+ + H2									RTYPE=2 K1=1.02E-08 K2=-0.5 K3=0.0 # gg08_rnum=2042
H31+ + NH2CO -> NH2CHO1+ + H2							RTYPE=2 K1=3.89E-09 K2=-0.5 K3=0.0 # gg08_rnum=2043
H31+ + NH2COCHO -> NH2COCHOH1+ + H2						RTYPE=2 K1=1.92E-09 K2=-0.5 K3=0.0 # gg08_rnum=2044
H31+ + NH2CONH -> NH2CONH21+ + H2						RTYPE=2 K1=7.38E-09 K2=-0.5 K3=0.0 # gg08_rnum=2045
H31+ + NH2COOH -> NH2COOH21+ + H2						RTYPE=2 K1=3.84E-09 K2=-0.5 K3=0.0 # gg08_rnum=2046
H31+ + NH2CONH2 -> NH2CONH2H1+ + H2						RTYPE=2 K1=7.36E-09 K2=-0.5 K3=0.0 # gg08_rnum=2047
H31+ + NH2NH -> NH2NH21+ + H2							RTYPE=2 K1=3.49E-09 K2=-0.5 K3=0.0 # gg08_rnum=2048
H31+ + NH2NH2 -> NH2NH2H1+ + H2							RTYPE=2 K1=3.49E-09 K2=-0.5 K3=0.0 # gg08_rnum=2049
H31+ + NH2OCH3 -> HNH2OCH31+ + H2						RTYPE=2 K1=3.84E-09 K2=-0.5 K3=0.0 # gg08_rnum=2050
H31+ + NH2CH2OH -> HNH2CH2OH1+ + H2						RTYPE=2 K1=2.88E-09 K2=-0.5 K3=0.0 # gg08_rnum=2051
H31+ + S -> HS1+ + H2									RTYPE=2 K1=2.60E-09 K2=0.0 K3=0.0 # gg08_rnum=2052
H31+ + S2 -> S2H1+ + H2									RTYPE=2 K1=2.00E-09 K2=0.0 K3=0.0 # gg08_rnum=2053
H31+ + Si -> SiH1+ + H2									RTYPE=2 K1=3.70E-09 K2=0.0 K3=0.0 # gg08_rnum=2054
H31+ + SiC -> SiCH1+ + H2								RTYPE=2 K1=9.31E-09 K2=-0.5 K3=0.0 # gg08_rnum=2055
H31+ + SiC2 -> SiC2H1+ + H2								RTYPE=2 K1=5.48E-09 K2=-0.5 K3=0.0 # gg08_rnum=2056
H31+ + SiC2H -> SiC2H21+ + H2							RTYPE=2 K1=3.21E-09 K2=-0.5 K3=0.0 # gg08_rnum=2057
H31+ + SiC2H2 -> SiC2H31+ + H2							RTYPE=2 K1=5.73E-09 K2=-0.5 K3=0.0 # gg08_rnum=2058
H31+ + cSiC3 -> SiC3H1+ + H2							RTYPE=2 K1=4.57E-09 K2=-0.5 K3=0.0 # gg08_rnum=2059
H31+ + SiC3H -> SiC3H21+ + H2							RTYPE=2 K1=6.85E-09 K2=-0.5 K3=0.0 # gg08_rnum=2060
H31+ + SiC4 -> SiC4H1+ + H2								RTYPE=2 K1=1.43E-08 K2=-0.5 K3=0.0 # gg08_rnum=2061
H31+ + SiCH2 -> SiCH31+ + H2							RTYPE=2 K1=3.46E-09 K2=-0.5 K3=0.0 # gg08_rnum=2062
H31+ + SiCH3 -> SiCH41+ + H2							RTYPE=2 K1=3.46E-09 K2=-0.5 K3=0.0 # gg08_rnum=2063
H31+ + SiH -> SiH21+ + H2								RTYPE=2 K1=6.66E-10 K2=-0.5 K3=0.0 # gg08_rnum=2064
H31+ + SiH2 -> SiH31+ + H2								RTYPE=2 K1=4.21E-10 K2=-0.5 K3=0.0 # gg08_rnum=2065
H31+ + SiH3 -> SiH41+ + H2								RTYPE=2 K1=2.00E-09 K2=0.0 K3=0.0 # gg08_rnum=2066
H31+ + SiH4 -> SiH51+ + H2								RTYPE=2 K1=2.00E-09 K2=0.0 K3=0.0 # gg08_rnum=2067
H31+ + SiN -> HNSi1+ + H2								RTYPE=2 K1=1.26E-08 K2=-0.5 K3=0.0 # gg08_rnum=2068
H31+ + SiNC -> SiNCH1+ + H2								RTYPE=2 K1=4.65E-09 K2=-0.5 K3=0.0 # gg08_rnum=2069
H31+ + SiO -> HSiO1+ + H2								RTYPE=2 K1=1.70E-08 K2=-0.5 K3=0.0 # gg08_rnum=2070
H31+ + SiO2 -> HSiO21+ + H2								RTYPE=2 K1=1.14E-09 K2=-0.5 K3=0.0 # gg08_rnum=2071
H31+ + SiS -> HSiS1+ + H2								RTYPE=2 K1=9.40E-09 K2=-0.5 K3=0.0 # gg08_rnum=2072
H31+ + SO -> HSO1+ + H2									RTYPE=2 K1=8.40E-09 K2=-0.5 K3=0.0 # gg08_rnum=2073
H31+ + SO2 -> HSO21+ + H2								RTYPE=2 K1=3.72E-09 K2=-0.5 K3=0.0 # gg08_rnum=2074
H31+ + CH3OCO -> HCOOCH31+ + H2							RTYPE=2 K1=3.40E-09 K2=-0.5 K3=0.0 # gg08_rnum=2075
H31+ + CH3OCOCHO -> CH3OCOCHOH1+ + H2					RTYPE=2 K1=1.91E-09 K2=-0.5 K3=0.0 # gg08_rnum=2076
H31+ + CH3OCONH -> CH3OCONH21+ + H2						RTYPE=2 K1=3.82E-09 K2=-0.5 K3=0.0 # gg08_rnum=2077
H31+ + CH3OCOOH -> CH3OCOOH21+ + H2						RTYPE=2 K1=3.82E-09 K2=-0.5 K3=0.0 # gg08_rnum=2078
H31+ + CH3OCONH2 -> CH3OCONH2H1+ + H2					RTYPE=2 K1=3.82E-09 K2=-0.5 K3=0.0 # gg08_rnum=2079
H31+ + CH3OCOOCH3 -> CH3OCOOCH3H1+ + H2					RTYPE=2 K1=3.43E-10 K2=-0.5 K3=0.0 # gg08_rnum=2080
H31+ + CH3OCOCH2OH -> CH3OCOCH2OHH1+ + H2				RTYPE=2 K1=6.07E-09 K2=-0.5 K3=0.0 # gg08_rnum=2081
H31+ + CH3OOH -> CH3OOH21+ + H2							RTYPE=2 K1=3.33E-09 K2=-0.5 K3=0.0 # gg08_rnum=2082
H31+ + CH3OOCH3 -> CH3OH + CH3O1+ + H2					RTYPE=2 K1=2.86E-09 K2=-0.5 K3=0.0 # gg08_rnum=2083
H31+ + CH3OCH2OH -> CH3OCH2OH21+ + H2					RTYPE=2 K1=2.86E-09 K2=-0.5 K3=0.0 # gg08_rnum=2084
H31+ + CH2OHCHO -> CH2OHCHOH1+ + H2						RTYPE=2 K1=5.22E-09 K2=-0.5 K3=0.0 # gg08_rnum=2085
H31+ + HOCH2CO -> CH2OHCHO1+ + H2						RTYPE=2 K1=5.24E-09 K2=-0.5 K3=0.0 # gg08_rnum=2086
H31+ + HOCH2COCHO -> HOCH2COCHOH1+ + H2					RTYPE=2 K1=1.91E-09 K2=-0.5 K3=0.0 # gg08_rnum=2087
H31+ + HNCOCH2OH -> NH2COCH2OH1+ + H2					RTYPE=2 K1=3.82E-09 K2=-0.5 K3=0.0 # gg08_rnum=2088
H31+ + CH2OHCOOH -> CH2OHCOOH21+ + H2					RTYPE=2 K1=3.43E-09 K2=-0.5 K3=0.0 # gg08_rnum=2089
H31+ + NH2COCH2OH -> NH2COCH2OH21+ + H2					RTYPE=2 K1=3.82E-09 K2=-0.5 K3=0.0 # gg08_rnum=2090
H31+ + HOCH2COCH2OH -> HOCH2COCH2OH21+ + H2				RTYPE=2 K1=3.43E-09 K2=-0.5 K3=0.0 # gg08_rnum=2091
H31+ + HOCH2OH -> HOCH2OH21+ + H2						RTYPE=2 K1=5.88E-09 K2=-0.5 K3=0.0 # gg08_rnum=2092
H31+ + HOCH2CH2OH -> HOCH2CH2OH21+ + H2					RTYPE=2 K1=1.48E-09 K2=0.0 K3=0.0 # gg08_rnum=2093
CH3C3N1+ + H2 -> C4H4N1+ + H							RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=2094
H3O1+ + C -> HCO1+ + H2									RTYPE=2 K1=1.00E-11 K2=0.0 K3=0.0 # gg08_rnum=2095
H3O1+ + H2C2H -> H2C2H21+ + H2O							RTYPE=2 K1=1.74E-09 K2=-0.5 K3=0.0 # gg08_rnum=2096
H3O1+ + CH3CH2CHO -> CH3CH2CHOH1+ + H2O					RTYPE=2 K1=2.87E-09 K2=-0.5 K3=0.0 # gg08_rnum=2097
H3O1+ + CH3CH2OH -> CH3CH2OH21+ + H2O					RTYPE=2 K1=1.79E-09 K2=-0.5 K3=0.0 # gg08_rnum=2098
H3O1+ + C2O -> C2OH1+ + H2O								RTYPE=2 K1=3.32E-09 K2=-0.5 K3=0.0 # gg08_rnum=2099
H3O1+ + C2S -> HC2S1+ + H2O								RTYPE=2 K1=2.88E-09 K2=-0.5 K3=0.0 # gg08_rnum=2100
H3O1+ + C3 -> C3H1+ + H2O								RTYPE=2 K1=2.00E-09 K2=0.0 K3=0.0 # gg08_rnum=2101
H3O1+ + C3H -> C3H21+ + H2O								RTYPE=2 K1=8.03E-09 K2=-0.5 K3=0.0 # gg08_rnum=2102
H3O1+ + C3H2 -> C3H31+ + H2O							RTYPE=2 K1=2.48E-09 K2=-0.5 K3=0.0 # gg08_rnum=2103
H3O1+ + C3H3 -> C3H41+ + H2O							RTYPE=2 K1=4.34E-09 K2=-0.5 K3=0.0 # gg08_rnum=2104
H3O1+ + C3H4 -> C3H51+ + H2O							RTYPE=2 K1=8.44E-10 K2=-0.5 K3=0.0 # gg08_rnum=2105
H3O1+ + C3N -> NC3H1+ + H2O								RTYPE=2 K1=5.44E-09 K2=-0.5 K3=0.0 # gg08_rnum=2106
H3O1+ + C3O -> HC3O1+ + H2O								RTYPE=2 K1=5.88E-09 K2=-0.5 K3=0.0 # gg08_rnum=2107
H3O1+ + C3P -> PC3H1+ + H2O								RTYPE=2 K1=1.01E-09 K2=-0.5 K3=0.0 # gg08_rnum=2108
H3O1+ + C3S -> HC3S1+ + H2O								RTYPE=2 K1=3.92E-09 K2=-0.5 K3=0.0 # gg08_rnum=2109
H3O1+ + C4 -> C4H1+ + H2O								RTYPE=2 K1=1.10E-09 K2=0.0 K3=0.0 # gg08_rnum=2110
H3O1+ + C4H -> HC4H1+ + H2O								RTYPE=2 K1=2.23E-09 K2=-0.5 K3=0.0 # gg08_rnum=2111
H3O1+ + HC4H -> C4H31+ + H2O							RTYPE=2 K1=1.10E-09 K2=0.0 K3=0.0 # gg08_rnum=2112
H3O1+ + C4H3 -> C4H41+ + H2O							RTYPE=2 K1=1.00E-09 K2=-0.5 K3=0.0 # gg08_rnum=2113
H3O1+ + C4P -> PC4H1+ + H2O								RTYPE=2 K1=4.95E-10 K2=-0.5 K3=0.0 # gg08_rnum=2114
H3O1+ + C4S -> HC4S1+ + H2O								RTYPE=2 K1=2.97E-09 K2=-0.5 K3=0.0 # gg08_rnum=2115
H3O1+ + C6H6 -> C6H71+ + H2O							RTYPE=2 K1=1.80E-09 K2=0.0 K3=0.0 # gg08_rnum=2116
H3O1+ + CCP -> HCCP1+ + H2O								RTYPE=2 K1=2.44E-09 K2=-0.5 K3=0.0 # gg08_rnum=2117
H3O1+ + CH -> CH21+ + H2O								RTYPE=2 K1=4.80E-09 K2=-0.5 K3=0.0 # gg08_rnum=2118
H3O1+ + CH2 -> CH31+ + H2O								RTYPE=2 K1=7.78E-10 K2=-0.5 K3=0.0 # gg08_rnum=2119
H3O1+ + CH2CO -> CH3CO1+ + H2O							RTYPE=2 K1=1.52E-09 K2=-0.5 K3=0.0 # gg08_rnum=2120
H3O1+ + CH2OH -> CH3OH1+ + H2O							RTYPE=2 K1=1.34E-09 K2=-0.5 K3=0.0 # gg08_rnum=2121
H3O1+ + CH2PH -> CH2PH21+ + H2O							RTYPE=2 K1=5.29E-10 K2=-0.5 K3=0.0 # gg08_rnum=2122
H3O1+ + CH3CHO -> CH3CH2O1+ + H2O						RTYPE=2 K1=2.86E-09 K2=-0.5 K3=0.0 # gg08_rnum=2123
H3O1+ + CH3CN -> CH3CNH1+ + H2O							RTYPE=2 K1=4.22E-09 K2=-0.5 K3=0.0 # gg08_rnum=2124
H3O1+ + CH3NH -> CH3NH21+ + H2O							RTYPE=2 K1=1.30E-09 K2=-0.5 K3=0.0 # gg08_rnum=2125
H3O1+ + CH3NH2 -> CH3NH31+ + H2O						RTYPE=2 K1=1.30E-09 K2=-0.5 K3=0.0 # gg08_rnum=2126
H3O1+ + CH3O -> CH3OH1+ + H2O							RTYPE=2 K1=2.05E-10 K2=-0.5 K3=0.0 # gg08_rnum=2127
H3O1+ + CH3OCH3 -> CH3OCH41+ + H2O						RTYPE=2 K1=1.37E-09 K2=-0.5 K3=0.0 # gg08_rnum=2128
H3O1+ + CH3OH -> CH3OH21+ + H2O							RTYPE=2 K1=1.91E-09 K2=-0.5 K3=0.0 # gg08_rnum=2129
H3O1+ + COCHO -> CHOCHO1+ + H2O							RTYPE=2 K1=8.74E-10 K2=-0.5 K3=0.0 # gg08_rnum=2130
H3O1+ + CP -> HCP1+ + H2O								RTYPE=2 K1=2.17E-09 K2=-0.5 K3=0.0 # gg08_rnum=2131
H3O1+ + CS -> HCS1+ + H2O								RTYPE=2 K1=4.99E-09 K2=-0.5 K3=0.0 # gg08_rnum=2132
H3O1+ + H2CO -> CH2OH1+ + H2O							RTYPE=2 K1=2.60E-09 K2=-0.5 K3=0.0 # gg08_rnum=2133
H3O1+ + H2S -> H3S1+ + H2O								RTYPE=2 K1=1.10E-09 K2=-0.5 K3=0.0 # gg08_rnum=2134
H3O1+ + H2S2 -> H3S21+ + H2O							RTYPE=2 K1=1.21E-09 K2=-0.5 K3=0.0 # gg08_rnum=2135
H3O1+ + H2SiO -> H3SiO1+ + H2O							RTYPE=2 K1=2.11E-09 K2=-0.5 K3=0.0 # gg08_rnum=2136
H3O1+ + HC2NC -> HC2NCH1+ + H2O							RTYPE=2 K1=5.79E-09 K2=-0.5 K3=0.0 # gg08_rnum=2137
H3O1+ + HC3N -> HC3NH1+ + H2O							RTYPE=2 K1=9.17E-09 K2=-0.5 K3=0.0 # gg08_rnum=2138
H3O1+ + HCCP -> PC2H21+ + H2O							RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=2139
H3O1+ + HCN -> H2CN1+ + H2O								RTYPE=2 K1=8.20E-09 K2=-0.5 K3=0.0 # gg08_rnum=2140
H3O1+ + C2NCH -> HC2NCH1+ + H2O							RTYPE=2 K1=2.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=2141
H3O1+ + HCOOCH3 -> HCOOCH3H1+ + H2O						RTYPE=2 K1=1.81E-09 K2=-0.5 K3=0.0 # gg08_rnum=2142
H3O1+ + HCOOH -> HCOOH21+ + H2O							RTYPE=2 K1=1.19E-09 K2=-0.5 K3=0.0 # gg08_rnum=2143
H3O1+ + HCP -> PCH21+ + H2O								RTYPE=2 K1=9.83E-10 K2=-0.5 K3=0.0 # gg08_rnum=2144
H3O1+ + SiCH -> SiCH21+ + H2O							RTYPE=2 K1=2.55E-09 K2=-0.5 K3=0.0 # gg08_rnum=2145
H3O1+ + CH3COCHO -> CH3COCHOH1+ + H2O					RTYPE=2 K1=3.34E-09 K2=-0.5 K3=0.0 # gg08_rnum=2146
H3O1+ + CH3COCH3 -> CH3COCH41+ + H2O					RTYPE=2 K1=2.33E-09 K2=-0.5 K3=0.0 # gg08_rnum=2147
H3O1+ + CH3CONH -> CH3CONH21+ + H2O						RTYPE=2 K1=3.09E-09 K2=-0.5 K3=0.0 # gg08_rnum=2148
H3O1+ + CH3COOH -> CH3COOH21+ + H2O						RTYPE=2 K1=7.41E-09 K2=-0.5 K3=0.0 # gg08_rnum=2149
H3O1+ + CH3CONH2 -> CH3CONH2H1+ + H2O					RTYPE=2 K1=3.06E-09 K2=-0.5 K3=0.0 # gg08_rnum=2150
H3O1+ + CH3COOCH3 -> CH3COOCH3H1+ + H2O					RTYPE=2 K1=1.40E-09 K2=-0.5 K3=0.0 # gg08_rnum=2151
H3O1+ + CH3COCH2OH -> CH3COCH2OHH1+ + H2O				RTYPE=2 K1=7.41E-09 K2=-0.5 K3=0.0 # gg08_rnum=2152
H3O1+ + HNC -> H2CN1+ + H2O								RTYPE=2 K1=7.42E-09 K2=-0.5 K3=0.0 # gg08_rnum=2153
H3O1+ + HNC3 -> HC3NH1+ + H2O							RTYPE=2 K1=5.19E-09 K2=-0.5 K3=0.0 # gg08_rnum=2154
H3O1+ + HNCHO -> NH2CHO1+ + H2O							RTYPE=2 K1=1.74E-09 K2=-0.5 K3=0.0 # gg08_rnum=2155
H3O1+ + HNCO -> HNCHO1+ + H2O							RTYPE=2 K1=1.82E-09 K2=-0.5 K3=0.0 # gg08_rnum=2156
H3O1+ + HNCOCHO -> NH2COCHO1+ + H2O						RTYPE=2 K1=8.23E-10 K2=-0.5 K3=0.0 # gg08_rnum=2157
H3O1+ + HNCONH -> NH2CONH1+ + H2O						RTYPE=2 K1=3.25E-09 K2=-0.5 K3=0.0 # gg08_rnum=2158
H3O1+ + HNCOOH -> NH2COOH1+ + H2O						RTYPE=2 K1=1.66E-09 K2=-0.5 K3=0.0 # gg08_rnum=2159
H3O1+ + HNOH -> NH2OH1+ + H2O							RTYPE=2 K1=1.80E-09 K2=-0.5 K3=0.0 # gg08_rnum=2160
H3O1+ + HNSi -> SiNH21+ + H2O							RTYPE=2 K1=4.04E-10 K2=-0.5 K3=0.0 # gg08_rnum=2161
H3O1+ + CH3ONH -> NH2OCH31+ + H2O						RTYPE=2 K1=1.25E-09 K2=-0.5 K3=0.0 # gg08_rnum=2162
H3O1+ + HNCH2OH -> NH2CH2OH1+ + H2O						RTYPE=2 K1=1.25E-09 K2=-0.5 K3=0.0 # gg08_rnum=2163
H3O1+ + HOCOOH -> HOCOOH21+ + H2O						RTYPE=2 K1=1.64E-09 K2=-0.5 K3=0.0 # gg08_rnum=2164
H3O1+ + S2H -> H2S21+ + H2O								RTYPE=2 K1=2.02E-09 K2=-0.5 K3=0.0 # gg08_rnum=2165
H3O1+ + HCOCOCHO -> HCOCOCHOH1+ + H2O					RTYPE=2 K1=1.62E-09 K2=-0.5 K3=0.0 # gg08_rnum=2166
H3O1+ + Na -> Na1+ + H2O + H							RTYPE=2 K1=3.10E-09 K2=0.0 K3=0.0 # gg08_rnum=2167
H3O1+ + NH2 -> NH31+ + H2O								RTYPE=2 K1=2.40E-09 K2=-0.5 K3=0.0 # gg08_rnum=2168
H3O1+ + NH2CHO -> NH2CHOH1+ + H2O						RTYPE=2 K1=9.37E-09 K2=-0.5 K3=0.0 # gg08_rnum=2169
H3O1+ + NH2OH -> NH3OH1+ + H2O							RTYPE=2 K1=5.18E-09 K2=-0.5 K3=0.0 # gg08_rnum=2170
H3O1+ + NH3 -> NH41+ + H2O								RTYPE=2 K1=1.90E-09 K2=-0.5 K3=0.0 # gg08_rnum=2171
H3O1+ + CHOCHO -> CHOCHOH1+ + H2O						RTYPE=2 K1=5.93E-10 K2=0.0 K3=0.0 # gg08_rnum=2172
H3O1+ + HCOCOOH -> HCOCOOH21+ + H2O						RTYPE=2 K1=8.13E-10 K2=-0.5 K3=0.0 # gg08_rnum=2173
H3O1+ + P -> HPO1+ + H2									RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=2174
H3O1+ + PN -> HPN1+ + H2O								RTYPE=2 K1=6.91E-09 K2=-0.5 K3=0.0 # gg08_rnum=2175
H3O1+ + NH2CO -> NH2CHO1+ + H2O							RTYPE=2 K1=1.74E-09 K2=-0.5 K3=0.0 # gg08_rnum=2176
H3O1+ + NH2COCHO -> NH2COCHOH1+ + H2O					RTYPE=2 K1=8.08E-10 K2=-0.5 K3=0.0 # gg08_rnum=2177
H3O1+ + NH2CONH -> NH2CONH21+ + H2O						RTYPE=2 K1=3.19E-09 K2=-0.5 K3=0.0 # gg08_rnum=2178
H3O1+ + NH2COOH -> NH2COOH21+ + H2O						RTYPE=2 K1=1.62E-09 K2=-0.5 K3=0.0 # gg08_rnum=2179
H3O1+ + NH2CONH2 -> NH2CONH2H1+ + H2O					RTYPE=2 K1=3.15E-09 K2=-0.5 K3=0.0 # gg08_rnum=2180
H3O1+ + NH2NH -> NH2NH21+ + H2O							RTYPE=2 K1=1.71E-09 K2=-0.5 K3=0.0 # gg08_rnum=2181
H3O1+ + NH2NH2 -> NH2NH2H1+ + H2O						RTYPE=2 K1=1.71E-09 K2=-0.5 K3=0.0 # gg08_rnum=2182
H3O1+ + NH2OCH3 -> HNH2OCH31+ + H2O						RTYPE=2 K1=1.64E-09 K2=-0.5 K3=0.0 # gg08_rnum=2183
H3O1+ + NH2CH2OH -> HNH2CH2OH1+ + H2O					RTYPE=2 K1=1.23E-09 K2=-0.5 K3=0.0 # gg08_rnum=2184
H3O1+ + S2 -> S2H1+ + H2O								RTYPE=2 K1=2.00E-09 K2=0.0 K3=0.0 # gg08_rnum=2185
H3O1+ + Si -> SiH1+ + H2O								RTYPE=2 K1=1.80E-09 K2=0.0 K3=0.0 # gg08_rnum=2186
H3O1+ + SiC -> SiCH1+ + H2O								RTYPE=2 K1=4.35E-09 K2=-0.5 K3=0.0 # gg08_rnum=2187
H3O1+ + SiC2 -> SiC2H1+ + H2O							RTYPE=2 K1=2.48E-09 K2=-0.5 K3=0.0 # gg08_rnum=2188
H3O1+ + SiC2H -> SiC2H21+ + H2O							RTYPE=2 K1=1.45E-09 K2=-0.5 K3=0.0 # gg08_rnum=2189
H3O1+ + SiCH3 -> SiCH41+ + H2O							RTYPE=2 K1=1.60E-09 K2=-0.5 K3=0.0 # gg08_rnum=2190
H3O1+ + SiH -> SiH21+ + H2O								RTYPE=2 K1=3.25E-10 K2=-0.5 K3=0.0 # gg08_rnum=2191
H3O1+ + SiH2 -> SiH31+ + H2O							RTYPE=2 K1=2.05E-10 K2=-0.5 K3=0.0 # gg08_rnum=2192
H3O1+ + SiNC -> SiNCH1+ + H2O							RTYPE=2 K1=2.10E-09 K2=-0.5 K3=0.0 # gg08_rnum=2193
H3O1+ + SiO -> HSiO1+ + H2O								RTYPE=2 K1=7.81E-09 K2=-0.5 K3=0.0 # gg08_rnum=2194
H3O1+ + CH3OCO -> HCOOCH31+ + H2O						RTYPE=2 K1=1.44E-09 K2=-0.5 K3=0.0 # gg08_rnum=2195
H3O1+ + CH3OCOCHO -> CH3OCOCHOH1+ + H2O					RTYPE=2 K1=7.87E-10 K2=-0.5 K3=0.0 # gg08_rnum=2196
H3O1+ + CH3OCONH -> CH3OCONH21+ + H2O					RTYPE=2 K1=1.60E-09 K2=-0.5 K3=0.0 # gg08_rnum=2197
H3O1+ + CH3OCOOH -> CH3OCOOH21+ + H2O					RTYPE=2 K1=1.58E-09 K2=-0.5 K3=0.0 # gg08_rnum=2198
H3O1+ + CH3OCONH2 -> CH3OCONH2H1+ + H2O					RTYPE=2 K1=1.58E-09 K2=-0.5 K3=0.0 # gg08_rnum=2199
H3O1+ + CH3OCOOCH3 -> CH3OCOOCH3H1+ + H2O				RTYPE=2 K1=1.40E-10 K2=-0.5 K3=0.0 # gg08_rnum=2200
H3O1+ + CH3OCOCH2OH -> CH3OCOCH2OHH1+ + H2O				RTYPE=2 K1=2.48E-09 K2=-0.5 K3=0.0 # gg08_rnum=2201
H3O1+ + CH3OOH -> CH3OOH21+ + H2O						RTYPE=2 K1=1.40E-09 K2=-0.5 K3=0.0 # gg08_rnum=2202
H3O1+ + CH3OOCH3 -> CH3OH + CH3O1+ + H2O				RTYPE=2 K1=1.18E-09 K2=-0.5 K3=0.0 # gg08_rnum=2203
H3O1+ + CH3OCH2OH -> CH3OCH2OH21+ + H2O					RTYPE=2 K1=1.18E-09 K2=-0.5 K3=0.0 # gg08_rnum=2204
H3O1+ + CH2OHCHO -> CH2OHCHOH1+ + H2O					RTYPE=2 K1=2.19E-09 K2=-0.5 K3=0.0 # gg08_rnum=2205
H3O1+ + HOCH2CO -> CH2OHCHO1+ + H2O						RTYPE=2 K1=2.22E-09 K2=-0.5 K3=0.0 # gg08_rnum=2206
H3O1+ + HOCH2COCHO -> HOCH2COCHOH1+ + H2O				RTYPE=2 K1=7.87E-10 K2=-0.5 K3=0.0 # gg08_rnum=2207
H3O1+ + HNCOCH2OH -> NH2COCH2OH1+ + H2O					RTYPE=2 K1=1.60E-09 K2=-0.5 K3=0.0 # gg08_rnum=2208
H3O1+ + CH2OHCOOH -> CH2OHCOOH21+ + H2O					RTYPE=2 K1=1.42E-09 K2=-0.5 K3=0.0 # gg08_rnum=2209
H3O1+ + NH2COCH2OH -> NH2COCH2OH21+ + H2O				RTYPE=2 K1=1.58E-09 K2=-0.5 K3=0.0 # gg08_rnum=2210
H3O1+ + HOCH2COCH2OH -> HOCH2COCH2OH21+ + H2O			RTYPE=2 K1=1.40E-09 K2=-0.5 K3=0.0 # gg08_rnum=2211
H3O1+ + HOCH2OH -> HOCH2OH21+ + H2O						RTYPE=2 K1=2.47E-09 K2=-0.5 K3=0.0 # gg08_rnum=2212
H3O1+ + HOCH2CH2OH -> HOCH2CH2OH21+ + H2O				RTYPE=2 K1=6.13E-10 K2=0.0 K3=0.0 # gg08_rnum=2213
H3S1+ + H -> H2S1+ + H2									RTYPE=2 K1=6.00E-11 K2=0.0 K3=0.0 # gg08_rnum=2214
H3S1+ + H2CO -> CH2OH1+ + H2S							RTYPE=2 K1=2.20E-09 K2=0.0 K3=0.0 # gg08_rnum=2215
H3S1+ + HCN -> H2CN1+ + H2S								RTYPE=2 K1=1.50E-09 K2=0.0 K3=0.0 # gg08_rnum=2216
H3S1+ + HNC -> H2CN1+ + H2S								RTYPE=2 K1=1.50E-09 K2=0.0 K3=0.0 # gg08_rnum=2217
H3S1+ + NH3 -> NH41+ + H2S								RTYPE=2 K1=1.90E-09 K2=0.0 K3=0.0 # gg08_rnum=2218
HC4N1+ + H2 -> H2C4N1+ + H								RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=2219
ClH1+ + H2 -> ClH21+ + H								RTYPE=2 K1=1.30E-09 K2=0.0 K3=0.0 # gg08_rnum=2220
HCN1+ + C -> CH1+ + CN									RTYPE=2 K1=1.10E-09 K2=0.0 K3=0.0 # gg08_rnum=2221
HCN1+ + C2 -> C2H1+ + CN								RTYPE=2 K1=8.40E-10 K2=0.0 K3=0.0 # gg08_rnum=2222
HCN1+ + C2H -> HC2H1+ + CN								RTYPE=2 K1=7.90E-10 K2=0.0 K3=0.0 # gg08_rnum=2223
HCN1+ + CH -> CH21+ + CN								RTYPE=2 K1=6.30E-10 K2=0.0 K3=0.0 # gg08_rnum=2224
HCN1+ + CH2 -> CH31+ + CN								RTYPE=2 K1=8.70E-10 K2=0.0 K3=0.0 # gg08_rnum=2225
HCN1+ + CH4 -> H2CN1+ + CH3								RTYPE=2 K1=1.04E-09 K2=0.0 K3=0.0 # gg08_rnum=2226
HCN1+ + CH4 -> H2C2H1+ + NH2							RTYPE=2 K1=2.60E-10 K2=0.0 K3=0.0 # gg08_rnum=2227
HCN1+ + CO -> HCO1+ + CN								RTYPE=2 K1=1.40E-10 K2=0.0 K3=0.0 # gg08_rnum=2228
HCN1+ + CO2 -> HOCO1+ + CN								RTYPE=2 K1=2.10E-10 K2=0.0 K3=0.0 # gg08_rnum=2229
HCN1+ + H -> H1+ + HCN									RTYPE=2 K1=3.70E-11 K2=0.0 K3=0.0 # gg08_rnum=2230
HCN1+ + H2 -> H2CN1+ + H								RTYPE=2 K1=9.00E-10 K2=0.0 K3=0.0 # gg08_rnum=2231
HCN1+ + H2CO -> CH2OH1+ + CN							RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=2232
HCN1+ + H2O -> H2O1+ + HCN								RTYPE=2 K1=1.80E-09 K2=0.0 K3=0.0 # gg08_rnum=2233
HCN1+ + H2O -> H3O1+ + CN								RTYPE=2 K1=8.50E-10 K2=0.0 K3=0.0 # gg08_rnum=2234
HCN1+ + HCN -> H2CN1+ + CN								RTYPE=2 K1=1.60E-09 K2=0.0 K3=0.0 # gg08_rnum=2235
HCN1+ + HCO -> H2CN1+ + CO								RTYPE=2 K1=3.70E-10 K2=0.0 K3=0.0 # gg08_rnum=2236
HCN1+ + HCO -> H2CO1+ + CN								RTYPE=2 K1=3.70E-10 K2=0.0 K3=0.0 # gg08_rnum=2237
HCN1+ + HNC -> H2CN1+ + CN								RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=2238
HCN1+ + NH -> NH21+ + CN								RTYPE=2 K1=6.50E-10 K2=0.0 K3=0.0 # gg08_rnum=2239
HCN1+ + NH2 -> NH31+ + CN								RTYPE=2 K1=9.00E-10 K2=0.0 K3=0.0 # gg08_rnum=2240
HCN1+ + NH3 -> NH31+ + HCN								RTYPE=2 K1=1.70E-09 K2=0.0 K3=0.0 # gg08_rnum=2241
HCN1+ + NH3 -> H2CN1+ + NH2								RTYPE=2 K1=8.40E-10 K2=0.0 K3=0.0 # gg08_rnum=2242
HCN1+ + NO -> NO1+ + HCN								RTYPE=2 K1=8.10E-10 K2=0.0 K3=0.0 # gg08_rnum=2243
HCN1+ + O -> O1+ + HCN									RTYPE=2 K1=6.50E-11 K2=0.0 K3=0.0 # gg08_rnum=2244
HCN1+ + O2 -> O21+ + HCN								RTYPE=2 K1=3.20E-10 K2=0.0 K3=0.0 # gg08_rnum=2245
HCN1+ + OH -> H2O1+ + CN								RTYPE=2 K1=6.30E-10 K2=0.0 K3=0.0 # gg08_rnum=2246
HCN1+ + S -> HS1+ + CN									RTYPE=2 K1=5.70E-10 K2=0.0 K3=0.0 # gg08_rnum=2247
HCN1+ + S -> S1+ + HCN									RTYPE=2 K1=5.70E-10 K2=0.0 K3=0.0 # gg08_rnum=2248
HCO1+ + C -> CH1+ + CO									RTYPE=2 K1=1.10E-09 K2=0.0 K3=0.0 # gg08_rnum=2249
HCO1+ + C2 -> C2H1+ + CO								RTYPE=2 K1=8.30E-10 K2=0.0 K3=0.0 # gg08_rnum=2250
HCO1+ + C2H -> HC2H1+ + CO								RTYPE=2 K1=2.00E-09 K2=-0.5 K3=0.0 # gg08_rnum=2251
HCO1+ + HC2H -> H2C2H1+ + CO							RTYPE=2 K1=1.40E-09 K2=0.0 K3=0.0 # gg08_rnum=2252
HCO1+ + H2C2H -> H2C2H21+ + CO							RTYPE=2 K1=1.60E-09 K2=-0.5 K3=0.0 # gg08_rnum=2253
HCO1+ + H2C2H2 -> CH3CH21+ + CO							RTYPE=2 K1=1.40E-09 K2=0.0 K3=0.0 # gg08_rnum=2254
HCO1+ + CH3CH2 -> CH3CH31+ + CO							RTYPE=2 K1=1.02E-09 K2=-0.5 K3=0.0 # gg08_rnum=2255
HCO1+ + CH3CH2CHO -> CH3CH2CHOH1+ + CO					RTYPE=2 K1=2.47E-09 K2=-0.5 K3=0.0 # gg08_rnum=2256
HCO1+ + CH3CH2OH -> CH3CH2OH21+ + CO					RTYPE=2 K1=8.50E-10 K2=-0.5 K3=0.0 # gg08_rnum=2257
HCO1+ + CH3CH2OH -> H3O1+ + H2C2H2 + CO					RTYPE=2 K1=7.00E-10 K2=-0.5 K3=0.0 # gg08_rnum=2258
HCO1+ + C2O -> C2OH1+ + CO								RTYPE=2 K1=2.91E-09 K2=-0.5 K3=0.0 # gg08_rnum=2259
HCO1+ + C2S -> HC2S1+ + CO								RTYPE=2 K1=2.48E-09 K2=-0.5 K3=0.0 # gg08_rnum=2260
HCO1+ + C3 -> C3H1+ + CO								RTYPE=2 K1=1.40E-09 K2=0.0 K3=0.0 # gg08_rnum=2261
HCO1+ + C3H -> C3H21+ + CO								RTYPE=2 K1=7.10E-09 K2=-0.5 K3=0.0 # gg08_rnum=2262
HCO1+ + C3H2 -> C3H31+ + CO								RTYPE=2 K1=2.14E-09 K2=-0.5 K3=0.0 # gg08_rnum=2263
HCO1+ + C3H3 -> C3H41+ + CO								RTYPE=2 K1=3.80E-09 K2=-0.5 K3=0.0 # gg08_rnum=2264
HCO1+ + C3H3N -> C3H4N1+ + CO							RTYPE=2 K1=3.50E-09 K2=-0.5 K3=0.0 # gg08_rnum=2265
HCO1+ + C3H4 -> C3H51+ + CO								RTYPE=2 K1=7.40E-10 K2=-0.5 K3=0.0 # gg08_rnum=2266
HCO1+ + C3N -> NC3H1+ + CO								RTYPE=2 K1=7.06E-09 K2=-0.5 K3=0.0 # gg08_rnum=2267
HCO1+ + C3O -> HC3O1+ + CO								RTYPE=2 K1=5.08E-09 K2=-0.5 K3=0.0 # gg08_rnum=2268
HCO1+ + C3P -> PC3H1+ + CO								RTYPE=2 K1=8.62E-10 K2=-0.5 K3=0.0 # gg08_rnum=2269
HCO1+ + C3S -> HC3S1+ + CO								RTYPE=2 K1=3.35E-09 K2=-0.5 K3=0.0 # gg08_rnum=2270
HCO1+ + C4 -> C4H1+ + CO								RTYPE=2 K1=1.40E-09 K2=0.0 K3=0.0 # gg08_rnum=2271
HCO1+ + C4H -> HC4H1+ + CO								RTYPE=2 K1=1.90E-09 K2=-0.5 K3=0.0 # gg08_rnum=2272
HCO1+ + HC4H -> C4H31+ + CO								RTYPE=2 K1=1.40E-09 K2=0.0 K3=0.0 # gg08_rnum=2273
HCO1+ + C4H3 -> C4H41+ + CO								RTYPE=2 K1=9.00E-10 K2=-0.5 K3=0.0 # gg08_rnum=2274
HCO1+ + C4P -> PC4H1+ + CO								RTYPE=2 K1=4.21E-10 K2=-0.5 K3=0.0 # gg08_rnum=2275
HCO1+ + C4S -> HC4S1+ + CO								RTYPE=2 K1=2.52E-09 K2=-0.5 K3=0.0 # gg08_rnum=2276
HCO1+ + C5 -> C5H1+ + CO								RTYPE=2 K1=1.40E-09 K2=0.0 K3=0.0 # gg08_rnum=2277
HCO1+ + C5H -> C5H21+ + CO								RTYPE=2 K1=8.90E-09 K2=-0.5 K3=0.0 # gg08_rnum=2278
HCO1+ + C5H2 -> C5H31+ + CO								RTYPE=2 K1=2.18E-09 K2=-0.5 K3=0.0 # gg08_rnum=2279
HCO1+ + C5N -> C5HN1+ + CO								RTYPE=2 K1=6.50E-09 K2=-0.5 K3=0.0 # gg08_rnum=2280
HCO1+ + C6 -> C6H1+ + CO								RTYPE=2 K1=1.40E-09 K2=0.0 K3=0.0 # gg08_rnum=2281
HCO1+ + C6H -> C6H21+ + CO								RTYPE=2 K1=1.01E-08 K2=-0.5 K3=0.0 # gg08_rnum=2282
HCO1+ + C6H2 -> C6H31+ + CO								RTYPE=2 K1=1.40E-09 K2=0.0 K3=0.0 # gg08_rnum=2283
HCO1+ + C6H6 -> C6H71+ + CO								RTYPE=2 K1=1.60E-09 K2=0.0 K3=0.0 # gg08_rnum=2284
HCO1+ + C7 -> C7H1+ + CO								RTYPE=2 K1=1.40E-09 K2=0.0 K3=0.0 # gg08_rnum=2285
HCO1+ + C7H -> C7H21+ + CO								RTYPE=2 K1=8.88E-09 K2=-0.5 K3=0.0 # gg08_rnum=2286
HCO1+ + C7H2 -> C7H31+ + CO								RTYPE=2 K1=2.08E-09 K2=-0.5 K3=0.0 # gg08_rnum=2287
HCO1+ + C7N -> C7HN1+ + CO								RTYPE=2 K1=5.82E-09 K2=-0.5 K3=0.0 # gg08_rnum=2288
HCO1+ + C8 -> C8H1+ + CO								RTYPE=2 K1=1.40E-09 K2=0.0 K3=0.0 # gg08_rnum=2289
HCO1+ + C8H -> C8H21+ + CO								RTYPE=2 K1=9.71E-09 K2=-0.5 K3=0.0 # gg08_rnum=2290
HCO1+ + C8H2 -> C8H31+ + CO								RTYPE=2 K1=1.40E-09 K2=0.0 K3=0.0 # gg08_rnum=2291
HCO1+ + C9 -> C9H1+ + CO								RTYPE=2 K1=1.40E-09 K2=0.0 K3=0.0 # gg08_rnum=2292
HCO1+ + C9H -> C9H21+ + CO								RTYPE=2 K1=9.01E-09 K2=-0.5 K3=0.0 # gg08_rnum=2293
HCO1+ + C9H2 -> C9H31+ + CO								RTYPE=2 K1=2.02E-09 K2=-0.5 K3=0.0 # gg08_rnum=2294
HCO1+ + C9N -> C9HN1+ + CO								RTYPE=2 K1=6.26E-09 K2=-0.5 K3=0.0 # gg08_rnum=2295
HCO1+ + CCP -> HCCP1+ + CO								RTYPE=2 K1=2.11E-09 K2=-0.5 K3=0.0 # gg08_rnum=2296
HCO1+ + CH -> CH21+ + CO								RTYPE=2 K1=4.50E-09 K2=-0.5 K3=0.0 # gg08_rnum=2297
HCO1+ + CH2 -> CH31+ + CO								RTYPE=2 K1=7.19E-10 K2=-0.5 K3=0.0 # gg08_rnum=2298
HCO1+ + CH2CO -> CH3CO1+ + CO							RTYPE=2 K1=1.30E-09 K2=-0.5 K3=0.0 # gg08_rnum=2299
HCO1+ + CH2NH -> CH3NH1+ + CO							RTYPE=2 K1=2.00E-09 K2=-0.5 K3=0.0 # gg08_rnum=2300
HCO1+ + CH2OH -> CH3OH1+ + CO							RTYPE=2 K1=1.15E-09 K2=-0.5 K3=0.0 # gg08_rnum=2301
HCO1+ + CH2PH -> CH2PH21+ + CO							RTYPE=2 K1=4.60E-10 K2=-0.5 K3=0.0 # gg08_rnum=2302
HCO1+ + CH3C3N -> C4H4N1+ + CO							RTYPE=2 K1=4.11E-09 K2=-0.5 K3=0.0 # gg08_rnum=2303
HCO1+ + CH3C4H -> C5H51+ + CO							RTYPE=2 K1=1.05E-09 K2=-0.5 K3=0.0 # gg08_rnum=2304
HCO1+ + CH3C5N -> C6H4N1+ + CO							RTYPE=2 K1=4.34E-09 K2=-0.5 K3=0.0 # gg08_rnum=2305
HCO1+ + CH3C6H -> C7H51+ + CO							RTYPE=2 K1=1.25E-09 K2=-0.5 K3=0.0 # gg08_rnum=2306
HCO1+ + CH3C7N -> C8H4N1+ + CO							RTYPE=2 K1=4.42E-09 K2=-0.5 K3=0.0 # gg08_rnum=2307
HCO1+ + CH3CHO -> CH3CH2O1+ + CO						RTYPE=2 K1=2.50E-09 K2=-0.5 K3=0.0 # gg08_rnum=2308
HCO1+ + CH3CN -> CH3CNH1+ + CO							RTYPE=2 K1=3.70E-09 K2=-0.5 K3=0.0 # gg08_rnum=2309
HCO1+ + CH3CO -> CH3CHO1+ + CO							RTYPE=2 K1=1.67E-09 K2=-0.5 K3=0.0 # gg08_rnum=2310
HCO1+ + CH3NH -> CH3NH21+ + CO							RTYPE=2 K1=1.17E-09 K2=-0.5 K3=0.0 # gg08_rnum=2311
HCO1+ + CH3NH2 -> CH3NH31+ + CO							RTYPE=2 K1=1.30E-09 K2=-0.5 K3=0.0 # gg08_rnum=2312
HCO1+ + CH3O -> CH3OH1+ + CO							RTYPE=2 K1=1.76E-10 K2=-0.5 K3=0.0 # gg08_rnum=2313
HCO1+ + CH3OCH3 -> CH3OCH41+ + CO						RTYPE=2 K1=1.20E-09 K2=-0.5 K3=0.0 # gg08_rnum=2314
HCO1+ + CH3OH -> CH3OH21+ + CO							RTYPE=2 K1=1.70E-09 K2=-0.5 K3=0.0 # gg08_rnum=2315
HCO1+ + COCHO -> CHOCHO1+ + CO							RTYPE=2 K1=7.54E-10 K2=-0.5 K3=0.0 # gg08_rnum=2316
HCO1+ + COOH -> HCOOH1+ + CO							RTYPE=2 K1=2.71E-09 K2=-0.5 K3=0.0 # gg08_rnum=2317
HCO1+ + CP -> HCP1+ + CO								RTYPE=2 K1=1.90E-09 K2=-0.5 K3=0.0 # gg08_rnum=2318
HCO1+ + CS -> HCS1+ + CO								RTYPE=2 K1=4.30E-09 K2=-0.5 K3=0.0 # gg08_rnum=2319
HCO1+ + Fe -> Fe1+ + HCO								RTYPE=2 K1=1.90E-09 K2=0.0 K3=0.0 # gg08_rnum=2320
HCO1+ + H2CO -> CH2OH1+ + CO							RTYPE=2 K1=2.40E-09 K2=-0.5 K3=0.0 # gg08_rnum=2321
HCO1+ + H2CS -> H3CS1+ + CO								RTYPE=2 K1=1.50E-09 K2=-0.5 K3=0.0 # gg08_rnum=2322
HCO1+ + H2O -> H3O1+ + CO								RTYPE=2 K1=2.10E-09 K2=-0.5 K3=0.0 # gg08_rnum=2323
HCO1+ + H2S -> H3S1+ + CO								RTYPE=2 K1=9.50E-10 K2=-0.5 K3=0.0 # gg08_rnum=2324
HCO1+ + H2S2 -> H3S21+ + CO								RTYPE=2 K1=1.04E-09 K2=-0.5 K3=0.0 # gg08_rnum=2325
HCO1+ + H2SiO -> H3SiO1+ + CO							RTYPE=2 K1=1.84E-09 K2=-0.5 K3=0.0 # gg08_rnum=2326
HCO1+ + HC2NC -> HC2NCH1+ + CO							RTYPE=2 K1=5.00E-09 K2=-0.5 K3=0.0 # gg08_rnum=2327
HCO1+ + HC3N -> HC3NH1+ + CO							RTYPE=2 K1=7.90E-09 K2=-0.5 K3=0.0 # gg08_rnum=2328
HCO1+ + HC5N -> C5H2N1+ + CO							RTYPE=2 K1=8.70E-09 K2=-0.5 K3=0.0 # gg08_rnum=2329
HCO1+ + HC7N -> C7H2N1+ + CO							RTYPE=2 K1=8.95E-09 K2=-0.5 K3=0.0 # gg08_rnum=2330
HCO1+ + HC9N -> C9H2N1+ + CO							RTYPE=2 K1=9.17E-09 K2=-0.5 K3=0.0 # gg08_rnum=2331
HCO1+ + HCCP -> PC2H21+ + CO							RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=2332
HCO1+ + HCN -> H2CN1+ + CO								RTYPE=2 K1=7.30E-09 K2=-0.5 K3=0.0 # gg08_rnum=2333
HCO1+ + C2NCH -> HC2NCH1+ + CO							RTYPE=2 K1=1.75E-08 K2=-0.5 K3=0.0 # gg08_rnum=2334
HCO1+ + HCO -> H2CO1+ + CO								RTYPE=2 K1=1.00E-09 K2=-0.5 K3=0.0 # gg08_rnum=2335
HCO1+ + HCOOCH3 -> HCOOCH3H1+ + CO						RTYPE=2 K1=1.55E-09 K2=-0.5 K3=0.0 # gg08_rnum=2336
HCO1+ + HCOOH -> HCOOH21+ + CO							RTYPE=2 K1=1.30E-09 K2=-0.5 K3=0.0 # gg08_rnum=2337
HCO1+ + HCP -> PCH21+ + CO								RTYPE=2 K1=8.56E-10 K2=-0.5 K3=0.0 # gg08_rnum=2338
HCO1+ + SiCH -> SiCH21+ + CO							RTYPE=2 K1=2.23E-09 K2=-0.5 K3=0.0 # gg08_rnum=2339
HCO1+ + CH3COCHO -> CH3COCHOH1+ + CO					RTYPE=2 K1=2.77E-09 K2=-0.5 K3=0.0 # gg08_rnum=2340
HCO1+ + CH3COCH3 -> CH3COCH41+ + CO						RTYPE=2 K1=1.95E-09 K2=-0.5 K3=0.0 # gg08_rnum=2341
HCO1+ + CH3CONH -> CH3CONH21+ + CO						RTYPE=2 K1=2.61E-09 K2=-0.5 K3=0.0 # gg08_rnum=2342
HCO1+ + CH3COOH -> CH3COOH21+ + CO						RTYPE=2 K1=1.16E-09 K2=-0.5 K3=0.0 # gg08_rnum=2343
HCO1+ + CH3CONH2 -> CH3CONH2H1+ + CO					RTYPE=2 K1=2.57E-09 K2=-0.5 K3=0.0 # gg08_rnum=2344
HCO1+ + CH3COOCH3 -> CH3COOCH3H1+ + CO					RTYPE=2 K1=1.16E-09 K2=-0.5 K3=0.0 # gg08_rnum=2345
HCO1+ + CH3COCH2OH -> CH3COCH2OHH1+ + CO				RTYPE=2 K1=2.01E-09 K2=-0.5 K3=0.0 # gg08_rnum=2346
HCO1+ + HNC -> H2CN1+ + CO								RTYPE=2 K1=6.63E-09 K2=-0.5 K3=0.0 # gg08_rnum=2347
HCO1+ + HNC3 -> HC3NH1+ + CO							RTYPE=2 K1=4.50E-09 K2=-0.5 K3=0.0 # gg08_rnum=2348
HCO1+ + HNCHO -> NH2CHO1+ + CO							RTYPE=2 K1=1.50E-09 K2=-0.5 K3=0.0 # gg08_rnum=2349
HCO1+ + HNCO -> HNCHO1+ + CO							RTYPE=2 K1=1.59E-09 K2=-0.5 K3=0.0 # gg08_rnum=2350
HCO1+ + HNCOCHO -> NH2COCHO1+ + CO						RTYPE=2 K1=6.93E-10 K2=-0.5 K3=0.0 # gg08_rnum=2351
HCO1+ + HNCONH -> NH2CONH1+ + CO						RTYPE=2 K1=2.78E-09 K2=-0.5 K3=0.0 # gg08_rnum=2352
HCO1+ + HNCOOH -> NH2COOH1+ + CO						RTYPE=2 K1=1.41E-09 K2=-0.5 K3=0.0 # gg08_rnum=2353
HCO1+ + HNO -> H2NO1+ + CO								RTYPE=2 K1=1.70E-09 K2=-0.5 K3=0.0 # gg08_rnum=2354
HCO1+ + HNOH -> NH2OH1+ + CO							RTYPE=2 K1=1.56E-09 K2=-0.5 K3=0.0 # gg08_rnum=2355
HCO1+ + HNSi -> SiNH21+ + CO							RTYPE=2 K1=3.53E-10 K2=-0.5 K3=0.0 # gg08_rnum=2356
HCO1+ + CH3ONH -> NH2OCH31+ + CO						RTYPE=2 K1=1.06E-09 K2=-0.5 K3=0.0 # gg08_rnum=2357
HCO1+ + HNCH2OH -> NH2CH2OH1+ + CO						RTYPE=2 K1=1.06E-09 K2=-0.5 K3=0.0 # gg08_rnum=2358
HCO1+ + HOCOOH -> HOCOOH21+ + CO						RTYPE=2 K1=1.38E-09 K2=-0.5 K3=0.0 # gg08_rnum=2359
HCO1+ + HPO -> H2PO1+ + CO								RTYPE=2 K1=5.03E-09 K2=-0.5 K3=0.0 # gg08_rnum=2360
HCO1+ + HS -> H2S1+ + CO								RTYPE=2 K1=1.80E-09 K2=-0.5 K3=0.0 # gg08_rnum=2361
HCO1+ + S2H -> H2S21+ + CO								RTYPE=2 K1=1.73E-09 K2=-0.5 K3=0.0 # gg08_rnum=2362
HCO1+ + HCOCOCHO -> HCOCOCHOH1+ + CO					RTYPE=2 K1=1.35E-09 K2=-0.5 K3=0.0 # gg08_rnum=2363
HCO1+ + Mg -> Mg1+ + HCO								RTYPE=2 K1=2.90E-09 K2=0.0 K3=0.0 # gg08_rnum=2364
HCO1+ + MgH -> Mg1+ + CO + H2							RTYPE=2 K1=3.20E-09 K2=-0.5 K3=0.0 # gg08_rnum=2365
HCO1+ + Na -> Na1+ + HCO								RTYPE=2 K1=2.60E-09 K2=0.0 K3=0.0 # gg08_rnum=2366
HCO1+ + NaH -> Na1+ + CO + H2							RTYPE=2 K1=1.70E-08 K2=-0.5 K3=0.0 # gg08_rnum=2367
HCO1+ + NaOH -> NaH2O1+ + CO							RTYPE=2 K1=4.70E-09 K2=-0.5 K3=0.0 # gg08_rnum=2368
HCO1+ + NH -> NH21+ + CO								RTYPE=2 K1=3.80E-09 K2=-0.5 K3=0.0 # gg08_rnum=2369
HCO1+ + NH2 -> NH31+ + CO								RTYPE=2 K1=2.20E-09 K2=-0.5 K3=0.0 # gg08_rnum=2370
HCO1+ + NH2CHO -> NH2CHOH1+ + CO						RTYPE=2 K1=8.15E-09 K2=-0.5 K3=0.0 # gg08_rnum=2371
HCO1+ + NH2CN -> NH2CNH1+ + CO							RTYPE=2 K1=4.00E-09 K2=-0.5 K3=0.0 # gg08_rnum=2372
HCO1+ + NH2OH -> NH3OH1+ + CO							RTYPE=2 K1=4.45E-10 K2=-0.5 K3=0.0 # gg08_rnum=2373
HCO1+ + NH3 -> NH41+ + CO								RTYPE=2 K1=1.70E-09 K2=-0.5 K3=0.0 # gg08_rnum=2374
HCO1+ + NS -> HNS1+ + CO								RTYPE=2 K1=3.90E-09 K2=-0.5 K3=0.0 # gg08_rnum=2375
HCO1+ + OCS -> HOCS1+ + CO								RTYPE=2 K1=1.50E-09 K2=-0.5 K3=0.0 # gg08_rnum=2376
HCO1+ + OH -> H2O1+ + CO								RTYPE=2 K1=2.33E-09 K2=-0.5 K3=0.0 # gg08_rnum=2377
HCO1+ + OH -> HOCO1+ + H								RTYPE=2 K1=2.33E-09 K2=-0.5 K3=0.0 # gg08_rnum=2378
HCO1+ + CHOCHO -> CHOCHOH1+ + CO						RTYPE=2 K1=5.02E-10 K2=0.0 K3=0.0 # gg08_rnum=2379
HCO1+ + HCOCOOH -> HCOCOOH21+ + CO						RTYPE=2 K1=6.82E-10 K2=-0.5 K3=0.0 # gg08_rnum=2380
HCO1+ + P -> PH1+ + CO									RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=2381
HCO1+ + PH -> PH21+ + CO								RTYPE=2 K1=1.51E-09 K2=-0.5 K3=0.0 # gg08_rnum=2382
HCO1+ + PH2 -> PH31+ + CO								RTYPE=2 K1=4.94E-10 K2=-0.5 K3=0.0 # gg08_rnum=2383
HCO1+ + PN -> HPN1+ + CO								RTYPE=2 K1=6.01E-09 K2=-0.5 K3=0.0 # gg08_rnum=2384
HCO1+ + PO -> HPO1+ + CO								RTYPE=2 K1=4.08E-09 K2=-0.5 K3=0.0 # gg08_rnum=2385
HCO1+ + NH2CO -> NH2CHO1+ + CO							RTYPE=2 K1=1.50E-09 K2=-0.5 K3=0.0 # gg08_rnum=2386
HCO1+ + NH2COCHO -> NH2COCHOH1+ + CO					RTYPE=2 K1=6.75E-10 K2=-0.5 K3=0.0 # gg08_rnum=2387
HCO1+ + NH2CONH -> NH2CONH21+ + CO						RTYPE=2 K1=2.70E-09 K2=-0.5 K3=0.0 # gg08_rnum=2388
HCO1+ + NH2COOH -> NH2COOH21+ + CO						RTYPE=2 K1=1.36E-09 K2=-0.5 K3=0.0 # gg08_rnum=2389
HCO1+ + NH2CONH2 -> NH2CONH2H1+ + CO					RTYPE=2 K1=2.65E-09 K2=-0.5 K3=0.0 # gg08_rnum=2390
HCO1+ + NH2NH -> NH2NH21+ + CO							RTYPE=2 K1=1.53E-09 K2=-0.5 K3=0.0 # gg08_rnum=2391
HCO1+ + NH2NH2 -> NH2NH2H1+ + CO						RTYPE=2 K1=1.53E-09 K2=-0.5 K3=0.0 # gg08_rnum=2392
HCO1+ + NH2OCH3 -> HNH2OCH31+ + CO						RTYPE=2 K1=1.38E-09 K2=-0.5 K3=0.0 # gg08_rnum=2393
HCO1+ + NH2CH2OH -> HNH2CH2OH1+ + CO					RTYPE=2 K1=1.04E-09 K2=-0.5 K3=0.0 # gg08_rnum=2394
HCO1+ + S -> HS1+ + CO									RTYPE=2 K1=3.30E-10 K2=0.0 K3=0.0 # gg08_rnum=2395
HCO1+ + S2 -> S2H1+ + CO								RTYPE=2 K1=2.00E-09 K2=0.0 K3=0.0 # gg08_rnum=2396
HCO1+ + Si -> SiH1+ + CO								RTYPE=2 K1=1.60E-09 K2=0.0 K3=0.0 # gg08_rnum=2397
HCO1+ + SiC -> SiCH1+ + CO								RTYPE=2 K1=3.80E-09 K2=-0.5 K3=0.0 # gg08_rnum=2398
HCO1+ + SiC2 -> SiC2H1+ + CO							RTYPE=2 K1=2.15E-09 K2=-0.5 K3=0.0 # gg08_rnum=2399
HCO1+ + SiC2H -> SiC2H21+ + CO							RTYPE=2 K1=1.25E-09 K2=-0.5 K3=0.0 # gg08_rnum=2400
HCO1+ + SiC2H2 -> SiC2H31+ + CO							RTYPE=2 K1=2.23E-09 K2=-0.5 K3=0.0 # gg08_rnum=2401
HCO1+ + cSiC3 -> SiC3H1+ + CO							RTYPE=2 K1=1.74E-09 K2=-0.5 K3=0.0 # gg08_rnum=2402
HCO1+ + SiC3H -> SiC3H21+ + CO							RTYPE=2 K1=2.60E-09 K2=-0.5 K3=0.0 # gg08_rnum=2403
HCO1+ + SiC4 -> SiC4H1+ + CO							RTYPE=2 K1=5.33E-09 K2=-0.5 K3=0.0 # gg08_rnum=2404
HCO1+ + SiCH2 -> SiCH31+ + CO							RTYPE=2 K1=1.40E-09 K2=-0.5 K3=0.0 # gg08_rnum=2405
HCO1+ + SiCH3 -> SiCH41+ + CO							RTYPE=2 K1=1.40E-09 K2=-0.5 K3=0.0 # gg08_rnum=2406
HCO1+ + SiH -> SiH21+ + CO								RTYPE=2 K1=2.89E-10 K2=-0.5 K3=0.0 # gg08_rnum=2407
HCO1+ + SiH2 -> SiH31+ + CO								RTYPE=2 K1=1.82E-10 K2=-0.5 K3=0.0 # gg08_rnum=2408
HCO1+ + SiH4 -> SiH51+ + CO								RTYPE=2 K1=1.40E-09 K2=0.0 K3=0.0 # gg08_rnum=2409
HCO1+ + SiNC -> SiNCH1+ + CO							RTYPE=2 K1=1.81E-09 K2=-0.5 K3=0.0 # gg08_rnum=2410
HCO1+ + SiO -> HSiO1+ + CO								RTYPE=2 K1=6.80E-09 K2=-0.5 K3=0.0 # gg08_rnum=2411
HCO1+ + SiO2 -> HSiO21+ + CO							RTYPE=2 K1=4.38E-10 K2=-0.5 K3=0.0 # gg08_rnum=2412
HCO1+ + SiS -> HSiS1+ + CO								RTYPE=2 K1=3.60E-09 K2=-0.5 K3=0.0 # gg08_rnum=2413
HCO1+ + SO -> HSO1+ + CO								RTYPE=2 K1=3.30E-09 K2=-0.5 K3=0.0 # gg08_rnum=2414
HCO1+ + CH3OCO -> HCOOCH31+ + CO						RTYPE=2 K1=1.21E-09 K2=-0.5 K3=0.0 # gg08_rnum=2415
HCO1+ + CH3OCOCHO -> CH3OCOCHOH1+ + CO					RTYPE=2 K1=6.50E-10 K2=-0.5 K3=0.0 # gg08_rnum=2416
HCO1+ + CH3OCONH -> CH3OCONH21+ + CO					RTYPE=2 K1=1.33E-09 K2=-0.5 K3=0.0 # gg08_rnum=2417
HCO1+ + CH3OCOOH -> CH3OCOOH21+ + CO					RTYPE=2 K1=1.30E-09 K2=-0.5 K3=0.0 # gg08_rnum=2418
HCO1+ + CH3OCONH2 -> CH3OCONH2H1+ + CO					RTYPE=2 K1=1.31E-09 K2=-0.5 K3=0.0 # gg08_rnum=2419
HCO1+ + CH3OCOOCH3 -> CH3OCOOCH3H1+ + CO				RTYPE=2 K1=1.15E-10 K2=-0.5 K3=0.0 # gg08_rnum=2420
HCO1+ + CH3OCOCH2OH -> CH3OCOCH2OHH1+ + CO				RTYPE=2 K1=2.04E-09 K2=-0.5 K3=0.0 # gg08_rnum=2421
HCO1+ + CH3OOH -> CH3OOH21+ + CO						RTYPE=2 K1=1.17E-09 K2=-0.5 K3=0.0 # gg08_rnum=2422
HCO1+ + CH3OOCH3 -> CH3OH + CH3O1+ + CO					RTYPE=2 K1=9.78E-10 K2=-0.5 K3=0.0 # gg08_rnum=2423
HCO1+ + CH3OCH2OH -> CH3OCH2OH21+ + CO					RTYPE=2 K1=9.78E-10 K2=-0.5 K3=0.0 # gg08_rnum=2424
HCO1+ + CH2OHCHO -> CH2OHCHOH1+ + CO					RTYPE=2 K1=1.82E-09 K2=-0.5 K3=0.0 # gg08_rnum=2425
HCO1+ + HOCH2CO -> CH2OHCHO1+ + CO						RTYPE=2 K1=1.86E-09 K2=-0.5 K3=0.0 # gg08_rnum=2426
HCO1+ + HOCH2COCHO -> HOCH2COCHOH1+ + CO				RTYPE=2 K1=6.50E-10 K2=-0.5 K3=0.0 # gg08_rnum=2427
HCO1+ + HNCOCH2OH -> NH2COCH2OH1+ + CO					RTYPE=2 K1=1.33E-09 K2=-0.5 K3=0.0 # gg08_rnum=2428
HCO1+ + CH2OHCOOH -> CH2OHCOOH21+ + CO					RTYPE=2 K1=1.17E-09 K2=-0.5 K3=0.0 # gg08_rnum=2429
HCO1+ + NH2COCH2OH -> NH2COCH2OH21+ + CO				RTYPE=2 K1=1.31E-09 K2=-0.5 K3=0.0 # gg08_rnum=2430
HCO1+ + HOCH2COCH2OH -> HOCH2COCH2OH21+ + CO			RTYPE=2 K1=1.15E-09 K2=-0.5 K3=0.0 # gg08_rnum=2431
HCO1+ + HOCH2OH -> HOCH2OH21+ + CO						RTYPE=2 K1=2.06E-09 K2=-0.5 K3=0.0 # gg08_rnum=2432
HCO1+ + HOCH2CH2OH -> HOCH2CH2OH21+ + CO				RTYPE=2 K1=5.07E-10 K2=0.0 K3=0.0 # gg08_rnum=2433
HOCO1+ + C -> CH1+ + CO2								RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=2434
HOCO1+ + HC2H -> H2C2H1+ + CO2							RTYPE=2 K1=1.37E-09 K2=0.0 K3=0.0 # gg08_rnum=2435
HOCO1+ + CH3CN -> CH3CNH1+ + CO2						RTYPE=2 K1=3.28E-09 K2=-0.5 K3=0.0 # gg08_rnum=2436
HOCO1+ + CH4 -> CH51+ + CO2								RTYPE=2 K1=7.80E-10 K2=0.0 K3=0.0 # gg08_rnum=2437
HOCO1+ + CO -> HCO1+ + CO2								RTYPE=2 K1=2.47E-10 K2=-0.5 K3=0.0 # gg08_rnum=2438
HOCO1+ + H2O -> H3O1+ + CO2								RTYPE=2 K1=2.00E-09 K2=-0.5 K3=0.0 # gg08_rnum=2439
HOCO1+ + NH3 -> NH41+ + CO2								RTYPE=2 K1=1.62E-09 K2=-0.5 K3=0.0 # gg08_rnum=2440
HOCO1+ + O -> HCO1+ + O2								RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=2441
HCP1+ + C -> CCP1+ + H									RTYPE=2 K1=2.00E-10 K2=0.0 K3=0.0 # gg08_rnum=2442
HCP1+ + H2 -> PCH21+ + H								RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=2443
HCP1+ + O -> PH1+ + CO									RTYPE=2 K1=2.00E-10 K2=0.0 K3=0.0 # gg08_rnum=2444
HCS1+ + NH3 -> NH41+ + CS								RTYPE=2 K1=1.62E-09 K2=-0.5 K3=0.0 # gg08_rnum=2445
HCS1+ + O -> HCO1+ + S									RTYPE=2 K1=5.00E-10 K2=0.0 K3=0.0 # gg08_rnum=2446
HCS1+ + O -> OCS1+ + H									RTYPE=2 K1=5.00E-10 K2=0.0 K3=0.0 # gg08_rnum=2447
He1+ + C2 -> C1+ + C + He								RTYPE=2 K1=1.60E-09 K2=0.0 K3=0.0 # gg08_rnum=2448
He1+ + C2 -> C21+ + He									RTYPE=2 K1=5.00E-10 K2=0.0 K3=0.0 # gg08_rnum=2449
He1+ + C2H -> C1+ + CH + He								RTYPE=2 K1=1.30E-09 K2=-0.5 K3=0.0 # gg08_rnum=2450
He1+ + C2H -> C21+ + H + He								RTYPE=2 K1=1.30E-09 K2=-0.5 K3=0.0 # gg08_rnum=2451
He1+ + C2H -> CH1+ + C + He								RTYPE=2 K1=1.30E-09 K2=-0.5 K3=0.0 # gg08_rnum=2452
He1+ + HC2H -> C21+ + H2 + He							RTYPE=2 K1=1.60E-09 K2=0.0 K3=0.0 # gg08_rnum=2453
He1+ + HC2H -> CH1+ + CH + He							RTYPE=2 K1=7.70E-10 K2=0.0 K3=0.0 # gg08_rnum=2454
He1+ + HC2H -> C2H1+ + H + He							RTYPE=2 K1=8.80E-10 K2=0.0 K3=0.0 # gg08_rnum=2455
He1+ + HC2H -> HC2H1+ + He								RTYPE=2 K1=2.50E-10 K2=0.0 K3=0.0 # gg08_rnum=2456
He1+ + H2C2H -> C2H1+ + H2 + He							RTYPE=2 K1=1.60E-09 K2=-0.5 K3=0.0 # gg08_rnum=2457
He1+ + H2C2H -> HC2H1+ + H + He							RTYPE=2 K1=1.60E-09 K2=-0.5 K3=0.0 # gg08_rnum=2458
He1+ + H2C2H2 -> C2H1+ + H + H2 + He					RTYPE=2 K1=4.40E-10 K2=0.0 K3=0.0 # gg08_rnum=2459
He1+ + H2C2H2 -> CH21+ + CH2 + He						RTYPE=2 K1=4.80E-10 K2=0.0 K3=0.0 # gg08_rnum=2460
He1+ + H2C2H2 -> HC2H1+ + H2 + He						RTYPE=2 K1=2.20E-09 K2=0.0 K3=0.0 # gg08_rnum=2461
He1+ + H2C2H2 -> H2C2H1+ + H + He						RTYPE=2 K1=1.70E-10 K2=0.0 K3=0.0 # gg08_rnum=2462
He1+ + H2C2H2 -> H2C2H21+ + He							RTYPE=2 K1=2.40E-10 K2=0.0 K3=0.0 # gg08_rnum=2463
He1+ + CH3CH2 -> H2C2H1+ + H2 + He						RTYPE=2 K1=6.90E-10 K2=-0.5 K3=0.0 # gg08_rnum=2464
He1+ + CH3CH2 -> H2C2H21+ + H + He						RTYPE=2 K1=6.90E-10 K2=-0.5 K3=0.0 # gg08_rnum=2465
He1+ + CH3CH2 -> CH3CH21+ + He							RTYPE=2 K1=6.90E-10 K2=-0.5 K3=0.0 # gg08_rnum=2466
He1+ + CH3CH2CHO -> CH3CH21+ + HCO + He					RTYPE=2 K1=2.80E-09 K2=-0.5 K3=0.0 # gg08_rnum=2467
He1+ + CH3CH2CHO -> HCO1+ + CH3CH2 + He					RTYPE=2 K1=2.80E-09 K2=-0.5 K3=0.0 # gg08_rnum=2468
He1+ + CH3CH2OH -> CH3CH2O1+ + H + He					RTYPE=2 K1=1.13E-09 K2=-0.5 K3=0.0 # gg08_rnum=2469
He1+ + CH3CH2OH -> CH3CH21+ + OH + He					RTYPE=2 K1=1.13E-09 K2=-0.5 K3=0.0 # gg08_rnum=2470
He1+ + CH3CH2OH -> OH1+ + CH3CH2 + He					RTYPE=2 K1=1.13E-09 K2=-0.5 K3=0.0 # gg08_rnum=2471
He1+ + CH3CH3 -> H2C2H21+ + H2 + He						RTYPE=2 K1=4.20E-10 K2=0.0 K3=0.0 # gg08_rnum=2472
He1+ + CH3CH3 -> H2C2H1+ + H2 + H + He					RTYPE=2 K1=1.74E-09 K2=0.0 K3=0.0 # gg08_rnum=2473
He1+ + CH3CH3 -> HC2H1+ + H2 + H2 + He					RTYPE=2 K1=8.40E-10 K2=0.0 K3=0.0 # gg08_rnum=2474
He1+ + C2N -> C1+ + CN + He								RTYPE=2 K1=2.89E-09 K2=-0.5 K3=0.0 # gg08_rnum=2475
He1+ + C2O -> C1+ + CO + He								RTYPE=2 K1=6.26E-09 K2=-0.5 K3=0.0 # gg08_rnum=2476
He1+ + C2S -> CS1+ + C + He								RTYPE=2 K1=1.40E-09 K2=-0.5 K3=0.0 # gg08_rnum=2477
He1+ + C2S -> C1+ + CS + He								RTYPE=2 K1=1.40E-09 K2=-0.5 K3=0.0 # gg08_rnum=2478
He1+ + C2S -> S1+ + C2 + He								RTYPE=2 K1=1.40E-09 K2=-0.5 K3=0.0 # gg08_rnum=2479
He1+ + C2S -> C21+ + S + He								RTYPE=2 K1=1.40E-09 K2=-0.5 K3=0.0 # gg08_rnum=2480
He1+ + C3 -> C21+ + C + He								RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=2481
He1+ + C3 -> C1+ + C2 + He								RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=2482
He1+ + C3H -> C31+ + H + He								RTYPE=2 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=2483
He1+ + C3H2 -> C31+ + H2 + He							RTYPE=2 K1=3.40E-09 K2=-0.5 K3=0.0 # gg08_rnum=2484
He1+ + C3H2 -> C3H1+ + H + He							RTYPE=2 K1=3.40E-09 K2=-0.5 K3=0.0 # gg08_rnum=2485
He1+ + C3H3 -> C3H1+ + H2 + He							RTYPE=2 K1=2.70E-09 K2=-0.5 K3=0.0 # gg08_rnum=2486
He1+ + C3H3 -> C3H21+ + H + He							RTYPE=2 K1=2.70E-09 K2=-0.5 K3=0.0 # gg08_rnum=2487
He1+ + C3H3 -> C31+ + H + H2 + He						RTYPE=2 K1=2.70E-09 K2=-0.5 K3=0.0 # gg08_rnum=2488
He1+ + C3H3N -> H2C2H1+ + CN + He						RTYPE=2 K1=7.80E-09 K2=-0.5 K3=0.0 # gg08_rnum=2489
He1+ + C3H4 -> C31+ + H2 + H2 + He						RTYPE=2 K1=4.00E-10 K2=-0.5 K3=0.0 # gg08_rnum=2490
He1+ + C3H4 -> C3H1+ + He + H + H2						RTYPE=2 K1=4.00E-10 K2=-0.5 K3=0.0 # gg08_rnum=2491
He1+ + C3H4 -> C3H21+ + He + H2							RTYPE=2 K1=4.00E-10 K2=-0.5 K3=0.0 # gg08_rnum=2492
He1+ + C3H4 -> C3H31+ + He + H							RTYPE=2 K1=4.00E-10 K2=-0.5 K3=0.0 # gg08_rnum=2493
He1+ + C3N -> C21+ + CN + He							RTYPE=2 K1=5.25E-09 K2=-0.5 K3=0.0 # gg08_rnum=2494
He1+ + C3N -> C3N1+ + He								RTYPE=2 K1=5.25E-09 K2=-0.5 K3=0.0 # gg08_rnum=2495
He1+ + C3O -> C21+ + CO + He							RTYPE=2 K1=5.70E-09 K2=-0.5 K3=0.0 # gg08_rnum=2496
He1+ + C3O -> C31+ + O + He								RTYPE=2 K1=5.70E-09 K2=-0.5 K3=0.0 # gg08_rnum=2497
He1+ + C3P -> P1+ + C3 + He								RTYPE=2 K1=1.00E-09 K2=-0.5 K3=0.0 # gg08_rnum=2498
He1+ + C3P -> C31+ + P + He								RTYPE=2 K1=1.00E-09 K2=-0.5 K3=0.0 # gg08_rnum=2499
He1+ + C3S -> C21+ + CS + He							RTYPE=2 K1=3.89E-09 K2=-0.5 K3=0.0 # gg08_rnum=2500
He1+ + C3S -> CS1+ + C2 + He							RTYPE=2 K1=3.89E-09 K2=-0.5 K3=0.0 # gg08_rnum=2501
He1+ + C4 -> C1+ + C3 + He								RTYPE=2 K1=6.70E-10 K2=0.0 K3=0.0 # gg08_rnum=2502
He1+ + C4 -> C21+ + C2 + He								RTYPE=2 K1=6.70E-10 K2=0.0 K3=0.0 # gg08_rnum=2503
He1+ + C4 -> C31+ + C + He								RTYPE=2 K1=6.70E-10 K2=0.0 K3=0.0 # gg08_rnum=2504
He1+ + C4H -> C2H1+ + C2 + He							RTYPE=2 K1=2.10E-09 K2=-0.5 K3=0.0 # gg08_rnum=2505
He1+ + C4H -> C41+ + H + He								RTYPE=2 K1=2.10E-09 K2=-0.5 K3=0.0 # gg08_rnum=2506
He1+ + HC4H -> C4H1+ + H + He							RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=2507
He1+ + HC4H -> C2H1+ + C2H + He							RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=2508
He1+ + HC4H -> C41+ + H2 + He							RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=2509
He1+ + C4H3 -> C3H31+ + He + C							RTYPE=2 K1=6.70E-10 K2=-0.5 K3=0.0 # gg08_rnum=2510
He1+ + C4H3 -> C3H21+ + CH + He							RTYPE=2 K1=6.70E-10 K2=-0.5 K3=0.0 # gg08_rnum=2511
He1+ + C4H3 -> HC4H1+ + He + H							RTYPE=2 K1=6.70E-10 K2=-0.5 K3=0.0 # gg08_rnum=2512
He1+ + C4H4 -> C4H31+ + H + He							RTYPE=2 K1=4.02E-10 K2=-0.5 K3=0.0 # gg08_rnum=2513
He1+ + C4H4 -> HC2H1+ + HC2H + He						RTYPE=2 K1=4.02E-10 K2=-0.5 K3=0.0 # gg08_rnum=2514
He1+ + C4P -> CCP1+ + C2 + He							RTYPE=2 K1=4.97E-10 K2=-0.5 K3=0.0 # gg08_rnum=2515
He1+ + C4P -> C21+ + CCP + He							RTYPE=2 K1=4.97E-10 K2=-0.5 K3=0.0 # gg08_rnum=2516
He1+ + C4S -> C31+ + CS + He							RTYPE=2 K1=2.98E-09 K2=-0.5 K3=0.0 # gg08_rnum=2517
He1+ + C4S -> CS1+ + C3 + He							RTYPE=2 K1=2.98E-09 K2=-0.5 K3=0.0 # gg08_rnum=2518
He1+ + C5 -> C41+ + C + He								RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=2519
He1+ + C5 -> C31+ + C2 + He								RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=2520
He1+ + C5H -> C51+ + H + He								RTYPE=2 K1=1.02E-08 K2=-0.5 K3=0.0 # gg08_rnum=2521
He1+ + C5H -> C3H1+ + C2 + He							RTYPE=2 K1=1.02E-08 K2=-0.5 K3=0.0 # gg08_rnum=2522
He1+ + C5H2 -> C5H1+ + H + He							RTYPE=2 K1=1.67E-09 K2=-0.5 K3=0.0 # gg08_rnum=2523
He1+ + C5H2 -> C3H1+ + C2H + He							RTYPE=2 K1=1.67E-09 K2=-0.5 K3=0.0 # gg08_rnum=2524
He1+ + C5H2 -> C51+ + H2 + He							RTYPE=2 K1=1.67E-09 K2=-0.5 K3=0.0 # gg08_rnum=2525
He1+ + C5H4 -> C3H21+ + HC2H + He						RTYPE=2 K1=5.18E-10 K2=-0.5 K3=0.0 # gg08_rnum=2526
He1+ + C5H4 -> C5H21+ + H2 + He							RTYPE=2 K1=5.18E-10 K2=-0.5 K3=0.0 # gg08_rnum=2527
He1+ + C5H4 -> C5H31+ + H + He							RTYPE=2 K1=5.18E-10 K2=-0.5 K3=0.0 # gg08_rnum=2528
He1+ + C5N -> C41+ + CN + He							RTYPE=2 K1=1.30E-08 K2=-0.5 K3=0.0 # gg08_rnum=2529
He1+ + C6 -> C41+ + C2 + He								RTYPE=2 K1=1.40E-09 K2=0.0 K3=0.0 # gg08_rnum=2530
He1+ + C6 -> C51+ + C + He								RTYPE=2 K1=1.40E-09 K2=0.0 K3=0.0 # gg08_rnum=2531
He1+ + C6H -> C61+ + H + He								RTYPE=2 K1=1.18E-08 K2=-0.5 K3=0.0 # gg08_rnum=2532
He1+ + C6H -> C4H1+ + C2 + He							RTYPE=2 K1=1.18E-08 K2=-0.5 K3=0.0 # gg08_rnum=2533
He1+ + C6H2 -> C4H1+ + C2H + He							RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=2534
He1+ + C6H2 -> C6H1+ + H + He							RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=2535
He1+ + C6H2 -> C61+ + H2 + He							RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=2536
He1+ + C6H4 -> HC4H1+ + HC2H + He						RTYPE=2 K1=2.65E-10 K2=-0.5 K3=0.0 # gg08_rnum=2537
He1+ + C6H4 -> C6H21+ + H2 + He							RTYPE=2 K1=2.65E-10 K2=-0.5 K3=0.0 # gg08_rnum=2538
He1+ + C6H4 -> C6H31+ + H + He							RTYPE=2 K1=2.65E-10 K2=-0.5 K3=0.0 # gg08_rnum=2539
He1+ + C6H6 -> C5H51+ + CH + He							RTYPE=2 K1=7.00E-10 K2=0.0 K3=0.0 # gg08_rnum=2540
He1+ + C6H6 -> C6H51+ + H + He							RTYPE=2 K1=7.00E-10 K2=0.0 K3=0.0 # gg08_rnum=2541
He1+ + C7 -> C51+ + C2 + He								RTYPE=2 K1=1.40E-09 K2=0.0 K3=0.0 # gg08_rnum=2542
He1+ + C7 -> C61+ + C + He								RTYPE=2 K1=1.40E-09 K2=0.0 K3=0.0 # gg08_rnum=2543
He1+ + C7H -> C5H1+ + C2 + He							RTYPE=2 K1=1.06E-08 K2=-0.5 K3=0.0 # gg08_rnum=2544
He1+ + C7H -> C71+ + H + He								RTYPE=2 K1=1.06E-08 K2=-0.5 K3=0.0 # gg08_rnum=2545
He1+ + C7H2 -> C5H1+ + C2H + He							RTYPE=2 K1=1.65E-09 K2=-0.5 K3=0.0 # gg08_rnum=2546
He1+ + C7H2 -> C71+ + H2 + He							RTYPE=2 K1=1.65E-09 K2=-0.5 K3=0.0 # gg08_rnum=2547
He1+ + C7H2 -> C7H1+ + H + He							RTYPE=2 K1=1.65E-09 K2=-0.5 K3=0.0 # gg08_rnum=2548
He1+ + C7H4 -> C7H21+ + H2 + He							RTYPE=2 K1=5.15E-10 K2=-0.5 K3=0.0 # gg08_rnum=2549
He1+ + C7H4 -> C7H31+ + H + He							RTYPE=2 K1=5.15E-10 K2=-0.5 K3=0.0 # gg08_rnum=2550
He1+ + C7H4 -> C5H21+ + HC2H + He						RTYPE=2 K1=5.15E-10 K2=-0.5 K3=0.0 # gg08_rnum=2551
He1+ + C7N -> C61+ + CN + He							RTYPE=2 K1=1.40E-08 K2=-0.5 K3=0.0 # gg08_rnum=2552
He1+ + C8 -> C61+ + C2 + He								RTYPE=2 K1=1.50E-09 K2=0.0 K3=0.0 # gg08_rnum=2553
He1+ + C8 -> C71+ + C + He								RTYPE=2 K1=1.50E-09 K2=0.0 K3=0.0 # gg08_rnum=2554
He1+ + C8H -> C6H1+ + C2 + He							RTYPE=2 K1=1.17E-08 K2=-0.5 K3=0.0 # gg08_rnum=2555
He1+ + C8H -> C81+ + H + He								RTYPE=2 K1=1.17E-08 K2=-0.5 K3=0.0 # gg08_rnum=2556
He1+ + C8H2 -> C8H1+ + H + He							RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=2557
He1+ + C8H2 -> C6H1+ + C2H + He							RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=2558
He1+ + C8H2 -> C81+ + H2 + He							RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=2559
He1+ + C8H4 -> C8H21+ + H2 + He							RTYPE=2 K1=2.63E-10 K2=-0.5 K3=0.0 # gg08_rnum=2560
He1+ + C8H4 -> C8H31+ + H + He							RTYPE=2 K1=2.63E-10 K2=-0.5 K3=0.0 # gg08_rnum=2561
He1+ + C8H4 -> C6H21+ + HC2H + He						RTYPE=2 K1=2.63E-10 K2=-0.5 K3=0.0 # gg08_rnum=2562
He1+ + C9 -> C71+ + C2 + He								RTYPE=2 K1=1.50E-09 K2=0.0 K3=0.0 # gg08_rnum=2563
He1+ + C9 -> C81+ + C + He								RTYPE=2 K1=1.50E-09 K2=0.0 K3=0.0 # gg08_rnum=2564
He1+ + C9H -> C7H1+ + C2 + He							RTYPE=2 K1=1.10E-08 K2=-0.5 K3=0.0 # gg08_rnum=2565
He1+ + C9H -> C91+ + H + He								RTYPE=2 K1=1.10E-08 K2=-0.5 K3=0.0 # gg08_rnum=2566
He1+ + C9H2 -> C9H1+ + H + He							RTYPE=2 K1=1.65E-09 K2=-0.5 K3=0.0 # gg08_rnum=2567
He1+ + C9H2 -> C7H1+ + C2H + He							RTYPE=2 K1=1.65E-09 K2=-0.5 K3=0.0 # gg08_rnum=2568
He1+ + C9H2 -> C91+ + H2 + He							RTYPE=2 K1=1.65E-09 K2=-0.5 K3=0.0 # gg08_rnum=2569
He1+ + C9H4 -> C9H21+ + H2 + He							RTYPE=2 K1=5.12E-10 K2=-0.5 K3=0.0 # gg08_rnum=2570
He1+ + C9H4 -> C9H31+ + H + He							RTYPE=2 K1=5.12E-10 K2=-0.5 K3=0.0 # gg08_rnum=2571
He1+ + C9H4 -> C7H21+ + HC2H + He						RTYPE=2 K1=5.12E-10 K2=-0.5 K3=0.0 # gg08_rnum=2572
He1+ + C9N -> C81+ + CN + He							RTYPE=2 K1=1.54E-08 K2=-0.5 K3=0.0 # gg08_rnum=2573
He1+ + ClC -> C1+ + Cl + He								RTYPE=2 K1=3.11E-09 K2=-0.5 K3=0.0 # gg08_rnum=2574
He1+ + CCP -> C1+ + CP + He								RTYPE=2 K1=2.38E-09 K2=-0.5 K3=0.0 # gg08_rnum=2575
He1+ + CCP -> P1+ + C2 + He								RTYPE=2 K1=2.38E-09 K2=-0.5 K3=0.0 # gg08_rnum=2576
He1+ + CH -> CH1+ + He									RTYPE=2 K1=3.83E-09 K2=-0.5 K3=0.0 # gg08_rnum=2577
He1+ + CH -> C1+ + H + He								RTYPE=2 K1=3.83E-09 K2=-0.5 K3=0.0 # gg08_rnum=2578
He1+ + CH2 -> C1+ + H2 + He								RTYPE=2 K1=6.25E-10 K2=-0.5 K3=0.0 # gg08_rnum=2579
He1+ + CH2 -> CH1+ + H + He								RTYPE=2 K1=6.25E-10 K2=-0.5 K3=0.0 # gg08_rnum=2580
He1+ + H2C2N -> CH21+ + CN + He							RTYPE=2 K1=3.25E-09 K2=-0.5 K3=0.0 # gg08_rnum=2581
He1+ + CH2CO -> CO1+ + CH2 + He							RTYPE=2 K1=1.40E-09 K2=-0.5 K3=0.0 # gg08_rnum=2582
He1+ + CH2CO -> CH21+ + CO + He							RTYPE=2 K1=1.40E-09 K2=-0.5 K3=0.0 # gg08_rnum=2583
He1+ + CH2NH -> CH21+ + NH + He							RTYPE=2 K1=1.00E-09 K2=-0.5 K3=0.0 # gg08_rnum=2584
He1+ + CH2NH -> HCN1+ + H2 + He							RTYPE=2 K1=1.00E-09 K2=-0.5 K3=0.0 # gg08_rnum=2585
He1+ + CH2NH -> H2CN1+ + H + He							RTYPE=2 K1=1.00E-09 K2=-0.5 K3=0.0 # gg08_rnum=2586
He1+ + CH2NH -> NH1+ + CH2 + He							RTYPE=2 K1=1.00E-09 K2=-0.5 K3=0.0 # gg08_rnum=2587
He1+ + CH2OH -> CH2 + OH1+ + He							RTYPE=2 K1=1.32E-09 K2=-0.5 K3=0.0 # gg08_rnum=2588
He1+ + CH2OH -> CH21+ + OH + He							RTYPE=2 K1=1.32E-09 K2=-0.5 K3=0.0 # gg08_rnum=2589
He1+ + CH2PH -> PH1+ + CH2 + He							RTYPE=2 K1=5.05E-10 K2=-0.5 K3=0.0 # gg08_rnum=2590
He1+ + CH2PH -> CH21+ + PH + He							RTYPE=2 K1=5.05E-10 K2=-0.5 K3=0.0 # gg08_rnum=2591
He1+ + CH3 -> CH21+ + H + He							RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=2592
He1+ + CH3 -> CH1+ + H2 + He							RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=2593
He1+ + CH3C3N -> CH31+ + C3N + He						RTYPE=2 K1=9.49E-09 K2=-0.5 K3=0.0 # gg08_rnum=2594
He1+ + CH3C4H -> C5H31+ + H + He						RTYPE=2 K1=1.21E-09 K2=-0.5 K3=0.0 # gg08_rnum=2595
He1+ + CH3C4H -> C5H21+ + H2 + He						RTYPE=2 K1=1.21E-09 K2=-0.5 K3=0.0 # gg08_rnum=2596
He1+ + CH3C5N -> CH31+ + C5N + He						RTYPE=2 K1=1.04E-08 K2=-0.5 K3=0.0 # gg08_rnum=2597
He1+ + CH3C6H -> C7H31+ + H + He						RTYPE=2 K1=1.49E-09 K2=-0.5 K3=0.0 # gg08_rnum=2598
He1+ + CH3C6H -> C7H21+ + H2 + He						RTYPE=2 K1=1.49E-09 K2=-0.5 K3=0.0 # gg08_rnum=2599
He1+ + CH3C7N -> CH31+ + C7N + He						RTYPE=2 K1=1.08E-08 K2=-0.5 K3=0.0 # gg08_rnum=2600
He1+ + CH3CHO -> CH31+ + HCO + He						RTYPE=2 K1=1.87E-09 K2=-0.5 K3=0.0 # gg08_rnum=2601
He1+ + CH3CHO -> CH3CO1+ + H + He						RTYPE=2 K1=1.87E-09 K2=-0.5 K3=0.0 # gg08_rnum=2602
He1+ + CH3CHO -> HCO1+ + CH3 + He						RTYPE=2 K1=1.87E-09 K2=-0.5 K3=0.0 # gg08_rnum=2603
He1+ + CH3CN -> CH31+ + CN + He							RTYPE=2 K1=4.00E-09 K2=-0.5 K3=0.0 # gg08_rnum=2604
He1+ + CH3CN -> CN1+ + CH3 + He							RTYPE=2 K1=4.00E-09 K2=-0.5 K3=0.0 # gg08_rnum=2605
He1+ + CH3CO -> CH31+ + CO + He							RTYPE=2 K1=1.96E-09 K2=-0.5 K3=0.0 # gg08_rnum=2606
He1+ + CH3CO -> CH3 + CO1+ + He							RTYPE=2 K1=1.96E-09 K2=-0.5 K3=0.0 # gg08_rnum=2607
He1+ + CH3NH -> CH3 + NH1+ + He							RTYPE=2 K1=1.16E-09 K2=-0.5 K3=0.0 # gg08_rnum=2608
He1+ + CH3NH -> CH31+ + NH + He							RTYPE=2 K1=1.16E-09 K2=-0.5 K3=0.0 # gg08_rnum=2609
He1+ + CH3NH2 -> NH21+ + CH3 + He						RTYPE=2 K1=8.93E-10 K2=-0.5 K3=0.0 # gg08_rnum=2610
He1+ + CH3NH2 -> CH31+ + NH2 + He						RTYPE=2 K1=8.93E-10 K2=-0.5 K3=0.0 # gg08_rnum=2611
He1+ + CH3NH2 -> H2CN1+ + H + H2 + He					RTYPE=2 K1=8.93E-10 K2=-0.5 K3=0.0 # gg08_rnum=2612
He1+ + CH3O -> H2CO + H1+ + He							RTYPE=2 K1=2.01E-10 K2=-0.5 K3=0.0 # gg08_rnum=2613
He1+ + CH3O -> H2CO1+ + H + He							RTYPE=2 K1=2.01E-10 K2=-0.5 K3=0.0 # gg08_rnum=2614
He1+ + CH3OCH3 -> CH31+ + CH3O + He						RTYPE=2 K1=1.32E-09 K2=-0.5 K3=0.0 # gg08_rnum=2615
He1+ + CH3OCH3 -> CH3O1+ + CH3 + He						RTYPE=2 K1=1.32E-09 K2=-0.5 K3=0.0 # gg08_rnum=2616
He1+ + CH3OH -> OH1+ + CH3 + He							RTYPE=2 K1=1.70E-09 K2=-0.5 K3=0.0 # gg08_rnum=2617
He1+ + CH3OH -> CH31+ + OH + He							RTYPE=2 K1=1.70E-09 K2=-0.5 K3=0.0 # gg08_rnum=2618
He1+ + CH4 -> CH1+ + H2 + H + He						RTYPE=2 K1=2.40E-10 K2=0.0 K3=0.0 # gg08_rnum=2619
He1+ + CH4 -> CH21+ + H2 + He							RTYPE=2 K1=9.50E-10 K2=0.0 K3=0.0 # gg08_rnum=2620
He1+ + CH4 -> CH31+ + H + He							RTYPE=2 K1=8.50E-11 K2=0.0 K3=0.0 # gg08_rnum=2621
He1+ + CH4 -> CH41+ + He								RTYPE=2 K1=5.10E-11 K2=0.0 K3=0.0 # gg08_rnum=2622
He1+ + CH4 -> H1+ + CH3 + He							RTYPE=2 K1=4.80E-10 K2=0.0 K3=0.0 # gg08_rnum=2623
He1+ + ClO -> Cl1+ + O + He								RTYPE=2 K1=5.91E-09 K2=-0.5 K3=0.0 # gg08_rnum=2624
He1+ + CN -> N1+ + C + He								RTYPE=2 K1=3.60E-09 K2=-0.5 K3=0.0 # gg08_rnum=2625
He1+ + CN -> C1+ + N + He								RTYPE=2 K1=3.60E-09 K2=-0.5 K3=0.0 # gg08_rnum=2626
He1+ + CO -> C1+ + O + He								RTYPE=2 K1=1.60E-09 K2=0.0 K3=0.0 # gg08_rnum=2627
He1+ + CO2 -> O1+ + CO + He								RTYPE=2 K1=2.00E-10 K2=0.0 K3=0.0 # gg08_rnum=2628
He1+ + CO2 -> CO1+ + O + He								RTYPE=2 K1=8.00E-10 K2=0.0 K3=0.0 # gg08_rnum=2629
He1+ + CO2 -> O21+ + C + He								RTYPE=2 K1=1.10E-11 K2=0.0 K3=0.0 # gg08_rnum=2630
He1+ + CO2 -> CO21+ + He								RTYPE=2 K1=1.21E-10 K2=0.0 K3=0.0 # gg08_rnum=2631
He1+ + CO2 -> C1+ + O2 + He								RTYPE=2 K1=4.00E-11 K2=0.0 K3=0.0 # gg08_rnum=2632
He1+ + COCHO -> HCO1+ + CO + He							RTYPE=2 K1=8.55E-10 K2=-0.5 K3=0.0 # gg08_rnum=2633
He1+ + COCHO -> HCO + CO1+ + He							RTYPE=2 K1=8.55E-10 K2=-0.5 K3=0.0 # gg08_rnum=2634
He1+ + COOH -> CO1+ + OH + He							RTYPE=2 K1=2.96E-09 K2=-0.5 K3=0.0 # gg08_rnum=2635
He1+ + COOH -> CO + OH1+ + He							RTYPE=2 K1=2.96E-09 K2=-0.5 K3=0.0 # gg08_rnum=2636
He1+ + CP -> C1+ + P + He								RTYPE=2 K1=2.06E-09 K2=-0.5 K3=0.0 # gg08_rnum=2637
He1+ + CP -> P1+ + C + He								RTYPE=2 K1=2.06E-09 K2=-0.5 K3=0.0 # gg08_rnum=2638
He1+ + CS -> C1+ + S + He								RTYPE=2 K1=4.70E-09 K2=-0.5 K3=0.0 # gg08_rnum=2639
He1+ + CS -> S1+ + C + He								RTYPE=2 K1=4.70E-09 K2=-0.5 K3=0.0 # gg08_rnum=2640
He1+ + H -> H1+ + He									RTYPE=2 K1=1.90E-15 K2=0.0 K3=0.0 # gg08_rnum=2641
He1+ + H2 -> H1+ + H + He								RTYPE=2 K1=3.30E-15 K2=0.0 K3=0.0 # gg08_rnum=2642
He1+ + H2 -> H21+ + He									RTYPE=2 K1=9.60E-15 K2=0.0 K3=0.0 # gg08_rnum=2643
He1+ + H2CO -> CO1+ + H2 + He							RTYPE=2 K1=2.40E-09 K2=-0.5 K3=0.0 # gg08_rnum=2644
He1+ + H2CO -> HCO1+ + H + He							RTYPE=2 K1=2.40E-09 K2=-0.5 K3=0.0 # gg08_rnum=2645
He1+ + H2CS -> CH21+ + S + He							RTYPE=2 K1=1.11E-09 K2=-0.5 K3=0.0 # gg08_rnum=2646
He1+ + H2CS -> S1+ + CH2 + He							RTYPE=2 K1=1.11E-09 K2=-0.5 K3=0.0 # gg08_rnum=2647
He1+ + H2CS -> CS1+ + H2 + He							RTYPE=2 K1=1.11E-09 K2=-0.5 K3=0.0 # gg08_rnum=2648
He1+ + H2O -> OH1+ + H + He								RTYPE=2 K1=1.32E-09 K2=-0.5 K3=0.0 # gg08_rnum=2649
He1+ + H2O -> H2O1+ + He								RTYPE=2 K1=1.32E-09 K2=-0.5 K3=0.0 # gg08_rnum=2650
He1+ + H2O -> H1+ + OH + He								RTYPE=2 K1=1.32E-09 K2=-0.5 K3=0.0 # gg08_rnum=2651
He1+ + H2S -> HS1+ + H + He								RTYPE=2 K1=2.20E-10 K2=-0.5 K3=0.0 # gg08_rnum=2652
He1+ + H2S -> H2S1+ + He								RTYPE=2 K1=1.40E-10 K2=-0.5 K3=0.0 # gg08_rnum=2653
He1+ + H2S -> S1+ + H2 + He								RTYPE=2 K1=1.60E-09 K2=-0.5 K3=0.0 # gg08_rnum=2654
He1+ + H2S2 -> HS1+ + HS + He							RTYPE=2 K1=1.20E-09 K2=-0.5 K3=0.0 # gg08_rnum=2655
He1+ + H2S2 -> S2H1+ + H + He							RTYPE=2 K1=1.20E-09 K2=-0.5 K3=0.0 # gg08_rnum=2656
He1+ + H2SiO -> HSiO1+ + H + He							RTYPE=2 K1=4.04E-09 K2=-0.5 K3=0.0 # gg08_rnum=2657
He1+ + H5C3N -> CH31+ + CH2 + CN + He					RTYPE=2 K1=8.05E-09 K2=-0.5 K3=0.0 # gg08_rnum=2658
He1+ + HC2NC -> CN1+ + C2H + He							RTYPE=2 K1=1.10E-09 K2=-0.5 K3=0.0 # gg08_rnum=2659
He1+ + HC2NC -> C2H1+ + CN + He							RTYPE=2 K1=1.10E-09 K2=-0.5 K3=0.0 # gg08_rnum=2660
He1+ + HC2NC -> C3N1+ + H + He							RTYPE=2 K1=1.10E-09 K2=-0.5 K3=0.0 # gg08_rnum=2661
He1+ + HC2NC -> CNC1+ + CH + He							RTYPE=2 K1=1.10E-09 K2=-0.5 K3=0.0 # gg08_rnum=2662
He1+ + HC3N -> C3H1+ + N + He							RTYPE=2 K1=2.53E-09 K2=-0.5 K3=0.0 # gg08_rnum=2663
He1+ + HC3N -> C3N1+ + H + He							RTYPE=2 K1=2.53E-09 K2=-0.5 K3=0.0 # gg08_rnum=2664
He1+ + HC3N -> C21+ + HCN + He							RTYPE=2 K1=2.53E-09 K2=-0.5 K3=0.0 # gg08_rnum=2665
He1+ + HC3N -> CN1+ + C2H + He							RTYPE=2 K1=2.53E-09 K2=-0.5 K3=0.0 # gg08_rnum=2666
He1+ + HC3N -> C2H1+ + CN + He							RTYPE=2 K1=2.53E-09 K2=-0.5 K3=0.0 # gg08_rnum=2667
He1+ + HC3N -> C2N1+ + CH + He							RTYPE=2 K1=2.53E-09 K2=-0.5 K3=0.0 # gg08_rnum=2668
He1+ + HC3N -> C31+ + NH + He							RTYPE=2 K1=2.53E-09 K2=-0.5 K3=0.0 # gg08_rnum=2669
He1+ + HC5N -> C4H + CN1+ + He							RTYPE=2 K1=6.80E-09 K2=-0.5 K3=0.0 # gg08_rnum=2670
He1+ + HC5N -> C2H1+ + C3N + He							RTYPE=2 K1=6.80E-09 K2=-0.5 K3=0.0 # gg08_rnum=2671
He1+ + HC5N -> C4H1+ + CN + He							RTYPE=2 K1=6.80E-09 K2=-0.5 K3=0.0 # gg08_rnum=2672
He1+ + HC7N -> C6H1+ + CN + He							RTYPE=2 K1=2.16E-08 K2=-0.5 K3=0.0 # gg08_rnum=2673
He1+ + HC9N -> C8H1+ + CN + He							RTYPE=2 K1=2.26E-08 K2=-0.5 K3=0.0 # gg08_rnum=2674
He1+ + HCCP -> CH1+ + CP + He							RTYPE=2 K1=5.00E-10 K2=0.0 K3=0.0 # gg08_rnum=2675
He1+ + HCCP -> CP1+ + CH + He							RTYPE=2 K1=5.00E-10 K2=0.0 K3=0.0 # gg08_rnum=2676
He1+ + ClH -> Cl1+ + H + He								RTYPE=2 K1=5.22E-09 K2=-0.5 K3=0.0 # gg08_rnum=2677
He1+ + HCN -> C1+ + N + H + He							RTYPE=2 K1=3.70E-09 K2=-0.5 K3=0.0 # gg08_rnum=2678
He1+ + HCN -> N1+ + CH + He								RTYPE=2 K1=1.00E-09 K2=-0.5 K3=0.0 # gg08_rnum=2679
He1+ + HCN -> CH1+ + N + He								RTYPE=2 K1=3.10E-09 K2=-0.5 K3=0.0 # gg08_rnum=2680
He1+ + HCN -> CN1+ + H + He								RTYPE=2 K1=6.90E-09 K2=-0.5 K3=0.0 # gg08_rnum=2681
He1+ + C2NCH -> C3N1+ + H + He							RTYPE=2 K1=3.70E-09 K2=-0.5 K3=0.0 # gg08_rnum=2682
He1+ + C2NCH -> HCN1+ + C2 + He							RTYPE=2 K1=3.70E-09 K2=-0.5 K3=0.0 # gg08_rnum=2683
He1+ + C2NCH -> C21+ + HCN + He							RTYPE=2 K1=3.70E-09 K2=-0.5 K3=0.0 # gg08_rnum=2684
He1+ + C2NCH -> C2N1+ + CH + He							RTYPE=2 K1=3.70E-09 K2=-0.5 K3=0.0 # gg08_rnum=2685
He1+ + HCO -> CH1+ + O + He								RTYPE=2 K1=6.90E-10 K2=-0.5 K3=0.0 # gg08_rnum=2686
He1+ + HCO -> CO1+ + H + He								RTYPE=2 K1=6.90E-10 K2=-0.5 K3=0.0 # gg08_rnum=2687
He1+ + HCO -> HeH1+ + CO								RTYPE=2 K1=6.90E-10 K2=-0.5 K3=0.0 # gg08_rnum=2688
He1+ + HCOOCH3 -> HOCO1+ + CH3 + He						RTYPE=2 K1=3.54E-09 K2=-0.5 K3=0.0 # gg08_rnum=2689
He1+ + HCOOH -> HOCO1+ + H + He							RTYPE=2 K1=1.40E-09 K2=-0.5 K3=0.0 # gg08_rnum=2690
He1+ + HCOOH -> CO21+ + H + H + He						RTYPE=2 K1=1.40E-09 K2=-0.5 K3=0.0 # gg08_rnum=2691
He1+ + HCP -> C1+ + PH + He								RTYPE=2 K1=9.35E-10 K2=-0.5 K3=0.0 # gg08_rnum=2692
He1+ + HCP -> P1+ + CH + He								RTYPE=2 K1=9.35E-10 K2=-0.5 K3=0.0 # gg08_rnum=2693
He1+ + HCS -> H1+ + CS + He								RTYPE=2 K1=4.79E-09 K2=-0.5 K3=0.0 # gg08_rnum=2694
He1+ + HCS -> CS1+ + H + He								RTYPE=2 K1=4.79E-09 K2=-0.5 K3=0.0 # gg08_rnum=2695
He1+ + SiCH -> Si1+ + CH + He							RTYPE=2 K1=2.41E-09 K2=-0.5 K3=0.0 # gg08_rnum=2696
He1+ + SiCH -> SiC1+ + H + He							RTYPE=2 K1=2.41E-09 K2=-0.5 K3=0.0 # gg08_rnum=2697
He1+ + CH3COCHO -> CH31+ + COCHO + He					RTYPE=2 K1=1.75E-09 K2=-0.5 K3=0.0 # gg08_rnum=2698
He1+ + CH3COCHO -> CH3 + COCHO1+ + He					RTYPE=2 K1=1.75E-09 K2=-0.5 K3=0.0 # gg08_rnum=2699
He1+ + CH3COCHO -> CH3CO1+ + HCO + He					RTYPE=2 K1=1.75E-09 K2=-0.5 K3=0.0 # gg08_rnum=2700
He1+ + CH3COCHO -> CH3CO + HCO1+ + He					RTYPE=2 K1=1.75E-09 K2=-0.5 K3=0.0 # gg08_rnum=2701
He1+ + CH3COCH3 -> CH3CO + CH31+ + He					RTYPE=2 K1=2.41E-09 K2=-0.5 K3=0.0 # gg08_rnum=2702
He1+ + CH3COCH3 -> CH3CO1+ + CH3 + He					RTYPE=2 K1=2.41E-09 K2=-0.5 K3=0.0 # gg08_rnum=2703
He1+ + CH3CONH -> CH3CO1+ + NH + He						RTYPE=2 K1=1.58E-09 K2=-0.5 K3=0.0 # gg08_rnum=2704
He1+ + CH3CONH -> CH3CO + NH1+ + He						RTYPE=2 K1=1.58E-09 K2=-0.5 K3=0.0 # gg08_rnum=2705
He1+ + CH3CONH -> CH31+ + HNCO + He						RTYPE=2 K1=1.58E-09 K2=-0.5 K3=0.0 # gg08_rnum=2706
He1+ + CH3CONH -> CH3 + HNCO1+ + He						RTYPE=2 K1=1.58E-09 K2=-0.5 K3=0.0 # gg08_rnum=2707
He1+ + CH3COOH -> CH3CO + OH1+ + He						RTYPE=2 K1=7.28E-10 K2=-0.5 K3=0.0 # gg08_rnum=2708
He1+ + CH3COOH -> CH31+ + COOH + He						RTYPE=2 K1=7.28E-10 K2=-0.5 K3=0.0 # gg08_rnum=2709
He1+ + CH3COOH -> CH3 + COOH1+ + He						RTYPE=2 K1=7.28E-10 K2=-0.5 K3=0.0 # gg08_rnum=2710
He1+ + CH3COOH -> CH3CO1+ + OH + He						RTYPE=2 K1=7.28E-10 K2=-0.5 K3=0.0 # gg08_rnum=2711
He1+ + CH3CONH2 -> CH3CO1+ + NH2 + He					RTYPE=2 K1=1.58E-09 K2=-0.5 K3=0.0 # gg08_rnum=2712
He1+ + CH3CONH2 -> CH3CO + NH21+ + He					RTYPE=2 K1=1.58E-09 K2=-0.5 K3=0.0 # gg08_rnum=2713
He1+ + CH3CONH2 -> CH31+ + NH2CO + He					RTYPE=2 K1=1.58E-09 K2=-0.5 K3=0.0 # gg08_rnum=2714
He1+ + CH3CONH2 -> CH3 + NH2CO1+ + He					RTYPE=2 K1=1.58E-09 K2=-0.5 K3=0.0 # gg08_rnum=2715
He1+ + CH3COOCH3 -> CH3CO + CH3O1+ + He					RTYPE=2 K1=7.43E-10 K2=-0.5 K3=0.0 # gg08_rnum=2716
He1+ + CH3COOCH3 -> CH31+ + CH3OCO + He					RTYPE=2 K1=7.43E-10 K2=-0.5 K3=0.0 # gg08_rnum=2717
He1+ + CH3COOCH3 -> CH3 + CH3OCO1+ + He					RTYPE=2 K1=7.43E-10 K2=-0.5 K3=0.0 # gg08_rnum=2718
He1+ + CH3COOCH3 -> CH3CO1+ + CH3O + He					RTYPE=2 K1=7.43E-10 K2=-0.5 K3=0.0 # gg08_rnum=2719
He1+ + CH3COCH2OH -> CH31+ + HOCH2CO + He				RTYPE=2 K1=1.29E-09 K2=-0.5 K3=0.0 # gg08_rnum=2720
He1+ + CH3COCH2OH -> CH3 + HOCH2CO1+ + He				RTYPE=2 K1=1.29E-09 K2=-0.5 K3=0.0 # gg08_rnum=2721
He1+ + CH3COCH2OH -> CH3CO1+ + CH2OH + He				RTYPE=2 K1=1.29E-09 K2=-0.5 K3=0.0 # gg08_rnum=2722
He1+ + CH3COCH2OH -> CH3CO + CH2OH1+ + He				RTYPE=2 K1=1.29E-09 K2=-0.5 K3=0.0 # gg08_rnum=2723
He1+ + HNC -> CN1+ + H + He								RTYPE=2 K1=4.43E-09 K2=-0.5 K3=0.0 # gg08_rnum=2724
He1+ + HNC -> NH1+ + C + He								RTYPE=2 K1=4.43E-09 K2=-0.5 K3=0.0 # gg08_rnum=2725
He1+ + HNC -> C1+ + N + H + He							RTYPE=2 K1=4.43E-09 K2=-0.5 K3=0.0 # gg08_rnum=2726
He1+ + HNC3 -> C21+ + HNC + He							RTYPE=2 K1=2.00E-09 K2=-0.5 K3=0.0 # gg08_rnum=2727
He1+ + HNC3 -> C31+ + NH + He							RTYPE=2 K1=2.00E-09 K2=-0.5 K3=0.0 # gg08_rnum=2728
He1+ + HNC3 -> C3N1+ + H + He							RTYPE=2 K1=2.00E-09 K2=-0.5 K3=0.0 # gg08_rnum=2729
He1+ + HNC3 -> NH1+ + C3 + He							RTYPE=2 K1=2.00E-09 K2=-0.5 K3=0.0 # gg08_rnum=2730
He1+ + HNC3 -> HC2N1+ + C + He							RTYPE=2 K1=2.00E-09 K2=-0.5 K3=0.0 # gg08_rnum=2731
He1+ + HNCHO -> NH1+ + HCO + He							RTYPE=2 K1=1.71E-09 K2=-0.5 K3=0.0 # gg08_rnum=2732
He1+ + HNCHO -> NH + HCO1+ + He							RTYPE=2 K1=1.71E-09 K2=-0.5 K3=0.0 # gg08_rnum=2733
He1+ + HNCO -> H1+ + OCN + He							RTYPE=2 K1=3.45E-09 K2=-0.5 K3=0.0 # gg08_rnum=2734
He1+ + HNCOCHO -> HNCO1+ + HCO + He						RTYPE=2 K1=4.20E-10 K2=-0.5 K3=0.0 # gg08_rnum=2735
He1+ + HNCOCHO -> NH + COCHO1+ + He						RTYPE=2 K1=4.20E-10 K2=-0.5 K3=0.0 # gg08_rnum=2736
He1+ + HNCOCHO -> NH1+ + COCHO + He						RTYPE=2 K1=4.20E-10 K2=-0.5 K3=0.0 # gg08_rnum=2737
He1+ + HNCOCHO -> HNCO + HCO1+ + He						RTYPE=2 K1=4.20E-10 K2=-0.5 K3=0.0 # gg08_rnum=2738
He1+ + HNCONH -> NH + HNCO1+ + He						RTYPE=2 K1=3.25E-09 K2=-0.5 K3=0.0 # gg08_rnum=2739
He1+ + HNCONH -> NH1+ + HNCO + He						RTYPE=2 K1=3.25E-09 K2=-0.5 K3=0.0 # gg08_rnum=2740
He1+ + HNCOOH -> HNCO + OH1+ + He						RTYPE=2 K1=8.43E-10 K2=-0.5 K3=0.0 # gg08_rnum=2741
He1+ + HNCOOH -> NH1+ + COOH + He						RTYPE=2 K1=8.43E-10 K2=-0.5 K3=0.0 # gg08_rnum=2742
He1+ + HNCOOH -> NH + COOH1+ + He						RTYPE=2 K1=8.43E-10 K2=-0.5 K3=0.0 # gg08_rnum=2743
He1+ + HNCOOH -> HNCO1+ + OH + He						RTYPE=2 K1=8.43E-10 K2=-0.5 K3=0.0 # gg08_rnum=2744
He1+ + HNO -> NO1+ + H + He								RTYPE=2 K1=1.80E-09 K2=-0.5 K3=0.0 # gg08_rnum=2745
He1+ + HNO -> H1+ + NO + He								RTYPE=2 K1=1.80E-09 K2=-0.5 K3=0.0 # gg08_rnum=2746
He1+ + HNOH -> NH + OH1+ + He							RTYPE=2 K1=1.72E-09 K2=-0.5 K3=0.0 # gg08_rnum=2747
He1+ + HNOH -> NH1+ + OH + He							RTYPE=2 K1=1.72E-09 K2=-0.5 K3=0.0 # gg08_rnum=2748
He1+ + HNSi -> SiN1+ + H + He							RTYPE=2 K1=7.68E-10 K2=-0.5 K3=0.0 # gg08_rnum=2749
He1+ + CH3ONH -> NH1+ + CH3O + He						RTYPE=2 K1=1.27E-09 K2=-0.5 K3=0.0 # gg08_rnum=2750
He1+ + CH3ONH -> NH + CH3O1+ + He						RTYPE=2 K1=1.27E-09 K2=-0.5 K3=0.0 # gg08_rnum=2751
He1+ + HNCH2OH -> NH + CH2OH1+ + He						RTYPE=2 K1=1.27E-09 K2=-0.5 K3=0.0 # gg08_rnum=2752
He1+ + HNCH2OH -> NH1+ + CH2OH + He						RTYPE=2 K1=1.27E-09 K2=-0.5 K3=0.0 # gg08_rnum=2753
He1+ + HOCOOH -> OH1+ + COOH + He						RTYPE=2 K1=1.68E-09 K2=-0.5 K3=0.0 # gg08_rnum=2754
He1+ + HOCOOH -> OH + COOH1+ + He						RTYPE=2 K1=1.68E-09 K2=-0.5 K3=0.0 # gg08_rnum=2755
He1+ + HPO -> PO1+ + H + He								RTYPE=2 K1=5.55E-09 K2=-0.5 K3=0.0 # gg08_rnum=2756
He1+ + HPO -> PH1+ + O + He								RTYPE=2 K1=5.55E-09 K2=-0.5 K3=0.0 # gg08_rnum=2757
He1+ + HS -> S1+ + H + He								RTYPE=2 K1=3.70E-09 K2=-0.5 K3=0.0 # gg08_rnum=2758
He1+ + S2H -> S21+ + H + He								RTYPE=2 K1=2.00E-09 K2=-0.5 K3=0.0 # gg08_rnum=2759
He1+ + S2H -> S1+ + HS + He								RTYPE=2 K1=2.00E-09 K2=-0.5 K3=0.0 # gg08_rnum=2760
He1+ + HCOCOCHO -> COCHO + HCO1+ + He					RTYPE=2 K1=1.68E-09 K2=-0.5 K3=0.0 # gg08_rnum=2761
He1+ + HCOCOCHO -> COCHO1+ + HCO + He					RTYPE=2 K1=1.68E-09 K2=-0.5 K3=0.0 # gg08_rnum=2762
He1+ + MgH -> Mg1+ + H + He								RTYPE=2 K1=6.30E-09 K2=-0.5 K3=0.0 # gg08_rnum=2763
He1+ + N2 -> N1+ + N + He								RTYPE=2 K1=8.00E-10 K2=0.0 K3=0.0 # gg08_rnum=2764
He1+ + N2 -> N21+ + He									RTYPE=2 K1=4.00E-10 K2=0.0 K3=0.0 # gg08_rnum=2765
He1+ + N2H2 -> N21+ + H + H + He						RTYPE=2 K1=2.00E-09 K2=0.0 K3=0.0 # gg08_rnum=2766
He1+ + N2O -> NO1+ + N + He								RTYPE=2 K1=8.45E-11 K2=-0.5 K3=0.0 # gg08_rnum=2767
He1+ + N2O -> N1+ + NO + He								RTYPE=2 K1=8.45E-11 K2=-0.5 K3=0.0 # gg08_rnum=2768
He1+ + N2O -> O1+ + N2 + He								RTYPE=2 K1=8.45E-11 K2=-0.5 K3=0.0 # gg08_rnum=2769
He1+ + N2O -> N21+ + O + He								RTYPE=2 K1=8.45E-11 K2=-0.5 K3=0.0 # gg08_rnum=2770
He1+ + NaH -> Na1+ + H + He								RTYPE=2 K1=3.30E-08 K2=-0.5 K3=0.0 # gg08_rnum=2771
He1+ + NaOH -> Na1+ + OH + He							RTYPE=2 K1=1.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=2772
He1+ + NH -> N1+ + H + He								RTYPE=2 K1=6.70E-09 K2=-0.5 K3=0.0 # gg08_rnum=2773
He1+ + NH2 -> NH1+ + H + He								RTYPE=2 K1=2.00E-09 K2=-0.5 K3=0.0 # gg08_rnum=2774
He1+ + NH2 -> N1+ + H2 + He								RTYPE=2 K1=2.00E-09 K2=-0.5 K3=0.0 # gg08_rnum=2775
He1+ + NH2CHO -> NH1+ + H2CO + He						RTYPE=2 K1=1.40E-09 K2=-0.5 K3=0.0 # gg08_rnum=2776
He1+ + NH2CHO -> NH31+ + CO + He						RTYPE=2 K1=1.40E-09 K2=-0.5 K3=0.0 # gg08_rnum=2777
He1+ + NH2CHO -> CO1+ + NH3 + He						RTYPE=2 K1=1.40E-09 K2=-0.5 K3=0.0 # gg08_rnum=2778
He1+ + NH2CHO -> HCO1+ + NH2 + He						RTYPE=2 K1=1.40E-09 K2=-0.5 K3=0.0 # gg08_rnum=2779
He1+ + NH2CHO -> H2CO1+ + NH + He						RTYPE=2 K1=1.40E-09 K2=-0.5 K3=0.0 # gg08_rnum=2780
He1+ + NH2CN -> NH2 + CN1+ + He							RTYPE=2 K1=4.33E-09 K2=-0.5 K3=0.0 # gg08_rnum=2781
He1+ + NH2CN -> NH21+ + CN + He							RTYPE=2 K1=4.33E-09 K2=-0.5 K3=0.0 # gg08_rnum=2782
He1+ + NH2OH -> NH2 + OH1+ + He							RTYPE=2 K1=1.02E-09 K2=-0.5 K3=0.0 # gg08_rnum=2783
He1+ + NH3 -> NH21+ + H + He							RTYPE=2 K1=2.50E-09 K2=-0.5 K3=0.0 # gg08_rnum=2784
He1+ + NH3 -> NH31+ + He								RTYPE=2 K1=3.80E-10 K2=-0.5 K3=0.0 # gg08_rnum=2785
He1+ + NH3 -> NH1+ + H2 + He							RTYPE=2 K1=2.50E-10 K2=-0.5 K3=0.0 # gg08_rnum=2786
He1+ + NO -> O1+ + N + He								RTYPE=2 K1=1.00E-10 K2=-0.5 K3=0.0 # gg08_rnum=2787
He1+ + NO -> N1+ + O + He								RTYPE=2 K1=6.40E-10 K2=-0.5 K3=0.0 # gg08_rnum=2788
He1+ + NS -> S1+ + N + He								RTYPE=2 K1=4.30E-09 K2=-0.5 K3=0.0 # gg08_rnum=2789
He1+ + NS -> N1+ + S + He								RTYPE=2 K1=4.30E-09 K2=-0.5 K3=0.0 # gg08_rnum=2790
He1+ + O2 -> O21+ + He									RTYPE=2 K1=3.30E-11 K2=0.0 K3=0.0 # gg08_rnum=2791
He1+ + O2 -> O1+ + O + He								RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=2792
He1+ + OCN -> CN1+ + O + He								RTYPE=2 K1=1.50E-09 K2=-0.5 K3=0.0 # gg08_rnum=2793
He1+ + OCN -> O1+ + CN + He								RTYPE=2 K1=1.50E-09 K2=-0.5 K3=0.0 # gg08_rnum=2794
He1+ + OCS -> S1+ + CO + He								RTYPE=2 K1=8.40E-10 K2=-0.5 K3=0.0 # gg08_rnum=2795
He1+ + OCS -> CO1+ + S + He								RTYPE=2 K1=8.40E-10 K2=-0.5 K3=0.0 # gg08_rnum=2796
He1+ + OCS -> CS1+ + O + He								RTYPE=2 K1=8.40E-10 K2=-0.5 K3=0.0 # gg08_rnum=2797
He1+ + OCS -> O1+ + CS + He								RTYPE=2 K1=8.40E-10 K2=-0.5 K3=0.0 # gg08_rnum=2798
He1+ + OH -> O1+ + H + He								RTYPE=2 K1=8.50E-09 K2=-0.5 K3=0.0 # gg08_rnum=2799
He1+ + CHOCHO -> HCO1+ + HCO + He						RTYPE=2 K1=1.20E-09 K2=0.0 K3=0.0 # gg08_rnum=2800
He1+ + HCOCOOH -> COCHO1+ + OH + He						RTYPE=2 K1=4.20E-10 K2=-0.5 K3=0.0 # gg08_rnum=2801
He1+ + HCOCOOH -> COCHO + OH1+ + He						RTYPE=2 K1=4.20E-10 K2=-0.5 K3=0.0 # gg08_rnum=2802
He1+ + HCOCOOH -> HCO1+ + COOH + He						RTYPE=2 K1=4.20E-10 K2=-0.5 K3=0.0 # gg08_rnum=2803
He1+ + HCOCOOH -> HCO + COOH1+ + He						RTYPE=2 K1=4.20E-10 K2=-0.5 K3=0.0 # gg08_rnum=2804
He1+ + P -> P1+ + He									RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=2805
He1+ + PH -> P1+ + H + He								RTYPE=2 K1=3.12E-09 K2=-0.5 K3=0.0 # gg08_rnum=2806
He1+ + PH2 -> P1+ + H2 + He								RTYPE=2 K1=1.03E-09 K2=-0.5 K3=0.0 # gg08_rnum=2807
He1+ + PN -> P1+ + N + He								RTYPE=2 K1=1.32E-08 K2=-0.5 K3=0.0 # gg08_rnum=2808
He1+ + PO -> P1+ + O + He								RTYPE=2 K1=8.99E-09 K2=-0.5 K3=0.0 # gg08_rnum=2809
He1+ + NH2CO -> NH2 + CO1+ + He							RTYPE=2 K1=1.71E-09 K2=-0.5 K3=0.0 # gg08_rnum=2810
He1+ + NH2CO -> NH21+ + CO + He							RTYPE=2 K1=1.71E-09 K2=-0.5 K3=0.0 # gg08_rnum=2811
He1+ + NH2COCHO -> COCHO + NH21+ + He					RTYPE=2 K1=4.18E-10 K2=-0.5 K3=0.0 # gg08_rnum=2812
He1+ + NH2COCHO -> HCO1+ + NH2CO + He					RTYPE=2 K1=4.18E-10 K2=-0.5 K3=0.0 # gg08_rnum=2813
He1+ + NH2COCHO -> HCO + NH2CO1+ + He					RTYPE=2 K1=4.18E-10 K2=-0.5 K3=0.0 # gg08_rnum=2814
He1+ + NH2COCHO -> COCHO1+ + NH2 + He					RTYPE=2 K1=4.18E-10 K2=-0.5 K3=0.0 # gg08_rnum=2815
He1+ + NH2CONH -> NH2CO1+ + NH + He						RTYPE=2 K1=1.62E-09 K2=-0.5 K3=0.0 # gg08_rnum=2816
He1+ + NH2CONH -> NH21+ + HNCO + He						RTYPE=2 K1=1.62E-09 K2=-0.5 K3=0.0 # gg08_rnum=2817
He1+ + NH2CONH -> NH2 + HNCO1+ + He						RTYPE=2 K1=1.62E-09 K2=-0.5 K3=0.0 # gg08_rnum=2818
He1+ + NH2CONH -> NH2CO + NH1+ + He						RTYPE=2 K1=1.62E-09 K2=-0.5 K3=0.0 # gg08_rnum=2819
He1+ + NH2COOH -> NH21+ + COOH + He						RTYPE=2 K1=8.38E-10 K2=-0.5 K3=0.0 # gg08_rnum=2820
He1+ + NH2COOH -> NH2 + COOH1+ + He						RTYPE=2 K1=8.38E-10 K2=-0.5 K3=0.0 # gg08_rnum=2821
He1+ + NH2COOH -> NH2CO1+ + OH + He						RTYPE=2 K1=8.38E-10 K2=-0.5 K3=0.0 # gg08_rnum=2822
He1+ + NH2COOH -> NH2CO + OH1+ + He						RTYPE=2 K1=8.38E-10 K2=-0.5 K3=0.0 # gg08_rnum=2823
He1+ + NH2CONH2 -> NH2CO1+ + NH2 + He					RTYPE=2 K1=6.43E-09 K2=-0.5 K3=0.0 # gg08_rnum=2824
He1+ + NH2NH -> NH21+ + NH + He							RTYPE=2 K1=1.54E-09 K2=-0.5 K3=0.0 # gg08_rnum=2825
He1+ + NH2NH -> NH2 + NH1+ + He							RTYPE=2 K1=1.54E-09 K2=-0.5 K3=0.0 # gg08_rnum=2826
He1+ + NH2NH2 -> NH21+ + NH2 + He						RTYPE=2 K1=3.08E-09 K2=-0.5 K3=0.0 # gg08_rnum=2827
He1+ + NH2OCH3 -> CH3O1+ + NH2 + He						RTYPE=2 K1=1.68E-09 K2=-0.5 K3=0.0 # gg08_rnum=2828
He1+ + NH2OCH3 -> CH3O + NH21+ + He						RTYPE=2 K1=1.68E-09 K2=-0.5 K3=0.0 # gg08_rnum=2829
He1+ + NH2CH2OH -> NH2 + CH2OH1+ + He					RTYPE=2 K1=1.26E-09 K2=-0.5 K3=0.0 # gg08_rnum=2830
He1+ + NH2CH2OH -> NH21+ + CH2OH + He					RTYPE=2 K1=1.26E-09 K2=-0.5 K3=0.0 # gg08_rnum=2831
He1+ + S2 -> S1+ + S + He								RTYPE=2 K1=2.00E-09 K2=0.0 K3=0.0 # gg08_rnum=2832
He1+ + Si -> Si1+ + He									RTYPE=2 K1=3.30E-09 K2=0.0 K3=0.0 # gg08_rnum=2833
He1+ + SiC -> Si1+ + C + He								RTYPE=2 K1=4.09E-09 K2=-0.5 K3=0.0 # gg08_rnum=2834
He1+ + SiC -> C1+ + Si + He								RTYPE=2 K1=4.09E-09 K2=-0.5 K3=0.0 # gg08_rnum=2835
He1+ + SiC2 -> Si1+ + C2 + He							RTYPE=2 K1=4.81E-09 K2=-0.5 K3=0.0 # gg08_rnum=2836
He1+ + SiC2H -> SiC21+ + H + He							RTYPE=2 K1=1.40E-09 K2=-0.5 K3=0.0 # gg08_rnum=2837
He1+ + SiC2H -> Si1+ + C2H + He							RTYPE=2 K1=1.40E-09 K2=-0.5 K3=0.0 # gg08_rnum=2838
He1+ + SiC2H2 -> SiC2H1+ + H + He						RTYPE=2 K1=2.51E-09 K2=-0.5 K3=0.0 # gg08_rnum=2839
He1+ + SiC2H2 -> SiH1+ + C2H + He						RTYPE=2 K1=2.51E-09 K2=-0.5 K3=0.0 # gg08_rnum=2840
He1+ + cSiC3 -> SiC21+ + C + He							RTYPE=2 K1=4.00E-09 K2=-0.5 K3=0.0 # gg08_rnum=2841
He1+ + SiC3H -> SiC31+ + H + He							RTYPE=2 K1=2.99E-09 K2=-0.5 K3=0.0 # gg08_rnum=2842
He1+ + SiC3H -> Si1+ + C3H + He							RTYPE=2 K1=2.99E-09 K2=-0.5 K3=0.0 # gg08_rnum=2843
He1+ + SiC4 -> SiC31+ + C + He							RTYPE=2 K1=1.25E-08 K2=-0.5 K3=0.0 # gg08_rnum=2844
He1+ + SiCH2 -> SiH1+ + CH + He							RTYPE=2 K1=1.52E-09 K2=-0.5 K3=0.0 # gg08_rnum=2845
He1+ + SiCH2 -> SiCH1+ + H + He							RTYPE=2 K1=1.52E-09 K2=-0.5 K3=0.0 # gg08_rnum=2846
He1+ + SiCH3 -> SiCH21+ + H + He						RTYPE=2 K1=1.52E-09 K2=-0.5 K3=0.0 # gg08_rnum=2847
He1+ + SiCH3 -> SiH1+ + CH2 + He						RTYPE=2 K1=1.52E-09 K2=-0.5 K3=0.0 # gg08_rnum=2848
He1+ + SiH -> Si1+ + H + He								RTYPE=2 K1=5.87E-10 K2=-0.5 K3=0.0 # gg08_rnum=2849
He1+ + SiH2 -> Si1+ + H2 + He							RTYPE=2 K1=1.86E-10 K2=-0.5 K3=0.0 # gg08_rnum=2850
He1+ + SiH2 -> SiH1+ + H + He							RTYPE=2 K1=1.86E-10 K2=-0.5 K3=0.0 # gg08_rnum=2851
He1+ + SiH3 -> SiH1+ + H2 + He							RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=2852
He1+ + SiH3 -> SiH21+ + H + He							RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=2853
He1+ + SiH4 -> SiH31+ + H + He							RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=2854
He1+ + SiH4 -> SiH21+ + H2 + He							RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=2855
He1+ + SiN -> Si1+ + N + He								RTYPE=2 K1=1.10E-08 K2=-0.5 K3=0.0 # gg08_rnum=2856
He1+ + SiNC -> CN1+ + Si + He							RTYPE=2 K1=4.08E-09 K2=-0.5 K3=0.0 # gg08_rnum=2857
He1+ + SiO -> Si1+ + O + He								RTYPE=2 K1=7.40E-09 K2=-0.5 K3=0.0 # gg08_rnum=2858
He1+ + SiO -> O1+ + Si + He								RTYPE=2 K1=7.40E-09 K2=-0.5 K3=0.0 # gg08_rnum=2859
He1+ + SiO2 -> Si1+ + O2 + He							RTYPE=2 K1=1.00E-09 K2=-0.5 K3=0.0 # gg08_rnum=2860
He1+ + SiS -> S1+ + Si + He								RTYPE=2 K1=4.10E-09 K2=-0.5 K3=0.0 # gg08_rnum=2861
He1+ + SiS -> Si1+ + S + He								RTYPE=2 K1=4.10E-09 K2=-0.5 K3=0.0 # gg08_rnum=2862
He1+ + SO -> O1+ + S + He								RTYPE=2 K1=3.70E-09 K2=-0.5 K3=0.0 # gg08_rnum=2863
He1+ + SO -> S1+ + O + He								RTYPE=2 K1=3.70E-09 K2=-0.5 K3=0.0 # gg08_rnum=2864
He1+ + SO2 -> S1+ + O2 + He								RTYPE=2 K1=8.15E-10 K2=-0.5 K3=0.0 # gg08_rnum=2865
He1+ + SO2 -> O21+ + S + He								RTYPE=2 K1=8.15E-10 K2=-0.5 K3=0.0 # gg08_rnum=2866
He1+ + SO2 -> SO1+ + O + He								RTYPE=2 K1=8.15E-10 K2=-0.5 K3=0.0 # gg08_rnum=2867
He1+ + SO2 -> SO21+ + He								RTYPE=2 K1=8.15E-10 K2=-0.5 K3=0.0 # gg08_rnum=2868
He1+ + CH3OCO -> CH3O + CO1+ + He						RTYPE=2 K1=1.49E-09 K2=-0.5 K3=0.0 # gg08_rnum=2869
He1+ + CH3OCO -> CH3O1+ + CO + He						RTYPE=2 K1=1.49E-09 K2=-0.5 K3=0.0 # gg08_rnum=2870
He1+ + CH3OCOCHO -> CH3O + COCHO1+ + He					RTYPE=2 K1=4.15E-10 K2=-0.5 K3=0.0 # gg08_rnum=2871
He1+ + CH3OCOCHO -> CH3OCO1+ + HCO + He					RTYPE=2 K1=4.15E-10 K2=-0.5 K3=0.0 # gg08_rnum=2872
He1+ + CH3OCOCHO -> CH3OCO + HCO1+ + He					RTYPE=2 K1=4.15E-10 K2=-0.5 K3=0.0 # gg08_rnum=2873
He1+ + CH3OCOCHO -> CH3O1+ + COCHO + He					RTYPE=2 K1=4.15E-10 K2=-0.5 K3=0.0 # gg08_rnum=2874
He1+ + CH3OCONH -> CH3OCO + NH1+ + He					RTYPE=2 K1=8.35E-10 K2=-0.5 K3=0.0 # gg08_rnum=2875
He1+ + CH3OCONH -> CH3O1+ + HNCO + He					RTYPE=2 K1=8.35E-10 K2=-0.5 K3=0.0 # gg08_rnum=2876
He1+ + CH3OCONH -> CH3O + HNCO1+ + He					RTYPE=2 K1=8.35E-10 K2=-0.5 K3=0.0 # gg08_rnum=2877
He1+ + CH3OCONH -> CH3OCO1+ + NH + He					RTYPE=2 K1=8.35E-10 K2=-0.5 K3=0.0 # gg08_rnum=2878
He1+ + CH3OCOOH -> CH3OCO + OH1+ + He					RTYPE=2 K1=8.33E-10 K2=-0.5 K3=0.0 # gg08_rnum=2879
He1+ + CH3OCOOH -> CH3O1+ + COOH + He					RTYPE=2 K1=8.33E-10 K2=-0.5 K3=0.0 # gg08_rnum=2880
He1+ + CH3OCOOH -> CH3O + COOH1+ + He					RTYPE=2 K1=8.33E-10 K2=-0.5 K3=0.0 # gg08_rnum=2881
He1+ + CH3OCOOH -> CH3OCO1+ + OH + He					RTYPE=2 K1=8.33E-10 K2=-0.5 K3=0.0 # gg08_rnum=2882
He1+ + CH3OCONH2 -> CH3O1+ + NH2CO + He					RTYPE=2 K1=8.33E-10 K2=-0.5 K3=0.0 # gg08_rnum=2883
He1+ + CH3OCONH2 -> CH3O + NH2CO1+ + He					RTYPE=2 K1=8.33E-10 K2=-0.5 K3=0.0 # gg08_rnum=2884
He1+ + CH3OCONH2 -> CH3OCO + NH21+ + He					RTYPE=2 K1=8.33E-10 K2=-0.5 K3=0.0 # gg08_rnum=2885
He1+ + CH3OCONH2 -> CH3OCO1+ + NH2 + He					RTYPE=2 K1=8.33E-10 K2=-0.5 K3=0.0 # gg08_rnum=2886
He1+ + CH3OCOOCH3 -> CH3O + CH3OCO1+ + He				RTYPE=2 K1=1.50E-10 K2=-0.5 K3=0.0 # gg08_rnum=2887
He1+ + CH3OCOOCH3 -> CH3O1+ + CH3OCO + He				RTYPE=2 K1=1.50E-10 K2=-0.5 K3=0.0 # gg08_rnum=2888
He1+ + CH3OCOCH2OH -> CH3O1+ + HOCH2CO + He				RTYPE=2 K1=1.32E-09 K2=-0.5 K3=0.0 # gg08_rnum=2889
He1+ + CH3OCOCH2OH -> CH3O + HOCH2CO1+ + He				RTYPE=2 K1=1.32E-09 K2=-0.5 K3=0.0 # gg08_rnum=2890
He1+ + CH3OCOCH2OH -> CH3OCO1+ + CH2OH + He				RTYPE=2 K1=1.32E-09 K2=-0.5 K3=0.0 # gg08_rnum=2891
He1+ + CH3OCOCH2OH -> CH3OCO + CH2OH1+ + He				RTYPE=2 K1=1.32E-09 K2=-0.5 K3=0.0 # gg08_rnum=2892
He1+ + CH3OOH -> CH3O1+ + OH + He						RTYPE=2 K1=1.46E-09 K2=-0.5 K3=0.0 # gg08_rnum=2893
He1+ + CH3OOH -> CH3O + OH1+ + He						RTYPE=2 K1=1.46E-09 K2=-0.5 K3=0.0 # gg08_rnum=2894
He1+ + CH3OOCH3 -> CH3O1+ + CH3O + He					RTYPE=2 K1=2.50E-09 K2=-0.5 K3=0.0 # gg08_rnum=2895
He1+ + CH3OCH2OH -> CH3O + CH2OH1+ + He					RTYPE=2 K1=1.25E-09 K2=-0.5 K3=0.0 # gg08_rnum=2896
He1+ + CH3OCH2OH -> CH3O1+ + CH2OH + He					RTYPE=2 K1=1.25E-09 K2=-0.5 K3=0.0 # gg08_rnum=2897
He1+ + CH2OHCHO -> CH2OH1+ + HCO + He					RTYPE=2 K1=2.28E-09 K2=-0.5 K3=0.0 # gg08_rnum=2898
He1+ + CH2OHCHO -> CH2OH + HCO1+ + He					RTYPE=2 K1=2.28E-09 K2=-0.5 K3=0.0 # gg08_rnum=2899
He1+ + HOCH2CO -> CH2OH1+ + CO + He						RTYPE=2 K1=2.29E-09 K2=-0.5 K3=0.0 # gg08_rnum=2900
He1+ + HOCH2CO -> CH2OH + CO1+ + He						RTYPE=2 K1=2.29E-09 K2=-0.5 K3=0.0 # gg08_rnum=2901
He1+ + HOCH2COCHO -> HOCH2CO + HCO1+ + He				RTYPE=2 K1=4.15E-10 K2=-0.5 K3=0.0 # gg08_rnum=2902
He1+ + HOCH2COCHO -> CH2OH1+ + COCHO + He				RTYPE=2 K1=4.15E-10 K2=-0.5 K3=0.0 # gg08_rnum=2903
He1+ + HOCH2COCHO -> CH2OH + COCHO1+ + He				RTYPE=2 K1=4.15E-10 K2=-0.5 K3=0.0 # gg08_rnum=2904
He1+ + HOCH2COCHO -> HOCH2CO1+ + HCO + He				RTYPE=2 K1=4.15E-10 K2=-0.5 K3=0.0 # gg08_rnum=2905
He1+ + HNCOCH2OH -> HOCH2CO1+ + NH + He					RTYPE=2 K1=8.35E-10 K2=-0.5 K3=0.0 # gg08_rnum=2906
He1+ + HNCOCH2OH -> HOCH2CO + NH1+ + He					RTYPE=2 K1=8.35E-10 K2=-0.5 K3=0.0 # gg08_rnum=2907
He1+ + HNCOCH2OH -> CH2OH1+ + HNCO + He					RTYPE=2 K1=8.35E-10 K2=-0.5 K3=0.0 # gg08_rnum=2908
He1+ + HNCOCH2OH -> CH2OH + HNCO1+ + He					RTYPE=2 K1=8.35E-10 K2=-0.5 K3=0.0 # gg08_rnum=2909
He1+ + CH2OHCOOH -> CH2OH1+ + COOH + He					RTYPE=2 K1=1.50E-09 K2=-0.5 K3=0.0 # gg08_rnum=2910
He1+ + CH2OHCOOH -> CH2OH + COOH1+ + He					RTYPE=2 K1=1.50E-09 K2=-0.5 K3=0.0 # gg08_rnum=2911
He1+ + NH2COCH2OH -> NH2 + HOCH2CO1+ + He				RTYPE=2 K1=8.33E-10 K2=-0.5 K3=0.0 # gg08_rnum=2912
He1+ + NH2COCH2OH -> NH2CO1+ + CH2OH + He				RTYPE=2 K1=8.33E-10 K2=-0.5 K3=0.0 # gg08_rnum=2913
He1+ + NH2COCH2OH -> NH2CO + CH2OH1+ + He				RTYPE=2 K1=8.33E-10 K2=-0.5 K3=0.0 # gg08_rnum=2914
He1+ + NH2COCH2OH -> NH21+ + HOCH2CO + He				RTYPE=2 K1=8.33E-10 K2=-0.5 K3=0.0 # gg08_rnum=2915
He1+ + HOCH2COCH2OH -> CH2OH + HOCH2CO1+ + He			RTYPE=2 K1=1.50E-09 K2=-0.5 K3=0.0 # gg08_rnum=2916
He1+ + HOCH2COCH2OH -> CH2OH1+ + HOCH2CO + He			RTYPE=2 K1=1.50E-09 K2=-0.5 K3=0.0 # gg08_rnum=2917
He1+ + HOCH2OH -> CH2OH + OH1+ + He						RTYPE=2 K1=2.57E-09 K2=-0.5 K3=0.0 # gg08_rnum=2918
He1+ + HOCH2OH -> CH2OH1+ + OH + He						RTYPE=2 K1=2.57E-09 K2=-0.5 K3=0.0 # gg08_rnum=2919
He1+ + HOCH2CH2OH -> CH2OH1+ + CH2OH + He				RTYPE=2 K1=1.29E-09 K2=0.0 K3=0.0 # gg08_rnum=2920
HeH1+ + H -> H21+ + He									RTYPE=2 K1=9.00E-10 K2=0.0 K3=0.0 # gg08_rnum=2921
HeH1+ + H2 -> H31+ + He									RTYPE=2 K1=1.80E-09 K2=0.0 K3=0.0 # gg08_rnum=2922
HNC1+ + C -> CH1+ + CN									RTYPE=2 K1=1.10E-09 K2=0.0 K3=0.0 # gg08_rnum=2923
HNC1+ + C2 -> C2H1+ + CN								RTYPE=2 K1=8.40E-10 K2=0.0 K3=0.0 # gg08_rnum=2924
HNC1+ + C2H -> HC2H1+ + CN								RTYPE=2 K1=7.90E-10 K2=0.0 K3=0.0 # gg08_rnum=2925
HNC1+ + CH -> CH21+ + CN								RTYPE=2 K1=6.30E-10 K2=0.0 K3=0.0 # gg08_rnum=2926
HNC1+ + CH2 -> CH31+ + CN								RTYPE=2 K1=8.70E-10 K2=0.0 K3=0.0 # gg08_rnum=2927
HNC1+ + H2 -> H2CN1+ + H								RTYPE=2 K1=7.00E-10 K2=0.0 K3=0.0 # gg08_rnum=2928
HNC1+ + H2CO -> CH2OH1+ + CN							RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=2929
HNC1+ + H2O -> H3O1+ + CN								RTYPE=2 K1=8.50E-10 K2=0.0 K3=0.0 # gg08_rnum=2930
HNC1+ + HCN -> H2CN1+ + CN								RTYPE=2 K1=1.60E-09 K2=0.0 K3=0.0 # gg08_rnum=2931
HNC1+ + HCO -> H2CO1+ + CN								RTYPE=2 K1=3.70E-10 K2=0.0 K3=0.0 # gg08_rnum=2932
HNC1+ + HCO -> H2CN1+ + CO								RTYPE=2 K1=3.70E-10 K2=0.0 K3=0.0 # gg08_rnum=2933
HNC1+ + NH -> NH21+ + CN								RTYPE=2 K1=6.50E-10 K2=0.0 K3=0.0 # gg08_rnum=2934
HNC1+ + NH2 -> NH31+ + CN								RTYPE=2 K1=9.00E-10 K2=0.0 K3=0.0 # gg08_rnum=2935
HNC1+ + NH3 -> NH31+ + HNC								RTYPE=2 K1=1.70E-09 K2=0.0 K3=0.0 # gg08_rnum=2936
HNC1+ + NO -> NO1+ + HNC								RTYPE=2 K1=8.10E-10 K2=0.0 K3=0.0 # gg08_rnum=2937
HNC1+ + O2 -> NO1+ + HCO								RTYPE=2 K1=9.00E-11 K2=0.0 K3=0.0 # gg08_rnum=2938
HNC1+ + OH -> H2O1+ + CN								RTYPE=2 K1=6.30E-10 K2=0.0 K3=0.0 # gg08_rnum=2939
HNC1+ + S -> HS1+ + CN									RTYPE=2 K1=5.70E-10 K2=0.0 K3=0.0 # gg08_rnum=2940
HNC1+ + S -> S1+ + HNC									RTYPE=2 K1=5.70E-10 K2=0.0 K3=0.0 # gg08_rnum=2941
HNO1+ + C -> CH1+ + NO									RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=2942
HNO1+ + C2 -> C2H1+ + NO								RTYPE=2 K1=8.20E-10 K2=0.0 K3=0.0 # gg08_rnum=2943
HNO1+ + C2H -> HC2H1+ + NO								RTYPE=2 K1=2.00E-09 K2=-0.5 K3=0.0 # gg08_rnum=2944
HNO1+ + CH -> CH21+ + NO								RTYPE=2 K1=4.40E-09 K2=-0.5 K3=0.0 # gg08_rnum=2945
HNO1+ + CH2 -> CH31+ + NO								RTYPE=2 K1=7.11E-10 K2=-0.5 K3=0.0 # gg08_rnum=2946
HNO1+ + CH4 -> CH51+ + NO								RTYPE=2 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=2947
HNO1+ + CO -> HCO1+ + NO								RTYPE=2 K1=2.70E-10 K2=-0.5 K3=0.0 # gg08_rnum=2948
HNO1+ + CO2 -> HOCO1+ + NO								RTYPE=2 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=2949
HNO1+ + H2CO -> CH2OH1+ + NO							RTYPE=2 K1=2.30E-09 K2=-0.5 K3=0.0 # gg08_rnum=2950
HNO1+ + H2O -> H3O1+ + NO								RTYPE=2 K1=2.10E-09 K2=-0.5 K3=0.0 # gg08_rnum=2951
HNO1+ + HCN -> H2CN1+ + NO								RTYPE=2 K1=7.20E-09 K2=-0.5 K3=0.0 # gg08_rnum=2952
HNO1+ + HCO -> H2CO1+ + NO								RTYPE=2 K1=1.00E-09 K2=-0.5 K3=0.0 # gg08_rnum=2953
HNO1+ + HNC -> H2CN1+ + NO								RTYPE=2 K1=6.52E-09 K2=-0.5 K3=0.0 # gg08_rnum=2954
HNO1+ + NH -> NH21+ + NO								RTYPE=2 K1=3.80E-09 K2=-0.5 K3=0.0 # gg08_rnum=2955
HNO1+ + NH2 -> NH31+ + NO								RTYPE=2 K1=2.20E-09 K2=-0.5 K3=0.0 # gg08_rnum=2956
HNO1+ + NH3 -> NH41+ + NO								RTYPE=2 K1=1.70E-09 K2=-0.5 K3=0.0 # gg08_rnum=2957
HNO1+ + NO -> NO1+ + HNO								RTYPE=2 K1=3.60E-10 K2=-0.5 K3=0.0 # gg08_rnum=2958
HNO1+ + O -> NO21+ + H									RTYPE=2 K1=1.00E-12 K2=0.0 K3=0.0 # gg08_rnum=2959
HNO1+ + OH -> H2O1+ + NO								RTYPE=2 K1=4.60E-09 K2=-0.5 K3=0.0 # gg08_rnum=2960
HNO1+ + S -> HS1+ + NO									RTYPE=2 K1=1.10E-09 K2=0.0 K3=0.0 # gg08_rnum=2961
HNSi1+ + CO -> HCO1+ + SiN								RTYPE=2 K1=2.00E-09 K2=0.0 K3=0.0 # gg08_rnum=2962
HNSi1+ + H2 -> SiNH21+ + H								RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=2963
HNSi1+ + H2O -> H3O1+ + SiN								RTYPE=2 K1=2.00E-09 K2=0.0 K3=0.0 # gg08_rnum=2964
HOC1+ + CO -> HCO1+ + CO								RTYPE=2 K1=4.00E-10 K2=0.0 K3=0.0 # gg08_rnum=2965
HOC1+ + H2 -> HCO1+ + H2								RTYPE=2 K1=1.00E-11 K2=0.0 K3=0.0 # gg08_rnum=2966
HOC1+ + N2 -> N2H1+ + CO								RTYPE=2 K1=2.00E-09 K2=0.0 K3=0.0 # gg08_rnum=2967
HPO1+ + H2O -> H3O1+ + PO								RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=2968
HS1+ + C -> CS1+ + H									RTYPE=2 K1=9.90E-10 K2=0.0 K3=0.0 # gg08_rnum=2969
HS1+ + CH -> CH21+ + S									RTYPE=2 K1=5.80E-10 K2=0.0 K3=0.0 # gg08_rnum=2970
HS1+ + CH4 -> H3CS1+ + H2								RTYPE=2 K1=5.40E-10 K2=0.0 K3=0.0 # gg08_rnum=2971
HS1+ + Fe -> Fe1+ + HS									RTYPE=2 K1=1.60E-09 K2=0.0 K3=0.0 # gg08_rnum=2972
HS1+ + H -> S1+ + H2									RTYPE=2 K1=1.10E-10 K2=0.0 K3=0.0 # gg08_rnum=2973
HS1+ + H2O -> H3O1+ + S									RTYPE=2 K1=7.80E-10 K2=0.0 K3=0.0 # gg08_rnum=2974
HS1+ + H2S -> H3S1+ + S									RTYPE=2 K1=2.90E-10 K2=0.0 K3=0.0 # gg08_rnum=2975
HS1+ + H2S -> S2H1+ + H2								RTYPE=2 K1=2.80E-10 K2=0.0 K3=0.0 # gg08_rnum=2976
HS1+ + HCN -> H2CN1+ + S								RTYPE=2 K1=8.90E-10 K2=0.0 K3=0.0 # gg08_rnum=2977
HS1+ + HNC -> H2CN1+ + S								RTYPE=2 K1=8.60E-10 K2=0.0 K3=0.0 # gg08_rnum=2978
HS1+ + Mg -> Mg1+ + HS									RTYPE=2 K1=2.60E-09 K2=0.0 K3=0.0 # gg08_rnum=2979
HS1+ + N -> NS1+ + H									RTYPE=2 K1=7.40E-10 K2=0.0 K3=0.0 # gg08_rnum=2980
HS1+ + Na -> Na1+ + HS									RTYPE=2 K1=2.20E-09 K2=0.0 K3=0.0 # gg08_rnum=2981
HS1+ + NH3 -> NH31+ + HS								RTYPE=2 K1=8.40E-10 K2=0.0 K3=0.0 # gg08_rnum=2982
HS1+ + NH3 -> NH41+ + S									RTYPE=2 K1=7.70E-10 K2=0.0 K3=0.0 # gg08_rnum=2983
HS1+ + NO -> NO1+ + HS									RTYPE=2 K1=4.50E-10 K2=0.0 K3=0.0 # gg08_rnum=2984
HS1+ + O -> SO1+ + H									RTYPE=2 K1=2.90E-10 K2=0.0 K3=0.0 # gg08_rnum=2985
HS1+ + O -> S1+ + OH									RTYPE=2 K1=2.90E-10 K2=0.0 K3=0.0 # gg08_rnum=2986
HS1+ + S -> S1+ + HS									RTYPE=2 K1=9.70E-10 K2=0.0 K3=0.0 # gg08_rnum=2987
HS1+ + Si -> Si1+ + HS									RTYPE=2 K1=1.40E-09 K2=0.0 K3=0.0 # gg08_rnum=2988
HSO21+ + H2O -> H3O1+ + SO2								RTYPE=2 K1=2.13E-09 K2=0.0 K3=0.0 # gg08_rnum=2989
HSO21+ + NH3 -> NH41+ + SO2								RTYPE=2 K1=2.00E-09 K2=0.0 K3=0.0 # gg08_rnum=2990
Mg1+ + Na -> Na1+ + Mg									RTYPE=2 K1=1.00E-11 K2=0.0 K3=0.0 # gg08_rnum=2991
Mg1+ + NaH -> Na1+ + MgH								RTYPE=2 K1=1.80E-08 K2=-0.5 K3=0.0 # gg08_rnum=2992
N1+ + C2 -> C21+ + N									RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=2993
N1+ + C2H -> C2H1+ + N									RTYPE=2 K1=9.50E-10 K2=0.0 K3=0.0 # gg08_rnum=2994
N1+ + CH -> CN1+ + H									RTYPE=2 K1=3.60E-10 K2=0.0 K3=0.0 # gg08_rnum=2995
N1+ + CH -> CH1+ + N									RTYPE=2 K1=3.60E-10 K2=0.0 K3=0.0 # gg08_rnum=2996
N1+ + CH2 -> CH21+ + N									RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=2997
N1+ + CH3OH -> NO1+ + CH3 + H							RTYPE=2 K1=3.10E-10 K2=0.0 K3=0.0 # gg08_rnum=2998
N1+ + CH3OH -> CH31+ + NO + H							RTYPE=2 K1=1.24E-10 K2=0.0 K3=0.0 # gg08_rnum=2999
N1+ + CH3OH -> H2CO1+ + NH + H							RTYPE=2 K1=9.30E-10 K2=0.0 K3=0.0 # gg08_rnum=3000
N1+ + CH3OH -> CH3O1+ + NH								RTYPE=2 K1=4.96E-10 K2=0.0 K3=0.0 # gg08_rnum=3001
N1+ + CH3OH -> CH3OH1+ + N								RTYPE=2 K1=1.24E-09 K2=0.0 K3=0.0 # gg08_rnum=3002
N1+ + CH4 -> CH31+ + NH									RTYPE=2 K1=4.70E-10 K2=0.0 K3=0.0 # gg08_rnum=3003
N1+ + CH4 -> H2CN1+ + H + H								RTYPE=2 K1=3.80E-10 K2=0.0 K3=0.0 # gg08_rnum=3004
N1+ + CH4 -> H2CN1+ + H2								RTYPE=2 K1=3.80E-10 K2=0.0 K3=0.0 # gg08_rnum=3005
N1+ + CH4 -> CH41+ + N									RTYPE=2 K1=2.80E-11 K2=0.0 K3=0.0 # gg08_rnum=3006
N1+ + CH4 -> HCN1+ + H2 + H								RTYPE=2 K1=5.60E-11 K2=0.0 K3=0.0 # gg08_rnum=3007
N1+ + CH4 -> CH31+ + N + H								RTYPE=2 K1=4.70E-10 K2=0.0 K3=0.0 # gg08_rnum=3008
N1+ + CN -> CN1+ + N									RTYPE=2 K1=1.10E-09 K2=0.0 K3=0.0 # gg08_rnum=3009
N1+ + CO -> CO1+ + N									RTYPE=2 K1=4.90E-10 K2=0.0 K3=0.0 # gg08_rnum=3010
N1+ + CO -> NO1+ + C									RTYPE=2 K1=6.00E-11 K2=0.0 K3=0.0 # gg08_rnum=3011
N1+ + CO2 -> CO1+ + NO									RTYPE=2 K1=2.50E-10 K2=0.0 K3=0.0 # gg08_rnum=3012
N1+ + CO2 -> CO21+ + N									RTYPE=2 K1=1.10E-09 K2=0.0 K3=0.0 # gg08_rnum=3013
N1+ + Fe -> Fe1+ + N									RTYPE=2 K1=1.50E-09 K2=0.0 K3=0.0 # gg08_rnum=3014
N1+ + H2 -> NH1+ + H									RTYPE=2 K1=1.00E-09 K2=0.0 K3=8.50E+01 # gg08_rnum=3015
N1+ + H2CO -> HCO1+ + NH								RTYPE=2 K1=7.30E-10 K2=0.0 K3=0.0 # gg08_rnum=3016
N1+ + H2CO -> H2CO1+ + N								RTYPE=2 K1=1.90E-09 K2=0.0 K3=0.0 # gg08_rnum=3017
N1+ + H2CO -> NO1+ + CH2								RTYPE=2 K1=2.90E-10 K2=0.0 K3=0.0 # gg08_rnum=3018
N1+ + H2O -> H2O1+ + N									RTYPE=2 K1=2.60E-09 K2=0.0 K3=0.0 # gg08_rnum=3019
N1+ + H2S -> HS1+ + NH									RTYPE=2 K1=5.51E-10 K2=0.0 K3=0.0 # gg08_rnum=3020
N1+ + H2S -> H2S1+ + N									RTYPE=2 K1=1.06E-09 K2=0.0 K3=0.0 # gg08_rnum=3021
N1+ + HCN -> HCN1+ + N									RTYPE=2 K1=1.20E-09 K2=0.0 K3=0.0 # gg08_rnum=3022
N1+ + HCO -> NH1+ + CO									RTYPE=2 K1=4.50E-10 K2=0.0 K3=0.0 # gg08_rnum=3023
N1+ + HCO -> HCO1+ + N									RTYPE=2 K1=4.50E-10 K2=0.0 K3=0.0 # gg08_rnum=3024
N1+ + Mg -> Mg1+ + N									RTYPE=2 K1=1.20E-09 K2=0.0 K3=0.0 # gg08_rnum=3025
N1+ + NH -> N21+ + H									RTYPE=2 K1=3.70E-10 K2=0.0 K3=0.0 # gg08_rnum=3026
N1+ + NH -> NH1+ + N									RTYPE=2 K1=3.70E-10 K2=0.0 K3=0.0 # gg08_rnum=3027
N1+ + NH2 -> NH21+ + N									RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=3028
N1+ + NH3 -> NH31+ + N									RTYPE=2 K1=2.00E-09 K2=0.0 K3=0.0 # gg08_rnum=3029
N1+ + NH3 -> N2H1+ + H2									RTYPE=2 K1=2.20E-10 K2=0.0 K3=0.0 # gg08_rnum=3030
N1+ + NH3 -> NH21+ + NH									RTYPE=2 K1=2.20E-10 K2=0.0 K3=0.0 # gg08_rnum=3031
N1+ + NO -> N21+ + O									RTYPE=2 K1=5.00E-11 K2=0.0 K3=0.0 # gg08_rnum=3032
N1+ + NO -> NO1+ + N									RTYPE=2 K1=5.10E-10 K2=0.0 K3=0.0 # gg08_rnum=3033
N1+ + O2 -> NO1+ + O									RTYPE=2 K1=1.70E-10 K2=0.0 K3=0.0 # gg08_rnum=3034
N1+ + O2 -> O21+ + N									RTYPE=2 K1=4.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3035
N1+ + O2 -> O1+ + NO									RTYPE=2 K1=3.60E-11 K2=0.0 K3=0.0 # gg08_rnum=3036
N1+ + OCS -> S1+ + CO + N								RTYPE=2 K1=3.08E-10 K2=0.0 K3=0.0 # gg08_rnum=3037
N1+ + OCS -> CS1+ + NO									RTYPE=2 K1=7.00E-11 K2=0.0 K3=0.0 # gg08_rnum=3038
N1+ + OCS -> OCS1+ + N									RTYPE=2 K1=1.02E-09 K2=0.0 K3=0.0 # gg08_rnum=3039
N1+ + OH -> NO1+ + H									RTYPE=2 K1=3.70E-10 K2=0.0 K3=0.0 # gg08_rnum=3040
N1+ + OH -> OH1+ + N									RTYPE=2 K1=3.70E-10 K2=0.0 K3=0.0 # gg08_rnum=3041
N21+ + C -> C1+ + N2									RTYPE=2 K1=1.10E-10 K2=0.0 K3=0.0 # gg08_rnum=3042
N21+ + C2 -> C21+ + N2									RTYPE=2 K1=8.40E-10 K2=0.0 K3=0.0 # gg08_rnum=3043
N21+ + C2H -> C2H1+ + N2								RTYPE=2 K1=7.90E-10 K2=0.0 K3=0.0 # gg08_rnum=3044
N21+ + CH -> CH1+ + N2									RTYPE=2 K1=6.30E-10 K2=0.0 K3=0.0 # gg08_rnum=3045
N21+ + CH2 -> CH21+ + N2								RTYPE=2 K1=8.70E-10 K2=0.0 K3=0.0 # gg08_rnum=3046
N21+ + CH4 -> CH31+ + N2 + H							RTYPE=2 K1=9.30E-10 K2=0.0 K3=0.0 # gg08_rnum=3047
N21+ + CH4 -> CH21+ + H2 + N2							RTYPE=2 K1=7.00E-11 K2=0.0 K3=0.0 # gg08_rnum=3048
N21+ + CN -> CN1+ + N2									RTYPE=2 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3049
N21+ + CO -> CO1+ + N2									RTYPE=2 K1=7.00E-11 K2=0.0 K3=0.0 # gg08_rnum=3050
N21+ + CO2 -> CO21+ + N2								RTYPE=2 K1=9.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3051
N21+ + Fe -> Fe1+ + N2									RTYPE=2 K1=4.30E-10 K2=0.0 K3=0.0 # gg08_rnum=3052
N21+ + H -> H1+ + N2									RTYPE=2 K1=1.20E-10 K2=0.0 K3=0.0 # gg08_rnum=3053
N21+ + H2 -> N2H1+ + H									RTYPE=2 K1=1.70E-09 K2=0.0 K3=0.0 # gg08_rnum=3054
N21+ + H2CO -> H2CO1+ + N2								RTYPE=2 K1=3.77E-10 K2=0.0 K3=0.0 # gg08_rnum=3055
N21+ + H2CO -> HCO1+ + N2 + H							RTYPE=2 K1=2.50E-09 K2=0.0 K3=0.0 # gg08_rnum=3056
N21+ + H2O -> H2O1+ + N2								RTYPE=2 K1=2.20E-09 K2=0.0 K3=0.0 # gg08_rnum=3057
N21+ + H2O -> N2H1+ + OH								RTYPE=2 K1=2.00E-09 K2=0.0 K3=0.0 # gg08_rnum=3058
N21+ + H2S -> S1+ + N2 + H2								RTYPE=2 K1=2.25E-10 K2=0.0 K3=0.0 # gg08_rnum=3059
N21+ + H2S -> HS1+ + N2 + H								RTYPE=2 K1=1.13E-09 K2=0.0 K3=0.0 # gg08_rnum=3060
N21+ + H2S -> H2S1+ + N2								RTYPE=2 K1=1.50E-10 K2=0.0 K3=0.0 # gg08_rnum=3061
N21+ + HCN -> HCN1+ + N2								RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=3062
N21+ + HCO -> HCO1+ + N2								RTYPE=2 K1=3.70E-10 K2=0.0 K3=0.0 # gg08_rnum=3063
N21+ + HCO -> N2H1+ + CO								RTYPE=2 K1=3.70E-10 K2=0.0 K3=0.0 # gg08_rnum=3064
N21+ + Mg -> Mg1+ + N2									RTYPE=2 K1=7.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3065
N21+ + N -> N1+ + N2									RTYPE=2 K1=1.00E-11 K2=0.0 K3=0.0 # gg08_rnum=3066
N21+ + Na -> Na1+ + N2									RTYPE=2 K1=1.90E-09 K2=0.0 K3=0.0 # gg08_rnum=3067
N21+ + NH -> NH1+ + N2									RTYPE=2 K1=6.50E-10 K2=0.0 K3=0.0 # gg08_rnum=3068
N21+ + NH2 -> NH21+ + N2								RTYPE=2 K1=8.90E-10 K2=0.0 K3=0.0 # gg08_rnum=3069
N21+ + NH3 -> NH31+ + N2								RTYPE=2 K1=1.90E-09 K2=0.0 K3=0.0 # gg08_rnum=3070
N21+ + NO -> NO1+ + N2									RTYPE=2 K1=4.40E-10 K2=0.0 K3=0.0 # gg08_rnum=3071
N21+ + O -> NO1+ + N									RTYPE=2 K1=1.40E-10 K2=0.0 K3=0.0 # gg08_rnum=3072
N21+ + O -> O1+ + N2									RTYPE=2 K1=1.00E-11 K2=0.0 K3=0.0 # gg08_rnum=3073
N21+ + O2 -> O21+ + N2									RTYPE=2 K1=5.00E-11 K2=0.0 K3=0.0 # gg08_rnum=3074
N21+ + OCS -> OCS1+ + N2								RTYPE=2 K1=2.60E-10 K2=0.0 K3=0.0 # gg08_rnum=3075
N21+ + OCS -> S1+ + N2 + CO								RTYPE=2 K1=1.04E-09 K2=0.0 K3=0.0 # gg08_rnum=3076
N21+ + OH -> OH1+ + N2									RTYPE=2 K1=6.30E-10 K2=0.0 K3=0.0 # gg08_rnum=3077
N21+ + S -> S1+ + N2									RTYPE=2 K1=1.10E-09 K2=0.0 K3=0.0 # gg08_rnum=3078
N2H1+ + C -> CH1+ + N2									RTYPE=2 K1=1.10E-09 K2=0.0 K3=0.0 # gg08_rnum=3079
N2H1+ + C2 -> C2H1+ + N2								RTYPE=2 K1=8.30E-10 K2=0.0 K3=0.0 # gg08_rnum=3080
N2H1+ + C2H -> HC2H1+ + N2								RTYPE=2 K1=2.00E-09 K2=-0.5 K3=0.0 # gg08_rnum=3081
N2H1+ + HC2H -> H2C2H1+ + N2							RTYPE=2 K1=1.40E-09 K2=0.0 K3=0.0 # gg08_rnum=3082
N2H1+ + C6H6 -> C6H71+ + N2								RTYPE=2 K1=1.60E-09 K2=0.0 K3=0.0 # gg08_rnum=3083
N2H1+ + CH -> CH21+ + N2								RTYPE=2 K1=4.50E-09 K2=-0.5 K3=0.0 # gg08_rnum=3084
N2H1+ + CH2 -> CH31+ + N2								RTYPE=2 K1=7.19E-10 K2=-0.5 K3=0.0 # gg08_rnum=3085
N2H1+ + CH4 -> CH51+ + N2								RTYPE=2 K1=9.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3086
N2H1+ + CO -> HCO1+ + N2								RTYPE=2 K1=8.80E-10 K2=0.0 K3=0.0 # gg08_rnum=3087
N2H1+ + CO2 -> HOCO1+ + N2								RTYPE=2 K1=9.20E-10 K2=0.0 K3=0.0 # gg08_rnum=3088
N2H1+ + H2CO -> CH2OH1+ + N2							RTYPE=2 K1=2.40E-09 K2=-0.5 K3=0.0 # gg08_rnum=3089
N2H1+ + H2O -> H3O1+ + N2								RTYPE=2 K1=2.20E-09 K2=-0.5 K3=0.0 # gg08_rnum=3090
N2H1+ + HC2NC -> HC2NCH1+ + N2							RTYPE=2 K1=5.00E-09 K2=-0.5 K3=0.0 # gg08_rnum=3091
N2H1+ + HC3N -> HC3NH1+ + N2							RTYPE=2 K1=7.94E-09 K2=-0.5 K3=0.0 # gg08_rnum=3092
N2H1+ + HCN -> H2CN1+ + N2								RTYPE=2 K1=7.30E-09 K2=-0.5 K3=0.0 # gg08_rnum=3093
N2H1+ + C2NCH -> HC2NCH1+ + N2							RTYPE=2 K1=1.75E-08 K2=-0.5 K3=0.0 # gg08_rnum=3094
N2H1+ + HCO -> H2CO1+ + N2								RTYPE=2 K1=1.00E-09 K2=-0.5 K3=0.0 # gg08_rnum=3095
N2H1+ + HNC -> H2CN1+ + N2								RTYPE=2 K1=6.63E-09 K2=-0.5 K3=0.0 # gg08_rnum=3096
N2H1+ + HNC3 -> HC3NH1+ + N2							RTYPE=2 K1=4.50E-09 K2=-0.5 K3=0.0 # gg08_rnum=3097
N2H1+ + NH -> NH21+ + N2								RTYPE=2 K1=3.80E-09 K2=-0.5 K3=0.0 # gg08_rnum=3098
N2H1+ + NH2 -> NH31+ + N2								RTYPE=2 K1=2.20E-09 K2=-0.5 K3=0.0 # gg08_rnum=3099
N2H1+ + NH2CHO -> NH2CHOH1+ + N2						RTYPE=2 K1=8.15E-09 K2=-0.5 K3=0.0 # gg08_rnum=3100
N2H1+ + NH3 -> NH41+ + N2								RTYPE=2 K1=1.70E-09 K2=-0.5 K3=0.0 # gg08_rnum=3101
N2H1+ + OH -> H2O1+ + N2								RTYPE=2 K1=4.70E-09 K2=-0.5 K3=0.0 # gg08_rnum=3102
N2H1+ + S -> HS1+ + N2									RTYPE=2 K1=1.10E-09 K2=0.0 K3=0.0 # gg08_rnum=3103
NaH21+ + H2O -> NaH2O1+ + H2							RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=3104
NH1+ + C -> CH1+ + N									RTYPE=2 K1=1.60E-09 K2=0.0 K3=0.0 # gg08_rnum=3105
NH1+ + C2 -> C2N1+ + H									RTYPE=2 K1=4.90E-10 K2=0.0 K3=0.0 # gg08_rnum=3106
NH1+ + C2 -> HCN1+ + C									RTYPE=2 K1=4.90E-10 K2=0.0 K3=0.0 # gg08_rnum=3107
NH1+ + C2 -> C2H1+ + N									RTYPE=2 K1=4.90E-10 K2=0.0 K3=0.0 # gg08_rnum=3108
NH1+ + C2H -> HC2H1+ + N								RTYPE=2 K1=1.40E-09 K2=0.0 K3=0.0 # gg08_rnum=3109
NH1+ + CH -> CH21+ + N									RTYPE=2 K1=9.90E-10 K2=0.0 K3=0.0 # gg08_rnum=3110
NH1+ + CH2 -> CH31+ + N									RTYPE=2 K1=1.40E-09 K2=0.0 K3=0.0 # gg08_rnum=3111
NH1+ + CN -> HCN1+ + N									RTYPE=2 K1=1.60E-09 K2=0.0 K3=0.0 # gg08_rnum=3112
NH1+ + CO -> OCN1+ + H									RTYPE=2 K1=5.39E-10 K2=0.0 K3=0.0 # gg08_rnum=3113
NH1+ + CO -> HCO1+ + N									RTYPE=2 K1=1.60E-09 K2=0.0 K3=0.0 # gg08_rnum=3114
NH1+ + CO2 -> HNO1+ + CO								RTYPE=2 K1=3.85E-10 K2=0.0 K3=0.0 # gg08_rnum=3115
NH1+ + CO2 -> HOCO1+ + N								RTYPE=2 K1=3.90E-10 K2=0.0 K3=0.0 # gg08_rnum=3116
NH1+ + CO2 -> NO1+ + HCO								RTYPE=2 K1=3.30E-10 K2=0.0 K3=0.0 # gg08_rnum=3117
NH1+ + H2 -> H31+ + N									RTYPE=2 K1=2.25E-10 K2=0.0 K3=0.0 # gg08_rnum=3118
NH1+ + H2 -> NH21+ + H									RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=3119
NH1+ + H2CO -> H2CO1+ + NH								RTYPE=2 K1=9.90E-10 K2=0.0 K3=0.0 # gg08_rnum=3120
NH1+ + H2CO -> CH2OH1+ + N								RTYPE=2 K1=4.95E-10 K2=0.0 K3=0.0 # gg08_rnum=3121
NH1+ + H2CO -> HCO1+ + NH2								RTYPE=2 K1=1.80E-09 K2=0.0 K3=0.0 # gg08_rnum=3122
NH1+ + H2O -> NH21+ + OH								RTYPE=2 K1=8.75E-10 K2=0.0 K3=0.0 # gg08_rnum=3123
NH1+ + H2O -> H3O1+ + N									RTYPE=2 K1=2.10E-09 K2=0.0 K3=0.0 # gg08_rnum=3124
NH1+ + H2O -> NH31+ + O									RTYPE=2 K1=1.75E-10 K2=0.0 K3=0.0 # gg08_rnum=3125
NH1+ + H2O -> H2O1+ + NH								RTYPE=2 K1=1.05E-09 K2=0.0 K3=0.0 # gg08_rnum=3126
NH1+ + H2O -> HNO1+ + H2								RTYPE=2 K1=3.50E-10 K2=0.0 K3=0.0 # gg08_rnum=3127
NH1+ + HCN -> H2CN1+ + N								RTYPE=2 K1=1.80E-09 K2=0.0 K3=0.0 # gg08_rnum=3128
NH1+ + HCO -> H2CO1+ + N								RTYPE=2 K1=1.30E-09 K2=0.0 K3=0.0 # gg08_rnum=3129
NH1+ + HNC -> H2CN1+ + N								RTYPE=2 K1=1.80E-09 K2=0.0 K3=0.0 # gg08_rnum=3130
NH1+ + N -> N21+ + H									RTYPE=2 K1=1.30E-09 K2=0.0 K3=0.0 # gg08_rnum=3131
NH1+ + N2 -> N2H1+ + N									RTYPE=2 K1=1.50E-09 K2=0.0 K3=0.0 # gg08_rnum=3132
NH1+ + NH -> NH21+ + N									RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=3133
NH1+ + NH2 -> NH31+ + N									RTYPE=2 K1=1.50E-09 K2=0.0 K3=0.0 # gg08_rnum=3134
NH1+ + NH3 -> NH41+ + N									RTYPE=2 K1=6.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3135
NH1+ + NH3 -> NH31+ + NH								RTYPE=2 K1=1.80E-09 K2=0.0 K3=0.0 # gg08_rnum=3136
NH1+ + NO -> NO1+ + NH									RTYPE=2 K1=7.10E-10 K2=0.0 K3=0.0 # gg08_rnum=3137
NH1+ + NO -> N2H1+ + O									RTYPE=2 K1=1.78E-10 K2=0.0 K3=0.0 # gg08_rnum=3138
NH1+ + O -> OH1+ + N									RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=3139
NH1+ + O2 -> O21+ + NH									RTYPE=2 K1=4.50E-10 K2=0.0 K3=0.0 # gg08_rnum=3140
NH1+ + O2 -> O2H1+ + N									RTYPE=2 K1=1.60E-10 K2=0.0 K3=0.0 # gg08_rnum=3141
NH1+ + O2 -> NO1+ + OH									RTYPE=2 K1=2.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3142
NH1+ + OH -> H2O1+ + N									RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=3143
NH1+ + S -> S1+ + NH									RTYPE=2 K1=6.90E-10 K2=0.0 K3=0.0 # gg08_rnum=3144
NH1+ + S -> HS1+ + N									RTYPE=2 K1=6.90E-10 K2=0.0 K3=0.0 # gg08_rnum=3145
NH1+ + S -> NS1+ + H									RTYPE=2 K1=6.90E-10 K2=0.0 K3=0.0 # gg08_rnum=3146
NH21+ + C2 -> C2H1+ + NH								RTYPE=2 K1=9.70E-10 K2=0.0 K3=0.0 # gg08_rnum=3147
NH21+ + C2H -> HC2H1+ + NH								RTYPE=2 K1=9.10E-10 K2=0.0 K3=0.0 # gg08_rnum=3148
NH21+ + CH -> CH1+ + NH2								RTYPE=2 K1=3.50E-10 K2=0.0 K3=0.0 # gg08_rnum=3149
NH21+ + CH -> CH21+ + NH								RTYPE=2 K1=3.50E-10 K2=0.0 K3=0.0 # gg08_rnum=3150
NH21+ + CH2 -> CH31+ + NH								RTYPE=2 K1=4.90E-10 K2=0.0 K3=0.0 # gg08_rnum=3151
NH21+ + CH2 -> CH21+ + NH2								RTYPE=2 K1=4.90E-10 K2=0.0 K3=0.0 # gg08_rnum=3152
NH21+ + CN -> H2CN1+ + N								RTYPE=2 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3153
NH21+ + CN -> H2NC1+ + N								RTYPE=2 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3154
NH21+ + H2 -> NH31+ + H									RTYPE=2 K1=1.20E-10 K2=0.0 K3=0.0 # gg08_rnum=3155
NH21+ + H2CO -> NH31+ + HCO								RTYPE=2 K1=5.60E-10 K2=0.0 K3=0.0 # gg08_rnum=3156
NH21+ + H2CO -> CH2OH1+ + NH							RTYPE=2 K1=2.20E-09 K2=0.0 K3=0.0 # gg08_rnum=3157
NH21+ + H2O -> NH41+ + O								RTYPE=2 K1=3.00E-11 K2=0.0 K3=0.0 # gg08_rnum=3158
NH21+ + H2O -> H3O1+ + NH								RTYPE=2 K1=1.60E-09 K2=0.0 K3=0.0 # gg08_rnum=3159
NH21+ + H2O -> NH31+ + OH								RTYPE=2 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3160
NH21+ + H2S -> NH31+ + HS								RTYPE=2 K1=4.50E-10 K2=0.0 K3=0.0 # gg08_rnum=3161
NH21+ + H2S -> NH41+ + S								RTYPE=2 K1=1.80E-10 K2=0.0 K3=0.0 # gg08_rnum=3162
NH21+ + H2S -> HS1+ + NH3								RTYPE=2 K1=1.80E-10 K2=0.0 K3=0.0 # gg08_rnum=3163
NH21+ + H2S -> H2S1+ + NH2								RTYPE=2 K1=3.40E-10 K2=0.0 K3=0.0 # gg08_rnum=3164
NH21+ + H2S -> H3S1+ + NH								RTYPE=2 K1=2.40E-10 K2=0.0 K3=0.0 # gg08_rnum=3165
NH21+ + HCN -> H2CN1+ + NH								RTYPE=2 K1=1.20E-09 K2=0.0 K3=0.0 # gg08_rnum=3166
NH21+ + HCO -> HCO1+ + NH2								RTYPE=2 K1=4.30E-10 K2=0.0 K3=0.0 # gg08_rnum=3167
NH21+ + HNC -> H2CN1+ + NH								RTYPE=2 K1=1.20E-09 K2=0.0 K3=0.0 # gg08_rnum=3168
NH21+ + N -> N2H1+ + H									RTYPE=2 K1=9.10E-11 K2=0.0 K3=0.0 # gg08_rnum=3169
NH21+ + NH -> NH31+ + N									RTYPE=2 K1=7.30E-10 K2=0.0 K3=0.0 # gg08_rnum=3170
NH21+ + NH2 -> NH31+ + NH								RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=3171
NH21+ + NH3 -> NH31+ + NH2								RTYPE=2 K1=1.50E-09 K2=0.0 K3=0.0 # gg08_rnum=3172
NH21+ + NH3 -> NH41+ + NH								RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=3173
NH21+ + NO -> NO1+ + NH2								RTYPE=2 K1=9.40E-10 K2=0.0 K3=0.0 # gg08_rnum=3174
NH21+ + O -> HNO1+ + H									RTYPE=2 K1=7.20E-11 K2=0.0 K3=0.0 # gg08_rnum=3175
NH21+ + O2 -> H2NO1+ + O								RTYPE=2 K1=1.20E-10 K2=0.0 K3=0.0 # gg08_rnum=3176
NH21+ + O2 -> HNO1+ + OH								RTYPE=2 K1=2.10E-11 K2=0.0 K3=0.0 # gg08_rnum=3177
NH21+ + S -> S1+ + NH2									RTYPE=2 K1=4.40E-10 K2=0.0 K3=0.0 # gg08_rnum=3178
NH21+ + S -> HS1+ + NH									RTYPE=2 K1=4.40E-10 K2=0.0 K3=0.0 # gg08_rnum=3179
NH21+ + S -> HNS1+ + H									RTYPE=2 K1=4.40E-10 K2=0.0 K3=0.0 # gg08_rnum=3180
NH31+ + C2 -> HC2H1+ + NH								RTYPE=2 K1=1.00E-11 K2=0.0 K3=0.0 # gg08_rnum=3181
NH31+ + CH -> NH41+ + C									RTYPE=2 K1=4.90E-09 K2=-0.5 K3=0.0 # gg08_rnum=3182
NH31+ + CH2 -> CH31+ + NH2								RTYPE=2 K1=7.97E-10 K2=-0.5 K3=0.0 # gg08_rnum=3183
NH31+ + CH4 -> NH41+ + CH3								RTYPE=2 K1=3.90E-10 K2=0.0 K3=0.0 # gg08_rnum=3184
NH31+ + Fe -> Fe1+ + NH3								RTYPE=2 K1=2.30E-09 K2=0.0 K3=0.0 # gg08_rnum=3185
NH31+ + H2 -> NH41+ + H									RTYPE=2 K1=1.50E-14 K2=-1.50E+00 K3=0.0 # gg08_rnum=3186
NH31+ + H2CO -> NH41+ + HCO								RTYPE=2 K1=2.70E-09 K2=-0.5 K3=0.0 # gg08_rnum=3187
NH31+ + H2O -> NH41+ + OH								RTYPE=2 K1=2.40E-09 K2=-0.5 K3=0.0 # gg08_rnum=3188
NH31+ + H2S -> NH41+ + HS								RTYPE=2 K1=9.60E-10 K2=-0.5 K3=0.0 # gg08_rnum=3189
NH31+ + HCO -> HCO1+ + NH3								RTYPE=2 K1=5.90E-10 K2=-0.5 K3=0.0 # gg08_rnum=3190
NH31+ + HCO -> NH41+ + CO								RTYPE=2 K1=5.90E-10 K2=-0.5 K3=0.0 # gg08_rnum=3191
NH31+ + Mg -> Mg1+ + NH3								RTYPE=2 K1=3.30E-09 K2=0.0 K3=0.0 # gg08_rnum=3192
NH31+ + Na -> Na1+ + NH3								RTYPE=2 K1=3.20E-09 K2=0.0 K3=0.0 # gg08_rnum=3193
NH31+ + NH -> NH41+ + N									RTYPE=2 K1=4.20E-09 K2=-0.5 K3=0.0 # gg08_rnum=3194
NH31+ + NH2 -> NH41+ + NH								RTYPE=2 K1=2.50E-09 K2=-0.5 K3=0.0 # gg08_rnum=3195
NH31+ + NH3 -> NH41+ + NH2								RTYPE=2 K1=2.00E-09 K2=-0.5 K3=0.0 # gg08_rnum=3196
NH31+ + NO -> NO1+ + NH3								RTYPE=2 K1=4.30E-10 K2=-0.5 K3=0.0 # gg08_rnum=3197
NH31+ + O -> HNO1+ + H2									RTYPE=2 K1=1.00E-11 K2=0.0 K3=0.0 # gg08_rnum=3198
NH31+ + O -> H2NO1+ + H									RTYPE=2 K1=1.00E-11 K2=0.0 K3=0.0 # gg08_rnum=3199
NH31+ + OH -> NH41+ + O									RTYPE=2 K1=5.20E-09 K2=-0.5 K3=0.0 # gg08_rnum=3200
NH31+ + Si -> Si1+ + NH3								RTYPE=2 K1=1.90E-09 K2=0.0 K3=0.0 # gg08_rnum=3201
NH41+ + C -> H2CN1+ + H2								RTYPE=2 K1=1.00E-11 K2=0.0 K3=0.0 # gg08_rnum=3202
NH41+ + C -> H2NC1+ + H2								RTYPE=2 K1=1.00E-11 K2=0.0 K3=0.0 # gg08_rnum=3203
NO1+ + Fe -> Fe1+ + NO									RTYPE=2 K1=9.20E-10 K2=0.0 K3=0.0 # gg08_rnum=3204
NO1+ + Mg -> Mg1+ + NO									RTYPE=2 K1=8.10E-10 K2=0.0 K3=0.0 # gg08_rnum=3205
NO1+ + Na -> Na1+ + NO									RTYPE=2 K1=7.00E-11 K2=0.0 K3=0.0 # gg08_rnum=3206
NO1+ + Si -> Si1+ + NO									RTYPE=2 K1=1.60E-09 K2=0.0 K3=0.0 # gg08_rnum=3207
NO21+ + H -> NO1+ + OH									RTYPE=2 K1=1.90E-10 K2=0.0 K3=0.0 # gg08_rnum=3208
NO21+ + H2 -> NO1+ + H2O								RTYPE=2 K1=1.50E-10 K2=0.0 K3=0.0 # gg08_rnum=3209
NS1+ + O -> NO1+ + S									RTYPE=2 K1=6.10E-10 K2=0.0 K3=0.0 # gg08_rnum=3210
O1+ + C2 -> C21+ + O									RTYPE=2 K1=4.80E-10 K2=0.0 K3=0.0 # gg08_rnum=3211
O1+ + C2 -> CO1+ + C									RTYPE=2 K1=4.80E-10 K2=0.0 K3=0.0 # gg08_rnum=3212
O1+ + C2H -> CO1+ + CH									RTYPE=2 K1=4.60E-10 K2=0.0 K3=0.0 # gg08_rnum=3213
O1+ + C2H -> C2H1+ + O									RTYPE=2 K1=4.60E-10 K2=0.0 K3=0.0 # gg08_rnum=3214
O1+ + CH -> CH1+ + O									RTYPE=2 K1=3.50E-10 K2=0.0 K3=0.0 # gg08_rnum=3215
O1+ + CH -> CO1+ + H									RTYPE=2 K1=3.50E-10 K2=0.0 K3=0.0 # gg08_rnum=3216
O1+ + CH2 -> CH21+ + O									RTYPE=2 K1=9.70E-10 K2=0.0 K3=0.0 # gg08_rnum=3217
O1+ + CH3OH -> CH3OH1+ + O								RTYPE=2 K1=4.75E-10 K2=0.0 K3=0.0 # gg08_rnum=3218
O1+ + CH3OH -> H2CO1+ + H2O								RTYPE=2 K1=9.50E-11 K2=0.0 K3=0.0 # gg08_rnum=3219
O1+ + CH3OH -> CH3O1+ + OH								RTYPE=2 K1=1.33E-09 K2=0.0 K3=0.0 # gg08_rnum=3220
O1+ + CH4 -> CH31+ + OH									RTYPE=2 K1=1.10E-10 K2=0.0 K3=0.0 # gg08_rnum=3221
O1+ + CH4 -> CH41+ + O									RTYPE=2 K1=8.90E-10 K2=0.0 K3=0.0 # gg08_rnum=3222
O1+ + CN -> NO1+ + C									RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=3223
O1+ + CO2 -> O21+ + CO									RTYPE=2 K1=1.10E-09 K2=0.0 K3=0.0 # gg08_rnum=3224
O1+ + Fe -> Fe1+ + O									RTYPE=2 K1=2.90E-09 K2=0.0 K3=0.0 # gg08_rnum=3225
O1+ + H -> H1+ + O										RTYPE=2 K1=7.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3226
O1+ + H2 -> OH1+ + H									RTYPE=2 K1=1.60E-09 K2=0.0 K3=0.0 # gg08_rnum=3227
O1+ + H2CO -> HCO1+ + OH								RTYPE=2 K1=1.40E-09 K2=0.0 K3=0.0 # gg08_rnum=3228
O1+ + H2CO -> H2CO1+ + O								RTYPE=2 K1=2.10E-09 K2=0.0 K3=0.0 # gg08_rnum=3229
O1+ + H2O -> H2O1+ + O									RTYPE=2 K1=3.20E-09 K2=0.0 K3=0.0 # gg08_rnum=3230
O1+ + H2S -> HS1+ + OH									RTYPE=2 K1=4.20E-10 K2=0.0 K3=0.0 # gg08_rnum=3231
O1+ + H2S -> H2S1+ + O									RTYPE=2 K1=1.80E-09 K2=0.0 K3=0.0 # gg08_rnum=3232
O1+ + H2S -> H2O + S1+									RTYPE=2 K1=2.20E-10 K2=0.0 K3=0.0 # gg08_rnum=3233
O1+ + HCN -> CO1+ + NH									RTYPE=2 K1=1.20E-09 K2=0.0 K3=0.0 # gg08_rnum=3234
O1+ + HCN -> NO1+ + CH									RTYPE=2 K1=1.20E-09 K2=0.0 K3=0.0 # gg08_rnum=3235
O1+ + HCN -> HCO1+ + N									RTYPE=2 K1=1.20E-09 K2=0.0 K3=0.0 # gg08_rnum=3236
O1+ + HCO -> HCO1+ + O									RTYPE=2 K1=4.30E-10 K2=0.0 K3=0.0 # gg08_rnum=3237
O1+ + HCO -> OH1+ + CO									RTYPE=2 K1=4.30E-10 K2=0.0 K3=0.0 # gg08_rnum=3238
O1+ + N2 -> NO1+ + N									RTYPE=2 K1=1.20E-12 K2=0.0 K3=0.0 # gg08_rnum=3239
O1+ + N2O -> NO1+ + NO									RTYPE=2 K1=6.30E-10 K2=0.0 K3=0.0 # gg08_rnum=3240
O1+ + NH -> NO1+ + H									RTYPE=2 K1=3.60E-10 K2=0.0 K3=0.0 # gg08_rnum=3241
O1+ + NH -> NH1+ + O									RTYPE=2 K1=3.60E-10 K2=0.0 K3=0.0 # gg08_rnum=3242
O1+ + NH2 -> NH21+ + O									RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=3243
O1+ + NH3 -> NH31+ + O									RTYPE=2 K1=1.20E-09 K2=0.0 K3=0.0 # gg08_rnum=3244
O1+ + NO -> NO1+ + O									RTYPE=2 K1=1.70E-12 K2=0.0 K3=0.0 # gg08_rnum=3245
O1+ + NO2 -> NO21+ + O									RTYPE=2 K1=1.60E-09 K2=0.0 K3=0.0 # gg08_rnum=3246
O1+ + O2 -> O21+ + O									RTYPE=2 K1=3.00E-11 K2=0.0 K3=0.0 # gg08_rnum=3247
O1+ + OCS -> S1+ + CO2									RTYPE=2 K1=2.00E-11 K2=0.0 K3=0.0 # gg08_rnum=3248
O1+ + OCS -> OCS1+ + O									RTYPE=2 K1=6.50E-10 K2=0.0 K3=0.0 # gg08_rnum=3249
O1+ + OH -> OH1+ + O									RTYPE=2 K1=3.60E-10 K2=0.0 K3=0.0 # gg08_rnum=3250
O1+ + OH -> O21+ + H									RTYPE=2 K1=3.60E-10 K2=0.0 K3=0.0 # gg08_rnum=3251
O1+ + SO2 -> O21+ + SO									RTYPE=2 K1=8.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3252
O21+ + C -> C1+ + O2									RTYPE=2 K1=5.20E-11 K2=0.0 K3=0.0 # gg08_rnum=3253
O21+ + C -> CO1+ + O									RTYPE=2 K1=5.20E-11 K2=0.0 K3=0.0 # gg08_rnum=3254
O21+ + C2 -> CO1+ + CO									RTYPE=2 K1=4.10E-10 K2=0.0 K3=0.0 # gg08_rnum=3255
O21+ + CH -> CH1+ + O2									RTYPE=2 K1=3.10E-10 K2=0.0 K3=0.0 # gg08_rnum=3256
O21+ + CH -> HCO1+ + O									RTYPE=2 K1=3.10E-10 K2=0.0 K3=0.0 # gg08_rnum=3257
O21+ + CH2 -> CH21+ + O2								RTYPE=2 K1=4.30E-10 K2=0.0 K3=0.0 # gg08_rnum=3258
O21+ + CH2 -> H2CO1+ + O								RTYPE=2 K1=4.30E-10 K2=0.0 K3=0.0 # gg08_rnum=3259
O21+ + CH3OH -> CH3O1+ + O2 + H							RTYPE=2 K1=5.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3260
O21+ + CH3OH -> CH3OH1+ + O2							RTYPE=2 K1=5.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3261
O21+ + CH4 -> HCOOH21+ + H								RTYPE=2 K1=3.80E-12 K2=-1.80E+00 K3=0.0 # gg08_rnum=3262
O21+ + Fe -> Fe1+ + O2									RTYPE=2 K1=1.10E-09 K2=0.0 K3=0.0 # gg08_rnum=3263
O21+ + H2CO -> H2CO1+ + O2								RTYPE=2 K1=9.90E-10 K2=0.0 K3=0.0 # gg08_rnum=3264
O21+ + H2CO -> HCO1+ + O2 + H							RTYPE=2 K1=2.30E-10 K2=0.0 K3=0.0 # gg08_rnum=3265
O21+ + H2S -> H2S1+ + O2								RTYPE=2 K1=1.40E-09 K2=0.0 K3=0.0 # gg08_rnum=3266
O21+ + HCO -> HCO1+ + O2								RTYPE=2 K1=3.60E-10 K2=0.0 K3=0.0 # gg08_rnum=3267
O21+ + HCO -> O2H1+ + CO								RTYPE=2 K1=3.60E-10 K2=0.0 K3=0.0 # gg08_rnum=3268
O21+ + Mg -> Mg1+ + O2									RTYPE=2 K1=1.20E-09 K2=0.0 K3=0.0 # gg08_rnum=3269
O21+ + N -> NO1+ + O									RTYPE=2 K1=1.80E-10 K2=0.0 K3=0.0 # gg08_rnum=3270
O21+ + Na -> Na1+ + O2									RTYPE=2 K1=7.10E-10 K2=0.0 K3=0.0 # gg08_rnum=3271
O21+ + NH -> NO21+ + H									RTYPE=2 K1=3.20E-10 K2=0.0 K3=0.0 # gg08_rnum=3272
O21+ + NH -> HNO1+ + O									RTYPE=2 K1=3.20E-10 K2=0.0 K3=0.0 # gg08_rnum=3273
O21+ + NH2 -> NH21+ + O2								RTYPE=2 K1=8.70E-10 K2=0.0 K3=0.0 # gg08_rnum=3274
O21+ + NH3 -> NH31+ + O2								RTYPE=2 K1=2.40E-09 K2=0.0 K3=0.0 # gg08_rnum=3275
O21+ + NO -> NO1+ + O2									RTYPE=2 K1=4.50E-10 K2=0.0 K3=0.0 # gg08_rnum=3276
O21+ + NO2 -> NO21+ + O2								RTYPE=2 K1=6.60E-10 K2=0.0 K3=0.0 # gg08_rnum=3277
O21+ + S -> SO1+ + O									RTYPE=2 K1=5.40E-10 K2=0.0 K3=0.0 # gg08_rnum=3278
O21+ + S -> S1+ + O2									RTYPE=2 K1=5.40E-10 K2=0.0 K3=0.0 # gg08_rnum=3279
O21+ + Si -> Si1+ + O2									RTYPE=2 K1=1.60E-09 K2=0.0 K3=0.0 # gg08_rnum=3280
O2H1+ + C -> CH1+ + O2									RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=3281
O2H1+ + C2 -> C2H1+ + O2								RTYPE=2 K1=8.10E-10 K2=0.0 K3=0.0 # gg08_rnum=3282
O2H1+ + C2H -> HC2H1+ + O2								RTYPE=2 K1=7.60E-10 K2=0.0 K3=0.0 # gg08_rnum=3283
O2H1+ + CH -> CH21+ + O2								RTYPE=2 K1=6.20E-10 K2=0.0 K3=0.0 # gg08_rnum=3284
O2H1+ + CH2 -> CH31+ + O2								RTYPE=2 K1=8.50E-10 K2=0.0 K3=0.0 # gg08_rnum=3285
O2H1+ + CN -> HCN1+ + O2								RTYPE=2 K1=8.60E-10 K2=0.0 K3=0.0 # gg08_rnum=3286
O2H1+ + CO -> HCO1+ + O2								RTYPE=2 K1=8.40E-10 K2=0.0 K3=0.0 # gg08_rnum=3287
O2H1+ + CO2 -> HOCO1+ + O2								RTYPE=2 K1=1.10E-09 K2=0.0 K3=0.0 # gg08_rnum=3288
O2H1+ + H2 -> H31+ + O2									RTYPE=2 K1=3.20E-10 K2=0.0 K3=0.0 # gg08_rnum=3289
O2H1+ + H2CO -> CH2OH1+ + O2							RTYPE=2 K1=9.80E-10 K2=0.0 K3=0.0 # gg08_rnum=3290
O2H1+ + H2O -> H3O1+ + O2								RTYPE=2 K1=8.20E-10 K2=0.0 K3=0.0 # gg08_rnum=3291
O2H1+ + HCN -> H2CN1+ + O2								RTYPE=2 K1=9.70E-10 K2=0.0 K3=0.0 # gg08_rnum=3292
O2H1+ + HCO -> H2CO1+ + O2								RTYPE=2 K1=7.10E-10 K2=0.0 K3=0.0 # gg08_rnum=3293
O2H1+ + HNC -> H2CN1+ + O2								RTYPE=2 K1=9.70E-10 K2=0.0 K3=0.0 # gg08_rnum=3294
O2H1+ + N -> NO21+ + H									RTYPE=2 K1=1.00E-12 K2=0.0 K3=0.0 # gg08_rnum=3295
O2H1+ + N2 -> N2H1+ + O2								RTYPE=2 K1=7.90E-10 K2=0.0 K3=0.0 # gg08_rnum=3296
O2H1+ + NH -> NH21+ + O2								RTYPE=2 K1=6.30E-10 K2=0.0 K3=0.0 # gg08_rnum=3297
O2H1+ + NH2 -> NH31+ + O2								RTYPE=2 K1=8.70E-10 K2=0.0 K3=0.0 # gg08_rnum=3298
O2H1+ + NH3 -> NH41+ + O2								RTYPE=2 K1=2.00E-09 K2=0.0 K3=0.0 # gg08_rnum=3299
O2H1+ + NO -> HNO1+ + O2								RTYPE=2 K1=7.70E-10 K2=0.0 K3=0.0 # gg08_rnum=3300
O2H1+ + O -> OH1+ + O2									RTYPE=2 K1=6.20E-10 K2=0.0 K3=0.0 # gg08_rnum=3301
O2H1+ + OH -> H2O1+ + O2								RTYPE=2 K1=6.10E-10 K2=0.0 K3=0.0 # gg08_rnum=3302
O2H1+ + S -> HS1+ + O2									RTYPE=2 K1=1.10E-09 K2=0.0 K3=0.0 # gg08_rnum=3303
OCS1+ + NH3 -> NH31+ + OCS								RTYPE=2 K1=1.60E-09 K2=-0.5 K3=0.0 # gg08_rnum=3304
OH1+ + C -> CH1+ + O									RTYPE=2 K1=1.20E-09 K2=0.0 K3=0.0 # gg08_rnum=3305
OH1+ + C2 -> C21+ + OH									RTYPE=2 K1=4.80E-10 K2=0.0 K3=0.0 # gg08_rnum=3306
OH1+ + C2 -> C2H1+ + O									RTYPE=2 K1=4.80E-10 K2=0.0 K3=0.0 # gg08_rnum=3307
OH1+ + C2H -> C2H1+ + OH								RTYPE=2 K1=4.50E-10 K2=0.0 K3=0.0 # gg08_rnum=3308
OH1+ + C2H -> HC2H1+ + O								RTYPE=2 K1=4.50E-10 K2=0.0 K3=0.0 # gg08_rnum=3309
OH1+ + CH -> CH1+ + OH									RTYPE=2 K1=3.50E-10 K2=0.0 K3=0.0 # gg08_rnum=3310
OH1+ + CH -> CH21+ + O									RTYPE=2 K1=3.50E-10 K2=0.0 K3=0.0 # gg08_rnum=3311
OH1+ + CH2 -> CH21+ + OH								RTYPE=2 K1=4.80E-10 K2=0.0 K3=0.0 # gg08_rnum=3312
OH1+ + CH2 -> CH31+ + O									RTYPE=2 K1=4.80E-10 K2=0.0 K3=0.0 # gg08_rnum=3313
OH1+ + CH4 -> CH51+ + O									RTYPE=2 K1=1.95E-10 K2=0.0 K3=0.0 # gg08_rnum=3314
OH1+ + CH4 -> H3O1+ + CH2								RTYPE=2 K1=1.40E-09 K2=0.0 K3=0.0 # gg08_rnum=3315
OH1+ + CN -> HCN1+ + O									RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=3316
OH1+ + CO -> HCO1+ + O									RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=3317
OH1+ + CO2 -> HOCO1+ + O								RTYPE=2 K1=1.40E-09 K2=0.0 K3=0.0 # gg08_rnum=3318
OH1+ + H2 -> H2O1+ + H									RTYPE=2 K1=1.10E-09 K2=0.0 K3=0.0 # gg08_rnum=3319
OH1+ + H2CO -> CH2OH1+ + O								RTYPE=2 K1=1.10E-09 K2=0.0 K3=0.0 # gg08_rnum=3320
OH1+ + H2CO -> H2CO1+ + OH								RTYPE=2 K1=7.40E-10 K2=0.0 K3=0.0 # gg08_rnum=3321
OH1+ + H2O -> H3O1+ + O									RTYPE=2 K1=1.30E-09 K2=0.0 K3=0.0 # gg08_rnum=3322
OH1+ + H2O -> H2O1+ + OH								RTYPE=2 K1=1.50E-09 K2=0.0 K3=0.0 # gg08_rnum=3323
OH1+ + H2S -> H2S1+ + OH								RTYPE=2 K1=1.20E-09 K2=0.0 K3=0.0 # gg08_rnum=3324
OH1+ + H2S -> H3S1+ + O									RTYPE=2 K1=8.20E-10 K2=0.0 K3=0.0 # gg08_rnum=3325
OH1+ + HCN -> H2CN1+ + O								RTYPE=2 K1=1.20E-09 K2=0.0 K3=0.0 # gg08_rnum=3326
OH1+ + HCO -> H2O1+ + CO								RTYPE=2 K1=2.80E-10 K2=0.0 K3=0.0 # gg08_rnum=3327
OH1+ + HCO -> HCO1+ + OH								RTYPE=2 K1=2.80E-10 K2=0.0 K3=0.0 # gg08_rnum=3328
OH1+ + HCO -> H2CO1+ + O								RTYPE=2 K1=2.80E-10 K2=0.0 K3=0.0 # gg08_rnum=3329
OH1+ + HNC -> H2CN1+ + O								RTYPE=2 K1=1.20E-09 K2=0.0 K3=0.0 # gg08_rnum=3330
OH1+ + N -> NO1+ + H									RTYPE=2 K1=8.90E-10 K2=0.0 K3=0.0 # gg08_rnum=3331
OH1+ + N2 -> N2H1+ + O									RTYPE=2 K1=3.60E-10 K2=0.0 K3=0.0 # gg08_rnum=3332
OH1+ + NH -> NH21+ + O									RTYPE=2 K1=3.60E-10 K2=0.0 K3=0.0 # gg08_rnum=3333
OH1+ + NH2 -> NH31+ + O									RTYPE=2 K1=5.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3334
OH1+ + NH2 -> NH21+ + OH								RTYPE=2 K1=5.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3335
OH1+ + NH3 -> NH41+ + O									RTYPE=2 K1=1.20E-09 K2=0.0 K3=0.0 # gg08_rnum=3336
OH1+ + NH3 -> NH31+ + OH								RTYPE=2 K1=1.20E-09 K2=0.0 K3=0.0 # gg08_rnum=3337
OH1+ + NO -> NO1+ + OH									RTYPE=2 K1=3.60E-10 K2=0.0 K3=0.0 # gg08_rnum=3338
OH1+ + NO -> HNO1+ + O									RTYPE=2 K1=6.10E-10 K2=0.0 K3=0.0 # gg08_rnum=3339
OH1+ + O -> O21+ + H									RTYPE=2 K1=7.10E-10 K2=0.0 K3=0.0 # gg08_rnum=3340
OH1+ + O2 -> O21+ + OH									RTYPE=2 K1=5.90E-10 K2=0.0 K3=0.0 # gg08_rnum=3341
OH1+ + OH -> H2O1+ + O									RTYPE=2 K1=7.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3342
OH1+ + S -> HS1+ + O									RTYPE=2 K1=4.30E-10 K2=0.0 K3=0.0 # gg08_rnum=3343
OH1+ + S -> SO1+ + H									RTYPE=2 K1=4.30E-10 K2=0.0 K3=0.0 # gg08_rnum=3344
OH1+ + S -> S1+ + OH									RTYPE=2 K1=4.30E-10 K2=0.0 K3=0.0 # gg08_rnum=3345
OH1+ + Si -> SiH1+ + O									RTYPE=2 K1=1.90E-09 K2=0.0 K3=0.0 # gg08_rnum=3346
OH1+ + SiC -> SiCH1+ + O								RTYPE=2 K1=4.50E-09 K2=0.0 K3=0.0 # gg08_rnum=3347
OH1+ + SiH -> SiH21+ + O								RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=3348
OH1+ + SiO -> HSiO1+ + O								RTYPE=2 K1=9.40E-10 K2=0.0 K3=0.0 # gg08_rnum=3349
P1+ + HC2H -> HCCP1+ + H								RTYPE=2 K1=1.24E-09 K2=0.0 K3=0.0 # gg08_rnum=3350
P1+ + HC2H -> PC2H21+									RTYPE=2 K1=6.00E-11 K2=0.0 K3=0.0 # gg08_rnum=3351
P1+ + H2C2H2 -> PC2H21+ + H2							RTYPE=2 K1=1.20E-09 K2=0.0 K3=0.0 # gg08_rnum=3352
P1+ + CH4 -> PCH21+ + H2								RTYPE=2 K1=9.60E-10 K2=0.0 K3=0.0 # gg08_rnum=3353
P1+ + H2O -> HPO1+ + H									RTYPE=2 K1=1.07E-09 K2=-0.5 K3=0.0 # gg08_rnum=3354
P1+ + H2O -> PO1+ + H2									RTYPE=2 K1=1.07E-09 K2=-0.5 K3=0.0 # gg08_rnum=3355
P1+ + NH3 -> PNH21+ + H									RTYPE=2 K1=8.60E-10 K2=-0.5 K3=0.0 # gg08_rnum=3356
P1+ + NH3 -> NH31+ + P									RTYPE=2 K1=8.60E-10 K2=-0.5 K3=0.0 # gg08_rnum=3357
P1+ + O2 -> PO1+ + O									RTYPE=2 K1=4.90E-10 K2=0.0 K3=0.0 # gg08_rnum=3358
P1+ + OH -> PO1+ + H									RTYPE=2 K1=4.60E-09 K2=-0.5 K3=0.0 # gg08_rnum=3359
P1+ + Si -> P + Si1+									RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=3360
HCCP1+ + HC2H -> PC4H21+ + H							RTYPE=2 K1=9.10E-10 K2=0.0 K3=0.0 # gg08_rnum=3361
PH1+ + HC2H -> PC2H21+ + H								RTYPE=2 K1=1.30E-09 K2=0.0 K3=0.0 # gg08_rnum=3362
PH1+ + H2C2H2 -> PCH21+ + CH3							RTYPE=2 K1=3.60E-10 K2=0.0 K3=0.0 # gg08_rnum=3363
PH1+ + H2C2H2 -> PC2H31+ + H2							RTYPE=2 K1=8.40E-10 K2=0.0 K3=0.0 # gg08_rnum=3364
PH1+ + CH4 -> CH2PH1+ + H2								RTYPE=2 K1=1.10E-09 K2=0.0 K3=0.0 # gg08_rnum=3365
PH1+ + CH4 -> CH2PH21+ + H								RTYPE=2 K1=5.50E-11 K2=0.0 K3=0.0 # gg08_rnum=3366
PH1+ + H2O -> HPO1+ + H2								RTYPE=2 K1=7.03E-10 K2=-0.5 K3=0.0 # gg08_rnum=3367
PH1+ + H2O -> H3O1+ + P									RTYPE=2 K1=7.03E-10 K2=-0.5 K3=0.0 # gg08_rnum=3368
PH1+ + H2O -> H2PO1+ + H								RTYPE=2 K1=7.03E-10 K2=-0.5 K3=0.0 # gg08_rnum=3369
PH1+ + HCN -> H2CN1+ + P								RTYPE=2 K1=7.15E-09 K2=-0.5 K3=0.0 # gg08_rnum=3370
PH1+ + NH3 -> PNH31+ + H								RTYPE=2 K1=5.70E-10 K2=-0.5 K3=0.0 # gg08_rnum=3371
PH1+ + NH3 -> PNH21+ + H2								RTYPE=2 K1=5.70E-10 K2=-0.5 K3=0.0 # gg08_rnum=3372
PH1+ + NH3 -> NH41+ + P									RTYPE=2 K1=5.70E-10 K2=-0.5 K3=0.0 # gg08_rnum=3373
PH1+ + O -> PO1+ + H									RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=3374
PH1+ + O2 -> PO1+ + OH									RTYPE=2 K1=4.90E-10 K2=0.0 K3=0.0 # gg08_rnum=3375
PH21+ + HC2H -> PC2H21+ + H2							RTYPE=2 K1=1.40E-09 K2=0.0 K3=0.0 # gg08_rnum=3376
PH21+ + H2C2H2 -> PCH21+ + CH4							RTYPE=2 K1=1.40E-10 K2=0.0 K3=0.0 # gg08_rnum=3377
PH21+ + H2C2H2 -> PC2H41+ + H2							RTYPE=2 K1=1.10E-09 K2=0.0 K3=0.0 # gg08_rnum=3378
PH31+ + HC2H -> PC2H31+ + H2							RTYPE=2 K1=5.80E-10 K2=0.0 K3=0.0 # gg08_rnum=3379
S1+ + C2 -> CS1+ + C									RTYPE=2 K1=8.10E-10 K2=0.0 K3=0.0 # gg08_rnum=3380
S1+ + C2H -> C2S1+ + H									RTYPE=2 K1=1.96E-09 K2=-0.5 K3=0.0 # gg08_rnum=3381
S1+ + HC2H -> HC2S1+ + H								RTYPE=2 K1=9.50E-10 K2=0.0 K3=0.0 # gg08_rnum=3382
S1+ + H2C2H -> HC2S1+ + H2								RTYPE=2 K1=1.52E-09 K2=-0.5 K3=0.0 # gg08_rnum=3383
S1+ + C3H -> C3S1+ + H									RTYPE=2 K1=6.87E-09 K2=-0.5 K3=0.0 # gg08_rnum=3384
S1+ + C3H2 -> HC3S1+ + H								RTYPE=2 K1=3.16E-09 K2=-0.5 K3=0.0 # gg08_rnum=3385
S1+ + C4H -> C4S1+ + H									RTYPE=2 K1=1.88E-09 K2=-0.5 K3=0.0 # gg08_rnum=3386
S1+ + HC4H -> C3H21+ + CS								RTYPE=2 K1=2.40E-10 K2=0.0 K3=0.0 # gg08_rnum=3387
S1+ + HC4H -> C4H1+ + HS								RTYPE=2 K1=1.60E-10 K2=0.0 K3=0.0 # gg08_rnum=3388
S1+ + HC4H -> HC4H1+ + S								RTYPE=2 K1=7.20E-10 K2=0.0 K3=0.0 # gg08_rnum=3389
S1+ + HC4H -> HC4S1+ + H								RTYPE=2 K1=4.80E-10 K2=0.0 K3=0.0 # gg08_rnum=3390
S1+ + CH -> CS1+ + H									RTYPE=2 K1=4.40E-09 K2=-0.5 K3=0.0 # gg08_rnum=3391
S1+ + CH2 -> HCS1+ + H									RTYPE=2 K1=7.08E-10 K2=-0.5 K3=0.0 # gg08_rnum=3392
S1+ + CH3 -> H2CS1+ + H									RTYPE=2 K1=1.00E-11 K2=0.0 K3=0.0 # gg08_rnum=3393
S1+ + CH4 -> H3CS1+ + H									RTYPE=2 K1=1.40E-10 K2=0.0 K3=0.0 # gg08_rnum=3394
S1+ + Fe -> Fe1+ + S									RTYPE=2 K1=1.80E-10 K2=0.0 K3=0.0 # gg08_rnum=3395
S1+ + H2CO -> HCO1+ + HS								RTYPE=2 K1=1.10E-09 K2=-0.5 K3=0.0 # gg08_rnum=3396
S1+ + H2CO -> H2S1+ + CO								RTYPE=2 K1=1.10E-09 K2=-0.5 K3=0.0 # gg08_rnum=3397
S1+ + H2S -> S21+ + H2									RTYPE=2 K1=6.40E-10 K2=-0.5 K3=0.0 # gg08_rnum=3398
S1+ + H2S -> S2H1+ + H									RTYPE=2 K1=2.00E-10 K2=-0.5 K3=0.0 # gg08_rnum=3399
S1+ + H2S -> H2S1+ + S									RTYPE=2 K1=4.40E-11 K2=-0.5 K3=0.0 # gg08_rnum=3400
S1+ + HCO -> HCO1+ + S									RTYPE=2 K1=5.00E-10 K2=-0.5 K3=0.0 # gg08_rnum=3401
S1+ + HCO -> HS1+ + CO									RTYPE=2 K1=5.00E-10 K2=-0.5 K3=0.0 # gg08_rnum=3402
S1+ + Mg -> Mg1+ + S									RTYPE=2 K1=2.80E-10 K2=0.0 K3=0.0 # gg08_rnum=3403
S1+ + Na -> Na1+ + S									RTYPE=2 K1=2.60E-10 K2=0.0 K3=0.0 # gg08_rnum=3404
S1+ + NH -> NS1+ + H									RTYPE=2 K1=3.70E-09 K2=-0.5 K3=0.0 # gg08_rnum=3405
S1+ + NH3 -> NH31+ + S									RTYPE=2 K1=1.60E-09 K2=-0.5 K3=0.0 # gg08_rnum=3406
S1+ + NO -> NO1+ + S									RTYPE=2 K1=3.60E-10 K2=-0.5 K3=0.0 # gg08_rnum=3407
S1+ + O2 -> SO1+ + O									RTYPE=2 K1=2.00E-11 K2=0.0 K3=0.0 # gg08_rnum=3408
S1+ + OCS -> S21+ + CO									RTYPE=2 K1=9.10E-10 K2=-0.5 K3=0.0 # gg08_rnum=3409
S1+ + OH -> SO1+ + H									RTYPE=2 K1=4.60E-09 K2=-0.5 K3=0.0 # gg08_rnum=3410
S1+ + Si -> Si1+ + S									RTYPE=2 K1=1.60E-09 K2=0.0 K3=0.0 # gg08_rnum=3411
S1+ + SiC -> SiC1+ + S									RTYPE=2 K1=3.70E-09 K2=-0.5 K3=0.0 # gg08_rnum=3412
S1+ + SiH -> SiS1+ + H									RTYPE=2 K1=1.41E-10 K2=-0.5 K3=0.0 # gg08_rnum=3413
S1+ + SiH -> SiH1+ + S									RTYPE=2 K1=1.41E-10 K2=-0.5 K3=0.0 # gg08_rnum=3414
S1+ + SiS -> SiS1+ + S									RTYPE=2 K1=3.50E-09 K2=-0.5 K3=0.0 # gg08_rnum=3415
S21+ + CH3CH2OH -> H2S21+ + CH3CHO						RTYPE=2 K1=8.50E-11 K2=0.0 K3=0.0 # gg08_rnum=3416
S21+ + CH3CH2OH -> CH3CH2O1+ + S2H						RTYPE=2 K1=1.60E-09 K2=0.0 K3=0.0 # gg08_rnum=3417
S21+ + CH3OCH3 -> CH3CH2O1+ + S2H						RTYPE=2 K1=1.60E-09 K2=0.0 K3=0.0 # gg08_rnum=3418
S21+ + NO -> NO1+ + S2									RTYPE=2 K1=5.10E-10 K2=0.0 K3=0.0 # gg08_rnum=3419
S2H1+ + H2 -> H3S21+									RTYPE=2 K1=1.00E-14 K2=-1.00E+00 K3=0.0 # gg08_rnum=3420
S2H1+ + H2S -> H3S1+ + S2								RTYPE=2 K1=7.95E-10 K2=-0.5 K3=0.0 # gg08_rnum=3421
Si1+ + HC2H -> SiC2H1+ + H								RTYPE=2 K1=2.50E-10 K2=0.0 K3=0.0 # gg08_rnum=3422
Si1+ + H2C2H -> SiC2H21+ + H							RTYPE=2 K1=1.57E-09 K2=-0.5 K3=0.0 # gg08_rnum=3423
Si1+ + H2C2H2 -> SiC2H31+ + H							RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=3424
Si1+ + C3H -> SiC31+ + H								RTYPE=2 K1=7.12E-09 K2=-0.5 K3=0.0 # gg08_rnum=3425
Si1+ + C3H2 -> SiC3H1+ + H								RTYPE=2 K1=3.28E-09 K2=-0.5 K3=0.0 # gg08_rnum=3426
Si1+ + C3H3 -> SiC3H21+ + H								RTYPE=2 K1=3.84E-09 K2=-0.5 K3=0.0 # gg08_rnum=3427
Si1+ + C3H4 -> SiC3H21+ + H2							RTYPE=2 K1=7.46E-10 K2=-0.5 K3=0.0 # gg08_rnum=3428
Si1+ + C4H -> SiC41+ + H								RTYPE=2 K1=1.96E-09 K2=-0.5 K3=0.0 # gg08_rnum=3429
Si1+ + HC4H -> C4H1+ + SiH								RTYPE=2 K1=1.60E-09 K2=0.0 K3=0.0 # gg08_rnum=3430
Si1+ + CH -> SiC1+ + H									RTYPE=2 K1=4.50E-09 K2=-0.5 K3=0.0 # gg08_rnum=3431
Si1+ + CH2 -> SiCH1+ + H								RTYPE=2 K1=7.23E-10 K2=-0.5 K3=0.0 # gg08_rnum=3432
Si1+ + CH3 -> SiCH21+ + H								RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=3433
Si1+ + CH3 -> SiCH1+ + H2								RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=3434
Si1+ + Fe -> Fe1+ + Si									RTYPE=2 K1=1.90E-09 K2=0.0 K3=0.0 # gg08_rnum=3435
Si1+ + H2O -> HSiO1+ + H								RTYPE=2 K1=2.20E-09 K2=-0.5 K3=0.0 # gg08_rnum=3436
Si1+ + HCN -> SiNC1+ + H								RTYPE=2 K1=7.37E-09 K2=-0.5 K3=0.0 # gg08_rnum=3437
Si1+ + HNC -> SiNC1+ + H								RTYPE=2 K1=6.68E-09 K2=-0.5 K3=0.0 # gg08_rnum=3438
Si1+ + Mg -> Mg1+ + Si									RTYPE=2 K1=2.90E-09 K2=0.0 K3=0.0 # gg08_rnum=3439
Si1+ + Na -> Na1+ + Si									RTYPE=2 K1=2.70E-09 K2=0.0 K3=0.0 # gg08_rnum=3440
Si1+ + NH -> SiN1+ + H									RTYPE=2 K1=3.85E-09 K2=-0.5 K3=0.0 # gg08_rnum=3441
Si1+ + NH3 -> SiNH21+ + H								RTYPE=2 K1=1.75E-09 K2=-0.5 K3=0.0 # gg08_rnum=3442
Si1+ + OCS -> SiS1+ + CO								RTYPE=2 K1=1.50E-09 K2=-0.5 K3=0.0 # gg08_rnum=3443
Si1+ + OH -> SiO1+ + H									RTYPE=2 K1=4.70E-09 K2=-0.5 K3=0.0 # gg08_rnum=3444
SiC1+ + H2 -> SiCH1+ + H								RTYPE=2 K1=1.50E-09 K2=0.0 K3=0.0 # gg08_rnum=3445
SiC1+ + N -> Si1+ + CN									RTYPE=2 K1=7.70E-10 K2=0.0 K3=0.0 # gg08_rnum=3446
SiC1+ + O -> SiO1+ + C									RTYPE=2 K1=6.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3447
SiC21+ + H2 -> SiC2H1+ + H								RTYPE=2 K1=1.50E-09 K2=0.0 K3=0.0 # gg08_rnum=3448
SiC2H1+ + C -> SiC31+ + H								RTYPE=2 K1=3.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3449
SiC2H1+ + HC2H -> SiC4H1+ + H2							RTYPE=2 K1=2.00E-11 K2=0.0 K3=0.0 # gg08_rnum=3450
SiC2H21+ + C -> SiC3H1+ + H								RTYPE=2 K1=2.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3451
SiC2H31+ + C -> SiC3H21+ + H							RTYPE=2 K1=2.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3452
SiC3H1+ + C -> SiC41+ + H								RTYPE=2 K1=2.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3453
SiC3H21+ + C -> SiC4H1+ + H								RTYPE=2 K1=2.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3454
SiCH31+ + C -> SiC2H21+ + H								RTYPE=2 K1=2.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3455
SiCH31+ + H2O -> H3O1+ + SiCH2							RTYPE=2 K1=2.00E-09 K2=0.0 K3=0.0 # gg08_rnum=3456
SiCH41+ + C -> SiC2H31+ + H								RTYPE=2 K1=2.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3457
SiH1+ + C -> SiC1+ + H									RTYPE=2 K1=2.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3458
SiH1+ + H -> Si1+ + H2									RTYPE=2 K1=1.90E-09 K2=0.0 K3=0.0 # gg08_rnum=3459
SiH1+ + N -> SiN1+ + H									RTYPE=2 K1=2.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3460
SiH1+ + NH3 -> NH41+ + Si								RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=3461
SiH1+ + O -> SiO1+ + H									RTYPE=2 K1=6.10E-10 K2=0.0 K3=0.0 # gg08_rnum=3462
SiH21+ + C -> SiCH1+ + H								RTYPE=2 K1=1.10E-09 K2=0.0 K3=0.0 # gg08_rnum=3463
SiH21+ + N -> HNSi1+ + H								RTYPE=2 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3464
SiH21+ + O -> HSiO1+ + H								RTYPE=2 K1=6.30E-10 K2=0.0 K3=0.0 # gg08_rnum=3465
SiH21+ + O2 -> HSiO1+ + OH								RTYPE=2 K1=2.40E-11 K2=0.0 K3=0.0 # gg08_rnum=3466
SiH21+ + S -> HSiS1+ + H								RTYPE=2 K1=1.10E-09 K2=0.0 K3=0.0 # gg08_rnum=3467
SiH31+ + C -> SiCH21+ + H								RTYPE=2 K1=2.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3468
SiH31+ + N -> HNSi1+ + H2								RTYPE=2 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3469
SiH31+ + N -> SiNH21+ + H								RTYPE=2 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3470
SiH31+ + O -> HSiO1+ + H2								RTYPE=2 K1=2.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3471
SiH31+ + O -> H2SiO1+ + H								RTYPE=2 K1=2.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3472
SiH31+ + O2 -> H3SiO1+ + O								RTYPE=2 K1=2.90E-12 K2=0.0 K3=0.0 # gg08_rnum=3473
SiH41+ + C -> SiCH21+ + H2								RTYPE=2 K1=3.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3474
SiH41+ + CO -> HCO1+ + SiH3								RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=3475
SiH41+ + H2 -> SiH51+ + H								RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=3476
SiH41+ + H2O -> H3O1+ + SiH3							RTYPE=2 K1=2.00E-09 K2=0.0 K3=0.0 # gg08_rnum=3477
SiH51+ + C -> SiCH41+ + H								RTYPE=2 K1=2.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3478
SiH51+ + H2O -> H3O1+ + SiH4							RTYPE=2 K1=2.00E-09 K2=0.0 K3=0.0 # gg08_rnum=3479
SiH51+ + O -> H3SiO1+ + H2								RTYPE=2 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3480
SiN1+ + O -> SiO1+ + N									RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=3481
SiNC1+ + O -> SiN1+ + CO								RTYPE=2 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3482
SiNCH1+ + O -> HCO1+ + SiN								RTYPE=2 K1=2.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3483
SiNH21+ + C -> SiNC1+ + H2								RTYPE=2 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3484
SiNH21+ + C -> SiNCH1+ + H								RTYPE=2 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3485
SiO1+ + C -> Si1+ + CO									RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=3486
SiO1+ + C2 -> SiC1+ + CO								RTYPE=2 K1=7.60E-10 K2=0.0 K3=0.0 # gg08_rnum=3487
SiO1+ + CH -> HCO1+ + Si								RTYPE=2 K1=5.90E-10 K2=0.0 K3=0.0 # gg08_rnum=3488
SiO1+ + CH2 -> Si1+ + H2CO								RTYPE=2 K1=8.20E-10 K2=0.0 K3=0.0 # gg08_rnum=3489
SiO1+ + CO -> Si1+ + CO2								RTYPE=2 K1=7.90E-10 K2=0.0 K3=0.0 # gg08_rnum=3490
SiO1+ + Fe -> Fe1+ + SiO								RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=3491
SiO1+ + H2 -> HSiO1+ + H								RTYPE=2 K1=3.20E-10 K2=0.0 K3=0.0 # gg08_rnum=3492
SiO1+ + HCO -> HCO1+ + SiO								RTYPE=2 K1=6.60E-10 K2=0.0 K3=0.0 # gg08_rnum=3493
SiO1+ + Mg -> Mg1+ + SiO								RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=3494
SiO1+ + N -> NO1+ + Si									RTYPE=2 K1=2.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3495
SiO1+ + N -> Si1+ + NO									RTYPE=2 K1=2.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3496
SiO1+ + NO -> NO1+ + SiO								RTYPE=2 K1=7.20E-10 K2=0.0 K3=0.0 # gg08_rnum=3497
SiO1+ + O -> Si1+ + O2									RTYPE=2 K1=2.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3498
SiO1+ + S -> Si1+ + SO									RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=3499
SiS1+ + H -> Si1+ + HS									RTYPE=2 K1=1.90E-09 K2=0.0 K3=0.0 # gg08_rnum=3500
SiS1+ + H2 -> HSiS1+ + H								RTYPE=2 K1=1.50E-09 K2=0.0 K3=0.0 # gg08_rnum=3501
SO1+ + Fe -> Fe1+ + SO									RTYPE=2 K1=1.60E-09 K2=0.0 K3=0.0 # gg08_rnum=3502
SO1+ + H2S -> S21+ + H2O								RTYPE=2 K1=1.10E-09 K2=0.0 K3=0.0 # gg08_rnum=3503
SO1+ + Mg -> Mg1+ + SO									RTYPE=2 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3504
SO1+ + N -> NS1+ + O									RTYPE=2 K1=5.00E-11 K2=0.0 K3=0.0 # gg08_rnum=3505
SO1+ + Na -> Na1+ + SO									RTYPE=2 K1=2.30E-09 K2=0.0 K3=0.0 # gg08_rnum=3506
SO1+ + NH3 -> NH31+ + SO								RTYPE=2 K1=1.30E-09 K2=0.0 K3=0.0 # gg08_rnum=3507
SO21+ + CO -> SO1+ + CO2								RTYPE=2 K1=3.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3508
SO21+ + H -> SO1+ + OH									RTYPE=2 K1=4.20E-10 K2=0.0 K3=0.0 # gg08_rnum=3509
SO21+ + H2 -> HSO21+ + H								RTYPE=2 K1=5.00E-12 K2=0.0 K3=0.0 # gg08_rnum=3510
SO21+ + O2 -> O21+ + SO2								RTYPE=2 K1=2.50E-10 K2=0.0 K3=0.0 # gg08_rnum=3511
C1- + NO -> CN1- + O									RTYPE=3 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=3512
C1- + O2 -> O1- + CO									RTYPE=3 K1=4.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3513
H1- + H2O -> OH1- + H2									RTYPE=3 K1=3.80E-09 K2=0.0 K3=0.0 # gg08_rnum=3514
H1- + HCN -> CN1- + H2									RTYPE=3 K1=3.80E-09 K2=0.0 K3=0.0 # gg08_rnum=3515
O1- + CH4 -> OH1- + CH3									RTYPE=3 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3516
O1- + CN -> CN1- + O									RTYPE=3 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=3517
O1- + H2 -> OH1- + H									RTYPE=3 K1=3.00E-11 K2=0.0 K3=0.0 # gg08_rnum=3518
O1- + HCN -> CN1- + OH									RTYPE=3 K1=1.20E-09 K2=0.0 K3=0.0 # gg08_rnum=3519
OH1- + CN -> CN1- + OH									RTYPE=3 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=3520
OH1- + HCN -> CN1- + H2O								RTYPE=3 K1=1.20E-09 K2=0.0 K3=0.0 # gg08_rnum=3521
C1- + CO2 -> CO + CO + E1-								RTYPE=3 K1=4.70E-11 K2=0.0 K3=0.0 # gg08_rnum=3522
C1+ + C3 -> C41+										RTYPE=4 K1=1.00E-13 K2=-1.00E+00 K3=0.0 # gg08_rnum=3523
C1+ + C4 -> C51+										RTYPE=4 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=3524
C1+ + C5 -> C61+										RTYPE=4 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=3525
C1+ + C6 -> C71+										RTYPE=4 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=3526
C1+ + C7 -> C81+										RTYPE=4 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=3527
C1+ + C8 -> C91+										RTYPE=4 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=3528
C1+ + C9 -> C101+										RTYPE=4 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=3529
C1+ + H -> CH1+											RTYPE=4 K1=1.70E-17 K2=0.0 K3=0.0 # gg08_rnum=3530
C1+ + H2 -> CH21+										RTYPE=4 K1=4.00E-16 K2=-2.00E-01 K3=0.0 # gg08_rnum=3531
C1+ + O -> CO1+											RTYPE=4 K1=2.50E-18 K2=0.0 K3=0.0 # gg08_rnum=3532
HC2H1+ + CO -> cH2C3O1+									RTYPE=4 K1=1.10E-15 K2=-2.00E+00 K3=0.0 # gg08_rnum=3533
HC2H1+ + H -> H2C2H1+									RTYPE=4 K1=7.00E-15 K2=-1.50E+00 K3=0.0 # gg08_rnum=3534
HC2H1+ + H2 -> H2C2H21+									RTYPE=4 K1=1.50E-14 K2=-1.00E+00 K3=0.0 # gg08_rnum=3535
HC2H1+ + HC3N -> C5H3N1+								RTYPE=4 K1=2.00E-12 K2=-2.50E+00 K3=0.0 # gg08_rnum=3536
HC2H1+ + HC5N -> C7H3N1+								RTYPE=4 K1=2.00E-12 K2=-2.50E+00 K3=0.0 # gg08_rnum=3537
HC2H1+ + HC7N -> C9H3N1+								RTYPE=4 K1=2.00E-12 K2=-2.50E+00 K3=0.0 # gg08_rnum=3538
H2C2H1+ + CO -> cH2C3OH1+								RTYPE=4 K1=2.00E-15 K2=-2.50E+00 K3=0.0 # gg08_rnum=3539
CH3CH21+ + H2O -> CH3CH2OH21+							RTYPE=4 K1=4.10E-16 K2=-2.40E+00 K3=0.0 # gg08_rnum=3540
C31+ + H -> C3H1+										RTYPE=4 K1=7.00E-16 K2=-1.50E+00 K3=0.0 # gg08_rnum=3541
C3H1+ + HC2H -> C5H31+									RTYPE=4 K1=2.90E-12 K2=-1.00E+00 K3=0.0 # gg08_rnum=3542
C3H1+ + CO -> HC4O1+									RTYPE=4 K1=1.10E-14 K2=-2.00E+00 K3=0.0 # gg08_rnum=3543
C3H1+ + H -> C3H21+										RTYPE=4 K1=2.00E-14 K2=-1.50E+00 K3=0.0 # gg08_rnum=3544
C3H1+ + H2 -> C3H31+									RTYPE=4 K1=0.0 K2=-1.00E+00 K3=0.0 # gg08_rnum=3545
C3H21+ + H -> C3H31+									RTYPE=4 K1=4.00E-15 K2=-1.50E+00 K3=0.0 # gg08_rnum=3546
HC3NH1+ + HC2H -> C5H4N1+								RTYPE=4 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=3547
C3H31+ + HC4H -> C7H51+									RTYPE=4 K1=1.00E-13 K2=-2.50E+00 K3=0.0 # gg08_rnum=3548
C4H1+ + H -> HC4H1+										RTYPE=4 K1=6.00E-14 K2=-1.50E+00 K3=0.0 # gg08_rnum=3549
HC4H1+ + HC2H -> C6H41+									RTYPE=4 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=3550
HC4H1+ + HC4H -> C8H41+									RTYPE=4 K1=1.00E-13 K2=-2.00E+00 K3=0.0 # gg08_rnum=3551
HC4H1+ + H -> C4H31+									RTYPE=4 K1=7.00E-11 K2=-1.00E-01 K3=0.0 # gg08_rnum=3552
HC4H1+ + HC3N -> C7H3N1+								RTYPE=4 K1=2.00E-12 K2=-2.50E+00 K3=0.0 # gg08_rnum=3553
HC4H1+ + HC5N -> C9H3N1+								RTYPE=4 K1=2.00E-12 K2=-2.50E+00 K3=0.0 # gg08_rnum=3554
C4H31+ + HC2H -> C6H51+									RTYPE=4 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=3555
C4H31+ + HC4H -> C8H51+									RTYPE=4 K1=1.00E-13 K2=-2.50E+00 K3=0.0 # gg08_rnum=3556
C4H31+ + H -> C4H41+									RTYPE=4 K1=6.00E-14 K2=-7.00E-01 K3=0.0 # gg08_rnum=3557
C5H1+ + HC4H -> C9H31+									RTYPE=4 K1=1.00E-13 K2=-2.00E+00 K3=0.0 # gg08_rnum=3558
C5H21+ + HC4H -> C9H41+									RTYPE=4 K1=1.00E-13 K2=-2.00E+00 K3=0.0 # gg08_rnum=3559
C5H31+ + HC4H -> C9H51+									RTYPE=4 K1=1.00E-13 K2=-2.50E+00 K3=0.0 # gg08_rnum=3560
C61+ + H2 -> C6H21+										RTYPE=4 K1=5.20E-14 K2=-0.5 K3=0.0 # gg08_rnum=3561
C6H21+ + HC2H -> C8H41+									RTYPE=4 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=3562
C6H41+ + H -> C6H51+									RTYPE=4 K1=3.00E-15 K2=-1.50E+00 K3=0.0 # gg08_rnum=3563
C6H51+ + H2 -> C6H71+									RTYPE=4 K1=6.00E-11 K2=0.0 K3=0.0 # gg08_rnum=3564
CH31+ + CH3OH -> CH3OCH41+								RTYPE=4 K1=7.80E-12 K2=-1.10E+00 K3=0.0 # gg08_rnum=3565
CH31+ + CO -> CH3CO1+									RTYPE=4 K1=1.20E-13 K2=-1.30E+00 K3=0.0 # gg08_rnum=3566
CH31+ + H2 -> CH51+										RTYPE=4 K1=1.30E-14 K2=-1.00E+00 K3=0.0 # gg08_rnum=3567
CH31+ + H2O -> CH3OH21+									RTYPE=4 K1=2.00E-12 K2=0.0 K3=0.0 # gg08_rnum=3568
CH31+ + HC3N -> C4H4N1+									RTYPE=4 K1=8.60E-11 K2=-1.40E+00 K3=0.0 # gg08_rnum=3569
CH31+ + HC5N -> C6H4N1+									RTYPE=4 K1=8.60E-11 K2=-1.40E+00 K3=0.0 # gg08_rnum=3570
CH31+ + HC7N -> C8H4N1+									RTYPE=4 K1=8.60E-11 K2=-1.40E+00 K3=0.0 # gg08_rnum=3571
CH31+ + HCN -> CH3CNH1+									RTYPE=4 K1=9.00E-09 K2=-0.5 K3=0.0 # gg08_rnum=3572
CH31+ + HCOOH -> HCOOCH3H1+								RTYPE=4 K1=1.00E-11 K2=-1.50E+00 K3=0.0 # gg08_rnum=3573
CH31+ + NH3 -> CH3NH31+									RTYPE=4 K1=9.40E-10 K2=-9.00E-01 K3=0.0 # gg08_rnum=3574
CH3O1+ + CH4 -> CH3OCH41+								RTYPE=4 K1=1.00E-17 K2=0.0 K3=0.0 # gg08_rnum=3575
CH3OH21+ + H2CO -> CH3OHH2COH1+							RTYPE=4 K1=3.10E-15 K2=-3.00E+00 K3=0.0 # gg08_rnum=3576
CH51+ + CO -> CH3CH2O1+									RTYPE=4 K1=1.00E-17 K2=0.0 K3=0.0 # gg08_rnum=3577
H1+ + H -> H21+											RTYPE=4 K1=2.00E-20 K2=1.00E+00 K3=0.0 # gg08_rnum=3578
H2CN1+ + HC2H -> C3H4N1+								RTYPE=4 K1=2.20E-15 K2=-2.00E+00 K3=0.0 # gg08_rnum=3579
H3O1+ + HC2H -> CH3CH2O1+								RTYPE=4 K1=2.70E-14 K2=-1.60E+00 K3=0.0 # gg08_rnum=3580
H3O1+ + H2C2H2 -> CH3CH2OH21+							RTYPE=4 K1=2.40E-14 K2=-2.80E+00 K3=0.0 # gg08_rnum=3581
HCO1+ + CH4 -> CH3CH2O1+								RTYPE=4 K1=1.00E-17 K2=0.0 K3=0.0 # gg08_rnum=3582
HCO1+ + H2O -> HCOOH21+									RTYPE=4 K1=4.00E-13 K2=-1.30E+00 K3=0.0 # gg08_rnum=3583
HS1+ + H2 -> H3S1+										RTYPE=4 K1=1.40E-16 K2=-6.00E-01 K3=0.0 # gg08_rnum=3584
Na1+ + H2 -> NaH21+										RTYPE=4 K1=4.00E-19 K2=0.0 K3=0.0 # gg08_rnum=3585
Na1+ + H2O -> NaH2O1+									RTYPE=4 K1=1.40E-17 K2=-1.30E+00 K3=0.0 # gg08_rnum=3586
NO1+ + H2 -> H2NO1+										RTYPE=4 K1=6.70E-20 K2=-1.00E+00 K3=0.0 # gg08_rnum=3587
OCS1+ + H2 -> HOCSH1+									RTYPE=4 K1=6.10E-20 K2=-1.50E+00 K3=0.0 # gg08_rnum=3588
P1+ + H2 -> PH21+										RTYPE=4 K1=7.50E-18 K2=-1.30E+00 K3=0.0 # gg08_rnum=3589
PH1+ + H2 -> PH31+										RTYPE=4 K1=2.40E-17 K2=-1.40E+00 K3=0.0 # gg08_rnum=3590
S1+ + H2 -> H2S1+										RTYPE=4 K1=1.00E-17 K2=-2.00E-01 K3=0.0 # gg08_rnum=3591
S2H1+ + H2 -> H3S21+									RTYPE=4 K1=1.00E-14 K2=-1.00E+00 K3=0.0 # gg08_rnum=3592
Si1+ + HC2H -> SiC2H21+									RTYPE=4 K1=1.00E-13 K2=-1.00E+00 K3=0.0 # gg08_rnum=3593
Si1+ + CH4 -> SiCH41+									RTYPE=4 K1=4.00E-16 K2=-1.50E+00 K3=0.0 # gg08_rnum=3594
Si1+ + H -> SiH1+										RTYPE=4 K1=1.00E-17 K2=0.0 K3=0.0 # gg08_rnum=3595
Si1+ + H2 -> SiH21+										RTYPE=4 K1=1.00E-15 K2=0.0 K3=0.0 # gg08_rnum=3596
Si1+ + HCN -> SiNCH1+									RTYPE=4 K1=6.00E-15 K2=-1.50E+00 K3=0.0 # gg08_rnum=3597
Si1+ + O -> SiO1+										RTYPE=4 K1=1.00E-17 K2=0.0 K3=0.0 # gg08_rnum=3598
SiC2H1+ + H2 -> SiC2H31+								RTYPE=4 K1=3.00E-16 K2=-1.00E+00 K3=0.0 # gg08_rnum=3599
SiH1+ + H2 -> SiH31+									RTYPE=4 K1=3.00E-17 K2=-1.00E+00 K3=0.0 # gg08_rnum=3600
SiH31+ + H2 -> SiH51+									RTYPE=4 K1=1.00E-18 K2=-0.5 K3=0.0 # gg08_rnum=3601
C1- + C -> C2 + E1-										RTYPE=5 K1=5.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3602
C1- + CH -> C2H + E1-									RTYPE=5 K1=5.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3603
C1- + CH2 -> HC2H + E1-									RTYPE=5 K1=5.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3604
C1- + H -> CH + E1-										RTYPE=5 K1=5.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3605
C1- + H2 -> CH2 + E1-									RTYPE=5 K1=1.00E-13 K2=0.0 K3=0.0 # gg08_rnum=3606
C1- + H2O -> H2CO + E1-									RTYPE=5 K1=5.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3607
C1- + N -> CN + E1-										RTYPE=5 K1=5.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3608
C1- + NH -> HCN + E1-									RTYPE=5 K1=5.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3609
C1- + O -> CO + E1-										RTYPE=5 K1=5.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3610
C1- + O2 -> CO2 + E1-									RTYPE=5 K1=5.00E-11 K2=0.0 K3=0.0 # gg08_rnum=3611
C1- + OH -> HCO + E1-									RTYPE=5 K1=5.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3612
CN1- + CH3 -> CH3CN + E1-								RTYPE=5 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=3613
CN1- + H -> HCN + E1-									RTYPE=5 K1=1.30E-09 K2=0.0 K3=0.0 # gg08_rnum=3614
H1- + C -> CH + E1-										RTYPE=5 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=3615
H1- + C2 -> C2H + E1-									RTYPE=5 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=3616
H1- + C2H -> HC2H + E1-									RTYPE=5 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=3617
H1- + CH -> CH2 + E1-									RTYPE=5 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3618
H1- + CH2 -> CH3 + E1-									RTYPE=5 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=3619
H1- + CH3 -> CH4 + E1-									RTYPE=5 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=3620
H1- + CN -> HCN + E1-									RTYPE=5 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3621
H1- + CO -> HCO + E1-									RTYPE=5 K1=5.00E-11 K2=0.0 K3=0.0 # gg08_rnum=3622
H1- + H -> H2 + E1-										RTYPE=5 K1=1.30E-09 K2=0.0 K3=0.0 # gg08_rnum=3623
H1- + HCO -> H2CO + E1-									RTYPE=5 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=3624
H1- + N -> NH + E1-										RTYPE=5 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=3625
H1- + NH -> NH2 + E1-									RTYPE=5 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3626
H1- + NH2 -> NH3 + E1-									RTYPE=5 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=3627
H1- + O -> OH + E1-										RTYPE=5 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=3628
H1- + OH -> H2O + E1-									RTYPE=5 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3629
O1- + C -> CO + E1-										RTYPE=5 K1=5.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3630
O1- + CH -> HCO + E1-									RTYPE=5 K1=5.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3631
O1- + CH2 -> H2CO + E1-									RTYPE=5 K1=5.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3632
O1- + CO -> CO2 + E1-									RTYPE=5 K1=6.50E-10 K2=0.0 K3=0.0 # gg08_rnum=3633
O1- + H -> OH + E1-										RTYPE=5 K1=5.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3634
O1- + H2 -> H2O + E1-									RTYPE=5 K1=7.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3635
O1- + N -> NO + E1-										RTYPE=5 K1=2.20E-10 K2=0.0 K3=0.0 # gg08_rnum=3636
O1- + O -> O2 + E1-										RTYPE=5 K1=1.90E-10 K2=0.0 K3=0.0 # gg08_rnum=3637
OH1- + C -> HCO + E1-									RTYPE=5 K1=5.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3638
OH1- + CH -> H2CO + E1-									RTYPE=5 K1=5.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3639
OH1- + CH3 -> CH3OH + E1-								RTYPE=5 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=3640
OH1- + H -> H2O + E1-									RTYPE=5 K1=1.40E-09 K2=0.0 K3=0.0 # gg08_rnum=3641
S1- + C -> CS + E1-										RTYPE=5 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3642
S1- + CO -> OCS + E1-									RTYPE=5 K1=3.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3643
S1- + H -> HS + E1-										RTYPE=5 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3644
S1- + N -> NS + E1-										RTYPE=5 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3645
S1- + O -> SO + E1-										RTYPE=5 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3646
S1- + O2 -> SO2 + E1-									RTYPE=5 K1=3.00E-11 K2=0.0 K3=0.0 # gg08_rnum=3647
O + CH -> HCO1+ + E1-									RTYPE=6 K1=2.00E-11 K2=4.40E-01 K3=0.0 # gg08_rnum=3648
C + C2H -> C3 + H										RTYPE=7 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3649
C + HC2H -> C3H + H										RTYPE=7 K1=1.45E-10 K2=-1.20E-01 K3=0.0 # gg08_rnum=3650
C + HC2H -> C3 + H2										RTYPE=7 K1=1.45E-10 K2=-1.20E-01 K3=0.0 # gg08_rnum=3651
C + H2C2H -> C3H2 + H									RTYPE=7 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3652
C + H2C2H2 -> C3H3 + H									RTYPE=7 K1=3.10E-10 K2=-7.00E-02 K3=0.0 # gg08_rnum=3653
C + CH3CH2 -> C3H4 + H									RTYPE=7 K1=2.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3654
C + C2N -> C2 + CN										RTYPE=7 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3655
C + C2O -> C2 + CO										RTYPE=7 K1=2.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3656
C + C3H -> C4 + H										RTYPE=7 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3657
C + C3H2 -> C4H + H										RTYPE=7 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3658
C + C3H3 -> HC4H + H									RTYPE=7 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3659
C + C3H4 -> C4H3 + H									RTYPE=7 K1=2.70E-10 K2=-1.10E-01 K3=0.0 # gg08_rnum=3660
C + C3H4 -> HC4H + H2									RTYPE=7 K1=4.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3661
C + C3N -> C3 + CN										RTYPE=7 K1=1.00E-13 K2=0.0 K3=0.0 # gg08_rnum=3662
C + C3O -> C3 + CO										RTYPE=7 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3663
C + C4 -> C2 + C3										RTYPE=7 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3664
C + C4H -> C5 + H										RTYPE=7 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3665
C + HC4H -> C5H + H										RTYPE=7 K1=6.50E-10 K2=0.0 K3=0.0 # gg08_rnum=3666
C + C4H3 -> C5H2 + H									RTYPE=7 K1=3.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3667
C + C5 -> C3 + C3										RTYPE=7 K1=3.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3668
C + C5H -> C6 + H										RTYPE=7 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3669
C + C5H2 -> C6H + H										RTYPE=7 K1=5.30E-10 K2=0.0 K3=0.0 # gg08_rnum=3670
C + C5N -> C5 + CN										RTYPE=7 K1=1.00E-13 K2=0.0 K3=0.0 # gg08_rnum=3671
C + C6 -> C2 + C5										RTYPE=7 K1=5.00E-11 K2=0.0 K3=0.0 # gg08_rnum=3672
C + C6 -> C3 + C4										RTYPE=7 K1=5.00E-11 K2=0.0 K3=0.0 # gg08_rnum=3673
C + C6H -> C7 + H										RTYPE=7 K1=2.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3674
C + C6H2 -> C7H + H										RTYPE=7 K1=7.40E-10 K2=0.0 K3=0.0 # gg08_rnum=3675
C + C7 -> C3 + C5										RTYPE=7 K1=3.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3676
C + C7H -> C8 + H										RTYPE=7 K1=2.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3677
C + C7H2 -> C8H + H										RTYPE=7 K1=8.40E-10 K2=0.0 K3=0.0 # gg08_rnum=3678
C + C7N -> C7 + CN										RTYPE=7 K1=1.00E-13 K2=0.0 K3=0.0 # gg08_rnum=3679
C + C8 -> C2 + C7										RTYPE=7 K1=3.00E-11 K2=0.0 K3=0.0 # gg08_rnum=3680
C + C8 -> C3 + C6										RTYPE=7 K1=3.00E-11 K2=0.0 K3=0.0 # gg08_rnum=3681
C + C8 -> C4 + C5										RTYPE=7 K1=3.00E-11 K2=0.0 K3=0.0 # gg08_rnum=3682
C + C8H -> C9 + H										RTYPE=7 K1=2.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3683
C + C8H2 -> C9H + H										RTYPE=7 K1=9.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3684
C + C9H -> C10 + H										RTYPE=7 K1=2.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3685
C + C9N -> C9 + CN										RTYPE=7 K1=1.00E-13 K2=0.0 K3=0.0 # gg08_rnum=3686
C + CH -> C2 + H										RTYPE=7 K1=6.59E-11 K2=0.0 K3=0.0 # gg08_rnum=3687
C + CH2 -> C2H + H										RTYPE=7 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3688
C + CH2 -> CH + CH										RTYPE=7 K1=2.69E-12 K2=0.0 K3=2.36E+04 # gg08_rnum=3689
C + H2C2N -> HC3N + H									RTYPE=7 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3690
C + CH3 -> HC2H + H										RTYPE=7 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3691
C + CH3C4H -> C6H2 + H2									RTYPE=7 K1=5.30E-10 K2=0.0 K3=0.0 # gg08_rnum=3692
C + CH3C6H -> C8H2 + H2									RTYPE=7 K1=7.40E-10 K2=0.0 K3=0.0 # gg08_rnum=3693
C + CN -> C2 + N										RTYPE=7 K1=4.98E-10 K2=0.0 K3=1.81E+04 # gg08_rnum=3694
C + CO -> C2 + O										RTYPE=7 K1=1.00E-10 K2=0.0 K3=5.28E+04 # gg08_rnum=3695
C + CS -> S + C2										RTYPE=7 K1=1.44E-11 K2=5.00E-01 K3=2.04E+04 # gg08_rnum=3696
C + H2CN -> C2N + H2									RTYPE=7 K1=2.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3697
C + C2NCH -> C3 + HCN									RTYPE=7 K1=2.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3698
C + HCO -> C2O + H										RTYPE=7 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3699
C + HCO -> CH + CO										RTYPE=7 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3700
C + HNC3 -> C3 + HNC									RTYPE=7 K1=2.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3701
C + HPO -> PH + CO										RTYPE=7 K1=4.00E-11 K2=0.0 K3=0.0 # gg08_rnum=3702
C + HS -> CS + H										RTYPE=7 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3703
C + HS -> S + CH										RTYPE=7 K1=1.20E-11 K2=5.80E-01 K3=5.88E+03 # gg08_rnum=3704
C + N2 -> CN + N										RTYPE=7 K1=8.70E-11 K2=0.0 K3=2.26E+04 # gg08_rnum=3705
C + NH -> N + CH										RTYPE=7 K1=1.73E-11 K2=5.00E-01 K3=4.00E+03 # gg08_rnum=3706
C + NH -> CN + H										RTYPE=7 K1=1.20E-10 K2=0.0 K3=0.0 # gg08_rnum=3707
C + NH2 -> HNC + H										RTYPE=7 K1=3.40E-11 K2=-3.60E-01 K3=0.0 # gg08_rnum=3708
C + NH2 -> CH + NH										RTYPE=7 K1=9.61E-13 K2=0.0 K3=1.05E+04 # gg08_rnum=3709
C + NH2 -> HCN + H										RTYPE=7 K1=3.40E-11 K2=-3.60E-01 K3=0.0 # gg08_rnum=3710
C + NO -> CO + N										RTYPE=7 K1=9.00E-11 K2=-1.60E-01 K3=0.0 # gg08_rnum=3711
C + NO -> CN + O										RTYPE=7 K1=6.00E-11 K2=-1.60E-01 K3=0.0 # gg08_rnum=3712
C + NS -> CN + S										RTYPE=7 K1=1.50E-10 K2=-1.60E-01 K3=0.0 # gg08_rnum=3713
C + NS -> CS + N										RTYPE=7 K1=1.73E-11 K2=5.00E-01 K3=4.00E+03 # gg08_rnum=3714
C + O2 -> CO + O										RTYPE=7 K1=4.70E-11 K2=-3.40E-01 K3=0.0 # gg08_rnum=3715
C + OCN -> CO + CN										RTYPE=7 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3716
C + OH -> CO + H										RTYPE=7 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3717
C + OH -> O + CH										RTYPE=7 K1=2.25E-11 K2=5.00E-01 K3=1.48E+04 # gg08_rnum=3718
C + PH -> CP + H										RTYPE=7 K1=7.50E-11 K2=0.0 K3=0.0 # gg08_rnum=3719
C + S2 -> S + CS										RTYPE=7 K1=7.00E-11 K2=0.0 K3=0.0 # gg08_rnum=3720
C + SiH -> SiC + H										RTYPE=7 K1=6.59E-11 K2=0.0 K3=0.0 # gg08_rnum=3721
C + SiH2 -> SiCH + H									RTYPE=7 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3722
C + SiH3 -> SiCH2 + H									RTYPE=7 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3723
C + SO -> CS + O										RTYPE=7 K1=3.50E-11 K2=0.0 K3=0.0 # gg08_rnum=3724
C + SO -> CO + S										RTYPE=7 K1=3.50E-11 K2=0.0 K3=0.0 # gg08_rnum=3725
C + SO2 -> CO + SO										RTYPE=7 K1=7.00E-11 K2=0.0 K3=0.0 # gg08_rnum=3726
C2 + HC2H -> C4H + H									RTYPE=7 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3727
C2 + H2C2H2 -> C4H3 + H									RTYPE=7 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3728
C2 + HC4H -> C6H + H									RTYPE=7 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3729
C2 + C6H2 -> C8H + H									RTYPE=7 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3730
C2 + O2 -> CO + CO										RTYPE=7 K1=1.83E-12 K2=0.0 K3=0.0 # gg08_rnum=3731
C2 + O2 -> CO2 + C										RTYPE=7 K1=6.60E-13 K2=0.0 K3=0.0 # gg08_rnum=3732
C2H + C2 -> C4 + H										RTYPE=7 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3733
C2H + HC2H -> HC4H + H									RTYPE=7 K1=1.06E-10 K2=-2.50E-01 K3=0.0 # gg08_rnum=3734
C2H + C3 -> C5 + H										RTYPE=7 K1=2.00E-10 K2=-2.50E-01 K3=0.0 # gg08_rnum=3735
C2H + C3H2 -> C5H2 + H									RTYPE=7 K1=1.06E-10 K2=-2.50E-01 K3=0.0 # gg08_rnum=3736
C2H + C4 -> C6 + H										RTYPE=7 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3737
C2H + HC4H -> C6H2 + H									RTYPE=7 K1=1.06E-10 K2=-2.50E-01 K3=0.0 # gg08_rnum=3738
C2H + C5 -> C7 + H										RTYPE=7 K1=2.00E-10 K2=-2.50E-01 K3=0.0 # gg08_rnum=3739
C2H + C5H2 -> C7H2 + H									RTYPE=7 K1=1.06E-10 K2=-2.50E-01 K3=0.0 # gg08_rnum=3740
C2H + C6 -> C8 + H										RTYPE=7 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3741
C2H + C6H2 -> C8H2 + H									RTYPE=7 K1=1.06E-10 K2=-2.50E-01 K3=0.0 # gg08_rnum=3742
C2H + C7 -> C9 + H										RTYPE=7 K1=2.00E-10 K2=-2.50E-01 K3=0.0 # gg08_rnum=3743
C2H + C7H2 -> C9H2 + H									RTYPE=7 K1=1.06E-10 K2=-2.50E-01 K3=0.0 # gg08_rnum=3744
C2H + C8 -> C10 + H										RTYPE=7 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3745
C2H + CS -> C3S + H										RTYPE=7 K1=1.70E-11 K2=0.0 K3=0.0 # gg08_rnum=3746
C2H + HC3N -> HC5N + H									RTYPE=7 K1=1.06E-10 K2=-2.50E-01 K3=0.0 # gg08_rnum=3747
C2H + HC5N -> HC7N + H									RTYPE=7 K1=1.06E-10 K2=-2.50E-01 K3=0.0 # gg08_rnum=3748
C2H + HC7N -> HC9N + H									RTYPE=7 K1=1.06E-10 K2=-2.50E-01 K3=0.0 # gg08_rnum=3749
C2H + HCN -> HC3N + H									RTYPE=7 K1=5.30E-12 K2=0.0 K3=7.70E+02 # gg08_rnum=3750
C2H + HNC -> HC3N + H									RTYPE=7 K1=5.30E-12 K2=0.0 K3=7.70E+02 # gg08_rnum=3751
C2H + O2 -> CH + CO2									RTYPE=7 K1=1.40E-11 K2=-3.20E-01 K3=0.0 # gg08_rnum=3752
C2H + O2 -> H + CO + CO									RTYPE=7 K1=1.40E-11 K2=-3.20E-01 K3=0.0 # gg08_rnum=3753
C2H + O2 -> HCO + CO									RTYPE=7 K1=4.20E-11 K2=-3.20E-01 K3=0.0 # gg08_rnum=3754
C2H + S -> C2S + H										RTYPE=7 K1=1.70E-11 K2=0.0 K3=0.0 # gg08_rnum=3755
H2C2H + O2 -> HO2 + HC2H								RTYPE=7 K1=4.11E-14 K2=0.0 K3=0.0 # gg08_rnum=3756
H2C2H + O2 -> H2CO + HCO								RTYPE=7 K1=2.73E-11 K2=-1.39E+00 K3=5.10E+02 # gg08_rnum=3757
CH + C2 -> C3 + H										RTYPE=7 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3758
CH + H2C2H2 -> C3H4 + H									RTYPE=7 K1=3.45E-10 K2=-5.50E-01 K3=2.96E+01 # gg08_rnum=3759
CH + C3 -> C4 + H										RTYPE=7 K1=4.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3760
CH + C3H4 -> C4H4 + H									RTYPE=7 K1=4.20E-10 K2=-2.30E-01 K3=1.60E+01 # gg08_rnum=3761
CH + C4 -> C5 + H										RTYPE=7 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3762
CH + HC4H -> C5H2 + H									RTYPE=7 K1=4.20E-10 K2=-2.30E-01 K3=1.60E+01 # gg08_rnum=3763
CH + C5 -> C6 + H										RTYPE=7 K1=4.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3764
CH + C6 -> C7 + H										RTYPE=7 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3765
CH + C6H2 -> C7H2 + H									RTYPE=7 K1=4.20E-10 K2=-2.30E-01 K3=1.60E+01 # gg08_rnum=3766
CH + C7 -> C8 + H										RTYPE=7 K1=4.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3767
CH + C8 -> C9 + H										RTYPE=7 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3768
CH + C9 -> C10 + H										RTYPE=7 K1=4.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3769
CH + CH4 -> CH3 + CH2									RTYPE=7 K1=2.28E-11 K2=7.00E-01 K3=3.00E+03 # gg08_rnum=3770
CH + CH4 -> H2C2H2 + H									RTYPE=7 K1=2.23E-12 K2=0.0 K3=0.0 # gg08_rnum=3771
CH + CO2 -> HCO + CO									RTYPE=7 K1=2.94E-13 K2=5.00E-01 K3=3.00E+03 # gg08_rnum=3772
CH + H2CO -> HCO + CH2									RTYPE=7 K1=9.21E-12 K2=7.00E-01 K3=2.00E+03 # gg08_rnum=3773
CH + HCO -> CO + CH2									RTYPE=7 K1=2.87E-12 K2=7.00E-01 K3=5.00E+02 # gg08_rnum=3774
CH + HNO -> NO + CH2									RTYPE=7 K1=1.73E-11 K2=0.0 K3=0.0 # gg08_rnum=3775
CH + N -> NH + C										RTYPE=7 K1=3.03E-11 K2=6.50E-01 K3=1.21E+03 # gg08_rnum=3776
CH + N2 -> HCN + N										RTYPE=7 K1=5.60E-13 K2=8.80E-01 K3=1.01E+04 # gg08_rnum=3777
CH + NO -> HCO + N										RTYPE=7 K1=2.70E-11 K2=0.0 K3=5.00E+03 # gg08_rnum=3778
CH + NO -> OCN + H										RTYPE=7 K1=1.13E-25 K2=0.0 K3=0.0 # gg08_rnum=3779
CH + NO -> CN + OH										RTYPE=7 K1=2.32E-26 K2=0.0 K3=0.0 # gg08_rnum=3780
CH + NO -> HCN + O										RTYPE=7 K1=1.20E-11 K2=-1.30E-01 K3=0.0 # gg08_rnum=3781
CH + O -> OH + C										RTYPE=7 K1=2.52E-11 K2=0.0 K3=2.38E+03 # gg08_rnum=3782
CH + O2 -> CO + OH										RTYPE=7 K1=3.80E-11 K2=-4.80E-01 K3=0.0 # gg08_rnum=3783
CH + O2 -> HCO + O										RTYPE=7 K1=1.44E-11 K2=7.00E-01 K3=3.00E+03 # gg08_rnum=3784
CH + HO2 -> HCO + OH									RTYPE=7 K1=1.44E-11 K2=5.00E-01 K3=3.00E+03 # gg08_rnum=3785
CH + HO2 -> O2 + CH2									RTYPE=7 K1=2.94E-13 K2=5.00E-01 K3=7.55E+03 # gg08_rnum=3786
CH + OH -> HCO + H										RTYPE=7 K1=1.44E-11 K2=5.00E-01 K3=5.00E+03 # gg08_rnum=3787
CH + S -> HS + C										RTYPE=7 K1=1.73E-11 K2=5.00E-01 K3=4.00E+03 # gg08_rnum=3788
CH2 + CH2 -> H2C2H + H									RTYPE=7 K1=3.32E-11 K2=0.0 K3=0.0 # gg08_rnum=3789
CH2 + CH2 -> CH3 + CH									RTYPE=7 K1=4.00E-10 K2=0.0 K3=5.00E+03 # gg08_rnum=3790
CH2 + CH2 -> HC2H + H + H								RTYPE=7 K1=1.80E-10 K2=0.0 K3=4.00E+02 # gg08_rnum=3791
CH2 + CH2 -> HC2H + H2									RTYPE=7 K1=2.63E-09 K2=0.0 K3=6.01E+03 # gg08_rnum=3792
CH2 + CH4 -> CH3 + CH3									RTYPE=7 K1=7.13E-12 K2=0.0 K3=5.05E+03 # gg08_rnum=3793
CH2 + CN -> HCN + CH									RTYPE=7 K1=5.30E-12 K2=0.0 K3=2.50E+03 # gg08_rnum=3794
CH2 + H2CO -> HCO + CH3									RTYPE=7 K1=3.30E-13 K2=0.0 K3=3.27E+03 # gg08_rnum=3795
CH2 + HCO -> CO + CH3									RTYPE=7 K1=3.00E-11 K2=0.0 K3=0.0 # gg08_rnum=3796
CH2 + HNO -> NO + CH3									RTYPE=7 K1=1.70E-11 K2=0.0 K3=0.0 # gg08_rnum=3797
CH2 + NO -> H2CO + N									RTYPE=7 K1=2.70E-12 K2=0.0 K3=3.50E+03 # gg08_rnum=3798
CH2 + NO -> HCN + OH									RTYPE=7 K1=8.32E-13 K2=0.0 K3=1.44E+03 # gg08_rnum=3799
CH2 + O -> HCO + H										RTYPE=7 K1=5.01E-11 K2=0.0 K3=0.0 # gg08_rnum=3800
CH2 + O -> OH + CH										RTYPE=7 K1=4.98E-10 K2=0.0 K3=6.00E+03 # gg08_rnum=3801
CH2 + O2 -> CO2 + H + H									RTYPE=7 K1=3.65E-11 K2=-3.30E+00 K3=1.44E+03 # gg08_rnum=3802
CH2 + O2 -> CO2 + H2									RTYPE=7 K1=2.92E-11 K2=-3.30E+00 K3=1.44E+03 # gg08_rnum=3803
CH2 + O2 -> HCO + OH									RTYPE=7 K1=4.10E-11 K2=0.0 K3=7.50E+02 # gg08_rnum=3804
CH2 + O2 -> H2CO + O									RTYPE=7 K1=3.65E-11 K2=-3.30E+00 K3=1.44E+03 # gg08_rnum=3805
CH2 + O2 -> CO + H2O									RTYPE=7 K1=2.48E-10 K2=-3.30E+00 K3=1.44E+03 # gg08_rnum=3806
CH2 + OH -> H2O + CH									RTYPE=7 K1=1.44E-11 K2=5.00E-01 K3=3.00E+03 # gg08_rnum=3807
CH2 + OH -> O + CH3										RTYPE=7 K1=1.44E-11 K2=5.00E-01 K3=3.00E+03 # gg08_rnum=3808
CH3 + CH3 -> CH4 + CH2									RTYPE=7 K1=7.13E-12 K2=0.0 K3=5.05E+03 # gg08_rnum=3809
CH3 + CH3 -> H2C2H2 + H2								RTYPE=7 K1=1.66E-08 K2=0.0 K3=1.66E+04 # gg08_rnum=3810
CH3 + CH3 -> CH3CH2 + H									RTYPE=7 K1=1.46E-11 K2=1.00E-01 K3=5.34E+03 # gg08_rnum=3811
CH3 + CN -> HCN + CH2									RTYPE=7 K1=9.21E-12 K2=7.00E-01 K3=1.50E+03 # gg08_rnum=3812
CH3 + H2CO -> HCO + CH4									RTYPE=7 K1=1.34E-15 K2=5.05E+00 K3=1.64E+03 # gg08_rnum=3813
CH3 + H2O -> OH + CH4									RTYPE=7 K1=2.30E-15 K2=3.47E+00 K3=6.68E+03 # gg08_rnum=3814
CH3 + H2S -> HS + CH4									RTYPE=7 K1=3.30E-13 K2=0.0 K3=1.10E+03 # gg08_rnum=3815
CH3 + NH3 -> CH4 + NH2									RTYPE=7 K1=9.55E-14 K2=0.0 K3=4.89E+03 # gg08_rnum=3816
CH3 + O2 -> H2CO + OH									RTYPE=7 K1=5.64E-13 K2=0.0 K3=4.50E+03 # gg08_rnum=3817
CH3 + O2 -> HO2 + CH2									RTYPE=7 K1=5.30E-12 K2=0.0 K3=3.50E+04 # gg08_rnum=3818
CH3 + O2 -> HCO + H2O									RTYPE=7 K1=1.66E-12 K2=0.0 K3=0.0 # gg08_rnum=3819
CH3 + HO2 -> O2 + CH4									RTYPE=7 K1=6.00E-12 K2=0.0 K3=0.0 # gg08_rnum=3820
CH3 + OH -> CH4 + O										RTYPE=7 K1=3.27E-14 K2=2.20E+00 K3=2.24E+03 # gg08_rnum=3821
CH3 + OH -> H2O + CH2									RTYPE=7 K1=1.20E-10 K2=0.0 K3=1.40E+03 # gg08_rnum=3822
CH3 + OH -> H2CO + H2									RTYPE=7 K1=1.70E-12 K2=0.0 K3=0.0 # gg08_rnum=3823
CH3 + OH -> HCOH + H2									RTYPE=7 K1=0.0 K2=0.0 K3=1.50E+03 # gg08_rnum=3824
CH4 + CN -> HCN + CH3									RTYPE=7 K1=3.14E-12 K2=1.53E+00 K3=5.04E+02 # gg08_rnum=3825
CH4 + O2 -> HO2 + CH3									RTYPE=7 K1=6.70E-11 K2=0.0 K3=2.86E+04 # gg08_rnum=3826
CH4 + OH -> H2O + CH3									RTYPE=7 K1=3.77E-13 K2=2.42E+00 K3=1.16E+03 # gg08_rnum=3827
CN + HC2H -> HC3N + H									RTYPE=7 K1=2.72E-10 K2=-5.20E-01 K3=1.90E+01 # gg08_rnum=3828
CN + H2C2H2 -> C3H3N + H								RTYPE=7 K1=2.67E-10 K2=-6.90E-01 K3=3.10E+01 # gg08_rnum=3829
CN + H2C2H2 -> H2C2H + HCN								RTYPE=7 K1=2.14E-10 K2=-6.90E-01 K3=3.10E+01 # gg08_rnum=3830
CN + CH3CH3 -> CH3CH2 + HCN								RTYPE=7 K1=2.40E-11 K2=0.0 K3=0.0 # gg08_rnum=3831
CN + HC4H -> HC5N + H									RTYPE=7 K1=2.72E-10 K2=-5.20E-01 K3=1.90E+01 # gg08_rnum=3832
CN + C6H2 -> HC7N + H									RTYPE=7 K1=2.72E-10 K2=-5.20E-01 K3=1.90E+01 # gg08_rnum=3833
CN + C8H2 -> HC9N + H									RTYPE=7 K1=2.72E-10 K2=-5.20E-01 K3=1.90E+01 # gg08_rnum=3834
CN + CN -> N2 + C2										RTYPE=7 K1=2.66E-09 K2=0.0 K3=2.16E+04 # gg08_rnum=3835
CN + H2CO -> HCO + HCN									RTYPE=7 K1=2.60E-10 K2=-4.70E-01 K3=8.26E+02 # gg08_rnum=3836
CN + HCO -> HCN + CO									RTYPE=7 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3837
CN + HNO -> NO + HCN									RTYPE=7 K1=3.00E-11 K2=0.0 K3=0.0 # gg08_rnum=3838
CN + NH3 -> NH2 + HCN									RTYPE=7 K1=1.38E-11 K2=-1.14E+00 K3=0.0 # gg08_rnum=3839
CN + NH3 -> NH2CN + H									RTYPE=7 K1=1.30E-11 K2=-1.11E+00 K3=0.0 # gg08_rnum=3840
CN + NO -> N2 + CO										RTYPE=7 K1=1.60E-13 K2=0.0 K3=0.0 # gg08_rnum=3841
CN + NO -> OCN + N										RTYPE=7 K1=1.62E-10 K2=0.0 K3=2.12E+04 # gg08_rnum=3842
CN + O2 -> O + OCN										RTYPE=7 K1=2.40E-11 K2=-6.00E-01 K3=0.0 # gg08_rnum=3843
CN + O2 -> CO + NO										RTYPE=7 K1=5.30E-13 K2=0.0 K3=0.0 # gg08_rnum=3844
CN + OH -> HCN + O										RTYPE=7 K1=1.00E-11 K2=0.0 K3=1.00E+03 # gg08_rnum=3845
CN + OH -> OCN + H										RTYPE=7 K1=7.01E-11 K2=0.0 K3=0.0 # gg08_rnum=3846
CN + S -> NS + C										RTYPE=7 K1=5.71E-11 K2=5.00E-01 K3=3.20E+04 # gg08_rnum=3847
CN + S -> CS + N										RTYPE=7 K1=1.73E-11 K2=5.00E-01 K3=0.0 # gg08_rnum=3848
CO + HNO -> NH + CO2									RTYPE=7 K1=3.32E-12 K2=0.0 K3=6.17E+03 # gg08_rnum=3849
CO + HS -> OCS + H										RTYPE=7 K1=5.95E-14 K2=1.12E+00 K3=8.33E+03 # gg08_rnum=3850
CO + N2O -> CO2 + N2									RTYPE=7 K1=1.62E-13 K2=0.0 K3=8.78E+03 # gg08_rnum=3851
CO + NO2 -> CO2 + NO									RTYPE=7 K1=1.48E-10 K2=0.0 K3=1.70E+04 # gg08_rnum=3852
CO + O2 -> CO2 + O										RTYPE=7 K1=5.99E-12 K2=0.0 K3=2.41E+04 # gg08_rnum=3853
CO + HO2 -> CO2 + OH									RTYPE=7 K1=5.60E-10 K2=0.0 K3=1.22E+04 # gg08_rnum=3854
CO + OH -> CO2 + H										RTYPE=7 K1=2.81E-13 K2=0.0 K3=1.76E+02 # gg08_rnum=3855
H + C2 -> CH + C										RTYPE=7 K1=4.67E-10 K2=5.00E-01 K3=3.04E+04 # gg08_rnum=3856
H + H2C2H -> HC2H + H2									RTYPE=7 K1=2.00E-11 K2=0.0 K3=0.0 # gg08_rnum=3857
H + CH -> C + H2										RTYPE=7 K1=2.70E-11 K2=3.80E-01 K3=0.0 # gg08_rnum=3858
H + CH2 -> CH + H2										RTYPE=7 K1=2.70E-10 K2=0.0 K3=0.0 # gg08_rnum=3859
H + CH3 -> CH2 + H2										RTYPE=7 K1=1.00E-10 K2=0.0 K3=7.60E+03 # gg08_rnum=3860
H + CH4 -> CH3 + H2										RTYPE=7 K1=7.34E-12 K2=0.0 K3=4.41E+03 # gg08_rnum=3861
H + CO -> OH + C										RTYPE=7 K1=1.10E-10 K2=5.00E-01 K3=7.77E+04 # gg08_rnum=3862
H + CO2 -> CO + OH										RTYPE=7 K1=2.51E-10 K2=0.0 K3=1.33E+04 # gg08_rnum=3863
H + H2CN -> H2 + HCN									RTYPE=7 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3864
H + H2CO -> HCO + H2									RTYPE=7 K1=2.14E-12 K2=1.62E+00 K3=1.09E+03 # gg08_rnum=3865
H + H2O -> OH + H2										RTYPE=7 K1=6.82E-12 K2=1.60E+00 K3=9.72E+03 # gg08_rnum=3866
H + H2O2 -> HO2 + H2									RTYPE=7 K1=2.81E-12 K2=0.0 K3=1.89E+03 # gg08_rnum=3867
H + H2O2 -> H2O + OH									RTYPE=7 K1=1.69E-11 K2=0.0 K3=1.80E+03 # gg08_rnum=3868
H + H2S -> HS + H2										RTYPE=7 K1=6.60E-11 K2=0.0 K3=1.35E+03 # gg08_rnum=3869
H + HCN -> CN + H2										RTYPE=7 K1=6.19E-10 K2=0.0 K3=1.25E+04 # gg08_rnum=3870
H + C2NCH -> HC2NC + H									RTYPE=7 K1=1.00E-11 K2=0.0 K3=0.0 # gg08_rnum=3871
H + HCO -> H2 + CO										RTYPE=7 K1=1.50E-10 K2=0.0 K3=0.0 # gg08_rnum=3872
H + HCO -> O + CH2										RTYPE=7 K1=6.61E-11 K2=0.0 K3=5.16E+04 # gg08_rnum=3873
H + HNC3 -> HC3N + H									RTYPE=7 K1=1.00E-11 K2=0.0 K3=0.0 # gg08_rnum=3874
H + HNO -> NO + H2										RTYPE=7 K1=4.50E-11 K2=7.20E-01 K3=3.29E+02 # gg08_rnum=3875
H + HNO -> NH2 + O										RTYPE=7 K1=1.05E-09 K2=-3.00E-01 K3=1.47E+04 # gg08_rnum=3876
H + HNO -> OH + NH										RTYPE=7 K1=2.41E-09 K2=-0.5 K3=9.01E+03 # gg08_rnum=3877
H + HS -> S + H2										RTYPE=7 K1=2.50E-11 K2=0.0 K3=0.0 # gg08_rnum=3878
H + N2O -> NO + NH										RTYPE=7 K1=2.94E-12 K2=5.00E-01 K3=1.51E+04 # gg08_rnum=3879
H + N2O -> OH + N2										RTYPE=7 K1=9.22E-14 K2=0.0 K3=2.99E+03 # gg08_rnum=3880
H + NH -> N + H2										RTYPE=7 K1=1.73E-11 K2=5.00E-01 K3=2.40E+03 # gg08_rnum=3881
H + NH2 -> NH + H2										RTYPE=7 K1=5.25E-12 K2=7.90E-01 K3=2.20E+03 # gg08_rnum=3882
H + NH3 -> NH2 + H2										RTYPE=7 K1=6.54E-13 K2=2.76E+00 K3=5.17E+03 # gg08_rnum=3883
H + NO -> O + NH										RTYPE=7 K1=9.30E-10 K2=-1.00E-01 K3=3.52E+04 # gg08_rnum=3884
H + NO -> N + OH										RTYPE=7 K1=3.60E-10 K2=0.0 K3=2.49E+04 # gg08_rnum=3885
H + NO2 -> NO + OH										RTYPE=7 K1=4.00E-10 K2=0.0 K3=3.40E+02 # gg08_rnum=3886
H + NS -> HS + N										RTYPE=7 K1=7.27E-11 K2=5.00E-01 K3=1.57E+04 # gg08_rnum=3887
H + NS -> S + NH										RTYPE=7 K1=7.27E-11 K2=5.00E-01 K3=2.07E+04 # gg08_rnum=3888
H + O2 -> OH + O										RTYPE=7 K1=2.94E-10 K2=0.0 K3=8.38E+03 # gg08_rnum=3889
H + HO2 -> OH + OH										RTYPE=7 K1=7.21E-11 K2=0.0 K3=0.0 # gg08_rnum=3890
H + HO2 -> H2O + O										RTYPE=7 K1=2.42E-12 K2=0.0 K3=0.0 # gg08_rnum=3891
H + HO2 -> O2 + H2										RTYPE=7 K1=5.60E-12 K2=0.0 K3=0.0 # gg08_rnum=3892
H + OCS -> CO + HS										RTYPE=7 K1=1.23E-11 K2=0.0 K3=1.95E+03 # gg08_rnum=3893
H + OH -> O + H2										RTYPE=7 K1=6.86E-14 K2=2.80E+00 K3=1.95E+03 # gg08_rnum=3894
H + S2 -> HS + S										RTYPE=7 K1=2.25E-10 K2=5.00E-01 K3=8.36E+03 # gg08_rnum=3895
H + SO -> HS + O										RTYPE=7 K1=1.73E-11 K2=5.00E-01 K3=1.99E+04 # gg08_rnum=3896
H + SO -> S + OH										RTYPE=7 K1=5.90E-10 K2=-3.10E-01 K3=1.11E+04 # gg08_rnum=3897
H2 + C -> CH + H										RTYPE=7 K1=6.64E-10 K2=0.0 K3=1.17E+04 # gg08_rnum=3898
H2 + CH -> CH2 + H										RTYPE=7 K1=3.75E-10 K2=0.0 K3=1.66E+03 # gg08_rnum=3899
H2 + CH2 -> CH3 + H										RTYPE=7 K1=5.00E-11 K2=0.0 K3=4.87E+03 # gg08_rnum=3900
H2 + CH3 -> CH4 + H										RTYPE=7 K1=2.51E-13 K2=0.0 K3=4.21E+03 # gg08_rnum=3901
H2 + CN -> HCN + H										RTYPE=7 K1=4.04E-13 K2=2.87E+00 K3=8.20E+02 # gg08_rnum=3902
H2 + CO2 -> CO + H2O									RTYPE=7 K1=2.94E-14 K2=5.00E-01 K3=7.55E+03 # gg08_rnum=3903
H2 + HS -> H2S + H										RTYPE=7 K1=6.52E-12 K2=9.00E-02 K3=8.05E+03 # gg08_rnum=3904
H2 + N -> NH + H										RTYPE=7 K1=4.65E-10 K2=0.0 K3=1.66E+04 # gg08_rnum=3905
H2 + NH -> NH2 + H										RTYPE=7 K1=5.96E-11 K2=0.0 K3=7.78E+03 # gg08_rnum=3906
H2 + NH2 -> NH3 + H										RTYPE=7 K1=1.76E-13 K2=2.23E+00 K3=3.61E+03 # gg08_rnum=3907
H2 + O -> OH + H										RTYPE=7 K1=3.44E-13 K2=2.67E+00 K3=3.16E+03 # gg08_rnum=3908
H2 + O2 -> OH + OH										RTYPE=7 K1=3.16E-10 K2=0.0 K3=2.19E+04 # gg08_rnum=3909
H2 + O2 -> HO2 + H										RTYPE=7 K1=2.40E-10 K2=0.0 K3=2.85E+04 # gg08_rnum=3910
H2 + HO2 -> H2O2 + H									RTYPE=7 K1=4.38E-12 K2=0.0 K3=1.08E+04 # gg08_rnum=3911
H2 + OH -> H2O + H										RTYPE=7 K1=8.40E-13 K2=0.0 K3=1.04E+03 # gg08_rnum=3912
H2 + S -> HS + H										RTYPE=7 K1=1.76E-13 K2=2.88E+00 K3=6.13E+03 # gg08_rnum=3913
H2CO + HO2 -> H2O2 + HCO								RTYPE=7 K1=3.30E-12 K2=0.0 K3=5.87E+03 # gg08_rnum=3914
H2O + HO2 -> H2O2 + OH									RTYPE=7 K1=4.65E-11 K2=0.0 K3=1.65E+04 # gg08_rnum=3915
HCO + CH3 -> CH4 + CO									RTYPE=7 K1=4.40E-11 K2=0.0 K3=0.0 # gg08_rnum=3916
HCO + HCO -> CO + CO + H2								RTYPE=7 K1=3.63E-11 K2=0.0 K3=0.0 # gg08_rnum=3917
HCO + HCO -> H2CO + CO									RTYPE=7 K1=5.00E-11 K2=0.0 K3=0.0 # gg08_rnum=3918
HCO + HNO -> H2CO + NO									RTYPE=7 K1=1.00E-12 K2=0.0 K3=9.76E+02 # gg08_rnum=3919
HCO + NO -> HNO + CO									RTYPE=7 K1=1.20E-11 K2=0.0 K3=0.0 # gg08_rnum=3920
HCO + O2 -> HO2 + CO									RTYPE=7 K1=5.58E-12 K2=0.0 K3=0.0 # gg08_rnum=3921
HCO + O2 -> CO2 + OH									RTYPE=7 K1=7.60E-13 K2=0.0 K3=0.0 # gg08_rnum=3922
HCO + HO2 -> O2 + H2CO									RTYPE=7 K1=5.00E-11 K2=0.0 K3=0.0 # gg08_rnum=3923
HNO + CH3 -> CH4 + NO									RTYPE=7 K1=3.32E-12 K2=0.0 K3=0.0 # gg08_rnum=3924
HS + HS -> H2S + S										RTYPE=7 K1=3.00E-11 K2=0.0 K3=0.0 # gg08_rnum=3925
N + C2 -> CN + C										RTYPE=7 K1=5.00E-11 K2=0.0 K3=0.0 # gg08_rnum=3926
N + C2H -> C2N + H										RTYPE=7 K1=1.70E-11 K2=0.0 K3=0.0 # gg08_rnum=3927
N + H2C2H -> HC2H + NH									RTYPE=7 K1=1.20E-11 K2=0.0 K3=0.0 # gg08_rnum=3928
N + H2C2H -> H2C2N + H									RTYPE=7 K1=6.20E-11 K2=0.0 K3=0.0 # gg08_rnum=3929
N + CH3CH2 -> H2C2H2 + NH								RTYPE=7 K1=7.15E-11 K2=0.0 K3=0.0 # gg08_rnum=3930
N + CH3CH2 -> H2CN + CH3								RTYPE=7 K1=3.85E-11 K2=0.0 K3=0.0 # gg08_rnum=3931
N + C2N -> CN + CN										RTYPE=7 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3932
N + C2O -> CN + CO										RTYPE=7 K1=5.50E-10 K2=0.0 K3=0.0 # gg08_rnum=3933
N + C3 -> CN + C2										RTYPE=7 K1=1.00E-13 K2=0.0 K3=0.0 # gg08_rnum=3934
N + C3H -> C3N + H										RTYPE=7 K1=1.70E-11 K2=0.0 K3=0.0 # gg08_rnum=3935
N + C3H2 -> HC3N + H									RTYPE=7 K1=1.00E-13 K2=0.0 K3=0.0 # gg08_rnum=3936
N + C3H3 -> HC3N + H2									RTYPE=7 K1=8.00E-11 K2=0.0 K3=0.0 # gg08_rnum=3937
N + C3N -> CN + C2N										RTYPE=7 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3938
N + C4 -> C3 + CN										RTYPE=7 K1=5.00E-11 K2=0.0 K3=0.0 # gg08_rnum=3939
N + C4H -> C4N + H										RTYPE=7 K1=1.70E-11 K2=0.0 K3=0.0 # gg08_rnum=3940
N + C4N -> CN + C3N										RTYPE=7 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3941
N + C5 -> CN + C4										RTYPE=7 K1=1.00E-13 K2=0.0 K3=0.0 # gg08_rnum=3942
N + C5H -> C5N + H										RTYPE=7 K1=1.70E-11 K2=0.0 K3=0.0 # gg08_rnum=3943
N + C5H2 -> HC5N + H									RTYPE=7 K1=1.00E-13 K2=0.0 K3=0.0 # gg08_rnum=3944
N + C5N -> CN + C4N										RTYPE=7 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3945
N + C6 -> CN + C5										RTYPE=7 K1=5.00E-11 K2=0.0 K3=0.0 # gg08_rnum=3946
N + C6H -> CN + C5H										RTYPE=7 K1=1.00E-13 K2=0.0 K3=0.0 # gg08_rnum=3947
N + C7 -> CN + C6										RTYPE=7 K1=1.00E-13 K2=0.0 K3=0.0 # gg08_rnum=3948
N + C7H -> C7N + H										RTYPE=7 K1=1.70E-11 K2=0.0 K3=0.0 # gg08_rnum=3949
N + C7H2 -> HC7N + H									RTYPE=7 K1=1.00E-13 K2=0.0 K3=0.0 # gg08_rnum=3950
N + C7N -> C2N + C5N									RTYPE=7 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3951
N + C8 -> CN + C7										RTYPE=7 K1=5.00E-11 K2=0.0 K3=0.0 # gg08_rnum=3952
N + C8H -> CN + C7H										RTYPE=7 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3953
N + C9 -> CN + C8										RTYPE=7 K1=1.00E-13 K2=0.0 K3=0.0 # gg08_rnum=3954
N + C9H -> C9N + H										RTYPE=7 K1=1.70E-11 K2=0.0 K3=0.0 # gg08_rnum=3955
N + C9N -> C2N + C7N									RTYPE=7 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3956
N + CH -> CN + H										RTYPE=7 K1=1.66E-10 K2=-9.00E-02 K3=0.0 # gg08_rnum=3957
N + CH2 -> HNC + H										RTYPE=7 K1=3.95E-11 K2=1.67E-01 K3=0.0 # gg08_rnum=3958
N + CH2 -> NH + CH										RTYPE=7 K1=9.96E-13 K2=0.0 K3=2.04E+04 # gg08_rnum=3959
N + CH2 -> HCN + H										RTYPE=7 K1=3.95E-11 K2=1.67E-01 K3=0.0 # gg08_rnum=3960
N + CH3 -> H2CN + H										RTYPE=7 K1=8.60E-11 K2=0.0 K3=0.0 # gg08_rnum=3961
N + CH3 -> HCN + H2										RTYPE=7 K1=1.30E-11 K2=5.00E-01 K3=0.0 # gg08_rnum=3962
N + CH3 -> HCN + H + H									RTYPE=7 K1=3.32E-13 K2=0.0 K3=0.0 # gg08_rnum=3963
N + CN -> C + N2										RTYPE=7 K1=3.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3964
N + CO2 -> NO + CO										RTYPE=7 K1=3.20E-13 K2=0.0 K3=1.71E+03 # gg08_rnum=3965
N + CS -> S + CN										RTYPE=7 K1=3.80E-11 K2=5.00E-01 K3=1.16E+03 # gg08_rnum=3966
N + H2CN -> NH + HCN									RTYPE=7 K1=3.70E-11 K2=0.0 K3=0.0 # gg08_rnum=3967
N + HCO -> OCN + H										RTYPE=7 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3968
N + HCO -> CO + NH										RTYPE=7 K1=5.71E-12 K2=5.00E-01 K3=1.00E+03 # gg08_rnum=3969
N + HCO -> HCN + O										RTYPE=7 K1=1.70E-10 K2=0.0 K3=0.0 # gg08_rnum=3970
N + HCS -> HCN + S										RTYPE=7 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3971
N + HNO -> NO + NH										RTYPE=7 K1=2.94E-12 K2=5.00E-01 K3=1.00E+03 # gg08_rnum=3972
N + HNO -> N2O + H										RTYPE=7 K1=1.43E-12 K2=5.00E-01 K3=1.50E+03 # gg08_rnum=3973
N + HS -> NS + H										RTYPE=7 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3974
N + HS -> S + NH										RTYPE=7 K1=1.73E-11 K2=5.00E-01 K3=9.06E+03 # gg08_rnum=3975
N + NH -> N2 + H										RTYPE=7 K1=5.00E-11 K2=0.0 K3=0.0 # gg08_rnum=3976
N + NO -> N2 + O										RTYPE=7 K1=3.00E-11 K2=-6.00E-01 K3=0.0 # gg08_rnum=3977
N + NO2 -> N2 + O2										RTYPE=7 K1=1.00E-12 K2=0.0 K3=0.0 # gg08_rnum=3978
N + NO2 -> NO + NO										RTYPE=7 K1=1.00E-12 K2=0.0 K3=0.0 # gg08_rnum=3979
N + NO2 -> N2O + O										RTYPE=7 K1=2.10E-11 K2=0.0 K3=0.0 # gg08_rnum=3980
N + NO2 -> N2 + O + O									RTYPE=7 K1=2.41E-12 K2=0.0 K3=0.0 # gg08_rnum=3981
N + NS -> N2 + S										RTYPE=7 K1=3.00E-11 K2=-6.00E-01 K3=0.0 # gg08_rnum=3982
N + O2 -> NO + O										RTYPE=7 K1=1.50E-11 K2=0.0 K3=3.68E+03 # gg08_rnum=3983
N + HO2 -> NH + O2										RTYPE=7 K1=1.70E-13 K2=0.0 K3=0.0 # gg08_rnum=3984
N + OH -> NO + H										RTYPE=7 K1=7.50E-11 K2=-1.80E-01 K3=0.0 # gg08_rnum=3985
N + OH -> O + NH										RTYPE=7 K1=1.88E-11 K2=1.00E-01 K3=1.07E+04 # gg08_rnum=3986
N + PH -> PN + H										RTYPE=7 K1=5.00E-11 K2=0.0 K3=0.0 # gg08_rnum=3987
N + PN -> P + N2										RTYPE=7 K1=1.00E-18 K2=0.0 K3=0.0 # gg08_rnum=3988
N + PO -> PN + O										RTYPE=7 K1=3.00E-11 K2=-6.00E-01 K3=0.0 # gg08_rnum=3989
N + PO -> P + NO										RTYPE=7 K1=2.55E-12 K2=0.0 K3=0.0 # gg08_rnum=3990
N + S2 -> NS + S										RTYPE=7 K1=1.73E-11 K2=5.00E-01 K3=4.00E+03 # gg08_rnum=3991
N + SiC -> SiN + C										RTYPE=7 K1=5.00E-11 K2=0.0 K3=0.0 # gg08_rnum=3992
N + SiC -> CN + Si										RTYPE=7 K1=5.00E-11 K2=0.0 K3=0.0 # gg08_rnum=3993
N + SiH -> SiN + H										RTYPE=7 K1=1.66E-10 K2=-9.00E-02 K3=0.0 # gg08_rnum=3994
N + SiH2 -> HNSi + H									RTYPE=7 K1=8.00E-11 K2=1.67E-01 K3=0.0 # gg08_rnum=3995
N + SiH3 -> HNSi + H2									RTYPE=7 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3996
N + SO -> NO + S										RTYPE=7 K1=1.50E-11 K2=0.0 K3=3.68E+03 # gg08_rnum=3997
N + SO -> NS + O										RTYPE=7 K1=4.68E-11 K2=5.00E-01 K3=8.25E+03 # gg08_rnum=3998
N2 + O2 -> N2O + O										RTYPE=7 K1=1.00E-10 K2=0.0 K3=5.52E+04 # gg08_rnum=3999
NH + CN -> HCN + N										RTYPE=7 K1=2.94E-12 K2=5.00E-01 K3=1.00E+03 # gg08_rnum=4000
NH + H2O -> OH + NH2									RTYPE=7 K1=1.83E-12 K2=1.60E+00 K3=1.41E+04 # gg08_rnum=4001
NH + NH -> NH2 + N										RTYPE=7 K1=4.35E-13 K2=0.0 K3=0.0 # gg08_rnum=4002
NH + NH -> N2 + H + H									RTYPE=7 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=4003
NH + NH -> N2 + H2										RTYPE=7 K1=1.70E-11 K2=0.0 K3=0.0 # gg08_rnum=4004
NH + NO -> N2O + H										RTYPE=7 K1=3.12E-11 K2=0.0 K3=0.0 # gg08_rnum=4005
NH + NO -> N2 + OH										RTYPE=7 K1=1.46E-11 K2=-5.80E-01 K3=3.70E+01 # gg08_rnum=4006
NH + NO -> O + N2 + H									RTYPE=7 K1=4.70E-11 K2=5.00E-01 K3=0.0 # gg08_rnum=4007
NH + NO2 -> N2O + OH									RTYPE=7 K1=4.30E-11 K2=0.0 K3=0.0 # gg08_rnum=4008
NH + NO2 -> HNO + NO									RTYPE=7 K1=5.72E-12 K2=5.00E-01 K3=2.50E+03 # gg08_rnum=4009
NH + OH -> H2O + N										RTYPE=7 K1=3.11E-12 K2=1.20E+00 K3=0.0 # gg08_rnum=4010
NH + OH -> HNO + H										RTYPE=7 K1=3.32E-11 K2=0.0 K3=0.0 # gg08_rnum=4011
NH + OH -> NH2 + O										RTYPE=7 K1=2.93E-12 K2=1.00E-01 K3=5.80E+03 # gg08_rnum=4012
NH + S -> HS + N										RTYPE=7 K1=1.73E-11 K2=5.00E-01 K3=4.00E+03 # gg08_rnum=4013
NH2 + H2CO -> NH2CHO + H								RTYPE=7 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=4014
NH2 + NO -> N2 + OH + H									RTYPE=7 K1=1.49E-12 K2=0.0 K3=0.0 # gg08_rnum=4015
NO + HNO -> N2O + OH									RTYPE=7 K1=1.41E-11 K2=0.0 K3=1.49E+04 # gg08_rnum=4016
NO + N2O -> NO2 + N2									RTYPE=7 K1=2.92E-13 K2=2.23E+00 K3=2.33E+04 # gg08_rnum=4017
NO + NH2 -> N2 + H2O									RTYPE=7 K1=1.70E-11 K2=0.0 K3=0.0 # gg08_rnum=4018
NO + NO -> N2O + O										RTYPE=7 K1=7.22E-12 K2=0.0 K3=3.32E+04 # gg08_rnum=4019
NO + NO -> O2 + N2										RTYPE=7 K1=2.51E-11 K2=0.0 K3=3.07E+04 # gg08_rnum=4020
NO + O2 -> NO2 + O										RTYPE=7 K1=2.80E-12 K2=0.0 K3=2.34E+04 # gg08_rnum=4021
NO + S -> SO + N										RTYPE=7 K1=1.75E-10 K2=0.0 K3=2.02E+04 # gg08_rnum=4022
NO + S -> NS + O										RTYPE=7 K1=2.94E-11 K2=5.00E-01 K3=1.75E+04 # gg08_rnum=4023
O + C10 -> CO + C9										RTYPE=7 K1=5.00E-11 K2=0.0 K3=0.0 # gg08_rnum=4024
O + C2 -> CO + C										RTYPE=7 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=4025
O + C2H -> CO + CH										RTYPE=7 K1=1.70E-11 K2=0.0 K3=0.0 # gg08_rnum=4026
O + HC2H -> C2H + OH									RTYPE=7 K1=5.30E-09 K2=0.0 K3=8.52E+03 # gg08_rnum=4027
O + HC2H -> CO + CH2									RTYPE=7 K1=8.39E-12 K2=1.03E+00 K3=1.20E+03 # gg08_rnum=4028
O + H2C2H -> CH2CO + H									RTYPE=7 K1=1.60E-10 K2=0.0 K3=0.0 # gg08_rnum=4029
O + H2C2H2 -> H2CO + CH2								RTYPE=7 K1=4.20E-11 K2=0.0 K3=2.52E+03 # gg08_rnum=4030
O + H2C2H2 -> H2C2H + OH								RTYPE=7 K1=2.42E-13 K2=2.13E+00 K3=1.34E+03 # gg08_rnum=4031
O + H2C2H2 -> HCO + CH3									RTYPE=7 K1=1.52E-12 K2=1.55E+00 K3=2.15E+02 # gg08_rnum=4032
O + H2C2H2 -> CH2CO + H2								RTYPE=7 K1=5.11E-14 K2=1.88E+00 K3=9.20E+01 # gg08_rnum=4033
O + CH3CH2 -> CH3CHO + H								RTYPE=7 K1=1.33E-10 K2=0.0 K3=0.0 # gg08_rnum=4034
O + CH3CH2 -> H2CO + CH3								RTYPE=7 K1=2.65E-11 K2=0.0 K3=0.0 # gg08_rnum=4035
O + C2N -> CO + CN										RTYPE=7 K1=6.00E-12 K2=0.0 K3=0.0 # gg08_rnum=4036
O + C2O -> CO + CO										RTYPE=7 K1=8.60E-11 K2=0.0 K3=0.0 # gg08_rnum=4037
O + C3 -> CO + C2										RTYPE=7 K1=5.00E-12 K2=0.0 K3=9.00E+02 # gg08_rnum=4038
O + C3H -> C2H + CO										RTYPE=7 K1=1.70E-11 K2=0.0 K3=0.0 # gg08_rnum=4039
O + C3H2 -> CO + HC2H									RTYPE=7 K1=5.00E-11 K2=5.00E-01 K3=0.0 # gg08_rnum=4040
O + C3N -> CO + C2N										RTYPE=7 K1=4.00E-11 K2=0.0 K3=0.0 # gg08_rnum=4041
O + C3O -> C2O + CO										RTYPE=7 K1=5.00E-12 K2=0.0 K3=9.00E+02 # gg08_rnum=4042
O + C3O -> C3 + O2										RTYPE=7 K1=1.00E-11 K2=0.0 K3=0.0 # gg08_rnum=4043
O + C3P -> CCP + CO										RTYPE=7 K1=4.00E-11 K2=0.0 K3=0.0 # gg08_rnum=4044
O + C4 -> CO + C3										RTYPE=7 K1=5.00E-11 K2=0.0 K3=0.0 # gg08_rnum=4045
O + C4H -> C3H + CO										RTYPE=7 K1=1.70E-11 K2=0.0 K3=0.0 # gg08_rnum=4046
O + C4N -> C3N + CO										RTYPE=7 K1=6.00E-12 K2=0.0 K3=0.0 # gg08_rnum=4047
O + C4P -> C3P + CO										RTYPE=7 K1=1.00E-11 K2=0.0 K3=0.0 # gg08_rnum=4048
O + C5 -> CO + C4										RTYPE=7 K1=5.00E-12 K2=0.0 K3=9.00E+02 # gg08_rnum=4049
O + C5H -> CO + C4H										RTYPE=7 K1=1.70E-11 K2=0.0 K3=0.0 # gg08_rnum=4050
O + C5N -> C4 + OCN										RTYPE=7 K1=2.00E-11 K2=5.00E-01 K3=0.0 # gg08_rnum=4051
O + C5N -> C4N + CO										RTYPE=7 K1=4.00E-11 K2=0.0 K3=0.0 # gg08_rnum=4052
O + C6 -> CO + C5										RTYPE=7 K1=5.00E-11 K2=0.0 K3=0.0 # gg08_rnum=4053
O + C6H -> CO + C5H										RTYPE=7 K1=1.70E-11 K2=0.0 K3=0.0 # gg08_rnum=4054
O + C7 -> CO + C6										RTYPE=7 K1=5.00E-12 K2=0.0 K3=9.00E+02 # gg08_rnum=4055
O + C7H -> CO + C6H										RTYPE=7 K1=1.70E-11 K2=0.0 K3=0.0 # gg08_rnum=4056
O + C7N -> OCN + C6										RTYPE=7 K1=4.00E-11 K2=0.0 K3=0.0 # gg08_rnum=4057
O + C8 -> CO + C7										RTYPE=7 K1=5.00E-11 K2=0.0 K3=0.0 # gg08_rnum=4058
O + C8H -> CO + C7H										RTYPE=7 K1=1.70E-11 K2=0.0 K3=0.0 # gg08_rnum=4059
O + C9 -> CO + C8										RTYPE=7 K1=5.00E-12 K2=0.0 K3=9.00E+02 # gg08_rnum=4060
O + C9H -> CO + C8H										RTYPE=7 K1=1.70E-11 K2=0.0 K3=0.0 # gg08_rnum=4061
O + C9N -> OCN + C8										RTYPE=7 K1=4.00E-11 K2=0.0 K3=0.0 # gg08_rnum=4062
O + ClC -> Cl + CO										RTYPE=7 K1=9.96E-11 K2=0.0 K3=0.0 # gg08_rnum=4063
O + ClC -> ClO + C										RTYPE=7 K1=1.38E-10 K2=0.0 K3=1.60E+04 # gg08_rnum=4064
O + CCP -> CP + CO										RTYPE=7 K1=6.00E-12 K2=0.0 K3=0.0 # gg08_rnum=4065
O + CH -> CO + H										RTYPE=7 K1=6.60E-11 K2=0.0 K3=0.0 # gg08_rnum=4066
O + CH2 -> CO + H + H									RTYPE=7 K1=1.20E-10 K2=0.0 K3=0.0 # gg08_rnum=4067
O + CH2 -> CO + H2										RTYPE=7 K1=8.00E-11 K2=0.0 K3=0.0 # gg08_rnum=4068
O + CH2PH -> PH2 + CO + H								RTYPE=7 K1=4.00E-11 K2=0.0 K3=0.0 # gg08_rnum=4069
O + CH3 -> H2CO + H										RTYPE=7 K1=1.40E-10 K2=0.0 K3=0.0 # gg08_rnum=4070
O + CH4 -> OH + CH3										RTYPE=7 K1=2.29E-12 K2=2.20E+00 K3=3.82E+03 # gg08_rnum=4071
O + ClO -> Cl + O2										RTYPE=7 K1=4.00E-11 K2=0.0 K3=0.0 # gg08_rnum=4072
O + CN -> CO + N										RTYPE=7 K1=4.00E-11 K2=0.0 K3=0.0 # gg08_rnum=4073
O + CN -> NO + C										RTYPE=7 K1=3.81E-11 K2=5.00E-01 K3=1.45E+04 # gg08_rnum=4074
O + CO2 -> O2 + CO										RTYPE=7 K1=2.46E-11 K2=0.0 K3=2.66E+04 # gg08_rnum=4075
O + CP -> P + CO										RTYPE=7 K1=4.00E-11 K2=0.0 K3=0.0 # gg08_rnum=4076
O + CS -> CO + S										RTYPE=7 K1=1.94E-11 K2=0.0 K3=2.31E+02 # gg08_rnum=4077
O + CS -> SO + C										RTYPE=7 K1=4.68E-11 K2=5.00E-01 K3=2.89E+04 # gg08_rnum=4078
O + H2CN -> OCN + H2									RTYPE=7 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=4079
O + H2CO -> CO + OH + H									RTYPE=7 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=4080
O + H2CO -> HCO + OH									RTYPE=7 K1=1.07E-11 K2=1.17E+00 K3=1.24E+03 # gg08_rnum=4081
O + H2O -> HO2 + H										RTYPE=7 K1=1.23E-53 K2=0.0 K3=0.0 # gg08_rnum=4082
O + H2O -> OH + OH										RTYPE=7 K1=1.85E-11 K2=9.50E-01 K3=8.57E+03 # gg08_rnum=4083
O + H2O2 -> HO2 + OH									RTYPE=7 K1=8.54E-14 K2=3.25E+00 K3=1.20E+03 # gg08_rnum=4084
O + H2S -> HS + OH										RTYPE=7 K1=9.22E-12 K2=0.0 K3=1.80E+03 # gg08_rnum=4085
O + HCCP -> HCP + CO									RTYPE=7 K1=4.00E-11 K2=0.0 K3=0.0 # gg08_rnum=4086
O + HCN -> CO + NH										RTYPE=7 K1=7.30E-13 K2=1.14E+00 K3=3.74E+03 # gg08_rnum=4087
O + HCN -> OCN + H										RTYPE=7 K1=3.61E-13 K2=2.10E+00 K3=3.08E+03 # gg08_rnum=4088
O + HCN -> CN + OH										RTYPE=7 K1=6.21E-10 K2=0.0 K3=1.24E+04 # gg08_rnum=4089
O + HCO -> OH + CO										RTYPE=7 K1=5.00E-11 K2=0.0 K3=0.0 # gg08_rnum=4090
O + HCO -> H + CO2										RTYPE=7 K1=5.00E-11 K2=0.0 K3=0.0 # gg08_rnum=4091
O + HCP -> CO + PH										RTYPE=7 K1=3.61E-13 K2=2.10E+00 K3=3.08E+03 # gg08_rnum=4092
O + HCS -> OCS + H										RTYPE=7 K1=5.00E-11 K2=0.0 K3=0.0 # gg08_rnum=4093
O + HCS -> OH + CS										RTYPE=7 K1=5.00E-11 K2=0.0 K3=0.0 # gg08_rnum=4094
O + SiCH -> SiO + CH									RTYPE=7 K1=2.00E-11 K2=0.0 K3=0.0 # gg08_rnum=4095
O + HNO -> OH + NO										RTYPE=7 K1=3.80E-11 K2=0.0 K3=0.0 # gg08_rnum=4096
O + HNO -> NO2 + H										RTYPE=7 K1=1.00E-12 K2=0.0 K3=0.0 # gg08_rnum=4097
O + HNO -> O2 + NH										RTYPE=7 K1=2.94E-12 K2=5.00E-01 K3=3.50E+03 # gg08_rnum=4098
O + HPO -> PO + OH										RTYPE=7 K1=3.80E-11 K2=0.0 K3=0.0 # gg08_rnum=4099
O + HS -> S + OH										RTYPE=7 K1=1.74E-11 K2=6.70E-01 K3=9.56E+02 # gg08_rnum=4100
O + HS -> SO + H										RTYPE=7 K1=1.60E-10 K2=5.00E-01 K3=0.0 # gg08_rnum=4101
O + N2 -> NO + N										RTYPE=7 K1=2.51E-10 K2=0.0 K3=3.86E+04 # gg08_rnum=4102
O + N2O -> NO + NO										RTYPE=7 K1=1.15E-10 K2=0.0 K3=1.34E+04 # gg08_rnum=4103
O + N2O -> O2 + N2										RTYPE=7 K1=1.66E-10 K2=0.0 K3=1.41E+04 # gg08_rnum=4104
O + NH -> NO + H										RTYPE=7 K1=1.16E-10 K2=0.0 K3=0.0 # gg08_rnum=4105
O + NH -> OH + N										RTYPE=7 K1=1.16E-11 K2=0.0 K3=0.0 # gg08_rnum=4106
O + NH2 -> NH + OH										RTYPE=7 K1=2.00E-11 K2=0.0 K3=0.0 # gg08_rnum=4107
O + NH2 -> NO + H2										RTYPE=7 K1=1.00E-11 K2=0.0 K3=0.0 # gg08_rnum=4108
O + NH2 -> HNO + H										RTYPE=7 K1=8.00E-11 K2=0.0 K3=0.0 # gg08_rnum=4109
O + NH3 -> OH + NH2										RTYPE=7 K1=1.89E-11 K2=0.0 K3=4.00E+03 # gg08_rnum=4110
O + NO -> O2 + N										RTYPE=7 K1=1.18E-11 K2=0.0 K3=2.04E+04 # gg08_rnum=4111
O + NO2 -> NO + O2										RTYPE=7 K1=1.00E-11 K2=0.0 K3=0.0 # gg08_rnum=4112
O + NS -> SO + N										RTYPE=7 K1=1.00E-11 K2=0.0 K3=0.0 # gg08_rnum=4113
O + NS -> NO + S										RTYPE=7 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=4114
O + HO2 -> OH + O2										RTYPE=7 K1=5.30E-11 K2=0.0 K3=0.0 # gg08_rnum=4115
O + OCN -> NO + CO										RTYPE=7 K1=1.50E-11 K2=0.0 K3=2.00E+02 # gg08_rnum=4116
O + OCN -> CN + O2										RTYPE=7 K1=4.05E-10 K2=-1.43E+00 K3=3.50E+03 # gg08_rnum=4117
O + OCS -> SO + CO										RTYPE=7 K1=1.60E-11 K2=0.0 K3=2.15E+03 # gg08_rnum=4118
O + OCS -> CO2 + S										RTYPE=7 K1=8.30E-11 K2=0.0 K3=5.53E+03 # gg08_rnum=4119
O + OH -> O2 + H										RTYPE=7 K1=7.50E-11 K2=-2.50E-01 K3=0.0 # gg08_rnum=4120
O + PH -> PO + H										RTYPE=7 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=4121
O + PH2 -> PO + H2										RTYPE=7 K1=4.00E-11 K2=0.0 K3=0.0 # gg08_rnum=4122
O + PH2 -> HPO + H										RTYPE=7 K1=8.00E-11 K2=0.0 K3=0.0 # gg08_rnum=4123
O + PH2 -> PH + OH										RTYPE=7 K1=2.00E-11 K2=0.0 K3=0.0 # gg08_rnum=4124
O + S2 -> SO + S										RTYPE=7 K1=1.70E-11 K2=0.0 K3=0.0 # gg08_rnum=4125
O + SiC -> CO + Si										RTYPE=7 K1=5.00E-11 K2=0.0 K3=0.0 # gg08_rnum=4126
O + SiC -> SiO + C										RTYPE=7 K1=5.00E-11 K2=0.0 K3=0.0 # gg08_rnum=4127
O + SiC2 -> SiC + CO									RTYPE=7 K1=4.00E-11 K2=0.0 K3=0.0 # gg08_rnum=4128
O + cSiC3 -> SiC2 + CO									RTYPE=7 K1=4.00E-11 K2=0.0 K3=0.0 # gg08_rnum=4129
O + SiC4 -> cSiC3 + CO									RTYPE=7 K1=4.00E-11 K2=0.0 K3=0.0 # gg08_rnum=4130
O + SiH -> SiO + H										RTYPE=7 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=4131
O + SiH2 -> SiO + H2									RTYPE=7 K1=8.00E-11 K2=0.0 K3=0.0 # gg08_rnum=4132
O + SiH2 -> SiO + H + H									RTYPE=7 K1=1.20E-10 K2=0.0 K3=0.0 # gg08_rnum=4133
O + SiH3 -> H2SiO + H									RTYPE=7 K1=1.40E-10 K2=0.0 K3=0.0 # gg08_rnum=4134
O + SiN -> SiO + N										RTYPE=7 K1=5.00E-11 K2=0.0 K3=0.0 # gg08_rnum=4135
O + SiN -> NO + Si										RTYPE=7 K1=5.00E-11 K2=0.0 K3=0.0 # gg08_rnum=4136
O + SiNC -> SiN + CO									RTYPE=7 K1=1.00E-11 K2=0.0 K3=0.0 # gg08_rnum=4137
O + SO -> S + O2										RTYPE=7 K1=6.60E-13 K2=0.0 K3=2.76E+03 # gg08_rnum=4138
O + SO2 -> SO + O2										RTYPE=7 K1=9.01E-12 K2=0.0 K3=9.84E+03 # gg08_rnum=4139
O2 + C3 -> CO2 + C2										RTYPE=7 K1=1.00E-12 K2=0.0 K3=0.0 # gg08_rnum=4140
O2 + C3 -> CO + C2 + O									RTYPE=7 K1=1.00E-12 K2=0.0 K3=0.0 # gg08_rnum=4141
O2 + OCN -> NO2 + CO									RTYPE=7 K1=8.10E-11 K2=0.0 K3=7.73E+02 # gg08_rnum=4142
O2 + OCN -> CO2 + NO									RTYPE=7 K1=1.32E-12 K2=0.0 K3=0.0 # gg08_rnum=4143
O2 + SO -> SO2 + O										RTYPE=7 K1=1.10E-14 K2=1.89E+00 K3=1.54E+03 # gg08_rnum=4144
HO2 + HO2 -> H2O2 + O2									RTYPE=7 K1=5.64E-12 K2=0.0 K3=0.0 # gg08_rnum=4145
OH + HC2H -> CH2CO + H									RTYPE=7 K1=1.84E-11 K2=0.0 K3=5.03E+03 # gg08_rnum=4146
OH + HC2H -> CO + CH3									RTYPE=7 K1=3.76E-16 K2=0.0 K3=0.0 # gg08_rnum=4147
OH + HC2H -> C2H + H2O									RTYPE=7 K1=1.05E-13 K2=2.68E+00 K3=6.06E+03 # gg08_rnum=4148
OH + H2C2H -> HC2H + H2O								RTYPE=7 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=4149
OH + CH3CH2 -> H2C2H2 + H2O								RTYPE=7 K1=4.00E-11 K2=0.0 K3=0.0 # gg08_rnum=4150
OH + CH3CH2 -> CH3CH3 + O								RTYPE=7 K1=1.04E-18 K2=8.80E+00 K3=2.50E+02 # gg08_rnum=4151
OH + CH2 -> H2CO + H									RTYPE=7 K1=3.00E-10 K2=0.0 K3=0.0 # gg08_rnum=4152
OH + HCOH -> CO2 + H2 + H								RTYPE=7 K1=0.0 K2=0.0 K3=0.0 # gg08_rnum=4153
OH + CS -> OCS + H										RTYPE=7 K1=9.39E-14 K2=1.12E+00 K3=8.00E+02 # gg08_rnum=4154
OH + H2CO -> HCO + H2O									RTYPE=7 K1=1.00E-11 K2=0.0 K3=0.0 # gg08_rnum=4155
OH + H2CO -> HCOOH + H									RTYPE=7 K1=2.01E-13 K2=0.0 K3=0.0 # gg08_rnum=4156
OH + H2O2 -> H2O + HO2									RTYPE=7 K1=2.91E-12 K2=0.0 K3=1.60E+02 # gg08_rnum=4157
OH + H2S -> H2O + HS									RTYPE=7 K1=6.00E-12 K2=0.0 K3=7.50E+01 # gg08_rnum=4158
OH + HCN -> CO + NH2									RTYPE=7 K1=1.07E-13 K2=0.0 K3=5.89E+03 # gg08_rnum=4159
OH + HCN -> CN + H2O									RTYPE=7 K1=1.87E-13 K2=1.50E+00 K3=3.89E+03 # gg08_rnum=4160
OH + HCO -> H2O + CO									RTYPE=7 K1=1.69E-10 K2=0.0 K3=0.0 # gg08_rnum=4161
OH + HNO -> H2O + NO									RTYPE=7 K1=8.00E-11 K2=0.0 K3=5.00E+02 # gg08_rnum=4162
OH + N2O -> HO2 + N2									RTYPE=7 K1=3.70E-13 K2=0.0 K3=2.74E+03 # gg08_rnum=4163
OH + N2O -> HNO + NO									RTYPE=7 K1=1.04E-17 K2=4.33E+00 K3=1.26E+04 # gg08_rnum=4164
OH + NH2 -> NH3 + O										RTYPE=7 K1=9.18E-14 K2=0.0 K3=0.0 # gg08_rnum=4165
OH + NH2 -> NH + H2O									RTYPE=7 K1=1.50E-12 K2=0.0 K3=0.0 # gg08_rnum=4166
OH + NH3 -> H2O + NH2									RTYPE=7 K1=1.47E-13 K2=2.05E+00 K3=7.00E+00 # gg08_rnum=4167
OH + NO -> NO2 + H										RTYPE=7 K1=5.20E-12 K2=0.0 K3=1.51E+04 # gg08_rnum=4168
OH + HO2 -> H2O + O2									RTYPE=7 K1=1.20E-10 K2=0.0 K3=0.0 # gg08_rnum=4169
OH + OH -> HO2 + H										RTYPE=7 K1=1.82E-40 K2=0.0 K3=0.0 # gg08_rnum=4170
OH + OH -> H2O + O										RTYPE=7 K1=1.65E-12 K2=1.14E+00 K3=5.00E+01 # gg08_rnum=4171
OH + S -> HS + O										RTYPE=7 K1=6.60E-11 K2=0.0 K3=0.0 # gg08_rnum=4172
OH + SiO -> SiO2 + H									RTYPE=7 K1=2.00E-12 K2=0.0 K3=0.0 # gg08_rnum=4173
OH + SO -> SO2 + H										RTYPE=7 K1=8.60E-11 K2=0.0 K3=0.0 # gg08_rnum=4174
P + O2 -> PO + O										RTYPE=7 K1=1.00E-13 K2=0.0 K3=0.0 # gg08_rnum=4175
S + C2 -> CS + C										RTYPE=7 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=4176
S + CH -> CS + H										RTYPE=7 K1=5.00E-11 K2=0.0 K3=0.0 # gg08_rnum=4177
S + CH2 -> CS + H2										RTYPE=7 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=4178
S + CH2 -> HCS + H										RTYPE=7 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=4179
S + CH3 -> H2CS + H										RTYPE=7 K1=1.40E-10 K2=0.0 K3=0.0 # gg08_rnum=4180
S + HS -> S2 + H										RTYPE=7 K1=4.50E-11 K2=0.0 K3=0.0 # gg08_rnum=4181
S + NH -> NS + H										RTYPE=7 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=4182
S + O2 -> SO + O										RTYPE=7 K1=2.30E-12 K2=0.0 K3=0.0 # gg08_rnum=4183
S + OH -> SO + H										RTYPE=7 K1=6.60E-11 K2=0.0 K3=0.0 # gg08_rnum=4184
S + SO -> S2 + O										RTYPE=7 K1=1.73E-11 K2=5.00E-01 K3=1.15E+04 # gg08_rnum=4185
Si + HC2H -> SiC2H + H									RTYPE=7 K1=2.70E-10 K2=-1.10E-01 K3=0.0 # gg08_rnum=4186
Si + CH2 -> SiCH + H									RTYPE=7 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=4187
Si + CH3 -> SiCH2 + H									RTYPE=7 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=4188
Si + NO -> SiO + N										RTYPE=7 K1=9.00E-11 K2=-9.60E-01 K3=2.80E+01 # gg08_rnum=4189
Si + O2 -> SiO + O										RTYPE=7 K1=1.72E-10 K2=-5.30E-01 K3=1.70E+01 # gg08_rnum=4190
Si + OH -> SiO + H										RTYPE=7 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=4191
C + C -> C2												RTYPE=8 K1=1.00E-17 K2=0.0 K3=0.0 # gg08_rnum=4192
C + C2 -> C3											RTYPE=8 K1=3.30E-16 K2=-1.00E+00 K3=0.0 # gg08_rnum=4193
C + C3 -> C4											RTYPE=8 K1=3.30E-14 K2=-1.00E+00 K3=0.0 # gg08_rnum=4194
C + H -> CH												RTYPE=8 K1=1.00E-17 K2=0.0 K3=0.0 # gg08_rnum=4195
C + H2 -> CH2											RTYPE=8 K1=1.00E-17 K2=0.0 K3=0.0 # gg08_rnum=4196
C + N -> CN												RTYPE=8 K1=1.00E-17 K2=0.0 K3=0.0 # gg08_rnum=4197
C + O -> CO												RTYPE=8 K1=2.10E-19 K2=0.0 K3=0.0 # gg08_rnum=4198
CH + H2 -> CH3											RTYPE=8 K1=3.25E-17 K2=-6.00E-01 K3=0.0 # gg08_rnum=4199
CN + C2H -> HC3N										RTYPE=8 K1=1.00E-16 K2=0.0 K3=0.0 # gg08_rnum=4200
CN + CH3 -> CH3CN										RTYPE=8 K1=1.00E-16 K2=0.0 K3=0.0 # gg08_rnum=4201
H + O -> OH												RTYPE=8 K1=9.90E-19 K2=-3.80E-01 K3=0.0 # gg08_rnum=4202
H + OH -> H2O											RTYPE=8 K1=4.00E-18 K2=-2.00E+00 K3=0.0 # gg08_rnum=4203
O + O -> O2												RTYPE=8 K1=4.90E-20 K2=1.58E+00 K3=0.0 # gg08_rnum=4204
O + SO -> SO2											RTYPE=8 K1=3.20E-16 K2=-1.60E+00 K3=0.0 # gg08_rnum=4205
OH + OH -> H2O2											RTYPE=8 K1=1.00E-18 K2=-2.00E+00 K3=0.0 # gg08_rnum=4206
S + CO -> OCS											RTYPE=8 K1=1.60E-17 K2=-1.50E+00 K3=0.0 # gg08_rnum=4207
C101+ + E1- -> C8 + C2									RTYPE=9 K1=1.00E-06 K2=-3.00E-01 K3=0.0 # gg08_rnum=4208
C101+ + E1- -> C9 + C									RTYPE=9 K1=1.00E-06 K2=-3.00E-01 K3=0.0 # gg08_rnum=4209
C21+ + E1- -> C + C										RTYPE=9 K1=8.84E-08 K2=-0.5 K3=0.0 # gg08_rnum=4210
C2H1+ + E1- -> C2 + H									RTYPE=9 K1=1.16E-07 K2=-7.60E-01 K3=0.0 # gg08_rnum=4211
C2H1+ + E1- -> CH + C									RTYPE=9 K1=1.05E-07 K2=-7.60E-01 K3=0.0 # gg08_rnum=4212
C2H1+ + E1- -> C + C + H								RTYPE=9 K1=4.80E-08 K2=-7.60E-01 K3=0.0 # gg08_rnum=4213
HC2H1+ + E1- -> C2 + H + H								RTYPE=9 K1=1.70E-07 K2=-0.5 K3=0.0 # gg08_rnum=4214
HC2H1+ + E1- -> CH + CH									RTYPE=9 K1=7.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4215
HC2H1+ + E1- -> C2H + H									RTYPE=9 K1=2.90E-07 K2=-0.5 K3=0.0 # gg08_rnum=4216
HC2H1+ + E1- -> CH2 + C									RTYPE=9 K1=2.89E-08 K2=-0.5 K3=0.0 # gg08_rnum=4217
HC2H1+ + E1- -> C2 + H2									RTYPE=9 K1=1.15E-08 K2=-0.5 K3=0.0 # gg08_rnum=4218
H2C2H1+ + E1- -> C2H + H2								RTYPE=9 K1=3.00E-08 K2=-8.40E-01 K3=0.0 # gg08_rnum=4219
H2C2H1+ + E1- -> HC2H + H								RTYPE=9 K1=1.45E-07 K2=-8.40E-01 K3=0.0 # gg08_rnum=4220
H2C2H1+ + E1- -> C2H + H + H							RTYPE=9 K1=2.95E-07 K2=-8.40E-01 K3=0.0 # gg08_rnum=4221
H2C2H1+ + E1- -> C2 + H + H2							RTYPE=9 K1=1.50E-08 K2=-8.40E-01 K3=0.0 # gg08_rnum=4222
H2C2H1+ + E1- -> CH3 + C								RTYPE=9 K1=3.00E-09 K2=-8.40E-01 K3=0.0 # gg08_rnum=4223
H2C2H1+ + E1- -> CH2 + CH								RTYPE=9 K1=1.50E-08 K2=-8.40E-01 K3=0.0 # gg08_rnum=4224
H2C2H21+ + E1- -> HC2H + H + H							RTYPE=9 K1=3.70E-07 K2=-7.60E-01 K3=0.0 # gg08_rnum=4225
H2C2H21+ + E1- -> HC2H + H2								RTYPE=9 K1=3.36E-08 K2=-7.60E-01 K3=0.0 # gg08_rnum=4226
H2C2H21+ + E1- -> H2C2H + H								RTYPE=9 K1=6.16E-08 K2=-7.60E-01 K3=0.0 # gg08_rnum=4227
H2C2H21+ + E1- -> C2H + H2 + H							RTYPE=9 K1=5.60E-08 K2=-7.60E-01 K3=0.0 # gg08_rnum=4228
H2C2H21+ + E1- -> CH4 + C								RTYPE=9 K1=5.60E-09 K2=-7.60E-01 K3=0.0 # gg08_rnum=4229
H2C2H21+ + E1- -> CH3 + CH								RTYPE=9 K1=1.12E-08 K2=-7.60E-01 K3=0.0 # gg08_rnum=4230
H2C2H21+ + E1- -> CH2 + CH2								RTYPE=9 K1=2.24E-08 K2=-7.60E-01 K3=0.0 # gg08_rnum=4231
CH3CH21+ + E1- -> HC2H + H2 + H							RTYPE=9 K1=3.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4232
CH3CH21+ + E1- -> H2C2H + H2							RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4233
CH3CH21+ + E1- -> H2C2H2 + H							RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4234
CH3CH21+ + E1- -> C2H + H2 + H2							RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4235
CH3CH2CHO1+ + E1- -> CH3 + CH2 + HCO					RTYPE=9 K1=7.13E-08 K2=-0.5 K3=0.0 # gg08_rnum=4236
CH3CH2CHO1+ + E1- -> CH3CH2 + CO + H					RTYPE=9 K1=7.13E-08 K2=-0.5 K3=0.0 # gg08_rnum=4237
CH3CH2CHO1+ + E1- -> H2C2H2 + HCO + H					RTYPE=9 K1=7.13E-08 K2=-0.5 K3=0.0 # gg08_rnum=4238
CH3CH2CHO1+ + E1- -> CH3 + CH2CO + H					RTYPE=9 K1=7.13E-08 K2=-0.5 K3=0.0 # gg08_rnum=4239
CH3CH2CHO1+ + E1- -> CH3CH2 + HCO						RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4240
CH3CH2OH1+ + E1- -> CH3 + CH2 + OH						RTYPE=9 K1=9.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=4241
CH3CH2OH1+ + E1- -> H2C2H2 + OH + H						RTYPE=9 K1=9.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=4242
CH3CH2OH1+ + E1- -> CH2 + CH2OH + H						RTYPE=9 K1=9.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=4243
CH3CH2OH1+ + E1- -> CH3CH2 + OH							RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4244
CH3CH2OH1+ + E1- -> CH3 + CH2OH							RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4245
CH3CH2OH21+ + E1- -> CH3 + CH2 + OH + H					RTYPE=9 K1=6.75E-07 K2=-0.5 K3=0.0 # gg08_rnum=4246
CH3CH2OH21+ + E1- -> CH3 + CH2 + H2O					RTYPE=9 K1=6.75E-08 K2=-0.5 K3=0.0 # gg08_rnum=4247
CH3CH2OH21+ + E1- -> CH3CH2 + OH + H					RTYPE=9 K1=6.75E-08 K2=-0.5 K3=0.0 # gg08_rnum=4248
CH3CH2OH21+ + E1- -> H2C2H2 + H2O + H					RTYPE=9 K1=6.75E-08 K2=-0.5 K3=0.0 # gg08_rnum=4249
CH3CH2OH21+ + E1- -> CH3CH2 + H2O						RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4250
CH3CH2OH21+ + E1- -> CH3CH2OH + H						RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4251
CH3CH31+ + E1- -> H2C2H2 + H2							RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4252
CH3CH31+ + E1- -> CH3CH2 + H							RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4253
C2OH1+ + E1- -> CO + C + H								RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4254
C2OH1+ + E1- -> CO + CH									RTYPE=9 K1=1.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4255
C2OH1+ + E1- -> C2H + O									RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4256
C2OH1+ + E1- -> C2O + H									RTYPE=9 K1=1.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4257
C2N1+ + E1- -> C2 + N									RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4258
C2N1+ + E1- -> C + CN									RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4259
NCCN1+ + E1- -> CN + CN									RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4260
NCCN1+ + E1- -> C2N + N									RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4261
HC2N1+ + E1- -> CH + CN									RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4262
HC2N1+ + E1- -> C2N + H									RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4263
C2O1+ + E1- -> CO + C									RTYPE=9 K1=3.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4264
C2S1+ + E1- -> CS + C									RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4265
C2S1+ + E1- -> C2 + S									RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4266
C31+ + E1- -> C2 + C									RTYPE=9 K1=3.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4267
C3H1+ + E1- -> C2H + C									RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4268
C3H1+ + E1- -> C3 + H									RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4269
C3H21+ + E1- -> HC2H + C								RTYPE=9 K1=3.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=4270
C3H21+ + E1- -> C3H + H									RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4271
C3H21+ + E1- -> C2 + CH2								RTYPE=9 K1=3.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=4272
C3H21+ + E1- -> C3 + H + H								RTYPE=9 K1=6.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=4273
C3H21+ + E1- -> C3 + H2									RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4274
HC3NH1+ + E1- -> HNC3 + H								RTYPE=9 K1=7.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4275
HC3NH1+ + E1- -> HC3N + H								RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4276
HC3NH1+ + E1- -> HC2H + CN								RTYPE=9 K1=2.28E-07 K2=-0.5 K3=0.0 # gg08_rnum=4277
HC3NH1+ + E1- -> C3N + H2								RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4278
HC3NH1+ + E1- -> C2H + HNC								RTYPE=9 K1=7.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4279
HC3NH1+ + E1- -> C2NCH + H								RTYPE=9 K1=1.20E-08 K2=-0.5 K3=0.0 # gg08_rnum=4280
HC3NH1+ + E1- -> HC2NC + H								RTYPE=9 K1=1.20E-08 K2=-0.5 K3=0.0 # gg08_rnum=4281
C3H31+ + E1- -> HC2H + CH								RTYPE=9 K1=6.99E-08 K2=-0.5 K3=0.0 # gg08_rnum=4282
C3H31+ + E1- -> C3H + H2								RTYPE=9 K1=3.15E-07 K2=-0.5 K3=0.0 # gg08_rnum=4283
C3H31+ + E1- -> C3H2 + H								RTYPE=9 K1=3.15E-07 K2=-0.5 K3=0.0 # gg08_rnum=4284
C3H3N1+ + E1- -> C3N + H2 + H							RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4285
C3H3N1+ + E1- -> HC3N + H2								RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4286
C3H41+ + E1- -> C2H + CH3								RTYPE=9 K1=2.95E-08 K2=-6.70E-01 K3=0.0 # gg08_rnum=4287
C3H41+ + E1- -> C3H2 + H + H							RTYPE=9 K1=1.18E-07 K2=-6.70E-01 K3=0.0 # gg08_rnum=4288
C3H41+ + E1- -> C3H2 + H2								RTYPE=9 K1=2.95E-08 K2=-6.70E-01 K3=0.0 # gg08_rnum=4289
C3H41+ + E1- -> C3H3 + H								RTYPE=9 K1=2.57E-06 K2=-6.70E-01 K3=0.0 # gg08_rnum=4290
C3H41+ + E1- -> H2C2H + CH								RTYPE=9 K1=2.95E-08 K2=-6.70E-01 K3=0.0 # gg08_rnum=4291
C3H41+ + E1- -> HC2H + CH2								RTYPE=9 K1=1.77E-07 K2=-6.70E-01 K3=0.0 # gg08_rnum=4292
C3H4N1+ + E1- -> C3N + H2 + H2							RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4293
C3H4N1+ + E1- -> HC3N + H2 + H							RTYPE=9 K1=1.00E-06 K2=-3.00E-01 K3=0.0 # gg08_rnum=4294
C3H4N1+ + E1- -> C3H3N + H								RTYPE=9 K1=1.00E-06 K2=-3.00E-01 K3=0.0 # gg08_rnum=4295
C3H51+ + E1- -> C3H4 + H								RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4296
C3H51+ + E1- -> C3H3 + H2								RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4297
CH3CH2CHOH1+ + E1- -> C3H4 + OH + H2					RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4298
NC3H1+ + E1- -> C2H + CN								RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4299
NC3H1+ + E1- -> C3N + H									RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4300
NC3H1+ + E1- -> C2 + HCN								RTYPE=9 K1=3.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4301
C3N1+ + E1- -> C2 + CN									RTYPE=9 K1=3.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4302
C3O1+ + E1- -> C2 + CO									RTYPE=9 K1=3.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4303
C3S1+ + E1- -> C3 + S									RTYPE=9 K1=1.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4304
C3S1+ + E1- -> C2 + CS									RTYPE=9 K1=1.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4305
C3S1+ + E1- -> C2S + C									RTYPE=9 K1=1.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4306
C41+ + E1- -> C2 + C2									RTYPE=9 K1=1.75E-07 K2=-0.5 K3=0.0 # gg08_rnum=4307
C41+ + E1- -> C3 + C									RTYPE=9 K1=2.75E-07 K2=-0.5 K3=0.0 # gg08_rnum=4308
C4H1+ + E1- -> C2H + C2									RTYPE=9 K1=9.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=4309
C4H1+ + E1- -> C3H + C									RTYPE=9 K1=2.75E-08 K2=-0.5 K3=0.0 # gg08_rnum=4310
C4H1+ + E1- -> C3 + CH									RTYPE=9 K1=4.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4311
C4H1+ + E1- -> C4 + H									RTYPE=9 K1=1.20E-07 K2=-0.5 K3=0.0 # gg08_rnum=4312
HC4H1+ + E1- -> C4H + H									RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4313
HC4H1+ + E1- -> C4 + H2									RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4314
C4H31+ + E1- -> C4H + H2								RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4315
C4H31+ + E1- -> HC4H + H								RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4316
C4H41+ + E1- -> C4H3 + H								RTYPE=9 K1=3.30E-07 K2=-0.5 K3=0.0 # gg08_rnum=4317
C4H41+ + E1- -> HC4H + H2								RTYPE=9 K1=3.30E-07 K2=-0.5 K3=0.0 # gg08_rnum=4318
C4H41+ + E1- -> C4H + H2 + H							RTYPE=9 K1=3.30E-07 K2=-0.5 K3=0.0 # gg08_rnum=4319
C4H4N1+ + E1- -> CH3 + HC3N								RTYPE=9 K1=1.00E-06 K2=-3.00E-01 K3=0.0 # gg08_rnum=4320
C4H4N1+ + E1- -> CH3C3N + H								RTYPE=9 K1=1.00E-06 K2=-3.00E-01 K3=0.0 # gg08_rnum=4321
C4H51+ + E1- -> C2H + H2C2H2							RTYPE=9 K1=1.01E-07 K2=-0.5 K3=0.0 # gg08_rnum=4322
C4H51+ + E1- -> C3H4 + CH								RTYPE=9 K1=4.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4323
C4H51+ + E1- -> C4H + H2 + H2							RTYPE=9 K1=1.01E-07 K2=-0.5 K3=0.0 # gg08_rnum=4324
C4H51+ + E1- -> HC4H + H + H2							RTYPE=9 K1=1.01E-07 K2=-0.5 K3=0.0 # gg08_rnum=4325
C4H51+ + E1- -> HC2H + H2C2H							RTYPE=9 K1=1.01E-07 K2=-0.5 K3=0.0 # gg08_rnum=4326
C4H71+ + E1- -> C3H3 + CH4								RTYPE=9 K1=1.95E-07 K2=-0.5 K3=0.0 # gg08_rnum=4327
C4H71+ + E1- -> H2C2H + H2C2H2							RTYPE=9 K1=2.25E-08 K2=-0.5 K3=0.0 # gg08_rnum=4328
C4H71+ + E1- -> HC2H + CH3CH2							RTYPE=9 K1=2.25E-08 K2=-0.5 K3=0.0 # gg08_rnum=4329
C4H71+ + E1- -> C4H + H2 + H2 + H2						RTYPE=9 K1=6.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=4330
C4N1+ + E1- -> C2N + C2									RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4331
C4N1+ + E1- -> C3N + C									RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4332
C4P1+ + E1- -> CCP + C2									RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4333
C4P1+ + E1- -> C3P + C									RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4334
C4S1+ + E1- -> C2S + C2									RTYPE=9 K1=1.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4335
C4S1+ + E1- -> C3 + CS									RTYPE=9 K1=1.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4336
C4S1+ + E1- -> C3S + C									RTYPE=9 K1=1.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4337
C51+ + E1- -> C3 + C2									RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4338
C51+ + E1- -> C4 + C									RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4339
C5H1+ + E1- -> C5 + H									RTYPE=9 K1=1.00E-06 K2=-3.00E-01 K3=0.0 # gg08_rnum=4340
C5H1+ + E1- -> C4H + C									RTYPE=9 K1=1.00E-06 K2=-3.00E-01 K3=0.0 # gg08_rnum=4341
C5H21+ + E1- -> C5 + H2									RTYPE=9 K1=1.00E-06 K2=-3.00E-01 K3=0.0 # gg08_rnum=4342
C5H21+ + E1- -> C5H + H									RTYPE=9 K1=1.00E-06 K2=-3.00E-01 K3=0.0 # gg08_rnum=4343
C5H2N1+ + E1- -> HC5N + H								RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4344
C5H2N1+ + E1- -> C5N + H2								RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4345
C5H31+ + E1- -> C5H2 + H								RTYPE=9 K1=4.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4346
C5H31+ + E1- -> C5H + H2								RTYPE=9 K1=4.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4347
C5H3N1+ + E1- -> C5N + H2 + H							RTYPE=9 K1=1.00E-06 K2=-3.00E-01 K3=0.0 # gg08_rnum=4348
C5H3N1+ + E1- -> HC5N + H2								RTYPE=9 K1=1.00E-06 K2=-3.00E-01 K3=0.0 # gg08_rnum=4349
C5H41+ + E1- -> C5H + H + H2							RTYPE=9 K1=1.00E-06 K2=-3.00E-01 K3=0.0 # gg08_rnum=4350
C5H41+ + E1- -> C5H2 + H2								RTYPE=9 K1=1.00E-06 K2=-3.00E-01 K3=0.0 # gg08_rnum=4351
C5H4N1+ + E1- -> HC5N + H2 + H							RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4352
C5H4N1+ + E1- -> C5N + H2 + H2							RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4353
C5H51+ + E1- -> CH3C4H + H								RTYPE=9 K1=1.00E-06 K2=-3.00E-01 K3=0.0 # gg08_rnum=4354
C5H51+ + E1- -> C5H2 + H2 + H							RTYPE=9 K1=1.00E-06 K2=-3.00E-01 K3=0.0 # gg08_rnum=4355
C5HN1+ + E1- -> C5N + H									RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4356
C5HN1+ + E1- -> CN + C4H								RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4357
C5HN1+ + E1- -> C3N + C2H								RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4358
C5N1+ + E1- -> C2 + C3N									RTYPE=9 K1=3.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4359
C61+ + E1- -> C4 + C2									RTYPE=9 K1=1.00E-06 K2=-3.00E-01 K3=0.0 # gg08_rnum=4360
C61+ + E1- -> C5 + C									RTYPE=9 K1=1.00E-06 K2=-3.00E-01 K3=0.0 # gg08_rnum=4361
C6H1+ + E1- -> C5H + C									RTYPE=9 K1=1.00E-06 K2=-3.00E-01 K3=0.0 # gg08_rnum=4362
C6H1+ + E1- -> C6 + H									RTYPE=9 K1=1.00E-06 K2=-3.00E-01 K3=0.0 # gg08_rnum=4363
C6H21+ + E1- -> C6 + H + H								RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4364
C6H21+ + E1- -> C6 + H2									RTYPE=9 K1=1.00E-06 K2=-3.00E-01 K3=0.0 # gg08_rnum=4365
C6H21+ + E1- -> C6H + H									RTYPE=9 K1=1.00E-06 K2=-3.00E-01 K3=0.0 # gg08_rnum=4366
C6H31+ + E1- -> C6H2 + H								RTYPE=9 K1=1.00E-06 K2=-3.00E-01 K3=0.0 # gg08_rnum=4367
C6H31+ + E1- -> C6H + H + H								RTYPE=9 K1=3.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4368
C6H31+ + E1- -> C6H + H2								RTYPE=9 K1=1.00E-06 K2=-3.00E-01 K3=0.0 # gg08_rnum=4369
C6H41+ + E1- -> C6H2 + H2								RTYPE=9 K1=1.00E-06 K2=-3.00E-01 K3=0.0 # gg08_rnum=4370
C6H41+ + E1- -> C6H + H2 + H							RTYPE=9 K1=1.00E-06 K2=-3.00E-01 K3=0.0 # gg08_rnum=4371
C6H4N1+ + E1- -> CH3 + HC5N								RTYPE=9 K1=1.00E-06 K2=-3.00E-01 K3=0.0 # gg08_rnum=4372
C6H4N1+ + E1- -> CH3C5N + H								RTYPE=9 K1=1.00E-06 K2=-3.00E-01 K3=0.0 # gg08_rnum=4373
C6H51+ + E1- -> C6H + H2 + H2							RTYPE=9 K1=1.00E-06 K2=-3.00E-01 K3=0.0 # gg08_rnum=4374
C6H51+ + E1- -> C6H2 + H2 + H							RTYPE=9 K1=1.00E-06 K2=-3.00E-01 K3=0.0 # gg08_rnum=4375
C6H71+ + E1- -> C6H2 + H2 + H2 + H						RTYPE=9 K1=5.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4376
C6H71+ + E1- -> C6H6 + H								RTYPE=9 K1=5.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4377
C71+ + E1- -> C5 + C2									RTYPE=9 K1=1.00E-06 K2=-3.00E-01 K3=0.0 # gg08_rnum=4378
C71+ + E1- -> C6 + C									RTYPE=9 K1=1.00E-06 K2=-3.00E-01 K3=0.0 # gg08_rnum=4379
C71+ + E1- -> C4 + C3									RTYPE=9 K1=3.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4380
C7H1+ + E1- -> C6H + C									RTYPE=9 K1=1.00E-06 K2=-3.00E-01 K3=0.0 # gg08_rnum=4381
C7H1+ + E1- -> C7 + H									RTYPE=9 K1=1.00E-06 K2=-3.00E-01 K3=0.0 # gg08_rnum=4382
C7H21+ + E1- -> C7 + H2									RTYPE=9 K1=1.00E-06 K2=-3.00E-01 K3=0.0 # gg08_rnum=4383
C7H21+ + E1- -> C7H + H									RTYPE=9 K1=1.00E-06 K2=-3.00E-01 K3=0.0 # gg08_rnum=4384
C7H2N1+ + E1- -> HC7N + H								RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4385
C7H2N1+ + E1- -> C7N + H2								RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4386
C7H31+ + E1- -> C7H2 + H								RTYPE=9 K1=1.00E-06 K2=-3.00E-01 K3=0.0 # gg08_rnum=4387
C7H31+ + E1- -> C7H + H2								RTYPE=9 K1=1.00E-06 K2=-3.00E-01 K3=0.0 # gg08_rnum=4388
C7H3N1+ + E1- -> C7N + H2 + H							RTYPE=9 K1=1.00E-06 K2=-3.00E-01 K3=0.0 # gg08_rnum=4389
C7H3N1+ + E1- -> HC7N + H2								RTYPE=9 K1=1.00E-06 K2=-3.00E-01 K3=0.0 # gg08_rnum=4390
C7H41+ + E1- -> C7H + H2 + H							RTYPE=9 K1=1.00E-06 K2=-3.00E-01 K3=0.0 # gg08_rnum=4391
C7H41+ + E1- -> C7H2 + H2								RTYPE=9 K1=1.00E-06 K2=-3.00E-01 K3=0.0 # gg08_rnum=4392
C7H51+ + E1- -> C7H2 + H2 + H							RTYPE=9 K1=3.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4393
C7H51+ + E1- -> CH3C6H + H								RTYPE=9 K1=3.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4394
C7HN1+ + E1- -> CN + C6H								RTYPE=9 K1=1.00E-06 K2=-3.00E-01 K3=0.0 # gg08_rnum=4395
C7HN1+ + E1- -> C7N + H									RTYPE=9 K1=1.00E-06 K2=-3.00E-01 K3=0.0 # gg08_rnum=4396
C7N1+ + E1- -> C2 + C5N									RTYPE=9 K1=3.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4397
C81+ + E1- -> C6 + C2									RTYPE=9 K1=1.00E-06 K2=-3.00E-01 K3=0.0 # gg08_rnum=4398
C81+ + E1- -> C7 + C									RTYPE=9 K1=1.00E-06 K2=-3.00E-01 K3=0.0 # gg08_rnum=4399
C8H1+ + E1- -> C7H + C									RTYPE=9 K1=1.00E-06 K2=-3.00E-01 K3=0.0 # gg08_rnum=4400
C8H1+ + E1- -> C8 + H									RTYPE=9 K1=1.00E-06 K2=-3.00E-01 K3=0.0 # gg08_rnum=4401
C8H21+ + E1- -> C8H + H									RTYPE=9 K1=1.00E-06 K2=-3.00E-01 K3=0.0 # gg08_rnum=4402
C8H21+ + E1- -> C8 + H2									RTYPE=9 K1=1.00E-06 K2=-3.00E-01 K3=0.0 # gg08_rnum=4403
C8H31+ + E1- -> C8H + H2								RTYPE=9 K1=1.00E-06 K2=-3.00E-01 K3=0.0 # gg08_rnum=4404
C8H31+ + E1- -> C8H2 + H								RTYPE=9 K1=1.00E-06 K2=-3.00E-01 K3=0.0 # gg08_rnum=4405
C8H41+ + E1- -> C8H + H2 + H							RTYPE=9 K1=1.00E-06 K2=-3.00E-01 K3=0.0 # gg08_rnum=4406
C8H41+ + E1- -> C8H2 + H2								RTYPE=9 K1=1.00E-06 K2=-3.00E-01 K3=0.0 # gg08_rnum=4407
C8H4N1+ + E1- -> CH3 + HC7N								RTYPE=9 K1=1.00E-06 K2=-3.00E-01 K3=0.0 # gg08_rnum=4408
C8H4N1+ + E1- -> CH3C7N + H								RTYPE=9 K1=1.00E-06 K2=-3.00E-01 K3=0.0 # gg08_rnum=4409
C8H51+ + E1- -> C8H + H2 + H2							RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4410
C8H51+ + E1- -> C8H2 + H + H2							RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4411
C91+ + E1- -> C7 + C2									RTYPE=9 K1=1.00E-06 K2=-3.00E-01 K3=0.0 # gg08_rnum=4412
C91+ + E1- -> C8 + C									RTYPE=9 K1=1.00E-06 K2=-3.00E-01 K3=0.0 # gg08_rnum=4413
C9H1+ + E1- -> C9 + H									RTYPE=9 K1=1.00E-06 K2=-3.00E-01 K3=0.0 # gg08_rnum=4414
C9H1+ + E1- -> C8H + C									RTYPE=9 K1=1.00E-06 K2=-3.00E-01 K3=0.0 # gg08_rnum=4415
C9H21+ + E1- -> C9 + H2									RTYPE=9 K1=1.00E-06 K2=-3.00E-01 K3=0.0 # gg08_rnum=4416
C9H21+ + E1- -> C9H + H									RTYPE=9 K1=1.00E-06 K2=-3.00E-01 K3=0.0 # gg08_rnum=4417
C9H2N1+ + E1- -> C9N + H2								RTYPE=9 K1=1.00E-06 K2=-3.00E-01 K3=0.0 # gg08_rnum=4418
C9H2N1+ + E1- -> HC9N + H								RTYPE=9 K1=1.00E-06 K2=-3.00E-01 K3=0.0 # gg08_rnum=4419
C9H31+ + E1- -> C9H + H2								RTYPE=9 K1=1.00E-06 K2=-3.00E-01 K3=0.0 # gg08_rnum=4420
C9H31+ + E1- -> C9H2 + H								RTYPE=9 K1=1.00E-06 K2=-3.00E-01 K3=0.0 # gg08_rnum=4421
C9H3N1+ + E1- -> C9N + H2 + H							RTYPE=9 K1=1.00E-06 K2=-3.00E-01 K3=0.0 # gg08_rnum=4422
C9H3N1+ + E1- -> HC9N + H2								RTYPE=9 K1=1.00E-06 K2=-3.00E-01 K3=0.0 # gg08_rnum=4423
C9H41+ + E1- -> C9H + H2 + H							RTYPE=9 K1=1.00E-06 K2=-3.00E-01 K3=0.0 # gg08_rnum=4424
C9H41+ + E1- -> C9H2 + H2								RTYPE=9 K1=1.00E-06 K2=-3.00E-01 K3=0.0 # gg08_rnum=4425
C9H51+ + E1- -> C9H + H2 + H2							RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4426
C9H51+ + E1- -> C9H2 + H2 + H							RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4427
C9HN1+ + E1- -> CN + C8H								RTYPE=9 K1=1.00E-06 K2=-3.00E-01 K3=0.0 # gg08_rnum=4428
C9HN1+ + E1- -> C9N + H									RTYPE=9 K1=1.00E-06 K2=-3.00E-01 K3=0.0 # gg08_rnum=4429
C9N1+ + E1- -> C2 + C7N									RTYPE=9 K1=3.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4430
ClC1+ + E1- -> C + Cl									RTYPE=9 K1=3.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4431
CCP1+ + E1- -> P + C2									RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4432
CCP1+ + E1- -> CP + C									RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4433
CH1+ + E1- -> C + H										RTYPE=9 K1=7.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=4434
CH21+ + E1- -> C + H + H								RTYPE=9 K1=4.00E-07 K2=-6.00E-01 K3=0.0 # gg08_rnum=4435
CH21+ + E1- -> C + H2									RTYPE=9 K1=7.70E-08 K2=-6.00E-01 K3=0.0 # gg08_rnum=4436
CH21+ + E1- -> CH + H									RTYPE=9 K1=1.60E-07 K2=-6.00E-01 K3=0.0 # gg08_rnum=4437
H2C2N1+ + E1- -> C2N + H2								RTYPE=9 K1=3.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4438
H2C2N1+ + E1- -> CH + HCN								RTYPE=9 K1=3.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4439
H2C2N1+ + E1- -> CN + CH2								RTYPE=9 K1=3.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4440
CH2CO1+ + E1- -> HC2H + O								RTYPE=9 K1=2.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4441
CH2CO1+ + E1- -> C2 + H2O								RTYPE=9 K1=2.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4442
CH2CO1+ + E1- -> CH2 + CO								RTYPE=9 K1=2.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4443
CH2OH1+ + E1- -> CH2 + OH								RTYPE=9 K1=3.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4444
CH2OH1+ + E1- -> H2CO + H								RTYPE=9 K1=3.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4445
SiCH21+ + E1- -> SiC + H2								RTYPE=9 K1=2.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4446
SiCH21+ + E1- -> SiCH + H								RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4447
SiCH21+ + E1- -> Si + CH2								RTYPE=9 K1=2.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4448
CH31+ + E1- -> CH2 + H									RTYPE=9 K1=4.00E-07 K2=-3.00E-01 K3=0.0 # gg08_rnum=4449
CH31+ + E1- -> H2 + C + H								RTYPE=9 K1=3.00E-07 K2=-3.00E-01 K3=0.0 # gg08_rnum=4450
CH31+ + E1- -> CH + H + H								RTYPE=9 K1=1.60E-07 K2=-3.00E-01 K3=0.0 # gg08_rnum=4451
CH31+ + E1- -> CH + H2									RTYPE=9 K1=1.40E-07 K2=-3.00E-01 K3=0.0 # gg08_rnum=4452
CH3CH2O1+ + E1- -> CH3 + HCO + H						RTYPE=9 K1=1.35E-07 K2=-0.5 K3=0.0 # gg08_rnum=4453
CH3CH2O1+ + E1- -> CH2 + H2CO + H						RTYPE=9 K1=1.35E-07 K2=-0.5 K3=0.0 # gg08_rnum=4454
CH3CH2O1+ + E1- -> CH3 + H2CO							RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4455
CH3CH2O1+ + E1- -> CH3CHO + H							RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4456
CH3CHO1+ + E1- -> CH3 + HCO								RTYPE=9 K1=3.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4457
CH3CN1+ + E1- -> CH2 + HCN								RTYPE=9 K1=3.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4458
CH3CN1+ + E1- -> CH3 + CN								RTYPE=9 K1=2.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4459
CH3CN1+ + E1- -> H2C2N + H								RTYPE=9 K1=2.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4460
CH3CN1+ + E1- -> C2N + H2 + H							RTYPE=9 K1=2.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4461
CH3CNH1+ + E1- -> H2C2N + H + H							RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4462
CH3CNH1+ + E1- -> CH3CN + H								RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4463
CH3CO1+ + E1- -> CH3 + CO								RTYPE=9 K1=3.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4464
CH3NH1+ + E1- -> CH2 + NH2								RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4465
CH3NH1+ + E1- -> HCN + H + H2							RTYPE=9 K1=3.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4466
CH3NH1+ + E1- -> CH2NH + H								RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4467
CH3NH1+ + E1- -> CN + H2 + H2							RTYPE=9 K1=3.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=4468
CH3NH21+ + E1- -> NH2 + CH3								RTYPE=9 K1=3.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4469
CH3NH31+ + E1- -> CH3NH2 + H							RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4470
CH3NH31+ + E1- -> CH3 + NH2 + H							RTYPE=9 K1=1.43E-07 K2=-0.5 K3=0.0 # gg08_rnum=4471
CH3NH31+ + E1- -> CH3 + NH3								RTYPE=9 K1=1.43E-07 K2=-0.5 K3=0.0 # gg08_rnum=4472
CH3O1+ + E1- -> HCO + H + H								RTYPE=9 K1=2.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4473
CH3O1+ + E1- -> H2CO + H								RTYPE=9 K1=2.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4474
CH3O1+ + E1- -> CO + H + H2								RTYPE=9 K1=2.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4475
HCOOH21+ + E1- -> CO2 + H2 + H							RTYPE=9 K1=2.85E-07 K2=-0.5 K3=0.0 # gg08_rnum=4476
HCOOH21+ + E1- -> HCOOH + H								RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4477
CH3OCH31+ + E1- -> CH3O + CH3							RTYPE=9 K1=6.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4478
CH3OCH41+ + E1- -> H2CO + CH4 + H						RTYPE=9 K1=4.80E-07 K2=-0.5 K3=0.0 # gg08_rnum=4479
CH3OCH41+ + E1- -> CH3O + CH3 + H						RTYPE=9 K1=6.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=4480
CH3OCH41+ + E1- -> CH3O + CH4							RTYPE=9 K1=3.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=4481
CH3OCH41+ + E1- -> CH3OCH3 + H							RTYPE=9 K1=3.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=4482
CH3OH1+ + E1- -> CH3 + OH								RTYPE=9 K1=6.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4483
CH3OH21+ + E1- -> CH3 + H2O								RTYPE=9 K1=8.01E-08 K2=-5.90E-01 K3=0.0 # gg08_rnum=4484
CH3OH21+ + E1- -> CH3 + OH + H							RTYPE=9 K1=4.54E-07 K2=-5.90E-01 K3=0.0 # gg08_rnum=4485
CH3OH21+ + E1- -> CH2 + H2O + H							RTYPE=9 K1=1.87E-07 K2=-5.90E-01 K3=0.0 # gg08_rnum=4486
CH3OH21+ + E1- -> CH3OH + H								RTYPE=9 K1=2.67E-08 K2=-5.90E-01 K3=0.0 # gg08_rnum=4487
CH3OH21+ + E1- -> CH3O + H2								RTYPE=9 K1=5.34E-08 K2=-5.90E-01 K3=0.0 # gg08_rnum=4488
CH3OH21+ + E1- -> H2CO + H2 + H							RTYPE=9 K1=8.90E-08 K2=-5.90E-01 K3=0.0 # gg08_rnum=4489
CH41+ + E1- -> CH2 + H + H								RTYPE=9 K1=3.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4490
CH41+ + E1- -> CH3 + H									RTYPE=9 K1=3.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4491
CH51+ + E1- -> CH + H2 + H2								RTYPE=9 K1=3.00E-09 K2=-5.20E-01 K3=0.0 # gg08_rnum=4492
CH51+ + E1- -> CH4 + H									RTYPE=9 K1=1.40E-08 K2=-5.20E-01 K3=0.0 # gg08_rnum=4493
CH51+ + E1- -> CH3 + H2									RTYPE=9 K1=1.40E-08 K2=-5.20E-01 K3=0.0 # gg08_rnum=4494
CH51+ + E1- -> CH3 + H + H								RTYPE=9 K1=1.95E-07 K2=-5.20E-01 K3=0.0 # gg08_rnum=4495
CH51+ + E1- -> CH2 + H2 + H								RTYPE=9 K1=4.80E-08 K2=-5.20E-01 K3=0.0 # gg08_rnum=4496
HCOOCH3H1+ + E1- -> CH3OH + HCO							RTYPE=9 K1=1.43E-07 K2=-0.5 K3=0.0 # gg08_rnum=4497
HCOOCH3H1+ + E1- -> HCOOCH3 + H							RTYPE=9 K1=7.50E-09 K2=-0.5 K3=0.0 # gg08_rnum=4498
SiCH1+ + E1- -> Si + CH									RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4499
SiCH1+ + E1- -> SiC + H									RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4500
ClO1+ + E1- -> Cl + O									RTYPE=9 K1=2.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4501
CN1+ + E1- -> C + N										RTYPE=9 K1=3.38E-07 K2=-5.50E-01 K3=0.0 # gg08_rnum=4502
CNC1+ + E1- -> CN + C									RTYPE=9 K1=3.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4503
CO1+ + E1- -> O + C										RTYPE=9 K1=2.75E-07 K2=-5.50E-01 K3=0.0 # gg08_rnum=4504
CO21+ + E1- -> O + CO									RTYPE=9 K1=4.20E-07 K2=-7.50E-01 K3=0.0 # gg08_rnum=4505
COCHO1+ + E1- -> HCO + CO								RTYPE=9 K1=3.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4506
COOH1+ + E1- -> CO + OH									RTYPE=9 K1=3.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4507
CP1+ + E1- -> P + C										RTYPE=9 K1=1.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4508
CS1+ + E1- -> C + S										RTYPE=9 K1=2.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4509
H21+ + E1- -> H + H										RTYPE=9 K1=2.53E-07 K2=-0.5 K3=0.0 # gg08_rnum=4510
cH2C3O1+ + E1- -> C3O + H2								RTYPE=9 K1=2.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4511
cH2C3O1+ + E1- -> HC2H + CO								RTYPE=9 K1=2.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4512
cH2C3O1+ + E1- -> C3O + H + H							RTYPE=9 K1=2.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4513
H2C4N1+ + E1- -> HC3N + CH								RTYPE=9 K1=3.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4514
ClCH21+ + E1- -> ClC + H + H							RTYPE=9 K1=3.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4515
ClH21+ + E1- -> Cl + H + H								RTYPE=9 K1=2.70E-07 K2=-0.5 K3=0.0 # gg08_rnum=4516
ClH21+ + E1- -> ClH + H									RTYPE=9 K1=3.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=4517
H2CN1+ + E1- -> HNC + H									RTYPE=9 K1=1.85E-07 K2=-6.50E-01 K3=0.0 # gg08_rnum=4518
H2CN1+ + E1- -> CN + H + H								RTYPE=9 K1=9.20E-08 K2=-6.50E-01 K3=0.0 # gg08_rnum=4519
H2CN1+ + E1- -> HCN + H									RTYPE=9 K1=1.85E-07 K2=-6.50E-01 K3=0.0 # gg08_rnum=4520
H2CO1+ + E1- -> HCO + H									RTYPE=9 K1=1.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4521
H2CO1+ + E1- -> CO + H + H								RTYPE=9 K1=5.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4522
CH2OHOCH21+ + E1- -> HCOOCH3 + H						RTYPE=9 K1=1.50E-09 K2=-0.5 K3=0.0 # gg08_rnum=4523
CH2OHOCH21+ + E1- -> CH3OH + HCO						RTYPE=9 K1=1.49E-07 K2=-0.5 K3=0.0 # gg08_rnum=4524
H2CS1+ + E1- -> HCS + H									RTYPE=9 K1=3.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4525
H2CS1+ + E1- -> CS + H + H								RTYPE=9 K1=3.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4526
H2NC1+ + E1- -> HNC + H									RTYPE=9 K1=1.80E-07 K2=-0.5 K3=0.0 # gg08_rnum=4527
H2NC1+ + E1- -> CN + H2									RTYPE=9 K1=1.80E-08 K2=-0.5 K3=0.0 # gg08_rnum=4528
H2NO1+ + E1- -> HNO + H									RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4529
H2NO1+ + E1- -> NO + H2									RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4530
H2O1+ + E1- -> O + H + H								RTYPE=9 K1=3.05E-07 K2=-0.5 K3=0.0 # gg08_rnum=4531
H2O1+ + E1- -> O + H2									RTYPE=9 K1=3.90E-08 K2=-0.5 K3=0.0 # gg08_rnum=4532
H2O1+ + E1- -> OH + H									RTYPE=9 K1=8.60E-08 K2=-0.5 K3=0.0 # gg08_rnum=4533
H2PO1+ + E1- -> PH + OH									RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4534
H2PO1+ + E1- -> HPO + H									RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4535
H2S1+ + E1- -> S + H + H								RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4536
H2S1+ + E1- -> HS + H									RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4537
H2S21+ + E1- -> HS + HS									RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4538
H2S21+ + E1- -> S2H + H									RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4539
H2SiO1+ + E1- -> SiH + OH								RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4540
H2SiO1+ + E1- -> SiO + H2								RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4541
H31+ + E1- -> H2 + H									RTYPE=9 K1=2.59E-08 K2=-0.5 K3=0.0 # gg08_rnum=4542
H31+ + E1- -> H + H + H									RTYPE=9 K1=4.61E-08 K2=-0.5 K3=0.0 # gg08_rnum=4543
cH2C3OH1+ + E1- -> H2C2H + CO							RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4544
cH2C3OH1+ + E1- -> C3H + H2O							RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4545
CH3C3N1+ + E1- -> HC3N + CH2							RTYPE=9 K1=3.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4546
H3CS1+ + E1- -> H2CS + H								RTYPE=9 K1=3.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4547
H3CS1+ + E1- -> CS + H + H2								RTYPE=9 K1=3.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4548
H3O1+ + E1- -> H2O + H									RTYPE=9 K1=1.10E-07 K2=-0.5 K3=0.0 # gg08_rnum=4549
H3O1+ + E1- -> OH + H2									RTYPE=9 K1=6.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=4550
H3O1+ + E1- -> H2 + H + O								RTYPE=9 K1=5.60E-09 K2=-0.5 K3=0.0 # gg08_rnum=4551
H3O1+ + E1- -> OH + H + H								RTYPE=9 K1=2.60E-07 K2=-0.5 K3=0.0 # gg08_rnum=4552
H3S1+ + E1- -> H2S + H									RTYPE=9 K1=3.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4553
H3S1+ + E1- -> HS + H + H								RTYPE=9 K1=1.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4554
H3S21+ + E1- -> H2S2 + H								RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4555
H3S21+ + E1- -> S2H + H2								RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4556
H3SiO1+ + E1- -> H2SiO + H								RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4557
H3SiO1+ + E1- -> SiO + H2 + H							RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4558
HC2NCH1+ + E1- -> C2H + HCN								RTYPE=9 K1=2.28E-07 K2=-0.5 K3=0.0 # gg08_rnum=4559
HC2NCH1+ + E1- -> HC2H + CN								RTYPE=9 K1=2.28E-07 K2=-0.5 K3=0.0 # gg08_rnum=4560
HC2NCH1+ + E1- -> C2NCH + H								RTYPE=9 K1=6.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=4561
HC2NCH1+ + E1- -> HC2NC + H								RTYPE=9 K1=6.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=4562
HC2NCH1+ + E1- -> HC3N + H								RTYPE=9 K1=1.20E-08 K2=-0.5 K3=0.0 # gg08_rnum=4563
HC2NCH1+ + E1- -> HNC3 + H								RTYPE=9 K1=1.20E-08 K2=-0.5 K3=0.0 # gg08_rnum=4564
HC2S1+ + E1- -> C2S + H									RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4565
HC2S1+ + E1- -> CS + CH									RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4566
HC3O1+ + E1- -> C3O + H									RTYPE=9 K1=3.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4567
HC3S1+ + E1- -> C3S + H									RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4568
HC3S1+ + E1- -> C2S + CH								RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4569
HC4N1+ + E1- -> C3N + CH								RTYPE=9 K1=3.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4570
HC4O1+ + E1- -> C3O + CH								RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4571
HC4O1+ + E1- -> C3H + CO								RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4572
HC4S1+ + E1- -> C3S + CH								RTYPE=9 K1=1.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4573
HC4S1+ + E1- -> C4S + H									RTYPE=9 K1=1.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4574
HC4S1+ + E1- -> C2S + C2H								RTYPE=9 K1=1.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4575
ClH1+ + E1- -> H + Cl									RTYPE=9 K1=3.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4576
HCN1+ + E1- -> CN + H									RTYPE=9 K1=2.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4577
HCO1+ + E1- -> CO + H									RTYPE=9 K1=2.40E-07 K2=-6.90E-01 K3=0.0 # gg08_rnum=4578
HOCO1+ + E1- -> OH + CO									RTYPE=9 K1=3.20E-07 K2=-6.40E-01 K3=0.0 # gg08_rnum=4579
HOCO1+ + E1- -> CO2 + H									RTYPE=9 K1=6.00E-08 K2=-6.40E-01 K3=0.0 # gg08_rnum=4580
HOCO1+ + E1- -> CO + H + O								RTYPE=9 K1=8.10E-07 K2=-6.40E-01 K3=0.0 # gg08_rnum=4581
HCOOH1+ + E1- -> HCO + OH								RTYPE=9 K1=3.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4582
HCOOH1+ + E1- -> CO2 + H + H							RTYPE=9 K1=3.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4583
HCP1+ + E1- -> P + CH									RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4584
HCP1+ + E1- -> CP + H									RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4585
HCS1+ + E1- -> CS + H									RTYPE=9 K1=1.84E-07 K2=-5.70E-01 K3=0.0 # gg08_rnum=4586
HCS1+ + E1- -> CH + S									RTYPE=9 K1=7.87E-07 K2=-5.70E-01 K3=0.0 # gg08_rnum=4587
HeH1+ + E1- -> H + He									RTYPE=9 K1=3.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=4588
CH3COCHO1+ + E1- -> CH3 + CO + HCO						RTYPE=9 K1=6.75E-08 K2=-0.5 K3=0.0 # gg08_rnum=4589
CH3COCHO1+ + E1- -> CH3CO + CO + H						RTYPE=9 K1=6.75E-08 K2=-0.5 K3=0.0 # gg08_rnum=4590
CH3COCHO1+ + E1- -> CH2CO + HCO + H						RTYPE=9 K1=6.75E-08 K2=-0.5 K3=0.0 # gg08_rnum=4591
CH3COCHO1+ + E1- -> CH2 + COCHO + H						RTYPE=9 K1=6.75E-08 K2=-0.5 K3=0.0 # gg08_rnum=4592
CH3COCHO1+ + E1- -> CH3CO + HCO							RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4593
CH3COCHO1+ + E1- -> CH3 + COCHO							RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4594
CH3COCH31+ + E1- -> CH3 + CO + CH3						RTYPE=9 K1=9.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=4595
CH3COCH31+ + E1- -> CH3CO + CH2 + H						RTYPE=9 K1=9.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=4596
CH3COCH31+ + E1- -> CH2CO + CH3 + H						RTYPE=9 K1=9.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=4597
CH3COCH31+ + E1- -> CH3CO + CH3							RTYPE=9 K1=3.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=4598
CH3COCH41+ + E1- -> CH3 + CO + CH3 + H					RTYPE=9 K1=6.75E-08 K2=-0.5 K3=0.0 # gg08_rnum=4599
CH3COCH41+ + E1- -> CH3 + CO + CH4						RTYPE=9 K1=6.75E-08 K2=-0.5 K3=0.0 # gg08_rnum=4600
CH3COCH41+ + E1- -> CH3CO + CH3 + H						RTYPE=9 K1=6.75E-08 K2=-0.5 K3=0.0 # gg08_rnum=4601
CH3COCH41+ + E1- -> CH2CO + CH4 + H						RTYPE=9 K1=6.75E-08 K2=-0.5 K3=0.0 # gg08_rnum=4602
CH3COCH41+ + E1- -> CH3CO + CH4							RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4603
CH3COCH41+ + E1- -> CH3COCH3 + H						RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4604
CH3COCHOH1+ + E1- -> CH3 + CO + H2CO					RTYPE=9 K1=6.75E-08 K2=-0.5 K3=0.0 # gg08_rnum=4605
CH3COCHOH1+ + E1- -> CH3 + COCHO + H					RTYPE=9 K1=6.75E-08 K2=-0.5 K3=0.0 # gg08_rnum=4606
CH3COCHOH1+ + E1- -> CH3CO + HCO + H					RTYPE=9 K1=6.75E-08 K2=-0.5 K3=0.0 # gg08_rnum=4607
CH3COCHOH1+ + E1- -> CH2CO + H2CO + H					RTYPE=9 K1=6.75E-08 K2=-0.5 K3=0.0 # gg08_rnum=4608
CH3COCHOH1+ + E1- -> CH3CO + H2CO						RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4609
CH3COCHOH1+ + E1- -> CH3COCHO + H						RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4610
CH3CONH1+ + E1- -> CH3 + CO + NH						RTYPE=9 K1=6.75E-08 K2=-0.5 K3=0.0 # gg08_rnum=4611
CH3CONH1+ + E1- -> CH3 + OCN + H						RTYPE=9 K1=6.75E-08 K2=-0.5 K3=0.0 # gg08_rnum=4612
CH3CONH1+ + E1- -> CH2 + HNCO + H						RTYPE=9 K1=6.75E-08 K2=-0.5 K3=0.0 # gg08_rnum=4613
CH3CONH1+ + E1- -> CH2CO + NH + H						RTYPE=9 K1=6.75E-08 K2=-0.5 K3=0.0 # gg08_rnum=4614
CH3CONH1+ + E1- -> CH3 + HNCO							RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4615
CH3CONH1+ + E1- -> CH3CO + NH							RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4616
CH3COOH1+ + E1- -> CH3 + CO + OH						RTYPE=9 K1=6.75E-08 K2=-0.5 K3=0.0 # gg08_rnum=4617
CH3COOH1+ + E1- -> CH2 + COOH + H						RTYPE=9 K1=6.75E-08 K2=-0.5 K3=0.0 # gg08_rnum=4618
CH3COOH1+ + E1- -> CH3 + CO2 + H						RTYPE=9 K1=6.75E-08 K2=-0.5 K3=0.0 # gg08_rnum=4619
CH3COOH1+ + E1- -> CH2CO + OH + H						RTYPE=9 K1=6.75E-08 K2=-0.5 K3=0.0 # gg08_rnum=4620
CH3COOH1+ + E1- -> CH3 + COOH							RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4621
CH3COOH1+ + E1- -> CH3CO + OH							RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4622
CH3COOH21+ + E1- -> CH3 + CO + OH + H					RTYPE=9 K1=5.40E-08 K2=-0.5 K3=0.0 # gg08_rnum=4623
CH3COOH21+ + E1- -> CH3 + CO + H2O						RTYPE=9 K1=5.40E-08 K2=-0.5 K3=0.0 # gg08_rnum=4624
CH3COOH21+ + E1- -> CH3CO + OH + H						RTYPE=9 K1=5.40E-08 K2=-0.5 K3=0.0 # gg08_rnum=4625
CH3COOH21+ + E1- -> CH2CO + H2O + H						RTYPE=9 K1=5.40E-08 K2=-0.5 K3=0.0 # gg08_rnum=4626
CH3COOH21+ + E1- -> CH3 + COOH + H						RTYPE=9 K1=5.40E-08 K2=-0.5 K3=0.0 # gg08_rnum=4627
CH3COOH21+ + E1- -> CH3CO + H2O							RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4628
CH3COOH21+ + E1- -> CH3COOH + H							RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4629
CH3CONH21+ + E1- -> CH3 + CO + NH2						RTYPE=9 K1=6.38E-08 K2=-0.5 K3=0.0 # gg08_rnum=4630
CH3CONH21+ + E1- -> CH3 + HNCO + H						RTYPE=9 K1=6.38E-08 K2=-0.5 K3=0.0 # gg08_rnum=4631
CH3CONH21+ + E1- -> CH2 + NH2CO + H						RTYPE=9 K1=6.38E-08 K2=-0.5 K3=0.0 # gg08_rnum=4632
CH3CONH21+ + E1- -> CH3CO + NH + H						RTYPE=9 K1=6.38E-08 K2=-0.5 K3=0.0 # gg08_rnum=4633
CH3CONH21+ + E1- -> CH3 + NH2CO							RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4634
CH3CONH21+ + E1- -> CH3CO + NH2							RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4635
CH3CONH21+ + E1- -> CH3CONH + H							RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4636
CH3CONH2H1+ + E1- -> CH3 + CO + NH3						RTYPE=9 K1=9.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4637
CH3CONH2H1+ + E1- -> CH3CO + NH2 + H					RTYPE=9 K1=9.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4638
CH3CONH2H1+ + E1- -> CH3 + NH2CO + H					RTYPE=9 K1=9.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4639
CH3CONH2H1+ + E1- -> CH3CONH2 + H						RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4640
CH3COOCH31+ + E1- -> CH3 + CO + CH3O					RTYPE=9 K1=6.75E-08 K2=-0.5 K3=0.0 # gg08_rnum=4641
CH3COOCH31+ + E1- -> CH3 + CO + H2CO + H				RTYPE=9 K1=6.75E-08 K2=-0.5 K3=0.0 # gg08_rnum=4642
CH3COOCH31+ + E1- -> CH3CO + H2CO + H					RTYPE=9 K1=6.75E-08 K2=-0.5 K3=0.0 # gg08_rnum=4643
CH3COOCH31+ + E1- -> CH2 + CH3OCO + H					RTYPE=9 K1=6.75E-08 K2=-0.5 K3=0.0 # gg08_rnum=4644
CH3COOCH31+ + E1- -> CH3CO + CH3O						RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4645
CH3COOCH31+ + E1- -> CH3 + CH3OCO						RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4646
CH3COOCH3H1+ + E1- -> CH3 + CO + CH3O + H				RTYPE=9 K1=6.75E-08 K2=-0.5 K3=0.0 # gg08_rnum=4647
CH3COOCH3H1+ + E1- -> CH3 + CO + CH3OH					RTYPE=9 K1=6.75E-08 K2=-0.5 K3=0.0 # gg08_rnum=4648
CH3COOCH3H1+ + E1- -> CH3 + CH3OCO + H					RTYPE=9 K1=6.75E-08 K2=-0.5 K3=0.0 # gg08_rnum=4649
CH3COOCH3H1+ + E1- -> CH3CO + CH3O + H					RTYPE=9 K1=6.75E-08 K2=-0.5 K3=0.0 # gg08_rnum=4650
CH3COOCH3H1+ + E1- -> CH3CO + CH3OH						RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4651
CH3COOCH3H1+ + E1- -> CH3COOCH3 + H						RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4652
CH3COCH2OH1+ + E1- -> CH3 + CO + CH2OH					RTYPE=9 K1=6.75E-08 K2=-0.5 K3=0.0 # gg08_rnum=4653
CH3COCH2OH1+ + E1- -> CH3 + CO + H2CO + H				RTYPE=9 K1=6.75E-08 K2=-0.5 K3=0.0 # gg08_rnum=4654
CH3COCH2OH1+ + E1- -> CH3CO + H2CO + H					RTYPE=9 K1=6.75E-08 K2=-0.5 K3=0.0 # gg08_rnum=4655
CH3COCH2OH1+ + E1- -> CH2 + HOCH2CO + H					RTYPE=9 K1=6.75E-08 K2=-0.5 K3=0.0 # gg08_rnum=4656
CH3COCH2OH1+ + E1- -> CH3CO + CH2OH						RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4657
CH3COCH2OH1+ + E1- -> CH3 + HOCH2CO						RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4658
CH3COCH2OHH1+ + E1- -> CH3 + CO + CH2OH + H				RTYPE=9 K1=6.75E-08 K2=-0.5 K3=0.0 # gg08_rnum=4659
CH3COCH2OHH1+ + E1- -> CH3 + CO + CH3OH					RTYPE=9 K1=6.75E-08 K2=-0.5 K3=0.0 # gg08_rnum=4660
CH3COCH2OHH1+ + E1- -> CH3 + HOCH2CO + H				RTYPE=9 K1=6.75E-08 K2=-0.5 K3=0.0 # gg08_rnum=4661
CH3COCH2OHH1+ + E1- -> CH3CO + CH2OH + H				RTYPE=9 K1=6.75E-08 K2=-0.5 K3=0.0 # gg08_rnum=4662
CH3COCH2OHH1+ + E1- -> CH3CO + CH3OH					RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4663
CH3COCH2OHH1+ + E1- -> CH3COCH2OH + H					RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4664
CH3OHH2COH1+ + E1- -> CH3OH + H2CO + H					RTYPE=9 K1=1.49E-07 K2=-0.5 K3=0.0 # gg08_rnum=4665
CH3OHH2COH1+ + E1- -> HCOOCH3 + H2 + H					RTYPE=9 K1=1.50E-09 K2=-0.5 K3=0.0 # gg08_rnum=4666
HNC1+ + E1- -> CN + H									RTYPE=9 K1=2.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4667
HNCHO1+ + E1- -> NH + HCO								RTYPE=9 K1=3.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4668
HNCO1+ + E1- -> CO + NH									RTYPE=9 K1=3.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4669
HNCOCHO1+ + E1- -> NH + CO + HCO						RTYPE=9 K1=9.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=4670
HNCOCHO1+ + E1- -> HNCO + CO + H						RTYPE=9 K1=9.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=4671
HNCOCHO1+ + E1- -> OCN + HCO + H						RTYPE=9 K1=9.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=4672
HNCOCHO1+ + E1- -> HNCO + HCO							RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4673
HNCOCHO1+ + E1- -> NH + COCHO							RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4674
HNCOOH1+ + E1- -> NH + CO + OH							RTYPE=9 K1=9.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=4675
HNCOOH1+ + E1- -> OCN + OH + H							RTYPE=9 K1=9.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=4676
HNCOOH1+ + E1- -> NH + CO2 + H							RTYPE=9 K1=9.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=4677
HNCOOH1+ + E1- -> HNCO + OH								RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4678
HNCOOH1+ + E1- -> NH + COOH								RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4679
HNO1+ + E1- -> NO + H									RTYPE=9 K1=3.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4680
HNOH1+ + E1- -> NH + OH									RTYPE=9 K1=3.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4681
HNS1+ + E1- -> NS + H									RTYPE=9 K1=3.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4682
HNSi1+ + E1- -> SiN + H									RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4683
HNSi1+ + E1- -> NH + Si									RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4684
HOC1+ + E1- -> CO + H									RTYPE=9 K1=2.00E-07 K2=-7.50E-01 K3=0.0 # gg08_rnum=4685
HOCOOH1+ + E1- -> OH + CO + OH							RTYPE=9 K1=1.35E-07 K2=-0.5 K3=0.0 # gg08_rnum=4686
HOCOOH1+ + E1- -> OH + CO2 + H							RTYPE=9 K1=1.35E-07 K2=-0.5 K3=0.0 # gg08_rnum=4687
HOCOOH1+ + E1- -> OH + COOH								RTYPE=9 K1=3.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=4688
HOCOOH21+ + E1- -> OH + CO + H2O						RTYPE=9 K1=1.35E-07 K2=-0.5 K3=0.0 # gg08_rnum=4689
HOCOOH21+ + E1- -> COOH + OH + H						RTYPE=9 K1=1.35E-07 K2=-0.5 K3=0.0 # gg08_rnum=4690
HOCOOH21+ + E1- -> COOH + H2O							RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4691
HOCOOH21+ + E1- -> HOCOOH + H							RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4692
HOCS1+ + E1- -> OCS + H									RTYPE=9 K1=2.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4693
HOCS1+ + E1- -> OH + CS									RTYPE=9 K1=2.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4694
HPN1+ + E1- -> PN + H									RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4695
HPN1+ + E1- -> PH + N									RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4696
HPO1+ + E1- -> PO + H									RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4697
HPO1+ + E1- -> PH + O									RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4698
NH2COOH21+ + E1- -> NH3 + CO + OH						RTYPE=9 K1=9.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=4699
NH2COOH21+ + E1- -> NH3 + CO2 + H						RTYPE=9 K1=9.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=4700
NH2COOH21+ + E1- -> NH2 + COOH + H						RTYPE=9 K1=9.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=4701
NH2COOH21+ + E1- -> NH2COOH + H							RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4702
NH2COOH21+ + E1- -> COOH + NH3							RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4703
HNH2OCH31+ + E1- -> NH2 + CH3O + H						RTYPE=9 K1=3.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=4704
HNH2OCH31+ + E1- -> NH3 + H2CO + H						RTYPE=9 K1=2.40E-07 K2=-0.5 K3=0.0 # gg08_rnum=4705
HNH2OCH31+ + E1- -> CH3O + NH3							RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4706
HNH2OCH31+ + E1- -> NH2OCH3 + H							RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4707
HNH2CH2OH1+ + E1- -> NH2 + CH2OH + H					RTYPE=9 K1=3.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=4708
HNH2CH2OH1+ + E1- -> NH3 + H2CO + H						RTYPE=9 K1=2.40E-07 K2=-0.5 K3=0.0 # gg08_rnum=4709
HNH2CH2OH1+ + E1- -> CH2OH + NH3						RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4710
HNH2CH2OH1+ + E1- -> NH2CH2OH + H						RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4711
HS1+ + E1- -> S + H										RTYPE=9 K1=2.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4712
HSiO1+ + E1- -> Si + OH									RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4713
HSiO1+ + E1- -> SiO + H									RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4714
HSiO21+ + E1- -> SiO + OH								RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4715
HSiO21+ + E1- -> SiO2 + H								RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4716
HSiS1+ + E1- -> Si + HS									RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4717
HSiS1+ + E1- -> SiS + H									RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4718
HSO1+ + E1- -> SO + H									RTYPE=9 K1=2.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4719
HSO21+ + E1- -> SO2 + H									RTYPE=9 K1=2.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4720
HSO21+ + E1- -> SO + H + O								RTYPE=9 K1=1.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4721
HSO21+ + E1- -> SO + OH									RTYPE=9 K1=1.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4722
HCOCOCHO1+ + E1- -> HCO + CO + HCO						RTYPE=9 K1=1.35E-07 K2=-0.5 K3=0.0 # gg08_rnum=4723
HCOCOCHO1+ + E1- -> COCHO + CO + H						RTYPE=9 K1=1.35E-07 K2=-0.5 K3=0.0 # gg08_rnum=4724
HCOCOCHO1+ + E1- -> COCHO + HCO							RTYPE=9 K1=3.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=4725
HCOCOCHOH1+ + E1- -> HCO + CO + HCO + H					RTYPE=9 K1=9.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4726
HCOCOCHOH1+ + E1- -> HCO + CO + H2CO					RTYPE=9 K1=9.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4727
HCOCOCHOH1+ + E1- -> COCHO + HCO + H					RTYPE=9 K1=9.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4728
HCOCOCHOH1+ + E1- -> COCHO + H2CO						RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4729
N21+ + E1- -> N + N										RTYPE=9 K1=1.80E-07 K2=-3.90E-01 K3=0.0 # gg08_rnum=4730
N2H1+ + E1- -> NH + N									RTYPE=9 K1=6.40E-08 K2=-5.10E-01 K3=0.0 # gg08_rnum=4731
N2H1+ + E1- -> N2 + H									RTYPE=9 K1=3.60E-08 K2=-5.10E-01 K3=0.0 # gg08_rnum=4732
NaH21+ + E1- -> Na + H2									RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4733
NaH21+ + E1- -> NaH + H									RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4734
NaH2O1+ + E1- -> NaOH + H								RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4735
NaH2O1+ + E1- -> Na + H2O								RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4736
OCN1+ + E1- -> CO + N									RTYPE=9 K1=3.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4737
NH1+ + E1- -> N + H										RTYPE=9 K1=1.18E-07 K2=-0.5 K3=0.0 # gg08_rnum=4738
NH21+ + E1- -> N + H + H								RTYPE=9 K1=2.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4739
NH21+ + E1- -> NH + H									RTYPE=9 K1=1.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4740
NH2CHOH1+ + E1- -> NH2 + HCO + H						RTYPE=9 K1=1.35E-07 K2=-0.5 K3=0.0 # gg08_rnum=4741
NH2CHOH1+ + E1- -> NH + H2CO + H						RTYPE=9 K1=1.35E-07 K2=-0.5 K3=0.0 # gg08_rnum=4742
NH2CHOH1+ + E1- -> NH2 + H2CO							RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4743
NH2CHOH1+ + E1- -> NH2CHO + H							RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4744
NH2CNH1+ + E1- -> NH2 + CN + H							RTYPE=9 K1=1.35E-07 K2=-0.5 K3=0.0 # gg08_rnum=4745
NH2CNH1+ + E1- -> NH + HNC + H							RTYPE=9 K1=1.35E-07 K2=-0.5 K3=0.0 # gg08_rnum=4746
NH2CNH1+ + E1- -> NH2 + HNC								RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4747
NH2CNH1+ + E1- -> NH2CN + H								RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4748
NH2OH1+ + E1- -> NH2 + OH								RTYPE=9 K1=3.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4749
NH31+ + E1- -> NH2 + H									RTYPE=9 K1=1.55E-07 K2=-0.5 K3=0.0 # gg08_rnum=4750
NH31+ + E1- -> NH + H + H								RTYPE=9 K1=1.55E-07 K2=-0.5 K3=0.0 # gg08_rnum=4751
NH3OH1+ + E1- -> NH3 + OH								RTYPE=9 K1=3.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4752
NH41+ + E1- -> NH2 + H2									RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4753
NH41+ + E1- -> NH2 + H + H								RTYPE=9 K1=3.20E-07 K2=-0.5 K3=0.0 # gg08_rnum=4754
NH41+ + E1- -> NH3 + H									RTYPE=9 K1=1.05E-06 K2=-0.5 K3=0.0 # gg08_rnum=4755
NHCONH1+ + E1- -> NH + OCN + H							RTYPE=9 K1=2.70E-07 K2=-0.5 K3=0.0 # gg08_rnum=4756
NHCONH1+ + E1- -> NH + HNCO								RTYPE=9 K1=3.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=4757
NO1+ + E1- -> N + O										RTYPE=9 K1=4.10E-07 K2=-1.00E+00 K3=0.0 # gg08_rnum=4758
NO21+ + E1- -> O + NO									RTYPE=9 K1=3.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4759
NS1+ + E1- -> N + S										RTYPE=9 K1=2.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4760
O21+ + E1- -> O + O										RTYPE=9 K1=1.95E-07 K2=-7.00E-01 K3=0.0 # gg08_rnum=4761
O2H1+ + E1- -> O2 + H									RTYPE=9 K1=3.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4762
OCS1+ + E1- -> CO + S									RTYPE=9 K1=3.00E-07 K2=0.0 K3=0.0 # gg08_rnum=4763
OCS1+ + E1- -> CS + O									RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4764
HOCSH1+ + E1- -> OCS + H2								RTYPE=9 K1=3.00E-07 K2=0.0 K3=0.0 # gg08_rnum=4765
OH1+ + E1- -> O + H										RTYPE=9 K1=6.30E-09 K2=-4.80E-01 K3=0.0 # gg08_rnum=4766
CHOCHOH1+ + E1- -> HCO + HCO + H						RTYPE=9 K1=2.70E-07 K2=-0.5 K3=0.0 # gg08_rnum=4767
CHOCHOH1+ + E1- -> HCO + H2CO							RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4768
CHOCHOH1+ + E1- -> CHOCHO + H							RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4769
CHOCHO1+ + E1- -> CO + HCO + H							RTYPE=9 K1=2.70E-07 K2=-0.5 K3=0.0 # gg08_rnum=4770
CHOCHO1+ + E1- -> HCO + HCO								RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4771
CHOCHO1+ + E1- -> COCHO + H								RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4772
HCOCOOH1+ + E1- -> HCO + CO + OH						RTYPE=9 K1=9.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=4773
HCOCOOH1+ + E1- -> HCO + CO2 + H						RTYPE=9 K1=9.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=4774
HCOCOOH1+ + E1- -> CO + COOH + H						RTYPE=9 K1=9.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=4775
HCOCOOH1+ + E1- -> COCHO + OH							RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4776
HCOCOOH1+ + E1- -> HCO + COOH							RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4777
NH2COCHOH1+ + E1- -> HCO + CO + NH3						RTYPE=9 K1=9.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=4778
NH2COCHOH1+ + E1- -> HCO + NH2CO + H					RTYPE=9 K1=9.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=4779
NH2COCHOH1+ + E1- -> COCHO + NH2 + H					RTYPE=9 K1=9.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=4780
NH2COCHOH1+ + E1- -> COCHO + NH3						RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4781
NH2COCHOH1+ + E1- -> NH2COCHO + H						RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4782
HCOCOOH21+ + E1- -> HCO + CO + H2O						RTYPE=9 K1=2.10E-07 K2=-0.5 K3=0.0 # gg08_rnum=4783
HCOCOOH21+ + E1- -> HCO + COOH + H						RTYPE=9 K1=3.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=4784
HCOCOOH21+ + E1- -> COCHO + OH + H						RTYPE=9 K1=3.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=4785
HCOCOOH21+ + E1- -> COCHO + H2O							RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4786
HCOCOOH21+ + E1- -> HCOCOOH + H							RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4787
HCCP1+ + E1- -> CCP + H									RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4788
HCCP1+ + E1- -> CP + CH									RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4789
PC2H21+ + E1- -> P + HC2H								RTYPE=9 K1=1.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4790
PC2H21+ + E1- -> CCP + H2								RTYPE=9 K1=1.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4791
PC2H21+ + E1- -> HCCP + H								RTYPE=9 K1=1.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4792
PC2H31+ + E1- -> CCP + H2 + H							RTYPE=9 K1=1.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4793
PC2H31+ + E1- -> HCCP + H2								RTYPE=9 K1=1.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4794
PC2H31+ + E1- -> PH + HC2H								RTYPE=9 K1=1.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4795
PC2H41+ + E1- -> HCCP + H2 + H							RTYPE=9 K1=1.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4796
PC2H41+ + E1- -> P + H2C2H2								RTYPE=9 K1=1.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4797
PC2H41+ + E1- -> CCP + H2 + H2							RTYPE=9 K1=1.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4798
PC3H1+ + E1- -> CCP + CH								RTYPE=9 K1=1.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4799
PC3H1+ + E1- -> C3P + H									RTYPE=9 K1=1.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4800
PC3H1+ + E1- -> HCCP + C								RTYPE=9 K1=1.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4801
PC4H1+ + E1- -> C3P + CH								RTYPE=9 K1=7.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4802
PC4H1+ + E1- -> C4P + H									RTYPE=9 K1=7.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4803
PC4H1+ + E1- -> CP + C3H								RTYPE=9 K1=7.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4804
PC4H1+ + E1- -> CCP + C2H								RTYPE=9 K1=7.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4805
PC4H21+ + E1- -> C4P + H2								RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4806
PC4H21+ + E1- -> CCP + HC2H								RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4807
PCH21+ + E1- -> HCP + H									RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4808
PCH21+ + E1- -> CP + H2									RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4809
CH2PH1+ + E1- -> CP + H2 + H							RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4810
CH2PH1+ + E1- -> HCP + H2								RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4811
CH2PH1+ + E1- -> CH3 + P								RTYPE=9 K1=3.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4812
CH2PH21+ + E1- -> HCP + H2 + H							RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4813
CH2PH21+ + E1- -> CH2PH + H								RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4814
CH2PH21+ + E1- -> CH4 + P								RTYPE=9 K1=3.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4815
PH1+ + E1- -> P + H										RTYPE=9 K1=1.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4816
PH21+ + E1- -> P + H2									RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4817
PH21+ + E1- -> PH + H									RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4818
PH31+ + E1- -> PH2 + H									RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4819
PH31+ + E1- -> PH + H2									RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4820
PN1+ + E1- -> P + N										RTYPE=9 K1=1.80E-07 K2=-0.5 K3=0.0 # gg08_rnum=4821
PNH21+ + E1- -> NH2 + P									RTYPE=9 K1=3.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4822
PNH31+ + E1- -> NH3 + P									RTYPE=9 K1=3.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4823
PO1+ + E1- -> P + O										RTYPE=9 K1=1.80E-07 K2=-0.5 K3=0.0 # gg08_rnum=4824
NH2CHO1+ + E1- -> NH2 + CO + H							RTYPE=9 K1=1.35E-07 K2=-0.5 K3=0.0 # gg08_rnum=4825
NH2CHO1+ + E1- -> NH + HCO + H							RTYPE=9 K1=1.35E-07 K2=-0.5 K3=0.0 # gg08_rnum=4826
NH2CHO1+ + E1- -> NH2 + HCO								RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4827
NH2CHO1+ + E1- -> NH2CO + H								RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4828
NH2CO1+ + E1- -> NH + CO + H							RTYPE=9 K1=2.70E-07 K2=-0.5 K3=0.0 # gg08_rnum=4829
NH2CO1+ + E1- -> NH2 + CO								RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4830
NH2CO1+ + E1- -> HNCO + H								RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4831
NH2COCHO1+ + E1- -> NH2 + CO + HCO						RTYPE=9 K1=6.75E-08 K2=-0.5 K3=0.0 # gg08_rnum=4832
NH2COCHO1+ + E1- -> NH2CO + CO + H						RTYPE=9 K1=6.75E-08 K2=-0.5 K3=0.0 # gg08_rnum=4833
NH2COCHO1+ + E1- -> HNCO + HCO + H						RTYPE=9 K1=6.75E-08 K2=-0.5 K3=0.0 # gg08_rnum=4834
NH2COCHO1+ + E1- -> NH + COCHO + H						RTYPE=9 K1=6.75E-08 K2=-0.5 K3=0.0 # gg08_rnum=4835
NH2COCHO1+ + E1- -> NH2CO + HCO							RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4836
NH2COCHO1+ + E1- -> COCHO + NH2							RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4837
NH2CONH1+ + E1- -> OCN + NH2 + H						RTYPE=9 K1=1.35E-07 K2=-0.5 K3=0.0 # gg08_rnum=4838
NH2CONH1+ + E1- -> HNCO + NH + H						RTYPE=9 K1=1.35E-07 K2=-0.5 K3=0.0 # gg08_rnum=4839
NH2CONH1+ + E1- -> NH + NH2CO							RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4840
NH2CONH1+ + E1- -> HNCO + NH2							RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4841
NH2COOH1+ + E1- -> NH2 + CO + OH						RTYPE=9 K1=6.75E-08 K2=-0.5 K3=0.0 # gg08_rnum=4842
NH2COOH1+ + E1- -> NH2 + CO2 + H						RTYPE=9 K1=6.75E-08 K2=-0.5 K3=0.0 # gg08_rnum=4843
NH2COOH1+ + E1- -> NH + COOH + H						RTYPE=9 K1=6.75E-08 K2=-0.5 K3=0.0 # gg08_rnum=4844
NH2COOH1+ + E1- -> HNCO + OH + H						RTYPE=9 K1=6.75E-08 K2=-0.5 K3=0.0 # gg08_rnum=4845
NH2COOH1+ + E1- -> COOH + NH2							RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4846
NH2COOH1+ + E1- -> OH + NH2CO							RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4847
NH2CONH21+ + E1- -> NH2 + NH2 + CO						RTYPE=9 K1=9.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=4848
NH2CONH21+ + E1- -> NH + NH2CO + H						RTYPE=9 K1=9.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=4849
NH2CONH21+ + E1- -> NH2 + HNCO + H						RTYPE=9 K1=9.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=4850
NH2CONH21+ + E1- -> NH2 + NH2CO							RTYPE=9 K1=3.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=4851
NH2CONH21+ + E1- -> NH2CONH + H							RTYPE=9 K1=3.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=4852
NH2CONH2H1+ + E1- -> NH2 + CO + NH3						RTYPE=9 K1=9.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=4853
NH2CONH2H1+ + E1- -> NH2CO + NH2 + H					RTYPE=9 K1=9.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=4854
NH2CONH2H1+ + E1- -> HNCO + NH3 + H						RTYPE=9 K1=9.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=4855
NH2CONH2H1+ + E1- -> NH2CONH2 + H						RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4856
NH2CONH2H1+ + E1- -> NH2CO + NH3						RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4857
NH2NH1+ + E1- -> NH + NH + H							RTYPE=9 K1=2.85E-07 K2=-0.5 K3=0.0 # gg08_rnum=4858
NH2NH1+ + E1- -> NH2 + NH								RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4859
NH2NH21+ + E1- -> NH2 + NH + H							RTYPE=9 K1=2.70E-07 K2=-0.5 K3=0.0 # gg08_rnum=4860
NH2NH21+ + E1- -> NH2NH + H								RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4861
NH2NH21+ + E1- -> NH2 + NH2								RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4862
NH2NH2H1+ + E1- -> NH2 + NH2 + H						RTYPE=9 K1=2.70E-07 K2=-0.5 K3=0.0 # gg08_rnum=4863
NH2NH2H1+ + E1- -> NH2 + NH3							RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4864
NH2NH2H1+ + E1- -> NH2NH2 + H							RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4865
NH2OCH31+ + E1- -> CH3O + NH + H						RTYPE=9 K1=1.35E-07 K2=-0.5 K3=0.0 # gg08_rnum=4866
NH2OCH31+ + E1- -> H2CO + NH2 + H						RTYPE=9 K1=1.35E-07 K2=-0.5 K3=0.0 # gg08_rnum=4867
NH2OCH31+ + E1- -> CH3O + NH2							RTYPE=9 K1=3.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=4868
NH2CH2OH1+ + E1- -> CH2OH + NH + H						RTYPE=9 K1=1.35E-07 K2=-0.5 K3=0.0 # gg08_rnum=4869
NH2CH2OH1+ + E1- -> H2CO + NH2 + H						RTYPE=9 K1=1.35E-07 K2=-0.5 K3=0.0 # gg08_rnum=4870
NH2CH2OH1+ + E1- -> CH2OH + NH2							RTYPE=9 K1=3.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=4871
S21+ + E1- -> S + S										RTYPE=9 K1=2.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4872
S2H1+ + E1- -> HS + S									RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4873
S2H1+ + E1- -> S2 + H									RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4874
SiC1+ + E1- -> Si + C									RTYPE=9 K1=2.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4875
SiC21+ + E1- -> Si + C2									RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4876
SiC21+ + E1- -> SiC + C									RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4877
SiC2H1+ + E1- -> C2H + Si								RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4878
SiC2H1+ + E1- -> SiC2 + H								RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4879
SiC2H21+ + E1- -> SiC2 + H2								RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4880
SiC2H21+ + E1- -> SiC2H + H								RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4881
SiC2H31+ + E1- -> SiC2H + H2							RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4882
SiC2H31+ + E1- -> SiC2H2 + H							RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4883
SiC31+ + E1- -> SiC + C2								RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4884
SiC31+ + E1- -> SiC2 + C								RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4885
SiC3H1+ + E1- -> Si + C3H								RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4886
SiC3H1+ + E1- -> cSiC3 + H								RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4887
SiC3H21+ + E1- -> cSiC3 + H2							RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4888
SiC3H21+ + E1- -> SiC3H + H								RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4889
SiC41+ + E1- -> SiC2 + C2								RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4890
SiC41+ + E1- -> cSiC3 + C								RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4891
SiC4H1+ + E1- -> cSiC3 + CH								RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4892
SiC4H1+ + E1- -> SiC4 + H								RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4893
SiCH31+ + E1- -> SiCH + H2								RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4894
SiCH31+ + E1- -> SiCH2 + H								RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4895
SiCH41+ + E1- -> SiCH2 + H2								RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4896
SiCH41+ + E1- -> SiCH3 + H								RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4897
SiH1+ + E1- -> Si + H									RTYPE=9 K1=2.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4898
SiH21+ + E1- -> SiH + H									RTYPE=9 K1=2.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4899
SiH21+ + E1- -> Si + H + H								RTYPE=9 K1=2.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4900
SiH21+ + E1- -> Si + H2									RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4901
SiH31+ + E1- -> SiH + H2								RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4902
SiH31+ + E1- -> SiH2 + H								RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4903
SiH41+ + E1- -> SiH2 + H2								RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4904
SiH41+ + E1- -> SiH3 + H								RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4905
SiH51+ + E1- -> SiH3 + H2								RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4906
SiH51+ + E1- -> SiH4 + H								RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4907
SiN1+ + E1- -> Si + N									RTYPE=9 K1=2.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4908
SiNC1+ + E1- -> Si + CN									RTYPE=9 K1=3.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4909
SiNCH1+ + E1- -> SiN + CH								RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4910
SiNCH1+ + E1- -> SiNC + H								RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4911
SiNH21+ + E1- -> SiN + H2								RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4912
SiNH21+ + E1- -> HNSi + H								RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4913
SiO1+ + E1- -> Si + O									RTYPE=9 K1=2.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4914
SiS1+ + E1- -> Si + S									RTYPE=9 K1=2.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4915
SO1+ + E1- -> O + S										RTYPE=9 K1=2.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4916
SO21+ + E1- -> SO + O									RTYPE=9 K1=2.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4917
HCOOCH31+ + E1- -> CH3O + CO + H						RTYPE=9 K1=1.35E-07 K2=-0.5 K3=0.0 # gg08_rnum=4918
HCOOCH31+ + E1- -> H2CO + HCO + H						RTYPE=9 K1=1.35E-07 K2=-0.5 K3=0.0 # gg08_rnum=4919
HCOOCH31+ + E1- -> CH3O + HCO							RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4920
HCOOCH31+ + E1- -> CH3OCO + H							RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4921
CH3OCO1+ + E1- -> H2CO + CO + H							RTYPE=9 K1=2.70E-07 K2=-0.5 K3=0.0 # gg08_rnum=4922
CH3OCO1+ + E1- -> CH3O + CO								RTYPE=9 K1=3.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=4923
CH3OCOCHO1+ + E1- -> CH3O + CO + HCO					RTYPE=9 K1=1.35E-07 K2=-0.5 K3=0.0 # gg08_rnum=4924
CH3OCOCHO1+ + E1- -> CH3OCO + CO + H					RTYPE=9 K1=1.35E-07 K2=-0.5 K3=0.0 # gg08_rnum=4925
CH3OCOCHO1+ + E1- -> CH3OCO + HCO						RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4926
CH3OCOCHO1+ + E1- -> CH3O + COCHO						RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4927
CH3OCOCHOH1+ + E1- -> CH3O + CO + H2CO					RTYPE=9 K1=9.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=4928
CH3OCOCHOH1+ + E1- -> CH3OCO + HCO + H					RTYPE=9 K1=9.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=4929
CH3OCOCHOH1+ + E1- -> CH3O + COCHO + H					RTYPE=9 K1=9.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=4930
CH3OCOCHOH1+ + E1- -> CH3OCO + H2CO						RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4931
CH3OCOCHOH1+ + E1- -> CH3OCOCHO + H						RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4932
CH3OCONH1+ + E1- -> CH3O + CO + NH						RTYPE=9 K1=9.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=4933
CH3OCONH1+ + E1- -> CH3O + OCN + H						RTYPE=9 K1=9.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=4934
CH3OCONH1+ + E1- -> H2CO + HNCO + H						RTYPE=9 K1=9.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=4935
CH3OCONH1+ + E1- -> CH3OCO + NH							RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4936
CH3OCONH1+ + E1- -> CH3O + HNCO							RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4937
CH3OCOOH1+ + E1- -> CH3O + CO + OH						RTYPE=9 K1=9.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4938
CH3OCOOH1+ + E1- -> H2CO + COOH + H						RTYPE=9 K1=9.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=4939
CH3OCOOH1+ + E1- -> CH3O + CO2 + H						RTYPE=9 K1=9.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=4940
CH3OCOOH1+ + E1- -> CH3OCO + OH							RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4941
CH3OCOOH1+ + E1- -> CH3O + COOH							RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4942
CH3OCOOH21+ + E1- -> CH3O + CO + OH + H					RTYPE=9 K1=6.75E-08 K2=-0.5 K3=0.0 # gg08_rnum=4943
CH3OCOOH21+ + E1- -> CH3O + CO + H2O					RTYPE=9 K1=6.75E-08 K2=-0.5 K3=0.0 # gg08_rnum=4944
CH3OCOOH21+ + E1- -> CH3OCO + OH + H					RTYPE=9 K1=6.75E-08 K2=-0.5 K3=0.0 # gg08_rnum=4945
CH3OCOOH21+ + E1- -> CH3O + COOH + H					RTYPE=9 K1=6.75E-08 K2=-0.5 K3=0.0 # gg08_rnum=4946
CH3OCOOH21+ + E1- -> CH3OCO + H2O						RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4947
CH3OCOOH21+ + E1- -> CH3OCOOH + H						RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4948
CH3OCONH21+ + E1- -> CH3O + CO + NH2					RTYPE=9 K1=6.75E-08 K2=-0.5 K3=0.0 # gg08_rnum=4949
CH3OCONH21+ + E1- -> CH3OCO + NH + H					RTYPE=9 K1=6.75E-08 K2=-0.5 K3=0.0 # gg08_rnum=4950
CH3OCONH21+ + E1- -> CH3O + HNCO + H					RTYPE=9 K1=6.75E-08 K2=-0.5 K3=0.0 # gg08_rnum=4951
CH3OCONH21+ + E1- -> H2CO + NH2CO + H					RTYPE=9 K1=6.75E-08 K2=-0.5 K3=0.0 # gg08_rnum=4952
CH3OCONH21+ + E1- -> CH3O + NH2CO						RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4953
CH3OCONH21+ + E1- -> CH3OCO + NH2						RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4954
CH3OCONH2H1+ + E1- -> CH3O + CO + NH2 + H				RTYPE=9 K1=6.75E-08 K2=-0.5 K3=0.0 # gg08_rnum=4955
CH3OCONH2H1+ + E1- -> CH3O + CO + NH3					RTYPE=9 K1=6.75E-08 K2=-0.5 K3=0.0 # gg08_rnum=4956
CH3OCONH2H1+ + E1- -> CH3OCO + NH2 + H					RTYPE=9 K1=6.75E-08 K2=-0.5 K3=0.0 # gg08_rnum=4957
CH3OCONH2H1+ + E1- -> CH3O + NH2CO + H					RTYPE=9 K1=6.75E-08 K2=-0.5 K3=0.0 # gg08_rnum=4958
CH3OCONH2H1+ + E1- -> CH3OCO + NH3						RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4959
CH3OCONH2H1+ + E1- -> CH3OCONH2 + H						RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4960
CH3OCOOCH31+ + E1- -> CH3O + CO + CH3O					RTYPE=9 K1=1.35E-07 K2=-0.5 K3=0.0 # gg08_rnum=4961
CH3OCOOCH31+ + E1- -> H2CO + CH3OCO + H					RTYPE=9 K1=1.35E-07 K2=-0.5 K3=0.0 # gg08_rnum=4962
CH3OCOOCH31+ + E1- -> CH3O + CH3OCO						RTYPE=9 K1=3.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=4963
CH3OCOOCH3H1+ + E1- -> CH3O + CO + CH3O + H				RTYPE=9 K1=9.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=4964
CH3OCOOCH3H1+ + E1- -> CH3O + CO + CH3OH				RTYPE=9 K1=9.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=4965
CH3OCOOCH3H1+ + E1- -> CH3O + CH3OCO + H				RTYPE=9 K1=9.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=4966
CH3OCOOCH3H1+ + E1- -> CH3OCO + CH3OH					RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4967
CH3OCOOCH3H1+ + E1- -> CH3OCOOCH3 + H					RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4968
CH3OCOCH2OH1+ + E1- -> CH3O + CO + CH2OH				RTYPE=9 K1=9.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=4969
CH3OCOCH2OH1+ + E1- -> H2CO + HOCH2CO + H				RTYPE=9 K1=9.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=4970
CH3OCOCH2OH1+ + E1- -> CH3OCO + H2CO + H				RTYPE=9 K1=9.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=4971
CH3OCOCH2OH1+ + E1- -> CH3OCO + CH2OH					RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4972
CH3OCOCH2OH1+ + E1- -> CH3O + HOCH2CO					RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4973
CH3OCOCH2OHH1+ + E1- -> CH3O + CO + CH2OH + H			RTYPE=9 K1=6.75E-08 K2=-0.5 K3=0.0 # gg08_rnum=4974
CH3OCOCH2OHH1+ + E1- -> CH3O + CO + CH3OH				RTYPE=9 K1=6.75E-08 K2=-0.5 K3=0.0 # gg08_rnum=4975
CH3OCOCH2OHH1+ + E1- -> CH3O + HOCH2CO + H				RTYPE=9 K1=6.75E-08 K2=-0.5 K3=0.0 # gg08_rnum=4976
CH3OCOCH2OHH1+ + E1- -> CH3OCO + CH2OH + H				RTYPE=9 K1=6.75E-08 K2=-0.5 K3=0.0 # gg08_rnum=4977
CH3OCOCH2OHH1+ + E1- -> CH3OCO + CH3OH					RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4978
CH3OCOCH2OHH1+ + E1- -> CH3OCOCH2OH + H					RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4979
CH3ONH1+ + E1- -> H2CO + NH + H							RTYPE=9 K1=2.70E-07 K2=-0.5 K3=0.0 # gg08_rnum=4980
CH3ONH1+ + E1- -> CH3O + NH								RTYPE=9 K1=3.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=4981
CH3OOH1+ + E1- -> H2CO + OH + H							RTYPE=9 K1=2.70E-07 K2=-0.5 K3=0.0 # gg08_rnum=4982
CH3OOH1+ + E1- -> CH3O + OH								RTYPE=9 K1=3.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=4983
CH3OOH21+ + E1- -> CH3O + OH + H						RTYPE=9 K1=3.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=4984
CH3OOH21+ + E1- -> H2CO + H2O + H						RTYPE=9 K1=2.40E-07 K2=-0.5 K3=0.0 # gg08_rnum=4985
CH3OOH21+ + E1- -> CH3O + H2O							RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4986
CH3OOH21+ + E1- -> CH3OOH + H							RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4987
CH3OOCH31+ + E1- -> H2CO + CH3O + H						RTYPE=9 K1=2.70E-07 K2=-0.5 K3=0.0 # gg08_rnum=4988
CH3OOCH31+ + E1- -> CH3O + CH3O							RTYPE=9 K1=3.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=4989
CH3OCH2OH1+ + E1- -> H2CO + CH2OH + H					RTYPE=9 K1=1.35E-07 K2=-0.5 K3=0.0 # gg08_rnum=4990
CH3OCH2OH1+ + E1- -> CH3O + H2CO + H					RTYPE=9 K1=1.35E-07 K2=-0.5 K3=0.0 # gg08_rnum=4991
CH3OCH2OH1+ + E1- -> CH3O + CH2OH						RTYPE=9 K1=3.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=4992
CH3OCH2OH21+ + E1- -> CH3O + CH2OH + H					RTYPE=9 K1=3.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=4993
CH3OCH2OH21+ + E1- -> H2CO + CH3OH + H					RTYPE=9 K1=2.40E-07 K2=-0.5 K3=0.0 # gg08_rnum=4994
CH3OCH2OH21+ + E1- -> CH3O + CH3OH						RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4995
CH3OCH2OH21+ + E1- -> CH3OCH2OH + H						RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=4996
CH2OHCHO1+ + E1- -> CH2 + OH + HCO						RTYPE=9 K1=9.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=4997
CH2OHCHO1+ + E1- -> CH2OH + CO + H						RTYPE=9 K1=9.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=4998
CH2OHCHO1+ + E1- -> H2CO + HCO + H						RTYPE=9 K1=9.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=4999
CH2OHCHO1+ + E1- -> CH2OH + HCO							RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=5000
CH2OHCHO1+ + E1- -> HOCH2CO + H							RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=5001
HOCH2CO1+ + E1- -> CH2 + OH + CO						RTYPE=9 K1=1.43E-07 K2=-0.5 K3=0.0 # gg08_rnum=5002
HOCH2CO1+ + E1- -> H2CO + CO + H						RTYPE=9 K1=1.43E-07 K2=-0.5 K3=0.0 # gg08_rnum=5003
HOCH2CO1+ + E1- -> CH2OH + CO							RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=5004
HOCH2COCHO1+ + E1- -> CH2OH + CO + HCO					RTYPE=9 K1=9.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=5005
HOCH2COCHO1+ + E1- -> HOCH2CO + CO + H					RTYPE=9 K1=9.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=5006
HOCH2COCHO1+ + E1- -> H2CO + COCHO + H					RTYPE=9 K1=9.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=5007
HOCH2COCHO1+ + E1- -> CH2OH + COCHO						RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=5008
HOCH2COCHO1+ + E1- -> HOCH2CO + HCO						RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=5009
HOCH2COCHOH1+ + E1- -> CH2OH + CO + H2CO				RTYPE=9 K1=9.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=5010
HOCH2COCHOH1+ + E1- -> HOCH2CO + HCO + H				RTYPE=9 K1=9.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=5011
HOCH2COCHOH1+ + E1- -> CH2OH + COCHO + H				RTYPE=9 K1=9.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=5012
HOCH2COCHOH1+ + E1- -> HOCH2CO + H2CO					RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=5013
HOCH2COCHOH1+ + E1- -> HOCH2COCHO + H					RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=5014
HNCOCH2OH1+ + E1- -> CH2OH + CO + NH					RTYPE=9 K1=9.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=5015
HNCOCH2OH1+ + E1- -> CH2OH + OCN + H					RTYPE=9 K1=9.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=5016
HNCOCH2OH1+ + E1- -> H2CO + HNCO + H					RTYPE=9 K1=9.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=5017
HNCOCH2OH1+ + E1- -> CH2OH + HNCO						RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=5018
HNCOCH2OH1+ + E1- -> HOCH2CO + NH						RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=5019
CH2OHCOOH1+ + E1- -> CH2OH + CO + OH					RTYPE=9 K1=6.75E-08 K2=-0.5 K3=0.0 # gg08_rnum=5020
CH2OHCOOH1+ + E1- -> CH2 + OH + COOH					RTYPE=9 K1=6.75E-08 K2=-0.5 K3=0.0 # gg08_rnum=5021
CH2OHCOOH1+ + E1- -> CH2OH + CO2 + H					RTYPE=9 K1=6.75E-08 K2=-0.5 K3=0.0 # gg08_rnum=5022
CH2OHCOOH1+ + E1- -> H2CO + COOH + H					RTYPE=9 K1=6.75E-08 K2=-0.5 K3=0.0 # gg08_rnum=5023
CH2OHCOOH1+ + E1- -> CH2OH + COOH						RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=5024
CH2OHCOOH1+ + E1- -> HOCH2CO + OH						RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=5025
CH2OHCOOH21+ + E1- -> CH2OH + CO + OH + H				RTYPE=9 K1=6.75E-08 K2=-0.5 K3=0.0 # gg08_rnum=5026
CH2OHCOOH21+ + E1- -> CH2OH + CO + H2O					RTYPE=9 K1=6.75E-08 K2=-0.5 K3=0.0 # gg08_rnum=5027
CH2OHCOOH21+ + E1- -> HOCH2CO + OH + H					RTYPE=9 K1=6.75E-08 K2=-0.5 K3=0.0 # gg08_rnum=5028
CH2OHCOOH21+ + E1- -> CH2OH + COOH + H					RTYPE=9 K1=6.75E-08 K2=-0.5 K3=0.0 # gg08_rnum=5029
CH2OHCOOH21+ + E1- -> HOCH2CO + H2O						RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=5030
CH2OHCOOH21+ + E1- -> CH2OHCOOH + H						RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=5031
NH2COCH2OH1+ + E1- -> CH2OH + CO + NH2					RTYPE=9 K1=6.75E-08 K2=-0.5 K3=0.0 # gg08_rnum=5032
NH2COCH2OH1+ + E1- -> CH2OH + HNCO + H					RTYPE=9 K1=6.75E-08 K2=-0.5 K3=0.0 # gg08_rnum=5033
NH2COCH2OH1+ + E1- -> H2CO + NH2CO + H					RTYPE=9 K1=6.75E-08 K2=-0.5 K3=0.0 # gg08_rnum=5034
NH2COCH2OH1+ + E1- -> HOCH2CO + NH + H					RTYPE=9 K1=6.75E-08 K2=-0.5 K3=0.0 # gg08_rnum=5035
NH2COCH2OH1+ + E1- -> CH2OH + NH2CO						RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=5036
NH2COCH2OH1+ + E1- -> HOCH2CO + NH2						RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=5037
NH2COCH2OH21+ + E1- -> CH2OH + CO + NH2 + H				RTYPE=9 K1=6.75E-08 K2=-0.5 K3=0.0 # gg08_rnum=5038
NH2COCH2OH21+ + E1- -> CH2OH + CO + NH3					RTYPE=9 K1=6.75E-08 K2=-0.5 K3=0.0 # gg08_rnum=5039
NH2COCH2OH21+ + E1- -> HOCH2CO + NH2 + H				RTYPE=9 K1=6.75E-08 K2=-0.5 K3=0.0 # gg08_rnum=5040
NH2COCH2OH21+ + E1- -> CH2OH + NH2CO + H				RTYPE=9 K1=6.75E-08 K2=-0.5 K3=0.0 # gg08_rnum=5041
NH2COCH2OH21+ + E1- -> HOCH2CO + NH3					RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=5042
NH2COCH2OH21+ + E1- -> NH2COCH2OH + H					RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=5043
HOCH2COCH2OH1+ + E1- -> CH2OH + CO + CH2OH				RTYPE=9 K1=1.35E-07 K2=-0.5 K3=0.0 # gg08_rnum=5044
HOCH2COCH2OH1+ + E1- -> H2CO + HOCH2CO + H				RTYPE=9 K1=1.35E-07 K2=-0.5 K3=0.0 # gg08_rnum=5045
HOCH2COCH2OH1+ + E1- -> HOCH2CO + CH2OH					RTYPE=9 K1=3.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=5046
HOCH2COCH2OH21+ + E1- -> CH2OH + CO + CH2OH + H			RTYPE=9 K1=9.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=5047
HOCH2COCH2OH21+ + E1- -> CH2OH + CO + CH3OH				RTYPE=9 K1=9.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=5048
HOCH2COCH2OH21+ + E1- -> CH2OH + HOCH2CO + H			RTYPE=9 K1=9.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=5049
HOCH2COCH2OH21+ + E1- -> HOCH2COCH2OH + H				RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=5050
CH2OHCHOH1+ + E1- -> CH2OH + HCO + H					RTYPE=9 K1=3.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=5051
CH2OHCHOH1+ + E1- -> H2CO + H2CO + H					RTYPE=9 K1=2.40E-07 K2=-0.5 K3=0.0 # gg08_rnum=5052
CH2OHCHOH1+ + E1- -> CH2OH + H2CO						RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=5053
CH2OHCHOH1+ + E1- -> CH2OHCHO + H						RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=5054
CH2OHNH1+ + E1- -> H2CO + NH + H						RTYPE=9 K1=2.70E-07 K2=-0.5 K3=0.0 # gg08_rnum=5055
CH2OHNH1+ + E1- -> CH2OH + NH							RTYPE=9 K1=3.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=5056
HOCH2OH1+ + E1- -> H2CO + OH + H						RTYPE=9 K1=2.70E-07 K2=-0.5 K3=0.0 # gg08_rnum=5057
HOCH2OH1+ + E1- -> CH2OH + OH							RTYPE=9 K1=3.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=5058
HOCH2OH21+ + E1- -> CH2OH + OH + H						RTYPE=9 K1=3.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=5059
HOCH2OH21+ + E1- -> H2CO + H2O + H						RTYPE=9 K1=2.40E-07 K2=-0.5 K3=0.0 # gg08_rnum=5060
HOCH2OH21+ + E1- -> CH2OH + H2O							RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=5061
HOCH2OH21+ + E1- -> HOCH2OH + H							RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=5062
HOCH2CH2OH1+ + E1- -> H2CO + CH2OH + H					RTYPE=9 K1=2.70E-07 K2=-0.5 K3=0.0 # gg08_rnum=5063
HOCH2CH2OH1+ + E1- -> CH2OH + CH2OH						RTYPE=9 K1=3.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=5064
HOCH2CH2OH21+ + E1- -> CH2OH + CH2OH + H				RTYPE=9 K1=3.00E-08 K2=-0.5 K3=0.0 # gg08_rnum=5065
HOCH2CH2OH21+ + E1- -> H2CO + CH3OH + H					RTYPE=9 K1=2.40E-07 K2=-0.5 K3=0.0 # gg08_rnum=5066
HOCH2CH2OH21+ + E1- -> CH3OH + CH2OH					RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=5067
HOCH2CH2OH21+ + E1- -> HOCH2CH2OH + H					RTYPE=9 K1=1.50E-08 K2=-0.5 K3=0.0 # gg08_rnum=5068
C1+ + E1- -> C											RTYPE=10 K1=4.40E-12 K2=-6.10E-01 K3=0.0 # gg08_rnum=5069
CH31+ + E1- -> CH3										RTYPE=10 K1=1.10E-10 K2=-7.00E-01 K3=0.0 # gg08_rnum=5070
Cl1+ + E1- -> Cl										RTYPE=10 K1=1.13E-10 K2=-7.00E-01 K3=0.0 # gg08_rnum=5071
Fe1+ + E1- -> Fe										RTYPE=10 K1=3.70E-12 K2=-6.50E-01 K3=0.0 # gg08_rnum=5072
H1+ + E1- -> H											RTYPE=10 K1=3.50E-12 K2=-7.00E-01 K3=0.0 # gg08_rnum=5073
H21+ + E1- -> H2										RTYPE=10 K1=2.25E-07 K2=-4.00E-01 K3=0.0 # gg08_rnum=5074
H2CO1+ + E1- -> H2CO									RTYPE=10 K1=1.10E-10 K2=-7.00E-01 K3=0.0 # gg08_rnum=5075
H2CS1+ + E1- -> H2CS									RTYPE=10 K1=1.10E-10 K2=-7.00E-01 K3=0.0 # gg08_rnum=5076
H2S1+ + E1- -> H2S										RTYPE=10 K1=1.10E-10 K2=-7.00E-01 K3=0.0 # gg08_rnum=5077
He1+ + E1- -> He										RTYPE=10 K1=4.50E-12 K2=-6.70E-01 K3=0.0 # gg08_rnum=5078
Mg1+ + E1- -> Mg										RTYPE=10 K1=2.80E-12 K2=-8.60E-01 K3=0.0 # gg08_rnum=5079
N1+ + E1- -> N											RTYPE=10 K1=3.80E-12 K2=-6.20E-01 K3=0.0 # gg08_rnum=5080
Na1+ + E1- -> Na										RTYPE=10 K1=2.70E-12 K2=-6.90E-01 K3=0.0 # gg08_rnum=5081
O1+ + E1- -> O											RTYPE=10 K1=3.40E-12 K2=-6.30E-01 K3=0.0 # gg08_rnum=5082
S1+ + E1- -> S											RTYPE=10 K1=3.90E-12 K2=-6.30E-01 K3=0.0 # gg08_rnum=5083
Si1+ + E1- -> Si										RTYPE=10 K1=4.90E-12 K2=-6.00E-01 K3=0.0 # gg08_rnum=5084
C1+ + C1- -> C + C										RTYPE=11 K1=2.30E-07 K2=-0.5 K3=0.0 # gg08_rnum=5085
C1+ + H1- -> H + C										RTYPE=11 K1=2.30E-07 K2=-0.5 K3=0.0 # gg08_rnum=5086
C1+ + S1- -> C + S										RTYPE=11 K1=2.30E-07 K2=-0.5 K3=0.0 # gg08_rnum=5087
Fe1+ + C1- -> Fe + C									RTYPE=11 K1=2.30E-07 K2=-0.5 K3=0.0 # gg08_rnum=5088
Fe1+ + H1- -> Fe + H									RTYPE=11 K1=2.30E-07 K2=-0.5 K3=0.0 # gg08_rnum=5089
Fe1+ + S1- -> Fe + S									RTYPE=11 K1=2.30E-07 K2=-0.5 K3=0.0 # gg08_rnum=5090
H1+ + C1- -> H + C										RTYPE=11 K1=2.30E-07 K2=-0.5 K3=0.0 # gg08_rnum=5091
H1+ + H1- -> H + H										RTYPE=11 K1=2.30E-07 K2=-0.5 K3=0.0 # gg08_rnum=5092
H1+ + S1- -> H + S										RTYPE=11 K1=2.30E-07 K2=-0.5 K3=0.0 # gg08_rnum=5093
H21+ + H1- -> H + H2									RTYPE=11 K1=2.30E-07 K2=-0.5 K3=0.0 # gg08_rnum=5094
H31+ + H1- -> H2 + H2									RTYPE=11 K1=2.30E-07 K2=-0.5 K3=0.0 # gg08_rnum=5095
H3O1+ + H1- -> OH + H2 + H								RTYPE=11 K1=2.30E-07 K2=-0.5 K3=0.0 # gg08_rnum=5096
H3O1+ + H1- -> H2O + H2									RTYPE=11 K1=2.30E-07 K2=-0.5 K3=0.0 # gg08_rnum=5097
HCO1+ + H1- -> CO + H2									RTYPE=11 K1=2.30E-07 K2=-0.5 K3=0.0 # gg08_rnum=5098
He1+ + C1- -> He + C									RTYPE=11 K1=2.30E-07 K2=-0.5 K3=0.0 # gg08_rnum=5099
He1+ + H1- -> H + He									RTYPE=11 K1=2.30E-07 K2=-0.5 K3=0.0 # gg08_rnum=5100
He1+ + S1- -> He + S									RTYPE=11 K1=2.30E-07 K2=-0.5 K3=0.0 # gg08_rnum=5101
Mg1+ + C1- -> Mg + C									RTYPE=11 K1=2.30E-07 K2=-0.5 K3=0.0 # gg08_rnum=5102
Mg1+ + H1- -> Mg + H									RTYPE=11 K1=2.30E-07 K2=-0.5 K3=0.0 # gg08_rnum=5103
Mg1+ + S1- -> Mg + S									RTYPE=11 K1=2.30E-07 K2=-0.5 K3=0.0 # gg08_rnum=5104
N1+ + C1- -> N + C										RTYPE=11 K1=2.30E-07 K2=-0.5 K3=0.0 # gg08_rnum=5105
N1+ + H1- -> H + N										RTYPE=11 K1=2.30E-07 K2=-0.5 K3=0.0 # gg08_rnum=5106
N1+ + S1- -> N + S										RTYPE=11 K1=2.30E-07 K2=-0.5 K3=0.0 # gg08_rnum=5107
Na1+ + C1- -> Na + C									RTYPE=11 K1=2.30E-07 K2=-0.5 K3=0.0 # gg08_rnum=5108
Na1+ + H1- -> Na + H									RTYPE=11 K1=2.30E-07 K2=-0.5 K3=0.0 # gg08_rnum=5109
Na1+ + S1- -> Na + S									RTYPE=11 K1=2.30E-07 K2=-0.5 K3=0.0 # gg08_rnum=5110
NH41+ + H1- -> NH3 + H2									RTYPE=11 K1=2.30E-07 K2=-0.5 K3=0.0 # gg08_rnum=5111
O1+ + C1- -> O + C										RTYPE=11 K1=2.30E-07 K2=-0.5 K3=0.0 # gg08_rnum=5112
O1+ + H1- -> H + O										RTYPE=11 K1=2.30E-07 K2=-0.5 K3=0.0 # gg08_rnum=5113
O1+ + S1- -> O + S										RTYPE=11 K1=2.30E-07 K2=-0.5 K3=0.0 # gg08_rnum=5114
S1+ + C1- -> S + C										RTYPE=11 K1=2.30E-07 K2=-0.5 K3=0.0 # gg08_rnum=5115
S1+ + H1- -> S + H										RTYPE=11 K1=2.30E-07 K2=-0.5 K3=0.0 # gg08_rnum=5116
S1+ + S1- -> S + S										RTYPE=11 K1=2.30E-07 K2=-0.5 K3=0.0 # gg08_rnum=5117
Si1+ + C1- -> Si + C									RTYPE=11 K1=2.30E-07 K2=-0.5 K3=0.0 # gg08_rnum=5118
Si1+ + H1- -> Si + H									RTYPE=11 K1=2.30E-07 K2=-0.5 K3=0.0 # gg08_rnum=5119
Si1+ + S1- -> Si + S									RTYPE=11 K1=2.30E-07 K2=-0.5 K3=0.0 # gg08_rnum=5120
C + E1- -> C1-											RTYPE=12 K1=3.00E-15 K2=0.0 K3=0.0 # gg08_rnum=5121
H + E1- -> H1-											RTYPE=12 K1=3.00E-16 K2=1.00E+00 K3=0.0 # gg08_rnum=5122
O + E1- -> O1-											RTYPE=12 K1=1.50E-15 K2=0.0 K3=0.0 # gg08_rnum=5123
S + E1- -> S1-											RTYPE=12 K1=5.00E-15 K2=0.0 K3=0.0 # gg08_rnum=5124
C -> C1+ + E1-											RTYPE=13 K1=2.16E-10 K2=0.0 K3=2.61E+00 # gg08_rnum=5125
C1- -> C + E1-											RTYPE=13 K1=2.40E-07 K2=0.0 K3=9.00E-01 # gg08_rnum=5126
C2 -> C21+ + E1-										RTYPE=13 K1=1.00E-10 K2=0.0 K3=2.00E+00 # gg08_rnum=5127
C2H -> C2H1+ + E1-										RTYPE=13 K1=1.00E-11 K2=0.0 K3=2.00E+00 # gg08_rnum=5128
HC2H -> HC2H1+ + E1-									RTYPE=13 K1=2.07E-10 K2=0.0 K3=2.67E+00 # gg08_rnum=5129
H2C2H -> H2C2H1+ + E1-									RTYPE=13 K1=3.00E-10 K2=0.0 K3=2.30E+00 # gg08_rnum=5130
H2C2H2 -> H2C2H21+ + E1-								RTYPE=13 K1=1.18E-10 K2=0.0 K3=2.66E+00 # gg08_rnum=5131
CH3CH2 -> CH3CH21+ + E1-								RTYPE=13 K1=2.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=5132
CH3CH2CHO -> CH3CH2CHO1+ + E1-							RTYPE=13 K1=2.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=5133
CH3CH2OH -> CH3CH2OH1+ + E1-							RTYPE=13 K1=5.27E-10 K2=0.0 K3=2.35E+00 # gg08_rnum=5134
C2O -> C2O1+ + E1-										RTYPE=13 K1=2.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=5135
C3H3N -> C3H3N1+ + E1-									RTYPE=13 K1=2.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=5136
C3H4 -> C3H41+ + E1-									RTYPE=13 K1=9.88E-10 K2=0.0 K3=2.37E+00 # gg08_rnum=5137
HC4H -> HC4H1+ + E1-									RTYPE=13 K1=2.60E-10 K2=0.0 K3=2.28E+00 # gg08_rnum=5138
C4P -> C4P1+ + E1-										RTYPE=13 K1=2.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=5139
C6 -> C61+ + E1-										RTYPE=13 K1=2.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=5140
C6H2 -> C6H21+ + E1-									RTYPE=13 K1=2.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=5141
C7 -> C71+ + E1-										RTYPE=13 K1=2.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=5142
C7H -> C7H1+ + E1-										RTYPE=13 K1=2.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=5143
C7H2 -> C7H21+ + E1-									RTYPE=13 K1=2.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=5144
C8 -> C81+ + E1-										RTYPE=13 K1=2.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=5145
C8H -> C8H1+ + E1-										RTYPE=13 K1=2.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=5146
C8H2 -> C8H21+ + E1-									RTYPE=13 K1=2.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=5147
C9 -> C91+ + E1-										RTYPE=13 K1=2.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=5148
C9H -> C9H1+ + E1-										RTYPE=13 K1=2.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=5149
C9H2 -> C9H21+ + E1-									RTYPE=13 K1=2.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=5150
C9N -> C9N1+ + E1-										RTYPE=13 K1=2.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=5151
CCP -> CCP1+ + E1-										RTYPE=13 K1=2.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=5152
CH -> CH1+ + E1-										RTYPE=13 K1=2.90E-10 K2=0.0 K3=2.80E+00 # gg08_rnum=5153
CH2 -> CH21+ + E1-										RTYPE=13 K1=1.00E-09 K2=0.0 K3=2.30E+00 # gg08_rnum=5154
H2C2N -> H2C2N1+ + E1-									RTYPE=13 K1=2.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=5155
CH2CO -> CH2CO1+ + E1-									RTYPE=13 K1=3.44E-10 K2=0.0 K3=2.01E+00 # gg08_rnum=5156
CH2PH -> CH2PH1+ + E1-									RTYPE=13 K1=2.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=5157
CH3 -> CH31+ + E1-										RTYPE=13 K1=1.00E-10 K2=0.0 K3=2.10E+00 # gg08_rnum=5158
CH3C6H -> C7H41+ + E1-									RTYPE=13 K1=2.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=5159
CH3CHO -> CH3CHO1+ + E1-								RTYPE=13 K1=2.60E-10 K2=0.0 K3=2.28E+00 # gg08_rnum=5160
CH3CN -> CH3CN1+ + E1-									RTYPE=13 K1=5.29E-10 K2=0.0 K3=3.11E+00 # gg08_rnum=5161
CH3NH2 -> CH3NH21+ + E1-								RTYPE=13 K1=2.60E-10 K2=0.0 K3=2.28E+00 # gg08_rnum=5162
CH3OCH3 -> CH3OCH31+ + E1-								RTYPE=13 K1=2.60E-10 K2=0.0 K3=2.28E+00 # gg08_rnum=5163
CH3OH -> CH3OH1+ + E1-									RTYPE=13 K1=4.80E-10 K2=0.0 K3=2.57E+00 # gg08_rnum=5164
Cl -> Cl1+ + E1-										RTYPE=13 K1=3.30E-11 K2=0.0 K3=3.10E+00 # gg08_rnum=5165
CN1- -> CN + E1-										RTYPE=13 K1=2.40E-07 K2=0.0 K3=9.00E-01 # gg08_rnum=5166
CP -> CP1+ + E1-										RTYPE=13 K1=2.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=5167
Fe -> Fe1+ + E1-										RTYPE=13 K1=1.20E-10 K2=0.0 K3=1.50E+00 # gg08_rnum=5168
H1- -> H + E1-											RTYPE=13 K1=2.40E-07 K2=0.0 K3=9.00E-01 # gg08_rnum=5169
H2CN -> H2CN1+ + E1-									RTYPE=13 K1=2.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=5170
H2CO -> H2CO1+ + E1-									RTYPE=13 K1=8.00E-11 K2=0.0 K3=2.80E+00 # gg08_rnum=5171
H2CO -> HCO1+ + H + E1-									RTYPE=13 K1=1.40E-11 K2=0.0 K3=3.10E+00 # gg08_rnum=5172
H2O -> H2O1+ + E1-										RTYPE=13 K1=2.10E-11 K2=0.0 K3=3.10E+00 # gg08_rnum=5173
H2S2 -> H2S21+ + E1-									RTYPE=13 K1=2.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=5174
H2SiO -> H2SiO1+ + E1-									RTYPE=13 K1=2.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=5175
HC7N -> C7HN1+ + E1-									RTYPE=13 K1=2.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=5176
HC9N -> C9HN1+ + E1-									RTYPE=13 K1=2.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=5177
HCCP -> HCCP1+ + E1-									RTYPE=13 K1=2.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=5178
HCO -> HCO1+ + E1-										RTYPE=13 K1=2.46E-10 K2=0.0 K3=2.11E+00 # gg08_rnum=5179
HCOOCH3 -> HCOOCH31+ + E1-								RTYPE=13 K1=2.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=5180
HCOOH -> HCOOH1+ + E1-									RTYPE=13 K1=1.73E-10 K2=0.0 K3=2.59E+00 # gg08_rnum=5181
HCP -> HCP1+ + E1-										RTYPE=13 K1=2.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=5182
HCS -> HCS1+ + E1-										RTYPE=13 K1=2.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=5183
SiCH -> SiCH1+ + E1-									RTYPE=13 K1=2.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=5184
HNSi -> HNSi1+ + E1-									RTYPE=13 K1=2.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=5185
HPO -> HPO1+ + E1-										RTYPE=13 K1=2.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=5186
S2H -> S2H1+ + E1-										RTYPE=13 K1=2.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=5187
Mg -> Mg1+ + E1-										RTYPE=13 K1=4.50E-11 K2=0.0 K3=1.40E+00 # gg08_rnum=5188
Na -> Na1+ + E1-										RTYPE=13 K1=5.60E-12 K2=0.0 K3=2.00E+00 # gg08_rnum=5189
NH -> NH1+ + E1-										RTYPE=13 K1=1.00E-11 K2=0.0 K3=2.00E+00 # gg08_rnum=5190
NH2 -> NH21+ + E1-										RTYPE=13 K1=1.73E-10 K2=0.0 K3=2.59E+00 # gg08_rnum=5191
NH3 -> NH31+ + E1-										RTYPE=13 K1=1.24E-10 K2=0.0 K3=2.47E+00 # gg08_rnum=5192
NO -> NO1+ + E1-										RTYPE=13 K1=2.00E-10 K2=0.0 K3=2.00E+00 # gg08_rnum=5193
O1- -> O + E1-											RTYPE=13 K1=2.40E-07 K2=0.0 K3=9.00E-01 # gg08_rnum=5194
O2 -> O21+ + E1-										RTYPE=13 K1=6.20E-12 K2=0.0 K3=3.10E+00 # gg08_rnum=5195
HO2 -> O2H1+ + E1-										RTYPE=13 K1=2.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=5196
OCS -> OCS1+ + E1-										RTYPE=13 K1=2.37E-10 K2=0.0 K3=2.71E+00 # gg08_rnum=5197
OH -> OH1+ + E1-										RTYPE=13 K1=1.60E-12 K2=0.0 K3=3.10E+00 # gg08_rnum=5198
OH1- -> OH + E1-										RTYPE=13 K1=2.40E-07 K2=0.0 K3=9.00E-01 # gg08_rnum=5199
P -> P1+ + E1-											RTYPE=13 K1=1.00E-09 K2=0.0 K3=2.65E+00 # gg08_rnum=5200
PH -> PH1+ + E1-										RTYPE=13 K1=2.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=5201
PH2 -> PH21+ + E1-										RTYPE=13 K1=2.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=5202
PN -> PN1+ + E1-										RTYPE=13 K1=2.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=5203
PO -> PO1+ + E1-										RTYPE=13 K1=2.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=5204
S -> S1+ + E1-											RTYPE=13 K1=7.20E-10 K2=0.0 K3=2.40E+00 # gg08_rnum=5205
S1- -> S + E1-											RTYPE=13 K1=2.40E-07 K2=0.0 K3=9.00E-01 # gg08_rnum=5206
S2 -> S21+ + E1-										RTYPE=13 K1=2.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=5207
Si -> Si1+ + E1-										RTYPE=13 K1=1.20E-09 K2=0.0 K3=1.60E+00 # gg08_rnum=5208
SiC2 -> SiC21+ + E1-									RTYPE=13 K1=2.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=5209
SiC2H -> SiC2H1+ + E1-									RTYPE=13 K1=2.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=5210
SiC2H2 -> SiC2H21+ + E1-								RTYPE=13 K1=2.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=5211
cSiC3 -> SiC31+ + E1-									RTYPE=13 K1=2.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=5212
SiC3H -> SiC3H1+ + E1-									RTYPE=13 K1=2.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=5213
SiC4 -> SiC41+ + E1-									RTYPE=13 K1=2.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=5214
SiCH2 -> SiCH21+ + E1-									RTYPE=13 K1=2.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=5215
SiCH3 -> SiCH31+ + E1-									RTYPE=13 K1=2.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=5216
SiH2 -> SiH21+ + E1-									RTYPE=13 K1=2.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=5217
SiH3 -> SiH31+ + E1-									RTYPE=13 K1=2.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=5218
SiH4 -> SiH41+ + E1-									RTYPE=13 K1=2.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=5219
SiN -> SiN1+ + E1-										RTYPE=13 K1=2.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=5220
SiNC -> SiNC1+ + E1-									RTYPE=13 K1=2.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=5221
C2 -> C + C												RTYPE=13 K1=4.70E-11 K2=0.0 K3=2.60E+00 # gg08_rnum=5222
C21+ -> C1+ + C											RTYPE=13 K1=1.00E-11 K2=0.0 K3=1.70E+00 # gg08_rnum=5223
C2H -> C2 + H											RTYPE=13 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=5224
C2H1+ -> C21+ + H										RTYPE=13 K1=1.00E-11 K2=0.0 K3=2.00E+00 # gg08_rnum=5225
HC2H -> C2H + H											RTYPE=13 K1=1.81E-09 K2=0.0 K3=1.72E+00 # gg08_rnum=5226
H2C2H -> HC2H + H										RTYPE=13 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=5227
H2C2H2 -> HC2H + H2										RTYPE=13 K1=1.62E-09 K2=0.0 K3=1.61E+00 # gg08_rnum=5228
CH3CH2 -> H2C2H2 + H									RTYPE=13 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=5229
CH3CH2CHO -> CH3CH2 + HCO								RTYPE=13 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=5230
CH3CH2OH -> CH3CH2 + OH									RTYPE=13 K1=5.00E-10 K2=0.0 K3=1.70E+00 # gg08_rnum=5231
C2N -> CN + C											RTYPE=13 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=5232
C2N -> C2 + N											RTYPE=13 K1=1.00E-10 K2=0.0 K3=1.70E+00 # gg08_rnum=5233
C2O -> C2 + O											RTYPE=13 K1=5.00E-10 K2=0.0 K3=1.70E+00 # gg08_rnum=5234
C2O -> CO + C											RTYPE=13 K1=5.00E-10 K2=0.0 K3=1.70E+00 # gg08_rnum=5235
C2S -> C2 + S											RTYPE=13 K1=1.00E-10 K2=0.0 K3=2.00E+00 # gg08_rnum=5236
C3 -> C2 + C											RTYPE=13 K1=2.60E-10 K2=0.0 K3=2.28E+00 # gg08_rnum=5237
C3H -> C3 + H											RTYPE=13 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=5238
C3H2 -> C3H + H											RTYPE=13 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=5239
C3H2 -> C3 + H2											RTYPE=13 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=5240
C3H3 -> C3H + H2										RTYPE=13 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=5241
C3H3 -> C3H2 + H										RTYPE=13 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=5242
C3H3N -> H2C2H + CN										RTYPE=13 K1=1.00E-10 K2=0.0 K3=1.70E+00 # gg08_rnum=5243
C3H4 -> C3H3 + H										RTYPE=13 K1=1.84E-09 K2=0.0 K3=1.72E+00 # gg08_rnum=5244
C3H4 -> C3H2 + H2										RTYPE=13 K1=2.25E-10 K2=0.0 K3=1.69E+00 # gg08_rnum=5245
C3N -> C2 + CN											RTYPE=13 K1=5.00E-10 K2=0.0 K3=1.80E+00 # gg08_rnum=5246
C3O -> C2 + CO											RTYPE=13 K1=4.52E-09 K2=0.0 K3=1.58E+00 # gg08_rnum=5247
C3P -> CCP + C											RTYPE=13 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=5248
C3S -> C2 + CS											RTYPE=13 K1=1.00E-10 K2=0.0 K3=2.00E+00 # gg08_rnum=5249
C4 -> C3 + C											RTYPE=13 K1=2.00E-10 K2=0.0 K3=2.30E+00 # gg08_rnum=5250
C4 -> C2 + C2											RTYPE=13 K1=2.00E-10 K2=0.0 K3=2.30E+00 # gg08_rnum=5251
C4H -> C4 + H											RTYPE=13 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=5252
C4H -> C2H + C2											RTYPE=13 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=5253
HC4H -> C4H + H											RTYPE=13 K1=1.13E-09 K2=0.0 K3=1.64E+00 # gg08_rnum=5254
HC4H -> C2H + C2H										RTYPE=13 K1=1.13E-09 K2=0.0 K3=1.64E+00 # gg08_rnum=5255
C4H4 -> C3H4 + C										RTYPE=13 K1=1.13E-09 K2=0.0 K3=1.64E+00 # gg08_rnum=5256
C4N -> C3 + CN											RTYPE=13 K1=5.00E-10 K2=0.0 K3=1.70E+00 # gg08_rnum=5257
C4P -> C3P + C											RTYPE=13 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=5258
C4S -> C3 + CS											RTYPE=13 K1=1.00E-10 K2=0.0 K3=2.00E+00 # gg08_rnum=5259
C5 -> C3 + C2											RTYPE=13 K1=1.00E-11 K2=0.0 K3=1.70E+00 # gg08_rnum=5260
C5H -> C5 + H											RTYPE=13 K1=1.00E-11 K2=0.0 K3=1.70E+00 # gg08_rnum=5261
C5H -> C2H + C3											RTYPE=13 K1=1.00E-11 K2=0.0 K3=1.70E+00 # gg08_rnum=5262
C5H -> C3H + C2											RTYPE=13 K1=1.00E-11 K2=0.0 K3=1.70E+00 # gg08_rnum=5263
C5H2 -> C5H + H											RTYPE=13 K1=1.00E-11 K2=0.0 K3=1.70E+00 # gg08_rnum=5264
C5H2 -> C3H + C2H										RTYPE=13 K1=1.00E-11 K2=0.0 K3=1.70E+00 # gg08_rnum=5265
C5N -> C4 + CN											RTYPE=13 K1=5.00E-10 K2=0.0 K3=1.70E+00 # gg08_rnum=5266
C6 -> C5 + C											RTYPE=13 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=5267
C6H -> C2H + C4											RTYPE=13 K1=5.00E-12 K2=0.0 K3=1.70E+00 # gg08_rnum=5268
C6H -> C3H + C3											RTYPE=13 K1=5.00E-12 K2=0.0 K3=1.70E+00 # gg08_rnum=5269
C6H2 -> C6H + H											RTYPE=13 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=5270
C7 -> C6 + C											RTYPE=13 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=5271
C7H -> C7 + H											RTYPE=13 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=5272
C7H2 -> C7H + H											RTYPE=13 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=5273
C8 -> C7 + C											RTYPE=13 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=5274
C8H -> C8 + H											RTYPE=13 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=5275
C8H2 -> C8H + H											RTYPE=13 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=5276
C9 -> C8 + C											RTYPE=13 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=5277
C9H -> C9 + H											RTYPE=13 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=5278
C9H2 -> C9H + H											RTYPE=13 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=5279
C9N -> C8 + CN											RTYPE=13 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=5280
ClC -> Cl + C											RTYPE=13 K1=1.00E-10 K2=0.0 K3=2.00E+00 # gg08_rnum=5281
CCP -> CP + C											RTYPE=13 K1=5.00E-10 K2=0.0 K3=1.70E+00 # gg08_rnum=5282
CCP -> C2 + P											RTYPE=13 K1=5.00E-10 K2=0.0 K3=1.70E+00 # gg08_rnum=5283
CH -> C + H												RTYPE=13 K1=1.40E-10 K2=0.0 K3=1.50E+00 # gg08_rnum=5284
CH1+ -> C1+ + H											RTYPE=13 K1=4.60E-12 K2=0.0 K3=3.00E+00 # gg08_rnum=5285
CH2 -> CH + H											RTYPE=13 K1=5.00E-11 K2=0.0 K3=1.70E+00 # gg08_rnum=5286
CH21+ -> CH1+ + H										RTYPE=13 K1=1.70E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=5287
H2C2N -> CH2 + CN										RTYPE=13 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=5288
CH2CO -> CH2 + CO										RTYPE=13 K1=9.04E-10 K2=0.0 K3=1.58E+00 # gg08_rnum=5289
CH2NH -> HCN + H2										RTYPE=13 K1=1.70E-09 K2=0.0 K3=1.63E+00 # gg08_rnum=5290
CH2OH -> CH2 + OH										RTYPE=13 K1=5.00E-10 K2=0.0 K3=1.70E+00 # gg08_rnum=5291
CH2PH -> HCP + H2										RTYPE=13 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=5292
CH3 -> CH2 + H											RTYPE=13 K1=3.00E-11 K2=0.0 K3=1.70E+00 # gg08_rnum=5293
CH3 -> CH + H2											RTYPE=13 K1=3.00E-11 K2=0.0 K3=1.70E+00 # gg08_rnum=5294
CH31+ -> CH21+ + H										RTYPE=13 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=5295
CH31+ -> CH1+ + H2										RTYPE=13 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=5296
CH3C3N -> C3N + CH3										RTYPE=13 K1=2.00E-11 K2=0.0 K3=1.70E+00 # gg08_rnum=5297
CH3C4H -> C4H + CH3										RTYPE=13 K1=2.00E-11 K2=0.0 K3=1.70E+00 # gg08_rnum=5298
CH3C5N -> CH3 + C5N										RTYPE=13 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=5299
CH3C6H -> CH3 + C6H										RTYPE=13 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=5300
CH3C7N -> CH3 + C7N										RTYPE=13 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=5301
CH3CHO -> CH3 + HCO										RTYPE=13 K1=3.43E-10 K2=0.0 K3=1.52E+00 # gg08_rnum=5302
CH3CHO -> CH4 + CO										RTYPE=13 K1=3.43E-10 K2=0.0 K3=1.52E+00 # gg08_rnum=5303
CH3CN -> CH3 + CN										RTYPE=13 K1=1.56E-09 K2=0.0 K3=1.95E+00 # gg08_rnum=5304
CH3CO -> CH3 + CO										RTYPE=13 K1=9.00E-10 K2=0.0 K3=1.60E+00 # gg08_rnum=5305
CH3NH -> NH + CH3										RTYPE=13 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=5306
CH3NH2 -> CH2NH + H + H									RTYPE=13 K1=6.63E-11 K2=0.0 K3=1.51E+00 # gg08_rnum=5307
CH3NH2 -> CN + H2 + H2 + H								RTYPE=13 K1=9.42E-11 K2=0.0 K3=1.76E+00 # gg08_rnum=5308
CH3NH2 -> HCN + H2 + H + H								RTYPE=13 K1=3.50E-10 K2=0.0 K3=1.73E+00 # gg08_rnum=5309
CH3NH2 -> CH3 + NH2										RTYPE=13 K1=1.55E-10 K2=0.0 K3=1.74E+00 # gg08_rnum=5310
CH3O -> CH3 + O											RTYPE=13 K1=5.00E-10 K2=0.0 K3=1.70E+00 # gg08_rnum=5311
CH3OCH3 -> CH3O + CH3									RTYPE=13 K1=9.00E-10 K2=0.0 K3=1.60E+00 # gg08_rnum=5312
CH3OH -> CH2OH + H										RTYPE=13 K1=5.00E-10 K2=0.0 K3=1.60E+00 # gg08_rnum=5313
CH3OH -> CH3O + H										RTYPE=13 K1=2.00E-10 K2=0.0 K3=1.60E+00 # gg08_rnum=5314
CH3OH -> CH3 + OH										RTYPE=13 K1=5.00E-10 K2=0.0 K3=1.70E+00 # gg08_rnum=5315
CH4 -> CH3 + H											RTYPE=13 K1=1.60E-10 K2=0.0 K3=2.20E+00 # gg08_rnum=5316
CH4 -> CH2 + H2											RTYPE=13 K1=4.80E-10 K2=0.0 K3=2.20E+00 # gg08_rnum=5317
CH4 -> CH + H + H2										RTYPE=13 K1=1.60E-10 K2=0.0 K3=2.20E+00 # gg08_rnum=5318
ClO -> Cl + O											RTYPE=13 K1=1.00E-10 K2=0.0 K3=2.00E+00 # gg08_rnum=5319
CN -> C + N												RTYPE=13 K1=1.00E-09 K2=0.0 K3=2.80E+00 # gg08_rnum=5320
CO -> C + O												RTYPE=13 K1=3.10E-11 K2=0.0 K3=2.54E+00 # gg08_rnum=5321
CO2 -> CO + O											RTYPE=13 K1=3.13E-10 K2=0.0 K3=2.03E+00 # gg08_rnum=5322
COCHO -> HCO + CO										RTYPE=13 K1=9.00E-10 K2=0.0 K3=1.60E+00 # gg08_rnum=5323
COOH -> CO + OH											RTYPE=13 K1=1.00E-09 K2=0.0 K3=1.80E+00 # gg08_rnum=5324
CP -> C + P												RTYPE=13 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=5325
CS -> C + S												RTYPE=13 K1=9.70E-10 K2=0.0 K3=2.00E+00 # gg08_rnum=5326
H2 -> H + H												RTYPE=13 K1=0.0 K2=0.0 K3=0.0 # gg08_rnum=5327
H21+ -> H1+ + H											RTYPE=13 K1=2.60E-10 K2=0.0 K3=1.80E+00 # gg08_rnum=5328
H2CN -> HCN + H											RTYPE=13 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=5329
H2CO -> CO + H2											RTYPE=13 K1=4.40E-10 K2=0.0 K3=1.60E+00 # gg08_rnum=5330
H2CO -> CO + H + H										RTYPE=13 K1=4.40E-10 K2=0.0 K3=1.60E+00 # gg08_rnum=5331
H2CS -> CS + H2											RTYPE=13 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=5332
H2O -> OH + H											RTYPE=13 K1=3.28E-10 K2=0.0 K3=1.63E+00 # gg08_rnum=5333
H2O2 -> OH + OH											RTYPE=13 K1=8.30E-10 K2=0.0 K3=1.82E+00 # gg08_rnum=5334
H2S -> HS + H											RTYPE=13 K1=3.20E-10 K2=0.0 K3=1.70E+00 # gg08_rnum=5335
H2S2 -> HS + HS											RTYPE=13 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=5336
H2SiO -> SiO + H2										RTYPE=13 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=5337
H31+ -> H21+ + H										RTYPE=13 K1=7.90E-09 K2=0.0 K3=2.30E+00 # gg08_rnum=5338
H31+ -> H1+ + H2										RTYPE=13 K1=2.00E-08 K2=0.0 K3=1.80E+00 # gg08_rnum=5339
HC3N -> C2H + CN										RTYPE=13 K1=9.54E-10 K2=0.0 K3=1.83E+00 # gg08_rnum=5340
HC5N -> C4H + CN										RTYPE=13 K1=5.00E-10 K2=0.0 K3=1.83E+00 # gg08_rnum=5341
HC5N -> H + C5N											RTYPE=13 K1=5.00E-10 K2=0.0 K3=1.83E+00 # gg08_rnum=5342
HC7N -> C6H + CN										RTYPE=13 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=5343
HC9N -> C8H + CN										RTYPE=13 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=5344
HCCP -> CCP + H											RTYPE=13 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=5345
ClH -> Cl + H											RTYPE=13 K1=1.10E-10 K2=0.0 K3=1.80E+00 # gg08_rnum=5346
HCN -> CN + H											RTYPE=13 K1=5.48E-10 K2=0.0 K3=2.00E+00 # gg08_rnum=5347
HCO -> H + CO											RTYPE=13 K1=5.87E-10 K2=0.0 K3=5.30E-01 # gg08_rnum=5348
HCOOCH3 -> HCO + CH3O									RTYPE=13 K1=5.00E-10 K2=0.0 K3=1.70E+00 # gg08_rnum=5349
HCOOH -> HCO + OH										RTYPE=13 K1=2.75E-10 K2=0.0 K3=1.80E+00 # gg08_rnum=5350
HCP -> CP + H											RTYPE=13 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=5351
SiCH -> CH + Si											RTYPE=13 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=5352
CH3COCHO -> CH3CO + HCO									RTYPE=13 K1=9.00E-10 K2=0.0 K3=1.60E+00 # gg08_rnum=5353
CH3COCH3 -> CH3CO + CH3									RTYPE=13 K1=9.00E-10 K2=0.0 K3=1.60E+00 # gg08_rnum=5354
CH3CONH -> CH3CO + NH									RTYPE=13 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=5355
CH3CONH -> CH3 + HNCO									RTYPE=13 K1=9.00E-10 K2=0.0 K3=1.60E+00 # gg08_rnum=5356
CH3COOH -> CH3CO + OH									RTYPE=13 K1=5.00E-10 K2=0.0 K3=1.70E+00 # gg08_rnum=5357
CH3COOH -> CH3 + COOH									RTYPE=13 K1=9.00E-10 K2=0.0 K3=1.60E+00 # gg08_rnum=5358
CH3CONH2 -> CH3CO + NH2									RTYPE=13 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=5359
CH3CONH2 -> CH3 + NH2CO									RTYPE=13 K1=1.00E-09 K2=0.0 K3=1.60E+00 # gg08_rnum=5360
CH3COOCH3 -> CH3 + CH3OCO								RTYPE=13 K1=9.00E-10 K2=0.0 K3=1.60E+00 # gg08_rnum=5361
CH3COOCH3 -> CH3CO + CH3O								RTYPE=13 K1=5.00E-10 K2=0.0 K3=1.70E+00 # gg08_rnum=5362
CH3COCH2OH -> CH3CO + CH2OH								RTYPE=13 K1=9.00E-10 K2=0.0 K3=1.60E+00 # gg08_rnum=5363
CH3COCH2OH -> CH3 + HOCH2CO								RTYPE=13 K1=9.00E-10 K2=0.0 K3=1.60E+00 # gg08_rnum=5364
HNC -> CN + H											RTYPE=13 K1=5.48E-10 K2=0.0 K3=2.00E+00 # gg08_rnum=5365
HNCHO -> NH + HCO										RTYPE=13 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=5366
HNCOCHO -> HNCO + HCO									RTYPE=13 K1=9.00E-10 K2=0.0 K3=1.60E+00 # gg08_rnum=5367
HNCOCHO -> NH + COCHO									RTYPE=13 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=5368
HNCONH -> NH + HNCO										RTYPE=13 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=5369
HNCOOH -> COOH + NH										RTYPE=13 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=5370
HNCOOH -> OH + HNCO										RTYPE=13 K1=5.00E-10 K2=0.0 K3=1.70E+00 # gg08_rnum=5371
HNO -> NO + H											RTYPE=13 K1=1.70E-10 K2=0.0 K3=5.30E-01 # gg08_rnum=5372
HNOH -> NH + OH											RTYPE=13 K1=1.00E-09 K2=0.0 K3=1.80E+00 # gg08_rnum=5373
HNSi -> SiN + H											RTYPE=13 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=5374
CH3ONH -> NH + CH3O										RTYPE=13 K1=1.00E-09 K2=0.0 K3=1.80E+00 # gg08_rnum=5375
HNCH2OH -> NH + CH2OH									RTYPE=13 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=5376
HOCOOH -> OH + COOH										RTYPE=13 K1=5.00E-10 K2=0.0 K3=1.70E+00 # gg08_rnum=5377
HPO -> PO + H											RTYPE=13 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=5378
HS -> H + S												RTYPE=13 K1=1.00E-11 K2=0.0 K3=2.00E+00 # gg08_rnum=5379
S2H -> HS + S											RTYPE=13 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=5380
HCOCOCHO -> HCO + COCHO									RTYPE=13 K1=9.00E-10 K2=0.0 K3=1.60E+00 # gg08_rnum=5381
MgH -> Mg + H											RTYPE=13 K1=5.00E-10 K2=0.0 K3=1.45E+00 # gg08_rnum=5382
N2 -> N + N												RTYPE=13 K1=5.00E-12 K2=0.0 K3=3.00E+00 # gg08_rnum=5383
N2O -> N2 + O											RTYPE=13 K1=1.40E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=5384
NaH -> Na + H											RTYPE=13 K1=7.30E-09 K2=0.0 K3=1.13E+00 # gg08_rnum=5385
NaOH -> Na + OH											RTYPE=13 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=5386
NH -> H + N												RTYPE=13 K1=4.00E-10 K2=0.0 K3=1.50E+00 # gg08_rnum=5387
NH2 -> NH + H											RTYPE=13 K1=2.11E-10 K2=0.0 K3=1.52E+00 # gg08_rnum=5388
NH2CN -> NH2 + CN										RTYPE=13 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=5389
NH3 -> NH2 + H											RTYPE=13 K1=4.94E-10 K2=0.0 K3=1.65E+00 # gg08_rnum=5390
NH3 -> NH + H2											RTYPE=13 K1=1.30E-10 K2=0.0 K3=1.91E+00 # gg08_rnum=5391
NO -> N + O												RTYPE=13 K1=3.00E-10 K2=0.0 K3=2.00E+00 # gg08_rnum=5392
NO2 -> NO + O											RTYPE=13 K1=1.29E-09 K2=0.0 K3=2.00E+00 # gg08_rnum=5393
NS -> N + S												RTYPE=13 K1=1.00E-11 K2=0.0 K3=2.00E+00 # gg08_rnum=5394
O2 -> O + O												RTYPE=13 K1=3.30E-10 K2=0.0 K3=1.40E+00 # gg08_rnum=5395
HO2 -> O2 + H											RTYPE=13 K1=5.00E-10 K2=0.0 K3=1.70E+00 # gg08_rnum=5396
HO2 -> OH + O											RTYPE=13 K1=5.00E-10 K2=0.0 K3=1.70E+00 # gg08_rnum=5397
OCN -> O + CN											RTYPE=13 K1=1.00E-11 K2=0.0 K3=2.00E+00 # gg08_rnum=5398
OCS -> CO + S											RTYPE=13 K1=2.28E-09 K2=0.0 K3=1.60E+00 # gg08_rnum=5399
OH -> O + H												RTYPE=13 K1=1.68E-10 K2=0.0 K3=1.66E+00 # gg08_rnum=5400
OH1+ -> H1+ + O											RTYPE=13 K1=7.20E-12 K2=0.0 K3=1.80E+00 # gg08_rnum=5401
CHOCHO -> HCO + HCO										RTYPE=13 K1=9.00E-10 K2=0.0 K3=1.60E+00 # gg08_rnum=5402
HCOCOOH -> COCHO + OH									RTYPE=13 K1=5.00E-10 K2=0.0 K3=1.70E+00 # gg08_rnum=5403
HCOCOOH -> HCO + COOH									RTYPE=13 K1=9.00E-10 K2=0.0 K3=1.60E+00 # gg08_rnum=5404
PH -> P + H												RTYPE=13 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=5405
PH2 -> PH + H											RTYPE=13 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=5406
PN -> N + P												RTYPE=13 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=5407
PO -> O + P												RTYPE=13 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=5408
NH2CO -> NH2 + CO										RTYPE=13 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=5409
NH2COCHO -> NH2CO + HCO									RTYPE=13 K1=9.00E-10 K2=0.0 K3=1.60E+00 # gg08_rnum=5410
NH2COCHO -> NH2 + COCHO									RTYPE=13 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=5411
NH2CONH -> NH2 + HNCO									RTYPE=13 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=5412
NH2CONH -> NH2CO + NH									RTYPE=13 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=5413
NH2COOH -> NH2 + COOH									RTYPE=13 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=5414
NH2COOH -> NH2CO + OH									RTYPE=13 K1=5.00E-10 K2=0.0 K3=1.70E+00 # gg08_rnum=5415
NH2CONH2 -> NH2 + NH2CO									RTYPE=13 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=5416
NH2NH -> NH + NH2										RTYPE=13 K1=1.00E-11 K2=0.0 K3=3.00E+00 # gg08_rnum=5417
NH2NH2 -> NH2 + NH2										RTYPE=13 K1=1.00E-11 K2=0.0 K3=3.00E+00 # gg08_rnum=5418
NH2OCH3 -> NH2 + CH3O									RTYPE=13 K1=1.00E-09 K2=0.0 K3=1.80E+00 # gg08_rnum=5419
NH2CH2OH -> NH2 + CH2OH									RTYPE=13 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=5420
S2 -> S + S												RTYPE=13 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=5421
SiC -> Si + C											RTYPE=13 K1=1.00E-10 K2=0.0 K3=2.30E+00 # gg08_rnum=5422
SiC2 -> SiC + C											RTYPE=13 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=5423
SiC2H -> SiC2 + H										RTYPE=13 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=5424
SiC2H2 -> SiC2 + H2										RTYPE=13 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=5425
cSiC3 -> SiC2 + C										RTYPE=13 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=5426
SiC3H -> cSiC3 + H										RTYPE=13 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=5427
SiC4 -> SiC2 + C2										RTYPE=13 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=5428
SiCH2 -> SiC + H2										RTYPE=13 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=5429
SiCH3 -> SiCH2 + H										RTYPE=13 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=5430
SiH -> Si + H											RTYPE=13 K1=1.00E-10 K2=0.0 K3=2.30E+00 # gg08_rnum=5431
SiH1+ -> Si1+ + H										RTYPE=13 K1=2.20E-09 K2=0.0 K3=2.00E+00 # gg08_rnum=5432
SiH2 -> SiH + H											RTYPE=13 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=5433
SiH3 -> SiH2 + H										RTYPE=13 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=5434
SiH4 -> SiH2 + H2										RTYPE=13 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=5435
SiN -> Si + N											RTYPE=13 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=5436
SiNC -> CN + Si											RTYPE=13 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=5437
SiO -> Si + O											RTYPE=13 K1=1.00E-10 K2=0.0 K3=2.30E+00 # gg08_rnum=5438
SiO2 -> SiO + O											RTYPE=13 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=5439
SiS -> Si + S											RTYPE=13 K1=1.00E-10 K2=0.0 K3=2.30E+00 # gg08_rnum=5440
SO -> O + S												RTYPE=13 K1=3.30E-10 K2=0.0 K3=1.40E+00 # gg08_rnum=5441
SO2 -> SO + O											RTYPE=13 K1=1.05E-09 K2=0.0 K3=1.74E+00 # gg08_rnum=5442
CH3OCO -> CH3O + CO										RTYPE=13 K1=5.00E-10 K2=0.0 K3=1.70E+00 # gg08_rnum=5443
CH3OCOCHO -> CH3OCO + HCO								RTYPE=13 K1=9.00E-10 K2=0.0 K3=1.60E+00 # gg08_rnum=5444
CH3OCOCHO -> CH3O + COCHO								RTYPE=13 K1=5.00E-10 K2=0.0 K3=1.70E+00 # gg08_rnum=5445
CH3OCONH -> CH3O + HNCO									RTYPE=13 K1=5.00E-10 K2=0.0 K3=1.70E+00 # gg08_rnum=5446
CH3OCONH -> CH3OCO + NH									RTYPE=13 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=5447
CH3OCOOH -> CH3OCO + OH									RTYPE=13 K1=5.00E-10 K2=0.0 K3=1.70E+00 # gg08_rnum=5448
CH3OCOOH -> CH3O + COOH									RTYPE=13 K1=5.00E-10 K2=0.0 K3=1.70E+00 # gg08_rnum=5449
CH3OCONH2 -> NH2 + CH3OCO								RTYPE=13 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=5450
CH3OCONH2 -> NH2CO + CH3O								RTYPE=13 K1=5.00E-10 K2=0.0 K3=1.70E+00 # gg08_rnum=5451
CH3OCOOCH3 -> CH3O + CH3OCO								RTYPE=13 K1=5.00E-10 K2=0.0 K3=1.70E+00 # gg08_rnum=5452
CH3OCOCH2OH -> CH3OCO + CH2OH							RTYPE=13 K1=9.00E-10 K2=0.0 K3=1.60E+00 # gg08_rnum=5453
CH3OCOCH2OH -> CH3O + HOCH2CO							RTYPE=13 K1=5.00E-10 K2=0.0 K3=1.70E+00 # gg08_rnum=5454
CH3OOH -> OH + CH3O										RTYPE=13 K1=1.00E-09 K2=0.0 K3=1.80E+00 # gg08_rnum=5455
CH3OOCH3 -> CH3O + CH3O									RTYPE=13 K1=1.00E-09 K2=0.0 K3=1.80E+00 # gg08_rnum=5456
CH3OCH2OH -> CH3O + CH2OH								RTYPE=13 K1=5.00E-10 K2=0.0 K3=1.70E+00 # gg08_rnum=5457
CH2OHCHO -> CH2OH + HCO									RTYPE=13 K1=9.00E-10 K2=0.0 K3=1.60E+00 # gg08_rnum=5458
HOCH2CO -> CH2OH + CO									RTYPE=13 K1=9.00E-10 K2=0.0 K3=1.60E+00 # gg08_rnum=5459
HOCH2COCHO -> HOCH2CO + HCO								RTYPE=13 K1=9.00E-10 K2=0.0 K3=1.60E+00 # gg08_rnum=5460
HOCH2COCHO -> CH2OH + COCHO								RTYPE=13 K1=9.00E-10 K2=0.0 K3=1.60E+00 # gg08_rnum=5461
HNCOCH2OH -> HOCH2CO + NH								RTYPE=13 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=5462
HNCOCH2OH -> CH2OH + HNCO								RTYPE=13 K1=9.00E-10 K2=0.0 K3=1.60E+00 # gg08_rnum=5463
CH2OHCOOH -> CH2OH + COOH								RTYPE=13 K1=9.00E-10 K2=0.0 K3=1.60E+00 # gg08_rnum=5464
CH2OHCOOH -> HOCH2CO + OH								RTYPE=13 K1=5.00E-10 K2=0.0 K3=1.70E+00 # gg08_rnum=5465
NH2COCH2OH -> NH2CO + CH2OH								RTYPE=13 K1=9.00E-10 K2=0.0 K3=1.60E+00 # gg08_rnum=5466
NH2COCH2OH -> NH2 + HOCH2CO								RTYPE=13 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=5467
HOCH2COCH2OH -> CH2OH + HOCH2CO							RTYPE=13 K1=9.00E-10 K2=0.0 K3=1.60E+00 # gg08_rnum=5468
HOCH2OH -> OH + CH2OH									RTYPE=13 K1=5.00E-10 K2=0.0 K3=1.70E+00 # gg08_rnum=5469
HOCH2CH2OH -> CH2OH + CH2OH								RTYPE=13 K1=9.00E-10 K2=0.0 K3=1.60E+00 # gg08_rnum=5470
gC + gC -> gC2											RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5471, gg08_EA_ref='AR'
gC + gC -> C2											RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5472
gC + gC2 -> gC3											RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5473, gg08_EA_ref='AR'
gC + gC2 -> C3											RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5474
gC + gC2H -> gC3H										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5475, gg08_EA_ref='AR(exactly)'
gC + gC2H -> C3H										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5476
gC + gH2C2H -> gC3H3									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5477, gg08_EA_ref='AR'
gC + gH2C2H -> C3H3										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5478
gC + gC2N -> gC3N										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5479, gg08_EA_ref='EH'
gC + gC2N -> C3N										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5480
gC + gC2O -> gC3O										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5481, gg08_EA_ref='AR'
gC + gC2O -> C3O										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5482
gC + gC2S -> gC3S										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5483, gg08_EA_ref='EH'
gC + gC2S -> C3S										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5484
gC + gC3 -> gC4											RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5485, gg08_EA_ref='EH'
gC + gC3 -> C4											RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5486
gC + gC3H -> gC4H										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5487, gg08_EA_ref='EH'
gC + gC3H -> C4H										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5488
gC + gC4 -> gC5											RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5489, gg08_EA_ref='EH'
gC + gC4 -> C5											RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5490
gC + gC4H -> gC5H										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5491, gg08_EA_ref='hasegawa'
gC + gC4H -> C5H										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5492
gC + gC5 -> gC6											RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5493, gg08_EA_ref='EH'
gC + gC5 -> C6											RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5494
gC + gC5H -> gC6H										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5495, gg08_EA_ref='hasegawa'
gC + gC5H -> C6H										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5496
gC + gC6 -> gC7											RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5497, gg08_EA_ref='EH'
gC + gC6 -> C7											RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5498
gC + gC6H -> gC7H										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5499, gg08_EA_ref='hasegawa'
gC + gC6H -> C7H										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5500
gC + gC7 -> gC8											RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5501, gg08_EA_ref='EH'
gC + gC7 -> C8											RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5502
gC + gC7H -> gC8H										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5503, gg08_EA_ref='hase'
gC + gC7H -> C8H										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5504
gC + gC8 -> gC9											RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5505, gg08_EA_ref='EH'
gC + gC8 -> C9											RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5506
gC + gC8H -> gC9H										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5507, gg08_EA_ref='hase'
gC + gC8H -> C9H										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5508
gC + gC9 -> gC10										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5509
gC + gC9 -> C10											RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5510
gC + gCH -> gC2H										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5511, gg08_EA_ref='AR'
gC + gCH -> C2H											RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5512
gC + gCH2 -> gHC2H										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5513, gg08_EA_ref='AR  (H2CC)'
gC + gCH2 -> HC2H										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5514
gC + gCH3 -> gH2C2H										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5515, gg08_EA_ref='AR'
gC + gCH3 -> H2C2H										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5516
gC + gCN -> gC2N										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5517, gg08_EA_ref='AR'
gC + gCN -> C2N											RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5518
gC + gHS -> gCS + gH									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5519, gg08_EA_ref='M84'
gC + gHS -> CS + H										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5520
gC + gN -> gCN											RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5521, gg08_EA_ref='AR'
gC + gN -> CN											RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5522
gC + gNH -> gHNC										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5523, gg08_EA_ref='AR'
gC + gNH -> HNC											RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5524
gC + gNH2 -> gHNC + gH									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5525, gg08_EA_ref='EH'
gC + gNH2 -> HNC + H									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5526
gC + gNO -> gCN + gO									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5527, gg08_EA_ref='From our gas network'
gC + gNO -> CN + O										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5528
gC + gNO -> gOCN										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5529, gg08_EA_ref='AR (CNO)'
gC + gNO -> OCN											RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5530
gC + gNS -> gCN + gS									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5531, gg08_EA_ref='M84'
gC + gNS -> CN + S										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5532
gC + gO -> gCO											RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5533, gg08_EA_ref='AR'
gC + gO -> CO											RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5534
gC + gO2 -> gCO + gO									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5535, gg08_EA_ref='AR'
gC + gO2 -> CO + O										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5536
gC + gOCN -> gCO + gCN									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5537, gg08_EA_ref='M84'
gC + gOCN -> CO + CN									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5538
gC + gOH -> gCO + gH									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5539, gg08_EA_ref='EH (Mitchell?)'
gC + gOH -> CO + H										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5540
gC + gOH -> gHOC										RTYPE=14 K1=0.0 K2=0.0 K3=0.0 # gg08_rnum=5541, gg08_EA_ref='AR'
gC + gOH -> HOC											RTYPE=14 K1=0.0 K2=0.0 K3=0.0 # gg08_rnum=5542
gC + gS -> gCS											RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5543, gg08_EA_ref='EH'
gC + gS -> CS											RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5544
gC + gSO -> gCO + gS									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5545, gg08_EA_ref='M84'
gC + gSO -> CO + S										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5546
gCH + gC2 -> gC3H										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5547, gg08_EA_ref='AR'
gCH + gC2 -> C3H										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5548
gCH + gC2H -> gC3H2										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5549, gg08_EA_ref='AR'
gCH + gC2H -> C3H2										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5550
gCH + gH2C2H -> gC3H4									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5551, gg08_EA_ref='AR (isomer)'
gCH + gH2C2H -> C3H4									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5552
gCH + gC3 -> gC4H										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5553, gg08_EA_ref='EH'
gCH + gC3 -> C4H										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5554
gCH + gC3H -> gHC4H										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5555, gg08_EA_ref='hasegawa'
gCH + gC3H -> HC4H										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5556
gCH + gC4 -> gC5H										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5557, gg08_EA_ref='EH'
gCH + gC4 -> C5H										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5558
gCH + gC4H -> gC5H2										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5559, gg08_EA_ref='hasegawa'
gCH + gC4H -> C5H2										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5560
gCH + gC5 -> gC6H										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5561, gg08_EA_ref='EH'
gCH + gC5 -> C6H										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5562
gCH + gC5H -> gC6H2										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5563, gg08_EA_ref='hasegawa'
gCH + gC5H -> C6H2										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5564
gCH + gC6 -> gC7H										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5565, gg08_EA_ref='EH'
gCH + gC6 -> C7H										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5566
gCH + gC6H -> gC7H2										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5567, gg08_EA_ref='hasegawa'
gCH + gC6H -> C7H2										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5568
gCH + gC7 -> gC8H										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5569, gg08_EA_ref='EH'
gCH + gC7 -> C8H										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5570
gCH + gC7H -> gC8H2										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5571, gg08_EA_ref='hasegawa'
gCH + gC7H -> C8H2										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5572
gCH + gC8 -> gC9H										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5573, gg08_EA_ref='EH'
gCH + gC8 -> C9H										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5574
gCH + gC8H -> gC9H2										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5575, gg08_EA_ref='hasegawa'
gCH + gC8H -> C9H2										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5576
gCH + gCH -> gHC2H										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5577, gg08_EA_ref='AR'
gCH + gCH -> HC2H										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5578
gCH + gCH2 -> gH2C2H									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5579, gg08_EA_ref='AR'
gCH + gCH2 -> H2C2H										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5580
gCH + gCH3 -> gH2C2H2									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5581, gg08_EA_ref='AR (isomer)'
gCH + gCH3 -> H2C2H2									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5582
gCH + gCN -> gHC2N										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5583, gg08_EA_ref='AR'
gCH + gCN -> HC2N										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5584
gCH + gHNO -> gNO + gCH2								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5585, gg08_EA_ref='M84(West.Ea=0-+2500K)'
gCH + gHNO -> NO + CH2									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5586
gCH + gNH -> gHCN + gH									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5587, gg08_EA_ref='Hase'
gCH + gNH -> HCN + H									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5588
gCH + gNH -> gHNC + gH									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5589, gg08_EA_ref='Hase'
gCH + gNH -> HNC + H									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5590
gCH + gNH -> gHCNH										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5591, gg08_EA_ref='AR'
gCH + gNH -> HCNH										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5592
gCH + gNH2 -> gCH2NH									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5593, gg08_EA_ref='hase -   DPR was CH2NH'
gCH + gNH2 -> CH2NH										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5594
gCH + gNO -> gHCN + gO									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5595, gg08_EA_ref='M84'
gCH + gNO -> HCN + O									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5596
gCH + gO2 -> gHCO + gO									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5597, gg08_EA_ref='AR'
gCH + gO2 -> HCO + O									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5598
gCH + gOH -> gHCOH										RTYPE=14 K1=0.0 K2=0.0 K3=0.0 # gg08_rnum=5599, gg08_EA_ref='AR'
gCH + gOH -> HCOH										RTYPE=14 K1=0.0 K2=0.0 K3=0.0 # gg08_rnum=5600
gCH2 + gCH2 -> gH2C2H2									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5601, gg08_EA_ref='AR'
gCH2 + gCH2 -> H2C2H2									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5602
gCH2 + gCH3 -> gCH3CH2									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5603, gg08_EA_ref='AR'
gCH2 + gCH3 -> CH3CH2									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5604
gCH2 + gCN -> gH2C2N									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5605, gg08_EA_ref='AR (CH2CN)'
gCH2 + gCN -> H2C2N										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5606
gCH2 + gHNO -> gCH3 + gNO								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5607, gg08_EA_ref='Westley Ea=0.0-+2500K'
gCH2 + gHNO -> CH3 + NO									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5608
gCH2 + gNH2 -> gCH2NH2									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5609, gg08_EA_ref='AR (implemented)'
gCH2 + gNH2 -> CH2NH2									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5610
gCH2 + gO2 -> gH2CO + gO								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5611, gg08_EA_ref='AR'
gCH2 + gO2 -> H2CO + O									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5612
gCH2OH + gCH2OH -> gHOCH2CH2OH							RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5613
gCH2OH + gCH2OH -> HOCH2CH2OH							RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5614
gCH2OH + gCH3CHO -> gCH3OH + gCH3CO						RTYPE=14 K1=1.00E+00 K2=0.0 K3=2.85E+03 # gg08_rnum=5615, gg08_EA_ref=null
gCH2OH + gCH3CHO -> CH3OH + CH3CO						RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5616
gCH2OH + gCH3CHO -> gCH3CH2OH + gHCO					RTYPE=14 K1=1.00E+00 K2=0.0 K3=2.40E+03 # gg08_rnum=5617, gg08_EA_ref=null
gCH2OH + gCH3CHO -> gCH2OHCHO + gCH3					RTYPE=14 K1=1.00E+00 K2=0.0 K3=2.40E+03 # gg08_rnum=5618, gg08_EA_ref=null
gCH2OH + gCH3CHO -> CH3CH2OH + HCO						RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5619
gCH2OH + gCH3CHO -> CH2OHCHO + CH3						RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5620
gCH2OH + gCH3CO -> CH3COCH2OH							RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5621
gCH2OH + gCH3CO -> gCH3COCH2OH							RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5622
gCH2OH + gCO -> gHOCH2CO								RTYPE=14 K1=1.00E+00 K2=0.0 K3=1.50E+03 # gg08_rnum=5623, gg08_EA_ref=null
gCH2OH + gCO -> HOCH2CO									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5624
gCH2OH + gCOCHO -> gHOCH2COCHO							RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5625
gCH2OH + gCOCHO -> HOCH2COCHO							RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5626
gCH2OH + gCOOH -> CH2OHCOOH								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5627
gCH2OH + gCOOH -> gCH2OHCOOH							RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5628
gCH2OH + gH2CO -> CH3OH + HCO							RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5629
gCH2OH + gH2CO -> gCH3OH + gHCO							RTYPE=14 K1=1.00E+00 K2=0.0 K3=2.95E+03 # gg08_rnum=5630, gg08_EA_ref=null
gCH2OH + gHCO -> gCH2OHCHO								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5631
gCH2OH + gHCO -> CH2OHCHO								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5632
gCH2OH + gHNCO -> gHNCOCH2OH							RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5633
gCH2OH + gHNCO -> HNCOCH2OH								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5634
gCH2OH + gNH2CHO -> gCH3OH + gNH2CO						RTYPE=14 K1=1.00E+00 K2=0.0 K3=2.85E+03 # gg08_rnum=5635, gg08_EA_ref=null
gCH2OH + gNH2CHO -> CH3OH + NH2CO						RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5636
gCH2OH + gCHOCHO -> gCH2OHCHO + gHCO					RTYPE=14 K1=1.00E+00 K2=0.0 K3=2.40E+03 # gg08_rnum=5637, gg08_EA_ref=null
gCH2OH + gCHOCHO -> CH2OHCHO + HCO						RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5638
gCH2OH + gNH2CO -> gNH2COCH2OH							RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5639
gCH2OH + gNH2CO -> NH2COCH2OH							RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5640
gCH2OH + gCH3OCO -> CH3OCOCH2OH							RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5641
gCH2OH + gCH3OCO -> gCH3OCOCH2OH						RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5642
gCH2OH + gCH2OHCHO -> gCH3OH + gHOCH2CO					RTYPE=14 K1=1.00E+00 K2=0.0 K3=2.85E+03 # gg08_rnum=5643, gg08_EA_ref=null
gCH2OH + gCH2OHCHO -> gHOCH2CH2OH + gHCO				RTYPE=14 K1=1.00E+00 K2=0.0 K3=2.40E+03 # gg08_rnum=5644, gg08_EA_ref=null
gCH2OH + gCH2OHCHO -> HOCH2CH2OH + HCO					RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5645
gCH2OH + gCH2OHCHO -> CH3OH + HOCH2CO					RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5646
gCH2OH + gHOCH2CO -> gHOCH2COCH2OH						RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5647
gCH2OH + gHOCH2CO -> HOCH2COCH2OH						RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5648
gCH3 + gC3N -> gCH3C3N									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5649, gg08_EA_ref='hasegawa'
gCH3 + gC3N -> CH3C3N									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5650
gCH3 + gC5N -> gCH3C5N									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5651, gg08_EA_ref='hasegawa'
gCH3 + gC5N -> CH3C5N									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5652
gCH3 + gC7N -> gCH3C7N									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5653, gg08_EA_ref='hasegawa'
gCH3 + gC7N -> CH3C7N									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5654
gCH3 + gCH2OH -> gCH3CH2OH								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5655, gg08_EA_ref='AR'
gCH3 + gCH2OH -> CH3CH2OH								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5656
gCH3 + gCH3 -> gCH3CH3									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5657, gg08_EA_ref='AR'
gCH3 + gCH3 -> CH3CH3									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5658
gCH3 + gCH3CHO -> gCH4 + gCH3CO							RTYPE=14 K1=1.00E+00 K2=0.0 K3=3.02E+03 # gg08_rnum=5659, gg08_EA_ref=null
gCH3 + gCH3CHO -> gCH3CH3 + gHCO						RTYPE=14 K1=1.00E+00 K2=0.0 K3=2.40E+03 # gg08_rnum=5660, gg08_EA_ref=null
gCH3 + gCH3CHO -> CH4 + CH3CO							RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5661
gCH3 + gCH3CHO -> CH3CH3 + HCO							RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5662
gCH3 + gCH3CO -> CH3COCH3								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5663
gCH3 + gCH3CO -> gCH3COCH3								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5664
gCH3 + gCH3O -> CH3OCH3									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5665
gCH3 + gCH3O -> gCH3OCH3								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5666
gCH3 + gCN -> CH3CN										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5667
gCH3 + gCN -> gCH3CN									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5668, gg08_EA_ref='AR (CH3CN)'
gCH3 + gCO -> gCH3CO									RTYPE=14 K1=1.00E+00 K2=0.0 K3=1.50E+03 # gg08_rnum=5669, gg08_EA_ref=null
gCH3 + gCO -> CH3CO										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5670
gCH3 + gCOCHO -> CH3COCHO								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5671
gCH3 + gCOCHO -> gCH3COCHO								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5672
gCH3 + gCOOH -> gCH3COOH								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5673
gCH3 + gCOOH -> CH3COOH									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5674
gCH3 + gH2CO -> gCH4 + gHCO								RTYPE=14 K1=1.00E+00 K2=0.0 K3=4.45E+03 # gg08_rnum=5675, gg08_EA_ref=null
gCH3 + gH2CO -> CH4 + HCO								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5676
gCH3 + gHCO -> CH3CHO									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5677
gCH3 + gHCO -> gCH3CHO									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5678, gg08_EA_ref='Makes sense(hasegawa)'
gCH3 + gHCOOCH3 -> gCH4 + gCH3OCO						RTYPE=14 K1=1.00E+00 K2=0.0 K3=5.18E+03 # gg08_rnum=5679, gg08_EA_ref=null
gCH3 + gHCOOCH3 -> CH4 + CH3OCO							RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5680
gCH3 + gHCOOH -> gCH4 + gCOOH							RTYPE=14 K1=1.00E+00 K2=0.0 K3=2.85E+03 # gg08_rnum=5681, gg08_EA_ref=null
gCH3 + gHCOOH -> CH4 + COOH								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5682
gCH3 + gHNCO -> CH3CONH									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5683
gCH3 + gHNCO -> gCH3CONH								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5684
gCH3 + gHNO -> CH4 + NO									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5685
gCH3 + gHNO -> gCH4 + gNO								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5686, gg08_EA_ref='Westley Ea=0. -+2500K'
gCH3 + gNH2CHO -> CH4 + NH2CO							RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5687
gCH3 + gNH2CHO -> gCH4 + gNH2CO							RTYPE=14 K1=1.00E+00 K2=0.0 K3=3.56E+03 # gg08_rnum=5688, gg08_EA_ref=null
gCH3 + gCHOCHO -> CH3CHO + HCO							RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5689
gCH3 + gCHOCHO -> gCH3CHO + gHCO						RTYPE=14 K1=1.00E+00 K2=0.0 K3=2.40E+03 # gg08_rnum=5690, gg08_EA_ref=null
gCH3 + gNH2CO -> gCH3CONH2								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5691
gCH3 + gNH2CO -> CH3CONH2								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5692
gCH3 + gCH3OCO -> gCH3COOCH3							RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5693
gCH3 + gCH3OCO -> CH3COOCH3								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5694
gCH3 + gCH2OHCHO -> CH3CH2OH + HCO						RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5695
gCH3 + gCH2OHCHO -> CH4 + HOCH2CO						RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5696
gCH3 + gCH2OHCHO -> gCH4 + gHOCH2CO						RTYPE=14 K1=1.00E+00 K2=0.0 K3=2.85E+03 # gg08_rnum=5697, gg08_EA_ref=null
gCH3 + gCH2OHCHO -> gCH3CH2OH + gHCO					RTYPE=14 K1=1.00E+00 K2=0.0 K3=2.40E+03 # gg08_rnum=5698, gg08_EA_ref=null
gCH3 + gHOCH2CO -> CH3COCH2OH							RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5699
gCH3 + gHOCH2CO -> gCH3COCH2OH							RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5700
gCH3O + gCH2OH -> gCH3OCH2OH							RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5701
gCH3O + gCH2OH -> CH3OCH2OH								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5702
gCH3O + gCH3CHO -> gHCOOCH3 + gCH3						RTYPE=14 K1=1.00E+00 K2=0.0 K3=2.40E+03 # gg08_rnum=5703, gg08_EA_ref=null
gCH3O + gCH3CHO -> CH3OH + CH3CO						RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5704
gCH3O + gCH3CHO -> gCH3OH + gCH3CO						RTYPE=14 K1=1.00E+00 K2=0.0 K3=2.85E+03 # gg08_rnum=5705, gg08_EA_ref=null
gCH3O + gCH3CHO -> HCOOCH3 + CH3						RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5706
gCH3O + gCH3CO -> gCH3COOCH3							RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5707
gCH3O + gCH3CO -> CH3COOCH3								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5708
gCH3O + gCH3O -> gCH3OOCH3								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5709
gCH3O + gCH3O -> CH3OOCH3								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5710
gCH3O + gCO -> gCH3OCO									RTYPE=14 K1=1.00E+00 K2=0.0 K3=1.50E+03 # gg08_rnum=5711, gg08_EA_ref=null
gCH3O + gCO -> CH3OCO									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5712
gCH3O + gCOCHO -> CH3OCOCHO								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5713
gCH3O + gCOCHO -> gCH3OCOCHO							RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5714
gCH3O + gCOOH -> gCH3OCOOH								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5715
gCH3O + gCOOH -> CH3OCOOH								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5716
gCH3O + gH2CO -> gHCOOCH3 + gH							RTYPE=14 K1=1.00E+00 K2=0.0 K3=2.85E+03 # gg08_rnum=5717, gg08_EA_ref=null
gCH3O + gH2CO -> gCH3OH + gHCO							RTYPE=14 K1=1.00E+00 K2=0.0 K3=1.50E+03 # gg08_rnum=5718, gg08_EA_ref=null
gCH3O + gH2CO -> CH3OH + HCO							RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5719
gCH3O + gH2CO -> HCOOCH3 + H							RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5720
gCH3O + gHCOOCH3 -> CH3OH + CH3OCO						RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5721
gCH3O + gHCOOCH3 -> gCH3OH + gCH3OCO					RTYPE=14 K1=1.00E+00 K2=0.0 K3=4.12E+03 # gg08_rnum=5722, gg08_EA_ref=null
gCH3O + gHCOOH -> gCH3OH + gCOOH						RTYPE=14 K1=1.00E+00 K2=0.0 K3=2.85E+03 # gg08_rnum=5723, gg08_EA_ref=null
gCH3O + gHCOOH -> CH3OH + COOH							RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5724
gCH3O + gHNCO -> gCH3OCONH								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5725
gCH3O + gHNCO -> CH3OCONH								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5726
gCH3O + gNH2CHO -> gCH3OH + gNH2CO						RTYPE=14 K1=1.00E+00 K2=0.0 K3=2.85E+03 # gg08_rnum=5727, gg08_EA_ref=null
gCH3O + gNH2CHO -> CH3OH + NH2CO						RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5728
gCH3O + gNH2CHO -> gHCOOCH3 + gNH2						RTYPE=14 K1=1.00E+00 K2=0.0 K3=2.10E+03 # gg08_rnum=5729, gg08_EA_ref=null
gCH3O + gNH2CHO -> HCOOCH3 + NH2						RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5730
gCH3O + gCHOCHO -> HCOOCH3 + HCO						RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5731
gCH3O + gCHOCHO -> gHCOOCH3 + gHCO						RTYPE=14 K1=1.00E+00 K2=0.0 K3=2.40E+03 # gg08_rnum=5732, gg08_EA_ref=null
gCH3O + gNH2CO -> CH3OCONH2								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5733
gCH3O + gNH2CO -> gCH3OCONH2							RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5734
gCH3O + gCH3OCO -> gCH3OCOOCH3							RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5735
gCH3O + gCH3OCO -> CH3OCOOCH3							RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5736
gCH3O + gCH2OHCHO -> gHCOOCH3 + gCH2OH					RTYPE=14 K1=1.00E+00 K2=0.0 K3=2.40E+03 # gg08_rnum=5737, gg08_EA_ref=null
gCH3O + gCH2OHCHO -> CH3OH + HOCH2CO					RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5738
gCH3O + gCH2OHCHO -> gCH3OH + gHOCH2CO					RTYPE=14 K1=1.00E+00 K2=0.0 K3=2.85E+03 # gg08_rnum=5739, gg08_EA_ref=null
gCH3O + gCH2OHCHO -> HCOOCH3 + CH2OH					RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5740
gCH3O + gHOCH2CO -> CH3OCOCH2OH							RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5741
gCH3O + gHOCH2CO -> gCH3OCOCH2OH						RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5742
gCH4 + gC2H -> gHC2H + gCH3								RTYPE=14 K1=1.00E+00 K2=0.0 K3=2.50E+02 # gg08_rnum=5743, gg08_EA_ref='Mitchell 84'
gCH4 + gC2H -> HC2H + CH3								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5744
gH + gC -> gCH											RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5745, gg08_EA_ref='AR'
gH + gC -> CH											RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5746
gH + gC2 -> gC2H										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5747, gg08_EA_ref='AR'
gH + gC2 -> C2H											RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5748
gH + gC2H -> gHC2H										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5749, gg08_EA_ref='AR'
gH + gC2H -> HC2H										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5750
gH + gHC2H -> gH2C2H									RTYPE=14 K1=1.00E+00 K2=0.0 K3=1.21E+03 # gg08_rnum=5751, gg08_EA_ref='C=_C triple bond HASE'
gH + gHC2H -> H2C2H										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5752
gH + gH2C2H -> gH2C2H2									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5753, gg08_EA_ref='AR'
gH + gH2C2H -> H2C2H2									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5754
gH + gH2C2H2 -> gCH3CH2									RTYPE=14 K1=1.00E+00 K2=0.0 K3=7.50E+02 # gg08_rnum=5755, gg08_EA_ref='C=C  double bond HASE'
gH + gH2C2H2 -> CH3CH2									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5756
gH + gCH3CH2 -> gCH3CH3									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5757, gg08_EA_ref='AR'
gH + gCH3CH2 -> CH3CH3									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5758
gH + gCH3CH3 -> gCH3CH2 + gH2							RTYPE=14 K1=1.00E+00 K2=0.0 K3=4.89E+03 # gg08_rnum=5759, gg08_EA_ref='TranFaradSco 65,2116'
gH + gCH3CH3 -> CH3CH2 + H2								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5760
gH + gC2N -> gHC2N										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5761, gg08_EA_ref='AR'
gH + gC2N -> HC2N										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5762
gH + gC2O -> gHC2O										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5763, gg08_EA_ref='AR'
gH + gC2O -> HC2O										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5764
gH + gC3 -> gC3H										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5765, gg08_EA_ref='EH'
gH + gC3 -> C3H											RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5766
gH + gC3H -> gC3H2										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5767, gg08_EA_ref='AR'
gH + gC3H -> C3H2										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5768
gH + gC3H2 -> gC3H3										RTYPE=14 K1=1.00E+00 K2=0.0 K3=1.21E+03 # gg08_rnum=5769, gg08_EA_ref='C=_C triple bond'
gH + gC3H2 -> C3H3										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5770
gH + gC3H3 -> gC3H4										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5771, gg08_EA_ref='AR'
gH + gC3H3 -> C3H4										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5772
gH + gC3H3N -> gH4C3N									RTYPE=14 K1=1.00E+00 K2=0.0 K3=7.50E+02 # gg08_rnum=5773, gg08_EA_ref='C=C + H   hase'
gH + gC3H3N -> H4C3N									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5774
gH + gC3N -> gHC3N										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5775, gg08_EA_ref='EH'
gH + gC3N -> HC3N										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5776
gH + gC3O -> gHC3O										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5777, gg08_EA_ref='AR'
gH + gC3O -> HC3O										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5778
gH + gC4 -> gC4H										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5779, gg08_EA_ref='EH'
gH + gC4 -> C4H											RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5780
gH + gC4H -> gHC4H										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5781, gg08_EA_ref='AR'
gH + gC4H -> HC4H										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5782
gH + gHC4H -> gC4H3										RTYPE=14 K1=1.00E+00 K2=0.0 K3=1.21E+03 # gg08_rnum=5783, gg08_EA_ref='C=_C bond HASE'
gH + gHC4H -> C4H3										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5784
gH + gC4H3 -> gC4H4										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5785, gg08_EA_ref='AR'
gH + gC4H3 -> C4H4										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5786
gH + gC5 -> gC5H										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5787, gg08_EA_ref='EH'
gH + gC5 -> C5H											RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5788
gH + gC5H -> gC5H2										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5789, gg08_EA_ref='EH'
gH + gC5H -> C5H2										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5790
gH + gC5H2 -> gC5H3										RTYPE=14 K1=1.00E+00 K2=0.0 K3=1.21E+03 # gg08_rnum=5791, gg08_EA_ref='C=_C bond HASE'
gH + gC5H2 -> C5H3										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5792
gH + gC5H3 -> gC5H4										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5793, gg08_EA_ref='HASE'
gH + gC5H3 -> C5H4										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5794
gH + gC5N -> gHC5N										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5795, gg08_EA_ref='EH'
gH + gC5N -> HC5N										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5796
gH + gC6 -> gC6H										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5797, gg08_EA_ref='EH'
gH + gC6 -> C6H											RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5798
gH + gC6H -> gC6H2										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5799, gg08_EA_ref='EH'
gH + gC6H -> C6H2										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5800
gH + gC6H2 -> gC6H3										RTYPE=14 K1=1.00E+00 K2=0.0 K3=1.21E+03 # gg08_rnum=5801, gg08_EA_ref='C=_C bond HASE'
gH + gC6H2 -> C6H3										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5802
gH + gC6H3 -> gC6H4										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5803, gg08_EA_ref='HASE'
gH + gC6H3 -> C6H4										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5804
gH + gC7 -> gC7H										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5805, gg08_EA_ref='EH'
gH + gC7 -> C7H											RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5806
gH + gC7H -> gC7H2										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5807, gg08_EA_ref='EH'
gH + gC7H -> C7H2										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5808
gH + gC7H2 -> gC7H3										RTYPE=14 K1=1.00E+00 K2=0.0 K3=1.21E+03 # gg08_rnum=5809, gg08_EA_ref='C=_C bond HASE'
gH + gC7H2 -> C7H3										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5810
gH + gC7H3 -> gC7H4										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5811, gg08_EA_ref='HASE'
gH + gC7H3 -> C7H4										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5812
gH + gC7N -> gHC7N										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5813, gg08_EA_ref='EH'
gH + gC7N -> HC7N										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5814
gH + gC8 -> gC8H										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5815, gg08_EA_ref='EH'
gH + gC8 -> C8H											RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5816
gH + gC8H -> gC8H2										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5817, gg08_EA_ref='EH'
gH + gC8H -> C8H2										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5818
gH + gC8H2 -> gC8H3										RTYPE=14 K1=1.00E+00 K2=0.0 K3=1.21E+03 # gg08_rnum=5819, gg08_EA_ref='C=_C bond HASE'
gH + gC8H2 -> C8H3										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5820
gH + gC8H3 -> gC8H4										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5821, gg08_EA_ref='HASE'
gH + gC8H3 -> C8H4										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5822
gH + gC9 -> gC9H										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5823, gg08_EA_ref='EH'
gH + gC9 -> C9H											RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5824
gH + gC9H -> gC9H2										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5825, gg08_EA_ref='EH'
gH + gC9H -> C9H2										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5826
gH + gC9H2 -> gC9H3										RTYPE=14 K1=1.00E+00 K2=0.0 K3=1.21E+03 # gg08_rnum=5827, gg08_EA_ref='C=_C bond HASE'
gH + gC9H2 -> C9H3										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5828
gH + gC9H3 -> gC9H4										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5829, gg08_EA_ref='HASE'
gH + gC9H3 -> C9H4										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5830
gH + gC9N -> gHC9N										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5831, gg08_EA_ref='EH'
gH + gC9N -> HC9N										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5832
gH + gCH -> gCH2										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5833, gg08_EA_ref='AR'
gH + gCH -> CH2											RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5834
gH + gCH2 -> gCH3										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5835, gg08_EA_ref='AR'
gH + gCH2 -> CH3										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5836
gH + gH2C2N -> gCH3CN									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5837, gg08_EA_ref='AR'
gH + gH2C2N -> CH3CN									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5838
gH + gCH2NH -> gCH2NH2									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5839, gg08_EA_ref='AR'
gH + gCH2NH -> CH2NH2									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5840
gH + gCH2NH -> gCH3NH									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5841, gg08_EA_ref='AR'
gH + gCH2NH -> CH3NH									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5842
gH + gCH2NH2 -> gCH3NH2									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5843, gg08_EA_ref='AR -DPR CH3NH'
gH + gCH2NH2 -> CH3NH2									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5844
gH + gCH2OH -> gCH3OH									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5845, gg08_EA_ref='AR'
gH + gCH2OH -> CH3OH									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5846
gH + gCH3 -> gCH4										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5847, gg08_EA_ref='AR'
gH + gCH3 -> CH4										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5848
gH + gCH3CHO -> HCO + CH4								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5849
gH + gCH3CHO -> H2CO + CH3								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5850
gH + gCH3CHO -> H2 + CH3CO								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5851
gH + gCH3CHO -> gH2 + gCH3CO							RTYPE=14 K1=1.00E+00 K2=0.0 K3=2.12E+03 # gg08_rnum=5852, gg08_EA_ref=null
gH + gCH3CHO -> gHCO + gCH4								RTYPE=14 K1=1.00E+00 K2=0.0 K3=2.40E+03 # gg08_rnum=5853, gg08_EA_ref=null
gH + gCH3CHO -> gH2CO + gCH3							RTYPE=14 K1=1.00E+00 K2=0.0 K3=2.40E+03 # gg08_rnum=5854, gg08_EA_ref=null
gH + gCH3CO -> gCH3CHO									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5855
gH + gCH3CO -> CH3CHO									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5856
gH + gCH3NH -> gCH3NH2									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5857, gg08_EA_ref='AR  -DPR CH3NH'
gH + gCH3NH -> CH3NH2									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5858
gH + gCH3O -> gCH3OH									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5859
gH + gCH3O -> CH3OH										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5860
gH + gCH4 -> gCH3 + gH2									RTYPE=14 K1=1.00E+00 K2=0.0 K3=5.94E+03 # gg08_rnum=5861, gg08_EA_ref='JChPhy 50,5076 (1969)'
gH + gCH4 -> CH3 + H2									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5862
gH + gHCNH -> gCH2NH									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5863, gg08_EA_ref='AR'
gH + gHCNH -> CH2NH										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5864
gH + gHCOH -> gCH2OH									RTYPE=14 K1=0.0 K2=0.0 K3=0.0 # gg08_rnum=5865, gg08_EA_ref='AR'
gH + gHCOH -> CH2OH										RTYPE=14 K1=0.0 K2=0.0 K3=0.0 # gg08_rnum=5866
gH + gCN -> gHCN										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5867, gg08_EA_ref='AR'
gH + gCN -> HCN											RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5868
gH + gCO -> gHCO										RTYPE=14 K1=1.00E+00 K2=0.0 K3=2.50E+03 # gg08_rnum=5869, gg08_EA_ref='WOON priv commun -DPR'
gH + gCO -> HOC											RTYPE=14 K1=0.0 K2=0.0 K3=0.0 # gg08_rnum=5870
gH + gCO -> HCO											RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5871
gH + gCO -> gHOC										RTYPE=14 K1=0.0 K2=0.0 K3=1.00E+03 # gg08_rnum=5872, gg08_EA_ref='EH      eliminated'
gH + gCOCHO -> gCHOCHO									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5873
gH + gCOCHO -> CHOCHO									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5874
gH + gCOOH -> HCOOH										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5875
gH + gCOOH -> gHCOOH									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5876
gH + gCS -> gHCS										RTYPE=14 K1=1.00E+00 K2=0.0 K3=1.00E+03 # gg08_rnum=5877, gg08_EA_ref='EH'
gH + gCS -> HCS											RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5878
gH + gFe -> gFeH										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5879, gg08_EA_ref='EH'
gH + gFe -> FeH											RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5880
gH + gH -> gH2											RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5881, gg08_EA_ref=null
gH + gH -> H2											RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5882
gH + gHC3NH -> gC3H3N									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5883, gg08_EA_ref='HASE'
gH + gHC3NH -> C3H3N									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5884
gH + gH2C5N -> gH3C5N									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5885, gg08_EA_ref='HASE'
gH + gH2C5N -> H3C5N									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5886
gH + gH2C7N -> gH3C7N									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5887, gg08_EA_ref='HASE'
gH + gH2C7N -> H3C7N									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5888
gH + gH2C9N -> gH3C9N									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5889, gg08_EA_ref='HASE'
gH + gH2C9N -> H3C9N									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5890
gH + gH2CN -> gCH2NH									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5891, gg08_EA_ref='AR -DPR CH2N'
gH + gH2CN -> CH2NH										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5892
gH + gH2CO -> gHCO + gH2								RTYPE=14 K1=0.0 K2=0.0 K3=0.0 # gg08_rnum=5893
gH + gH2CO -> HCO + H2									RTYPE=14 K1=0.0 K2=0.0 K3=0.0 # gg08_rnum=5894
gH + gH2CO -> gCH2OH									RTYPE=14 K1=1.00E+00 K2=0.0 K3=2.50E+03 # gg08_rnum=5895, gg08_EA_ref='WOON priv commun -DPR'
gH + gH2CO -> CH2OH										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5896
gH + gH2CO -> gCH3O										RTYPE=14 K1=1.00E+00 K2=0.0 K3=2.50E+03 # gg08_rnum=5897, gg08_EA_ref='WOON priv commun -DPR'
gH + gH2CO -> CH3O										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5898
gH + gH2O2 -> gHO2 + gH2								RTYPE=14 K1=1.00E+00 K2=0.0 K3=1.90E+03 # gg08_rnum=5899, gg08_EA_ref='M84'
gH + gH2O2 -> HO2 + H2									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5900
gH + gH2O2 -> gH2O + gOH								RTYPE=14 K1=1.00E+00 K2=0.0 K3=1.40E+03 # gg08_rnum=5901, gg08_EA_ref='TH'
gH + gH2O2 -> H2O + OH									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5902
gH + gH2S -> gH2 + gHS									RTYPE=14 K1=1.00E+00 K2=0.0 K3=8.60E+02 # gg08_rnum=5903, gg08_EA_ref='TH 8.60E+02 1.35E+03'
gH + gH2S -> H2 + HS									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5904
gH + gH4C3N -> gH5C3N									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5905, gg08_EA_ref='C2H5-CN   hase'
gH + gH4C3N -> H5C3N									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5906
gH + gHC2O -> gCH2CO									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5907, gg08_EA_ref='AR'
gH + gHC2O -> CH2CO										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5908
gH + gHC3N -> gHC3NH									RTYPE=14 K1=1.00E+00 K2=0.0 K3=1.21E+03 # gg08_rnum=5909, gg08_EA_ref='C=_C bond HASE'
gH + gHC3N -> HC3NH										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5910
gH + gHC3O -> gcH2C3O									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5911, gg08_EA_ref='AR'
gH + gHC3O -> cH2C3O									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5912
gH + gHC5N -> gH2C5N									RTYPE=14 K1=1.00E+00 K2=0.0 K3=1.21E+03 # gg08_rnum=5913, gg08_EA_ref='C=_C bond HASE'
gH + gHC5N -> H2C5N										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5914
gH + gHC7N -> gH2C7N									RTYPE=14 K1=1.00E+00 K2=0.0 K3=1.21E+03 # gg08_rnum=5915, gg08_EA_ref='C=_C bond HASE'
gH + gHC7N -> H2C7N										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5916
gH + gHC9N -> gH2C9N									RTYPE=14 K1=1.00E+00 K2=0.0 K3=1.21E+03 # gg08_rnum=5917, gg08_EA_ref='C=_C bond HASE'
gH + gHC9N -> H2C9N										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5918
gH + gHC2N -> gH2C2N									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5919, gg08_EA_ref='AR'
gH + gHC2N -> H2C2N										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5920
gH + gHCO -> gH2CO										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5921, gg08_EA_ref='AR  87kcal/mol exoth'
gH + gHCO -> H2CO										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5922
gH + gHCOOCH3 -> H2 + CH3OCO							RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5923
gH + gHCOOCH3 -> CH3OH + HCO							RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5924
gH + gHCOOCH3 -> gCH3OH + gHCO							RTYPE=14 K1=1.00E+00 K2=0.0 K3=2.45E+03 # gg08_rnum=5925, gg08_EA_ref=null
gH + gHCOOCH3 -> gH2 + gCH3OCO							RTYPE=14 K1=1.00E+00 K2=0.0 K3=3.97E+03 # gg08_rnum=5926, gg08_EA_ref=null
gH + gHCOOH -> HCO + H2O								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5927
gH + gHCOOH -> H2 + COOH								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5928
gH + gHCOOH -> gH2 + gCOOH								RTYPE=14 K1=1.00E+00 K2=0.0 K3=2.85E+03 # gg08_rnum=5929, gg08_EA_ref=null
gH + gHCOOH -> gHCO + gH2O								RTYPE=14 K1=1.00E+00 K2=0.0 K3=2.45E+03 # gg08_rnum=5930, gg08_EA_ref=null
gH + gHCS -> gH2CS										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5931, gg08_EA_ref='guess. Exoth OK'
gH + gHCS -> H2CS										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5932
gH + gCH3CONH -> CH3CONH2								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5933
gH + gCH3CONH -> gCH3CONH2								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5934
gH + gHNCHO -> gNH2CHO									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5935
gH + gHNCHO -> NH2CHO									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5936
gH + gHNCO -> HNCHO										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5937
gH + gHNCO -> gHNCHO									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5938
gH + gHNCO -> gNH2CO									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5939
gH + gHNCO -> NH2CO										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5940
gH + gHNCOCHO -> NH2COCHO								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5941
gH + gHNCOCHO -> gNH2COCHO								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5942
gH + gHNCONH -> gNH2CONH								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5943
gH + gHNCONH -> NH2CONH									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5944
gH + gHNCOOH -> gNH2COOH								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5945
gH + gHNCOOH -> NH2COOH									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5946
gH + gHNO -> gNO + gH2									RTYPE=14 K1=1.00E+00 K2=0.0 K3=1.50E+03 # gg08_rnum=5947, gg08_EA_ref='M84,Origin=Igl&Silk'
gH + gHNO -> NO + H2									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5948
gH + gHNOH -> gNH2OH									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5949
gH + gHNOH -> NH2OH										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5950
gH + gCH3ONH -> gNH2OCH3								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5951
gH + gCH3ONH -> NH2OCH3									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5952
gH + gHNCH2OH -> gNH2CH2OH								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5953
gH + gHNCH2OH -> NH2CH2OH								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5954
gH + gHOC -> gHCOH										RTYPE=14 K1=0.0 K2=0.0 K3=0.0 # gg08_rnum=5955, gg08_EA_ref='AR  eliminated'
gH + gHOC -> HCOH										RTYPE=14 K1=0.0 K2=0.0 K3=0.0 # gg08_rnum=5956
gH + gHS -> gH2S										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5957, gg08_EA_ref='EH'
gH + gHS -> H2S											RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5958
gH + gMg -> gMgH										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5959, gg08_EA_ref='EH'
gH + gMg -> MgH											RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5960
gH + gMgH -> gMgH2										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5961, gg08_EA_ref='EH'
gH + gMgH -> MgH2										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5962
gH + gN -> gNH											RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5963, gg08_EA_ref='AR'
gH + gN -> NH											RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5964
gH + gN2H2 -> gH2 + gN2 + gH							RTYPE=14 K1=1.00E+00 K2=0.0 K3=6.50E+02 # gg08_rnum=5965, gg08_EA_ref='Alternative'
gH + gN2H2 -> H2 + N2 + H								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5966
gH + gNa -> gNaH										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5967, gg08_EA_ref='EH'
gH + gNa -> NaH											RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5968
gH + gNH -> gNH2										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5969, gg08_EA_ref='AR'
gH + gNH -> NH2											RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5970
gH + gNH2 -> gNH3										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5971, gg08_EA_ref='AR'
gH + gNH2 -> NH3										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5972
gH + gNH2CHO -> gH2 + gNH2CO							RTYPE=14 K1=1.00E+00 K2=0.0 K3=9.63E+03 # gg08_rnum=5973, gg08_EA_ref=null
gH + gNH2CHO -> HCO + NH3								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5974
gH + gNH2CHO -> H2 + NH2CO								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5975
gH + gNH2CHO -> gHCO + gNH3								RTYPE=14 K1=1.00E+00 K2=0.0 K3=2.10E+03 # gg08_rnum=5976, gg08_EA_ref=null
gH + gNO -> gHNO										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5977, gg08_EA_ref='AR'
gH + gNO -> HNO											RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5978
gH + gO -> gOH											RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5979, gg08_EA_ref='AR'
gH + gO -> OH											RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5980
gH + gO2 -> gHO2										RTYPE=14 K1=1.00E+00 K2=0.0 K3=264 # gg08_rnum=5981, gg08_EA_ref='TH', Ea via Zhou+2013
gH + gO2 -> HO2											RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5982
gH + gHO2 -> gH2O2										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5983, gg08_EA_ref='AR'
gH + gHO2 -> H2O2										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5984
gH + gO3 -> gO2 + gOH									RTYPE=14 K1=1.00E+00 K2=0.0 K3=4.50E+02 # gg08_rnum=5985, gg08_EA_ref='TH'
gH + gO3 -> O2 + OH										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5986
gH + gOCN -> gHNCO										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5987, gg08_EA_ref='AR'
gH + gOCN -> HNCO										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5988
gH + gOCS -> gCO + gHS									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5989, gg08_EA_ref='M84'
gH + gOCS -> CO + HS									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5990
gH + gOH -> gH2O										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5991, gg08_EA_ref='AR'
gH + gOH -> H2O											RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5992
gH + gCHOCHO -> H2CO + HCO								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5993
gH + gCHOCHO -> gH2CO + gHCO							RTYPE=14 K1=1.00E+00 K2=0.0 K3=2.40E+03 # gg08_rnum=5994, gg08_EA_ref=null
gH + gNH2CO -> gNH2CHO									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5995
gH + gNH2CO -> NH2CHO									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5996
gH + gNH2CONH -> gNH2CONH2								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5997
gH + gNH2CONH -> NH2CONH2								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5998
gH + gNH2NH -> gNH2NH2									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5999
gH + gNH2NH -> NH2NH2									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6000
gH + gS -> gHS											RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6001, gg08_EA_ref='EH'
gH + gS -> HS											RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6002
gH + gSi -> gSiH										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6003, gg08_EA_ref='EH'
gH + gSi -> SiH											RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6004
gH + gSiH -> gSiH2										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6005, gg08_EA_ref='EH'
gH + gSiH -> SiH2										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6006
gH + gSiH2 -> gSiH3										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6007, gg08_EA_ref='EH'
gH + gSiH2 -> SiH3										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6008
gH + gSiH3 -> gSiH4										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6009, gg08_EA_ref='EH'
gH + gSiH3 -> SiH4										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6010
gH + gSO2 -> gO2 + gHS									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6011, gg08_EA_ref='EH 92 Summer'
gH + gSO2 -> O2 + HS									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6012
gH + gCH3OCO -> HCOOCH3									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6013
gH + gCH3OCO -> gHCOOCH3								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6014
gH + gCH3OCONH -> gCH3OCONH2							RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6015
gH + gCH3OCONH -> CH3OCONH2								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6016
gH + gCH2OHCHO -> CH3OH + HCO							RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6017
gH + gCH2OHCHO -> gH2 + gHOCH2CO						RTYPE=14 K1=1.00E+00 K2=0.0 K3=2.85E+03 # gg08_rnum=6018, gg08_EA_ref=null
gH + gCH2OHCHO -> H2 + HOCH2CO							RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6019
gH + gCH2OHCHO -> gCH3OH + gHCO							RTYPE=14 K1=1.00E+00 K2=0.0 K3=2.40E+03 # gg08_rnum=6020, gg08_EA_ref=null
gH + gHOCH2CO -> gCH2OHCHO								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6021
gH + gHOCH2CO -> CH2OHCHO								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6022
gH + gHNCOCH2OH -> gNH2COCH2OH							RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6023
gH + gHNCOCH2OH -> NH2COCH2OH							RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6024
gH2 + gC -> gCH2										RTYPE=14 K1=1.00E+00 K2=0.0 K3=2.50E+03 # gg08_rnum=6025, gg08_EA_ref='92Summer. Exoth.'
gH2 + gC -> CH2											RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6026
gH2 + gC2 -> gC2H + gH									RTYPE=14 K1=1.00E+00 K2=0.0 K3=4.20E+03 # gg08_rnum=6027, gg08_EA_ref='Our test'
gH2 + gC2 -> C2H + H									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6028
gH2 + gC2H -> gHC2H + gH								RTYPE=14 K1=1.00E+00 K2=0.0 K3=4.20E+03 # gg08_rnum=6029, gg08_EA_ref=null
gH2 + gC2H -> HC2H + H									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6030
gH2 + gC3 -> gC3H + gH									RTYPE=14 K1=1.00E+00 K2=0.0 K3=4.20E+03 # gg08_rnum=6031, gg08_EA_ref=null
gH2 + gC3 -> C3H + H									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6032
gH2 + gC3H -> gC3H2 + gH								RTYPE=14 K1=1.00E+00 K2=0.0 K3=4.20E+03 # gg08_rnum=6033, gg08_EA_ref=null
gH2 + gC3H -> C3H2 + H									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6034
gH2 + gC4 -> gC4H + gH									RTYPE=14 K1=1.00E+00 K2=0.0 K3=4.20E+03 # gg08_rnum=6035, gg08_EA_ref=null
gH2 + gC4 -> C4H + H									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6036
gH2 + gC4H -> gHC4H + gH								RTYPE=14 K1=1.00E+00 K2=0.0 K3=4.20E+03 # gg08_rnum=6037, gg08_EA_ref=null
gH2 + gC4H -> HC4H + H									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6038
gH2 + gC5 -> gC5H + gH									RTYPE=14 K1=1.00E+00 K2=0.0 K3=4.20E+03 # gg08_rnum=6039, gg08_EA_ref=null
gH2 + gC5 -> C5H + H									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6040
gH2 + gC5H -> gC5H2 + gH								RTYPE=14 K1=1.00E+00 K2=0.0 K3=4.20E+03 # gg08_rnum=6041, gg08_EA_ref=null
gH2 + gC5H -> C5H2 + H									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6042
gH2 + gC6 -> gC6H + gH									RTYPE=14 K1=1.00E+00 K2=0.0 K3=4.20E+03 # gg08_rnum=6043, gg08_EA_ref=null
gH2 + gC6 -> C6H + H									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6044
gH2 + gC6H -> gC6H2 + gH								RTYPE=14 K1=1.00E+00 K2=0.0 K3=4.20E+03 # gg08_rnum=6045, gg08_EA_ref=null
gH2 + gC6H -> C6H2 + H									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6046
gH2 + gC7 -> gC7H + gH									RTYPE=14 K1=1.00E+00 K2=0.0 K3=4.20E+03 # gg08_rnum=6047, gg08_EA_ref=null
gH2 + gC7 -> C7H + H									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6048
gH2 + gC7H -> gC7H2 + gH								RTYPE=14 K1=1.00E+00 K2=0.0 K3=4.20E+03 # gg08_rnum=6049, gg08_EA_ref=null
gH2 + gC7H -> C7H2 + H									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6050
gH2 + gC8 -> gC8H + gH									RTYPE=14 K1=1.00E+00 K2=0.0 K3=4.20E+03 # gg08_rnum=6051, gg08_EA_ref=null
gH2 + gC8 -> C8H + H									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6052
gH2 + gC8H -> gC8H2 + gH								RTYPE=14 K1=1.00E+00 K2=0.0 K3=4.20E+03 # gg08_rnum=6053, gg08_EA_ref=null
gH2 + gC8H -> C8H2 + H									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6054
gH2 + gC9 -> gC9H + gH									RTYPE=14 K1=1.00E+00 K2=0.0 K3=4.20E+03 # gg08_rnum=6055, gg08_EA_ref=null
gH2 + gC9 -> C9H + H									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6056
gH2 + gC9H -> gC9H2 + gH								RTYPE=14 K1=1.00E+00 K2=0.0 K3=4.20E+03 # gg08_rnum=6057, gg08_EA_ref=null
gH2 + gC9H -> C9H2 + H									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6058
gH2 + gCH2 -> gCH3 + gH									RTYPE=14 K1=1.00E+00 K2=0.0 K3=3.53E+03 # gg08_rnum=6059, gg08_EA_ref='NSRDS-NBS67 Westley 80'
gH2 + gCH2 -> CH3 + H									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6060
gH2 + gCH3 -> gCH4 + gH									RTYPE=14 K1=1.00E+00 K2=0.0 K3=6.44E+03 # gg08_rnum=6061, gg08_EA_ref='JChPhy 50,5076 (1969)'
gH2 + gCH3 -> CH4 + H									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6062
gH2 + gCN -> gHCN + gH									RTYPE=14 K1=1.00E+00 K2=0.0 K3=2.07E+03 # gg08_rnum=6063, gg08_EA_ref='M84 and 1988 UMIST'
gH2 + gCN -> HCN + H									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6064
gH2 + gNH2 -> gNH3 + gH									RTYPE=14 K1=1.00E+00 K2=0.0 K3=6.30E+03 # gg08_rnum=6065, gg08_EA_ref='M84'
gH2 + gNH2 -> NH3 + H									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6066
gH2 + gOH -> gH2O + gH									RTYPE=14 K1=1.00E+00 K2=0.0 K3=2.10E+03 # gg08_rnum=6067, gg08_EA_ref='JPhyChRefDat,13,1259'
gH2 + gOH -> H2O + H									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6068
gHCO + gCH3CO -> CH3COCHO								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6069
gHCO + gCH3CO -> gCH3COCHO								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6070
gHCO + gCH3O -> HCOOCH3									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6071
gHCO + gCH3O -> gHCOOCH3								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6072
gHCO + gCOCHO -> gHCOCOCHO								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6073
gHCO + gCOCHO -> HCOCOCHO								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6074
gHCO + gCOOH -> HCOCOOH									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6075
gHCO + gCOOH -> gHCOCOOH								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6076
gHCO + gHCO -> CHOCHO									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6077
gHCO + gHCO -> gCHOCHO									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6078
gHCO + gHNCO -> HNCOCHO									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6079
gHCO + gHNCO -> gHNCOCHO								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6080
gHCO + gNH2CO -> gNH2COCHO								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6081
gHCO + gNH2CO -> NH2COCHO								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6082
gHCO + gCH3OCO -> gCH3OCOCHO							RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6083
gHCO + gCH3OCO -> CH3OCOCHO								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6084
gHCO + gHOCH2CO -> HOCH2COCHO							RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6085
gHCO + gHOCH2CO -> gHOCH2COCHO							RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6086
gN + gC2 -> gC2N										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6087, gg08_EA_ref='AR'
gN + gC2 -> C2N											RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6088
gN + gC3 -> gC3N										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6089, gg08_EA_ref='EH'
gN + gC3 -> C3N											RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6090
gN + gC3H -> gHC3N										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6091, gg08_EA_ref='EH'
gN + gC3H -> HC3N										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6092
gN + gC5 -> gC5N										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6093, gg08_EA_ref='EH'
gN + gC5 -> C5N											RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6094
gN + gC5H -> gHC5N										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6095, gg08_EA_ref='EH'
gN + gC5H -> HC5N										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6096
gN + gC7 -> gC7N										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6097, gg08_EA_ref='EH'
gN + gC7 -> C7N											RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6098
gN + gC7H -> gHC7N										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6099, gg08_EA_ref='EH'
gN + gC7H -> HC7N										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6100
gN + gC9 -> gC9N										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6101, gg08_EA_ref='EH'
gN + gC9 -> C9N											RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6102
gN + gC9H -> gHC9N										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6103, gg08_EA_ref='EH'
gN + gC9H -> HC9N										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6104
gN + gCH -> gHCN										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6105, gg08_EA_ref='AR'
gN + gCH -> HCN											RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6106
gN + gCH2 -> gH2CN										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6107, gg08_EA_ref='AR'
gN + gCH2 -> H2CN										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6108
gN + gCH3 -> gCH2NH										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6109, gg08_EA_ref='AR'
gN + gCH3 -> CH2NH										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6110
gN + gHS -> gNS + gH									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6111, gg08_EA_ref='M84'
gN + gHS -> NS + H										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6112
gN + gN -> gN2											RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6113, gg08_EA_ref='AR'
gN + gN -> N2											RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6114
gN + gNH -> gN2 + gH									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6115, gg08_EA_ref='AR Alternative'
gN + gNH -> N2 + H										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6116
gN + gNH2 -> gN2H2										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6117, gg08_EA_ref='AR'
gN + gNH2 -> N2H2										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6118
gN + gNS -> gN2 + gS									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6119, gg08_EA_ref='M84'
gN + gNS -> N2 + S										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6120
gN + gO -> gNO											RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6121, gg08_EA_ref='AR'
gN + gO -> NO											RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6122
gN + gHO2 -> gO2 + gNH									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6123, gg08_EA_ref='M84'
gN + gHO2 -> O2 + NH									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6124
gN + gS -> gNS											RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6125, gg08_EA_ref='Hasegawa'
gN + gS -> NS											RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6126
gNH + gCH2 -> gCH2NH									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6127, gg08_EA_ref='AR'
gNH + gCH2 -> CH2NH										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6128
gNH + gCH2OH -> gHNCH2OH								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6129
gNH + gCH2OH -> HNCH2OH									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6130
gNH + gCH3 -> CH3NH										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6131
gNH + gCH3 -> gCH3NH									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6132, gg08_EA_ref='AR (Implemented)'
gNH + gCH3CHO -> gNH2 + gCH3CO							RTYPE=14 K1=1.00E+00 K2=0.0 K3=5.77E+03 # gg08_rnum=6133, gg08_EA_ref=null
gNH + gCH3CHO -> NH2 + CH3CO							RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6134
gNH + gCH3CO -> gCH3CONH								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6135
gNH + gCH3CO -> CH3CONH									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6136
gNH + gCH3O -> CH3ONH									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6137
gNH + gCH3O -> gCH3ONH									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6138
gNH + gHCOH -> gNH2CHO									RTYPE=14 K1=0.0 K2=0.0 K3=0.0 # gg08_rnum=6139
gNH + gHCOH -> NH2CHO									RTYPE=14 K1=0.0 K2=0.0 K3=0.0 # gg08_rnum=6140
gNH + gCO -> gHNCO										RTYPE=14 K1=1.00E+00 K2=0.0 K3=1.50E+03 # gg08_rnum=6141, gg08_EA_ref=null
gNH + gCO -> HNCO										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6142
gNH + gCOCHO -> HNCOCHO									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6143
gNH + gCOCHO -> gHNCOCHO								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6144
gNH + gCOOH -> gHNCOOH									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6145
gNH + gCOOH -> HNCOOH									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6146
gNH + gH2CO -> NH2 + HCO								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6147
gNH + gH2CO -> gNH2 + gHCO								RTYPE=14 K1=1.00E+00 K2=0.0 K3=2.85E+03 # gg08_rnum=6148, gg08_EA_ref=null
gNH + gHCO -> HNCHO										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6149
gNH + gHCO -> gHNCHO									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6150
gNH + gHNCO -> gHNCONH									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6151
gNH + gHNCO -> HNCONH									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6152
gNH + gNH -> gN2H2										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6153, gg08_EA_ref='AR   130kcal/mol exoth'
gNH + gNH -> N2H2										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6154
gNH + gNH -> gN2 + gH2									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6155, gg08_EA_ref='M84. 180kcal/mol exoth'
gNH + gNH -> N2 + H2									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6156
gNH + gNH2 -> gNH2NH									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6157
gNH + gNH2 -> NH2NH										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6158
gNH + gNO -> N2 + O + H									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6159
gNH + gNO -> gN2 + gO + gH								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6160, gg08_EA_ref='From our gas network'
gNH + gOH -> gHNOH										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6161
gNH + gOH -> HNOH										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6162
gNH + gCHOCHO -> HNCHO + HCO							RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6163
gNH + gCHOCHO -> gHNCHO + gHCO							RTYPE=14 K1=1.00E+00 K2=0.0 K3=2.40E+03 # gg08_rnum=6164, gg08_EA_ref=null
gNH + gNH2CO -> NH2CONH									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6165
gNH + gNH2CO -> gNH2CONH								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6166
gNH + gCH3OCO -> CH3OCONH								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6167
gNH + gCH3OCO -> gCH3OCONH								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6168
gNH + gCH2OHCHO -> NH2 + HOCH2CO						RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6169
gNH + gCH2OHCHO -> gNH2 + gHOCH2CO						RTYPE=14 K1=1.00E+00 K2=0.0 K3=2.85E+03 # gg08_rnum=6170, gg08_EA_ref=null
gNH + gHOCH2CO -> HNCOCH2OH								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6171
gNH + gHOCH2CO -> gHNCOCH2OH							RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6172
gNH2 + gCH2OH -> NH2CH2OH								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6173
gNH2 + gCH2OH -> gNH2CH2OH								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6174
gNH2 + gCH3 -> gCH3NH2									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6175, gg08_EA_ref='AR'
gNH2 + gCH3 -> CH3NH2									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6176
gNH2 + gCH3CHO -> gNH3 + gCH3CO							RTYPE=14 K1=1.00E+00 K2=0.0 K3=1.25E+03 # gg08_rnum=6177, gg08_EA_ref=null
gNH2 + gCH3CHO -> NH3 + CH3CO							RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6178
gNH2 + gCH3CHO -> gCH3NH2 + gHCO						RTYPE=14 K1=1.00E+00 K2=0.0 K3=2.40E+03 # gg08_rnum=6179, gg08_EA_ref=null
gNH2 + gCH3CHO -> gNH2CHO + gCH3						RTYPE=14 K1=1.00E+00 K2=0.0 K3=2.40E+03 # gg08_rnum=6180, gg08_EA_ref=null
gNH2 + gCH3CHO -> CH3NH2 + HCO							RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6181
gNH2 + gCH3CHO -> NH2CHO + CH3							RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6182
gNH2 + gCH3CO -> gCH3CONH2								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6183
gNH2 + gCH3CO -> CH3CONH2								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6184
gNH2 + gCH3O -> gNH2OCH3								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6185
gNH2 + gCH3O -> NH2OCH3									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6186
gNH2 + gCO -> gNH2CO									RTYPE=14 K1=1.00E+00 K2=0.0 K3=1.50E+03 # gg08_rnum=6187, gg08_EA_ref=null
gNH2 + gCO -> NH2CO										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6188
gNH2 + gCOCHO -> gNH2COCHO								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6189
gNH2 + gCOCHO -> NH2COCHO								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6190
gNH2 + gCOOH -> NH2COOH									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6191
gNH2 + gCOOH -> gNH2COOH								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6192
gNH2 + gH2CO -> NH3 + HCO								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6193
gNH2 + gH2CO -> NH2CHO + H								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6194
gNH2 + gH2CO -> gNH2CHO + gH							RTYPE=14 K1=1.00E+00 K2=0.0 K3=2.85E+03 # gg08_rnum=6195, gg08_EA_ref=null
gNH2 + gH2CO -> gNH3 + gHCO								RTYPE=14 K1=1.00E+00 K2=0.0 K3=2.85E+03 # gg08_rnum=6196, gg08_EA_ref=null
gNH2 + gHCO -> gNH2CHO									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6197, gg08_EA_ref='DPR-GUESS'
gNH2 + gHCO -> NH2CHO									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6198
gNH2 + gHCOOCH3 -> NH3 + CH3OCO							RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6199
gNH2 + gHCOOCH3 -> gNH3 + gCH3OCO						RTYPE=14 K1=1.00E+00 K2=0.0 K3=2.85E+03 # gg08_rnum=6200, gg08_EA_ref=null
gNH2 + gHCOOH -> NH3 + COOH								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6201
gNH2 + gHCOOH -> gNH3 + gCOOH							RTYPE=14 K1=1.00E+00 K2=0.0 K3=2.85E+03 # gg08_rnum=6202, gg08_EA_ref=null
gNH2 + gHNCO -> gNH2CONH								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6203
gNH2 + gHNCO -> NH2CONH									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6204
gNH2 + gNH2 -> NH2NH2									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6205
gNH2 + gNH2 -> gNH2NH2									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6206
gNH2 + gNH2CHO -> gNH3 + gNH2CO							RTYPE=14 K1=1.00E+00 K2=0.0 K3=2.85E+03 # gg08_rnum=6207, gg08_EA_ref=null
gNH2 + gNH2CHO -> NH3 + NH2CO							RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6208
gNH2 + gNO -> gH2O + gN2								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6209, gg08_EA_ref='From our gas network'
gNH2 + gNO -> H2O + N2									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6210
gNH2 + gCHOCHO -> gNH2CHO + gHCO						RTYPE=14 K1=1.00E+00 K2=0.0 K3=2.40E+03 # gg08_rnum=6211, gg08_EA_ref=null
gNH2 + gCHOCHO -> NH2CHO + HCO							RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6212
gNH2 + gNH2CO -> NH2CONH2								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6213
gNH2 + gNH2CO -> gNH2CONH2								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6214
gNH2 + gCH3OCO -> CH3OCONH2								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6215
gNH2 + gCH3OCO -> gCH3OCONH2							RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6216
gNH2 + gCH2OHCHO -> NH2CHO + CH2OH						RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6217
gNH2 + gCH2OHCHO -> NH3 + HOCH2CO						RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6218
gNH2 + gCH2OHCHO -> gNH2CHO + gCH2OH					RTYPE=14 K1=1.00E+00 K2=0.0 K3=2.40E+03 # gg08_rnum=6219, gg08_EA_ref=null
gNH2 + gCH2OHCHO -> gNH3 + gHOCH2CO						RTYPE=14 K1=1.00E+00 K2=0.0 K3=2.85E+03 # gg08_rnum=6220, gg08_EA_ref=null
gNH2 + gHOCH2CO -> gNH2COCH2OH							RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6221
gNH2 + gHOCH2CO -> NH2COCH2OH							RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6222
gO + gC2 -> gC2O										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6223, gg08_EA_ref='AR (more likely)'
gO + gC2 -> C2O											RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6224
gO + gC3 -> gC3O										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6225, gg08_EA_ref='EH'
gO + gC3 -> C3O											RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6226
gO + gCH -> gHCO										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6227, gg08_EA_ref='AR'
gO + gCH -> HCO											RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6228
gO + gCH2 -> gH2CO										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6229, gg08_EA_ref='AR'
gO + gCH2 -> H2CO										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6230
gO + gCH3 -> gCH3O										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6231, gg08_EA_ref='AR (CH3O), Brown'
gO + gCH3 -> CH3O										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6232
gO + gCN -> gOCN										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6233, gg08_EA_ref='AR'
gO + gCN -> OCN											RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6234
gO + gCO -> gCO2										RTYPE=14 K1=1.00E+00 K2=0.0 K3=1.00E+03 # gg08_rnum=6235, gg08_EA_ref='dHendecourt etal'
gO + gCO -> CO2											RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6236
gO + gCS -> gOCS										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6237, gg08_EA_ref='TH'
gO + gCS -> OCS											RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6238
gO + gHCO -> gCO2 + gH									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6239, gg08_EA_ref='M84 and Eric/gas chem'
gO + gHCO -> CO2 + H									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6240
gO + gHCO -> gCO + gOH									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6241, gg08_EA_ref='Leung/Eric gas chem'
gO + gHCO -> CO + OH									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6242
gO + gHNO -> gNO + gOH									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6243, gg08_EA_ref='M84'
gO + gHNO -> NO + OH									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6244
gO + gHOC -> gCO + gOH									RTYPE=14 K1=0.0 K2=0.0 K3=0.0 # gg08_rnum=6245, gg08_EA_ref='From above reactions'
gO + gHOC -> CO + OH									RTYPE=14 K1=0.0 K2=0.0 K3=0.0 # gg08_rnum=6246
gO + gHOC -> gCO2 + gH									RTYPE=14 K1=0.0 K2=0.0 K3=0.0 # gg08_rnum=6247, gg08_EA_ref='Paola/Eric'
gO + gHOC -> CO2 + H									RTYPE=14 K1=0.0 K2=0.0 K3=0.0 # gg08_rnum=6248
gO + gHS -> gSO + gH									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6249, gg08_EA_ref='M84'
gO + gHS -> SO + H										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6250
gO + gNH -> gHNO										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6251, gg08_EA_ref='AR'
gO + gNH -> HNO											RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6252
gO + gNH2 -> gHNO + gH									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6253, gg08_EA_ref='M84'
gO + gNH2 -> HNO + H									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6254
gO + gNS -> gNO + gS									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6255, gg08_EA_ref='M84'
gO + gNS -> NO + S										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6256
gO + gO -> gO2											RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6257, gg08_EA_ref='AR'
gO + gO -> O2											RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6258
gO + gO2 -> gO3											RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6259, gg08_EA_ref='AR,TH'
gO + gO2 -> O3											RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6260
gO + gHO2 -> gO2 + gOH									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6261, gg08_EA_ref='M84'
gO + gHO2 -> O2 + OH									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6262
gO + gOH -> gHO2										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6263, gg08_EA_ref='AR'
gO + gOH -> HO2											RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6264
gO + gS -> gSO											RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6265, gg08_EA_ref='TH'
gO + gS -> SO											RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6266
gO + gSO -> gSO2										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6267, gg08_EA_ref='TH'
gO + gSO -> SO2											RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6268
gOH + gCH2 -> gCH2OH									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6269, gg08_EA_ref='AR'
gOH + gCH2 -> CH2OH										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6270
gOH + gCH2OH -> gHOCH2OH								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6271
gOH + gCH2OH -> HOCH2OH									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6272
gOH + gCH3 -> gCH3OH									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6273, gg08_EA_ref='AR'
gOH + gCH3 -> CH3OH										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6274
gOH + gCH3CHO -> gHCOOH + gCH3							RTYPE=14 K1=1.00E+00 K2=0.0 K3=2.40E+03 # gg08_rnum=6275, gg08_EA_ref=null
gOH + gCH3CHO -> CH3OH + HCO							RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6276
gOH + gCH3CHO -> HCOOH + CH3							RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6277
gOH + gCH3CHO -> gH2O + gCH3CO							RTYPE=14 K1=1.00E+00 K2=0.0 K3=1.50E+03 # gg08_rnum=6278, gg08_EA_ref=null
gOH + gCH3CHO -> H2O + CH3CO							RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6279
gOH + gCH3CHO -> gCH3OH + gHCO							RTYPE=14 K1=1.00E+00 K2=0.0 K3=2.40E+03 # gg08_rnum=6280, gg08_EA_ref=null
gOH + gCH3CO -> gCH3COOH								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6281
gOH + gCH3CO -> CH3COOH									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6282
gOH + gCH3O -> CH3OOH									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6283
gOH + gCH3O -> gCH3OOH									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6284
gOH + gCO -> gCOOH										RTYPE=14 K1=1.00E+00 K2=0.0 K3=1.50E+03 # gg08_rnum=6285, gg08_EA_ref=null
gOH + gCO -> COOH										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6286
gOH + gCO -> gCO2 + gH									RTYPE=14 K1=1.00E+00 K2=0.0 K3=8.00E+01 # gg08_rnum=6287, gg08_EA_ref='Eric/Chun, Vidali=290K'
gOH + gCO -> CO2 + H									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6288
gOH + gCOCHO -> gHCOCOOH								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6289
gOH + gCOCHO -> HCOCOOH									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6290
gOH + gCOOH -> HOCOOH									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6291
gOH + gCOOH -> gHOCOOH									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6292
gOH + gH2CO -> HCOOH + H								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6293
gOH + gH2CO -> HCO + H2O								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6294
gOH + gH2CO -> gHCOOH + gH								RTYPE=14 K1=1.00E+00 K2=0.0 K3=2.85E+03 # gg08_rnum=6295, gg08_EA_ref=null
gOH + gH2CO -> gHCO + gH2O								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6296, gg08_EA_ref='Changed 8-10-06 (only 1)'
gOH + gHCO -> HCOOH										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6297
gOH + gHCO -> gHCOOH									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6298, gg08_EA_ref='AR(HCO2H) makes sense'
gOH + gHCOOCH3 -> gHCOOH + gCH3O						RTYPE=14 K1=1.00E+00 K2=0.0 K3=2.45E+03 # gg08_rnum=6299, gg08_EA_ref=null
gOH + gHCOOCH3 -> gH2O + gCH3OCO						RTYPE=14 K1=1.00E+00 K2=0.0 K3=1.50E+03 # gg08_rnum=6300, gg08_EA_ref=null
gOH + gHCOOCH3 -> H2O + CH3OCO							RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6301
gOH + gHCOOCH3 -> HCOOH + CH3O							RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6302
gOH + gHCOOH -> H2O + COOH								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6303
gOH + gHCOOH -> gH2O + gCOOH							RTYPE=14 K1=1.00E+00 K2=0.0 K3=1.50E+03 # gg08_rnum=6304, gg08_EA_ref=null
gOH + gHNCO -> gHNCOOH									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6305
gOH + gHNCO -> HNCOOH									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6306
gOH + gNH2 -> gNH2OH									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6307, gg08_EA_ref='AR'
gOH + gNH2 -> NH2OH										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6308
gOH + gNH2CHO -> gH2O + gNH2CO							RTYPE=14 K1=1.00E+00 K2=0.0 K3=1.50E+03 # gg08_rnum=6309, gg08_EA_ref=null
gOH + gNH2CHO -> HCOOH + NH2							RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6310
gOH + gNH2CHO -> gHCOOH + gNH2							RTYPE=14 K1=1.00E+00 K2=0.0 K3=2.10E+03 # gg08_rnum=6311, gg08_EA_ref=null
gOH + gNH2CHO -> H2O + NH2CO							RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6312
gOH + gOH -> H2O2										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6313
gOH + gOH -> gH2O2										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6314, gg08_EA_ref='AR'
gOH + gCHOCHO -> COCHO + H2O							RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6315
gOH + gCHOCHO -> HCOOH + HCO							RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6316
gOH + gCHOCHO -> gHCOOH + gHCO							RTYPE=14 K1=1.00E+00 K2=0.0 K3=2.40E+03 # gg08_rnum=6317, gg08_EA_ref=null
gOH + gCHOCHO -> gCOCHO + gH2O							RTYPE=14 K1=1.00E+00 K2=0.0 K3=1.50E+03 # gg08_rnum=6318, gg08_EA_ref=null
gOH + gNH2CO -> NH2COOH									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6319
gOH + gNH2CO -> gNH2COOH								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6320
gOH + gCH3OCO -> CH3OCOOH								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6321
gOH + gCH3OCO -> gCH3OCOOH								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6322
gOH + gCH2OHCHO -> HCOOH + CH2OH						RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6323
gOH + gCH2OHCHO -> H2O + HOCH2CO						RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6324
gOH + gCH2OHCHO -> gH2O + gHOCH2CO						RTYPE=14 K1=1.00E+00 K2=0.0 K3=1.50E+03 # gg08_rnum=6325, gg08_EA_ref=null
gOH + gCH2OHCHO -> gHCOOH + gCH2OH						RTYPE=14 K1=1.00E+00 K2=0.0 K3=2.40E+03 # gg08_rnum=6326, gg08_EA_ref=null
gOH + gHOCH2CO -> gCH2OHCOOH							RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6327
gOH + gHOCH2CO -> CH2OHCOOH								RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6328
gS + gCH -> gHCS										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6329, gg08_EA_ref='hasegawa'
gS + gCH -> HCS											RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6330
gS + gCH3 -> gH2CS + gH									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6331, gg08_EA_ref='M84'
gS + gCH3 -> H2CS + H									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6332
gS + gCO -> gOCS										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6333, gg08_EA_ref='From our gas network'
gS + gCO -> OCS											RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6334
gS + gNH -> gNS + gH									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6335, gg08_EA_ref='M84'
gS + gNH -> NS + H										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6336
gC -> C													RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6337
gC10 -> C10												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6338
gC2 -> C2												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6339
gC2H -> C2H												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6340
gHC2H -> HC2H											RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6341
gH2C2H -> H2C2H											RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6342
gH2C2H2 -> H2C2H2										RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6343
gCH3CH2 -> CH3CH2										RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6344
gCH3CH2CHO -> CH3CH2CHO									RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6345
gCH3CH2OH -> CH3CH2OH									RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6346
gCH3CH3 -> CH3CH3										RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6347
gC2N -> C2N												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6348
gC2O -> C2O												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6349
gC2S -> C2S												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6350
gC3 -> C3												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6351
gC3H -> C3H												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6352
gC3H2 -> C3H2											RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6353
gC3H3 -> C3H3											RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6354
gC3H3N -> C3H3N											RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6355
gC3H4 -> C3H4											RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6356
gC3N -> C3N												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6357
gC3O -> C3O												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6358
gC3P -> C3P												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6359
gC3S -> C3S												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6360
gC4 -> C4												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6361
gC4H -> C4H												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6362
gHC4H -> HC4H											RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6363
gC4H3 -> C4H3											RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6364
gC4H4 -> C4H4											RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6365
gC4N -> C4N												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6366
gC4P -> C4P												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6367
gC4S -> C4S												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6368
gC5 -> C5												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6369
gC5H -> C5H												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6370
gC5H2 -> C5H2											RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6371
gC5H3 -> C5H3											RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6372
gC5H4 -> C5H4											RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6373
gC5N -> C5N												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6374
gC6 -> C6												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6375
gC6H -> C6H												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6376
gC6H2 -> C6H2											RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6377
gC6H3 -> C6H3											RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6378
gC6H4 -> C6H4											RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6379
gC6H6 -> C6H6											RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6380
gC7 -> C7												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6381
gC7H -> C7H												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6382
gC7H2 -> C7H2											RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6383
gC7H3 -> C7H3											RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6384
gC7H4 -> C7H4											RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6385
gC7N -> C7N												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6386
gC8 -> C8												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6387
gC8H -> C8H												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6388
gC8H2 -> C8H2											RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6389
gC8H3 -> C8H3											RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6390
gC8H4 -> C8H4											RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6391
gC9 -> C9												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6392
gC9H -> C9H												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6393
gC9H2 -> C9H2											RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6394
gC9H3 -> C9H3											RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6395
gC9H4 -> C9H4											RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6396
gC9N -> C9N												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6397
gClC -> ClC												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6398
gCCP -> CCP												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6399
gCH -> CH												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6400
gCH2 -> CH2												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6401
gH2C2N -> H2C2N											RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6402
gCH2CO -> CH2CO											RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6403
gCH2NH -> CH2NH											RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6404
gCH2NH2 -> CH2NH2										RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6405
gCH2OH -> CH2OH											RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6406
gCH2PH -> CH2PH											RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6407
gCH3 -> CH3												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6408
gCH3C3N -> CH3C3N										RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6409
gCH3C4H -> CH3C4H										RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6410
gCH3C5N -> CH3C5N										RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6411
gCH3C6H -> CH3C6H										RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6412
gCH3C7N -> CH3C7N										RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6413
gCH3CHO -> CH3CHO										RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6414
gCH3CN -> CH3CN											RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6415
gCH3CO -> CH3CO											RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6416
gCH3NH -> CH3NH											RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6417
gCH3NH2 -> CH3NH2										RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6418
gCH3O -> CH3O											RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6419
gCH3OCH3 -> CH3OCH3										RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6420
gCH3OH -> CH3OH											RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6421
gCH4 -> CH4												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6422
gHCNH -> HCNH											RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6423
gHCOH -> HCOH											RTYPE=15 K1=0.0 K2=0.0 K3=0.0 # gg08_rnum=6424
gCl -> Cl												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6425
gClO -> ClO												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6426
gCN -> CN												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6427
gCO -> CO												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6428
gCO2 -> CO2												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6429
gCOCHO -> COCHO											RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6430
gCOOH -> COOH											RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6431
gCP -> CP												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6432
gCS -> CS												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6433
gFe -> Fe												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6434
gFeH -> FeH												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6435
gH -> H													RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6436
gH2 -> H2												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6437
gHC3NH -> HC3NH											RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6438
gcH2C3O -> cH2C3O										RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6439
gH2C5N -> H2C5N											RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6440
gH2C7N -> H2C7N											RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6441
gH2C9N -> H2C9N											RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6442
gH2CN -> H2CN											RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6443
gH2CO -> H2CO											RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6444
gH2CS -> H2CS											RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6445
gH2O -> H2O												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6446
gH2O2 -> H2O2											RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6447
gH2S -> H2S												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6448
gH2S2 -> H2S2											RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6449
gH2SiO -> H2SiO											RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6450
gH3C5N -> H3C5N											RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6451
gH3C7N -> H3C7N											RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6452
gH3C9N -> H3C9N											RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6453
gH4C3N -> H4C3N											RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6454
gH5C3N -> H5C3N											RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6455
gHC2NC -> HC2NC											RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6456
gHC2O -> HC2O											RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6457
gHC3N -> HC3N											RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6458
gHC3O -> HC3O											RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6459
gHC5N -> HC5N											RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6460
gHC7N -> HC7N											RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6461
gHC9N -> HC9N											RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6462
gHC2N -> HC2N											RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6463
gHCCP -> HCCP											RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6464
gClH -> ClH												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6465
gHCN -> HCN												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6466
gC2NCH -> C2NCH											RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6467
gHCO -> HCO												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6468
gHCOOCH3 -> HCOOCH3										RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6469
gHCOOH -> HCOOH											RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6470
gHCP -> HCP												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6471
gHCS -> HCS												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6472
gSiCH -> SiCH											RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6473
gHe -> He												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6474
gCH3COCHO -> CH3COCHO									RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6475
gCH3COCH3 -> CH3COCH3									RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6476
gCH3CONH -> CH3CONH										RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6477
gCH3COOH -> CH3COOH										RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6478
gCH3CONH2 -> CH3CONH2									RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6479
gCH3COOCH3 -> CH3COOCH3									RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6480
gCH3COCH2OH -> CH3COCH2OH								RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6481
gHNC -> HNC												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6482
gHNC3 -> HNC3											RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6483
gHNCHO -> HNCHO											RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6484
gHNCO -> HNCO											RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6485
gHNCOCHO -> HNCOCHO										RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6486
gHNCONH -> HNCONH										RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6487
gHNCOOH -> HNCOOH										RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6488
gHNO -> HNO												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6489
gHNOH -> HNOH											RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6490
gHNSi -> HNSi											RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6491
gCH3ONH -> CH3ONH										RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6492
gHNCH2OH -> HNCH2OH										RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6493
gHOC -> HOC												RTYPE=15 K1=0.0 K2=0.0 K3=0.0 # gg08_rnum=6494
gHOCOOH -> HOCOOH										RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6495
gHPO -> HPO												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6496
gHS -> HS												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6497
gS2H -> S2H												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6498
gHCOCOCHO -> HCOCOCHO									RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6499
gMg -> Mg												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6500
gMgH -> MgH												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6501
gMgH2 -> MgH2											RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6502
gN -> N													RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6503
gN2 -> N2												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6504
gN2H2 -> N2H2											RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6505
gN2O -> N2O												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6506
gNa -> Na												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6507
gNaH -> NaH												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6508
gNaOH -> NaOH											RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6509
gNH -> NH												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6510
gNH2 -> NH2												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6511
gNH2CHO -> NH2CHO										RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6512
gNH2CN -> NH2CN											RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6513
gNH2OH -> NH2OH											RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6514
gNH3 -> NH3												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6515
gNO -> NO												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6516
gNO2 -> NO2												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6517
gNS -> NS												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6518
gO -> O													RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6519
gO2 -> O2												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6520
gHO2 -> HO2												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6521
gO3 -> O3												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6522
gOCN -> OCN												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6523
gOCS -> OCS												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6524
gOH -> OH												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6525
gCHOCHO -> CHOCHO										RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6526
gHCOCOOH -> HCOCOOH										RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6527
gP -> P													RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6528
gPH -> PH												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6529
gPH2 -> PH2												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6530
gPN -> PN												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6531
gPO -> PO												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6532
gNH2CO -> NH2CO											RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6533
gNH2COCHO -> NH2COCHO									RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6534
gNH2CONH -> NH2CONH										RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6535
gNH2COOH -> NH2COOH										RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6536
gNH2CONH2 -> NH2CONH2									RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6537
gNH2NH -> NH2NH											RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6538
gNH2NH2 -> NH2NH2										RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6539
gNH2OCH3 -> NH2OCH3										RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6540
gNH2CH2OH -> NH2CH2OH									RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6541
gS -> S													RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6542
gS2 -> S2												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6543
gSi -> Si												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6544
gSiC -> SiC												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6545
gSiC2 -> SiC2											RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6546
gSiC2H -> SiC2H											RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6547
gSiC2H2 -> SiC2H2										RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6548
gSiC3 -> cSiC3											RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6549
gSiC3H -> SiC3H											RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6550
gSiC4 -> SiC4											RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6551
gSiCH2 -> SiCH2											RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6552
gSiCH3 -> SiCH3											RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6553
gSiH -> SiH												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6554
gSiH2 -> SiH2											RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6555
gSiH3 -> SiH3											RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6556
gSiH4 -> SiH4											RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6557
gSiN -> SiN												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6558
gSiNC -> SiNC											RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6559
gSiO -> SiO												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6560
gSiO2 -> SiO2											RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6561
gSiS -> SiS												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6562
gSO -> SO												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6563
gSO2 -> SO2												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6564
gCH3OCO -> CH3OCO										RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6565
gCH3OCOCHO -> CH3OCOCHO									RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6566
gCH3OCONH -> CH3OCONH									RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6567
gCH3OCOOH -> CH3OCOOH									RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6568
gCH3OCONH2 -> CH3OCONH2									RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6569
gCH3OCOOCH3 -> CH3OCOOCH3								RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6570
gCH3OCOCH2OH -> CH3OCOCH2OH								RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6571
gCH3OOH -> CH3OOH										RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6572
gCH3OOCH3 -> CH3OOCH3									RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6573
gCH3OCH2OH -> CH3OCH2OH									RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6574
gCH2OHCHO -> CH2OHCHO									RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6575
gHOCH2CO -> HOCH2CO										RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6576
gHOCH2COCHO -> HOCH2COCHO								RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6577
gHNCOCH2OH -> HNCOCH2OH									RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6578
gCH2OHCOOH -> CH2OHCOOH									RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6579
gNH2COCH2OH -> NH2COCH2OH								RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6580
gHOCH2COCH2OH -> HOCH2COCH2OH							RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6581
gHOCH2OH -> HOCH2OH										RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6582
gHOCH2CH2OH -> HOCH2CH2OH								RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6583
gC -> C													RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6584
gC10 -> C10												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6585
gC2 -> C2												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6586
gC2H -> C2H												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6587
gHC2H -> HC2H											RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6588
gH2C2H -> H2C2H											RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6589
gH2C2H2 -> H2C2H2										RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6590
gCH3CH2 -> CH3CH2										RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6591
gCH3CH2CHO -> CH3CH2CHO									RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6592
gCH3CH2OH -> CH3CH2OH									RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6593
gCH3CH3 -> CH3CH3										RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6594
gC2N -> C2N												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6595
gC2O -> C2O												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6596
gC2S -> C2S												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6597
gC3 -> C3												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6598
gC3H -> C3H												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6599
gC3H2 -> C3H2											RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6600
gC3H3 -> C3H3											RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6601
gC3H3N -> C3H3N											RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6602
gC3H4 -> C3H4											RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6603
gC3N -> C3N												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6604
gC3O -> C3O												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6605
gC3P -> C3P												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6606
gC3S -> C3S												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6607
gC4 -> C4												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6608
gC4H -> C4H												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6609
gHC4H -> HC4H											RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6610
gC4H3 -> C4H3											RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6611
gC4H4 -> C4H4											RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6612
gC4N -> C4N												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6613
gC4P -> C4P												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6614
gC4S -> C4S												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6615
gC5 -> C5												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6616
gC5H -> C5H												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6617
gC5H2 -> C5H2											RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6618
gC5H3 -> C5H3											RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6619
gC5H4 -> C5H4											RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6620
gC5N -> C5N												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6621
gC6 -> C6												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6622
gC6H -> C6H												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6623
gC6H2 -> C6H2											RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6624
gC6H3 -> C6H3											RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6625
gC6H4 -> C6H4											RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6626
gC6H6 -> C6H6											RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6627
gC7 -> C7												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6628
gC7H -> C7H												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6629
gC7H2 -> C7H2											RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6630
gC7H3 -> C7H3											RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6631
gC7H4 -> C7H4											RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6632
gC7N -> C7N												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6633
gC8 -> C8												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6634
gC8H -> C8H												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6635
gC8H2 -> C8H2											RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6636
gC8H3 -> C8H3											RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6637
gC8H4 -> C8H4											RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6638
gC9 -> C9												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6639
gC9H -> C9H												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6640
gC9H2 -> C9H2											RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6641
gC9H3 -> C9H3											RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6642
gC9H4 -> C9H4											RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6643
gC9N -> C9N												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6644
gClC -> ClC												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6645
gCCP -> CCP												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6646
gCH -> CH												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6647
gCH2 -> CH2												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6648
gH2C2N -> H2C2N											RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6649
gCH2CO -> CH2CO											RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6650
gCH2NH -> CH2NH											RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6651
gCH2NH2 -> CH2NH2										RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6652
gCH2OH -> CH2OH											RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6653
gCH2PH -> CH2PH											RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6654
gCH3 -> CH3												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6655
gCH3C3N -> CH3C3N										RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6656
gCH3C4H -> CH3C4H										RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6657
gCH3C5N -> CH3C5N										RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6658
gCH3C6H -> CH3C6H										RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6659
gCH3C7N -> CH3C7N										RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6660
gCH3CHO -> CH3CHO										RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6661
gCH3CN -> CH3CN											RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6662
gCH3CO -> CH3CO											RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6663
gCH3NH -> CH3NH											RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6664
gCH3NH2 -> CH3NH2										RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6665
gCH3O -> CH3O											RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6666
gCH3OCH3 -> CH3OCH3										RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6667
gCH3OH -> CH3OH											RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6668
gCH4 -> CH4												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6669
gHCNH -> HCNH											RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6670
gHCOH -> HCOH											RTYPE=16 K1=0.0 K2=0.0 K3=0.0 # gg08_rnum=6671
gCl -> Cl												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6672
gClO -> ClO												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6673
gCN -> CN												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6674
gCO -> CO												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6675
gCO2 -> CO2												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6676
gCOCHO -> COCHO											RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6677
gCOOH -> COOH											RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6678
gCP -> CP												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6679
gCS -> CS												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6680
gFe -> Fe												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6681
gFeH -> FeH												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6682
gH -> H													RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6683
gH2 -> H2												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6684
gHC3NH -> HC3NH											RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6685
gcH2C3O -> cH2C3O										RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6686
gH2C5N -> H2C5N											RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6687
gH2C7N -> H2C7N											RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6688
gH2C9N -> H2C9N											RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6689
gH2CN -> H2CN											RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6690
gH2CO -> H2CO											RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6691
gH2CS -> H2CS											RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6692
gH2O -> H2O												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6693
gH2O2 -> H2O2											RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6694
gH2S -> H2S												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6695
gH2S2 -> H2S2											RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6696
gH2SiO -> H2SiO											RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6697
gH3C5N -> H3C5N											RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6698
gH3C7N -> H3C7N											RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6699
gH3C9N -> H3C9N											RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6700
gH4C3N -> H4C3N											RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6701
gH5C3N -> H5C3N											RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6702
gHC2NC -> HC2NC											RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6703
gHC2O -> HC2O											RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6704
gHC3N -> HC3N											RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6705
gHC3O -> HC3O											RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6706
gHC5N -> HC5N											RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6707
gHC7N -> HC7N											RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6708
gHC9N -> HC9N											RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6709
gHC2N -> HC2N											RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6710
gHCCP -> HCCP											RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6711
gClH -> ClH												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6712
gHCN -> HCN												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6713
gC2NCH -> C2NCH											RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6714
gHCO -> HCO												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6715
gHCOOCH3 -> HCOOCH3										RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6716
gHCOOH -> HCOOH											RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6717
gHCP -> HCP												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6718
gHCS -> HCS												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6719
gSiCH -> SiCH											RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6720
gHe -> He												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6721
gCH3COCHO -> CH3COCHO									RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6722
gCH3COCH3 -> CH3COCH3									RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6723
gCH3CONH -> CH3CONH										RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6724
gCH3COOH -> CH3COOH										RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6725
gCH3CONH2 -> CH3CONH2									RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6726
gCH3COOCH3 -> CH3COOCH3									RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6727
gCH3COCH2OH -> CH3COCH2OH								RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6728
gHNC -> HNC												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6729
gHNC3 -> HNC3											RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6730
gHNCHO -> HNCHO											RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6731
gHNCO -> HNCO											RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6732
gHNCOCHO -> HNCOCHO										RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6733
gHNCONH -> HNCONH										RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6734
gHNCOOH -> HNCOOH										RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6735
gHNO -> HNO												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6736
gHNOH -> HNOH											RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6737
gHNSi -> HNSi											RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6738
gCH3ONH -> CH3ONH										RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6739
gHNCH2OH -> HNCH2OH										RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6740
gHOC -> HOC												RTYPE=16 K1=0.0 K2=0.0 K3=0.0 # gg08_rnum=6741
gHOCOOH -> HOCOOH										RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6742
gHPO -> HPO												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6743
gHS -> HS												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6744
gS2H -> S2H												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6745
gHCOCOCHO -> HCOCOCHO									RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6746
gMg -> Mg												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6747
gMgH -> MgH												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6748
gMgH2 -> MgH2											RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6749
gN -> N													RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6750
gN2 -> N2												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6751
gN2H2 -> N2H2											RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6752
gN2O -> N2O												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6753
gNa -> Na												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6754
gNaH -> NaH												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6755
gNaOH -> NaOH											RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6756
gNH -> NH												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6757
gNH2 -> NH2												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6758
gNH2CHO -> NH2CHO										RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6759
gNH2CN -> NH2CN											RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6760
gNH2OH -> NH2OH											RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6761
gNH3 -> NH3												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6762
gNO -> NO												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6763
gNO2 -> NO2												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6764
gNS -> NS												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6765
gO -> O													RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6766
gO2 -> O2												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6767
gHO2 -> HO2												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6768
gO3 -> O3												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6769
gOCN -> OCN												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6770
gOCS -> OCS												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6771
gOH -> OH												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6772
gCHOCHO -> CHOCHO										RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6773
gHCOCOOH -> HCOCOOH										RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6774
gP -> P													RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6775
gPH -> PH												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6776
gPH2 -> PH2												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6777
gPN -> PN												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6778
gPO -> PO												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6779
gNH2CO -> NH2CO											RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6780
gNH2COCHO -> NH2COCHO									RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6781
gNH2CONH -> NH2CONH										RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6782
gNH2COOH -> NH2COOH										RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6783
gNH2CONH2 -> NH2CONH2									RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6784
gNH2NH -> NH2NH											RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6785
gNH2NH2 -> NH2NH2										RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6786
gNH2OCH3 -> NH2OCH3										RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6787
gNH2CH2OH -> NH2CH2OH									RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6788
gS -> S													RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6789
gS2 -> S2												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6790
gSi -> Si												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6791
gSiC -> SiC												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6792
gSiC2 -> SiC2											RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6793
gSiC2H -> SiC2H											RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6794
gSiC2H2 -> SiC2H2										RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6795
gSiC3 -> cSiC3											RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6796
gSiC3H -> SiC3H											RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6797
gSiC4 -> SiC4											RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6798
gSiCH2 -> SiCH2											RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6799
gSiCH3 -> SiCH3											RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6800
gSiH -> SiH												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6801
gSiH2 -> SiH2											RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6802
gSiH3 -> SiH3											RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6803
gSiH4 -> SiH4											RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6804
gSiN -> SiN												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6805
gSiNC -> SiNC											RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6806
gSiO -> SiO												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6807
gSiO2 -> SiO2											RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6808
gSiS -> SiS												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6809
gSO -> SO												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6810
gSO2 -> SO2												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6811
gCH3OCO -> CH3OCO										RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6812
gCH3OCOCHO -> CH3OCOCHO									RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6813
gCH3OCONH -> CH3OCONH									RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6814
gCH3OCOOH -> CH3OCOOH									RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6815
gCH3OCONH2 -> CH3OCONH2									RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6816
gCH3OCOOCH3 -> CH3OCOOCH3								RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6817
gCH3OCOCH2OH -> CH3OCOCH2OH								RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6818
gCH3OOH -> CH3OOH										RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6819
gCH3OOCH3 -> CH3OOCH3									RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6820
gCH3OCH2OH -> CH3OCH2OH									RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6821
gCH2OHCHO -> CH2OHCHO									RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6822
gHOCH2CO -> HOCH2CO										RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6823
gHOCH2COCHO -> HOCH2COCHO								RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6824
gHNCOCH2OH -> HNCOCH2OH									RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6825
gCH2OHCOOH -> CH2OHCOOH									RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6826
gNH2COCH2OH -> NH2COCH2OH								RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6827
gHOCH2COCH2OH -> HOCH2COCH2OH							RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6828
gHOCH2OH -> HOCH2OH										RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6829
gHOCH2CH2OH -> HOCH2CH2OH								RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6830
gC10 -> gC9 + gC										RTYPE=17 K1=1.00E+03 K2=0.0 K3=0.0 # gg08_rnum=6831
gC2 -> gC + gC											RTYPE=17 K1=2.37E+02 K2=0.0 K3=0.0 # gg08_rnum=6832
gC2H -> gC2 + gH										RTYPE=17 K1=5.00E+03 K2=0.0 K3=0.0 # gg08_rnum=6833
gHC2H -> gC2H + gH										RTYPE=17 K1=5.15E+03 K2=0.0 K3=0.0 # gg08_rnum=6834
gH2C2H -> gHC2H + gH									RTYPE=17 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=6835
gH2C2H2 -> gHC2H + gH2									RTYPE=17 K1=3.70E+03 K2=0.0 K3=0.0 # gg08_rnum=6836
gCH3CH2 -> gH2C2H2 + gH									RTYPE=17 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=6837
gCH3CH2CHO -> gCH3CH2 + gHCO							RTYPE=17 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=6838
gCH3CH2OH -> gCH3 + gCH2OH								RTYPE=17 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=6839
gCH3CH2OH -> gCH3CH2 + gOH								RTYPE=17 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=6840
gCH3CH3 -> gH2C2H2 + gH2								RTYPE=17 K1=3.00E+03 K2=0.0 K3=0.0 # gg08_rnum=6841
gC2N -> gC + gCN										RTYPE=17 K1=1.00E+03 K2=0.0 K3=0.0 # gg08_rnum=6842
gC2O -> gCO + gC										RTYPE=17 K1=7.50E+02 K2=0.0 K3=0.0 # gg08_rnum=6843
gC2O -> gC2 + gO										RTYPE=17 K1=7.50E+02 K2=0.0 K3=0.0 # gg08_rnum=6844
gC2S -> gCS + gC										RTYPE=17 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=6845
gC3 -> gC2 + gC											RTYPE=17 K1=1.12E+03 K2=0.0 K3=0.0 # gg08_rnum=6846
gC3H -> gC3 + gH										RTYPE=17 K1=1.00E+04 K2=0.0 K3=0.0 # gg08_rnum=6847
gC3H2 -> gC3H + gH										RTYPE=17 K1=1.00E+04 K2=0.0 K3=0.0 # gg08_rnum=6848
gC3H3 -> gC3H2 + gH										RTYPE=17 K1=1.00E+04 K2=0.0 K3=0.0 # gg08_rnum=6849
gC3H3N -> gH2C2H + gCN									RTYPE=17 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=6850
gC3H4 -> gC3H3 + gH										RTYPE=17 K1=3.28E+03 K2=0.0 K3=0.0 # gg08_rnum=6851
gC3N -> gC2 + gCN										RTYPE=17 K1=1.75E+03 K2=0.0 K3=0.0 # gg08_rnum=6852
gC3O -> gC2 + gCO										RTYPE=17 K1=6.60E+03 K2=0.0 K3=0.0 # gg08_rnum=6853
gC3P -> gCCP + gC										RTYPE=17 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=6854
gC3S -> gC2 + gCS										RTYPE=17 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=6855
gC4 -> gC3 + gC											RTYPE=17 K1=1.00E+03 K2=0.0 K3=0.0 # gg08_rnum=6856
gC4H -> gC4 + gH										RTYPE=17 K1=5.00E+03 K2=0.0 K3=0.0 # gg08_rnum=6857
gHC4H -> gC2H + gC2H									RTYPE=17 K1=1.73E+03 K2=0.0 K3=0.0 # gg08_rnum=6858
gHC4H -> gC4H + gH										RTYPE=17 K1=1.73E+03 K2=0.0 K3=0.0 # gg08_rnum=6859
gC4H3 -> gHC4H + gH										RTYPE=17 K1=1.00E+04 K2=0.0 K3=0.0 # gg08_rnum=6860
gC4H4 -> gHC4H + gH2									RTYPE=17 K1=7.50E+03 K2=0.0 K3=0.0 # gg08_rnum=6861
gC4N -> gC3 + gCN										RTYPE=17 K1=1.00E+03 K2=0.0 K3=0.0 # gg08_rnum=6862
gC4P -> gC3P + gC										RTYPE=17 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=6863
gC4S -> gC3 + gCS										RTYPE=17 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=6864
gC5 -> gC4 + gC											RTYPE=17 K1=1.00E+03 K2=0.0 K3=0.0 # gg08_rnum=6865
gC5H -> gC5 + gH										RTYPE=17 K1=5.00E+03 K2=0.0 K3=0.0 # gg08_rnum=6866
gC5H2 -> gC5H + gH										RTYPE=17 K1=1.75E+03 K2=0.0 K3=0.0 # gg08_rnum=6867
gC5H3 -> gC5H2 + gH										RTYPE=17 K1=1.00E+04 K2=0.0 K3=0.0 # gg08_rnum=6868
gC5H4 -> gC5H2 + gH2									RTYPE=17 K1=7.50E+03 K2=0.0 K3=0.0 # gg08_rnum=6869
gC5N -> gC4 + gCN										RTYPE=17 K1=1.75E+03 K2=0.0 K3=0.0 # gg08_rnum=6870
gC6 -> gC5 + gC											RTYPE=17 K1=1.00E+03 K2=0.0 K3=0.0 # gg08_rnum=6871
gC6H -> gC6 + gH										RTYPE=17 K1=5.00E+03 K2=0.0 K3=0.0 # gg08_rnum=6872
gC6H2 -> gC6H + gH										RTYPE=17 K1=1.75E+03 K2=0.0 K3=0.0 # gg08_rnum=6873
gC6H3 -> gC6H2 + gH										RTYPE=17 K1=1.00E+04 K2=0.0 K3=0.0 # gg08_rnum=6874
gC6H4 -> gC6H2 + gH2									RTYPE=17 K1=7.50E+03 K2=0.0 K3=0.0 # gg08_rnum=6875
gC6H6 -> gC6H4 + gH2									RTYPE=17 K1=3.00E+03 K2=0.0 K3=0.0 # gg08_rnum=6876
gC7 -> gC6 + gC											RTYPE=17 K1=1.00E+03 K2=0.0 K3=0.0 # gg08_rnum=6877
gC7H -> gC7 + gH										RTYPE=17 K1=5.00E+03 K2=0.0 K3=0.0 # gg08_rnum=6878
gC7H2 -> gC7H + gH										RTYPE=17 K1=1.75E+03 K2=0.0 K3=0.0 # gg08_rnum=6879
gC7H3 -> gC7H2 + gH										RTYPE=17 K1=1.00E+04 K2=0.0 K3=0.0 # gg08_rnum=6880
gC7H4 -> gC7H2 + gH2									RTYPE=17 K1=7.50E+03 K2=0.0 K3=0.0 # gg08_rnum=6881
gC7N -> gC6 + gCN										RTYPE=17 K1=1.75E+03 K2=0.0 K3=0.0 # gg08_rnum=6882
gC8 -> gC7 + gC											RTYPE=17 K1=1.00E+03 K2=0.0 K3=0.0 # gg08_rnum=6883
gC8H -> gC8 + gH										RTYPE=17 K1=5.00E+03 K2=0.0 K3=0.0 # gg08_rnum=6884
gC8H2 -> gC8H + gH										RTYPE=17 K1=1.75E+03 K2=0.0 K3=0.0 # gg08_rnum=6885
gC8H3 -> gC8H2 + gH										RTYPE=17 K1=1.00E+04 K2=0.0 K3=0.0 # gg08_rnum=6886
gC8H4 -> gC8H2 + gH2									RTYPE=17 K1=7.50E+03 K2=0.0 K3=0.0 # gg08_rnum=6887
gC9 -> gC8 + gC											RTYPE=17 K1=1.00E+03 K2=0.0 K3=0.0 # gg08_rnum=6888
gC9H -> gC9 + gH										RTYPE=17 K1=5.00E+03 K2=0.0 K3=0.0 # gg08_rnum=6889
gC9H2 -> gC9H + gH										RTYPE=17 K1=1.75E+03 K2=0.0 K3=0.0 # gg08_rnum=6890
gC9H3 -> gC9H2 + gH										RTYPE=17 K1=1.00E+04 K2=0.0 K3=0.0 # gg08_rnum=6891
gC9H4 -> gC9H2 + gH2									RTYPE=17 K1=7.50E+03 K2=0.0 K3=0.0 # gg08_rnum=6892
gC9N -> gC8 + gCN										RTYPE=17 K1=1.75E+03 K2=0.0 K3=0.0 # gg08_rnum=6893
gClC -> gC + gCl										RTYPE=17 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=6894
gCCP -> gC2 + gP										RTYPE=17 K1=7.50E+02 K2=0.0 K3=0.0 # gg08_rnum=6895
gCCP -> gCP + gC										RTYPE=17 K1=7.50E+02 K2=0.0 K3=0.0 # gg08_rnum=6896
gCH -> gC + gH											RTYPE=17 K1=7.30E+02 K2=0.0 K3=0.0 # gg08_rnum=6897
gCH2 -> gCH + gH										RTYPE=17 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=6898
gH2C2N -> gCH2 + gCN									RTYPE=17 K1=5.00E+03 K2=0.0 K3=0.0 # gg08_rnum=6899
gCH2CO -> gCH2 + gCO									RTYPE=17 K1=9.15E+02 K2=0.0 K3=0.0 # gg08_rnum=6900
gCH2NH -> gCH2 + gNH									RTYPE=17 K1=4.98E+03 K2=0.0 K3=0.0 # gg08_rnum=6901
gCH2NH2 -> gCH2 + gNH2									RTYPE=17 K1=9.50E+03 K2=0.0 K3=0.0 # gg08_rnum=6902
gCH2OH -> gCH2 + gOH									RTYPE=17 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=6903
gCH2PH -> gCH2 + gPH									RTYPE=17 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=6904
gCH3 -> gCH2 + gH										RTYPE=17 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=6905
gCH3C3N -> gCH3 + gC3N									RTYPE=17 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=6906
gCH3C4H -> gCH3 + gC4H									RTYPE=17 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=6907
gCH3C5N -> gCH3 + gC5N									RTYPE=17 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=6908
gCH3C6H -> gCH3 + gC6H									RTYPE=17 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=6909
gCH3C7N -> gCH3 + gC7N									RTYPE=17 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=6910
gCH3CHO -> gCH3 + gHCO									RTYPE=17 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=6911
gCH3CN -> gCH3 + gCN									RTYPE=17 K1=4.76E+03 K2=0.0 K3=0.0 # gg08_rnum=6912
gCH3CO -> gCH3 + gCO									RTYPE=17 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=6913
gCH3NH -> gCH2NH + gH									RTYPE=17 K1=9.50E+03 K2=0.0 K3=0.0 # gg08_rnum=6914
gCH3NH -> gCH3 + gNH									RTYPE=17 K1=1.00E+04 K2=0.0 K3=0.0 # gg08_rnum=6915
gCH3NH2 -> gCH3 + gNH2									RTYPE=17 K1=1.00E+04 K2=0.0 K3=0.0 # gg08_rnum=6916
gCH3O -> gCH3 + gO										RTYPE=17 K1=3.00E+03 K2=0.0 K3=0.0 # gg08_rnum=6917
gCH3OCH3 -> gCH3O + gCH3								RTYPE=17 K1=1.72E+03 K2=0.0 K3=0.0 # gg08_rnum=6918
gCH3OH -> gCH3 + gOH									RTYPE=17 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=6919
gCH3OH -> gCH2OH + gH									RTYPE=17 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=6920
gCH3OH -> gCH3O + gH									RTYPE=17 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=6921
gCH4 -> gCH2 + gH2										RTYPE=17 K1=2.34E+03 K2=0.0 K3=0.0 # gg08_rnum=6922
gHCNH -> gCH + gNH										RTYPE=17 K1=1.00E+03 K2=0.0 K3=0.0 # gg08_rnum=6923
gHCOH -> gCH + gOH										RTYPE=17 K1=0.0 K2=0.0 K3=0.0 # gg08_rnum=6924
gClO -> gCl + gO										RTYPE=17 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=6925
gCN -> gC + gN											RTYPE=17 K1=1.06E+04 K2=0.0 K3=0.0 # gg08_rnum=6926
gCO -> gC + gO											RTYPE=17 K1=5.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6927
gCO2 -> gCO + gO										RTYPE=17 K1=1.71E+03 K2=0.0 K3=0.0 # gg08_rnum=6928
gCOCHO -> gHCO + gCO									RTYPE=17 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=6929
gCOOH -> gCO + gOH										RTYPE=17 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=6930
gCP -> gC + gP											RTYPE=17 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=6931
gCS -> gC + gS											RTYPE=17 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=6932
gFeH -> gFe + gH										RTYPE=17 K1=1.00E+03 K2=0.0 K3=0.0 # gg08_rnum=6933
gHC3NH -> gHC2H + gCN									RTYPE=17 K1=3.00E+03 K2=0.0 K3=0.0 # gg08_rnum=6934
gcH2C3O -> gHC2H + gCO									RTYPE=17 K1=1.80E+03 K2=0.0 K3=0.0 # gg08_rnum=6935
gH2C5N -> gHC4H + gCN									RTYPE=17 K1=3.00E+03 K2=0.0 K3=0.0 # gg08_rnum=6936
gH2C7N -> gC6H2 + gCN									RTYPE=17 K1=3.00E+03 K2=0.0 K3=0.0 # gg08_rnum=6937
gH2C9N -> gC8H2 + gCN									RTYPE=17 K1=3.00E+03 K2=0.0 K3=0.0 # gg08_rnum=6938
gH2CN -> gHCN + gH										RTYPE=17 K1=1.00E+03 K2=0.0 K3=0.0 # gg08_rnum=6939
gH2CO -> gHCO + gH										RTYPE=17 K1=1.33E+03 K2=0.0 K3=0.0 # gg08_rnum=6940
gH2CO -> gCO + gH2										RTYPE=17 K1=1.33E+03 K2=0.0 K3=0.0 # gg08_rnum=6941
gH2CS -> gHCS + gH										RTYPE=17 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=6942
gH2O -> gOH + gH										RTYPE=17 K1=9.70E+02 K2=0.0 K3=0.0 # gg08_rnum=6943
gH2O2 -> gOH + gOH										RTYPE=17 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=6944
gH2S -> gH2 + gS										RTYPE=17 K1=5.15E+03 K2=0.0 K3=0.0 # gg08_rnum=6945
gH2S2 -> gHS + gHS										RTYPE=17 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=6946
gH2SiO -> gSiO + gH2									RTYPE=17 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=6947
gH3C5N -> gH2C2H + gC3N									RTYPE=17 K1=3.00E+03 K2=0.0 K3=0.0 # gg08_rnum=6948
gH3C7N -> gH2C2H + gC5N									RTYPE=17 K1=3.00E+03 K2=0.0 K3=0.0 # gg08_rnum=6949
gH3C9N -> gH2C2H + gC7N									RTYPE=17 K1=3.00E+03 K2=0.0 K3=0.0 # gg08_rnum=6950
gH4C3N -> gH2C2H2 + gCN									RTYPE=17 K1=3.00E+03 K2=0.0 K3=0.0 # gg08_rnum=6951
gH5C3N -> gCH3CH2 + gCN									RTYPE=17 K1=3.00E+03 K2=0.0 K3=0.0 # gg08_rnum=6952
gHC2NC -> gC2H + gCN									RTYPE=17 K1=3.45E+03 K2=0.0 K3=0.0 # gg08_rnum=6953
gHC2O -> gCO + gCH										RTYPE=17 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=6954
gHC3N -> gC2H + gCN										RTYPE=17 K1=1.72E+03 K2=0.0 K3=0.0 # gg08_rnum=6955
gHC3O -> gCO + gC2H										RTYPE=17 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=6956
gHC5N -> gC4H + gCN										RTYPE=17 K1=1.75E+03 K2=0.0 K3=0.0 # gg08_rnum=6957
gHC7N -> gC6H + gCN										RTYPE=17 K1=1.75E+03 K2=0.0 K3=0.0 # gg08_rnum=6958
gHC9N -> gC8H + gCN										RTYPE=17 K1=1.75E+03 K2=0.0 K3=0.0 # gg08_rnum=6959
gHC2N -> gC2N + gH										RTYPE=17 K1=1.00E+04 K2=0.0 K3=0.0 # gg08_rnum=6960
gHCCP -> gCCP + gH										RTYPE=17 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=6961
gClH -> gH + gCl										RTYPE=17 K1=6.10E+02 K2=0.0 K3=0.0 # gg08_rnum=6962
gHCN -> gCN + gH										RTYPE=17 K1=3.12E+03 K2=0.0 K3=0.0 # gg08_rnum=6963
gC2NCH -> gC2H + gCN									RTYPE=17 K1=3.45E+03 K2=0.0 K3=0.0 # gg08_rnum=6964
gHCO -> gCO + gH										RTYPE=17 K1=4.21E+02 K2=0.0 K3=0.0 # gg08_rnum=6965
gHCOOCH3 -> gHCO + gCH3O								RTYPE=17 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=6966
gHCOOH -> gHCO + gOH									RTYPE=17 K1=2.49E+02 K2=0.0 K3=0.0 # gg08_rnum=6967
gHCP -> gCP + gH										RTYPE=17 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=6968
gHCS -> gCH + gS										RTYPE=17 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=6969
gHCS -> gCS + gH										RTYPE=17 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=6970
gSiCH -> gCH + gSi										RTYPE=17 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=6971
gCH3COCHO -> gCH3CO + gHCO								RTYPE=17 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=6972
gCH3COCH3 -> gCH3CO + gCH3								RTYPE=17 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=6973
gCH3CONH -> gCH3CO + gNH								RTYPE=17 K1=1.00E+04 K2=0.0 K3=0.0 # gg08_rnum=6974
gCH3CONH -> gCH3 + gHNCO								RTYPE=17 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=6975
gCH3COOH -> gCH3 + gCOOH								RTYPE=17 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=6976
gCH3COOH -> gCH3CO + gOH								RTYPE=17 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=6977
gCH3CONH2 -> gCH3 + gNH2CO								RTYPE=17 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=6978
gCH3CONH2 -> gCH3CO + gNH2								RTYPE=17 K1=1.00E+04 K2=0.0 K3=0.0 # gg08_rnum=6979
gCH3COOCH3 -> gCH3CO + gCH3O							RTYPE=17 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=6980
gCH3COOCH3 -> gCH3 + gCH3OCO							RTYPE=17 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=6981
gCH3COCH2OH -> gCH3 + gHOCH2CO							RTYPE=17 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=6982
gCH3COCH2OH -> gCH3CO + gCH2OH							RTYPE=17 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=6983
gHNC -> gCN + gH										RTYPE=17 K1=3.00E+03 K2=0.0 K3=0.0 # gg08_rnum=6984
gHNC3 -> gC2H + gCN										RTYPE=17 K1=3.45E+03 K2=0.0 K3=0.0 # gg08_rnum=6985
gHNCHO -> gNH + gHCO									RTYPE=17 K1=1.00E+04 K2=0.0 K3=0.0 # gg08_rnum=6986
gHNCO -> gNH + gCO										RTYPE=17 K1=6.00E+03 K2=0.0 K3=0.0 # gg08_rnum=6987
gHNCOCHO -> gHNCO + gHCO								RTYPE=17 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=6988
gHNCOCHO -> gNH + gCOCHO								RTYPE=17 K1=1.00E+04 K2=0.0 K3=0.0 # gg08_rnum=6989
gHNCONH -> gNH + gHNCO									RTYPE=17 K1=1.00E+04 K2=0.0 K3=0.0 # gg08_rnum=6990
gHNCOOH -> gCOOH + gNH									RTYPE=17 K1=1.00E+04 K2=0.0 K3=0.0 # gg08_rnum=6991
gHNCOOH -> gOH + gHNCO									RTYPE=17 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=6992
gHNO -> gNH + gO										RTYPE=17 K1=1.00E+03 K2=0.0 K3=0.0 # gg08_rnum=6993
gHNO -> gNO + gH										RTYPE=17 K1=1.00E+03 K2=0.0 K3=0.0 # gg08_rnum=6994
gHNOH -> gNH + gOH										RTYPE=17 K1=1.00E+03 K2=0.0 K3=0.0 # gg08_rnum=6995
gHNSi -> gSiN + gH										RTYPE=17 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=6996
gCH3ONH -> gNH + gCH3O									RTYPE=17 K1=1.00E+03 K2=0.0 K3=0.0 # gg08_rnum=6997
gHNCH2OH -> gNH + gCH2OH								RTYPE=17 K1=1.00E+04 K2=0.0 K3=0.0 # gg08_rnum=6998
gHOC -> gCO + gH										RTYPE=17 K1=0.0 K2=0.0 K3=0.0 # gg08_rnum=6999
gHOCOOH -> gOH + gCOOH									RTYPE=17 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=7000
gHPO -> gPO + gH										RTYPE=17 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=7001
gHS -> gH + gS											RTYPE=17 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=7002
gS2H -> gHS + gS										RTYPE=17 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=7003
gHCOCOCHO -> gHCO + gCOCHO								RTYPE=17 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=7004
gMgH -> gMg + gH										RTYPE=17 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=7005
gMgH2 -> gMgH + gH										RTYPE=17 K1=3.00E+03 K2=0.0 K3=0.0 # gg08_rnum=7006
gN2 -> gN + gN											RTYPE=17 K1=5.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7007
gN2H2 -> gNH + gNH										RTYPE=17 K1=2.00E+02 K2=0.0 K3=0.0 # gg08_rnum=7008
gN2O -> gNO + gN										RTYPE=17 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=7009
gNaH -> gNa + gH										RTYPE=17 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=7010
gNaOH -> gNa + gOH										RTYPE=17 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=7011
gNH -> gN + gH											RTYPE=17 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=7012
gNH2 -> gNH + gH										RTYPE=17 K1=8.00E+01 K2=0.0 K3=0.0 # gg08_rnum=7013
gNH2CHO -> gNH2 + gHCO									RTYPE=17 K1=3.00E+03 K2=0.0 K3=0.0 # gg08_rnum=7014
gNH2CN -> gNH2 + gCN									RTYPE=17 K1=9.50E+03 K2=0.0 K3=0.0 # gg08_rnum=7015
gNH2OH -> gNH2 + gOH									RTYPE=17 K1=3.00E+03 K2=0.0 K3=0.0 # gg08_rnum=7016
gNH3 -> gNH + gH2										RTYPE=17 K1=5.40E+02 K2=0.0 K3=0.0 # gg08_rnum=7017
gNH3 -> gNH2 + gH										RTYPE=17 K1=1.32E+03 K2=0.0 K3=0.0 # gg08_rnum=7018
gNO -> gN + gO											RTYPE=17 K1=4.82E+02 K2=0.0 K3=0.0 # gg08_rnum=7019
gNO2 -> gNO + gO										RTYPE=17 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=7020
gNS -> gN + gS											RTYPE=17 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=7021
gO2 -> gO + gO											RTYPE=17 K1=7.50E+02 K2=0.0 K3=0.0 # gg08_rnum=7022
gHO2 -> gO + gOH										RTYPE=17 K1=7.50E+02 K2=0.0 K3=0.0 # gg08_rnum=7023
gHO2 -> gO2 + gH										RTYPE=17 K1=7.50E+02 K2=0.0 K3=0.0 # gg08_rnum=7024
gO3 -> gO2 + gO											RTYPE=17 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=7025
gOCN -> gCN + gO										RTYPE=17 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=7026
gOCS -> gCO + gS										RTYPE=17 K1=5.35E+03 K2=0.0 K3=0.0 # gg08_rnum=7027
gOH -> gO + gH											RTYPE=17 K1=5.10E+02 K2=0.0 K3=0.0 # gg08_rnum=7028
gCHOCHO -> gHCO + gHCO									RTYPE=17 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=7029
gHCOCOOH -> gHCO + gCOOH								RTYPE=17 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=7030
gHCOCOOH -> gCOCHO + gOH								RTYPE=17 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=7031
gPH -> gP + gH											RTYPE=17 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=7032
gPH2 -> gPH + gH										RTYPE=17 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=7033
gPN -> gP + gN											RTYPE=17 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=7034
gPO -> gP + gO											RTYPE=17 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=7035
gNH2CO -> gNH2 + gCO									RTYPE=17 K1=1.00E+04 K2=0.0 K3=0.0 # gg08_rnum=7036
gNH2COCHO -> gNH2 + gCOCHO								RTYPE=17 K1=1.00E+04 K2=0.0 K3=0.0 # gg08_rnum=7037
gNH2COCHO -> gNH2CO + gHCO								RTYPE=17 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=7038
gNH2CONH -> gNH2 + gHNCO								RTYPE=17 K1=1.00E+04 K2=0.0 K3=0.0 # gg08_rnum=7039
gNH2CONH -> gNH2CO + gNH								RTYPE=17 K1=1.00E+04 K2=0.0 K3=0.0 # gg08_rnum=7040
gNH2COOH -> gNH2CO + gOH								RTYPE=17 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=7041
gNH2COOH -> gNH2 + gCOOH								RTYPE=17 K1=1.00E+04 K2=0.0 K3=0.0 # gg08_rnum=7042
gNH2CONH2 -> gNH2 + gNH2CO								RTYPE=17 K1=1.00E+04 K2=0.0 K3=0.0 # gg08_rnum=7043
gNH2NH -> gNH + gNH2									RTYPE=17 K1=2.00E+02 K2=0.0 K3=0.0 # gg08_rnum=7044
gNH2NH2 -> gNH2 + gNH2									RTYPE=17 K1=2.00E+02 K2=0.0 K3=0.0 # gg08_rnum=7045
gNH2OCH3 -> gNH2 + gCH3O								RTYPE=17 K1=1.00E+03 K2=0.0 K3=0.0 # gg08_rnum=7046
gNH2CH2OH -> gNH2 + gCH2OH								RTYPE=17 K1=1.00E+04 K2=0.0 K3=0.0 # gg08_rnum=7047
gS2 -> gS + gS											RTYPE=17 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=7048
gSiC -> gSi + gC										RTYPE=17 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=7049
gSiC2 -> gSiC + gC										RTYPE=17 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=7050
gSiC2H -> gSiC2 + gH									RTYPE=17 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=7051
gSiC2H2 -> gSiC2 + gH2									RTYPE=17 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=7052
gSiC3 -> gSiC2 + gC										RTYPE=17 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=7053
gSiC3H -> gSiC3 + gH									RTYPE=17 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=7054
gSiC4 -> gSiC2 + gC2									RTYPE=17 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=7055
gSiCH2 -> gSiC + gH2									RTYPE=17 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=7056
gSiCH3 -> gSiCH2 + gH									RTYPE=17 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=7057
gSiH -> gSi + gH										RTYPE=17 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=7058
gSiH2 -> gSiH + gH										RTYPE=17 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=7059
gSiH3 -> gSiH2 + gH										RTYPE=17 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=7060
gSiH4 -> gSiH2 + gH2									RTYPE=17 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=7061
gSiN -> gSi + gN										RTYPE=17 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=7062
gSiNC -> gSi + gCN										RTYPE=17 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=7063
gSiO -> gSi + gO										RTYPE=17 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=7064
gSiO2 -> gSiO + gO										RTYPE=17 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=7065
gSiS -> gSi + gS										RTYPE=17 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=7066
gSO -> gS + gO											RTYPE=17 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=7067
gSO2 -> gSO + gO										RTYPE=17 K1=1.88E+03 K2=0.0 K3=0.0 # gg08_rnum=7068
gCH3OCO -> gCH3O + gCO									RTYPE=17 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=7069
gCH3OCOCHO -> gCH3O + gCOCHO							RTYPE=17 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=7070
gCH3OCOCHO -> gCH3OCO + gHCO							RTYPE=17 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=7071
gCH3OCONH -> gCH3O + gHNCO								RTYPE=17 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=7072
gCH3OCONH -> gCH3OCO + gNH								RTYPE=17 K1=1.00E+04 K2=0.0 K3=0.0 # gg08_rnum=7073
gCH3OCOOH -> gCH3O + gCOOH								RTYPE=17 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=7074
gCH3OCOOH -> gCH3OCO + gOH								RTYPE=17 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=7075
gCH3OCONH2 -> gNH2 + gCH3OCO							RTYPE=17 K1=1.00E+04 K2=0.0 K3=0.0 # gg08_rnum=7076
gCH3OCONH2 -> gNH2CO + gCH3O							RTYPE=17 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=7077
gCH3OCOOCH3 -> gCH3O + gCH3OCO							RTYPE=17 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=7078
gCH3OCOCH2OH -> gCH3O + gHOCH2CO						RTYPE=17 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=7079
gCH3OCOCH2OH -> gCH3OCO + gCH2OH						RTYPE=17 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=7080
gCH3OOH -> gOH + gCH3O									RTYPE=17 K1=1.00E+03 K2=0.0 K3=0.0 # gg08_rnum=7081
gCH3OOCH3 -> gCH3O + gCH3O								RTYPE=17 K1=1.00E+03 K2=0.0 K3=0.0 # gg08_rnum=7082
gCH3OCH2OH -> gCH3O + gCH2OH							RTYPE=17 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=7083
gCH2OHCHO -> gCH2OH + gHCO								RTYPE=17 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=7084
gHOCH2CO -> gCH2OH + gCO								RTYPE=17 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=7085
gHOCH2COCHO -> gHOCH2CO + gHCO							RTYPE=17 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=7086
gHOCH2COCHO -> gCH2OH + gCOCHO							RTYPE=17 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=7087
gHNCOCH2OH -> gHOCH2CO + gNH							RTYPE=17 K1=1.00E+04 K2=0.0 K3=0.0 # gg08_rnum=7088
gHNCOCH2OH -> gCH2OH + gHNCO							RTYPE=17 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=7089
gCH2OHCOOH -> gHOCH2CO + gOH							RTYPE=17 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=7090
gCH2OHCOOH -> gCH2OH + gCOOH							RTYPE=17 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=7091
gNH2COCH2OH -> gNH2 + gHOCH2CO							RTYPE=17 K1=1.00E+04 K2=0.0 K3=0.0 # gg08_rnum=7092
gNH2COCH2OH -> gNH2CO + gCH2OH							RTYPE=17 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=7093
gHOCH2COCH2OH -> gCH2OH + gHOCH2CO						RTYPE=17 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=7094
gHOCH2OH -> gOH + gCH2OH								RTYPE=17 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=7095
gHOCH2CH2OH -> gCH2OH + gCH2OH							RTYPE=17 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=7096
gHC2H -> gC2H + gH										RTYPE=18 K1=7.10E+02 K2=0.0 K3=0.0 # gg08_rnum=7097
gHC2H -> gC2 + gH + gH									RTYPE=18 K1=4.16E+02 K2=0.0 K3=0.0 # gg08_rnum=7098
gHC2H -> gCH + gCH										RTYPE=18 K1=1.87E+02 K2=0.0 K3=0.0 # gg08_rnum=7099
gH2C2H2 -> gHC2H + gH + gH								RTYPE=18 K1=6.20E+02 K2=0.0 K3=0.0 # gg08_rnum=7100
gH2C2H2 -> gHC2H + gH2									RTYPE=18 K1=5.63E+01 K2=0.0 K3=0.0 # gg08_rnum=7101
gH2C2H2 -> gH2C2H + gH									RTYPE=18 K1=1.03E+02 K2=0.0 K3=0.0 # gg08_rnum=7102
gCH3CH2OH -> gCH3 + gCH2 + gOH							RTYPE=18 K1=1.92E+03 K2=0.0 K3=0.0 # gg08_rnum=7103
gCH3CH2OH -> gH2C2H2 + gOH + gH							RTYPE=18 K1=2.74E+02 K2=0.0 K3=0.0 # gg08_rnum=7104
gCH3CH2OH -> gCH2 + gCH2OH + gH							RTYPE=18 K1=2.74E+02 K2=0.0 K3=0.0 # gg08_rnum=7105
gCH3CH2OH -> gCH3CH2 + gOH								RTYPE=18 K1=1.37E+02 K2=0.0 K3=0.0 # gg08_rnum=7106
gCH3CH2OH -> gCH3 + gCH2OH								RTYPE=18 K1=1.37E+02 K2=0.0 K3=0.0 # gg08_rnum=7107
gC3H4 -> gC3H2 + gH2									RTYPE=18 K1=2.92E+02 K2=0.0 K3=0.0 # gg08_rnum=7108
gC3H4 -> gC3H3 + gH										RTYPE=18 K1=5.01E+03 K2=0.0 K3=0.0 # gg08_rnum=7109
gHC4H -> gC4 + gH2										RTYPE=18 K1=5.60E+02 K2=0.0 K3=0.0 # gg08_rnum=7110
gHC4H -> gC4H + gH										RTYPE=18 K1=5.60E+02 K2=0.0 K3=0.0 # gg08_rnum=7111
gCH2 -> gC + gH2										RTYPE=18 K1=6.04E+01 K2=0.0 K3=0.0 # gg08_rnum=7112
gCH2 -> gCH + gH										RTYPE=18 K1=1.26E+02 K2=0.0 K3=0.0 # gg08_rnum=7113
gCH2 -> gC + gH + gH									RTYPE=18 K1=3.14E+02 K2=0.0 K3=0.0 # gg08_rnum=7114
gCH2CO -> gC2 + gH2O									RTYPE=18 K1=4.07E+02 K2=0.0 K3=0.0 # gg08_rnum=7115
gCH2CO -> gCH2 + gCO									RTYPE=18 K1=4.07E+02 K2=0.0 K3=0.0 # gg08_rnum=7116
gCH2CO -> gHC2H + gO									RTYPE=18 K1=4.07E+02 K2=0.0 K3=0.0 # gg08_rnum=7117
gCH3 -> gC + gH2 + gH									RTYPE=18 K1=1.50E+02 K2=0.0 K3=0.0 # gg08_rnum=7118
gCH3 -> gCH + gH + gH									RTYPE=18 K1=8.00E+01 K2=0.0 K3=0.0 # gg08_rnum=7119
gCH3 -> gCH + gH2										RTYPE=18 K1=7.00E+01 K2=0.0 K3=0.0 # gg08_rnum=7120
gCH3 -> gCH2 + gH										RTYPE=18 K1=2.00E+02 K2=0.0 K3=0.0 # gg08_rnum=7121
gCH3CHO -> gCH3 + gHCO									RTYPE=18 K1=1.12E+03 K2=0.0 K3=0.0 # gg08_rnum=7122
gCH3CN -> gCH3 + gCN									RTYPE=18 K1=4.98E+02 K2=0.0 K3=0.0 # gg08_rnum=7123
gCH3CN -> gH2C2N + gH									RTYPE=18 K1=4.98E+02 K2=0.0 K3=0.0 # gg08_rnum=7124
gCH3CN -> gC2N + gH2 + gH								RTYPE=18 K1=4.98E+02 K2=0.0 K3=0.0 # gg08_rnum=7125
gCH3CN -> gCH2 + gHCN									RTYPE=18 K1=7.47E+02 K2=0.0 K3=0.0 # gg08_rnum=7126
gCH3NH2 -> gCH3 + gNH2									RTYPE=18 K1=5.06E+02 K2=0.0 K3=0.0 # gg08_rnum=7127
gCH3NH2 -> gCH2NH + gH2									RTYPE=18 K1=5.06E+02 K2=0.0 K3=0.0 # gg08_rnum=7128
gCH3OCH3 -> gCH3O + gCH3								RTYPE=18 K1=1.13E+03 K2=0.0 K3=0.0 # gg08_rnum=7129
gCH3OH -> gCH2 + gOH + gH								RTYPE=18 K1=2.48E+01 K2=0.0 K3=0.0 # gg08_rnum=7130
gCH3OH -> gH2CO + gH + gH								RTYPE=18 K1=4.13E+01 K2=0.0 K3=0.0 # gg08_rnum=7131
gCH3OH -> gCH3 + gOH									RTYPE=18 K1=1.44E+03 K2=0.0 K3=0.0 # gg08_rnum=7132
gCH3OH -> gCO + gH2 + gH + gH							RTYPE=18 K1=1.65E+01 K2=0.0 K3=0.0 # gg08_rnum=7133
gCH3OH -> gHCO + gH + gH + gH							RTYPE=18 K1=1.65E+01 K2=0.0 K3=0.0 # gg08_rnum=7134
gCO -> gO + gC											RTYPE=18 K1=3.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7135
gH2S -> gS + gH + gH									RTYPE=18 K1=8.50E+02 K2=0.0 K3=0.0 # gg08_rnum=7136
gH2S -> gHS + gH										RTYPE=18 K1=8.50E+02 K2=0.0 K3=0.0 # gg08_rnum=7137
gHCO -> gCO + gH										RTYPE=18 K1=1.17E+03 K2=0.0 K3=0.0 # gg08_rnum=7138
gHCOOH -> gCO2 + gH + gH								RTYPE=18 K1=6.50E+02 K2=0.0 K3=0.0 # gg08_rnum=7139
gHCS -> gCS + gH										RTYPE=18 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=7140
gHNO -> gNO + gH										RTYPE=18 K1=1.00E+03 K2=0.0 K3=0.0 # gg08_rnum=7141
gNH2 -> gNH + gH										RTYPE=18 K1=2.17E+02 K2=0.0 K3=0.0 # gg08_rnum=7142
gNH2 -> gN + gH + gH									RTYPE=18 K1=4.33E+02 K2=0.0 K3=0.0 # gg08_rnum=7143
gNH3 -> gNH2 + gH										RTYPE=18 K1=2.88E+02 K2=0.0 K3=0.0 # gg08_rnum=7144
gNH3 -> gNH + gH + gH									RTYPE=18 K1=2.88E+02 K2=0.0 K3=0.0 # gg08_rnum=7145
gNO -> gN + gO											RTYPE=18 K1=4.94E+02 K2=0.0 K3=0.0 # gg08_rnum=7146
gO2 -> gO + gO											RTYPE=18 K1=1.17E+02 K2=0.0 K3=0.0 # gg08_rnum=7147
gOCS -> gCS + gO										RTYPE=18 K1=4.80E+02 K2=0.0 K3=0.0 # gg08_rnum=7148
gOCS -> gCO + gS										RTYPE=18 K1=9.60E+02 K2=0.0 K3=0.0 # gg08_rnum=7149
gC10 -> gC9 + gC										RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7150
gC2 -> gC + gC											RTYPE=19 K1=4.70E-11 K2=0.0 K3=2.60E+00 # gg08_rnum=7151
gC2H -> gC2 + gH										RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7152
gHC2H -> gC2H + gH										RTYPE=19 K1=1.81E-09 K2=0.0 K3=1.72E+00 # gg08_rnum=7153
gH2C2H -> gHC2H + gH									RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7154
gH2C2H2 -> gHC2H + gH2									RTYPE=19 K1=1.62E-09 K2=0.0 K3=1.61E+00 # gg08_rnum=7155
gCH3CH2 -> gH2C2H2 + gH									RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7156
gCH3CH2CHO -> gCH3CH2 + gHCO							RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7157
gCH3CH2OH -> gCH3CH2 + gOH								RTYPE=19 K1=1.38E-09 K2=0.0 K3=1.73E+00 # gg08_rnum=7158
gCH3CH3 -> gH2C2H2 + gH2								RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7159
gC2N -> gC2 + gN										RTYPE=19 K1=1.00E-10 K2=0.0 K3=1.70E+00 # gg08_rnum=7160
gC2N -> gCN + gC										RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7161
gC2O -> gCO + gC										RTYPE=19 K1=5.00E-10 K2=0.0 K3=1.70E+00 # gg08_rnum=7162
gC2O -> gC2 + gO										RTYPE=19 K1=5.00E-10 K2=0.0 K3=1.70E+00 # gg08_rnum=7163
gC2S -> gC2 + gS										RTYPE=19 K1=1.00E-10 K2=0.0 K3=2.00E+00 # gg08_rnum=7164
gC3 -> gC2 + gC											RTYPE=19 K1=2.60E-10 K2=0.0 K3=2.28E+00 # gg08_rnum=7165
gC3H -> gC3 + gH										RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7166
gC3H2 -> gC3 + gH2										RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7167
gC3H2 -> gC3H + gH										RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7168
gC3H3 -> gC3H2 + gH										RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7169
gC3H3 -> gC3H + gH2										RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7170
gC3H3N -> gH2C2H + gCN									RTYPE=19 K1=1.00E-10 K2=0.0 K3=1.70E+00 # gg08_rnum=7171
gC3H4 -> gC3H2 + gH2									RTYPE=19 K1=2.25E-10 K2=0.0 K3=1.69E+00 # gg08_rnum=7172
gC3H4 -> gC3H3 + gH										RTYPE=19 K1=1.84E-09 K2=0.0 K3=1.72E+00 # gg08_rnum=7173
gC3N -> gC2 + gCN										RTYPE=19 K1=5.00E-10 K2=0.0 K3=1.80E+00 # gg08_rnum=7174
gC3O -> gC2 + gCO										RTYPE=19 K1=4.52E-09 K2=0.0 K3=1.58E+00 # gg08_rnum=7175
gC3P -> gCCP + gC										RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7176
gC3S -> gC2 + gCS										RTYPE=19 K1=1.00E-10 K2=0.0 K3=2.00E+00 # gg08_rnum=7177
gC4 -> gC2 + gC2										RTYPE=19 K1=2.00E-10 K2=0.0 K3=2.30E+00 # gg08_rnum=7178
gC4 -> gC3 + gC											RTYPE=19 K1=2.00E-10 K2=0.0 K3=2.30E+00 # gg08_rnum=7179
gC4H -> gC2H + gC2										RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7180
gC4H -> gC4 + gH										RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7181
gHC4H -> gC2H + gC2H									RTYPE=19 K1=1.13E-09 K2=0.0 K3=1.64E+00 # gg08_rnum=7182
gHC4H -> gC4H + gH										RTYPE=19 K1=1.13E-09 K2=0.0 K3=1.64E+00 # gg08_rnum=7183
gC4H3 -> gHC4H + gH										RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7184
gC4H4 -> gC3H4 + gC										RTYPE=19 K1=1.13E-09 K2=0.0 K3=1.64E+00 # gg08_rnum=7185
gC4N -> gC3 + gCN										RTYPE=19 K1=5.00E-10 K2=0.0 K3=1.70E+00 # gg08_rnum=7186
gC4P -> gC3P + gC										RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7187
gC4S -> gC3 + gCS										RTYPE=19 K1=1.00E-10 K2=0.0 K3=2.00E+00 # gg08_rnum=7188
gC5 -> gC3 + gC2										RTYPE=19 K1=1.00E-11 K2=0.0 K3=1.70E+00 # gg08_rnum=7189
gC5H -> gC2H + gC3										RTYPE=19 K1=1.00E-11 K2=0.0 K3=1.70E+00 # gg08_rnum=7190
gC5H -> gC3H + gC2										RTYPE=19 K1=1.00E-11 K2=0.0 K3=1.70E+00 # gg08_rnum=7191
gC5H -> gC5 + gH										RTYPE=19 K1=1.00E-11 K2=0.0 K3=1.70E+00 # gg08_rnum=7192
gC5H2 -> gC3H + gC2H									RTYPE=19 K1=1.00E-11 K2=0.0 K3=1.70E+00 # gg08_rnum=7193
gC5H2 -> gC5H + gH										RTYPE=19 K1=1.00E-11 K2=0.0 K3=1.70E+00 # gg08_rnum=7194
gC5H3 -> gC5H2 + gH										RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7195
gC5H4 -> gC5H2 + gH2									RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7196
gC5N -> gC4 + gCN										RTYPE=19 K1=5.00E-10 K2=0.0 K3=1.70E+00 # gg08_rnum=7197
gC6 -> gC5 + gC											RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7198
gC6H -> gC3H + gC3										RTYPE=19 K1=5.00E-12 K2=0.0 K3=1.70E+00 # gg08_rnum=7199
gC6H -> gC2H + gC4										RTYPE=19 K1=5.00E-12 K2=0.0 K3=1.70E+00 # gg08_rnum=7200
gC6H2 -> gC6H + gH										RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7201
gC6H3 -> gC6H2 + gH										RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7202
gC6H4 -> gC6H2 + gH2									RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7203
gC6H6 -> gC6H4 + gH2									RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7204
gC7 -> gC6 + gC											RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7205
gC7H -> gC7 + gH										RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7206
gC7H2 -> gC7H + gH										RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7207
gC7H3 -> gC7H2 + gH										RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7208
gC7H4 -> gC7H2 + gH2									RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7209
gC7N -> gC6 + gCN										RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7210
gC8 -> gC7 + gC											RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7211
gC8H -> gC8 + gH										RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7212
gC8H2 -> gC8H + gH										RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7213
gC8H3 -> gC8H2 + gH										RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7214
gC8H4 -> gC8H2 + gH2									RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7215
gC9 -> gC8 + gC											RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7216
gC9H -> gC9 + gH										RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7217
gC9H2 -> gC9H + gH										RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7218
gC9H3 -> gC9H2 + gH										RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7219
gC9H4 -> gC9H2 + gH2									RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7220
gC9N -> gC8 + gCN										RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7221
gClC -> gCl + gC										RTYPE=19 K1=1.00E-10 K2=0.0 K3=2.00E+00 # gg08_rnum=7222
gCCP -> gCP + gC										RTYPE=19 K1=5.00E-10 K2=0.0 K3=1.70E+00 # gg08_rnum=7223
gCCP -> gC2 + gP										RTYPE=19 K1=5.00E-10 K2=0.0 K3=1.70E+00 # gg08_rnum=7224
gCH -> gC + gH											RTYPE=19 K1=1.40E-10 K2=0.0 K3=1.50E+00 # gg08_rnum=7225
gCH2 -> gCH + gH										RTYPE=19 K1=5.00E-11 K2=0.0 K3=1.70E+00 # gg08_rnum=7226
gH2C2N -> gCH2 + gCN									RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7227
gCH2CO -> gCH2 + gCO									RTYPE=19 K1=9.04E-10 K2=0.0 K3=1.58E+00 # gg08_rnum=7228
gCH2NH -> gHCN + gH2									RTYPE=19 K1=1.70E-09 K2=0.0 K3=1.63E+00 # gg08_rnum=7229
gCH2NH2 -> gCH2 + gNH2									RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7230
gCH2OH -> gCH2 + gOH									RTYPE=19 K1=5.00E-10 K2=0.0 K3=1.70E+00 # gg08_rnum=7231
gCH2PH -> gHCP + gH2									RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7232
gCH3 -> gCH + gH2										RTYPE=19 K1=3.00E-11 K2=0.0 K3=1.70E+00 # gg08_rnum=7233
gCH3 -> gCH2 + gH										RTYPE=19 K1=3.00E-11 K2=0.0 K3=1.70E+00 # gg08_rnum=7234
gCH3C3N -> gC3N + gCH3									RTYPE=19 K1=2.00E-11 K2=0.0 K3=1.70E+00 # gg08_rnum=7235
gCH3C4H -> gC4H + gCH3									RTYPE=19 K1=2.00E-11 K2=0.0 K3=1.70E+00 # gg08_rnum=7236
gCH3C5N -> gCH3 + gC5N									RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7237
gCH3C6H -> gCH3 + gC6H									RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7238
gCH3C7N -> gCH3 + gC7N									RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7239
gCH3CHO -> gCH4 + gCO									RTYPE=19 K1=3.43E-10 K2=0.0 K3=1.52E+00 # gg08_rnum=7240
gCH3CHO -> gCH3 + gHCO									RTYPE=19 K1=3.43E-10 K2=0.0 K3=1.52E+00 # gg08_rnum=7241
gCH3CN -> gCH3 + gCN									RTYPE=19 K1=1.56E-09 K2=0.0 K3=1.95E+00 # gg08_rnum=7242
gCH3CO -> gCH3 + gCO									RTYPE=19 K1=9.00E-10 K2=0.0 K3=1.60E+00 # gg08_rnum=7243
gCH3NH -> gCH2NH + gH									RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7244
gCH3NH -> gNH + gCH3									RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7245
gCH3NH2 -> gHCN + gH2 + gH + gH							RTYPE=19 K1=3.50E-10 K2=0.0 K3=1.73E+00 # gg08_rnum=7246
gCH3NH2 -> gCH3 + gNH2									RTYPE=19 K1=1.55E-10 K2=0.0 K3=1.74E+00 # gg08_rnum=7247
gCH3NH2 -> gCH2NH + gH + gH								RTYPE=19 K1=6.63E-11 K2=0.0 K3=1.51E+00 # gg08_rnum=7248
gCH3NH2 -> gCN + gH2 + gH2 + gH							RTYPE=19 K1=9.42E-11 K2=0.0 K3=1.76E+00 # gg08_rnum=7249
gCH3O -> gCH3 + gO										RTYPE=19 K1=5.00E-10 K2=0.0 K3=1.70E+00 # gg08_rnum=7250
gCH3OCH3 -> gCH3O + gCH3								RTYPE=19 K1=9.00E-10 K2=0.0 K3=1.60E+00 # gg08_rnum=7251
gCH3OH -> gCH2OH + gH									RTYPE=19 K1=5.00E-10 K2=0.0 K3=1.60E+00 # gg08_rnum=7252
gCH3OH -> gCH3O + gH									RTYPE=19 K1=2.00E-10 K2=0.0 K3=1.60E+00 # gg08_rnum=7253
gCH3OH -> gCH3 + gOH									RTYPE=19 K1=5.00E-10 K2=0.0 K3=1.70E+00 # gg08_rnum=7254
gCH4 -> gCH2 + gH2										RTYPE=19 K1=4.80E-10 K2=0.0 K3=2.20E+00 # gg08_rnum=7255
gCH4 -> gCH + gH2 + gH									RTYPE=19 K1=1.60E-10 K2=0.0 K3=2.20E+00 # gg08_rnum=7256
gCH4 -> gCH3 + gH										RTYPE=19 K1=1.60E-10 K2=0.0 K3=2.20E+00 # gg08_rnum=7257
gHCNH -> gCH + gNH										RTYPE=19 K1=1.00E-10 K2=0.0 K3=1.70E+00 # gg08_rnum=7258
gHCOH -> gCH + gOH										RTYPE=19 K1=0.0 K2=0.0 K3=1.60E+00 # gg08_rnum=7259
gClO -> gCl + gO										RTYPE=19 K1=1.00E-10 K2=0.0 K3=2.00E+00 # gg08_rnum=7260
gCN -> gC + gN											RTYPE=19 K1=1.00E-09 K2=0.0 K3=2.80E+00 # gg08_rnum=7261
gCO -> gC + gO											RTYPE=19 K1=3.10E-11 K2=0.0 K3=2.54E+00 # gg08_rnum=7262
gCO2 -> gCO + gO										RTYPE=19 K1=3.13E-10 K2=0.0 K3=2.03E+00 # gg08_rnum=7263
gCOCHO -> gHCO + gCO									RTYPE=19 K1=9.00E-10 K2=0.0 K3=1.60E+00 # gg08_rnum=7264
gCOOH -> gCO + gOH										RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.80E+00 # gg08_rnum=7265
gCP -> gC + gP											RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7266
gCS -> gC + gS											RTYPE=19 K1=9.70E-10 K2=0.0 K3=2.00E+00 # gg08_rnum=7267
gHC3NH -> gHC2H + gCN									RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7268
gcH2C3O -> gHC2H + gCO									RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7269
gH2C5N -> gHC4H + gCN									RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7270
gH2C7N -> gC6H2 + gCN									RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7271
gH2C9N -> gC8H2 + gCN									RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7272
gH2CN -> gHCN + gH										RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7273
gH2CO -> gCO + gH2										RTYPE=19 K1=4.40E-10 K2=0.0 K3=1.60E+00 # gg08_rnum=7274
gH2CO -> gCO + gH + gH									RTYPE=19 K1=4.40E-10 K2=0.0 K3=1.60E+00 # gg08_rnum=7275
gH2CS -> gCS + gH2										RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7276
gH2O -> gOH + gH										RTYPE=19 K1=3.28E-10 K2=0.0 K3=1.63E+00 # gg08_rnum=7277
gH2O2 -> gOH + gOH										RTYPE=19 K1=8.30E-10 K2=0.0 K3=1.82E+00 # gg08_rnum=7278
gH2S -> gHS + gH										RTYPE=19 K1=3.20E-10 K2=0.0 K3=1.70E+00 # gg08_rnum=7279
gH2S2 -> gHS + gHS										RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7280
gH2SiO -> gSiO + gH2									RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7281
gH3C5N -> gH2C2H + gC3N									RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7282
gH3C7N -> gH2C2H + gC5N									RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7283
gH3C9N -> gH2C2H + gC7N									RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7284
gH4C3N -> gH2C2H2 + gCN									RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7285
gH5C3N -> gCH3CH2 + gCN									RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7286
gHC2NC -> gC2H + gCN									RTYPE=19 K1=9.54E-09 K2=0.0 K3=1.83E+00 # gg08_rnum=7287
gHC2O -> gCO + gCH										RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7288
gHC3N -> gC2H + gCN										RTYPE=19 K1=9.54E-10 K2=0.0 K3=1.83E+00 # gg08_rnum=7289
gHC3O -> gCO + gC2H										RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7290
gHC5N -> gC4H + gCN										RTYPE=19 K1=5.00E-10 K2=0.0 K3=1.83E+00 # gg08_rnum=7291
gHC5N -> gH + gC5N										RTYPE=19 K1=5.00E-10 K2=0.0 K3=1.83E+00 # gg08_rnum=7292
gHC7N -> gC6H + gCN										RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7293
gHC9N -> gC8H + gCN										RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7294
gHC2N -> gC2N + gH										RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7295
gHCCP -> gCCP + gH										RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7296
gClH -> gCl + gH										RTYPE=19 K1=1.10E-10 K2=0.0 K3=1.80E+00 # gg08_rnum=7297
gHCN -> gCN + gH										RTYPE=19 K1=5.48E-10 K2=0.0 K3=2.00E+00 # gg08_rnum=7298
gC2NCH -> gC2H + gCN									RTYPE=19 K1=9.54E-09 K2=0.0 K3=1.83E+00 # gg08_rnum=7299
gHCO -> gH + gCO										RTYPE=19 K1=5.87E-10 K2=0.0 K3=5.30E-01 # gg08_rnum=7300
gHCOOCH3 -> gHCO + gCH3O								RTYPE=19 K1=5.00E-10 K2=0.0 K3=1.70E+00 # gg08_rnum=7301
gHCOOH -> gHCO + gOH									RTYPE=19 K1=2.75E-10 K2=0.0 K3=1.80E+00 # gg08_rnum=7302
gHCP -> gCP + gH										RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7303
gSiCH -> gCH + gSi										RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7304
gCH3COCHO -> gCH3CO + gHCO								RTYPE=19 K1=9.00E-10 K2=0.0 K3=1.60E+00 # gg08_rnum=7305
gCH3COCH3 -> gCH3CO + gCH3								RTYPE=19 K1=9.00E-10 K2=0.0 K3=1.60E+00 # gg08_rnum=7306
gCH3CONH -> gCH3CO + gNH								RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7307
gCH3CONH -> gCH3 + gHNCO								RTYPE=19 K1=9.00E-10 K2=0.0 K3=1.60E+00 # gg08_rnum=7308
gCH3COOH -> gCH3CO + gOH								RTYPE=19 K1=5.00E-10 K2=0.0 K3=1.70E+00 # gg08_rnum=7309
gCH3COOH -> gCH3 + gCOOH								RTYPE=19 K1=9.00E-10 K2=0.0 K3=1.60E+00 # gg08_rnum=7310
gCH3CONH2 -> gCH3CO + gNH2								RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7311
gCH3CONH2 -> gCH3 + gNH2CO								RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.60E+00 # gg08_rnum=7312
gCH3COOCH3 -> gCH3 + gCH3OCO							RTYPE=19 K1=9.00E-10 K2=0.0 K3=1.60E+00 # gg08_rnum=7313
gCH3COOCH3 -> gCH3CO + gCH3O							RTYPE=19 K1=5.00E-10 K2=0.0 K3=1.70E+00 # gg08_rnum=7314
gCH3COCH2OH -> gCH3CO + gCH2OH							RTYPE=19 K1=9.00E-10 K2=0.0 K3=1.60E+00 # gg08_rnum=7315
gCH3COCH2OH -> gCH3 + gHOCH2CO							RTYPE=19 K1=9.00E-10 K2=0.0 K3=1.60E+00 # gg08_rnum=7316
gHNC -> gCN + gH										RTYPE=19 K1=5.48E-10 K2=0.0 K3=2.00E+00 # gg08_rnum=7317
gHNC3 -> gC2H + gCN										RTYPE=19 K1=9.54E-09 K2=0.0 K3=1.83E+00 # gg08_rnum=7318
gHNCHO -> gNH + gHCO									RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7319
gHNCO -> gNH + gCO										RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7320
gHNCOCHO -> gHNCO + gHCO								RTYPE=19 K1=9.00E-10 K2=0.0 K3=1.60E+00 # gg08_rnum=7321
gHNCOCHO -> gNH + gCOCHO								RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7322
gHNCONH -> gNH + gHNCO									RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7323
gHNCOOH -> gCOOH + gNH									RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7324
gHNCOOH -> gOH + gHNCO									RTYPE=19 K1=5.00E-10 K2=0.0 K3=1.70E+00 # gg08_rnum=7325
gHNO -> gNO + gH										RTYPE=19 K1=1.70E-10 K2=0.0 K3=5.30E-01 # gg08_rnum=7326
gHNOH -> gNH + gOH										RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.80E+00 # gg08_rnum=7327
gHNSi -> gSiN + gH										RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7328
gCH3ONH -> gNH + gCH3O									RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.80E+00 # gg08_rnum=7329
gHNCH2OH -> gNH + gCH2OH								RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7330
gHOC -> gCO + gH										RTYPE=19 K1=0.0 K2=0.0 K3=1.70E+00 # gg08_rnum=7331
gHOCOOH -> gOH + gCOOH									RTYPE=19 K1=5.00E-10 K2=0.0 K3=1.70E+00 # gg08_rnum=7332
gHPO -> gPO + gH										RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7333
gHS -> gH + gS											RTYPE=19 K1=1.00E-11 K2=0.0 K3=2.00E+00 # gg08_rnum=7334
gS2H -> gHS + gS										RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7335
gHCOCOCHO -> gHCO + gCOCHO								RTYPE=19 K1=9.00E-10 K2=0.0 K3=1.60E+00 # gg08_rnum=7336
gMgH -> gMg + gH										RTYPE=19 K1=5.00E-10 K2=0.0 K3=1.45E+00 # gg08_rnum=7337
gMgH2 -> gMgH + gH										RTYPE=19 K1=5.00E-10 K2=0.0 K3=1.45E+00 # gg08_rnum=7338
gN2 -> gN + gN											RTYPE=19 K1=5.00E-12 K2=0.0 K3=3.00E+00 # gg08_rnum=7339
gN2H2 -> gNH2 + gN										RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7340
gN2O -> gN2 + gO										RTYPE=19 K1=1.40E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7341
gNaH -> gNa + gH										RTYPE=19 K1=7.30E-09 K2=0.0 K3=1.13E+00 # gg08_rnum=7342
gNaOH -> gNa + gOH										RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7343
gNH -> gH + gN											RTYPE=19 K1=4.00E-10 K2=0.0 K3=1.50E+00 # gg08_rnum=7344
gNH2 -> gNH + gH										RTYPE=19 K1=2.11E-10 K2=0.0 K3=1.52E+00 # gg08_rnum=7345
gNH2CHO -> gNH2 + gHCO									RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7346
gNH2CN -> gNH2 + gCN									RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7347
gNH2OH -> gNH2 + gOH									RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7348
gNH3 -> gNH + gH2										RTYPE=19 K1=1.30E-10 K2=0.0 K3=1.91E+00 # gg08_rnum=7349
gNH3 -> gNH2 + gH										RTYPE=19 K1=4.94E-10 K2=0.0 K3=1.65E+00 # gg08_rnum=7350
gNO -> gN + gO											RTYPE=19 K1=3.00E-10 K2=0.0 K3=2.00E+00 # gg08_rnum=7351
gNO2 -> gNO + gO										RTYPE=19 K1=1.29E-09 K2=0.0 K3=2.00E+00 # gg08_rnum=7352
gNS -> gN + gS											RTYPE=19 K1=1.00E-11 K2=0.0 K3=2.00E+00 # gg08_rnum=7353
gO2 -> gO + gO											RTYPE=19 K1=3.30E-10 K2=0.0 K3=1.40E+00 # gg08_rnum=7354
gHO2 -> gOH + gO										RTYPE=19 K1=5.00E-10 K2=0.0 K3=1.70E+00 # gg08_rnum=7355
gHO2 -> gO2 + gH										RTYPE=19 K1=5.00E-10 K2=0.0 K3=1.70E+00 # gg08_rnum=7356
gO3 -> gO2 + gO											RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7357
gOCN -> gO + gCN										RTYPE=19 K1=1.00E-11 K2=0.0 K3=2.00E+00 # gg08_rnum=7358
gOCS -> gCO + gS										RTYPE=19 K1=2.28E-09 K2=0.0 K3=1.60E+00 # gg08_rnum=7359
gOH -> gO + gH											RTYPE=19 K1=1.68E-10 K2=0.0 K3=1.66E+00 # gg08_rnum=7360
gCHOCHO -> gHCO + gHCO									RTYPE=19 K1=9.00E-10 K2=0.0 K3=1.60E+00 # gg08_rnum=7361
gHCOCOOH -> gCOCHO + gOH								RTYPE=19 K1=5.00E-10 K2=0.0 K3=1.70E+00 # gg08_rnum=7362
gHCOCOOH -> gHCO + gCOOH								RTYPE=19 K1=9.00E-10 K2=0.0 K3=1.60E+00 # gg08_rnum=7363
gPH -> gP + gH											RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7364
gPH2 -> gPH + gH										RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7365
gPN -> gN + gP											RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7366
gPO -> gO + gP											RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7367
gNH2CO -> gNH2 + gCO									RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7368
gNH2COCHO -> gNH2CO + gHCO								RTYPE=19 K1=9.00E-10 K2=0.0 K3=1.60E+00 # gg08_rnum=7369
gNH2COCHO -> gNH2 + gCOCHO								RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7370
gNH2CONH -> gNH2 + gHNCO								RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7371
gNH2CONH -> gNH2CO + gNH								RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7372
gNH2COOH -> gNH2 + gCOOH								RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7373
gNH2COOH -> gNH2CO + gOH								RTYPE=19 K1=5.00E-10 K2=0.0 K3=1.70E+00 # gg08_rnum=7374
gNH2CONH2 -> gNH2 + gNH2CO								RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7375
gNH2NH -> gNH + gNH2									RTYPE=19 K1=1.00E-11 K2=0.0 K3=3.00E+00 # gg08_rnum=7376
gNH2NH2 -> gNH2 + gNH2									RTYPE=19 K1=1.00E-11 K2=0.0 K3=3.00E+00 # gg08_rnum=7377
gNH2OCH3 -> gNH2 + gCH3O								RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.80E+00 # gg08_rnum=7378
gNH2CH2OH -> gNH2 + gCH2OH								RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7379
gS2 -> gS + gS											RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7380
gSiC -> gSi + gC										RTYPE=19 K1=1.00E-10 K2=0.0 K3=2.30E+00 # gg08_rnum=7381
gSiC2 -> gSiC + gC										RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7382
gSiC2H -> gSiC2 + gH									RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7383
gSiC2H2 -> gSiC2 + gH2									RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7384
gSiC3 -> gSiC2 + gC										RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7385
gSiC3H -> gSiC3 + gH									RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7386
gSiC4 -> gSiC2 + gC2									RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7387
gSiCH2 -> gSiC + gH2									RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7388
gSiCH3 -> gSiCH2 + gH									RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7389
gSiH -> gSi + gH										RTYPE=19 K1=1.00E-10 K2=0.0 K3=2.30E+00 # gg08_rnum=7390
gSiH2 -> gSiH + gH										RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7391
gSiH3 -> gSiH2 + gH										RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7392
gSiH4 -> gSiH2 + gH2									RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7393
gSiN -> gSi + gN										RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7394
gSiNC -> gCN + gSi										RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7395
gSiO -> gSi + gO										RTYPE=19 K1=1.00E-10 K2=0.0 K3=2.30E+00 # gg08_rnum=7396
gSiO2 -> gSiO + gO										RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7397
gSiS -> gSi + gS										RTYPE=19 K1=1.00E-10 K2=0.0 K3=2.30E+00 # gg08_rnum=7398
gSO -> gO + gS											RTYPE=19 K1=3.30E-10 K2=0.0 K3=1.40E+00 # gg08_rnum=7399
gSO2 -> gSO + gO										RTYPE=19 K1=1.05E-09 K2=0.0 K3=1.74E+00 # gg08_rnum=7400
gCH3OCO -> gCH3O + gCO									RTYPE=19 K1=5.00E-10 K2=0.0 K3=1.70E+00 # gg08_rnum=7401
gCH3OCOCHO -> gCH3OCO + gHCO							RTYPE=19 K1=9.00E-10 K2=0.0 K3=1.60E+00 # gg08_rnum=7402
gCH3OCOCHO -> gCH3O + gCOCHO							RTYPE=19 K1=5.00E-10 K2=0.0 K3=1.70E+00 # gg08_rnum=7403
gCH3OCONH -> gCH3O + gHNCO								RTYPE=19 K1=5.00E-10 K2=0.0 K3=1.70E+00 # gg08_rnum=7404
gCH3OCONH -> gCH3OCO + gNH								RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7405
gCH3OCOOH -> gCH3OCO + gOH								RTYPE=19 K1=5.00E-10 K2=0.0 K3=1.70E+00 # gg08_rnum=7406
gCH3OCOOH -> gCH3O + gCOOH								RTYPE=19 K1=5.00E-10 K2=0.0 K3=1.70E+00 # gg08_rnum=7407
gCH3OCONH2 -> gNH2 + gCH3OCO							RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7408
gCH3OCONH2 -> gNH2CO + gCH3O							RTYPE=19 K1=5.00E-10 K2=0.0 K3=1.70E+00 # gg08_rnum=7409
gCH3OCOOCH3 -> gCH3O + gCH3OCO							RTYPE=19 K1=5.00E-10 K2=0.0 K3=1.70E+00 # gg08_rnum=7410
gCH3OCOCH2OH -> gCH3OCO + gCH2OH						RTYPE=19 K1=9.00E-10 K2=0.0 K3=1.60E+00 # gg08_rnum=7411
gCH3OCOCH2OH -> gCH3O + gHOCH2CO						RTYPE=19 K1=5.00E-10 K2=0.0 K3=1.70E+00 # gg08_rnum=7412
gCH3OOH -> gOH + gCH3O									RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.80E+00 # gg08_rnum=7413
gCH3OOCH3 -> gCH3O + gCH3O								RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.80E+00 # gg08_rnum=7414
gCH3OCH2OH -> gCH3O + gCH2OH							RTYPE=19 K1=5.00E-10 K2=0.0 K3=1.70E+00 # gg08_rnum=7415
gCH2OHCHO -> gCH2OH + gHCO								RTYPE=19 K1=9.00E-10 K2=0.0 K3=1.60E+00 # gg08_rnum=7416
gHOCH2CO -> gCH2OH + gCO								RTYPE=19 K1=9.00E-10 K2=0.0 K3=1.60E+00 # gg08_rnum=7417
gHOCH2COCHO -> gHOCH2CO + gHCO							RTYPE=19 K1=9.00E-10 K2=0.0 K3=1.60E+00 # gg08_rnum=7418
gHOCH2COCHO -> gCH2OH + gCOCHO							RTYPE=19 K1=9.00E-10 K2=0.0 K3=1.60E+00 # gg08_rnum=7419
gHNCOCH2OH -> gHOCH2CO + gNH							RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7420
gHNCOCH2OH -> gCH2OH + gHNCO							RTYPE=19 K1=9.00E-10 K2=0.0 K3=1.60E+00 # gg08_rnum=7421
gCH2OHCOOH -> gCH2OH + gCOOH							RTYPE=19 K1=9.00E-10 K2=0.0 K3=1.60E+00 # gg08_rnum=7422
gCH2OHCOOH -> gHOCH2CO + gOH							RTYPE=19 K1=5.00E-10 K2=0.0 K3=1.70E+00 # gg08_rnum=7423
gNH2COCH2OH -> gNH2CO + gCH2OH							RTYPE=19 K1=9.00E-10 K2=0.0 K3=1.60E+00 # gg08_rnum=7424
gNH2COCH2OH -> gNH2 + gHOCH2CO							RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7425
gHOCH2COCH2OH -> gCH2OH + gHOCH2CO						RTYPE=19 K1=9.00E-10 K2=0.0 K3=1.60E+00 # gg08_rnum=7426
gHOCH2OH -> gOH + gCH2OH								RTYPE=19 K1=5.00E-10 K2=0.0 K3=1.70E+00 # gg08_rnum=7427
gHOCH2CH2OH -> gCH2OH + gCH2OH							RTYPE=19 K1=9.00E-10 K2=0.0 K3=1.60E+00 # gg08_rnum=7428
gC10 -> gC8 + gC2										RTYPE=20 K1=1.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=7429
gC10 -> gC9 + gC										RTYPE=20 K1=1.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=7430
gC2 -> gC + gC											RTYPE=20 K1=1.00E-10 K2=0.0 K3=2.00E+00 # gg08_rnum=7431
gC2H -> gC2 + gH										RTYPE=20 K1=5.25E-12 K2=0.0 K3=2.00E+00 # gg08_rnum=7432
gC2H -> gCH + gC										RTYPE=20 K1=4.75E-12 K2=0.0 K3=2.00E+00 # gg08_rnum=7433
gHC2H -> gC2H + gH										RTYPE=20 K1=1.12E-10 K2=0.0 K3=2.67E+00 # gg08_rnum=7434
gHC2H -> gC2 + gH + gH									RTYPE=20 K1=6.58E-11 K2=0.0 K3=2.67E+00 # gg08_rnum=7435
gHC2H -> gCH + gCH										RTYPE=20 K1=2.90E-11 K2=0.0 K3=2.67E+00 # gg08_rnum=7436
gH2C2H -> gC2H + gH2									RTYPE=20 K1=1.91E-11 K2=0.0 K3=2.30E+00 # gg08_rnum=7437
gH2C2H -> gHC2H + gH									RTYPE=20 K1=9.26E-11 K2=0.0 K3=2.30E+00 # gg08_rnum=7438
gH2C2H -> gC2H + gH + gH								RTYPE=20 K1=1.88E-10 K2=0.0 K3=2.30E+00 # gg08_rnum=7439
gH2C2H2 -> gHC2H + gH + gH								RTYPE=20 K1=9.39E-11 K2=0.0 K3=2.66E+00 # gg08_rnum=7440
gH2C2H2 -> gHC2H + gH2									RTYPE=20 K1=8.52E-12 K2=0.0 K3=2.66E+00 # gg08_rnum=7441
gH2C2H2 -> gH2C2H + gH									RTYPE=20 K1=1.56E-11 K2=0.0 K3=2.66E+00 # gg08_rnum=7442
gCH3CH2 -> gH2C2H2 + gH									RTYPE=20 K1=4.00E-11 K2=0.0 K3=2.50E+00 # gg08_rnum=7443
gCH3CH2 -> gC2H + gH2 + gH2								RTYPE=20 K1=4.00E-11 K2=0.0 K3=2.50E+00 # gg08_rnum=7444
gCH3CH2 -> gHC2H + gH2 + gH								RTYPE=20 K1=8.00E-11 K2=0.0 K3=2.50E+00 # gg08_rnum=7445
gCH3CH2 -> gH2C2H + gH2									RTYPE=20 K1=4.00E-11 K2=0.0 K3=2.50E+00 # gg08_rnum=7446
gCH3CH2CHO -> gCH3CH2 + gHCO							RTYPE=20 K1=2.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=7447
gCH3CH2OH -> gCH3CH2 + gOH								RTYPE=20 K1=5.27E-10 K2=0.0 K3=2.35E+00 # gg08_rnum=7448
gC2O -> gCO + gC										RTYPE=20 K1=2.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=7449
gC3H3N -> gC3N + gH2 + gH								RTYPE=20 K1=1.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=7450
gC3H3N -> gHC3N + gH2									RTYPE=20 K1=1.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=7451
gC3H4 -> gC3H2 + gH2									RTYPE=20 K1=5.45E-11 K2=0.0 K3=2.37E+00 # gg08_rnum=7452
gC3H4 -> gC3H3 + gH										RTYPE=20 K1=9.34E-10 K2=0.0 K3=2.37E+00 # gg08_rnum=7453
gHC4H -> gC4 + gH2										RTYPE=20 K1=1.30E-10 K2=0.0 K3=2.28E+00 # gg08_rnum=7454
gHC4H -> gC4H + gH										RTYPE=20 K1=1.30E-10 K2=0.0 K3=2.28E+00 # gg08_rnum=7455
gC4P -> gC3P + gC										RTYPE=20 K1=1.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=7456
gC4P -> gCCP + gC2										RTYPE=20 K1=1.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=7457
gC6 -> gC5 + gC											RTYPE=20 K1=1.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=7458
gC6 -> gC4 + gC2										RTYPE=20 K1=1.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=7459
gC6H2 -> gC6 + gH2										RTYPE=20 K1=9.30E-11 K2=0.0 K3=2.50E+00 # gg08_rnum=7460
gC6H2 -> gC6H + gH										RTYPE=20 K1=9.30E-11 K2=0.0 K3=2.50E+00 # gg08_rnum=7461
gC6H2 -> gC6 + gH + gH									RTYPE=20 K1=1.40E-11 K2=0.0 K3=2.50E+00 # gg08_rnum=7462
gC7 -> gC6 + gC											RTYPE=20 K1=8.70E-11 K2=0.0 K3=2.50E+00 # gg08_rnum=7463
gC7 -> gC5 + gC2										RTYPE=20 K1=8.70E-11 K2=0.0 K3=2.50E+00 # gg08_rnum=7464
gC7 -> gC4 + gC3										RTYPE=20 K1=2.61E-11 K2=0.0 K3=2.50E+00 # gg08_rnum=7465
gC7H -> gC7 + gH										RTYPE=20 K1=1.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=7466
gC7H -> gC6H + gC										RTYPE=20 K1=1.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=7467
gC7H2 -> gC7H + gH										RTYPE=20 K1=1.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=7468
gC7H2 -> gC7 + gH2										RTYPE=20 K1=1.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=7469
gC8 -> gC7 + gC											RTYPE=20 K1=1.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=7470
gC8 -> gC6 + gC2										RTYPE=20 K1=1.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=7471
gC8H -> gC8 + gH										RTYPE=20 K1=1.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=7472
gC8H -> gC7H + gC										RTYPE=20 K1=1.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=7473
gC8H2 -> gC8 + gH2										RTYPE=20 K1=1.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=7474
gC8H2 -> gC8H + gH										RTYPE=20 K1=1.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=7475
gC9 -> gC8 + gC											RTYPE=20 K1=1.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=7476
gC9 -> gC7 + gC2										RTYPE=20 K1=1.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=7477
gC9H -> gC8H + gC										RTYPE=20 K1=1.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=7478
gC9H -> gC9 + gH										RTYPE=20 K1=1.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=7479
gC9H2 -> gC9H + gH										RTYPE=20 K1=1.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=7480
gC9H2 -> gC9 + gH2										RTYPE=20 K1=1.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=7481
gC9N -> gC2 + gC7N										RTYPE=20 K1=2.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=7482
gCCP -> gCP + gC										RTYPE=20 K1=1.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=7483
gCCP -> gC2 + gP										RTYPE=20 K1=1.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=7484
gCH -> gC + gH											RTYPE=20 K1=2.90E-10 K2=0.0 K3=2.80E+00 # gg08_rnum=7485
gCH2 -> gC + gH2										RTYPE=20 K1=1.21E-10 K2=0.0 K3=2.30E+00 # gg08_rnum=7486
gCH2 -> gCH + gH										RTYPE=20 K1=2.51E-10 K2=0.0 K3=2.30E+00 # gg08_rnum=7487
gCH2 -> gC + gH + gH									RTYPE=20 K1=6.28E-10 K2=0.0 K3=2.30E+00 # gg08_rnum=7488
gH2C2N -> gCH + gHCN									RTYPE=20 K1=6.67E-11 K2=0.0 K3=2.50E+00 # gg08_rnum=7489
gH2C2N -> gCN + gCH2									RTYPE=20 K1=6.67E-11 K2=0.0 K3=2.50E+00 # gg08_rnum=7490
gH2C2N -> gC2N + gH2									RTYPE=20 K1=6.67E-11 K2=0.0 K3=2.50E+00 # gg08_rnum=7491
gCH2CO -> gC2 + gH2O									RTYPE=20 K1=1.15E-10 K2=0.0 K3=2.01E+00 # gg08_rnum=7492
gCH2CO -> gCH2 + gCO									RTYPE=20 K1=1.15E-10 K2=0.0 K3=2.01E+00 # gg08_rnum=7493
gCH2CO -> gHC2H + gO									RTYPE=20 K1=1.15E-10 K2=0.0 K3=2.01E+00 # gg08_rnum=7494
gCH2PH -> gCP + gH2 + gH								RTYPE=20 K1=5.00E-11 K2=0.0 K3=2.50E+00 # gg08_rnum=7495
gCH2PH -> gHCP + gH2									RTYPE=20 K1=5.00E-11 K2=0.0 K3=2.50E+00 # gg08_rnum=7496
gCH2PH -> gCH3 + gP										RTYPE=20 K1=1.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=7497
gCH3 -> gH2 + gC + gH									RTYPE=20 K1=3.00E-11 K2=0.0 K3=2.10E+00 # gg08_rnum=7498
gCH3 -> gCH + gH + gH									RTYPE=20 K1=1.60E-11 K2=0.0 K3=2.10E+00 # gg08_rnum=7499
gCH3 -> gCH + gH2										RTYPE=20 K1=1.40E-11 K2=0.0 K3=2.10E+00 # gg08_rnum=7500
gCH3 -> gCH2 + gH										RTYPE=20 K1=4.00E-11 K2=0.0 K3=2.10E+00 # gg08_rnum=7501
gCH3C6H -> gC7H2 + gH2									RTYPE=20 K1=1.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=7502
gCH3C6H -> gC7H + gH2 + gH								RTYPE=20 K1=1.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=7503
gCH3CHO -> gCH3 + gHCO									RTYPE=20 K1=2.60E-10 K2=0.0 K3=2.28E+00 # gg08_rnum=7504
gCH3CN -> gCH3 + gCN									RTYPE=20 K1=1.18E-10 K2=0.0 K3=3.11E+00 # gg08_rnum=7505
gCH3CN -> gH2C2N + gH									RTYPE=20 K1=1.18E-10 K2=0.0 K3=3.11E+00 # gg08_rnum=7506
gCH3CN -> gC2N + gH2 + gH								RTYPE=20 K1=1.18E-10 K2=0.0 K3=3.11E+00 # gg08_rnum=7507
gCH3CN -> gCH2 + gHCN									RTYPE=20 K1=1.76E-10 K2=0.0 K3=3.11E+00 # gg08_rnum=7508
gCH3NH2 -> gCH3 + gNH2									RTYPE=20 K1=1.30E-10 K2=0.0 K3=2.28E+00 # gg08_rnum=7509
gCH3NH2 -> gCH2NH + gH2									RTYPE=20 K1=1.30E-10 K2=0.0 K3=2.28E+00 # gg08_rnum=7510
gCH3OCH3 -> gCH3O + gCH3								RTYPE=20 K1=2.60E-10 K2=0.0 K3=2.28E+00 # gg08_rnum=7511
gCH3OH -> gCH3 + gOH									RTYPE=20 K1=4.80E-10 K2=0.0 K3=2.57E+00 # gg08_rnum=7512
gCP -> gC + gP											RTYPE=20 K1=2.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=7513
gH2CN -> gCN + gH + gH									RTYPE=20 K1=3.98E-11 K2=0.0 K3=2.50E+00 # gg08_rnum=7514
gH2CN -> gHCN + gH										RTYPE=20 K1=8.01E-11 K2=0.0 K3=2.50E+00 # gg08_rnum=7515
gH2CN -> gHNC + gH										RTYPE=20 K1=8.01E-11 K2=0.0 K3=2.50E+00 # gg08_rnum=7516
gH2CO -> gHCO + gH										RTYPE=20 K1=1.33E-11 K2=0.0 K3=2.80E+00 # gg08_rnum=7517
gH2CO -> gCO + gH2										RTYPE=20 K1=6.67E-11 K2=0.0 K3=2.80E+00 # gg08_rnum=7518
gH2CO -> gCO + gH + gH									RTYPE=20 K1=1.40E-11 K2=0.0 K3=3.10E+00 # gg08_rnum=7519
gH2O -> gH2 + gO										RTYPE=20 K1=1.90E-12 K2=0.0 K3=3.10E+00 # gg08_rnum=7520
gH2O -> gOH + gH										RTYPE=20 K1=4.20E-12 K2=0.0 K3=3.10E+00 # gg08_rnum=7521
gH2O -> gO + gH + gH									RTYPE=20 K1=1.49E-11 K2=0.0 K3=3.10E+00 # gg08_rnum=7522
gH2S2 -> gHS + gHS										RTYPE=20 K1=1.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=7523
gH2S2 -> gS2H + gH										RTYPE=20 K1=1.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=7524
gH2SiO -> gSiH + gOH									RTYPE=20 K1=1.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=7525
gH2SiO -> gSiO + gH2									RTYPE=20 K1=1.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=7526
gHC7N -> gC7N + gH										RTYPE=20 K1=1.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=7527
gHC7N -> gCN + gC6H										RTYPE=20 K1=1.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=7528
gHC9N -> gC9N + gH										RTYPE=20 K1=1.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=7529
gHC9N -> gCN + gC8H										RTYPE=20 K1=1.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=7530
gHCCP -> gCCP + gH										RTYPE=20 K1=1.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=7531
gHCCP -> gCP + gCH										RTYPE=20 K1=1.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=7532
gHCO -> gCO + gH										RTYPE=20 K1=2.46E-10 K2=0.0 K3=2.11E+00 # gg08_rnum=7533
gHCOOCH3 -> gCH3OH + gCO								RTYPE=20 K1=1.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=7534
gHCOOCH3 -> gCH3 + gCO2 + gH							RTYPE=20 K1=1.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=7535
gHCOOH -> gCO2 + gH + gH								RTYPE=20 K1=1.73E-10 K2=0.0 K3=2.59E+00 # gg08_rnum=7536
gHCP -> gCH + gP										RTYPE=20 K1=1.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=7537
gHCP -> gCP + gH										RTYPE=20 K1=1.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=7538
gHCS -> gCS + gH										RTYPE=20 K1=2.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=7539
gSiCH -> gSi + gCH										RTYPE=20 K1=1.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=7540
gSiCH -> gSiC + gH										RTYPE=20 K1=1.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=7541
gHNSi -> gSiN + gH										RTYPE=20 K1=1.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=7542
gHNSi -> gNH + gSi										RTYPE=20 K1=1.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=7543
gHPO -> gPO + gH										RTYPE=20 K1=1.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=7544
gHPO -> gPH + gO										RTYPE=20 K1=1.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=7545
gS2H -> gHS + gS										RTYPE=20 K1=1.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=7546
gS2H -> gS2 + gH										RTYPE=20 K1=1.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=7547
gNH -> gN + gH											RTYPE=20 K1=1.00E-11 K2=0.0 K3=2.00E+00 # gg08_rnum=7548
gNH2 -> gNH + gH										RTYPE=20 K1=5.77E-11 K2=0.0 K3=2.59E+00 # gg08_rnum=7549
gNH2 -> gN + gH + gH									RTYPE=20 K1=1.15E-10 K2=0.0 K3=2.59E+00 # gg08_rnum=7550
gNH3 -> gNH2 + gH										RTYPE=20 K1=6.20E-11 K2=0.0 K3=2.47E+00 # gg08_rnum=7551
gNH3 -> gNH + gH + gH									RTYPE=20 K1=6.20E-11 K2=0.0 K3=2.47E+00 # gg08_rnum=7552
gNO -> gN + gO											RTYPE=20 K1=2.00E-10 K2=0.0 K3=2.00E+00 # gg08_rnum=7553
gO2 -> gO + gO											RTYPE=20 K1=6.20E-12 K2=0.0 K3=3.10E+00 # gg08_rnum=7554
gHO2 -> gO2 + gH										RTYPE=20 K1=2.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=7555
gOCS -> gCS + gO										RTYPE=20 K1=7.90E-11 K2=0.0 K3=2.71E+00 # gg08_rnum=7556
gOCS -> gCO + gS										RTYPE=20 K1=1.58E-10 K2=0.0 K3=2.71E+00 # gg08_rnum=7557
gOH -> gO + gH											RTYPE=20 K1=1.60E-12 K2=0.0 K3=3.10E+00 # gg08_rnum=7558
gPH -> gP + gH											RTYPE=20 K1=2.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=7559
gPH2 -> gP + gH2										RTYPE=20 K1=1.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=7560
gPH2 -> gPH + gH										RTYPE=20 K1=1.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=7561
gPN -> gP + gN											RTYPE=20 K1=2.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=7562
gPO -> gP + gO											RTYPE=20 K1=2.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=7563
gS2 -> gS + gS											RTYPE=20 K1=2.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=7564
gSiC2 -> gSi + gC2										RTYPE=20 K1=1.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=7565
gSiC2 -> gSiC + gC										RTYPE=20 K1=1.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=7566
gSiC2H -> gC2H + gSi									RTYPE=20 K1=1.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=7567
gSiC2H -> gSiC2 + gH									RTYPE=20 K1=1.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=7568
gSiC2H2 -> gSiC2 + gH2									RTYPE=20 K1=1.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=7569
gSiC2H2 -> gSiC2H + gH									RTYPE=20 K1=1.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=7570
gSiC3 -> gSiC + gC2										RTYPE=20 K1=1.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=7571
gSiC3 -> gSiC2 + gC										RTYPE=20 K1=1.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=7572
gSiC3H -> gSi + gC3H									RTYPE=20 K1=1.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=7573
gSiC3H -> gSiC3 + gH									RTYPE=20 K1=1.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=7574
gSiC4 -> gSiC2 + gC2									RTYPE=20 K1=1.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=7575
gSiC4 -> gSiC3 + gC										RTYPE=20 K1=1.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=7576
gSiCH2 -> gSi + gCH2									RTYPE=20 K1=7.27E-11 K2=0.0 K3=2.50E+00 # gg08_rnum=7577
gSiCH2 -> gSiC + gH2									RTYPE=20 K1=7.27E-11 K2=0.0 K3=2.50E+00 # gg08_rnum=7578
gSiCH2 -> gSiCH + gH									RTYPE=20 K1=5.45E-11 K2=0.0 K3=2.50E+00 # gg08_rnum=7579
gSiCH3 -> gSiCH + gH2									RTYPE=20 K1=1.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=7580
gSiCH3 -> gSiCH2 + gH									RTYPE=20 K1=1.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=7581
gSiH2 -> gSi + gH2										RTYPE=20 K1=5.45E-11 K2=0.0 K3=2.50E+00 # gg08_rnum=7582
gSiH2 -> gSiH + gH										RTYPE=20 K1=7.27E-11 K2=0.0 K3=2.50E+00 # gg08_rnum=7583
gSiH2 -> gSi + gH + gH									RTYPE=20 K1=7.27E-11 K2=0.0 K3=2.50E+00 # gg08_rnum=7584
gSiH3 -> gSiH + gH2										RTYPE=20 K1=1.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=7585
gSiH3 -> gSiH2 + gH										RTYPE=20 K1=1.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=7586
gSiH4 -> gSiH2 + gH2									RTYPE=20 K1=1.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=7587
gSiH4 -> gSiH3 + gH										RTYPE=20 K1=1.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=7588
gSiN -> gSi + gN										RTYPE=20 K1=2.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=7589
gSiNC -> gSi + gCN										RTYPE=20 K1=2.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=7590
C -> gC													RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7591
C10 -> gC10												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7592
C2 -> gC2												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7593
C2H -> gC2H												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7594
HC2H -> gHC2H											RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7595
H2C2H -> gH2C2H											RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7596
H2C2H2 -> gH2C2H2										RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7597
CH3CH2 -> gCH3CH2										RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7598
CH3CH2CHO -> gCH3CH2CHO									RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7599
CH3CH2OH -> gCH3CH2OH									RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7600
CH3CH3 -> gCH3CH3										RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7601
C2N -> gC2N												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7602
C2O -> gC2O												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7603
C2S -> gC2S												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7604
C3 -> gC3												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7605
C3H -> gC3H												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7606
C3H2 -> gC3H2											RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7607
C3H3 -> gC3H3											RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7608
C3H3N -> gC3H3N											RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7609
C3H4 -> gC3H4											RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7610
C3N -> gC3N												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7611
C3O -> gC3O												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7612
C3P -> gC3P												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7613
C3S -> gC3S												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7614
C4 -> gC4												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7615
C4H -> gC4H												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7616
HC4H -> gHC4H											RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7617
C4H3 -> gC4H3											RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7618
C4H4 -> gC4H4											RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7619
C4N -> gC4N												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7620
C4P -> gC4P												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7621
C4S -> gC4S												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7622
C5 -> gC5												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7623
C5H -> gC5H												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7624
C5H2 -> gC5H2											RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7625
C5H3 -> gC5H3											RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7626
C5H4 -> gC5H4											RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7627
C5N -> gC5N												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7628
C6 -> gC6												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7629
C6H -> gC6H												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7630
C6H2 -> gC6H2											RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7631
C6H3 -> gC6H3											RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7632
C6H4 -> gC6H4											RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7633
C6H6 -> gC6H6											RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7634
C7 -> gC7												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7635
C7H -> gC7H												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7636
C7H2 -> gC7H2											RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7637
C7H3 -> gC7H3											RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7638
C7H4 -> gC7H4											RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7639
C7N -> gC7N												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7640
C8 -> gC8												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7641
C8H -> gC8H												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7642
C8H2 -> gC8H2											RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7643
C8H3 -> gC8H3											RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7644
C8H4 -> gC8H4											RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7645
C9 -> gC9												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7646
C9H -> gC9H												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7647
C9H2 -> gC9H2											RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7648
C9H3 -> gC9H3											RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7649
C9H4 -> gC9H4											RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7650
C9N -> gC9N												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7651
ClC -> gClC												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7652
CCP -> gCCP												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7653
CH -> gCH												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7654
CH2 -> gCH2												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7655
H2C2N -> gH2C2N											RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7656
CH2CO -> gCH2CO											RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7657
CH2NH -> gCH2NH											RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7658
CH2NH2 -> gCH2NH2										RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7659
CH2OH -> gCH2OH											RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7660
CH2PH -> gCH2PH											RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7661
CH3 -> gCH3												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7662
CH3C3N -> gCH3C3N										RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7663
CH3C4H -> gCH3C4H										RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7664
CH3C5N -> gCH3C5N										RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7665
CH3C6H -> gCH3C6H										RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7666
CH3C7N -> gCH3C7N										RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7667
CH3CHO -> gCH3CHO										RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7668
CH3CN -> gCH3CN											RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7669
CH3CO -> gCH3CO											RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7670
CH3NH -> gCH3NH											RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7671
CH3NH2 -> gCH3NH2										RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7672
CH3O -> gCH3O											RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7673
CH3OCH3 -> gCH3OCH3										RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7674
CH3OH -> gCH3OH											RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7675
CH4 -> gCH4												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7676
HCNH -> gHCNH											RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7677
HCOH -> gHCOH											RTYPE=99 K1=0.0 K2=0.0 K3=0.0 # gg08_rnum=7678
Cl -> gCl												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7679
ClO -> gClO												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7680
CN -> gCN												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7681
CO -> gCO												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7682
CO2 -> gCO2												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7683
COCHO -> gCOCHO											RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7684
COOH -> gCOOH											RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7685
CP -> gCP												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7686
CS -> gCS												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7687
Fe -> gFe												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7688
FeH -> gFeH												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7689
H -> gH													RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7690
H2 -> gH2												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7691
HC3NH -> gHC3NH											RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7692
cH2C3O -> gcH2C3O										RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7693
H2C5N -> gH2C5N											RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7694
H2C7N -> gH2C7N											RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7695
H2C9N -> gH2C9N											RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7696
H2CN -> gH2CN											RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7697
H2CO -> gH2CO											RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7698
H2CS -> gH2CS											RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7699
H2O -> gH2O												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7700
H2O2 -> gH2O2											RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7701
H2S -> gH2S												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7702
H2S2 -> gH2S2											RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7703
H2SiO -> gH2SiO											RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7704
H3C5N -> gH3C5N											RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7705
H3C7N -> gH3C7N											RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7706
H3C9N -> gH3C9N											RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7707
H4C3N -> gH4C3N											RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7708
H5C3N -> gH5C3N											RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7709
HC2NC -> gHC2NC											RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7710
HC2O -> gHC2O											RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7711
HC3N -> gHC3N											RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7712
HC3O -> gHC3O											RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7713
HC5N -> gHC5N											RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7714
HC7N -> gHC7N											RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7715
HC9N -> gHC9N											RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7716
HC2N -> gHC2N											RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7717
HCCP -> gHCCP											RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7718
ClH -> gClH												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7719
HCN -> gHCN												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7720
C2NCH -> gC2NCH											RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7721
HCO -> gHCO												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7722
HCOOCH3 -> gHCOOCH3										RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7723
HCOOH -> gHCOOH											RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7724
HCP -> gHCP												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7725
HCS -> gHCS												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7726
SiCH -> gSiCH											RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7727
He -> gHe												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7728
CH3COCHO -> gCH3COCHO									RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7729
CH3COCH3 -> gCH3COCH3									RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7730
CH3CONH -> gCH3CONH										RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7731
CH3COOH -> gCH3COOH										RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7732
CH3CONH2 -> gCH3CONH2									RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7733
CH3COOCH3 -> gCH3COOCH3									RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7734
CH3COCH2OH -> gCH3COCH2OH								RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7735
HNC -> gHNC												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7736
HNC3 -> gHNC3											RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7737
HNCHO -> gHNCHO											RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7738
HNCO -> gHNCO											RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7739
HNCOCHO -> gHNCOCHO										RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7740
HNCONH -> gHNCONH										RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7741
HNCOOH -> gHNCOOH										RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7742
HNO -> gHNO												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7743
HNOH -> gHNOH											RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7744
HNSi -> gHNSi											RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7745
CH3ONH -> gCH3ONH										RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7746
HNCH2OH -> gHNCH2OH										RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7747
HOC -> gHOC												RTYPE=99 K1=0.0 K2=0.0 K3=0.0 # gg08_rnum=7748
HOCOOH -> gHOCOOH										RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7749
HPO -> gHPO												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7750
HS -> gHS												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7751
S2H -> gS2H												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7752
HCOCOCHO -> gHCOCOCHO									RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7753
Mg -> gMg												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7754
MgH -> gMgH												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7755
MgH2 -> gMgH2											RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7756
N -> gN													RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7757
N2 -> gN2												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7758
N2H2 -> gN2H2											RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7759
N2O -> gN2O												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7760
Na -> gNa												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7761
NaH -> gNaH												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7762
NaOH -> gNaOH											RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7763
NH -> gNH												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7764
NH2 -> gNH2												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7765
NH2CHO -> gNH2CHO										RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7766
NH2CN -> gNH2CN											RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7767
NH2OH -> gNH2OH											RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7768
NH3 -> gNH3												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7769
NO -> gNO												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7770
NO2 -> gNO2												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7771
NS -> gNS												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7772
O -> gO													RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7773
O2 -> gO2												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7774
HO2 -> gHO2												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7775
O3 -> gO3												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7776
OCN -> gOCN												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7777
OCS -> gOCS												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7778
OH -> gOH												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7779
CHOCHO -> gCHOCHO										RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7780
HCOCOOH -> gHCOCOOH										RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7781
P -> gP													RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7782
PH -> gPH												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7783
PH2 -> gPH2												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7784
PN -> gPN												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7785
PO -> gPO												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7786
NH2CO -> gNH2CO											RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7787
NH2COCHO -> gNH2COCHO									RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7788
NH2CONH -> gNH2CONH										RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7789
NH2COOH -> gNH2COOH										RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7790
NH2CONH2 -> gNH2CONH2									RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7791
NH2NH -> gNH2NH											RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7792
NH2NH2 -> gNH2NH2										RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7793
NH2OCH3 -> gNH2OCH3										RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7794
NH2CH2OH -> gNH2CH2OH									RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7795
S -> gS													RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7796
S2 -> gS2												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7797
Si -> gSi												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7798
SiC -> gSiC												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7799
SiC2 -> gSiC2											RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7800
SiC2H -> gSiC2H											RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7801
SiC2H2 -> gSiC2H2										RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7802
cSiC3 -> gSiC3											RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7803
SiC3H -> gSiC3H											RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7804
SiC4 -> gSiC4											RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7805
SiCH2 -> gSiCH2											RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7806
SiCH3 -> gSiCH3											RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7807
SiH -> gSiH												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7808
SiH2 -> gSiH2											RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7809
SiH3 -> gSiH3											RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7810
SiH4 -> gSiH4											RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7811
SiN -> gSiN												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7812
SiNC -> gSiNC											RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7813
SiO -> gSiO												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7814
SiO2 -> gSiO2											RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7815
SiS -> gSiS												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7816
SO -> gSO												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7817
SO2 -> gSO2												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7818
CH3OCO -> gCH3OCO										RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7819
CH3OCOCHO -> gCH3OCOCHO									RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7820
CH3OCONH -> gCH3OCONH									RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7821
CH3OCOOH -> gCH3OCOOH									RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7822
CH3OCONH2 -> gCH3OCONH2									RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7823
CH3OCOOCH3 -> gCH3OCOOCH3								RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7824
CH3OCOCH2OH -> gCH3OCOCH2OH								RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7825
CH3OOH -> gCH3OOH										RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7826
CH3OOCH3 -> gCH3OOCH3									RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7827
CH3OCH2OH -> gCH3OCH2OH									RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7828
CH2OHCHO -> gCH2OHCHO									RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7829
HOCH2CO -> gHOCH2CO										RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7830
HOCH2COCHO -> gHOCH2COCHO								RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7831
HNCOCH2OH -> gHNCOCH2OH									RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7832
CH2OHCOOH -> gCH2OHCOOH									RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7833
NH2COCH2OH -> gNH2COCH2OH								RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7834
HOCH2COCH2OH -> gHOCH2COCH2OH							RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7835
HOCH2OH -> gHOCH2OH										RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7836
HOCH2CH2OH -> gHOCH2CH2OH								RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7837
#
# REFERENCES BEGIN HERE
#
# AK70 Avgul, N.N., and Kiselev, A.V. 1970, Chemistry and Physics of Carbon,
#      (journal) vol. 6, p1. (editor P.L.Walker).  This article was used
#      by AR to calculate adsorption energies.
# AR = Allen Robinson
#      Allen, M.,and Robinson, G.W. 1977, Ap.J.,212,396.
#      Mostly based on Avgul Diselev 1970, Chem.Phys.Carbon,6,p1.
#      These are actually carbonatious and graphite surfaces.
#      Converted by 1kcal/mole = 503.4K, Numbers checked twice by Hasegawa.
#      They say these ED may be reduced to 1/5 on a H2-ice surface.
# EH = Erics Notes. Some rounded. Others from polarizability.
#      The others "estimate".
# Le = Leger, 1983, A&Ap,123, 271.
# M84= Mitchell 1984, ApJ. S., 54, 81.  or  1984 ApJ., 287, 665.
# Hasegawa = Tatsuhiko Hasegawa
# RG = Robin Garrod
# SA = Sandford and Allamandola, 1990, Ap.J.355, 357.
#      Lab. More realistic and up-to-dated.
#      Also, Sandford and Allamandola, 1988,Icarus, 76, 201.
# SA93=Sandford & Allamandola, 1993, ApJ, 417, 815. Lab, Table 2.
# TA = Tielens Allamandola
# TH = Tielens and Hagen.
#      Tielens, A.G.G.M., and Hagen, W. 1982, A&Ap.,114, 245.
# WT = Watson = mostly from Hollenbach Salpeter
#      Watson, W.D. 1976, Rev.Mod.Phys., 48, 513.
#
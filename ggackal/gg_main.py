#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# TODO
# - provide input argument for alternative path for input files/model directory
#
# standard library
from __future__ import print_function
from __future__ import absolute_import
import sys
import os
if sys.version_info[0] == 3:
	izip = zip
else:
	from itertools import izip
from timeit import default_timer as timer
import pprint
import operator
import shutil
import argparse
if sys.version_info[0] == 3:
	import pickle
else:
	import cPickle as pickle
# third-party
import sympy
from sympy.utilities import autowrap
import numpy as np
from scipy import interpolate
try:
	from progressbar import *
except ImportError:
	s = ""
	s += "\nERROR: You do not appear to have the progressbar module installed.\n"
	s += "\tYou can either through your system's package manager (e.g. apt, port)\n"
	s += "\tor the python package manager via >pip install --user progressbar"
	raise ImportError(s)
# local
if not os.path.dirname(os.path.abspath(__file__)) in sys.path:
	sys.path.append(os.path.dirname(os.path.abspath(__file__)))
from gg_lib_sys import * # load the system library first and foremost
from gg_lib_math import *
from gg_lib_formatter import *
from gg_lib_reac import *
from gg_lib_physics import *
from gg_lib_plot import *
from gg_lib_logs import *
from gg_lib_objects import *

if sys.version_info[0] == 3:
	from importlib import reload
	xrange = range


######################
# begin main program #
######################

global ggtrial
ggtrial = trial()

def rungg(initonly=False, updatepaths=True):
	"""
	Runs the model to completion (unless there are any lines commented out..).
	"""
	
	ggtrial.trialname = params['trialname']
	if params['verbosity'] > 0:
		print ("\nWILL RUN TRIAL NAMED '%s'" % ggtrial.trialname)
		print ("PROCESS ID IS: '%s'" % os.getpid())
		print ("PYTHON VERSION IS: '%s'\n" % sys.version.replace("\n", " "))
	
	# possibly update paths to filenames
	if updatepaths:
		def updatePath(fname):
			local_fpath = os.path.join(os.getcwd(), params[fname])
			model_fpath = os.path.join(params['model_dir'], params[fname])
			if os.path.isfile(local_fpath):
				params[fname] = local_fpath
			elif os.path.isfile(model_fpath):
				params[fname] = model_fpath
			else:
				msg = "the input file '%s' was specified " % fname
				msg += " in the parameter file '%s' but it wasn't found" % params["paramfname"]
				msg += " in the current directory (as %s) or " % local_fpath
				msg += " in the model directory (as %s)!" % model_fpath
				raise Exception(msg)
			print("updated path: %s" % params[fname])
		updatePath('networkfname')
		updatePath('initialabundfname')
		if params['initadjfname'] is not None:
			updatePath('initadjfname')
		updatePath('surfspeciesfname')
		updatePath('ssdatafname')
	
	# start timer for run
	time_startrun = timer()
	
	if (params['log_level'] > 0) and not initonly: runinitiallogs()
	
	# init_network
	ggtrial.init_network(params['networkfname'])
	if params['verbosity'] > 0: ggtrial.network.print_properties()
	if params['verbosity'] > 2.5: ggtrial.network.print_reactions()
	elif params['verbosity'] >= 1: ggtrial.network.print_reactions(limit=5, randomize=True)
	if params['verbosity'] > 0: printsep()
	
	# init_molecules
	ggtrial.init_molecules()
	if params['verbosity'] > 2.5: ggtrial.molecules.print_properties()
	elif params['verbosity'] >= 1: ggtrial.molecules.print_properties(limit=5, randomize=True)
	if params['verbosity'] > 0: printsep()
	
	if params['ssmodel'] == "table":
		if params['verbosity'] > 0: print("loading the self-shielding data")
		ggtrial.getssdata(params['ssdatafname'])
	
	if params['verbosity'] > 0: print("finished setting initial molecular properties")
	if params['verbosity'] > 0: printsep()
	
	# init_physgrid
	ggtrial.init_physgrid()
	if params['verbosity'] > 0: print("trial will contain %s iterations" % (ggtrial.physgrid.length-1))
	if params['verbosity'] > 2.5: ggtrial.physgrid.print_iterations()
	elif params['verbosity'] >= 1: ggtrial.physgrid.print_iterations(limit=200)
	
	# set up the ODEs
	ggtrial.network.set_ratelaws()
	#timeit.timeit('ggtrial.network.set_ratelaws()', number=1)
	ggtrial.network.set_des()
	#timeit.timeit('ggtrial.network.set_des()', number=1)
	if params['forcetriangmat']:
		ggtrial.molecules.reorder_by_reactions()
	
	# set up the conservation laws (if desired)
	if not (params['particlestoconserve'] and params['particlestoconserve'] == "none"):
		ggtrial.set_conservation_constraints()
	
	# purge reactions/species from network
	if params['removeunusedreactions']:
		ggtrial.network.purge_unused_reactions()
	if params['removeunusedspecies']:
		ggtrial.molecules.purge_unused_species()
	
	# sanity checks
	if params['verbosity'] > 0: print("running sanity checks on the physical model")
	ggtrial.physgrid.runphyssanitychecks()
	if params['verbosity'] > 0: printsep()
	if params['verbosity'] > 0: print("running initial sanity checks on the reaction network")
	ggtrial.network.runinitialrxnsanitychecks()
	if params['verbosity'] > 0: printsep()
	
	# make note of memory usage with chemistry loaded
	gg14_memory_usage.loadedchem = gg14_memory_usage.get()
	if params['verbosity'] > 0: print("memory usage with chemistry initialized is", gg14_memory_usage.loadedchem)
	
	# stop here, if only performing a check or initonly run
	if params['checkonly']:
		print("\nThis was just a check run; you are now free to do as you please.")
		time_elapsedcheck = timer() - time_startrun
		print("time for rungg() was", time_elapsedcheck)
		return
	if initonly:
		return
	
	runsolver()
	gg14_memory_usage.postsolver = gg14_memory_usage.get()
	if params['verbosity'] > 0:
		print("memory usage after the solver is", gg14_memory_usage.postsolver)
		printsep()
	
	if (params['log_level'] > 0) and not initonly: runfinallogs(ggtrial)
	
	if params['verbosity'] > 0: print("running final sanity checks on the reaction network")
	ggtrial.network.runfinalrxnsanitychecks()
	
	# stop timer for run
	time_elapsedrun = timer() - time_startrun
	if params['verbosity'] > 0: print("time for rungg() was", time_elapsedrun)
	
	if params['log_email_upon_completion']: sendemail_completed()
	
	if params['usegui']: rungui()


def runsolver():
	r"""
	Runs the ODE solver, which should be called only after initializing all the
	wrappers.
	"""
	
	if params['verbosity'] > 0: print("setting up the solver..")
	
	# calculate time prior to setup
	time_startevolver = timer()
	
	ggtrial.solution.lib = params['intlib']
	
	###
	# define dvars - dep. variables, where ORDER is important d(vars[i])/dt = des[i]
	#	defines each concentration conc<SPECIES> as a symbolic variable
	ggtrial.solution.dvars = []
	for s in ggtrial.molecules.molecules:
		ggtrial.solution.dvars.append(s.var)
	ggtrial.solution.dvars += [T, gasdensity, av] # independent physical variables
	ggtrial.solution.dvars = np.asarray(ggtrial.solution.dvars)
	if params['verbosity'] > 1:
		print("dvars are:")
		pp = pprint.PrettyPrinter(indent=4)
		pp.pprint(ggtrial.solution.dvars)
	
	###
	# define des - right hand sides of the system
	ggtrial.solution.des = []
	for s in ggtrial.molecules.molecules:
		ggtrial.solution.des.append(s.de)
	# append d<var>/dt=0 for physical properties
	ggtrial.solution.des += [0, 0, 0] # independent physical variables
	ggtrial.solution.desval = np.empty([len(ggtrial.solution.des)], dtype=float)
	if params['verbosity'] > 1:
		print("initial des are:")
		pp = pprint.PrettyPrinter(indent=4)
		pp.pprint(ggtrial.solution.des)
	
	###
	# compute the Jacobian
	ggtrial.solution.jac = None
	if params['computejac']:
		ggtrial.solution.jac = getjac(ggtrial)
		ggtrial.solution.jacval = np.empty([len(ggtrial.solution.des),len(ggtrial.solution.dvars)], dtype=float)
		if params['plot_jacadjacency']:
			print("will plot the sparsity of the jacobian..")
			plotsparsity(ggtrial.solution.jac)
	
	###
	# make ODEs callable
	if params['verbosity'] > 0: print("defining callable ODEs")
	if params['verbosity'] > 0:
		pbwidgets = [Percentage(), Bar('>')]
		pbar = ProgressBar(widgets=pbwidgets, maxval=len(ggtrial.solution.des)).start()
	if mathlib == "sympy":
		for i,de in enumerate(ggtrial.solution.des):
			if is_Expression(de):
				ggtrial.solution.des[i] = sympy.lambdify(ggtrial.solution.dvars, de, 'numpy')
			else:
				ggtrial.solution.des[i] = dummyfunc
			if params['verbosity'] > 0: pbar.update(i)
	elif mathlib == "symengine":
		for i,de in enumerate(ggtrial.solution.des):
			if is_Expression(de):
				ggtrial.solution.des[i] = symengine.Lambdify(ggtrial.solution.dvars, [de]) # correct syntax, but slower!
			else:
				ggtrial.solution.des[i] = dummyfunc
			if params['verbosity'] > 0: pbar.update(i)
	elif mathlib == "theano":
		print("WARNING! this is gonna be slow...")
		for i,de in enumerate(ggtrial.solution.des):
			ggtrial.solution.des[i] = lambda x: de.eval(dict(izip(ggtrial.solution.dvars, x)))
			if params['verbosity'] > 0: pbar.update(i)
	ggtrial.solution.des = np.asarray(ggtrial.solution.des)
	if params['verbosity'] > 0: pbar.finish()
	
	global func
	global jacfunc
	if params['usecythonfuncs']:
		if params['alwayscompcy']: # touch the modification times to force recompilation
			os.utime('gg_lib_solverfunc.pyx', None)
			os.utime('gg_lib_solverjacfunc.pyx', None)
		import gg_lib_solverfunc
		func = gg_lib_solverfunc.func
		if params['computejac']:
			import gg_lib_solverjacfunc
			jacfunc = gg_lib_solverjacfunc.jacfunc
	else:
		from cytoolz import juxt
		def getffabovethreshold(de, v, cutoff):
			val = de(v)
			if abs(val) > cutoff:
				return val
			else:
				return 0
		def func(y, t, args=0.0):
			try:
				float(args)
			except TypeError:
				args = float(args[0])
			#if params['verbosity'] > 2: print "%.2e yr" % float(t/yr)
			y[-3] = ggtrial.physgrid.gettemp(t+args)
			y[-2] = ggtrial.physgrid.getdens(t+args)
			y[-1] = ggtrial.physgrid.getav(t+args)
			return np.asarray(juxt(ggtrial.solution.des)(y))
			#return ggtrial.solution.desval
			#if minratecoeff:
			#	vals = [getffabovethreshold(de, y, params['minratecoeff']) for de in des]
			#else:
			#	vals = juxt(des)(y) # for symengine.Lambdify
			#if np.any(np.vectorize(np.isnan)(vals)):
			#	print "received NaN at func output! (t=%s, y=%s, vals=%s)" % (t, y, vals)
			#	debughere()
			#return vals
		def jacfunc(y, t, args=0.0):
			try:
				float(args)
			except TypeError:
				args = float(args[0])
			y[-3] = ggtrial.physgrid.gettemp(t+args)
			y[-2] = ggtrial.physgrid.getdens(t+args)
			y[-1] = ggtrial.physgrid.getav(t+args)
			for irow,row in enumerate(ggtrial.solution.jac):
				ggtrial.solution.jacval[irow] = np.asarray(juxt(row)(y))
			return np.asarray(ggtrial.solution.jacval)
			#vals = [juxt(row)(y) for row in jac]
			#for i,row in enumerate(vals):
			#	if np.any(np.vectorize(np.isnan)(row)):
			#		print "received NaN in row %s from jacfunc output! (t=%s, y=%s, row=%s)" % (i, t, y, row)
			#		debughere()
			#return vals
	if not params['computejac']:
		jacfunc = None
	
	###
	# if possible, do preprocessing for object-oriented solvers
	#
	#	establish inverted-argument-order functions
	func_inv = None
	jacfunc_inv = None
	if (params['intlib'] in scipyodemethods+gslodemethods):
		def func_inv(t, y, args=0.0):
			if not args:
				args = 0.0
			return func(y, t, args)
		if params['computejac']:
			def jacfunc_inv(t, y, args=0.0):
				if not args:
					args = 0.0
				return jacfunc(y, t, args)
	#	initialize solver objects
	if not params['tstep_first'] and params['tstep_rfirst']:
		params['tstep_first'] = ggtrial.physgrid.times[1] * params['tstep_rfirst']
	if (params['intlib']=='odeint'):
		from scipy.integrate import odeint
	elif (params['intlib'] in scipyodemethods):
		from scipy.integrate import ode
		oosolver = ode(func_inv, jacfunc_inv)
		kwargs = {}
		kwargs["first_step"] = params['tstep_first']
		if params['tstep_min']:
			kwargs["min_step"] = params['tstep_min']
		if params['tstep_max']:
			kwargs["max_step"] = params['tstep_max']
		if params['intlib'] == "lsoda":
			kwargs["max_order_ns"] = params['maxordns']
			kwargs["max_order_s"] = params['maxords']
		elif params['intlib'] == "vode":
			kwargs["method"] = "bdf"
			kwargs["order"] = params['maxords']
		if params['iter_atol']:
			kwargs["atol"] = params['iter_atol']
		if params['iter_rtol']:
			kwargs["rtol"] = params['iter_rtol']
		if params['iter_maxnum']:
			kwargs["nsteps"] = params['iter_maxnum']
		oosolver.set_integrator(params['intlib'], **kwargs)
	elif (params['intlib'] in gslodemethods):
		from pygsl import odeiv
		# http://stackoverflow.com/questions/16219259/converting-gsl-ode-solver-to-python
		# http://stackoverflow.com/a/16366525
		oosolver = type('oosolver', (), {})()
		N = len(ggtrial.solution.des)
		stepper = getattr(odeiv, "step_%s" % params['intlib'])
		oosolver.step = stepper(N, func_inv, jacfunc_inv, args=None)
		if not params['iter_atol']:
			params['iter_atol'] = 1e-6
		if not params['iter_rtol']:
			params['iter_rtol'] = 1e-6
		oosolver.control = odeiv.control_y_new(oosolver.step, params['iter_atol'], params['iter_rtol'])
		oosolver.evolve = odeiv.evolve(oosolver.step, oosolver.control, N)
		oosolver.jacobian = jacfunc_inv
		oosolver.algorithm = params['intlib']
	
	if params['quickrun']:
		# print warning message(s) related to physical parameters
		if params['ddensitydt']:
			print("WARNING! you requested a time-dependent density profile, but this is not supported by a quickrun!")
		# define ics - initial conditions
		ics = []
		for s in ggtrial.molecules.molecules:
			try:
				ics.append(s.density)
				#ics.append(s.fabund0)
			except KeyError as e:
				raise KeyError("density does not seem to be a defined molecular property!", e)
		# set static physical properties
		ics.append(ggtrial.physgrid.temperatures[0])
		density = ggtrial.physgrid.densities[0]
		ics.append(density)
		ics.append(ggtrial.physgrid.extinctions[0])
		if params['verbosity'] > 2:
			print("initial concentrations are:")
			pp = pprint.PrettyPrinter(indent=4)
			pp.pprint(ics)
		# directly call scipy's odeint
		if params['verbosity'] > 0: print("now running the solver...", end=' ')
		time_presolver = timer()
		# finally run the solver
		ggtrial.solution.has_run = True
		t0 = 0.0
		if (params['intlib'] == 'odeint'):
			print("via the LSODE from scipy.integrate.odeint")
			ggtrial.solution.solout, ggtrial.solution.solinfo = odeint(
				func, ics, ggtrial.physgrid.times, Dfun=jacfunc,
				full_output=True, ixpr=params['iter_printswitch'], printmessg=params['print_solverconvmsg'],
				col_deriv=False,
				args=(t0,),
				ml=None, mu=None,
				rtol=params['iter_rtol'], atol=params['iter_atol'],
				tcrit=None,
				h0=params['tstep_first'],
				hmax=params['tstep_max'], hmin=params['tstep_min'],
				mxstep=int(params['iter_maxnum']),
				mxhnil=params['iter_maxmsg'],
				mxordn=params['maxordns'], mxords=params['maxords'])
			ggtrial.solution.has_finished = True
			# process the solver's output
			if params['verbosity'] > 0: print("reformatting the solution output")
			for ((r,c), val) in np.ndenumerate(ggtrial.solution.solout[:,:-3]):
				ggtrial.molecules.molecules[c].fabunds.append(val/float(params['initialdensity']))
		elif (params['intlib'] in scipyodemethods):
			raise NotImplementedError("only scipy's odeint library is possible during a quickrun!")
			print("via the", params['intlib'], "method from scipy.integrate.ode")
			raise NotImplementedError("fix the numpy dtype issue without breaking the other two libraries!")
			oosolver.set_initial_value(ics, ggtrial.physgrid.times[0])
			ggtrial.solution.solout = []
			n = 0
			while oosolver.successful() and oosolver.t < ggtrial.physgrid.times[-1]:
				time_startstep = timer()
				dt = ggtrial.physgrid.times[n+1] - ggtrial.physgrid.times[n]
				if params['verbosity'] > 0: print("working on iteration #", n+1, "..", end=' ')
				oosolver.integrate(oosolver.t+dt)
				ggtrial.solution.solout.append(oosolver.y)
				for i,y in enumerate(oosolver.y[:-3]):
					ggtrial.molecules.molecules[i].fabunds.append(y/float(params['initialdensity']))
				time_endstep = timer()
				time_elapsed = time_endstep - time_startstep
				if params['verbosity'] > 0: print("..time for step was", time_elapsed)
				n += 1
			ggtrial.solution.has_finished = True
			print("")
		elif (params['intlib'] in gslodemethods):
			raise NotImplementedError("only scipy's odeint library is possible during a quickrun!")
			print("via the", params['intlib'], "method from GSL")
			y = np.asarray(ics, dtype=np.float64)
			ggtrial.solution.solout = []
			h = params['tstep_first']
			if not h:
				h = params['tstep_rfirst'] * ggtrial.physgrid.times[1]
			t = ggtrial.physgrid.times[0]
			while t < ggtrial.physgrid.times[-1]:
				t, h, y = oosolver.evolve.apply(t, ggtrial.physgrid.times[-1], h, y)
				ggtrial.solution.solout.append((t, y))
			ggtrial.solution.has_finished = True
			print("")
		else:
			raise NotImplementedError("unfortunately the integrator %s is not supported, sorry" % intlib)
		time_postsolver = timer()
		time_elapsed = time_postsolver - time_presolver
		if params['verbosity'] > 0: print("time for solver was", time_elapsed)
	# otherwise run the evolver
	else:
		ggtrial.solution.solout = []
		# initialize physical values
		prevtime = ggtrial.physgrid.times[0]
		prevdensity = ggtrial.physgrid.densities[0]
		for m in ggtrial.molecules.molecules:
			m.density = m.fabund0 * prevdensity
			m.fabunds.append(m.fabund0)
		###
		# loop through iterations
		for iteration,t in enumerate(ggtrial.physgrid.times):
			
			# define time at start of iteration
			time_startstep = timer()
			
			###
			# update physical grid
			#	set time
			currenttime = ggtrial.physgrid.times[iteration]
			#	set temperatures
			currenttemp = ggtrial.physgrid.temperatures[iteration]
			#	set densities
			currentdensity = ggtrial.physgrid.densities[iteration]
			#	set visual extinction
			currentav = ggtrial.physgrid.extinctions[iteration]
			
			if params['verbosity'] > 0:
				print("-"*10)
				print("working on iteration #", iteration)
				print("\tt =", currenttime/float(yr))
				print("\tT =", currenttemp)
				print("\tdensity =", currentdensity)
				print("\tav =", currentav)
			
			###
			# define ics - initial conditions (i.e. densities at start of timestep)
			#	update total density
			densitymultiplier = currentdensity / float(prevdensity)
			#	initialize container
			ics = []
			#	rescale all densities accordingly
			for m in ggtrial.molecules.molecules:
				ics.append(m.density * densitymultiplier)
			# update physical properties
			ics += [currenttemp, currentdensity, currentav]
			if params['verbosity'] > 2.5:
				print("ics at this iteration are:")
				pp = pprint.PrettyPrinter(indent=4)
				pp.pprint(ics)
			
			###
			# define time to integrate
			dt = currenttime - prevtime
			timestep = [0, dt]
			
			###
			# update integrator flags appropriate to the current iteration
			#	skip t=0, where dt=0
			if (currenttime == 0):
				# initialize all the integrator options
				h0 = params['tstep_first']
				continue
			elif (not params['tstep_adapt']) and params['tstep_rfirst']: # TODO: check this and what is below
				h0 = params['tstep_rfirst'] * dt
			else:
				try:
					h0 = ggtrial.solution.solinfo["hu"][0]/2.0
				except AttributeError:
					h0 = params['tstep_first']
			#	if relative internal time steps are specified..
			if (not params['tstep_min']) and params['tstep_rmin']:
				tstep_min = params['tstep_rmin'] * dt
			else:
				tstep_min = params['tstep_min']
			if (not params['tstep_max']) and params['tstep_rmax']:
				tstep_max = params['tstep_rmax'] * dt
			else:
				tstep_max = params['tstep_max']
			
			time_presolver = timer()
			time_elapsed = timer() - time_startstep
			if params['verbosity'] > 1: print("\ttime before solver was", time_elapsed)
			ggtrial.solution.has_run = True
			
			###
			# finally run the solver
			if (params['intlib'] == 'odeint'):
				# directly call scipy.integrate.odeint
				ggtrial.solution.solout, ggtrial.solution.solinfo = odeint(
					func, ics, timestep, Dfun=jacfunc,
					full_output=True, ixpr=params['iter_printswitch'], printmessg=params['print_solverconvmsg'],
					col_deriv=False,
					args=(prevtime,),
					ml=None, mu=None,
					rtol=params['iter_rtol'], atol=params['iter_atol'],
					tcrit=None,
					h0=h0, hmax=tstep_max, hmin=tstep_min,
					mxstep=int(params['iter_maxnum']),
					mxhnil=params['iter_maxmsg'],
					mxordn=params['maxordns'], mxords=params['maxords'])
				if (ggtrial.solution.solinfo['message']=="Integration successful.") and (params['verbosity'] > 0):
					print("\tIntegration was successful.")
					# process the solver's output
					for ((r,c), val) in np.ndenumerate(ggtrial.solution.solout[-1:,:-3]):
						ggtrial.molecules.molecules[c].density = val
						ggtrial.molecules.molecules[c].fabunds.append(val/float(currentdensity))
				elif (not ggtrial.solution.solinfo['message']=="Integration successful."):
					# be more careful if the normal integration was not successful:
					#	multiple chunks of smaller time steps
					#	(optionally) ignore the jacobian
					#	smaller internal time steps
					#	lower max order
					retry_count = 0
					if params['verbosity'] > 0: print("\twarning: integration was initially not successful..")
					retry_timestep_start = prevtime
					retry_timestep_final = currenttime
					# update ics
					retry_ics = list(ics)
					for i_m,m in enumerate(ggtrial.molecules.molecules):
						retry_ics[i_m] /= densitymultiplier
					if params['usejacforretries']:
						retry_jacfunc = jacfunc
					else:
						retry_jacfunc = None
					while (not ggtrial.solution.solinfo['message']=="Integration successful.") and (retry_count < params['max_retries']):
						retry_count += 1
						if params['verbosity'] > 0: print("\t\n..will try another attempt with relaxed constraints (retry #%s)" % retry_count)
						retry_h0 = h0 * 0.6**retry_count
						retry_tstep_max = tstep_max * 0.6**retry_count
						retry_iter_maxnum = int(params['iter_maxnum'] * 2**retry_count)
						retry_maxords = int(params['maxords']/2.0 + 1)
						num_of_intm_steps = retry_count + 1
						if (retry_count-1)==params['max_retries']:
							retry_jacfunc = None
							num_of_intm_steps = 2
						retry_dt = (retry_timestep_final - retry_timestep_start)/float(num_of_intm_steps)
						retry_timesteps = []
						for i_r in range(num_of_intm_steps + 1):
							retry_timesteps.append(retry_timestep_start + retry_dt * i_r)
						if params['verbosity'] > 2:
							print("local time steps will be: %s" % retry_timesteps)
						# loop through smaller time steps
						for i_r in range(num_of_intm_steps):
							if params['verbosity'] > 0:
								print("\t\tdoing intermediate step %s of %s" % (int(i_r+1), num_of_intm_steps))
							intm_prevtime = retry_timesteps[i_r]
							intm_currenttime = retry_timesteps[i_r+1]
							if params['verbosity'] > 1: print("\t\tstepping from", intm_prevtime/float(yr), "to", intm_currenttime/float(yr))
							intm_dt = intm_currenttime - intm_prevtime
							intm_timestep = [0, intm_dt]
							intm_temp = ggtrial.physgrid.gettemp(intm_currenttime)
							intm_dens = ggtrial.physgrid.getdens(intm_currenttime)
							intm_av = ggtrial.physgrid.getav(intm_currenttime)
							retry_ics[-3] = intm_temp
							retry_ics[-2] = intm_dens
							retry_ics[-1] = intm_av
							if params['verbosity'] > 2: print("\t\twhich now has (temp,dens,av):", intm_temp, intm_dens, intm_av)
							intm_densmultiplier = intm_dens / float(ggtrial.physgrid.getdens(intm_prevtime))
							for i_s,s in enumerate(ggtrial.molecules.molecules):
								retry_ics[i_s] *= intm_densmultiplier
							solout, solinfo = odeint(
								func, retry_ics, intm_timestep, Dfun=retry_jacfunc,
								full_output=True, ixpr=params['iter_printswitch'], printmessg=params['print_solverconvmsg'],
								col_deriv=False,
								args=(intm_prevtime,),
								ml=None, mu=None,
								rtol=params['iter_rtol'], atol=params['iter_atol'],
								tcrit=None,
								h0=retry_h0, hmax=retry_tstep_max, hmin=tstep_min,
								mxstep=retry_iter_maxnum,
								mxhnil=params['iter_maxmsg'],
								mxordn=params['maxordns'], mxords=retry_maxords)
							print("\t\t%s" % solinfo['message'])
							if (solinfo['message']=="Integration successful."):
								intm_prevtime = intm_currenttime
								retry_timestep_start = intm_currenttime
								for ((r,c), val) in np.ndenumerate(solout[-1:,:-3]):
									retry_ics[c] = val
							else:
								for i_s,s in enumerate(ggtrial.molecules.molecules):
									retry_ics[i_s] /= intm_densmultiplier
								break
						ggtrial.solution.solout, ggtrial.solution.solinfo = solout, solinfo
						if (solinfo['message']=="Integration successful."):
							for ((r,c), val) in np.ndenumerate(solout[-1:,:-3]):
								ggtrial.molecules.molecules[c].fabunds.append(val/float(intm_dens))
						elif retry_count==params['max_retries']:
							for i_s,s in enumerate(ggtrial.molecules.molecules):
								s.fabunds.append(0)
			elif (params['intlib'] in scipyodemethods):
				oosolver.set_initial_value(ics, 0).set_f_params(currenttime).set_jac_params(currenttime)
				dt = currenttime - prevtime
				oosolver.integrate(oosolver.t+dt)
				if not oosolver.successful():
					raise ArithmeticError("the integration was not successful!")
				ggtrial.solution.solout.append(oosolver.y)
				for i,y in enumerate(oosolver.y[:-3]):
					ggtrial.molecules.molecules[i].density = y
					ggtrial.molecules.molecules[i].fabunds.append(y/float(currentdensity))
			elif (params['intlib'] in gslodemethods):
				y = np.asarray(ics, dtype=np.float64)
				h = params['tstep_first']
				if not h:
					h = params['tstep_rfirst'] * dt
				print("dt is %.3e" % float(dt))
				print("h is %.3e" % float(h))
				print("currenttime is %.3e" % float(currenttime))
				t = prevtime
				while t < currenttime:
					if params['tstep_adapt']:
						h *= 1.1
					t, h, y = oosolver.evolve.apply(t, currenttime, h, y)
					print("t(yr) is %.3e" % float(t))
					if contains_nan(y):
						raise ArithmeticError("the solver output contained a NaN!")
				ggtrial.solution.solout.append(y)
				for i,val in enumerate(y[:-3]):
					ggtrial.molecules.molecules[i].density = val
					ggtrial.molecules.molecules[i].fabunds.append(val/float(currentdensity))
				ggtrial.solution.has_finished = True
			else:
				raise NotImplementedError("unfortunately this integrator is not supported yet, sorry..")
			
			time_postsolver = timer()
			time_elapsed = time_postsolver - time_presolver
			if params['verbosity'] > 1: print("\ttime for solver was", time_elapsed)
			
			###
			# update 'previous' values for next step
			prevtime = currenttime
			prevdensity = currentdensity
			
			# calculate timing for the end of the iteration
			time_endstep = timer()
			#time_elapsed = time_endstep - time_postsolver
			#if params['verbosity'] > 0: print "\ttime after solver was", time_elapsed
			time_elapsed = time_endstep - time_startstep
			if params['verbosity'] > 0: print("\ttime for step was", time_elapsed)
		
	print("finished running the solver")
	
	###
	# calculate timing at the end of the setup
	time_stopevolver = timer()
	time_elapsedfull = time_stopevolver - time_startevolver
	print("time for setup and solver was", time_elapsedfull)
	
	if params['verbosity'] >= 1:
		print("\nH2 goes like", ggtrial.getmol('H2').fabunds[:10])
		print("first and last entries of the final solution are:")
		for m in sorted(ggtrial.molecules.molecules, key=lambda x: x.name):
			print("\t%s: %s , %s ... %s" % (m.name, m.fabunds[0], m.fabunds[1], m.fabunds[-1]))
		print("top 10 final abundances are:")
		fabunds = []
		for m in ggtrial.molecules.molecules:
			fabunds.append((m.fabunds[-1], m.name))
		for m in sorted(fabunds, reverse=True)[:10]:
			print("\t%s: %s" % (m[1], m[0]))
		printsep()

def get_ggtrial():
	return ggtrial




def loadprevgg(trialdir, newverbosity=None, forcegui=False):
	r"""
	Loads into memory all the logged data of a previous trial, which can then be
	used for browsing with the Tk-based GUI.
	
	INPUTS:
	- ``trialdir`` -- a string pointing to the parent directory of a previously-run trial
	- ``newverbosity`` -- (optional) a new value for setting the verbosity
	
	EXAMPLES:
	- loadprevgg('output/trials/oxygen_trials/dense_20K_newO')
	"""
	# start timer for run
	time_startload = timer()
	
	# load previous param file
	logged_param = os.path.join(trialdir, 'gg_in_params.yml')
	params_fh = open(logged_param, 'r')
	new_params = yaml.load(params_fh)
	for k,v in list(new_params.items()):
		if isinstance(v, str):
			if v.lower() == "none": new_params[k] = None
			else:
				match = re.match(r'[\d\.]+[e][-]*[\d]+', v)
				if match: new_params[k] = float(v)
	params.update(new_params)
	
	# update the paths to the additional input files
	params['networkfname'] = os.path.join(trialdir, params['networkfname'])
	params['initialabundfname'] = os.path.join(trialdir, params['initialabundfname'])
	if ('initadjfname' in list(params.keys())) and (params['initadjfname'] is not None):
		params['initadjfname'] = os.path.join(trialdir, params['initadjfname'])
	else:
		params['initadjfname'] = None
	params['surfspeciesfname'] = os.path.join(trialdir, params['surfspeciesfname'])
	params['ssdatafname'] = os.path.join(trialdir, params['ssdatafname'])
	if newverbosity:
		params['verbosity'] = newverbosity
	
	# run initialization
	print("running the wrappers..")
	rungg(initonly=True, updatepaths=False)
	
	# load full abundance info
	printsep()
	logged_fabundfname = os.path.join(trialdir, 'ggo_fullabund.obj')
	if os.path.isfile(logged_fabundfname):
		print("loading the logged abundance information..")
		try:
			handle = open(logged_fabundfname, 'r')
			logged_fabunds = pickle.load(handle)
		except UnicodeDecodeError:	# this can happen when loading an old trial from a different version of python
			if not handle.closed:
				handle.close()
			if sys.version_info[0] == 3:
				handle = open(logged_fabundfname, 'rb')
				logged_fabunds = pickle.load(handle, encoding='latin1')
			else:
				raise NotImplementedError
		for s in ggtrial.molecules.get_names():
			ggtrial.getmol(s).fabunds = logged_fabunds[s]
		# check that the defined times match those that were loaded
		if not list(map(operator.eq, ggtrial.physgrid.times, logged_fabunds['times'])):
			raise IOError('ERROR: it appears the logged times do not match the reconstructed times!')
	else:
		print("WARNING: no logged abundance information was found.. this logged trial may be useless unless it is re-run")
	
	print("loading of the logged data seems complete!")
	ggtrial.solution.was_loaded = True
	time_elapsedload = timer() - time_startload
	print("time for loadprevgg() was", time_elapsedload)
	printsep()
	
	# load the gui
	if params['usegui'] or (forcegui is not None and forcegui):
		print("running the GUI now..")
		rungui()
	else:
		print("the GUI was not set to be loaded.. you can do this manually by running rungui()")
	
	if params['verbosity'] > 0: print("(WHILE TESTING) running final sanity checks on the reaction network")
	ggtrial.network.runfinalrxnsanitychecks()




def compare_networks(
	fname1, fname2,
	doreport_rtypecomp=False,
	doreport_printuniqrxns=False,
	doreport_printuniqmols=False,
	doreport_latex=False):
	r"""
	Loads into memory two network files, and provides a number of reports
	as comparison between the two.
	
	INPUTS:
	- ``fname1`` -- the first network file to load
	- ``fname2`` -- the second network file to load
	- ``blah`` -- (optional) an optional report about...
	
	EXAMPLES:
	- loadprevgg('gg_in_network_origS.d', 'gg_in_network_newS.d')
	"""
	if params['verbosity'] > 0: print("will run a comparison of two network files..")
	# start timer for run
	time_startload = timer()
	
	if params['verbosity'] > 0: print("\nloading the network files..")
	network1 = network(fname=fname1)
	network2 = network(fname=fname2)
	
	### group them by rtype.. isn't this pointless?
	if params['verbosity'] > 0: print("\ngrouping each of them according to rtype..")
	time_startrtype1 = timer()
	network1_byrtypes = {}
	for r in network1.reactions:
		rtype = r.rtype
		if not rtype in list(network1_byrtypes.keys()):
			network1_byrtypes[rtype] = []
		network1_byrtypes[rtype].append(r)
	network2_byrtypes = {}
	for r in network2.reactions:
		rtype = r.rtype
		if not rtype in list(network2_byrtypes.keys()):
			network2_byrtypes[rtype] = []
		network2_byrtypes[rtype].append(r)
	time_stoprtype1 = timer() - time_startrtype1
	
	
	if doreport_rtypecomp: # do reporting
		if params['verbosity'] > 0: print("\npreparing report for intersections/uniqueness..")
		# mutually-exlusive:
		isdisjoint = set(network1_byrtypes.keys()).isdisjoint(set(network2_byrtypes.keys()))
		# extras from X or Y:
		extrafrom1_rtype = set(network1_byrtypes.keys()).difference(set(network2_byrtypes.keys()))
		extrafrom2_rtype = set(network2_byrtypes.keys()).difference(set(network1_byrtypes.keys()))
		# intersections:
		perfectionintersection = (not len(extrafrom1_rtype)) and (not len(extrafrom2_rtype))
		intersection_rtype = set(network1_byrtypes.keys()).intersection(set(network2_byrtypes.keys()))
		if isdisjoint:
			print("the two networks contain no common rtypes!")
		if len(extrafrom1_rtype):
			print("network1 has rtypes=%s that are not in network2" % extrafrom1_rtype)
		if len(extrafrom2_rtype):
			print("network2 has rtypes=%s that are not in network1" % extrafrom2_rtype)
		if perfectionintersection:
			print("both network files contain the same rtypes: %s" % intersection_rtype)
	
	### make copy of networks, and use this for removing perfect duplicates
	if params['verbosity'] > 0: print("\nnow populating a list of unique rxns from each network..")
	network1_unique = network(None)
	network2_unique = network(None)
	for rxn in network1.reactions:
		duprxns = network2.filter( # filter by reaction
			attr="reaction", val=rxn.reaction,
			reactions=network2_byrtypes[rxn.rtype])
		if not len(duprxns): # add if reaction is missing completely..
			network1_unique.reactions.append(rxn)
		else: # check kinetics to see if they are different
			dupkinetics = network2.filter( # filter by k1
				attr="k1", val=rxn.k1,
				reactions=duprxns)
			dupkinetics = network2.filter( # filter by k2
				attr="k2", val=rxn.k2,
				reactions=dupkinetics)
			dupkinetics = network2.filter( # filter by k3
				attr="k3", val=rxn.k3,
				reactions=dupkinetics)
			if not len(dupkinetics):
				network1_unique.reactions.append(rxn)
	for rxn in network2.reactions:
		duprxns = network1.filter( # filter by reaction
			attr="reaction", val=rxn.reaction,
			reactions=network1_byrtypes[rxn.rtype])
		if not len(duprxns): # add if reaction is missing completely..
			network2_unique.reactions.append(rxn)
		else: # check kinetics to see if they are different
			dupkinetics = network1.filter( # filter by k1
				attr="k1", val=rxn.k1,
				reactions=duprxns)
			dupkinetics = network1.filter( # filter by k2
				attr="k2", val=rxn.k2,
				reactions=dupkinetics)
			dupkinetics = network1.filter( # filter by k3
				attr="k3", val=rxn.k3,
				reactions=dupkinetics)
			if not len(dupkinetics):
				network2_unique.reactions.append(rxn)
	if doreport_printuniqrxns:
		if len(network1_unique.reactions):
			print("found %s unique reactions from network1:" % len(network1_unique.reactions))
			for rxn in network1_unique.reactions:
				print("\t%s" % rxn)
		if len(network2_unique.reactions):
			print("found %s unique reactions from network2:" % len(network2_unique.reactions))
			for rxn in network2_unique.reactions:
				print("\t%s" % rxn)
	if doreport_printuniqmols:
		if params['verbosity'] > 0: print("\npreparing report for comparison of molecules..")
		network1.molecules = molecules(network1, network=network1)
		network2.molecules = molecules(network2, network=network2)
		# extras from X or Y:
		extrafrom1_mol = set(network1.molecules.get_names()).difference(set(network2.molecules.get_names()))
		extrafrom2_mol = set(network2.molecules.get_names()).difference(set(network1.molecules.get_names()))
		# intersections:
		perfectionintersection = (not len(extrafrom1_mol)) and (not len(extrafrom2_mol))
		intersection_mol = set(network1.molecules.get_names()).intersection(set(network2.molecules.get_names()))
		if len(extrafrom1_mol):
			print("network1 has %s molecules that are not in network2:" % len(extrafrom1_mol))
			for m in extrafrom1_mol:
				print("\t%s" % m)
		if len(extrafrom2_mol):
			print("network2 has %s molecules that are not in network1:" % len(extrafrom2_mol))
			for m in extrafrom2_mol:
				print("\t%s" % m)
		if perfectionintersection:
			print("both network files contain the same rtypes: %s" % intersection_mol)
	if not doreport_latex:
		return
	
	# loop through unique lists to combine new/old values
	network_rem = network(None)
	network_mod = network(None)
	network_new = network(None)
	for rxn in reversed(network1_unique.reactions):
		duplicates = network2_unique.filter(attr=["reactants", "products", "rtype"], val=[rxn.reactants, rxn.products, rxn.rtype])
		if len(duplicates):
			for r in duplicates:
				network2_unique.reactions.remove(r)
			network_mod.reactions += duplicates
		else:
			result = findupdatedcomment(oldrxn=rxn, newfile=fname2)
			if result is not None:
				rxn.comment = result
			network_rem.reactions.append(rxn)
		network1_unique.reactions.remove(rxn)
	network_new.reactions = network2_unique.reactions
	# now sort by reactants and rtype
	network_rem.sort(["-k1", "-comment", "reactants", "rtype"])
	network_mod.reactions = list(set(network_mod.reactions))
	network_mod.sort(["-k1", "-comment", "reactants", "rtype"])
	network_new.sort(["-k1", "-comment", "reactants", "rtype"])
	# now remove the reactive desorption entries (b/c we aren't using them, and they're basically duplicates)
	for n in [network_rem, network_mod, network_new]:
		for i in reversed(xrange(len(n.reactions))):
			rxn = n.reactions[i]
			if not rxn.rtype == 14: continue
			if not rxn.products[0][0] == "g":
				n.reactions.remove(rxn)
	# finally do latex table report (if desired)
	if not doreport_latex in (None, False):
		print("\ngenerating a report on network_rem (%s rxns)" % len(network_rem.reactions))
		network_rem.molecules = molecules(network_rem, network=network_rem)
		table_rem = network_to_deluxetable(
			network_rem, title="removed reactions",
			columns=["reaction", "rtype", ("k1", "exp"), "k2", ("k3", "gen"), ("comment", "simptext")],
			datalimit=None, label="table:removedrxns", tabletype=None, commentreftofootnote=True)
		
		print("\ngenerating a report on network_mod (%s rxns)" % len(network_mod.reactions))
		network_mod.molecules = molecules(network_mod, network=network_mod)
		table_mod = network_to_deluxetable(
			network_mod, title="modified reactions",
			columns=["reaction", "rtype", ("k1", "exp"), "k2", ("k3", "gen"), ("comment", "simptext")],
			datalimit=None, label="table:modifiedrxns", tabletype=None, commentreftofootnote=True)
		
		print("\ngenerating a report on network_new (%s rxns)" % len(network_new.reactions))
		network_new.molecules = molecules(network_new, network=network_new)
		table_new = network_to_deluxetable(
			network_new, title="new reactions",
			columns=["reaction", "rtype", ("k1", "exp"), "k2", ("k3", "gen"), ("comment", "simptext")],
			datalimit=None, label="table:newrxns", tabletype="longtable", commentreftofootnote=True)
		
		outputfile = open(doreport_latex, "w")
		outputfile.write(table_rem)
		outputfile.write("\n"*5)
		outputfile.write(table_mod)
		outputfile.write("\n"*5)
		outputfile.write(table_new)
		outputfile.close()






def rungui():
	import gg_lib_tk
	reload(gg_lib_tk)
	win_master = gg_lib_tk.ggtkgui(ggtrial)
	win_master.mainloop()




if __name__ == '__main__':
	
	showintro()
	
	gg14_memory_usage.postintro = gg14_memory_usage.get()
	if params['verbosity'] > 0: print("post-intro memory usage is", gg14_memory_usage.postintro)
	
	# define arguments
	parser = argparse.ArgumentParser()
	subparsers = parser.add_subparsers()
	### general (optional) flags
	parser.add_argument(
		"-d", "--debug", action='store_true', default=False,
		help="whether to add extra print messages to the terminal")
	parser.add_argument("--check", action='store_true', help="performs a check-only run")
	parser.add_argument("--profile", action='store_true', help="profiles a run")
	parser.add_argument("--ipython", action='store_true', help="activates ipython (actually, only instructs HOW to do so!)")
	parser.add_argument("--forcegui", action='store_true', help="whether to override the input parameter and show the GUI after all")
	### subcommands
	# run new trial
	p_run = subparsers.add_parser("run", help="performs a normal trial run")
	p_run.set_defaults(command="run")
	# load old trial
	p_load = subparsers.add_parser("load", help="loads an old trial")
	p_load.add_argument("OLDDIR", nargs=1, help="directory containing an old trial")
	p_load.set_defaults(command="load")
	# compare two networks
	p_comp = subparsers.add_parser("comp", help="compare two network files and prepare a report about it")
	p_comp.add_argument("NETWORK1", nargs=1, help="first network for comparison")
	p_comp.add_argument("NETWORK2", nargs=1, help="second network for comparison")
	p_comp.set_defaults(command="comp")
	p_comp.add_argument("--reportrtype", help="(optional) whether to print in terminal any difference in included rxn types")
	p_comp.add_argument("--reportuniquerxns", action='store_true', help="(optional) whether to print in terminal all the unique reactions from each network")
	p_comp.add_argument("--reportuniquemols", action='store_true', help="(optional) whether to print in terminal all the unique molecules from each network")
	p_comp.add_argument("--reportlatex", help="(optional) the output file for a latex report")
	
	# parse arguments and perform actions
	args = parser.parse_args()
	if args.debug:
		print("args were: %s" % args)
	params['checkonly'] = args.check
	if not "command" in args: # in py2, isn't automatically added to Namespace
		args.command = None
	if args.ipython:
		print("\nyou should try running:")
		print("ipython -i gg_main.py -- [args]")
	elif args.command == "run":
		if args.profile:
			runsnake('rungg()')
		else:
			rungg()
		if args.check and (not params['log_dir'] == "."):
			shutil.rmtree(params['log_fulldir']) # clean up output directory on clean exit
	elif args.command == "load":
		if args.profile:
			runsnake('loadprevgg(%s, forcegui=%s)' % (args.OLDDIR[0], args.forcegui))
		else:
			loadprevgg(args.OLDDIR[0], forcegui=args.forcegui)
	elif args.command == "comp":
		if not os.path.isfile(args.NETWORK1[0]):
			raise IOError("the first network file '%s' does not appear to be a file!" % args.NETWORK1[0])
		elif not os.path.isfile(args.NETWORK2[0]):
			raise IOError("the second network file '%s' does not appear to be a file!" % args.NETWORK2[0])
		compare_networks(
			fname1=args.NETWORK1[0], fname2=args.NETWORK2[0],
			doreport_rtypecomp=args.reportrtype,
			doreport_printuniqrxns=args.reportuniquerxns,
			doreport_printuniqmols=args.reportuniquemols,
			doreport_latex=args.reportlatex)
	else:
		print("you must specify an action ().. here's the help message (also available via -h/--help):")
		parser.print_help()
	
	sys.exit()


 H + H + G0 -> H2 + G0 								RTYPE=0 K1=4.95E-17 K2=5.0E-1 K3=0.0 #
 H + H + G1- -> H2 + G1- 							RTYPE=0 K1=4.95E-17 K2=5.0E-1 K3=0.0 #
 E1- + G0 -> G1- 									RTYPE=0 K1=6.9E-15 K2=5.0E-1 K3=0.0 #
 C1+ + G1- -> C + G0 								RTYPE=0 K1=4.9E-17 K2=5.0E-1 K3=0.0 #
 H1+ + G1- -> H + G0 								RTYPE=0 K1=1.7E-16 K2=5.0E-1 K3=0.0 #
 N1+ + G1- -> N + G0 								RTYPE=0 K1=4.7E-17 K2=5.0E-1 K3=0.0 #
 O1+ + G1- -> O + G0 								RTYPE=0 K1=4.4E-17 K2=5.0E-1 K3=0.0 #
 H31+ + G1- -> H2 + H + G0 							RTYPE=0 K1=1.0E-16 K2=5.0E-1 K3=0.0 #
 HCO1+ + G1- -> H + CO + G0 						RTYPE=0 K1=3.1E-17 K2=5.0E-1 K3=0.0 #
 C -> C1+ + E1- 									RTYPE=1 K1=1.02E+3 K2=0.0 K3=0.0 #
 CH2 -> CH21+ + E1- 								RTYPE=1 K1=5.0E+2 K2=0.0 K3=0.0 #
 CH2CO -> CH2CO1+ + E1- 							RTYPE=1 K1=1.22E+3 K2=0.0 K3=0.0 #
 CH3 -> CH31+ + E1- 								RTYPE=1 K1=5.0E+2 K2=0.0 K3=0.0 #
 CH3CHO -> CH3CHO1+ + E1- 							RTYPE=1 K1=1.12E+3 K2=0.0 K3=0.0 #
 CH3CN -> CH3CN1+ + E1- 							RTYPE=1 K1=2.24E+3 K2=0.0 K3=0.0 #
 CH3NH2 -> CH3NH21+ + E1- 							RTYPE=1 K1=1.12E+3 K2=0.0 K3=0.0 #
 CH3OCH3 -> CH3OCH31+ + E1- 						RTYPE=1 K1=1.12E+3 K2=0.0 K3=0.0 #
 CH3OH -> CH2OH1+ + H + E1- 						RTYPE=1 K1=4.95E+1 K2=0.0 K3=0.0 #
 CH3OH -> CH3OH1+ + E1- 							RTYPE=1 K1=1.44E+3 K2=0.0 K3=0.0 #
 CH3OH -> CH3O1+ + H + E1- 							RTYPE=1 K1=4.95E+1 K2=0.0 K3=0.0 #
 CO -> CO1+ + E1- 									RTYPE=1 K1=3.0 K2=0.0 K3=0.0 #
 H -> H1+ + E1- 									RTYPE=1 K1=4.6E-1 K2=0.0 K3=0.0 #
 H2 -> H1+ + H + E1- 								RTYPE=1 K1=2.2E-2 K2=0.0 K3=0.0 #
 H2 -> H21+ + E1- 									RTYPE=1 K1=9.3E-1 K2=0.0 K3=0.0 #
 HCO -> HCO1+ + E1- 								RTYPE=1 K1=1.17E+3 K2=0.0 K3=0.0 #
 HCOOH -> HCOOH1+ + E1- 							RTYPE=1 K1=6.5E+2 K2=0.0 K3=0.0 #
 HNO -> HNO1+ + E1- 								RTYPE=1 K1=1.03 K2=0.0 K3=0.0 #
 N -> N1+ + E1- 									RTYPE=1 K1=2.1 K2=0.0 K3=0.0 #
 NH2 -> NH21+ + E1- 								RTYPE=1 K1=6.5E+2 K2=0.0 K3=0.0 #
 NH3 -> NH31+ + E1- 								RTYPE=1 K1=5.75E+2 K2=0.0 K3=0.0 #
 NO -> NO1+ + E1- 									RTYPE=1 K1=4.94E+2 K2=0.0 K3=0.0 #
 O -> O1+ + E1- 									RTYPE=1 K1=2.8 K2=0.0 K3=0.0 #
 O2 -> O21+ + E1- 									RTYPE=1 K1=1.17E+2 K2=0.0 K3=0.0 #
 P -> P1+ + E1- 									RTYPE=1 K1=1.5E+3 K2=0.0 K3=0.0 #
 CH3CH2OH -> CH3CH2 + OH 							RTYPE=1 K1=1.5E+3 K2=0.0 K3=0.0 #
 CH3CH2OH -> CH3 + CH2OH 							RTYPE=1 K1=5.0E+2 K2=0.0 K3=0.0 #
 CH -> C + H 										RTYPE=1 K1=7.3E+2 K2=0.0 K3=0.0 #
 CH1+ -> C + H1+ 									RTYPE=1 K1=1.76E+2 K2=0.0 K3=0.0 #
 CH2 -> CH + H 										RTYPE=1 K1=5.0E+2 K2=0.0 K3=0.0 #
 CH2CO -> CH2 + CO 									RTYPE=1 K1=9.15E+2 K2=0.0 K3=0.0 #
 CH2NH -> CH2 + NH 									RTYPE=1 K1=4.98E+3 K2=0.0 K3=0.0 #
 CH2NH2 -> CH2 + NH2 								RTYPE=1 K1=9.5E+3 K2=0.0 K3=0.0 #
 CH2OH -> CH2 + OH 									RTYPE=1 K1=1.5E+3 K2=0.0 K3=0.0 #
 CH3 -> CH2 + H 									RTYPE=1 K1=5.0E+2 K2=0.0 K3=0.0 #
 CH3CHO -> CH3 + HCO 								RTYPE=1 K1=5.0E+2 K2=0.0 K3=0.0 #
 CH3CN -> CH3 + CN 									RTYPE=1 K1=4.76E+3 K2=0.0 K3=0.0 #
 CH3CO -> CH3 + CO 									RTYPE=1 K1=5.0E+2 K2=0.0 K3=0.0 #
 CH3NH -> CH2NH + H 								RTYPE=1 K1=9.5E+3 K2=0.0 K3=0.0 #
 CH3NH -> CH3 + NH 									RTYPE=1 K1=1.04 K2=0.0 K3=0.0 #
 CH3NH2 -> CH3 + NH2 								RTYPE=1 K1=1.04 K2=0.0 K3=0.0 #
 CH3O -> CH3 + O 									RTYPE=1 K1=3.0E+3 K2=0.0 K3=0.0 #
 CH3OCH3 -> CH3O + CH3 								RTYPE=1 K1=1.72E+3 K2=0.0 K3=0.0 #
 CH3OH -> CH3 + OH 									RTYPE=1 K1=1.5E+3 K2=0.0 K3=0.0 #
 CH3OH -> CH2OH + H 								RTYPE=1 K1=5.0E+2 K2=0.0 K3=0.0 #
 CH3OH -> CH3O + H 									RTYPE=1 K1=5.0E+2 K2=0.0 K3=0.0 #
 CH4 -> CH2 + H2 									RTYPE=1 K1=2.34E+3 K2=0.0 K3=0.0 #
 HCNH -> CH + NH 									RTYPE=1 K1=1.03 K2=0.0 K3=0.0 #
 HCOH -> CH + OH 									RTYPE=1 K1=0.0 K2=0.0 K3=0.0 #
 CN -> C + N 										RTYPE=1 K1=1.06E+4 K2=0.0 K3=0.0 #
 CO -> C + O 										RTYPE=1 K1=5.0 K2=0.0 K3=0.0 #
 CO2 -> CO + O 										RTYPE=1 K1=1.71E+3 K2=0.0 K3=0.0 #
 COCHO -> HCO + CO 									RTYPE=1 K1=5.0E+2 K2=0.0 K3=0.0 #
 COOH -> CO + OH 									RTYPE=1 K1=1.5E+3 K2=0.0 K3=0.0 #
 H2 -> H + H 										RTYPE=1 K1=1.0E-1 K2=0.0 K3=0.0 #
 H2 -> H1+ + H1- 									RTYPE=1 K1=3.0E-4 K2=0.0 K3=0.0 #
 H2CN -> HCN + H 									RTYPE=1 K1=1.03 K2=0.0 K3=0.0 #
 H2CO -> HCO + H 									RTYPE=1 K1=1.33E+3 K2=0.0 K3=0.0 #
 H2CO -> CO + H2 									RTYPE=1 K1=1.33E+3 K2=0.0 K3=0.0 #
 H2O -> OH + H 										RTYPE=1 K1=9.7E+2 K2=0.0 K3=0.0 #
 H2O2 -> OH + OH 									RTYPE=1 K1=1.5E+3 K2=0.0 K3=0.0 #
 HCN -> CN + H 										RTYPE=1 K1=3.12E+3 K2=0.0 K3=0.0 #
 HCO -> CO + H 										RTYPE=1 K1=4.21E+2 K2=0.0 K3=0.0 #
 HCOOCH3 -> HCO + CH3O 								RTYPE=1 K1=5.0E+2 K2=0.0 K3=0.0 #
 HCOOH -> HCO + OH 									RTYPE=1 K1=2.49E+2 K2=0.0 K3=0.0 #
 CH3COCHO -> CH3CO + HCO 							RTYPE=1 K1=5.0E+2 K2=0.0 K3=0.0 #
 CH3COCH3 -> CH3CO + CH3 							RTYPE=1 K1=5.0E+2 K2=0.0 K3=0.0 #
 CH3CONH -> CH3CO + NH 								RTYPE=1 K1=1.04 K2=0.0 K3=0.0 #
 CH3CONH -> CH3 + HNCO 								RTYPE=1 K1=5.0E+2 K2=0.0 K3=0.0 #
 CH3COOH -> CH3 + COOH 								RTYPE=1 K1=5.0E+2 K2=0.0 K3=0.0 #
 CH3COOH -> CH3CO + OH 								RTYPE=1 K1=1.5E+3 K2=0.0 K3=0.0 #
 CH3CONH2 -> CH3 + NH2CO 							RTYPE=1 K1=5.0E+2 K2=0.0 K3=0.0 #
 CH3CONH2 -> CH3CO + NH2 							RTYPE=1 K1=1.04 K2=0.0 K3=0.0 #
 HNC -> CN + H 										RTYPE=1 K1=3.0E+3 K2=0.0 K3=0.0 #
 HNCHO -> NH + HCO 									RTYPE=1 K1=1.04 K2=0.0 K3=0.0 #
 HNCO -> NH + CO 									RTYPE=1 K1=6.0E+3 K2=0.0 K3=0.0 #
 HNCOCHO -> HNCO + HCO 								RTYPE=1 K1=5.0E+2 K2=0.0 K3=0.0 #
 HNCOCHO -> NH + COCHO 								RTYPE=1 K1=1.04 K2=0.0 K3=0.0 #
 HNCONH -> NH + HNCO 								RTYPE=1 K1=1.04 K2=0.0 K3=0.0 #
 HNCOOH -> COOH + NH 								RTYPE=1 K1=1.04 K2=0.0 K3=0.0 #
 HNCOOH -> OH + HNCO 								RTYPE=1 K1=1.5E+3 K2=0.0 K3=0.0 #
 HNO -> NH + O 										RTYPE=1 K1=1.03 K2=0.0 K3=0.0 #
 HNO -> NO + H 										RTYPE=1 K1=1.03 K2=0.0 K3=0.0 #
 HNOH -> NH + OH 									RTYPE=1 K1=1.03 K2=0.0 K3=0.0 #
 CH3ONH -> NH + CH3O 								RTYPE=1 K1=1.03 K2=0.0 K3=0.0 #
 HNCH2OH -> NH + CH2OH 								RTYPE=1 K1=1.04 K2=0.0 K3=0.0 #
 HOC -> CO + H 										RTYPE=1 K1=0.0 K2=0.0 K3=0.0 #
 HOCOOH -> OH + COOH 								RTYPE=1 K1=1.5E+3 K2=0.0 K3=0.0 #
 HPO -> PO + H 										RTYPE=1 K1=1.5E+3 K2=0.0 K3=0.0 #
 HCOCOCHO -> HCO + COCHO 							RTYPE=1 K1=5.0E+2 K2=0.0 K3=0.0 #
 N2 -> N + N 										RTYPE=1 K1=5.0 K2=0.0 K3=0.0 #
 N2H2 -> NH + NH 									RTYPE=1 K1=2.0E+2 K2=0.0 K3=0.0 #
 N2O -> NO + N 										RTYPE=1 K1=1.5E+3 K2=0.0 K3=0.0 #
 NH -> N + H 										RTYPE=1 K1=5.0E+2 K2=0.0 K3=0.0 #
 NH2 -> NH + H 										RTYPE=1 K1=8.0E+1 K2=0.0 K3=0.0 #
 NH2CHO -> NH2 + HCO 								RTYPE=1 K1=3.0E+3 K2=0.0 K3=0.0 #
 NH2CN -> NH2 + CN 									RTYPE=1 K1=9.5E+3 K2=0.0 K3=0.0 #
 NH2OH -> NH2 + OH 									RTYPE=1 K1=3.0E+3 K2=0.0 K3=0.0 #
 NH3 -> NH + H2 									RTYPE=1 K1=5.4E+2 K2=0.0 K3=0.0 #
 NH3 -> NH2 + H 									RTYPE=1 K1=1.32E+3 K2=0.0 K3=0.0 #
 NO -> N + O 										RTYPE=1 K1=4.82E+2 K2=0.0 K3=0.0 #
 NO2 -> NO + O 										RTYPE=1 K1=1.5E+3 K2=0.0 K3=0.0 #
 O2 -> O + O 										RTYPE=1 K1=7.5E+2 K2=0.0 K3=0.0 #
 HO2 -> O + OH 										RTYPE=1 K1=7.5E+2 K2=0.0 K3=0.0 #
 HO2 -> O2 + H 										RTYPE=1 K1=7.5E+2 K2=0.0 K3=0.0 #
 O3 -> O2 + O 										RTYPE=1 K1=1.5E+3 K2=0.0 K3=0.0 #
 OCN -> CN + O 										RTYPE=1 K1=1.5E+3 K2=0.0 K3=0.0 #
 OH -> O + H 										RTYPE=1 K1=5.1E+2 K2=0.0 K3=0.0 #
 CHOCHO -> HCO + HCO 								RTYPE=1 K1=5.0E+2 K2=0.0 K3=0.0 #
 HCOCOOH -> HCO + COOH 								RTYPE=1 K1=5.0E+2 K2=0.0 K3=0.0 #
 HCOCOOH -> COCHO + OH 								RTYPE=1 K1=1.5E+3 K2=0.0 K3=0.0 #
 PN -> P + N 										RTYPE=1 K1=5.0E+2 K2=0.0 K3=0.0 #
 PO -> P + O 										RTYPE=1 K1=5.0E+2 K2=0.0 K3=0.0 #
 NH2CO -> NH2 + CO 									RTYPE=1 K1=1.04 K2=0.0 K3=0.0 #
 NH2COCHO -> NH2 + COCHO 							RTYPE=1 K1=1.04 K2=0.0 K3=0.0 #
 NH2COCHO -> NH2CO + HCO 							RTYPE=1 K1=5.0E+2 K2=0.0 K3=0.0 #
 NH2CONH -> NH2 + HNCO 								RTYPE=1 K1=1.04 K2=0.0 K3=0.0 #
 NH2CONH -> NH2CO + NH 								RTYPE=1 K1=1.04 K2=0.0 K3=0.0 #
 NH2COOH -> NH2CO + OH 								RTYPE=1 K1=1.5E+3 K2=0.0 K3=0.0 #
 NH2COOH -> NH2 + COOH 								RTYPE=1 K1=1.04 K2=0.0 K3=0.0 #
 NH2CONH2 -> NH2 + NH2CO 							RTYPE=1 K1=1.04 K2=0.0 K3=0.0 #
 NH2NH -> NH + NH2 									RTYPE=1 K1=2.0E+2 K2=0.0 K3=0.0 #
 NH2NH2 -> NH2 + NH2 								RTYPE=1 K1=2.0E+2 K2=0.0 K3=0.0 #
 NH2OCH3 -> NH2 + CH3O 								RTYPE=1 K1=1.03 K2=0.0 K3=0.0 #
 NH2CH2OH -> NH2 + CH2OH 							RTYPE=1 K1=1.04 K2=0.0 K3=0.0 #
 CH3OCO -> CH3O + CO 								RTYPE=1 K1=5.0E+2 K2=0.0 K3=0.0 #
 CH3OCONH -> CH3O + HNCO 							RTYPE=1 K1=5.0E+2 K2=0.0 K3=0.0 #
 CH3OCONH -> CH3OCO + NH 							RTYPE=1 K1=1.04 K2=0.0 K3=0.0 #
 CH3OCOOH -> CH3O + COOH 							RTYPE=1 K1=5.0E+2 K2=0.0 K3=0.0 #
 CH3OCOOH -> CH3OCO + OH 							RTYPE=1 K1=1.5E+3 K2=0.0 K3=0.0 #
 CH3OOH -> OH + CH3O 								RTYPE=1 K1=1.03 K2=0.0 K3=0.0 #
 CH3OOCH3 -> CH3O + CH3O 							RTYPE=1 K1=1.03 K2=0.0 K3=0.0 #
 CH2OHCHO -> CH2OH + HCO 							RTYPE=1 K1=5.0E+2 K2=0.0 K3=0.0 #
 HOCH2CO -> CH2OH + CO 								RTYPE=1 K1=5.0E+2 K2=0.0 K3=0.0 #
 HOCH2OH -> OH + CH2OH 								RTYPE=1 K1=1.5E+3 K2=0.0 K3=0.0 #
 C1+ + CH3CH2 -> CH3CH21+ + C 						RTYPE=2 K1=6.65E-10 K2=-5.0E-1 K3=0.0 #
 C1+ + CH3CH2OH -> CH3CH2O1+ + CH 					RTYPE=2 K1=7.07E-10 K2=-5.0E-1 K3=0.0 #
 C1+ + CH3CH3 -> CH3CH21+ + CH 						RTYPE=2 K1=1.7E-10 K2=0.0 K3=0.0 #
 C1+ + CH -> CH1+ + C 								RTYPE=2 K1=2.7E-9 K2=-5.0E-1 K3=0.0 #
 C1+ + CH2 -> CH21+ + C 							RTYPE=2 K1=4.34E-10 K2=-5.0E-1 K3=0.0 #
 C1+ + CH2CO -> CH2CO1+ + C 						RTYPE=2 K1=1.8E-9 K2=-5.0E-1 K3=0.0 #
 C1+ + CH2OH -> CH21+ + HCO 						RTYPE=2 K1=1.61E-9 K2=-5.0E-1 K3=0.0 #
 C1+ + CH3 -> CH31+ + C 							RTYPE=2 K1=1.0E-9 K2=0.0 K3=0.0 #
 C1+ + CH3CHO -> CH3CHO1+ + C 						RTYPE=2 K1=1.7E-9 K2=-5.0E-1 K3=0.0 #
 C1+ + CH3CHO -> CH3CO1+ + CH 						RTYPE=2 K1=1.7E-9 K2=-5.0E-1 K3=0.0 #
 C1+ + CH3CO -> CH3CO1+ + C 						RTYPE=2 K1=2.37E-9 K2=-5.0E-1 K3=0.0 #
 C1+ + CH3NH -> CH3NH1+ + C 						RTYPE=2 K1=1.51E-9 K2=-5.0E-1 K3=0.0 #
 C1+ + CH3NH2 -> CH3NH21+ + C 						RTYPE=2 K1=1.1E-9 K2=-5.0E-1 K3=0.0 #
 C1+ + CH3NH2 -> CH3NH1+ + CH 						RTYPE=2 K1=6.2E-10 K2=-5.0E-1 K3=0.0 #
 C1+ + CH3O -> CH3O1+ + C 							RTYPE=2 K1=2.46E-10 K2=-5.0E-1 K3=0.0 #
 C1+ + CH3OCH3 -> CH3OCH31+ + C 					RTYPE=2 K1=1.6E-9 K2=-5.0E-1 K3=0.0 #
 C1+ + CH3OH -> CH31+ + HCO 						RTYPE=2 K1=6.9E-10 K2=-5.0E-1 K3=0.0 #
 C1+ + CH3OH -> CH3O1+ + CH 						RTYPE=2 K1=6.9E-10 K2=-5.0E-1 K3=0.0 #
 C1+ + CH3OH -> CH3OH1+ + C 						RTYPE=2 K1=8.1E-10 K2=-5.0E-1 K3=0.0 #
 C1+ + CO2 -> CO1+ + CO 							RTYPE=2 K1=1.1E-9 K2=0.0 K3=0.0 #
 C1+ + COCHO -> COCHO1+ + C 						RTYPE=2 K1=1.05E-9 K2=-5.0E-1 K3=0.0 #
 C1+ + COOH -> COOH1+ + C 							RTYPE=2 K1=3.69E-9 K2=-5.0E-1 K3=0.0 #
 C1+ + H2CO -> HCO1+ + CH 							RTYPE=2 K1=6.5E-10 K2=-5.0E-1 K3=0.0 #
 C1+ + H2CO -> H2CO1+ + C 							RTYPE=2 K1=9.6E-10 K2=-5.0E-1 K3=0.0 #
 C1+ + H2CO -> CH21+ + CO 							RTYPE=2 K1=1.5E-9 K2=-5.0E-1 K3=0.0 #
 C1+ + H2O -> HCO1+ + H 							RTYPE=2 K1=8.9E-10 K2=-5.0E-1 K3=0.0 #
 C1+ + H2O -> HOC1+ + H 							RTYPE=2 K1=1.8E-9 K2=-5.0E-1 K3=0.0 #
 C1+ + HCN -> CNC1+ + H 							RTYPE=2 K1=4.75E-9 K2=-5.0E-1 K3=0.0 #
 C1+ + HCO -> CH1+ + CO 							RTYPE=2 K1=6.7E-10 K2=-5.0E-1 K3=0.0 #
 C1+ + HCO -> HCO1+ + C 							RTYPE=2 K1=6.7E-10 K2=-5.0E-1 K3=0.0 #
 C1+ + HCOOCH3 -> HCOOCH31+ + C 					RTYPE=2 K1=2.17E-9 K2=-5.0E-1 K3=0.0 #
 C1+ + HCOOH -> HCOOH1+ + C 						RTYPE=2 K1=1.44E-9 K2=-5.0E-1 K3=0.0 #
 C1+ + CH3CONH -> CH3CONH1+ + C 					RTYPE=2 K1=3.78E-9 K2=-5.0E-1 K3=0.0 #
 C1+ + CH3COOH -> CH3COOH1+ + C 					RTYPE=2 K1=1.72E-9 K2=-5.0E-1 K3=0.0 #
 C1+ + HNCHO -> HNCHO1+ + C 						RTYPE=2 K1=2.09E-9 K2=-5.0E-1 K3=0.0 #
 C1+ + HNCO -> HNCO1+ + C 							RTYPE=2 K1=2.16E-9 K2=-5.0E-1 K3=0.0 #
 C1+ + HNCOCHO -> HNCOCHO1+ + C 					RTYPE=2 K1=1.01E-9 K2=-5.0E-1 K3=0.0 #
 C1+ + HNCONH -> NHCONH1+ + C 						RTYPE=2 K1=3.94E-9 K2=-5.0E-1 K3=0.0 #
 C1+ + HNCOOH -> HNCOOH1+ + C 						RTYPE=2 K1=2.03E-9 K2=-5.0E-1 K3=0.0 #
 C1+ + HNOH -> HNOH1+ + C 							RTYPE=2 K1=2.14E-9 K2=-5.0E-1 K3=0.0 #
 C1+ + CH3ONH -> CH3ONH1+ + C 						RTYPE=2 K1=1.52E-9 K2=-5.0E-1 K3=0.0 #
 C1+ + HNCH2OH -> CH2OHNH1+ + C 					RTYPE=2 K1=1.52E-9 K2=-5.0E-1 K3=0.0 #
 C1+ + HOCOOH -> HOCOOH1+ + C 						RTYPE=2 K1=2.01E-9 K2=-5.0E-1 K3=0.0 #
 C1+ + HPO -> HPO1+ + C 							RTYPE=2 K1=6.9E-9 K2=-5.0E-1 K3=0.0 #
 C1+ + NH -> CN1+ + H 								RTYPE=2 K1=4.6E-9 K2=-5.0E-1 K3=0.0 #
 C1+ + NH2 -> HCN1+ + H 							RTYPE=2 K1=2.7E-9 K2=-5.0E-1 K3=0.0 #
 C1+ + NH2CHO -> H2CO1+ + HCN 						RTYPE=2 K1=1.85E-9 K2=-5.0E-1 K3=0.0 #
 C1+ + NH2CHO -> HCN1+ + H2CO 						RTYPE=2 K1=1.85E-9 K2=-5.0E-1 K3=0.0 #
 C1+ + NH2CHO -> CH3CN1+ + O 						RTYPE=2 K1=1.85E-9 K2=-5.0E-1 K3=0.0 #
 C1+ + NH2CHO -> CH3CO1+ + N 						RTYPE=2 K1=1.85E-9 K2=-5.0E-1 K3=0.0 #
 C1+ + NH2CHO -> CH3O1+ + CN 						RTYPE=2 K1=1.85E-9 K2=-5.0E-1 K3=0.0 #
 C1+ + NH2OH -> NH2OH1+ + C 						RTYPE=2 K1=6.23E-10 K2=-5.0E-1 K3=0.0 #
 C1+ + NH3 -> HCN1+ + H2 							RTYPE=2 K1=1.08E-10 K2=-5.0E-1 K3=0.0 #
 C1+ + NH3 -> H2CN1+ + H 							RTYPE=2 K1=1.36E-9 K2=-5.0E-1 K3=0.0 #
 C1+ + NH3 -> NH31+ + C 							RTYPE=2 K1=6.8E-10 K2=-5.0E-1 K3=0.0 #
 C1+ + NO -> NO1+ + C 								RTYPE=2 K1=4.8E-10 K2=-5.0E-1 K3=0.0 #
 C1+ + O2 -> CO1+ + O 								RTYPE=2 K1=7.5E-10 K2=0.0 K3=0.0 #
 C1+ + O2 -> O1+ + CO 								RTYPE=2 K1=4.1E-10 K2=0.0 K3=0.0 #
 C1+ + OCN -> CO1+ + CN 							RTYPE=2 K1=1.9E-9 K2=-5.0E-1 K3=0.0 #
 C1+ + OH -> CO1+ + H 								RTYPE=2 K1=2.9E-9 K2=-3.33E-1 K3=0.0 #
 C1+ + CHOCHO -> CHOCHO1+ + C 						RTYPE=2 K1=7.22E-10 K2=0.0 K3=0.0 #
 C1+ + HCOCOOH -> HCOCOOH1+ + C 					RTYPE=2 K1=9.98E-10 K2=-5.0E-1 K3=0.0 #
 C1+ + P -> P1+ + C 								RTYPE=2 K1=1.0E-9 K2=0.0 K3=0.0 #
 C1+ + PO -> PO1+ + C 								RTYPE=2 K1=5.58E-9 K2=-5.0E-1 K3=0.0 #
 C1+ + NH2CO -> NH2CO1+ + C 						RTYPE=2 K1=2.09E-9 K2=-5.0E-1 K3=0.0 #
 C1+ + NH2CONH -> NH2CONH1+ + C 					RTYPE=2 K1=3.88E-9 K2=-5.0E-1 K3=0.0 #
 C1+ + NH2COOH -> NH2COOH1+ + C 					RTYPE=2 K1=1.99E-9 K2=-5.0E-1 K3=0.0 #
 C1+ + NH2NH -> NH2NH1+ + C 						RTYPE=2 K1=1.99E-9 K2=-5.0E-1 K3=0.0 #
 C1+ + NH2NH2 -> NH2NH21+ + C 						RTYPE=2 K1=1.99E-9 K2=-5.0E-1 K3=0.0 #
 C1+ + NH2OCH3 -> NH2OCH31+ + C 					RTYPE=2 K1=2.01E-9 K2=-5.0E-1 K3=0.0 #
 C1+ + CH3OCO -> CH3OCO1+ + C 						RTYPE=2 K1=1.77E-9 K2=-5.0E-1 K3=0.0 #
 C1+ + CH3OOH -> CH3OOH1+ + C 						RTYPE=2 K1=1.72E-9 K2=-5.0E-1 K3=0.0 #
 C1+ + HOCH2CO -> HOCH2CO1+ + C 					RTYPE=2 K1=2.72E-9 K2=-5.0E-1 K3=0.0 #
 C1+ + HOCH2OH -> HOCH2OH1+ + C 					RTYPE=2 K1=3.04E-9 K2=-5.0E-1 K3=0.0 #
 CH3CH21+ + O -> HCO1+ + CH4 						RTYPE=2 K1=1.0E-11 K2=0.0 K3=0.0 #
 CH3CH21+ + O -> CH3CHO1+ + H 						RTYPE=2 K1=1.0E-11 K2=0.0 K3=0.0 #
 CH3CH31+ + H -> CH3CH21+ + H2 						RTYPE=2 K1=1.0E-10 K2=0.0 K3=0.0 #
 NCCN1+ + H -> HNC1+ + CN 							RTYPE=2 K1=5.0E-10 K2=0.0 K3=0.0 #
 CH1+ + CH3OH -> H2CO1+ + CH3 						RTYPE=2 K1=1.45E-9 K2=0.0 K3=0.0 #
 CH1+ + CH3OH -> CH3O1+ + CH2 						RTYPE=2 K1=2.9E-10 K2=0.0 K3=0.0 #
 CH1+ + CH3OH -> CH3OH21+ + C 						RTYPE=2 K1=1.16E-9 K2=0.0 K3=0.0 #
 CH1+ + CN -> CNC1+ + H 							RTYPE=2 K1=5.5E-10 K2=0.0 K3=0.0 #
 CH1+ + CO2 -> HCO1+ + CO 							RTYPE=2 K1=1.6E-9 K2=0.0 K3=0.0 #
 CH1+ + H -> C1+ + H2 								RTYPE=2 K1=7.5E-10 K2=0.0 K3=0.0 #
 CH1+ + H2 -> CH21+ + H 							RTYPE=2 K1=1.2E-9 K2=0.0 K3=0.0 #
 CH1+ + H2CO -> CH2OH1+ + C 						RTYPE=2 K1=9.6E-10 K2=0.0 K3=0.0 #
 CH1+ + H2CO -> HCO1+ + CH2 						RTYPE=2 K1=9.6E-10 K2=0.0 K3=0.0 #
 CH1+ + H2CO -> CH31+ + CO 							RTYPE=2 K1=9.6E-10 K2=0.0 K3=0.0 #
 CH1+ + H2O -> HCO1+ + H2 							RTYPE=2 K1=2.9E-9 K2=0.0 K3=0.0 #
 CH1+ + H2O -> H2CO1+ + H 							RTYPE=2 K1=5.8E-10 K2=0.0 K3=0.0 #
 CH1+ + H2O -> H3O1+ + C 							RTYPE=2 K1=5.8E-10 K2=0.0 K3=0.0 #
 CH1+ + HCN -> H2CN1+ + C 							RTYPE=2 K1=2.4E-9 K2=0.0 K3=0.0 #
 CH1+ + HCO -> HCO1+ + CH 							RTYPE=2 K1=4.6E-10 K2=0.0 K3=0.0 #
 CH1+ + HCO -> CH21+ + CO 							RTYPE=2 K1=4.6E-10 K2=0.0 K3=0.0 #
 CH1+ + HNC -> H2CN1+ + C 							RTYPE=2 K1=1.8E-9 K2=0.0 K3=0.0 #
 CH1+ + N -> CN1+ + H 								RTYPE=2 K1=1.9E-10 K2=0.0 K3=0.0 #
 CH1+ + NH -> CN1+ + H2 							RTYPE=2 K1=7.6E-10 K2=0.0 K3=0.0 #
 CH1+ + NH2 -> HCN1+ + H2 							RTYPE=2 K1=1.1E-9 K2=0.0 K3=0.0 #
 CH1+ + NH3 -> H2NC1+ + H2 							RTYPE=2 K1=1.84E-9 K2=0.0 K3=0.0 #
 CH1+ + NH3 -> NH31+ + CH 							RTYPE=2 K1=4.59E-10 K2=0.0 K3=0.0 #
 CH1+ + NH3 -> NH41+ + C 							RTYPE=2 K1=4.05E-10 K2=0.0 K3=0.0 #
 CH1+ + NO -> NO1+ + CH 							RTYPE=2 K1=7.6E-10 K2=0.0 K3=0.0 #
 CH1+ + O -> CO1+ + H 								RTYPE=2 K1=3.5E-10 K2=0.0 K3=0.0 #
 CH1+ + O2 -> CO1+ + OH 							RTYPE=2 K1=1.0E-11 K2=0.0 K3=0.0 #
 CH1+ + O2 -> HCO1+ + O 							RTYPE=2 K1=9.7E-10 K2=0.0 K3=0.0 #
 CH1+ + O2 -> HCO + O1+ 							RTYPE=2 K1=1.0E-11 K2=0.0 K3=0.0 #
 CH1+ + OH -> CO1+ + H2 							RTYPE=2 K1=7.5E-10 K2=0.0 K3=0.0 #
 CH21+ + CH4 -> CH3CH21+ + H 						RTYPE=2 K1=3.6E-10 K2=0.0 K3=0.0 #
 CH21+ + CO2 -> H2CO1+ + CO 						RTYPE=2 K1=1.2E-9 K2=0.0 K3=0.0 #
 CH21+ + H2 -> CH31+ + H 							RTYPE=2 K1=1.2E-9 K2=0.0 K3=0.0 #
 CH21+ + H2CO -> CH3CO1+ + H 						RTYPE=2 K1=3.3E-10 K2=0.0 K3=0.0 #
 CH21+ + H2CO -> HCO1+ + CH3 						RTYPE=2 K1=2.8E-9 K2=0.0 K3=0.0 #
 CH21+ + H2CO -> CH2CO1+ + H2 						RTYPE=2 K1=1.65E-10 K2=0.0 K3=0.0 #
 CH21+ + H2O -> CH2OH1+ + H 						RTYPE=2 K1=1.2E-9 K2=0.0 K3=0.0 #
 CH21+ + HCO -> CH31+ + CO 							RTYPE=2 K1=4.5E-10 K2=0.0 K3=0.0 #
 CH21+ + HCO -> CH2CO1+ + H 						RTYPE=2 K1=4.5E-10 K2=0.0 K3=0.0 #
 CH21+ + N -> HCN1+ + H 							RTYPE=2 K1=2.2E-10 K2=0.0 K3=0.0 #
 CH21+ + NH -> H2CN1+ + H 							RTYPE=2 K1=7.5E-10 K2=0.0 K3=0.0 #
 CH21+ + NH2 -> H2CN1+ + H2 						RTYPE=2 K1=1.0E-9 K2=0.0 K3=0.0 #
 CH21+ + NH3 -> NH41+ + CH 							RTYPE=2 K1=1.3E-9 K2=0.0 K3=0.0 #
 CH21+ + NH3 -> CH3NH1+ + H 						RTYPE=2 K1=1.5E-9 K2=0.0 K3=0.0 #
 CH21+ + NO -> NO1+ + CH2 							RTYPE=2 K1=4.2E-10 K2=0.0 K3=0.0 #
 CH21+ + O -> HCO1+ + H 							RTYPE=2 K1=7.5E-10 K2=0.0 K3=0.0 #
 CH21+ + O2 -> HCO1+ + OH 							RTYPE=2 K1=9.1E-10 K2=0.0 K3=0.0 #
 CH21+ + O2 -> HOCO1+ + H 							RTYPE=2 K1=4.7E-10 K2=0.0 K3=0.0 #
 CH21+ + OH -> H2CO1+ + H 							RTYPE=2 K1=7.4E-10 K2=0.0 K3=0.0 #
 CH2OH1+ + CH -> CH21+ + H2CO 						RTYPE=2 K1=4.4E-9 K2=-5.0E-1 K3=0.0 #
 CH2OH1+ + HNC -> H2CN1+ + H2CO 					RTYPE=2 K1=6.52E-9 K2=-5.0E-1 K3=0.0 #
 CH2OH1+ + NH2 -> NH31+ + H2CO 						RTYPE=2 K1=2.2E-9 K2=-5.0E-1 K3=0.0 #
 CH2OH1+ + NH3 -> NH41+ + H2CO 						RTYPE=2 K1=1.7E-9 K2=-5.0E-1 K3=0.0 #
 CH31+ + CH3OH -> CH3O1+ + CH4 						RTYPE=2 K1=2.1E-9 K2=-5.0E-1 K3=0.0 #
 CH31+ + CH4 -> CH3CH21+ + H2 						RTYPE=2 K1=9.6E-10 K2=0.0 K3=0.0 #
 CH31+ + H2CO -> HCO1+ + CH4 						RTYPE=2 K1=2.9E-9 K2=-5.0E-1 K3=0.0 #
 CH31+ + HCO -> CH41+ + CO 							RTYPE=2 K1=6.1E-10 K2=-5.0E-1 K3=0.0 #
 CH31+ + HCO -> HCO1+ + CH3 						RTYPE=2 K1=6.1E-10 K2=-5.0E-1 K3=0.0 #
 CH31+ + N -> H2NC1+ + H 							RTYPE=2 K1=6.7E-11 K2=0.0 K3=0.0 #
 CH31+ + N -> HCN1+ + H2 							RTYPE=2 K1=6.7E-11 K2=0.0 K3=0.0 #
 CH31+ + NH -> H2CN1+ + H2 							RTYPE=2 K1=4.4E-9 K2=-5.0E-1 K3=0.0 #
 CH31+ + NH2 -> CH3NH1+ + H 						RTYPE=2 K1=2.6E-9 K2=-5.0E-1 K3=0.0 #
 CH31+ + NH2CHO -> CH3NH21+ + HCO 					RTYPE=2 K1=1.0E-8 K2=-5.0E-1 K3=0.0 #
 CH31+ + NH3 -> CH3NH1+ + H2 						RTYPE=2 K1=1.72E-9 K2=-5.0E-1 K3=0.0 #
 CH31+ + NH3 -> NH41+ + CH2 						RTYPE=2 K1=3.0E-10 K2=-5.0E-1 K3=0.0 #
 CH31+ + NO -> NO1+ + CH3 							RTYPE=2 K1=4.44E-10 K2=-5.0E-1 K3=0.0 #
 CH31+ + O -> H31+ + CO 							RTYPE=2 K1=8.8E-11 K2=0.0 K3=0.0 #
 CH31+ + O -> HCO1+ + H2 							RTYPE=2 K1=2.05E-10 K2=0.0 K3=0.0 #
 CH31+ + O -> HOC1+ + H2 							RTYPE=2 K1=2.05E-10 K2=0.0 K3=0.0 #
 CH31+ + O -> H2CO1+ + H 							RTYPE=2 K1=1.0E-15 K2=0.0 K3=0.0 #
 CH31+ + O2 -> CH3O1+ + O 							RTYPE=2 K1=5.0E-12 K2=0.0 K3=0.0 #
 CH31+ + OH -> H2CO1+ + H2 							RTYPE=2 K1=5.4E-9 K2=-5.0E-1 K3=0.0 #
 CH31+ + P -> PCH21+ + H 							RTYPE=2 K1=1.0E-9 K2=0.0 K3=0.0 #
 CH3O1+ + CH -> CH21+ + H2CO 						RTYPE=2 K1=4.4E-9 K2=-5.0E-1 K3=0.0 #
 CH3O1+ + HNC -> H2CN1+ + H2CO 						RTYPE=2 K1=6.52E-9 K2=-5.0E-1 K3=0.0 #
 CH3O1+ + NH2 -> NH31+ + H2CO 						RTYPE=2 K1=2.2E-9 K2=-5.0E-1 K3=0.0 #
 CH3O1+ + NH3 -> NH41+ + H2CO 						RTYPE=2 K1=1.7E-9 K2=-5.0E-1 K3=0.0 #
 CH3OH21+ + CH3OH -> CH3OCH41+ + H2O 				RTYPE=2 K1=1.0E-10 K2=-1.0 K3=0.0 #
 CH41+ + CH3OH -> CH3OH1+ + CH4 					RTYPE=2 K1=1.8E-9 K2=0.0 K3=0.0 #
 CH41+ + CH3OH -> CH3OH21+ + CH3 					RTYPE=2 K1=1.2E-9 K2=0.0 K3=0.0 #
 CH41+ + CH4 -> CH51+ + CH3 						RTYPE=2 K1=1.5E-9 K2=0.0 K3=0.0 #
 CH41+ + CO -> HCO1+ + CH3 							RTYPE=2 K1=1.4E-9 K2=0.0 K3=0.0 #
 CH41+ + CO2 -> HOCO1+ + CH3 						RTYPE=2 K1=1.2E-9 K2=0.0 K3=0.0 #
 CH41+ + H -> CH31+ + H2 							RTYPE=2 K1=1.0E-11 K2=0.0 K3=0.0 #
 CH41+ + H2 -> CH51+ + H 							RTYPE=2 K1=3.5E-11 K2=0.0 K3=0.0 #
 CH41+ + H2CO -> CH2OH1+ + CH3 						RTYPE=2 K1=1.98E-9 K2=0.0 K3=0.0 #
 CH41+ + H2CO -> H2CO1+ + CH4 						RTYPE=2 K1=1.62E-9 K2=0.0 K3=0.0 #
 CH41+ + H2O -> H3O1+ + CH3 						RTYPE=2 K1=2.5E-9 K2=0.0 K3=0.0 #
 CH41+ + NH3 -> CH51+ + NH2 						RTYPE=2 K1=3.0E-11 K2=0.0 K3=0.0 #
 CH41+ + NH3 -> NH31+ + CH4 						RTYPE=2 K1=6.9E-10 K2=0.0 K3=0.0 #
 CH41+ + NH3 -> NH41+ + CH3 						RTYPE=2 K1=6.6E-10 K2=0.0 K3=0.0 #
 CH41+ + O -> CH31+ + OH 							RTYPE=2 K1=1.0E-9 K2=0.0 K3=0.0 #
 CH41+ + O2 -> O21+ + CH4 							RTYPE=2 K1=4.0E-10 K2=0.0 K3=0.0 #
 CH51+ + C -> CH1+ + CH4 							RTYPE=2 K1=1.0E-9 K2=0.0 K3=0.0 #
 CH51+ + CH -> CH21+ + CH4 							RTYPE=2 K1=4.9E-9 K2=-5.0E-1 K3=0.0 #
 CH51+ + CH2 -> CH31+ + CH4 						RTYPE=2 K1=7.97E-10 K2=-5.0E-1 K3=0.0 #
 CH51+ + CO -> HCO1+ + CH4 							RTYPE=2 K1=3.16E-10 K2=-5.0E-1 K3=0.0 #
 CH51+ + H -> CH41+ + H2 							RTYPE=2 K1=1.5E-10 K2=0.0 K3=4.81E+2 #
 CH51+ + H2CO -> CH2OH1+ + CH4 						RTYPE=2 K1=2.7E-9 K2=-5.0E-1 K3=0.0 #
 CH51+ + H2O -> H3O1+ + CH4 						RTYPE=2 K1=2.4E-9 K2=-5.0E-1 K3=0.0 #
 CH51+ + HCN -> H2CN1+ + CH4 						RTYPE=2 K1=8.5E-9 K2=-5.0E-1 K3=0.0 #
 CH51+ + HCO -> H2CO1+ + CH4 						RTYPE=2 K1=1.2E-9 K2=-5.0E-1 K3=0.0 #
 CH51+ + HNC -> H2CN1+ + CH4 						RTYPE=2 K1=7.67E-9 K2=-5.0E-1 K3=0.0 #
 CH51+ + NH -> NH21+ + CH4 							RTYPE=2 K1=4.2E-9 K2=-5.0E-1 K3=0.0 #
 CH51+ + NH2 -> NH31+ + CH4 						RTYPE=2 K1=2.5E-9 K2=-5.0E-1 K3=0.0 #
 CH51+ + NH3 -> NH41+ + CH4 						RTYPE=2 K1=2.0E-9 K2=-5.0E-1 K3=0.0 #
 CH51+ + O -> H3O1+ + CH2 							RTYPE=2 K1=2.2E-10 K2=0.0 K3=0.0 #
 CH51+ + O -> CH3O1+ + H2 							RTYPE=2 K1=4.4E-12 K2=0.0 K3=0.0 #
 CH51+ + OH -> H2O1+ + CH4 							RTYPE=2 K1=5.2E-9 K2=-5.0E-1 K3=0.0 #
 CN1+ + C -> C1+ + CN 								RTYPE=2 K1=1.1E-10 K2=0.0 K3=0.0 #
 CN1+ + CH -> CH1+ + CN 							RTYPE=2 K1=6.4E-10 K2=0.0 K3=0.0 #
 CN1+ + CH2 -> CH21+ + CN 							RTYPE=2 K1=8.8E-10 K2=0.0 K3=0.0 #
 CN1+ + CO -> CO1+ + CN 							RTYPE=2 K1=6.3E-10 K2=0.0 K3=0.0 #
 CN1+ + CO2 -> OCN1+ + CO 							RTYPE=2 K1=2.25E-10 K2=0.0 K3=0.0 #
 CN1+ + CO2 -> CO21+ + CN 							RTYPE=2 K1=4.5E-10 K2=0.0 K3=0.0 #
 CN1+ + H -> H1+ + CN 								RTYPE=2 K1=6.4E-10 K2=0.0 K3=0.0 #
 CN1+ + H2 -> HCN1+ + H 							RTYPE=2 K1=7.5E-10 K2=0.0 K3=0.0 #
 CN1+ + H2 -> HNC1+ + H 							RTYPE=2 K1=7.5E-10 K2=0.0 K3=0.0 #
 CN1+ + H2CO -> HCO1+ + HCN 						RTYPE=2 K1=5.2E-10 K2=0.0 K3=0.0 #
 CN1+ + H2CO -> H2CO1+ + CN 						RTYPE=2 K1=5.2E-10 K2=0.0 K3=0.0 #
 CN1+ + H2O -> HCO1+ + NH 							RTYPE=2 K1=1.6E-10 K2=0.0 K3=0.0 #
 CN1+ + H2O -> H2NC1+ + O 							RTYPE=2 K1=4.8E-10 K2=0.0 K3=0.0 #
 CN1+ + H2O -> HNCO1+ + H 							RTYPE=2 K1=6.4E-10 K2=0.0 K3=0.0 #
 CN1+ + H2O -> HCN1+ + OH 							RTYPE=2 K1=1.6E-9 K2=0.0 K3=0.0 #
 CN1+ + HCN -> NCCN1+ + H 							RTYPE=2 K1=3.15E-10 K2=0.0 K3=0.0 #
 CN1+ + HCN -> HCN1+ + CN 							RTYPE=2 K1=2.4E-9 K2=0.0 K3=0.0 #
 CN1+ + HCO -> HCO1+ + CN 							RTYPE=2 K1=3.7E-10 K2=0.0 K3=0.0 #
 CN1+ + HCO -> HCN1+ + CO 							RTYPE=2 K1=3.7E-10 K2=0.0 K3=0.0 #
 CN1+ + NH -> NH1+ + CN 							RTYPE=2 K1=6.5E-10 K2=0.0 K3=0.0 #
 CN1+ + NH2 -> NH21+ + CN 							RTYPE=2 K1=9.1E-10 K2=0.0 K3=0.0 #
 CN1+ + NO -> OCN1+ + N 							RTYPE=2 K1=1.9E-10 K2=0.0 K3=0.0 #
 CN1+ + NO -> NO1+ + CN 							RTYPE=2 K1=8.1E-10 K2=0.0 K3=0.0 #
 CN1+ + O -> O1+ + CN 								RTYPE=2 K1=6.5E-11 K2=0.0 K3=0.0 #
 CN1+ + O2 -> OCN1+ + O 							RTYPE=2 K1=8.6E-11 K2=0.0 K3=0.0 #
 CN1+ + O2 -> NO1+ + CO 							RTYPE=2 K1=8.6E-11 K2=0.0 K3=0.0 #
 CN1+ + O2 -> O21+ + CN 							RTYPE=2 K1=7.8E-10 K2=0.0 K3=0.0 #
 CN1+ + OH -> OH1+ + CN 							RTYPE=2 K1=6.4E-10 K2=0.0 K3=0.0 #
 CNC1+ + H2O -> HCO1+ + HCN 						RTYPE=2 K1=6.4E-11 K2=0.0 K3=0.0 #
 CNC1+ + NH3 -> H2CN1+ + HCN 						RTYPE=2 K1=1.9E-9 K2=0.0 K3=0.0 #
 CO1+ + C -> C1+ + CO 								RTYPE=2 K1=1.1E-10 K2=0.0 K3=0.0 #
 CO1+ + CH -> CH1+ + CO 							RTYPE=2 K1=3.2E-10 K2=0.0 K3=0.0 #
 CO1+ + CH -> HCO1+ + C 							RTYPE=2 K1=3.2E-10 K2=0.0 K3=0.0 #
 CO1+ + CH2 -> CH21+ + CO 							RTYPE=2 K1=4.3E-10 K2=0.0 K3=0.0 #
 CO1+ + CH2 -> HCO1+ + CH 							RTYPE=2 K1=4.3E-10 K2=0.0 K3=0.0 #
 CO1+ + CH4 -> CH41+ + CO 							RTYPE=2 K1=7.93E-10 K2=0.0 K3=0.0 #
 CO1+ + CH4 -> CH3CO1+ + H 							RTYPE=2 K1=5.2E-11 K2=0.0 K3=0.0 #
 CO1+ + CH4 -> HCO1+ + CH3 							RTYPE=2 K1=4.55E-10 K2=0.0 K3=0.0 #
 CO1+ + CO2 -> CO21+ + CO 							RTYPE=2 K1=1.1E-9 K2=0.0 K3=0.0 #
 CO1+ + H -> H1+ + CO 								RTYPE=2 K1=4.0E-10 K2=0.0 K3=0.0 #
 CO1+ + H2 -> HOC1+ + H 							RTYPE=2 K1=7.5E-10 K2=0.0 K3=0.0 #
 CO1+ + H2 -> HCO1+ + H 							RTYPE=2 K1=7.5E-10 K2=0.0 K3=0.0 #
 CO1+ + H2CO -> H2CO1+ + CO 						RTYPE=2 K1=1.4E-9 K2=0.0 K3=0.0 #
 CO1+ + H2CO -> HCO1+ + HCO 						RTYPE=2 K1=1.7E-9 K2=0.0 K3=0.0 #
 CO1+ + H2O -> HCO1+ + OH 							RTYPE=2 K1=8.8E-10 K2=0.0 K3=0.0 #
 CO1+ + H2O -> H2O1+ + CO 							RTYPE=2 K1=1.7E-9 K2=0.0 K3=0.0 #
 CO1+ + HCN -> HCN1+ + CO 							RTYPE=2 K1=3.4E-10 K2=0.0 K3=0.0 #
 CO1+ + HCO -> HCO1+ + CO 							RTYPE=2 K1=7.4E-10 K2=0.0 K3=0.0 #
 CO1+ + N -> NO1+ + C 								RTYPE=2 K1=8.1E-11 K2=0.0 K3=0.0 #
 CO1+ + NH -> NH1+ + CO 							RTYPE=2 K1=3.2E-10 K2=0.0 K3=0.0 #
 CO1+ + NH -> HCO1+ + N 							RTYPE=2 K1=3.2E-10 K2=0.0 K3=0.0 #
 CO1+ + NH2 -> HCO1+ + NH 							RTYPE=2 K1=4.5E-10 K2=0.0 K3=0.0 #
 CO1+ + NH2 -> NH21+ + CO 							RTYPE=2 K1=4.5E-10 K2=0.0 K3=0.0 #
 CO1+ + NH3 -> HCO1+ + NH2 							RTYPE=2 K1=4.12E-11 K2=0.0 K3=0.0 #
 CO1+ + NH3 -> NH31+ + CO 							RTYPE=2 K1=2.02E-9 K2=0.0 K3=0.0 #
 CO1+ + NO -> NO1+ + CO 							RTYPE=2 K1=3.3E-10 K2=0.0 K3=0.0 #
 CO1+ + O -> O1+ + CO 								RTYPE=2 K1=1.4E-10 K2=0.0 K3=0.0 #
 CO1+ + O2 -> O21+ + CO 							RTYPE=2 K1=1.2E-10 K2=0.0 K3=0.0 #
 CO1+ + OH -> OH1+ + CO 							RTYPE=2 K1=3.1E-10 K2=0.0 K3=0.0 #
 CO1+ + OH -> HCO1+ + O 							RTYPE=2 K1=3.1E-10 K2=0.0 K3=0.0 #
 CO21+ + CH4 -> HOCO1+ + CH3 						RTYPE=2 K1=5.5E-10 K2=0.0 K3=0.0 #
 CO21+ + CH4 -> CH41+ + CO2 						RTYPE=2 K1=5.5E-10 K2=0.0 K3=0.0 #
 CO21+ + H -> H1+ + CO2 							RTYPE=2 K1=1.0E-10 K2=0.0 K3=0.0 #
 CO21+ + H -> HCO1+ + O 							RTYPE=2 K1=2.9E-10 K2=0.0 K3=0.0 #
 CO21+ + H2 -> HOCO1+ + H 							RTYPE=2 K1=8.7E-10 K2=0.0 K3=0.0 #
 CO21+ + H2O -> H2O1+ + CO2 						RTYPE=2 K1=2.04E-9 K2=0.0 K3=0.0 #
 CO21+ + H2O -> HOCO1+ + OH 						RTYPE=2 K1=7.56E-10 K2=0.0 K3=0.0 #
 CO21+ + NH3 -> NH31+ + CO2 						RTYPE=2 K1=1.98E-9 K2=0.0 K3=0.0 #
 CO21+ + NO -> NO1+ + CO2 							RTYPE=2 K1=1.2E-10 K2=0.0 K3=0.0 #
 CO21+ + O -> O1+ + CO2 							RTYPE=2 K1=9.62E-11 K2=0.0 K3=0.0 #
 CO21+ + O -> O21+ + CO 							RTYPE=2 K1=1.64E-10 K2=0.0 K3=0.0 #
 CO21+ + O2 -> O21+ + CO2 							RTYPE=2 K1=5.0E-11 K2=0.0 K3=0.0 #
 H1+ + CH3CH2 -> CH3CH21+ + H 						RTYPE=2 K1=1.97E-9 K2=-5.0E-1 K3=0.0 #
 H1+ + CH3CH2OH -> CH3CH2O1+ + H2 					RTYPE=2 K1=3.3E-9 K2=-5.0E-1 K3=0.0 #
 H1+ + CH3CH3 -> CH3CH21+ + H2 						RTYPE=2 K1=1.3E-9 K2=0.0 K3=0.0 #
 H1+ + CH -> CH1+ + H 								RTYPE=2 K1=1.4E-8 K2=-5.0E-1 K3=0.0 #
 H1+ + CH2 -> CH21+ + H 							RTYPE=2 K1=1.14E-9 K2=-5.0E-1 K3=0.0 #
 H1+ + CH2 -> CH1+ + H2 							RTYPE=2 K1=1.14E-9 K2=-5.0E-1 K3=0.0 #
 H1+ + CH2CO -> CH2CO1+ + H 						RTYPE=2 K1=5.6E-9 K2=-5.0E-1 K3=0.0 #
 H1+ + CH2NH -> H2CN1+ + H2 						RTYPE=2 K1=7.9E-9 K2=-5.0E-1 K3=0.0 #
 H1+ + CH3 -> CH31+ + H 							RTYPE=2 K1=3.4E-9 K2=0.0 K3=0.0 #
 H1+ + CH3CHO -> CH3CHO1+ + H 						RTYPE=2 K1=5.0E-9 K2=-5.0E-1 K3=0.0 #
 H1+ + CH3CHO -> CH3CO1+ + H2 						RTYPE=2 K1=5.0E-9 K2=-5.0E-1 K3=0.0 #
 H1+ + CH3CN -> CH3CN1+ + H 						RTYPE=2 K1=7.7E-9 K2=-5.0E-1 K3=0.0 #
 H1+ + CH3NH2 -> CH3NH21+ + H 						RTYPE=2 K1=2.6E-9 K2=-5.0E-1 K3=0.0 #
 H1+ + CH3NH2 -> CH3NH1+ + H2 						RTYPE=2 K1=2.6E-9 K2=-5.0E-1 K3=0.0 #
 H1+ + CH3OCH3 -> CH3OCH31+ + H 					RTYPE=2 K1=2.5E-9 K2=-5.0E-1 K3=0.0 #
 H1+ + CH3OCH3 -> CH3CH2O1+ + H2 					RTYPE=2 K1=2.5E-9 K2=-5.0E-1 K3=0.0 #
 H1+ + CH3OH -> CH3OH1+ + H 						RTYPE=2 K1=3.3E-9 K2=-5.0E-1 K3=0.0 #
 H1+ + CH3OH -> CH3O1+ + H2 						RTYPE=2 K1=3.3E-9 K2=-5.0E-1 K3=0.0 #
 H1+ + CH4 -> CH31+ + H2 							RTYPE=2 K1=2.3E-9 K2=0.0 K3=0.0 #
 H1+ + CH4 -> CH41+ + H 							RTYPE=2 K1=1.5E-9 K2=0.0 K3=0.0 #
 H1+ + CO2 -> HCO1+ + O 							RTYPE=2 K1=3.0E-9 K2=0.0 K3=0.0 #
 H1+ + H2CO -> H2CO1+ + H 							RTYPE=2 K1=4.6E-9 K2=-5.0E-1 K3=0.0 #
 H1+ + H2CO -> HCO1+ + H2 							RTYPE=2 K1=4.6E-9 K2=-5.0E-1 K3=0.0 #
 H1+ + H2O -> H2O1+ + H 							RTYPE=2 K1=7.3E-9 K2=-5.0E-1 K3=0.0 #
 H1+ + HCN -> HCN1+ + H 							RTYPE=2 K1=2.78E-8 K2=-5.0E-1 K3=0.0 #
 H1+ + HCO -> HCO1+ + H 							RTYPE=2 K1=1.3E-9 K2=-5.0E-1 K3=0.0 #
 H1+ + HCO -> CO1+ + H2 							RTYPE=2 K1=1.3E-9 K2=-5.0E-1 K3=0.0 #
 H1+ + HCO -> H21+ + CO 							RTYPE=2 K1=1.3E-9 K2=-5.0E-1 K3=0.0 #
 H1+ + HCOOCH3 -> HCOOCH31+ + H 					RTYPE=2 K1=6.9E-9 K2=-5.0E-1 K3=0.0 #
 H1+ + HCOOH -> HCOOH1+ + H 						RTYPE=2 K1=2.8E-9 K2=-5.0E-1 K3=0.0 #
 H1+ + HCOOH -> HOCO1+ + H2 						RTYPE=2 K1=2.8E-9 K2=-5.0E-1 K3=0.0 #
 H1+ + HNC -> H1+ + HCN 							RTYPE=2 K1=2.51E-8 K2=-5.0E-1 K3=0.0 #
 H1+ + HNCO -> NH21+ + CO 							RTYPE=2 K1=1.48E-8 K2=-5.0E-1 K3=0.0 #
 H1+ + HNO -> NO1+ + H2 							RTYPE=2 K1=6.7E-9 K2=-5.0E-1 K3=0.0 #
 H1+ + HPO -> HPO1+ + H 							RTYPE=2 K1=2.15E-8 K2=-5.0E-1 K3=0.0 #
 H1+ + N2H2 -> N2H1+ + H2 							RTYPE=2 K1=2.0E-9 K2=0.0 K3=0.0 #
 H1+ + NH -> NH1+ + H 								RTYPE=2 K1=1.2E-8 K2=-5.0E-1 K3=0.0 #
 H1+ + NH2 -> NH21+ + H 							RTYPE=2 K1=7.3E-9 K2=-5.0E-1 K3=0.0 #
 H1+ + NH2CHO -> H2NC1+ + H2O 						RTYPE=2 K1=8.62E-9 K2=-5.0E-1 K3=0.0 #
 H1+ + NH2CHO -> NH21+ + H2CO 						RTYPE=2 K1=8.62E-9 K2=-5.0E-1 K3=0.0 #
 H1+ + NH2CHO -> NH41+ + CO 						RTYPE=2 K1=8.62E-9 K2=-5.0E-1 K3=0.0 #
 H1+ + NH2CHO -> HCO1+ + NH3 						RTYPE=2 K1=8.62E-9 K2=-5.0E-1 K3=0.0 #
 H1+ + NH2CN -> HNC1+ + NH2 						RTYPE=2 K1=8.35E-9 K2=-5.0E-1 K3=0.0 #
 H1+ + NH2CN -> NH21+ + HNC 						RTYPE=2 K1=8.35E-9 K2=-5.0E-1 K3=0.0 #
 H1+ + NH3 -> NH31+ + H 							RTYPE=2 K1=5.8E-9 K2=-5.0E-1 K3=0.0 #
 H1+ + NO -> NO1+ + H 								RTYPE=2 K1=1.4E-9 K2=-5.0E-1 K3=0.0 #
 H1+ + O -> O1+ + H 								RTYPE=2 K1=7.0E-10 K2=0.0 K3=2.32E+2 #
 H1+ + O2 -> O21+ + H 								RTYPE=2 K1=1.2E-9 K2=0.0 K3=0.0 #
 H1+ + OH -> OH1+ + H 								RTYPE=2 K1=1.6E-8 K2=-5.0E-1 K3=0.0 #
 H1+ + P -> P1+ + H 								RTYPE=2 K1=1.0E-9 K2=0.0 K3=0.0 #
 H1+ + PN -> PN1+ + H 								RTYPE=2 K1=2.54E-8 K2=-5.0E-1 K3=0.0 #
 H1+ + PO -> PO1+ + H 								RTYPE=2 K1=1.74E-8 K2=-5.0E-1 K3=0.0 #
 H21+ + C -> CH1+ + H 								RTYPE=2 K1=2.4E-9 K2=0.0 K3=0.0 #
 H21+ + CH -> CH21+ + H 							RTYPE=2 K1=7.1E-10 K2=0.0 K3=0.0 #
 H21+ + CH -> CH1+ + H2 							RTYPE=2 K1=7.1E-10 K2=0.0 K3=0.0 #
 H21+ + CH2 -> CH21+ + H2 							RTYPE=2 K1=1.0E-9 K2=0.0 K3=0.0 #
 H21+ + CH2 -> CH31+ + H 							RTYPE=2 K1=1.0E-9 K2=0.0 K3=0.0 #
 H21+ + CH4 -> CH41+ + H2 							RTYPE=2 K1=1.4E-9 K2=0.0 K3=0.0 #
 H21+ + CH4 -> CH51+ + H 							RTYPE=2 K1=1.1E-10 K2=0.0 K3=0.0 #
 H21+ + CH4 -> CH31+ + H + H2 						RTYPE=2 K1=2.3E-9 K2=0.0 K3=0.0 #
 H21+ + CN -> CN1+ + H2 							RTYPE=2 K1=1.2E-9 K2=0.0 K3=0.0 #
 H21+ + CN -> HCN1+ + H 							RTYPE=2 K1=1.2E-9 K2=0.0 K3=0.0 #
 H21+ + CO -> CO1+ + H2 							RTYPE=2 K1=6.0E-10 K2=0.0 K3=0.0 #
 H21+ + CO -> HCO1+ + H 							RTYPE=2 K1=2.2E-9 K2=0.0 K3=0.0 #
 H21+ + CO2 -> CO21+ + H2 							RTYPE=2 K1=1.4E-9 K2=0.0 K3=0.0 #
 H21+ + CO2 -> HOCO1+ + H 							RTYPE=2 K1=2.35E-9 K2=0.0 K3=0.0 #
 H21+ + CO2 -> CO1+ + H2O 							RTYPE=2 K1=1.4E-9 K2=0.0 K3=0.0 #
 H21+ + H -> H1+ + H2 								RTYPE=2 K1=6.4E-10 K2=0.0 K3=0.0 #
 H21+ + H2 -> H31+ + H 								RTYPE=2 K1=2.1E-9 K2=0.0 K3=0.0 #
 H21+ + H2CO -> HCO1+ + H + H2 						RTYPE=2 K1=1.4E-9 K2=0.0 K3=0.0 #
 H21+ + H2CO -> H2CO1+ + H2 						RTYPE=2 K1=1.4E-9 K2=0.0 K3=0.0 #
 H21+ + H2O -> H2O1+ + H2 							RTYPE=2 K1=3.9E-9 K2=0.0 K3=0.0 #
 H21+ + H2O -> H3O1+ + H 							RTYPE=2 K1=3.4E-9 K2=0.0 K3=0.0 #
 H21+ + HCN -> HCN1+ + H2 							RTYPE=2 K1=2.7E-9 K2=0.0 K3=0.0 #
 H21+ + HCO -> H31+ + CO 							RTYPE=2 K1=1.0E-9 K2=0.0 K3=0.0 #
 H21+ + HCO -> HCO1+ + H2 							RTYPE=2 K1=1.0E-9 K2=0.0 K3=0.0 #
 H21+ + N -> NH1+ + H 								RTYPE=2 K1=1.9E-9 K2=0.0 K3=0.0 #
 H21+ + N2 -> N2H1+ + H 							RTYPE=2 K1=2.0E-9 K2=0.0 K3=0.0 #
 H21+ + NH -> NH21+ + H 							RTYPE=2 K1=7.6E-10 K2=0.0 K3=0.0 #
 H21+ + NH -> NH1+ + H2 							RTYPE=2 K1=7.6E-10 K2=0.0 K3=0.0 #
 H21+ + NH2 -> NH21+ + H2 							RTYPE=2 K1=2.1E-9 K2=0.0 K3=0.0 #
 H21+ + NH3 -> NH31+ + H2 							RTYPE=2 K1=5.7E-9 K2=0.0 K3=0.0 #
 H21+ + NO -> NO1+ + H2 							RTYPE=2 K1=1.1E-9 K2=0.0 K3=0.0 #
 H21+ + NO -> HNO1+ + H 							RTYPE=2 K1=1.1E-9 K2=0.0 K3=0.0 #
 H21+ + O -> OH1+ + H 								RTYPE=2 K1=1.5E-9 K2=0.0 K3=0.0 #
 H21+ + O2 -> O21+ + H2 							RTYPE=2 K1=8.0E-10 K2=0.0 K3=0.0 #
 H21+ + O2 -> O2H1+ + H 							RTYPE=2 K1=1.9E-9 K2=0.0 K3=0.0 #
 H21+ + OH -> OH1+ + H2 							RTYPE=2 K1=7.6E-10 K2=0.0 K3=0.0 #
 H21+ + OH -> H2O1+ + H 							RTYPE=2 K1=7.6E-10 K2=0.0 K3=0.0 #
 H2CN1+ + CH -> CH21+ + HCN 						RTYPE=2 K1=4.5E-9 K2=-5.0E-1 K3=0.0 #
 H2CN1+ + CH2 -> CH31+ + HNC 						RTYPE=2 K1=3.61E-10 K2=-5.0E-1 K3=0.0 #
 H2CN1+ + CH2 -> CH31+ + HCN 						RTYPE=2 K1=3.61E-10 K2=-5.0E-1 K3=0.0 #
 H2CN1+ + H2CO -> CH2OH1+ + HCN 					RTYPE=2 K1=2.37E-9 K2=-5.0E-1 K3=0.0 #
 H2CN1+ + NH2 -> NH31+ + HNC 						RTYPE=2 K1=1.11E-9 K2=-5.0E-1 K3=0.0 #
 H2CN1+ + NH2 -> NH31+ + HCN 						RTYPE=2 K1=1.11E-9 K2=-5.0E-1 K3=0.0 #
 H2CN1+ + NH3 -> NH41+ + HCN 						RTYPE=2 K1=8.75E-10 K2=-5.0E-1 K3=0.0 #
 H2CN1+ + NH3 -> NH41+ + HNC 						RTYPE=2 K1=8.75E-10 K2=-5.0E-1 K3=0.0 #
 H2CO1+ + CH -> CH21+ + HCO 						RTYPE=2 K1=3.1E-10 K2=0.0 K3=0.0 #
 H2CO1+ + CH -> CH1+ + H2CO 						RTYPE=2 K1=3.1E-10 K2=0.0 K3=0.0 #
 H2CO1+ + CH2 -> CH21+ + H2CO 						RTYPE=2 K1=4.3E-10 K2=0.0 K3=0.0 #
 H2CO1+ + CH2 -> CH31+ + HCO 						RTYPE=2 K1=4.3E-10 K2=0.0 K3=0.0 #
 H2CO1+ + CH4 -> CH2OH1+ + CH3 						RTYPE=2 K1=1.1E-10 K2=0.0 K3=0.0 #
 H2CO1+ + H2CO -> CH2OH1+ + HCO 					RTYPE=2 K1=1.0E-9 K2=0.0 K3=0.0 #
 H2CO1+ + H2O -> H3O1+ + HCO 						RTYPE=2 K1=2.6E-9 K2=0.0 K3=0.0 #
 H2CO1+ + HCN -> H2CN1+ + HCO 						RTYPE=2 K1=1.4E-9 K2=0.0 K3=0.0 #
 H2CO1+ + HCO -> HCO1+ + H2CO 						RTYPE=2 K1=3.2E-9 K2=0.0 K3=0.0 #
 H2CO1+ + HCO -> CH2OH1+ + CO 						RTYPE=2 K1=3.6E-10 K2=0.0 K3=0.0 #
 H2CO1+ + HNC -> H2CN1+ + HCO 						RTYPE=2 K1=1.4E-9 K2=0.0 K3=0.0 #
 H2CO1+ + NH -> CH2OH1+ + N 						RTYPE=2 K1=6.4E-10 K2=0.0 K3=0.0 #
 H2CO1+ + NH2 -> NH31+ + HCO 						RTYPE=2 K1=8.8E-10 K2=0.0 K3=0.0 #
 H2CO1+ + NH3 -> NH41+ + HCO 						RTYPE=2 K1=1.3E-9 K2=0.0 K3=0.0 #
 H2CO1+ + NH3 -> NH31+ + H2CO 						RTYPE=2 K1=4.3E-10 K2=0.0 K3=0.0 #
 H2CO1+ + NO -> NO1+ + H2CO 						RTYPE=2 K1=7.8E-10 K2=0.0 K3=0.0 #
 H2CO1+ + O2 -> HCO1+ + HO2 						RTYPE=2 K1=7.7E-11 K2=0.0 K3=0.0 #
 H2O1+ + C -> CH1+ + OH 							RTYPE=2 K1=1.1E-9 K2=0.0 K3=0.0 #
 H2O1+ + CH -> CH1+ + H2O 							RTYPE=2 K1=3.4E-10 K2=0.0 K3=0.0 #
 H2O1+ + CH -> CH21+ + OH 							RTYPE=2 K1=3.4E-10 K2=0.0 K3=0.0 #
 H2O1+ + CH2 -> CH31+ + OH 							RTYPE=2 K1=4.7E-10 K2=0.0 K3=0.0 #
 H2O1+ + CH2 -> CH21+ + H2O 						RTYPE=2 K1=4.7E-10 K2=0.0 K3=0.0 #
 H2O1+ + CH4 -> H3O1+ + CH3 						RTYPE=2 K1=1.3E-9 K2=0.0 K3=0.0 #
 H2O1+ + CO -> HCO1+ + OH 							RTYPE=2 K1=5.0E-10 K2=0.0 K3=0.0 #
 H2O1+ + H2 -> H3O1+ + H 							RTYPE=2 K1=6.1E-10 K2=0.0 K3=0.0 #
 H2O1+ + H2CO -> CH2OH1+ + OH 						RTYPE=2 K1=6.6E-10 K2=0.0 K3=0.0 #
 H2O1+ + H2CO -> H2CO1+ + H2O 						RTYPE=2 K1=1.4E-9 K2=0.0 K3=0.0 #
 H2O1+ + H2O -> H3O1+ + OH 							RTYPE=2 K1=2.1E-9 K2=0.0 K3=0.0 #
 H2O1+ + HCN -> H2CN1+ + OH 						RTYPE=2 K1=2.1E-9 K2=0.0 K3=0.0 #
 H2O1+ + HCO -> H2CO1+ + OH 						RTYPE=2 K1=2.8E-10 K2=0.0 K3=0.0 #
 H2O1+ + HCO -> H3O1+ + CO 							RTYPE=2 K1=2.8E-10 K2=0.0 K3=0.0 #
 H2O1+ + HCO -> HCO1+ + H2O 						RTYPE=2 K1=2.8E-10 K2=0.0 K3=0.0 #
 H2O1+ + HNC -> H2CN1+ + OH 						RTYPE=2 K1=1.1E-9 K2=0.0 K3=0.0 #
 H2O1+ + N -> HNO1+ + H 							RTYPE=2 K1=1.9E-10 K2=0.0 K3=0.0 #
 H2O1+ + NH -> H3O1+ + N 							RTYPE=2 K1=7.1E-10 K2=0.0 K3=0.0 #
 H2O1+ + NH2 -> NH21+ + H2O 						RTYPE=2 K1=4.9E-10 K2=0.0 K3=0.0 #
 H2O1+ + NH2 -> NH31+ + OH 							RTYPE=2 K1=4.9E-10 K2=0.0 K3=0.0 #
 H2O1+ + NH3 -> NH31+ + H2O 						RTYPE=2 K1=2.2E-9 K2=0.0 K3=0.0 #
 H2O1+ + NH3 -> NH41+ + OH 							RTYPE=2 K1=9.0E-10 K2=0.0 K3=0.0 #
 H2O1+ + NO -> NO1+ + H2O 							RTYPE=2 K1=1.2E-9 K2=0.0 K3=0.0 #
 H2O1+ + O -> O21+ + H2 							RTYPE=2 K1=4.0E-11 K2=0.0 K3=0.0 #
 H2O1+ + O2 -> O21+ + H2O 							RTYPE=2 K1=4.3E-10 K2=0.0 K3=0.0 #
 H2O1+ + OH -> H3O1+ + O 							RTYPE=2 K1=6.9E-10 K2=0.0 K3=0.0 #
 H31+ + C -> CH1+ + H2 								RTYPE=2 K1=2.0E-9 K2=0.0 K3=0.0 #
 H31+ + CH3CH2 -> CH3CH31+ + H2 					RTYPE=2 K1=2.34E-9 K2=-5.0E-1 K3=0.0 #
 H31+ + CH3CH2OH -> CH3CH21+ + H2O + H2 			RTYPE=2 K1=1.3E-9 K2=-5.0E-1 K3=0.0 #
 H31+ + CH3CH3 -> CH3CH21+ + H2 + H2 				RTYPE=2 K1=3.37E-9 K2=0.0 K3=0.0 #
 H31+ + CH -> CH21+ + H2 							RTYPE=2 K1=8.5E-9 K2=-5.0E-1 K3=0.0 #
 H31+ + CH2 -> CH31+ + H2 							RTYPE=2 K1=1.4E-9 K2=-5.0E-1 K3=0.0 #
 H31+ + CH2CO -> CH3CO1+ + H2 						RTYPE=2 K1=3.3E-9 K2=-5.0E-1 K3=0.0 #
 H31+ + CH2NH -> CH3NH1+ + H2 						RTYPE=2 K1=4.7E-9 K2=-5.0E-1 K3=0.0 #
 H31+ + CH2OH -> CH3OH1+ + H2 						RTYPE=2 K1=3.0E-9 K2=-5.0E-1 K3=0.0 #
 H31+ + CH3 -> CH41+ + H2 							RTYPE=2 K1=2.1E-9 K2=0.0 K3=0.0 #
 H31+ + CH3CHO -> CH3CH2O1+ + H2 					RTYPE=2 K1=6.2E-9 K2=-5.0E-1 K3=0.0 #
 H31+ + CH3CN -> CH3CNH1+ + H2 						RTYPE=2 K1=9.1E-9 K2=-5.0E-1 K3=0.0 #
 H31+ + CH3CO -> CH3CHO1+ + H2 						RTYPE=2 K1=4.47E-9 K2=-5.0E-1 K3=0.0 #
 H31+ + CH3NH -> CH3NH21+ + H2 						RTYPE=2 K1=2.62E-9 K2=-5.0E-1 K3=0.0 #
 H31+ + CH3NH2 -> CH3NH31+ + H2 					RTYPE=2 K1=3.1E-9 K2=-5.0E-1 K3=0.0 #
 H31+ + CH3O -> CH3OH1+ + H2 						RTYPE=2 K1=4.58E-10 K2=-5.0E-1 K3=0.0 #
 H31+ + CH3OCH3 -> CH3OCH41+ + H2 					RTYPE=2 K1=3.0E-9 K2=-5.0E-1 K3=0.0 #
 H31+ + CH3OH -> CH3O1+ + H2 + H2 					RTYPE=2 K1=1.12E-9 K2=-5.0E-1 K3=0.0 #
 H31+ + CH3OH -> CH3OH21+ + H2 						RTYPE=2 K1=1.04E-9 K2=-5.0E-1 K3=0.0 #
 H31+ + CH3OH -> CH31+ + H2O + H2 					RTYPE=2 K1=1.8E-9 K2=-5.0E-1 K3=0.0 #
 H31+ + CH4 -> CH51+ + H2 							RTYPE=2 K1=2.4E-9 K2=0.0 K3=0.0 #
 H31+ + CN -> HCN1+ + H2 							RTYPE=2 K1=8.1E-9 K2=-5.0E-1 K3=0.0 #
 H31+ + CO -> HCO1+ + H2 							RTYPE=2 K1=1.61E-9 K2=0.0 K3=0.0 #
 H31+ + CO -> HOC1+ + H2 							RTYPE=2 K1=9.44E-11 K2=0.0 K3=0.0 #
 H31+ + CO2 -> HOCO1+ + H2 							RTYPE=2 K1=1.9E-9 K2=0.0 K3=0.0 #
 H31+ + COCHO -> CHOCHO1+ + H2 						RTYPE=2 K1=1.94E-9 K2=-5.0E-1 K3=0.0 #
 H31+ + COOH -> HCOOH1+ + H2 						RTYPE=2 K1=6.74E-9 K2=-5.0E-1 K3=0.0 #
 H31+ + H2CO -> CH2OH1+ + H2 						RTYPE=2 K1=5.5E-9 K2=-5.0E-1 K3=0.0 #
 H31+ + H2O -> H3O1+ + H2 							RTYPE=2 K1=4.5E-9 K2=-5.0E-1 K3=0.0 #
 H31+ + HCN -> H2CN1+ + H2 							RTYPE=2 K1=1.7E-8 K2=-5.0E-1 K3=0.0 #
 H31+ + HCO -> H2CO1+ + H2 							RTYPE=2 K1=2.3E-9 K2=-5.0E-1 K3=0.0 #
 H31+ + HCOOH -> H3O1+ + CO + H2 					RTYPE=2 K1=9.7E-10 K2=-5.0E-1 K3=0.0 #
 H31+ + HCOOH -> HCO1+ + H2O + H2 					RTYPE=2 K1=2.3E-9 K2=-5.0E-1 K3=0.0 #
 H31+ + HNC -> H2CN1+ + H2 							RTYPE=2 K1=1.5E-8 K2=-5.0E-1 K3=0.0 #
 H31+ + HNCHO -> NH2CHO1+ + H2 						RTYPE=2 K1=3.89E-9 K2=-5.0E-1 K3=0.0 #
 H31+ + HNCO -> HNCHO1+ + H2 						RTYPE=2 K1=3.92E-9 K2=-5.0E-1 K3=0.0 #
 H31+ + HNCONH -> NH2CONH1+ + H2 					RTYPE=2 K1=7.41E-9 K2=-5.0E-1 K3=0.0 #
 H31+ + HNCOOH -> NH2COOH1+ + H2 					RTYPE=2 K1=3.85E-9 K2=-5.0E-1 K3=0.0 #
 H31+ + HNO -> H2NO1+ + H2 							RTYPE=2 K1=4.0E-9 K2=-5.0E-1 K3=0.0 #
 H31+ + HNOH -> NH2OH1+ + H2 						RTYPE=2 K1=3.91E-9 K2=-5.0E-1 K3=0.0 #
 H31+ + CH3ONH -> NH2OCH31+ + H2 					RTYPE=2 K1=2.89E-9 K2=-5.0E-1 K3=0.0 #
 H31+ + HOCOOH -> HOCOOH21+ + H2 					RTYPE=2 K1=3.84E-9 K2=-5.0E-1 K3=0.0 #
 H31+ + HPO -> H2PO1+ + H2 							RTYPE=2 K1=1.27E-8 K2=-5.0E-1 K3=0.0 #
 H31+ + N -> NH21+ + H 								RTYPE=2 K1=1.0E-17 K2=0.0 K3=0.0 #
 H31+ + N2 -> N2H1+ + H2 							RTYPE=2 K1=1.7E-9 K2=0.0 K3=0.0 #
 H31+ + N2H2 -> N2H1+ + H2 + H2 					RTYPE=2 K1=2.0E-9 K2=0.0 K3=0.0 #
 H31+ + NH -> NH21+ + H2 							RTYPE=2 K1=7.5E-9 K2=-5.0E-1 K3=0.0 #
 H31+ + NH2 -> NH31+ + H2 							RTYPE=2 K1=4.5E-9 K2=-5.0E-1 K3=0.0 #
 H31+ + NH2CHO -> NH2CHOH1+ + H2 					RTYPE=2 K1=2.0E-8 K2=-5.0E-1 K3=0.0 #
 H31+ + NH2CN -> NH2CNH1+ + H2 						RTYPE=2 K1=9.86E-9 K2=-5.0E-1 K3=0.0 #
 H31+ + NH2OH -> NH3OH1+ + H2 						RTYPE=2 K1=1.16E-9 K2=-5.0E-1 K3=0.0 #
 H31+ + NH3 -> NH41+ + H2 							RTYPE=2 K1=3.6E-9 K2=-5.0E-1 K3=0.0 #
 H31+ + NO -> HNO1+ + H2 							RTYPE=2 K1=8.5E-10 K2=-5.0E-1 K3=0.0 #
 H31+ + NO2 -> NO1+ + H2 + OH 						RTYPE=2 K1=7.28E-10 K2=-5.0E-1 K3=0.0 #
 H31+ + O -> OH1+ + H2 								RTYPE=2 K1=8.0E-10 K2=0.0 K3=0.0 #
 H31+ + O2 -> O2H1+ + H2 							RTYPE=2 K1=6.4E-10 K2=0.0 K3=0.0 #
 H31+ + OH -> H2O1+ + H2 							RTYPE=2 K1=9.5E-9 K2=-5.0E-1 K3=0.0 #
 H31+ + CHOCHO -> CHOCHOH1+ + H2 					RTYPE=2 K1=1.37E-9 K2=0.0 K3=0.0 #
 H31+ + PN -> HPN1+ + H2 							RTYPE=2 K1=1.5E-8 K2=-5.0E-1 K3=0.0 #
 H31+ + PO -> HPO1+ + H2 							RTYPE=2 K1=1.02E-8 K2=-5.0E-1 K3=0.0 #
 H31+ + NH2CO -> NH2CHO1+ + H2 						RTYPE=2 K1=3.89E-9 K2=-5.0E-1 K3=0.0 #
 H31+ + NH2NH -> NH2NH21+ + H2 						RTYPE=2 K1=3.49E-9 K2=-5.0E-1 K3=0.0 #
 H31+ + NH2NH2 -> NH2NH2H1+ + H2 					RTYPE=2 K1=3.49E-9 K2=-5.0E-1 K3=0.0 #
 H31+ + CH3OCO -> HCOOCH31+ + H2 					RTYPE=2 K1=3.4E-9 K2=-5.0E-1 K3=0.0 #
 H31+ + CH3OOH -> CH3OOH21+ + H2 					RTYPE=2 K1=3.33E-9 K2=-5.0E-1 K3=0.0 #
 H31+ + CH3OOCH3 -> CH3OH + CH3O1+ + H2 			RTYPE=2 K1=2.86E-9 K2=-5.0E-1 K3=0.0 #
 H3O1+ + C -> HCO1+ + H2 							RTYPE=2 K1=1.0E-11 K2=0.0 K3=0.0 #
 H3O1+ + CH -> CH21+ + H2O 							RTYPE=2 K1=4.8E-9 K2=-5.0E-1 K3=0.0 #
 H3O1+ + CH2 -> CH31+ + H2O 						RTYPE=2 K1=7.78E-10 K2=-5.0E-1 K3=0.0 #
 H3O1+ + CH2CO -> CH3CO1+ + H2O 					RTYPE=2 K1=1.52E-9 K2=-5.0E-1 K3=0.0 #
 H3O1+ + CH2OH -> CH3OH1+ + H2O 					RTYPE=2 K1=1.34E-9 K2=-5.0E-1 K3=0.0 #
 H3O1+ + CH3CHO -> CH3CH2O1+ + H2O 					RTYPE=2 K1=2.86E-9 K2=-5.0E-1 K3=0.0 #
 H3O1+ + CH3CN -> CH3CNH1+ + H2O 					RTYPE=2 K1=4.22E-9 K2=-5.0E-1 K3=0.0 #
 H3O1+ + CH3NH -> CH3NH21+ + H2O 					RTYPE=2 K1=1.3E-9 K2=-5.0E-1 K3=0.0 #
 H3O1+ + CH3NH2 -> CH3NH31+ + H2O 					RTYPE=2 K1=1.3E-9 K2=-5.0E-1 K3=0.0 #
 H3O1+ + CH3O -> CH3OH1+ + H2O 						RTYPE=2 K1=2.05E-10 K2=-5.0E-1 K3=0.0 #
 H3O1+ + CH3OCH3 -> CH3OCH41+ + H2O 				RTYPE=2 K1=1.37E-9 K2=-5.0E-1 K3=0.0 #
 H3O1+ + CH3OH -> CH3OH21+ + H2O 					RTYPE=2 K1=1.91E-9 K2=-5.0E-1 K3=0.0 #
 H3O1+ + COCHO -> CHOCHO1+ + H2O 					RTYPE=2 K1=8.74E-10 K2=-5.0E-1 K3=0.0 #
 H3O1+ + H2CO -> CH2OH1+ + H2O 						RTYPE=2 K1=2.6E-9 K2=-5.0E-1 K3=0.0 #
 H3O1+ + HCN -> H2CN1+ + H2O 						RTYPE=2 K1=8.2E-9 K2=-5.0E-1 K3=0.0 #
 H3O1+ + HCOOH -> HCOOH21+ + H2O 					RTYPE=2 K1=1.19E-9 K2=-5.0E-1 K3=0.0 #
 H3O1+ + HNC -> H2CN1+ + H2O 						RTYPE=2 K1=7.42E-9 K2=-5.0E-1 K3=0.0 #
 H3O1+ + HNCHO -> NH2CHO1+ + H2O 					RTYPE=2 K1=1.74E-9 K2=-5.0E-1 K3=0.0 #
 H3O1+ + HNCO -> HNCHO1+ + H2O 						RTYPE=2 K1=1.82E-9 K2=-5.0E-1 K3=0.0 #
 H3O1+ + HNCONH -> NH2CONH1+ + H2O 					RTYPE=2 K1=3.25E-9 K2=-5.0E-1 K3=0.0 #
 H3O1+ + HNCOOH -> NH2COOH1+ + H2O 					RTYPE=2 K1=1.66E-9 K2=-5.0E-1 K3=0.0 #
 H3O1+ + HNOH -> NH2OH1+ + H2O 						RTYPE=2 K1=1.8E-9 K2=-5.0E-1 K3=0.0 #
 H3O1+ + CH3ONH -> NH2OCH31+ + H2O 					RTYPE=2 K1=1.25E-9 K2=-5.0E-1 K3=0.0 #
 H3O1+ + HOCOOH -> HOCOOH21+ + H2O 					RTYPE=2 K1=1.64E-9 K2=-5.0E-1 K3=0.0 #
 H3O1+ + NH2 -> NH31+ + H2O 						RTYPE=2 K1=2.4E-9 K2=-5.0E-1 K3=0.0 #
 H3O1+ + NH2CHO -> NH2CHOH1+ + H2O 					RTYPE=2 K1=9.37E-9 K2=-5.0E-1 K3=0.0 #
 H3O1+ + NH2OH -> NH3OH1+ + H2O 					RTYPE=2 K1=5.18E-9 K2=-5.0E-1 K3=0.0 #
 H3O1+ + NH3 -> NH41+ + H2O 						RTYPE=2 K1=1.9E-9 K2=-5.0E-1 K3=0.0 #
 H3O1+ + CHOCHO -> CHOCHOH1+ + H2O 					RTYPE=2 K1=5.93E-10 K2=0.0 K3=0.0 #
 H3O1+ + P -> HPO1+ + H2 							RTYPE=2 K1=1.0E-9 K2=0.0 K3=0.0 #
 H3O1+ + PN -> HPN1+ + H2O 							RTYPE=2 K1=6.91E-9 K2=-5.0E-1 K3=0.0 #
 H3O1+ + NH2CO -> NH2CHO1+ + H2O 					RTYPE=2 K1=1.74E-9 K2=-5.0E-1 K3=0.0 #
 H3O1+ + NH2NH -> NH2NH21+ + H2O 					RTYPE=2 K1=1.71E-9 K2=-5.0E-1 K3=0.0 #
 H3O1+ + NH2NH2 -> NH2NH2H1+ + H2O 					RTYPE=2 K1=1.71E-9 K2=-5.0E-1 K3=0.0 #
 H3O1+ + CH3OCO -> HCOOCH31+ + H2O 					RTYPE=2 K1=1.44E-9 K2=-5.0E-1 K3=0.0 #
 H3O1+ + CH3OOH -> CH3OOH21+ + H2O 					RTYPE=2 K1=1.4E-9 K2=-5.0E-1 K3=0.0 #
 H3O1+ + CH3OOCH3 -> CH3OH + CH3O1+ + H2O 			RTYPE=2 K1=1.18E-9 K2=-5.0E-1 K3=0.0 #
 HCN1+ + C -> CH1+ + CN 							RTYPE=2 K1=1.1E-9 K2=0.0 K3=0.0 #
 HCN1+ + CH -> CH21+ + CN 							RTYPE=2 K1=6.3E-10 K2=0.0 K3=0.0 #
 HCN1+ + CH2 -> CH31+ + CN 							RTYPE=2 K1=8.7E-10 K2=0.0 K3=0.0 #
 HCN1+ + CH4 -> H2CN1+ + CH3 						RTYPE=2 K1=1.04E-9 K2=0.0 K3=0.0 #
 HCN1+ + CO -> HCO1+ + CN 							RTYPE=2 K1=1.4E-10 K2=0.0 K3=0.0 #
 HCN1+ + CO2 -> HOCO1+ + CN 						RTYPE=2 K1=2.1E-10 K2=0.0 K3=0.0 #
 HCN1+ + H -> H1+ + HCN 							RTYPE=2 K1=3.7E-11 K2=0.0 K3=0.0 #
 HCN1+ + H2 -> H2CN1+ + H 							RTYPE=2 K1=9.0E-10 K2=0.0 K3=0.0 #
 HCN1+ + H2CO -> CH2OH1+ + CN 						RTYPE=2 K1=1.0E-9 K2=0.0 K3=0.0 #
 HCN1+ + H2O -> H2O1+ + HCN 						RTYPE=2 K1=1.8E-9 K2=0.0 K3=0.0 #
 HCN1+ + H2O -> H3O1+ + CN 							RTYPE=2 K1=8.5E-10 K2=0.0 K3=0.0 #
 HCN1+ + HCN -> H2CN1+ + CN 						RTYPE=2 K1=1.6E-9 K2=0.0 K3=0.0 #
 HCN1+ + HCO -> H2CN1+ + CO 						RTYPE=2 K1=3.7E-10 K2=0.0 K3=0.0 #
 HCN1+ + HCO -> H2CO1+ + CN 						RTYPE=2 K1=3.7E-10 K2=0.0 K3=0.0 #
 HCN1+ + HNC -> H2CN1+ + CN 						RTYPE=2 K1=1.0E-9 K2=0.0 K3=0.0 #
 HCN1+ + NH -> NH21+ + CN 							RTYPE=2 K1=6.5E-10 K2=0.0 K3=0.0 #
 HCN1+ + NH2 -> NH31+ + CN 							RTYPE=2 K1=9.0E-10 K2=0.0 K3=0.0 #
 HCN1+ + NH3 -> NH31+ + HCN 						RTYPE=2 K1=1.7E-9 K2=0.0 K3=0.0 #
 HCN1+ + NH3 -> H2CN1+ + NH2 						RTYPE=2 K1=8.4E-10 K2=0.0 K3=0.0 #
 HCN1+ + NO -> NO1+ + HCN 							RTYPE=2 K1=8.1E-10 K2=0.0 K3=0.0 #
 HCN1+ + O -> O1+ + HCN 							RTYPE=2 K1=6.5E-11 K2=0.0 K3=0.0 #
 HCN1+ + O2 -> O21+ + HCN 							RTYPE=2 K1=3.2E-10 K2=0.0 K3=0.0 #
 HCN1+ + OH -> H2O1+ + CN 							RTYPE=2 K1=6.3E-10 K2=0.0 K3=0.0 #
 HCO1+ + C -> CH1+ + CO 							RTYPE=2 K1=1.1E-9 K2=0.0 K3=0.0 #
 HCO1+ + CH3CH2 -> CH3CH31+ + CO 					RTYPE=2 K1=1.02E-9 K2=-5.0E-1 K3=0.0 #
 HCO1+ + CH -> CH21+ + CO 							RTYPE=2 K1=4.5E-9 K2=-5.0E-1 K3=0.0 #
 HCO1+ + CH2 -> CH31+ + CO 							RTYPE=2 K1=7.19E-10 K2=-5.0E-1 K3=0.0 #
 HCO1+ + CH2CO -> CH3CO1+ + CO 						RTYPE=2 K1=1.3E-9 K2=-5.0E-1 K3=0.0 #
 HCO1+ + CH2NH -> CH3NH1+ + CO 						RTYPE=2 K1=2.0E-9 K2=-5.0E-1 K3=0.0 #
 HCO1+ + CH2OH -> CH3OH1+ + CO 						RTYPE=2 K1=1.15E-9 K2=-5.0E-1 K3=0.0 #
 HCO1+ + CH3CHO -> CH3CH2O1+ + CO 					RTYPE=2 K1=2.5E-9 K2=-5.0E-1 K3=0.0 #
 HCO1+ + CH3CN -> CH3CNH1+ + CO 					RTYPE=2 K1=3.7E-9 K2=-5.0E-1 K3=0.0 #
 HCO1+ + CH3CO -> CH3CHO1+ + CO 					RTYPE=2 K1=1.67E-9 K2=-5.0E-1 K3=0.0 #
 HCO1+ + CH3NH -> CH3NH21+ + CO 					RTYPE=2 K1=1.17E-9 K2=-5.0E-1 K3=0.0 #
 HCO1+ + CH3NH2 -> CH3NH31+ + CO 					RTYPE=2 K1=1.3E-9 K2=-5.0E-1 K3=0.0 #
 HCO1+ + CH3O -> CH3OH1+ + CO 						RTYPE=2 K1=1.76E-10 K2=-5.0E-1 K3=0.0 #
 HCO1+ + CH3OCH3 -> CH3OCH41+ + CO 					RTYPE=2 K1=1.2E-9 K2=-5.0E-1 K3=0.0 #
 HCO1+ + CH3OH -> CH3OH21+ + CO 					RTYPE=2 K1=1.7E-9 K2=-5.0E-1 K3=0.0 #
 HCO1+ + COCHO -> CHOCHO1+ + CO 					RTYPE=2 K1=7.54E-10 K2=-5.0E-1 K3=0.0 #
 HCO1+ + COOH -> HCOOH1+ + CO 						RTYPE=2 K1=2.71E-9 K2=-5.0E-1 K3=0.0 #
 HCO1+ + H2CO -> CH2OH1+ + CO 						RTYPE=2 K1=2.4E-9 K2=-5.0E-1 K3=0.0 #
 HCO1+ + H2O -> H3O1+ + CO 							RTYPE=2 K1=2.1E-9 K2=-5.0E-1 K3=0.0 #
 HCO1+ + HCN -> H2CN1+ + CO 						RTYPE=2 K1=7.3E-9 K2=-5.0E-1 K3=0.0 #
 HCO1+ + HCO -> H2CO1+ + CO 						RTYPE=2 K1=1.0E-9 K2=-5.0E-1 K3=0.0 #
 HCO1+ + HCOOH -> HCOOH21+ + CO 					RTYPE=2 K1=1.3E-9 K2=-5.0E-1 K3=0.0 #
 HCO1+ + HNC -> H2CN1+ + CO 						RTYPE=2 K1=6.63E-9 K2=-5.0E-1 K3=0.0 #
 HCO1+ + HNCHO -> NH2CHO1+ + CO 					RTYPE=2 K1=1.5E-9 K2=-5.0E-1 K3=0.0 #
 HCO1+ + HNCO -> HNCHO1+ + CO 						RTYPE=2 K1=1.59E-9 K2=-5.0E-1 K3=0.0 #
 HCO1+ + HNCONH -> NH2CONH1+ + CO 					RTYPE=2 K1=2.78E-9 K2=-5.0E-1 K3=0.0 #
 HCO1+ + HNCOOH -> NH2COOH1+ + CO 					RTYPE=2 K1=1.41E-9 K2=-5.0E-1 K3=0.0 #
 HCO1+ + HNO -> H2NO1+ + CO 						RTYPE=2 K1=1.7E-9 K2=-5.0E-1 K3=0.0 #
 HCO1+ + HNOH -> NH2OH1+ + CO 						RTYPE=2 K1=1.56E-9 K2=-5.0E-1 K3=0.0 #
 HCO1+ + CH3ONH -> NH2OCH31+ + CO 					RTYPE=2 K1=1.06E-9 K2=-5.0E-1 K3=0.0 #
 HCO1+ + HOCOOH -> HOCOOH21+ + CO 					RTYPE=2 K1=1.38E-9 K2=-5.0E-1 K3=0.0 #
 HCO1+ + HPO -> H2PO1+ + CO 						RTYPE=2 K1=5.03E-9 K2=-5.0E-1 K3=0.0 #
 HCO1+ + NH -> NH21+ + CO 							RTYPE=2 K1=3.8E-9 K2=-5.0E-1 K3=0.0 #
 HCO1+ + NH2 -> NH31+ + CO 							RTYPE=2 K1=2.2E-9 K2=-5.0E-1 K3=0.0 #
 HCO1+ + NH2CHO -> NH2CHOH1+ + CO 					RTYPE=2 K1=8.15E-9 K2=-5.0E-1 K3=0.0 #
 HCO1+ + NH2CN -> NH2CNH1+ + CO 					RTYPE=2 K1=4.0E-9 K2=-5.0E-1 K3=0.0 #
 HCO1+ + NH2OH -> NH3OH1+ + CO 						RTYPE=2 K1=4.45E-10 K2=-5.0E-1 K3=0.0 #
 HCO1+ + NH3 -> NH41+ + CO 							RTYPE=2 K1=1.7E-9 K2=-5.0E-1 K3=0.0 #
 HCO1+ + OH -> H2O1+ + CO 							RTYPE=2 K1=2.33E-9 K2=-5.0E-1 K3=0.0 #
 HCO1+ + OH -> HOCO1+ + H 							RTYPE=2 K1=2.33E-9 K2=-5.0E-1 K3=0.0 #
 HCO1+ + CHOCHO -> CHOCHOH1+ + CO 					RTYPE=2 K1=5.02E-10 K2=0.0 K3=0.0 #
 HCO1+ + PN -> HPN1+ + CO 							RTYPE=2 K1=6.01E-9 K2=-5.0E-1 K3=0.0 #
 HCO1+ + PO -> HPO1+ + CO 							RTYPE=2 K1=4.08E-9 K2=-5.0E-1 K3=0.0 #
 HCO1+ + NH2CO -> NH2CHO1+ + CO 					RTYPE=2 K1=1.5E-9 K2=-5.0E-1 K3=0.0 #
 HCO1+ + NH2NH -> NH2NH21+ + CO 					RTYPE=2 K1=1.53E-9 K2=-5.0E-1 K3=0.0 #
 HCO1+ + NH2NH2 -> NH2NH2H1+ + CO 					RTYPE=2 K1=1.53E-9 K2=-5.0E-1 K3=0.0 #
 HCO1+ + CH3OCO -> HCOOCH31+ + CO 					RTYPE=2 K1=1.21E-9 K2=-5.0E-1 K3=0.0 #
 HCO1+ + CH3OOH -> CH3OOH21+ + CO 					RTYPE=2 K1=1.17E-9 K2=-5.0E-1 K3=0.0 #
 HCO1+ + CH3OOCH3 -> CH3OH + CH3O1+ + CO 			RTYPE=2 K1=9.78E-10 K2=-5.0E-1 K3=0.0 #
 HOCO1+ + C -> CH1+ + CO2 							RTYPE=2 K1=1.0E-9 K2=0.0 K3=0.0 #
 HOCO1+ + CH3CN -> CH3CNH1+ + CO2 					RTYPE=2 K1=3.28E-9 K2=-5.0E-1 K3=0.0 #
 HOCO1+ + CH4 -> CH51+ + CO2 						RTYPE=2 K1=7.8E-10 K2=0.0 K3=0.0 #
 HOCO1+ + CO -> HCO1+ + CO2 						RTYPE=2 K1=2.47E-10 K2=-5.0E-1 K3=0.0 #
 HOCO1+ + H2O -> H3O1+ + CO2 						RTYPE=2 K1=2.0E-9 K2=-5.0E-1 K3=0.0 #
 HOCO1+ + NH3 -> NH41+ + CO2 						RTYPE=2 K1=1.62E-9 K2=-5.0E-1 K3=0.0 #
 HOCO1+ + O -> HCO1+ + O2 							RTYPE=2 K1=1.0E-9 K2=0.0 K3=0.0 #
 HNC1+ + C -> CH1+ + CN 							RTYPE=2 K1=1.1E-9 K2=0.0 K3=0.0 #
 HNC1+ + CH -> CH21+ + CN 							RTYPE=2 K1=6.3E-10 K2=0.0 K3=0.0 #
 HNC1+ + CH2 -> CH31+ + CN 							RTYPE=2 K1=8.7E-10 K2=0.0 K3=0.0 #
 HNC1+ + H2 -> H2CN1+ + H 							RTYPE=2 K1=7.0E-10 K2=0.0 K3=0.0 #
 HNC1+ + H2CO -> CH2OH1+ + CN 						RTYPE=2 K1=1.0E-9 K2=0.0 K3=0.0 #
 HNC1+ + H2O -> H3O1+ + CN 							RTYPE=2 K1=8.5E-10 K2=0.0 K3=0.0 #
 HNC1+ + HCN -> H2CN1+ + CN 						RTYPE=2 K1=1.6E-9 K2=0.0 K3=0.0 #
 HNC1+ + HCO -> H2CO1+ + CN 						RTYPE=2 K1=3.7E-10 K2=0.0 K3=0.0 #
 HNC1+ + HCO -> H2CN1+ + CO 						RTYPE=2 K1=3.7E-10 K2=0.0 K3=0.0 #
 HNC1+ + NH -> NH21+ + CN 							RTYPE=2 K1=6.5E-10 K2=0.0 K3=0.0 #
 HNC1+ + NH2 -> NH31+ + CN 							RTYPE=2 K1=9.0E-10 K2=0.0 K3=0.0 #
 HNC1+ + NH3 -> NH31+ + HNC 						RTYPE=2 K1=1.7E-9 K2=0.0 K3=0.0 #
 HNC1+ + NO -> NO1+ + HNC 							RTYPE=2 K1=8.1E-10 K2=0.0 K3=0.0 #
 HNC1+ + O2 -> NO1+ + HCO 							RTYPE=2 K1=9.0E-11 K2=0.0 K3=0.0 #
 HNC1+ + OH -> H2O1+ + CN 							RTYPE=2 K1=6.3E-10 K2=0.0 K3=0.0 #
 HNO1+ + C -> CH1+ + NO 							RTYPE=2 K1=1.0E-9 K2=0.0 K3=0.0 #
 HNO1+ + CH -> CH21+ + NO 							RTYPE=2 K1=4.4E-9 K2=-5.0E-1 K3=0.0 #
 HNO1+ + CH2 -> CH31+ + NO 							RTYPE=2 K1=7.11E-10 K2=-5.0E-1 K3=0.0 #
 HNO1+ + CH4 -> CH51+ + NO 							RTYPE=2 K1=1.0E-10 K2=0.0 K3=0.0 #
 HNO1+ + CO -> HCO1+ + NO 							RTYPE=2 K1=2.7E-10 K2=-5.0E-1 K3=0.0 #
 HNO1+ + CO2 -> HOCO1+ + NO 						RTYPE=2 K1=1.0E-10 K2=0.0 K3=0.0 #
 HNO1+ + H2CO -> CH2OH1+ + NO 						RTYPE=2 K1=2.3E-9 K2=-5.0E-1 K3=0.0 #
 HNO1+ + H2O -> H3O1+ + NO 							RTYPE=2 K1=2.1E-9 K2=-5.0E-1 K3=0.0 #
 HNO1+ + HCN -> H2CN1+ + NO 						RTYPE=2 K1=7.2E-9 K2=-5.0E-1 K3=0.0 #
 HNO1+ + HCO -> H2CO1+ + NO 						RTYPE=2 K1=1.0E-9 K2=-5.0E-1 K3=0.0 #
 HNO1+ + HNC -> H2CN1+ + NO 						RTYPE=2 K1=6.52E-9 K2=-5.0E-1 K3=0.0 #
 HNO1+ + NH -> NH21+ + NO 							RTYPE=2 K1=3.8E-9 K2=-5.0E-1 K3=0.0 #
 HNO1+ + NH2 -> NH31+ + NO 							RTYPE=2 K1=2.2E-9 K2=-5.0E-1 K3=0.0 #
 HNO1+ + NH3 -> NH41+ + NO 							RTYPE=2 K1=1.7E-9 K2=-5.0E-1 K3=0.0 #
 HNO1+ + NO -> NO1+ + HNO 							RTYPE=2 K1=3.6E-10 K2=-5.0E-1 K3=0.0 #
 HNO1+ + O -> NO21+ + H 							RTYPE=2 K1=1.0E-12 K2=0.0 K3=0.0 #
 HNO1+ + OH -> H2O1+ + NO 							RTYPE=2 K1=4.6E-9 K2=-5.0E-1 K3=0.0 #
 HOC1+ + CO -> HCO1+ + CO 							RTYPE=2 K1=4.0E-10 K2=0.0 K3=0.0 #
 HOC1+ + H2 -> HCO1+ + H2 							RTYPE=2 K1=1.0E-11 K2=0.0 K3=0.0 #
 HOC1+ + N2 -> N2H1+ + CO 							RTYPE=2 K1=2.0E-9 K2=0.0 K3=0.0 #
 HPO1+ + H2O -> H3O1+ + PO 							RTYPE=2 K1=1.0E-9 K2=0.0 K3=0.0 #
 N1+ + CH -> CN1+ + H 								RTYPE=2 K1=3.6E-10 K2=0.0 K3=0.0 #
 N1+ + CH -> CH1+ + N 								RTYPE=2 K1=3.6E-10 K2=0.0 K3=0.0 #
 N1+ + CH2 -> CH21+ + N 							RTYPE=2 K1=1.0E-9 K2=0.0 K3=0.0 #
 N1+ + CH3OH -> NO1+ + CH3 + H 						RTYPE=2 K1=3.1E-10 K2=0.0 K3=0.0 #
 N1+ + CH3OH -> CH31+ + NO + H 						RTYPE=2 K1=1.24E-10 K2=0.0 K3=0.0 #
 N1+ + CH3OH -> H2CO1+ + NH + H 					RTYPE=2 K1=9.3E-10 K2=0.0 K3=0.0 #
 N1+ + CH3OH -> CH3O1+ + NH 						RTYPE=2 K1=4.96E-10 K2=0.0 K3=0.0 #
 N1+ + CH3OH -> CH3OH1+ + N 						RTYPE=2 K1=1.24E-9 K2=0.0 K3=0.0 #
 N1+ + CH4 -> CH31+ + NH 							RTYPE=2 K1=4.7E-10 K2=0.0 K3=0.0 #
 N1+ + CH4 -> H2CN1+ + H + H 						RTYPE=2 K1=3.8E-10 K2=0.0 K3=0.0 #
 N1+ + CH4 -> H2CN1+ + H2 							RTYPE=2 K1=3.8E-10 K2=0.0 K3=0.0 #
 N1+ + CH4 -> CH41+ + N 							RTYPE=2 K1=2.8E-11 K2=0.0 K3=0.0 #
 N1+ + CH4 -> HCN1+ + H2 + H 						RTYPE=2 K1=5.6E-11 K2=0.0 K3=0.0 #
 N1+ + CH4 -> CH31+ + N + H 						RTYPE=2 K1=4.7E-10 K2=0.0 K3=0.0 #
 N1+ + CN -> CN1+ + N 								RTYPE=2 K1=1.1E-9 K2=0.0 K3=0.0 #
 N1+ + CO -> CO1+ + N 								RTYPE=2 K1=4.9E-10 K2=0.0 K3=0.0 #
 N1+ + CO -> NO1+ + C 								RTYPE=2 K1=6.0E-11 K2=0.0 K3=0.0 #
 N1+ + CO2 -> CO1+ + NO 							RTYPE=2 K1=2.5E-10 K2=0.0 K3=0.0 #
 N1+ + CO2 -> CO21+ + N 							RTYPE=2 K1=1.1E-9 K2=0.0 K3=0.0 #
 N1+ + H2 -> NH1+ + H 								RTYPE=2 K1=1.0E-9 K2=0.0 K3=8.5E+1 #
 N1+ + H2CO -> HCO1+ + NH 							RTYPE=2 K1=7.3E-10 K2=0.0 K3=0.0 #
 N1+ + H2CO -> H2CO1+ + N 							RTYPE=2 K1=1.9E-9 K2=0.0 K3=0.0 #
 N1+ + H2CO -> NO1+ + CH2 							RTYPE=2 K1=2.9E-10 K2=0.0 K3=0.0 #
 N1+ + H2O -> H2O1+ + N 							RTYPE=2 K1=2.6E-9 K2=0.0 K3=0.0 #
 N1+ + HCN -> HCN1+ + N 							RTYPE=2 K1=1.2E-9 K2=0.0 K3=0.0 #
 N1+ + HCO -> NH1+ + CO 							RTYPE=2 K1=4.5E-10 K2=0.0 K3=0.0 #
 N1+ + HCO -> HCO1+ + N 							RTYPE=2 K1=4.5E-10 K2=0.0 K3=0.0 #
 N1+ + NH -> N21+ + H 								RTYPE=2 K1=3.7E-10 K2=0.0 K3=0.0 #
 N1+ + NH -> NH1+ + N 								RTYPE=2 K1=3.7E-10 K2=0.0 K3=0.0 #
 N1+ + NH2 -> NH21+ + N 							RTYPE=2 K1=1.0E-9 K2=0.0 K3=0.0 #
 N1+ + NH3 -> NH31+ + N 							RTYPE=2 K1=2.0E-9 K2=0.0 K3=0.0 #
 N1+ + NH3 -> N2H1+ + H2 							RTYPE=2 K1=2.2E-10 K2=0.0 K3=0.0 #
 N1+ + NH3 -> NH21+ + NH 							RTYPE=2 K1=2.2E-10 K2=0.0 K3=0.0 #
 N1+ + NO -> N21+ + O 								RTYPE=2 K1=5.0E-11 K2=0.0 K3=0.0 #
 N1+ + NO -> NO1+ + N 								RTYPE=2 K1=5.1E-10 K2=0.0 K3=0.0 #
 N1+ + O2 -> NO1+ + O 								RTYPE=2 K1=1.7E-10 K2=0.0 K3=0.0 #
 N1+ + O2 -> O21+ + N 								RTYPE=2 K1=4.0E-10 K2=0.0 K3=0.0 #
 N1+ + O2 -> O1+ + NO 								RTYPE=2 K1=3.6E-11 K2=0.0 K3=0.0 #
 N1+ + OH -> NO1+ + H 								RTYPE=2 K1=3.7E-10 K2=0.0 K3=0.0 #
 N1+ + OH -> OH1+ + N 								RTYPE=2 K1=3.7E-10 K2=0.0 K3=0.0 #
 N21+ + C -> C1+ + N2 								RTYPE=2 K1=1.1E-10 K2=0.0 K3=0.0 #
 N21+ + CH -> CH1+ + N2 							RTYPE=2 K1=6.3E-10 K2=0.0 K3=0.0 #
 N21+ + CH2 -> CH21+ + N2 							RTYPE=2 K1=8.7E-10 K2=0.0 K3=0.0 #
 N21+ + CH4 -> CH31+ + N2 + H 						RTYPE=2 K1=9.3E-10 K2=0.0 K3=0.0 #
 N21+ + CH4 -> CH21+ + H2 + N2 						RTYPE=2 K1=7.0E-11 K2=0.0 K3=0.0 #
 N21+ + CN -> CN1+ + N2 							RTYPE=2 K1=1.0E-10 K2=0.0 K3=0.0 #
 N21+ + CO -> CO1+ + N2 							RTYPE=2 K1=7.0E-11 K2=0.0 K3=0.0 #
 N21+ + CO2 -> CO21+ + N2 							RTYPE=2 K1=9.0E-10 K2=0.0 K3=0.0 #
 N21+ + H -> H1+ + N2 								RTYPE=2 K1=1.2E-10 K2=0.0 K3=0.0 #
 N21+ + H2 -> N2H1+ + H 							RTYPE=2 K1=1.7E-9 K2=0.0 K3=0.0 #
 N21+ + H2CO -> H2CO1+ + N2 						RTYPE=2 K1=3.77E-10 K2=0.0 K3=0.0 #
 N21+ + H2CO -> HCO1+ + N2 + H 						RTYPE=2 K1=2.5E-9 K2=0.0 K3=0.0 #
 N21+ + H2O -> H2O1+ + N2 							RTYPE=2 K1=2.2E-9 K2=0.0 K3=0.0 #
 N21+ + H2O -> N2H1+ + OH 							RTYPE=2 K1=2.0E-9 K2=0.0 K3=0.0 #
 N21+ + HCN -> HCN1+ + N2 							RTYPE=2 K1=1.0E-9 K2=0.0 K3=0.0 #
 N21+ + HCO -> HCO1+ + N2 							RTYPE=2 K1=3.7E-10 K2=0.0 K3=0.0 #
 N21+ + HCO -> N2H1+ + CO 							RTYPE=2 K1=3.7E-10 K2=0.0 K3=0.0 #
 N21+ + N -> N1+ + N2 								RTYPE=2 K1=1.0E-11 K2=0.0 K3=0.0 #
 N21+ + NH -> NH1+ + N2 							RTYPE=2 K1=6.5E-10 K2=0.0 K3=0.0 #
 N21+ + NH2 -> NH21+ + N2 							RTYPE=2 K1=8.9E-10 K2=0.0 K3=0.0 #
 N21+ + NH3 -> NH31+ + N2 							RTYPE=2 K1=1.9E-9 K2=0.0 K3=0.0 #
 N21+ + NO -> NO1+ + N2 							RTYPE=2 K1=4.4E-10 K2=0.0 K3=0.0 #
 N21+ + O -> NO1+ + N 								RTYPE=2 K1=1.4E-10 K2=0.0 K3=0.0 #
 N21+ + O -> O1+ + N2 								RTYPE=2 K1=1.0E-11 K2=0.0 K3=0.0 #
 N21+ + O2 -> O21+ + N2 							RTYPE=2 K1=5.0E-11 K2=0.0 K3=0.0 #
 N21+ + OH -> OH1+ + N2 							RTYPE=2 K1=6.3E-10 K2=0.0 K3=0.0 #
 N2H1+ + C -> CH1+ + N2 							RTYPE=2 K1=1.1E-9 K2=0.0 K3=0.0 #
 N2H1+ + CH -> CH21+ + N2 							RTYPE=2 K1=4.5E-9 K2=-5.0E-1 K3=0.0 #
 N2H1+ + CH2 -> CH31+ + N2 							RTYPE=2 K1=7.19E-10 K2=-5.0E-1 K3=0.0 #
 N2H1+ + CH4 -> CH51+ + N2 							RTYPE=2 K1=9.0E-10 K2=0.0 K3=0.0 #
 N2H1+ + CO -> HCO1+ + N2 							RTYPE=2 K1=8.8E-10 K2=0.0 K3=0.0 #
 N2H1+ + CO2 -> HOCO1+ + N2 						RTYPE=2 K1=9.2E-10 K2=0.0 K3=0.0 #
 N2H1+ + H2CO -> CH2OH1+ + N2 						RTYPE=2 K1=2.4E-9 K2=-5.0E-1 K3=0.0 #
 N2H1+ + H2O -> H3O1+ + N2 							RTYPE=2 K1=2.2E-9 K2=-5.0E-1 K3=0.0 #
 N2H1+ + HCN -> H2CN1+ + N2 						RTYPE=2 K1=7.3E-9 K2=-5.0E-1 K3=0.0 #
 N2H1+ + HCO -> H2CO1+ + N2 						RTYPE=2 K1=1.0E-9 K2=-5.0E-1 K3=0.0 #
 N2H1+ + HNC -> H2CN1+ + N2 						RTYPE=2 K1=6.63E-9 K2=-5.0E-1 K3=0.0 #
 N2H1+ + NH -> NH21+ + N2 							RTYPE=2 K1=3.8E-9 K2=-5.0E-1 K3=0.0 #
 N2H1+ + NH2 -> NH31+ + N2 							RTYPE=2 K1=2.2E-9 K2=-5.0E-1 K3=0.0 #
 N2H1+ + NH2CHO -> NH2CHOH1+ + N2 					RTYPE=2 K1=8.15E-9 K2=-5.0E-1 K3=0.0 #
 N2H1+ + NH3 -> NH41+ + N2 							RTYPE=2 K1=1.7E-9 K2=-5.0E-1 K3=0.0 #
 N2H1+ + OH -> H2O1+ + N2 							RTYPE=2 K1=4.7E-9 K2=-5.0E-1 K3=0.0 #
 NH1+ + C -> CH1+ + N 								RTYPE=2 K1=1.6E-9 K2=0.0 K3=0.0 #
 NH1+ + CH -> CH21+ + N 							RTYPE=2 K1=9.9E-10 K2=0.0 K3=0.0 #
 NH1+ + CH2 -> CH31+ + N 							RTYPE=2 K1=1.4E-9 K2=0.0 K3=0.0 #
 NH1+ + CN -> HCN1+ + N 							RTYPE=2 K1=1.6E-9 K2=0.0 K3=0.0 #
 NH1+ + CO -> OCN1+ + H 							RTYPE=2 K1=5.39E-10 K2=0.0 K3=0.0 #
 NH1+ + CO -> HCO1+ + N 							RTYPE=2 K1=1.6E-9 K2=0.0 K3=0.0 #
 NH1+ + CO2 -> HNO1+ + CO 							RTYPE=2 K1=3.85E-10 K2=0.0 K3=0.0 #
 NH1+ + CO2 -> HOCO1+ + N 							RTYPE=2 K1=3.9E-10 K2=0.0 K3=0.0 #
 NH1+ + CO2 -> NO1+ + HCO 							RTYPE=2 K1=3.3E-10 K2=0.0 K3=0.0 #
 NH1+ + H2 -> H31+ + N 								RTYPE=2 K1=2.25E-10 K2=0.0 K3=0.0 #
 NH1+ + H2 -> NH21+ + H 							RTYPE=2 K1=1.0E-9 K2=0.0 K3=0.0 #
 NH1+ + H2CO -> H2CO1+ + NH 						RTYPE=2 K1=9.9E-10 K2=0.0 K3=0.0 #
 NH1+ + H2CO -> CH2OH1+ + N 						RTYPE=2 K1=4.95E-10 K2=0.0 K3=0.0 #
 NH1+ + H2CO -> HCO1+ + NH2 						RTYPE=2 K1=1.8E-9 K2=0.0 K3=0.0 #
 NH1+ + H2O -> NH21+ + OH 							RTYPE=2 K1=8.75E-10 K2=0.0 K3=0.0 #
 NH1+ + H2O -> H3O1+ + N 							RTYPE=2 K1=2.1E-9 K2=0.0 K3=0.0 #
 NH1+ + H2O -> NH31+ + O 							RTYPE=2 K1=1.75E-10 K2=0.0 K3=0.0 #
 NH1+ + H2O -> H2O1+ + NH 							RTYPE=2 K1=1.05E-9 K2=0.0 K3=0.0 #
 NH1+ + H2O -> HNO1+ + H2 							RTYPE=2 K1=3.5E-10 K2=0.0 K3=0.0 #
 NH1+ + HCN -> H2CN1+ + N 							RTYPE=2 K1=1.8E-9 K2=0.0 K3=0.0 #
 NH1+ + HCO -> H2CO1+ + N 							RTYPE=2 K1=1.3E-9 K2=0.0 K3=0.0 #
 NH1+ + HNC -> H2CN1+ + N 							RTYPE=2 K1=1.8E-9 K2=0.0 K3=0.0 #
 NH1+ + N -> N21+ + H 								RTYPE=2 K1=1.3E-9 K2=0.0 K3=0.0 #
 NH1+ + N2 -> N2H1+ + N 							RTYPE=2 K1=1.5E-9 K2=0.0 K3=0.0 #
 NH1+ + NH -> NH21+ + N 							RTYPE=2 K1=1.0E-9 K2=0.0 K3=0.0 #
 NH1+ + NH2 -> NH31+ + N 							RTYPE=2 K1=1.5E-9 K2=0.0 K3=0.0 #
 NH1+ + NH3 -> NH41+ + N 							RTYPE=2 K1=6.0E-10 K2=0.0 K3=0.0 #
 NH1+ + NH3 -> NH31+ + NH 							RTYPE=2 K1=1.8E-9 K2=0.0 K3=0.0 #
 NH1+ + NO -> NO1+ + NH 							RTYPE=2 K1=7.1E-10 K2=0.0 K3=0.0 #
 NH1+ + NO -> N2H1+ + O 							RTYPE=2 K1=1.78E-10 K2=0.0 K3=0.0 #
 NH1+ + O -> OH1+ + N 								RTYPE=2 K1=1.0E-9 K2=0.0 K3=0.0 #
 NH1+ + O2 -> O21+ + NH 							RTYPE=2 K1=4.5E-10 K2=0.0 K3=0.0 #
 NH1+ + O2 -> O2H1+ + N 							RTYPE=2 K1=1.6E-10 K2=0.0 K3=0.0 #
 NH1+ + O2 -> NO1+ + OH 							RTYPE=2 K1=2.0E-10 K2=0.0 K3=0.0 #
 NH1+ + OH -> H2O1+ + N 							RTYPE=2 K1=1.0E-9 K2=0.0 K3=0.0 #
 NH21+ + CH -> CH1+ + NH2 							RTYPE=2 K1=3.5E-10 K2=0.0 K3=0.0 #
 NH21+ + CH -> CH21+ + NH 							RTYPE=2 K1=3.5E-10 K2=0.0 K3=0.0 #
 NH21+ + CH2 -> CH31+ + NH 							RTYPE=2 K1=4.9E-10 K2=0.0 K3=0.0 #
 NH21+ + CH2 -> CH21+ + NH2 						RTYPE=2 K1=4.9E-10 K2=0.0 K3=0.0 #
 NH21+ + CN -> H2CN1+ + N 							RTYPE=2 K1=1.0E-10 K2=0.0 K3=0.0 #
 NH21+ + CN -> H2NC1+ + N 							RTYPE=2 K1=1.0E-10 K2=0.0 K3=0.0 #
 NH21+ + H2 -> NH31+ + H 							RTYPE=2 K1=1.2E-10 K2=0.0 K3=0.0 #
 NH21+ + H2CO -> NH31+ + HCO 						RTYPE=2 K1=5.6E-10 K2=0.0 K3=0.0 #
 NH21+ + H2CO -> CH2OH1+ + NH 						RTYPE=2 K1=2.2E-9 K2=0.0 K3=0.0 #
 NH21+ + H2O -> NH41+ + O 							RTYPE=2 K1=3.0E-11 K2=0.0 K3=0.0 #
 NH21+ + H2O -> H3O1+ + NH 							RTYPE=2 K1=1.6E-9 K2=0.0 K3=0.0 #
 NH21+ + H2O -> NH31+ + OH 							RTYPE=2 K1=1.0E-10 K2=0.0 K3=0.0 #
 NH21+ + HCN -> H2CN1+ + NH 						RTYPE=2 K1=1.2E-9 K2=0.0 K3=0.0 #
 NH21+ + HCO -> HCO1+ + NH2 						RTYPE=2 K1=4.3E-10 K2=0.0 K3=0.0 #
 NH21+ + HNC -> H2CN1+ + NH 						RTYPE=2 K1=1.2E-9 K2=0.0 K3=0.0 #
 NH21+ + N -> N2H1+ + H 							RTYPE=2 K1=9.1E-11 K2=0.0 K3=0.0 #
 NH21+ + NH -> NH31+ + N 							RTYPE=2 K1=7.3E-10 K2=0.0 K3=0.0 #
 NH21+ + NH2 -> NH31+ + NH 							RTYPE=2 K1=1.0E-9 K2=0.0 K3=0.0 #
 NH21+ + NH3 -> NH31+ + NH2 						RTYPE=2 K1=1.5E-9 K2=0.0 K3=0.0 #
 NH21+ + NH3 -> NH41+ + NH 							RTYPE=2 K1=1.0E-9 K2=0.0 K3=0.0 #
 NH21+ + NO -> NO1+ + NH2 							RTYPE=2 K1=9.4E-10 K2=0.0 K3=0.0 #
 NH21+ + O -> HNO1+ + H 							RTYPE=2 K1=7.2E-11 K2=0.0 K3=0.0 #
 NH21+ + O2 -> H2NO1+ + O 							RTYPE=2 K1=1.2E-10 K2=0.0 K3=0.0 #
 NH21+ + O2 -> HNO1+ + OH 							RTYPE=2 K1=2.1E-11 K2=0.0 K3=0.0 #
 NH31+ + CH -> NH41+ + C 							RTYPE=2 K1=4.9E-9 K2=-5.0E-1 K3=0.0 #
 NH31+ + CH2 -> CH31+ + NH2 						RTYPE=2 K1=7.97E-10 K2=-5.0E-1 K3=0.0 #
 NH31+ + CH4 -> NH41+ + CH3 						RTYPE=2 K1=3.9E-10 K2=0.0 K3=0.0 #
 NH31+ + H2 -> NH41+ + H 							RTYPE=2 K1=1.5E-14 K2=-1.5 K3=0.0 #
 NH31+ + H2CO -> NH41+ + HCO 						RTYPE=2 K1=2.7E-9 K2=-5.0E-1 K3=0.0 #
 NH31+ + H2O -> NH41+ + OH 							RTYPE=2 K1=2.4E-9 K2=-5.0E-1 K3=0.0 #
 NH31+ + HCO -> HCO1+ + NH3 						RTYPE=2 K1=5.9E-10 K2=-5.0E-1 K3=0.0 #
 NH31+ + HCO -> NH41+ + CO 							RTYPE=2 K1=5.9E-10 K2=-5.0E-1 K3=0.0 #
 NH31+ + NH -> NH41+ + N 							RTYPE=2 K1=4.2E-9 K2=-5.0E-1 K3=0.0 #
 NH31+ + NH2 -> NH41+ + NH 							RTYPE=2 K1=2.5E-9 K2=-5.0E-1 K3=0.0 #
 NH31+ + NH3 -> NH41+ + NH2 						RTYPE=2 K1=2.0E-9 K2=-5.0E-1 K3=0.0 #
 NH31+ + NO -> NO1+ + NH3 							RTYPE=2 K1=4.3E-10 K2=-5.0E-1 K3=0.0 #
 NH31+ + O -> HNO1+ + H2 							RTYPE=2 K1=1.0E-11 K2=0.0 K3=0.0 #
 NH31+ + O -> H2NO1+ + H 							RTYPE=2 K1=1.0E-11 K2=0.0 K3=0.0 #
 NH31+ + OH -> NH41+ + O 							RTYPE=2 K1=5.2E-9 K2=-5.0E-1 K3=0.0 #
 NH41+ + C -> H2CN1+ + H2 							RTYPE=2 K1=1.0E-11 K2=0.0 K3=0.0 #
 NH41+ + C -> H2NC1+ + H2 							RTYPE=2 K1=1.0E-11 K2=0.0 K3=0.0 #
 NO21+ + H -> NO1+ + OH 							RTYPE=2 K1=1.9E-10 K2=0.0 K3=0.0 #
 NO21+ + H2 -> NO1+ + H2O 							RTYPE=2 K1=1.5E-10 K2=0.0 K3=0.0 #
 O1+ + CH -> CH1+ + O 								RTYPE=2 K1=3.5E-10 K2=0.0 K3=0.0 #
 O1+ + CH -> CO1+ + H 								RTYPE=2 K1=3.5E-10 K2=0.0 K3=0.0 #
 O1+ + CH2 -> CH21+ + O 							RTYPE=2 K1=9.7E-10 K2=0.0 K3=0.0 #
 O1+ + CH3OH -> CH3OH1+ + O 						RTYPE=2 K1=4.75E-10 K2=0.0 K3=0.0 #
 O1+ + CH3OH -> H2CO1+ + H2O 						RTYPE=2 K1=9.5E-11 K2=0.0 K3=0.0 #
 O1+ + CH3OH -> CH3O1+ + OH 						RTYPE=2 K1=1.33E-9 K2=0.0 K3=0.0 #
 O1+ + CH4 -> CH31+ + OH 							RTYPE=2 K1=1.1E-10 K2=0.0 K3=0.0 #
 O1+ + CH4 -> CH41+ + O 							RTYPE=2 K1=8.9E-10 K2=0.0 K3=0.0 #
 O1+ + CN -> NO1+ + C 								RTYPE=2 K1=1.0E-9 K2=0.0 K3=0.0 #
 O1+ + CO2 -> O21+ + CO 							RTYPE=2 K1=1.1E-9 K2=0.0 K3=0.0 #
 O1+ + H -> H1+ + O 								RTYPE=2 K1=7.0E-10 K2=0.0 K3=0.0 #
 O1+ + H2 -> OH1+ + H 								RTYPE=2 K1=1.6E-9 K2=0.0 K3=0.0 #
 O1+ + H2CO -> HCO1+ + OH 							RTYPE=2 K1=1.4E-9 K2=0.0 K3=0.0 #
 O1+ + H2CO -> H2CO1+ + O 							RTYPE=2 K1=2.1E-9 K2=0.0 K3=0.0 #
 O1+ + H2O -> H2O1+ + O 							RTYPE=2 K1=3.2E-9 K2=0.0 K3=0.0 #
 O1+ + HCN -> CO1+ + NH 							RTYPE=2 K1=1.2E-9 K2=0.0 K3=0.0 #
 O1+ + HCN -> NO1+ + CH 							RTYPE=2 K1=1.2E-9 K2=0.0 K3=0.0 #
 O1+ + HCN -> HCO1+ + N 							RTYPE=2 K1=1.2E-9 K2=0.0 K3=0.0 #
 O1+ + HCO -> HCO1+ + O 							RTYPE=2 K1=4.3E-10 K2=0.0 K3=0.0 #
 O1+ + HCO -> OH1+ + CO 							RTYPE=2 K1=4.3E-10 K2=0.0 K3=0.0 #
 O1+ + N2 -> NO1+ + N 								RTYPE=2 K1=1.2E-12 K2=0.0 K3=0.0 #
 O1+ + N2O -> NO1+ + NO 							RTYPE=2 K1=6.3E-10 K2=0.0 K3=0.0 #
 O1+ + NH -> NO1+ + H 								RTYPE=2 K1=3.6E-10 K2=0.0 K3=0.0 #
 O1+ + NH -> NH1+ + O 								RTYPE=2 K1=3.6E-10 K2=0.0 K3=0.0 #
 O1+ + NH2 -> NH21+ + O 							RTYPE=2 K1=1.0E-9 K2=0.0 K3=0.0 #
 O1+ + NH3 -> NH31+ + O 							RTYPE=2 K1=1.2E-9 K2=0.0 K3=0.0 #
 O1+ + NO -> NO1+ + O 								RTYPE=2 K1=1.7E-12 K2=0.0 K3=0.0 #
 O1+ + NO2 -> NO21+ + O 							RTYPE=2 K1=1.6E-9 K2=0.0 K3=0.0 #
 O1+ + O2 -> O21+ + O 								RTYPE=2 K1=3.0E-11 K2=0.0 K3=0.0 #
 O1+ + OH -> OH1+ + O 								RTYPE=2 K1=3.6E-10 K2=0.0 K3=0.0 #
 O1+ + OH -> O21+ + H 								RTYPE=2 K1=3.6E-10 K2=0.0 K3=0.0 #
 O21+ + C -> C1+ + O2 								RTYPE=2 K1=5.2E-11 K2=0.0 K3=0.0 #
 O21+ + C -> CO1+ + O 								RTYPE=2 K1=5.2E-11 K2=0.0 K3=0.0 #
 O21+ + CH -> CH1+ + O2 							RTYPE=2 K1=3.1E-10 K2=0.0 K3=0.0 #
 O21+ + CH -> HCO1+ + O 							RTYPE=2 K1=3.1E-10 K2=0.0 K3=0.0 #
 O21+ + CH2 -> CH21+ + O2 							RTYPE=2 K1=4.3E-10 K2=0.0 K3=0.0 #
 O21+ + CH2 -> H2CO1+ + O 							RTYPE=2 K1=4.3E-10 K2=0.0 K3=0.0 #
 O21+ + CH3OH -> CH3O1+ + O2 + H 					RTYPE=2 K1=5.0E-10 K2=0.0 K3=0.0 #
 O21+ + CH3OH -> CH3OH1+ + O2 						RTYPE=2 K1=5.0E-10 K2=0.0 K3=0.0 #
 O21+ + CH4 -> HCOOH21+ + H 						RTYPE=2 K1=3.8E-12 K2=-1.8 K3=0.0 #
 O21+ + H2CO -> H2CO1+ + O2 						RTYPE=2 K1=9.9E-10 K2=0.0 K3=0.0 #
 O21+ + H2CO -> HCO1+ + O2 + H 						RTYPE=2 K1=2.3E-10 K2=0.0 K3=0.0 #
 O21+ + HCO -> HCO1+ + O2 							RTYPE=2 K1=3.6E-10 K2=0.0 K3=0.0 #
 O21+ + HCO -> O2H1+ + CO 							RTYPE=2 K1=3.6E-10 K2=0.0 K3=0.0 #
 O21+ + N -> NO1+ + O 								RTYPE=2 K1=1.8E-10 K2=0.0 K3=0.0 #
 O21+ + NH -> NO21+ + H 							RTYPE=2 K1=3.2E-10 K2=0.0 K3=0.0 #
 O21+ + NH -> HNO1+ + O 							RTYPE=2 K1=3.2E-10 K2=0.0 K3=0.0 #
 O21+ + NH2 -> NH21+ + O2 							RTYPE=2 K1=8.7E-10 K2=0.0 K3=0.0 #
 O21+ + NH3 -> NH31+ + O2 							RTYPE=2 K1=2.4E-9 K2=0.0 K3=0.0 #
 O21+ + NO -> NO1+ + O2 							RTYPE=2 K1=4.5E-10 K2=0.0 K3=0.0 #
 O21+ + NO2 -> NO21+ + O2 							RTYPE=2 K1=6.6E-10 K2=0.0 K3=0.0 #
 O2H1+ + C -> CH1+ + O2 							RTYPE=2 K1=1.0E-9 K2=0.0 K3=0.0 #
 O2H1+ + CH -> CH21+ + O2 							RTYPE=2 K1=6.2E-10 K2=0.0 K3=0.0 #
 O2H1+ + CH2 -> CH31+ + O2 							RTYPE=2 K1=8.5E-10 K2=0.0 K3=0.0 #
 O2H1+ + CN -> HCN1+ + O2 							RTYPE=2 K1=8.6E-10 K2=0.0 K3=0.0 #
 O2H1+ + CO -> HCO1+ + O2 							RTYPE=2 K1=8.4E-10 K2=0.0 K3=0.0 #
 O2H1+ + CO2 -> HOCO1+ + O2 						RTYPE=2 K1=1.1E-9 K2=0.0 K3=0.0 #
 O2H1+ + H2 -> H31+ + O2 							RTYPE=2 K1=3.2E-10 K2=0.0 K3=0.0 #
 O2H1+ + H2CO -> CH2OH1+ + O2 						RTYPE=2 K1=9.8E-10 K2=0.0 K3=0.0 #
 O2H1+ + H2O -> H3O1+ + O2 							RTYPE=2 K1=8.2E-10 K2=0.0 K3=0.0 #
 O2H1+ + HCN -> H2CN1+ + O2 						RTYPE=2 K1=9.7E-10 K2=0.0 K3=0.0 #
 O2H1+ + HCO -> H2CO1+ + O2 						RTYPE=2 K1=7.1E-10 K2=0.0 K3=0.0 #
 O2H1+ + HNC -> H2CN1+ + O2 						RTYPE=2 K1=9.7E-10 K2=0.0 K3=0.0 #
 O2H1+ + N -> NO21+ + H 							RTYPE=2 K1=1.0E-12 K2=0.0 K3=0.0 #
 O2H1+ + N2 -> N2H1+ + O2 							RTYPE=2 K1=7.9E-10 K2=0.0 K3=0.0 #
 O2H1+ + NH -> NH21+ + O2 							RTYPE=2 K1=6.3E-10 K2=0.0 K3=0.0 #
 O2H1+ + NH2 -> NH31+ + O2 							RTYPE=2 K1=8.7E-10 K2=0.0 K3=0.0 #
 O2H1+ + NH3 -> NH41+ + O2 							RTYPE=2 K1=2.0E-9 K2=0.0 K3=0.0 #
 O2H1+ + NO -> HNO1+ + O2 							RTYPE=2 K1=7.7E-10 K2=0.0 K3=0.0 #
 O2H1+ + O -> OH1+ + O2 							RTYPE=2 K1=6.2E-10 K2=0.0 K3=0.0 #
 O2H1+ + OH -> H2O1+ + O2 							RTYPE=2 K1=6.1E-10 K2=0.0 K3=0.0 #
 OH1+ + C -> CH1+ + O 								RTYPE=2 K1=1.2E-9 K2=0.0 K3=0.0 #
 OH1+ + CH -> CH1+ + OH 							RTYPE=2 K1=3.5E-10 K2=0.0 K3=0.0 #
 OH1+ + CH -> CH21+ + O 							RTYPE=2 K1=3.5E-10 K2=0.0 K3=0.0 #
 OH1+ + CH2 -> CH21+ + OH 							RTYPE=2 K1=4.8E-10 K2=0.0 K3=0.0 #
 OH1+ + CH2 -> CH31+ + O 							RTYPE=2 K1=4.8E-10 K2=0.0 K3=0.0 #
 OH1+ + CH4 -> CH51+ + O 							RTYPE=2 K1=1.95E-10 K2=0.0 K3=0.0 #
 OH1+ + CH4 -> H3O1+ + CH2 							RTYPE=2 K1=1.4E-9 K2=0.0 K3=0.0 #
 OH1+ + CN -> HCN1+ + O 							RTYPE=2 K1=1.0E-9 K2=0.0 K3=0.0 #
 OH1+ + CO -> HCO1+ + O 							RTYPE=2 K1=1.0E-9 K2=0.0 K3=0.0 #
 OH1+ + CO2 -> HOCO1+ + O 							RTYPE=2 K1=1.4E-9 K2=0.0 K3=0.0 #
 OH1+ + H2 -> H2O1+ + H 							RTYPE=2 K1=1.1E-9 K2=0.0 K3=0.0 #
 OH1+ + H2CO -> CH2OH1+ + O 						RTYPE=2 K1=1.1E-9 K2=0.0 K3=0.0 #
 OH1+ + H2CO -> H2CO1+ + OH 						RTYPE=2 K1=7.4E-10 K2=0.0 K3=0.0 #
 OH1+ + H2O -> H3O1+ + O 							RTYPE=2 K1=1.3E-9 K2=0.0 K3=0.0 #
 OH1+ + H2O -> H2O1+ + OH 							RTYPE=2 K1=1.5E-9 K2=0.0 K3=0.0 #
 OH1+ + HCN -> H2CN1+ + O 							RTYPE=2 K1=1.2E-9 K2=0.0 K3=0.0 #
 OH1+ + HCO -> H2O1+ + CO 							RTYPE=2 K1=2.8E-10 K2=0.0 K3=0.0 #
 OH1+ + HCO -> HCO1+ + OH 							RTYPE=2 K1=2.8E-10 K2=0.0 K3=0.0 #
 OH1+ + HCO -> H2CO1+ + O 							RTYPE=2 K1=2.8E-10 K2=0.0 K3=0.0 #
 OH1+ + HNC -> H2CN1+ + O 							RTYPE=2 K1=1.2E-9 K2=0.0 K3=0.0 #
 OH1+ + N -> NO1+ + H 								RTYPE=2 K1=8.9E-10 K2=0.0 K3=0.0 #
 OH1+ + N2 -> N2H1+ + O 							RTYPE=2 K1=3.6E-10 K2=0.0 K3=0.0 #
 OH1+ + NH -> NH21+ + O 							RTYPE=2 K1=3.6E-10 K2=0.0 K3=0.0 #
 OH1+ + NH2 -> NH31+ + O 							RTYPE=2 K1=5.0E-10 K2=0.0 K3=0.0 #
 OH1+ + NH2 -> NH21+ + OH 							RTYPE=2 K1=5.0E-10 K2=0.0 K3=0.0 #
 OH1+ + NH3 -> NH41+ + O 							RTYPE=2 K1=1.2E-9 K2=0.0 K3=0.0 #
 OH1+ + NH3 -> NH31+ + OH 							RTYPE=2 K1=1.2E-9 K2=0.0 K3=0.0 #
 OH1+ + NO -> NO1+ + OH 							RTYPE=2 K1=3.6E-10 K2=0.0 K3=0.0 #
 OH1+ + NO -> HNO1+ + O 							RTYPE=2 K1=6.1E-10 K2=0.0 K3=0.0 #
 OH1+ + O -> O21+ + H 								RTYPE=2 K1=7.1E-10 K2=0.0 K3=0.0 #
 OH1+ + O2 -> O21+ + OH 							RTYPE=2 K1=5.9E-10 K2=0.0 K3=0.0 #
 OH1+ + OH -> H2O1+ + O 							RTYPE=2 K1=7.0E-10 K2=0.0 K3=0.0 #
 P1+ + CH4 -> PCH21+ + H2 							RTYPE=2 K1=9.6E-10 K2=0.0 K3=0.0 #
 P1+ + H2O -> HPO1+ + H 							RTYPE=2 K1=1.07E-9 K2=-5.0E-1 K3=0.0 #
 P1+ + H2O -> PO1+ + H2 							RTYPE=2 K1=1.07E-9 K2=-5.0E-1 K3=0.0 #
 P1+ + NH3 -> PNH21+ + H 							RTYPE=2 K1=8.6E-10 K2=-5.0E-1 K3=0.0 #
 P1+ + NH3 -> NH31+ + P 							RTYPE=2 K1=8.6E-10 K2=-5.0E-1 K3=0.0 #
 P1+ + O2 -> PO1+ + O 								RTYPE=2 K1=4.9E-10 K2=0.0 K3=0.0 #
 P1+ + OH -> PO1+ + H 								RTYPE=2 K1=4.6E-9 K2=-5.0E-1 K3=0.0 #
 C1- + NO -> CN1- + O 								RTYPE=3 K1=1.0E-9 K2=0.0 K3=0.0 #
 C1- + O2 -> O1- + CO 								RTYPE=3 K1=4.0E-10 K2=0.0 K3=0.0 #
 H1- + H2O -> OH1- + H2 							RTYPE=3 K1=3.8E-9 K2=0.0 K3=0.0 #
 H1- + HCN -> CN1- + H2 							RTYPE=3 K1=3.8E-9 K2=0.0 K3=0.0 #
 O1- + CH4 -> OH1- + CH3 							RTYPE=3 K1=1.0E-10 K2=0.0 K3=0.0 #
 O1- + CN -> CN1- + O 								RTYPE=3 K1=1.0E-9 K2=0.0 K3=0.0 #
 O1- + H2 -> OH1- + H 								RTYPE=3 K1=3.0E-11 K2=0.0 K3=0.0 #
 O1- + HCN -> CN1- + OH 							RTYPE=3 K1=1.2E-9 K2=0.0 K3=0.0 #
 OH1- + CN -> CN1- + OH 							RTYPE=3 K1=1.0E-9 K2=0.0 K3=0.0 #
 OH1- + HCN -> CN1- + H2O 							RTYPE=3 K1=1.2E-9 K2=0.0 K3=0.0 #
 C1- + CO2 -> CO + CO + E1- 						RTYPE=3 K1=4.7E-11 K2=0.0 K3=0.0 #
 C1+ + H -> CH1+ 									RTYPE=4 K1=1.7E-17 K2=0.0 K3=0.0 #
 C1+ + H2 -> CH21+ 									RTYPE=4 K1=4.0E-16 K2=-2.0E-1 K3=0.0 #
 C1+ + O -> CO1+ 									RTYPE=4 K1=2.5E-18 K2=0.0 K3=0.0 #
 CH31+ + CH3OH -> CH3OCH41+ 						RTYPE=4 K1=7.8E-12 K2=-1.1 K3=0.0 #
 CH31+ + CO -> CH3CO1+ 								RTYPE=4 K1=1.2E-13 K2=-1.3 K3=0.0 #
 CH31+ + H2 -> CH51+ 								RTYPE=4 K1=1.3E-14 K2=-1.0 K3=0.0 #
 CH31+ + H2O -> CH3OH21+ 							RTYPE=4 K1=2.0E-12 K2=0.0 K3=0.0 #
 CH31+ + HCN -> CH3CNH1+ 							RTYPE=4 K1=9.0E-9 K2=-5.0E-1 K3=0.0 #
 CH31+ + NH3 -> CH3NH31+ 							RTYPE=4 K1=9.4E-10 K2=-9.0E-1 K3=0.0 #
 CH3O1+ + CH4 -> CH3OCH41+ 							RTYPE=4 K1=1.0E-17 K2=0.0 K3=0.0 #
 CH51+ + CO -> CH3CH2O1+ 							RTYPE=4 K1=1.0E-17 K2=0.0 K3=0.0 #
 H1+ + H -> H21+ 									RTYPE=4 K1=2.0E-20 K2=1.0 K3=0.0 #
 HCO1+ + CH4 -> CH3CH2O1+ 							RTYPE=4 K1=1.0E-17 K2=0.0 K3=0.0 #
 HCO1+ + H2O -> HCOOH21+ 							RTYPE=4 K1=4.0E-13 K2=-1.3 K3=0.0 #
 NO1+ + H2 -> H2NO1+ 								RTYPE=4 K1=6.7E-20 K2=-1.0 K3=0.0 #
 C1- + H -> CH + E1- 								RTYPE=5 K1=5.0E-10 K2=0.0 K3=0.0 #
 C1- + H2 -> CH2 + E1- 								RTYPE=5 K1=1.0E-13 K2=0.0 K3=0.0 #
 C1- + H2O -> H2CO + E1- 							RTYPE=5 K1=5.0E-10 K2=0.0 K3=0.0 #
 C1- + N -> CN + E1- 								RTYPE=5 K1=5.0E-10 K2=0.0 K3=0.0 #
 C1- + NH -> HCN + E1- 								RTYPE=5 K1=5.0E-10 K2=0.0 K3=0.0 #
 C1- + O -> CO + E1- 								RTYPE=5 K1=5.0E-10 K2=0.0 K3=0.0 #
 C1- + O2 -> CO2 + E1- 								RTYPE=5 K1=5.0E-11 K2=0.0 K3=0.0 #
 C1- + OH -> HCO + E1- 								RTYPE=5 K1=5.0E-10 K2=0.0 K3=0.0 #
 CN1- + CH3 -> CH3CN + E1- 							RTYPE=5 K1=1.0E-9 K2=0.0 K3=0.0 #
 CN1- + H -> HCN + E1- 								RTYPE=5 K1=1.3E-9 K2=0.0 K3=0.0 #
 H1- + C -> CH + E1- 								RTYPE=5 K1=1.0E-9 K2=0.0 K3=0.0 #
 H1- + CH -> CH2 + E1- 								RTYPE=5 K1=1.0E-10 K2=0.0 K3=0.0 #
 H1- + CH2 -> CH3 + E1- 							RTYPE=5 K1=1.0E-9 K2=0.0 K3=0.0 #
 H1- + CH3 -> CH4 + E1- 							RTYPE=5 K1=1.0E-9 K2=0.0 K3=0.0 #
 H1- + CN -> HCN + E1- 								RTYPE=5 K1=1.0E-10 K2=0.0 K3=0.0 #
 H1- + CO -> HCO + E1- 								RTYPE=5 K1=5.0E-11 K2=0.0 K3=0.0 #
 H1- + H -> H2 + E1- 								RTYPE=5 K1=1.3E-9 K2=0.0 K3=0.0 #
 H1- + HCO -> H2CO + E1- 							RTYPE=5 K1=1.0E-9 K2=0.0 K3=0.0 #
 H1- + N -> NH + E1- 								RTYPE=5 K1=1.0E-9 K2=0.0 K3=0.0 #
 H1- + NH -> NH2 + E1- 								RTYPE=5 K1=1.0E-10 K2=0.0 K3=0.0 #
 H1- + NH2 -> NH3 + E1- 							RTYPE=5 K1=1.0E-9 K2=0.0 K3=0.0 #
 H1- + O -> OH + E1- 								RTYPE=5 K1=1.0E-9 K2=0.0 K3=0.0 #
 H1- + OH -> H2O + E1- 								RTYPE=5 K1=1.0E-10 K2=0.0 K3=0.0 #
 O1- + C -> CO + E1- 								RTYPE=5 K1=5.0E-10 K2=0.0 K3=0.0 #
 O1- + CH -> HCO + E1- 								RTYPE=5 K1=5.0E-10 K2=0.0 K3=0.0 #
 O1- + CH2 -> H2CO + E1- 							RTYPE=5 K1=5.0E-10 K2=0.0 K3=0.0 #
 O1- + CO -> CO2 + E1- 								RTYPE=5 K1=6.5E-10 K2=0.0 K3=0.0 #
 O1- + H -> OH + E1- 								RTYPE=5 K1=5.0E-10 K2=0.0 K3=0.0 #
 O1- + H2 -> H2O + E1- 								RTYPE=5 K1=7.0E-10 K2=0.0 K3=0.0 #
 O1- + N -> NO + E1- 								RTYPE=5 K1=2.2E-10 K2=0.0 K3=0.0 #
 O1- + O -> O2 + E1- 								RTYPE=5 K1=1.9E-10 K2=0.0 K3=0.0 #
 OH1- + C -> HCO + E1- 								RTYPE=5 K1=5.0E-10 K2=0.0 K3=0.0 #
 OH1- + CH -> H2CO + E1- 							RTYPE=5 K1=5.0E-10 K2=0.0 K3=0.0 #
 OH1- + CH3 -> CH3OH + E1- 							RTYPE=5 K1=1.0E-9 K2=0.0 K3=0.0 #
 OH1- + H -> H2O + E1- 								RTYPE=5 K1=1.4E-9 K2=0.0 K3=0.0 #
 O + CH -> HCO1+ + E1- 								RTYPE=6 K1=2.0E-11 K2=4.4E-1 K3=0.0 #
 C + CH2 -> CH + CH 								RTYPE=7 K1=2.69E-12 K2=0.0 K3=2.36E+4 #
 C + HCO -> CH + CO 								RTYPE=7 K1=1.0E-10 K2=0.0 K3=0.0 #
 C + N2 -> CN + N 									RTYPE=7 K1=8.7E-11 K2=0.0 K3=2.26E+4 #
 C + NH -> N + CH 									RTYPE=7 K1=1.73E-11 K2=5.0E-1 K3=4.0E+3 #
 C + NH -> CN + H 									RTYPE=7 K1=1.2E-10 K2=0.0 K3=0.0 #
 C + NH2 -> HNC + H 								RTYPE=7 K1=3.4E-11 K2=-3.6E-1 K3=0.0 #
 C + NH2 -> CH + NH 								RTYPE=7 K1=9.61E-13 K2=0.0 K3=1.05E+4 #
 C + NH2 -> HCN + H 								RTYPE=7 K1=3.4E-11 K2=-3.6E-1 K3=0.0 #
 C + NO -> CO + N 									RTYPE=7 K1=9.0E-11 K2=-1.6E-1 K3=0.0 #
 C + NO -> CN + O 									RTYPE=7 K1=6.0E-11 K2=-1.6E-1 K3=0.0 #
 C + O2 -> CO + O 									RTYPE=7 K1=4.7E-11 K2=-3.4E-1 K3=0.0 #
 C + OCN -> CO + CN 								RTYPE=7 K1=1.0E-10 K2=0.0 K3=0.0 #
 C + OH -> CO + H 									RTYPE=7 K1=1.0E-10 K2=0.0 K3=0.0 #
 C + OH -> O + CH 									RTYPE=7 K1=2.25E-11 K2=5.0E-1 K3=1.48E+4 #
 CH + CH4 -> CH3 + CH2 								RTYPE=7 K1=2.28E-11 K2=7.0E-1 K3=3.0E+3 #
 CH + CO2 -> HCO + CO 								RTYPE=7 K1=2.94E-13 K2=5.0E-1 K3=3.0E+3 #
 CH + H2CO -> HCO + CH2 							RTYPE=7 K1=9.21E-12 K2=7.0E-1 K3=2.0E+3 #
 CH + HCO -> CO + CH2 								RTYPE=7 K1=2.87E-12 K2=7.0E-1 K3=5.0E+2 #
 CH + HNO -> NO + CH2 								RTYPE=7 K1=1.73E-11 K2=0.0 K3=0.0 #
 CH + N -> NH + C 									RTYPE=7 K1=3.03E-11 K2=6.5E-1 K3=1.21E+3 #
 CH + N2 -> HCN + N 								RTYPE=7 K1=5.6E-13 K2=8.8E-1 K3=1.01E+4 #
 CH + NO -> HCO + N 								RTYPE=7 K1=2.7E-11 K2=0.0 K3=5.0E+3 #
 CH + NO -> OCN + H 								RTYPE=7 K1=1.13E-25 K2=0.0 K3=0.0 #
 CH + NO -> CN + OH 								RTYPE=7 K1=2.32E-26 K2=0.0 K3=0.0 #
 CH + NO -> HCN + O 								RTYPE=7 K1=1.2E-11 K2=-1.3E-1 K3=0.0 #
 CH + O -> OH + C 									RTYPE=7 K1=2.52E-11 K2=0.0 K3=2.38E+3 #
 CH + O2 -> CO + OH 								RTYPE=7 K1=3.8E-11 K2=-4.8E-1 K3=0.0 #
 CH + O2 -> HCO + O 								RTYPE=7 K1=1.44E-11 K2=7.0E-1 K3=3.0E+3 #
 CH + HO2 -> HCO + OH 								RTYPE=7 K1=1.44E-11 K2=5.0E-1 K3=3.0E+3 #
 CH + HO2 -> O2 + CH2 								RTYPE=7 K1=2.94E-13 K2=5.0E-1 K3=7.55E+3 #
 CH + OH -> HCO + H 								RTYPE=7 K1=1.44E-11 K2=5.0E-1 K3=5.0E+3 #
 CH2 + CH2 -> CH3 + CH 								RTYPE=7 K1=4.0E-10 K2=0.0 K3=5.0E+3 #
 CH2 + CH4 -> CH3 + CH3 							RTYPE=7 K1=7.13E-12 K2=0.0 K3=5.05E+3 #
 CH2 + CN -> HCN + CH 								RTYPE=7 K1=5.3E-12 K2=0.0 K3=2.5E+3 #
 CH2 + H2CO -> HCO + CH3 							RTYPE=7 K1=3.3E-13 K2=0.0 K3=3.27E+3 #
 CH2 + HCO -> CO + CH3 								RTYPE=7 K1=3.0E-11 K2=0.0 K3=0.0 #
 CH2 + HNO -> NO + CH3 								RTYPE=7 K1=1.7E-11 K2=0.0 K3=0.0 #
 CH2 + NO -> H2CO + N 								RTYPE=7 K1=2.7E-12 K2=0.0 K3=3.5E+3 #
 CH2 + NO -> HCN + OH 								RTYPE=7 K1=8.32E-13 K2=0.0 K3=1.44E+3 #
 CH2 + O -> HCO + H 								RTYPE=7 K1=5.01E-11 K2=0.0 K3=0.0 #
 CH2 + O -> OH + CH 								RTYPE=7 K1=4.98E-10 K2=0.0 K3=6.0E+3 #
 CH2 + O2 -> CO2 + H + H 							RTYPE=7 K1=3.65E-11 K2=-3.3 K3=1.44E+3 #
 CH2 + O2 -> CO2 + H2 								RTYPE=7 K1=2.92E-11 K2=-3.3 K3=1.44E+3 #
 CH2 + O2 -> HCO + OH 								RTYPE=7 K1=4.1E-11 K2=0.0 K3=7.5E+2 #
 CH2 + O2 -> H2CO + O 								RTYPE=7 K1=3.65E-11 K2=-3.3 K3=1.44E+3 #
 CH2 + O2 -> CO + H2O 								RTYPE=7 K1=2.48E-10 K2=-3.3 K3=1.44E+3 #
 CH2 + OH -> H2O + CH 								RTYPE=7 K1=1.44E-11 K2=5.0E-1 K3=3.0E+3 #
 CH2 + OH -> O + CH3 								RTYPE=7 K1=1.44E-11 K2=5.0E-1 K3=3.0E+3 #
 CH3 + CH3 -> CH4 + CH2 							RTYPE=7 K1=7.13E-12 K2=0.0 K3=5.05E+3 #
 CH3 + CH3 -> CH3CH2 + H 							RTYPE=7 K1=1.46E-11 K2=1.0E-1 K3=5.34E+3 #
 CH3 + CN -> HCN + CH2 								RTYPE=7 K1=9.21E-12 K2=7.0E-1 K3=1.5E+3 #
 CH3 + H2CO -> HCO + CH4 							RTYPE=7 K1=1.34E-15 K2=5.05 K3=1.64E+3 #
 CH3 + H2O -> OH + CH4 								RTYPE=7 K1=2.3E-15 K2=3.47 K3=6.68E+3 #
 CH3 + NH3 -> CH4 + NH2 							RTYPE=7 K1=9.55E-14 K2=0.0 K3=4.89E+3 #
 CH3 + O2 -> H2CO + OH 								RTYPE=7 K1=5.64E-13 K2=0.0 K3=4.5E+3 #
 CH3 + O2 -> HO2 + CH2 								RTYPE=7 K1=5.3E-12 K2=0.0 K3=3.5E+4 #
 CH3 + O2 -> HCO + H2O 								RTYPE=7 K1=1.66E-12 K2=0.0 K3=0.0 #
 CH3 + HO2 -> O2 + CH4 								RTYPE=7 K1=6.0E-12 K2=0.0 K3=0.0 #
 CH3 + OH -> CH4 + O 								RTYPE=7 K1=3.27E-14 K2=2.2 K3=2.24E+3 #
 CH3 + OH -> H2O + CH2 								RTYPE=7 K1=1.2E-10 K2=0.0 K3=1.4E+3 #
 CH3 + OH -> H2CO + H2 								RTYPE=7 K1=1.7E-12 K2=0.0 K3=0.0 #
 CH3 + OH -> HCOH + H2 								RTYPE=7 K1=0.0 K2=0.0 K3=1.5E+3 #
 CH4 + CN -> HCN + CH3 								RTYPE=7 K1=3.14E-12 K2=1.53 K3=5.04E+2 #
 CH4 + O2 -> HO2 + CH3 								RTYPE=7 K1=6.7E-11 K2=0.0 K3=2.86E+4 #
 CH4 + OH -> H2O + CH3 								RTYPE=7 K1=3.77E-13 K2=2.42 K3=1.16E+3 #
 CN + CH3CH3 -> CH3CH2 + HCN 						RTYPE=7 K1=2.4E-11 K2=0.0 K3=0.0 #
 CN + H2CO -> HCO + HCN 							RTYPE=7 K1=2.6E-10 K2=-4.7E-1 K3=8.26E+2 #
 CN + HCO -> HCN + CO 								RTYPE=7 K1=1.0E-10 K2=0.0 K3=0.0 #
 CN + HNO -> NO + HCN 								RTYPE=7 K1=3.0E-11 K2=0.0 K3=0.0 #
 CN + NH3 -> NH2 + HCN 								RTYPE=7 K1=1.38E-11 K2=-1.14 K3=0.0 #
 CN + NH3 -> NH2CN + H 								RTYPE=7 K1=1.3E-11 K2=-1.11 K3=0.0 #
 CN + NO -> N2 + CO 								RTYPE=7 K1=1.6E-13 K2=0.0 K3=0.0 #
 CN + NO -> OCN + N 								RTYPE=7 K1=1.62E-10 K2=0.0 K3=2.12E+4 #
 CN + O2 -> O + OCN 								RTYPE=7 K1=2.4E-11 K2=-6.0E-1 K3=0.0 #
 CN + O2 -> CO + NO 								RTYPE=7 K1=5.3E-13 K2=0.0 K3=0.0 #
 CN + OH -> HCN + O 								RTYPE=7 K1=1.0E-11 K2=0.0 K3=1.03 #
 CN + OH -> OCN + H 								RTYPE=7 K1=7.01E-11 K2=0.0 K3=0.0 #
 CO + HNO -> NH + CO2 								RTYPE=7 K1=3.32E-12 K2=0.0 K3=6.17E+3 #
 CO + N2O -> CO2 + N2 								RTYPE=7 K1=1.62E-13 K2=0.0 K3=8.78E+3 #
 CO + NO2 -> CO2 + NO 								RTYPE=7 K1=1.48E-10 K2=0.0 K3=1.7E+4 #
 CO + O2 -> CO2 + O 								RTYPE=7 K1=5.99E-12 K2=0.0 K3=2.41E+4 #
 CO + HO2 -> CO2 + OH 								RTYPE=7 K1=5.6E-10 K2=0.0 K3=1.22E+4 #
 CO + OH -> CO2 + H 								RTYPE=7 K1=2.81E-13 K2=0.0 K3=1.76E+2 #
 H + CH -> C + H2 									RTYPE=7 K1=2.7E-11 K2=3.8E-1 K3=0.0 #
 H + CH2 -> CH + H2 								RTYPE=7 K1=2.7E-10 K2=0.0 K3=0.0 #
 H + CH3 -> CH2 + H2 								RTYPE=7 K1=1.0E-10 K2=0.0 K3=7.6E+3 #
 H + CH4 -> CH3 + H2 								RTYPE=7 K1=7.34E-12 K2=0.0 K3=4.41E+3 #
 H + CO -> OH + C 									RTYPE=7 K1=1.1E-10 K2=5.0E-1 K3=7.77E+4 #
 H + CO2 -> CO + OH 								RTYPE=7 K1=2.51E-10 K2=0.0 K3=1.33E+4 #
 H + H2CN -> H2 + HCN 								RTYPE=7 K1=1.0E-10 K2=0.0 K3=0.0 #
 H + H2CO -> HCO + H2 								RTYPE=7 K1=2.14E-12 K2=1.62 K3=1.09E+3 #
 H + H2O -> OH + H2 								RTYPE=7 K1=6.82E-12 K2=1.6 K3=9.72E+3 #
 H + H2O2 -> HO2 + H2 								RTYPE=7 K1=2.81E-12 K2=0.0 K3=1.89E+3 #
 H + H2O2 -> H2O + OH 								RTYPE=7 K1=1.69E-11 K2=0.0 K3=1.8E+3 #
 H + HCN -> CN + H2 								RTYPE=7 K1=6.19E-10 K2=0.0 K3=1.25E+4 #
 H + HCO -> H2 + CO 								RTYPE=7 K1=1.5E-10 K2=0.0 K3=0.0 #
 H + HCO -> O + CH2 								RTYPE=7 K1=6.61E-11 K2=0.0 K3=5.16E+4 #
 H + HNO -> NO + H2 								RTYPE=7 K1=4.5E-11 K2=7.2E-1 K3=3.29E+2 #
 H + HNO -> NH2 + O 								RTYPE=7 K1=1.05E-9 K2=-3.0E-1 K3=1.47E+4 #
 H + HNO -> OH + NH 								RTYPE=7 K1=2.41E-9 K2=-5.0E-1 K3=9.01E+3 #
 H + N2O -> NO + NH 								RTYPE=7 K1=2.94E-12 K2=5.0E-1 K3=1.51E+4 #
 H + N2O -> OH + N2 								RTYPE=7 K1=9.22E-14 K2=0.0 K3=2.99E+3 #
 H + NH -> N + H2 									RTYPE=7 K1=1.73E-11 K2=5.0E-1 K3=2.4E+3 #
 H + NH2 -> NH + H2 								RTYPE=7 K1=5.25E-12 K2=7.9E-1 K3=2.2E+3 #
 H + NH3 -> NH2 + H2 								RTYPE=7 K1=6.54E-13 K2=2.76 K3=5.17E+3 #
 H + NO -> O + NH 									RTYPE=7 K1=9.3E-10 K2=-1.0E-1 K3=3.52E+4 #
 H + NO -> N + OH 									RTYPE=7 K1=3.6E-10 K2=0.0 K3=2.49E+4 #
 H + NO2 -> NO + OH 								RTYPE=7 K1=4.0E-10 K2=0.0 K3=3.4E+2 #
 H + O2 -> OH + O 									RTYPE=7 K1=2.94E-10 K2=0.0 K3=8.38E+3 #
 H + HO2 -> OH + OH 								RTYPE=7 K1=7.21E-11 K2=0.0 K3=0.0 #
 H + HO2 -> H2O + O 								RTYPE=7 K1=2.42E-12 K2=0.0 K3=0.0 #
 H + HO2 -> O2 + H2 								RTYPE=7 K1=5.6E-12 K2=0.0 K3=0.0 #
 H + OH -> O + H2 									RTYPE=7 K1=6.86E-14 K2=2.8 K3=1.95E+3 #
 H2 + C -> CH + H 									RTYPE=7 K1=6.64E-10 K2=0.0 K3=1.17E+4 #
 H2 + CH -> CH2 + H 								RTYPE=7 K1=3.75E-10 K2=0.0 K3=1.66E+3 #
 H2 + CH2 -> CH3 + H 								RTYPE=7 K1=5.0E-11 K2=0.0 K3=4.87E+3 #
 H2 + CH3 -> CH4 + H 								RTYPE=7 K1=2.51E-13 K2=0.0 K3=4.21E+3 #
 H2 + CN -> HCN + H 								RTYPE=7 K1=4.04E-13 K2=2.87 K3=8.2E+2 #
 H2 + CO2 -> CO + H2O 								RTYPE=7 K1=2.94E-14 K2=5.0E-1 K3=7.55E+3 #
 H2 + N -> NH + H 									RTYPE=7 K1=4.65E-10 K2=0.0 K3=1.66E+4 #
 H2 + NH -> NH2 + H 								RTYPE=7 K1=5.96E-11 K2=0.0 K3=7.78E+3 #
 H2 + NH2 -> NH3 + H 								RTYPE=7 K1=1.76E-13 K2=2.23 K3=3.61E+3 #
 H2 + O -> OH + H 									RTYPE=7 K1=3.44E-13 K2=2.67 K3=3.16E+3 #
 H2 + O2 -> OH + OH 								RTYPE=7 K1=3.16E-10 K2=0.0 K3=2.19E+4 #
 H2 + O2 -> HO2 + H 								RTYPE=7 K1=2.4E-10 K2=0.0 K3=2.85E+4 #
 H2 + HO2 -> H2O2 + H 								RTYPE=7 K1=4.38E-12 K2=0.0 K3=1.08E+4 #
 H2 + OH -> H2O + H 								RTYPE=7 K1=8.4E-13 K2=0.0 K3=1.04E+3 #
 H2CO + HO2 -> H2O2 + HCO 							RTYPE=7 K1=3.3E-12 K2=0.0 K3=5.87E+3 #
 H2O + HO2 -> H2O2 + OH 							RTYPE=7 K1=4.65E-11 K2=0.0 K3=1.65E+4 #
 HCO + CH3 -> CH4 + CO 								RTYPE=7 K1=4.4E-11 K2=0.0 K3=0.0 #
 HCO + HCO -> CO + CO + H2 							RTYPE=7 K1=3.63E-11 K2=0.0 K3=0.0 #
 HCO + HCO -> H2CO + CO 							RTYPE=7 K1=5.0E-11 K2=0.0 K3=0.0 #
 HCO + HNO -> H2CO + NO 							RTYPE=7 K1=1.0E-12 K2=0.0 K3=9.76E+2 #
 HCO + NO -> HNO + CO 								RTYPE=7 K1=1.2E-11 K2=0.0 K3=0.0 #
 HCO + O2 -> HO2 + CO 								RTYPE=7 K1=5.58E-12 K2=0.0 K3=0.0 #
 HCO + O2 -> CO2 + OH 								RTYPE=7 K1=7.6E-13 K2=0.0 K3=0.0 #
 HCO + HO2 -> O2 + H2CO 							RTYPE=7 K1=5.0E-11 K2=0.0 K3=0.0 #
 HNO + CH3 -> CH4 + NO 								RTYPE=7 K1=3.32E-12 K2=0.0 K3=0.0 #
 N + CH3CH2 -> H2CN + CH3 							RTYPE=7 K1=3.85E-11 K2=0.0 K3=0.0 #
 N + CH -> CN + H 									RTYPE=7 K1=1.66E-10 K2=-9.0E-2 K3=0.0 #
 N + CH2 -> HNC + H 								RTYPE=7 K1=3.95E-11 K2=1.67E-1 K3=0.0 #
 N + CH2 -> NH + CH 								RTYPE=7 K1=9.96E-13 K2=0.0 K3=2.04E+4 #
 N + CH2 -> HCN + H 								RTYPE=7 K1=3.95E-11 K2=1.67E-1 K3=0.0 #
 N + CH3 -> H2CN + H 								RTYPE=7 K1=8.6E-11 K2=0.0 K3=0.0 #
 N + CH3 -> HCN + H2 								RTYPE=7 K1=1.3E-11 K2=5.0E-1 K3=0.0 #
 N + CH3 -> HCN + H + H 							RTYPE=7 K1=3.32E-13 K2=0.0 K3=0.0 #
 N + CN -> C + N2 									RTYPE=7 K1=3.0E-10 K2=0.0 K3=0.0 #
 N + CO2 -> NO + CO 								RTYPE=7 K1=3.2E-13 K2=0.0 K3=1.71E+3 #
 N + H2CN -> NH + HCN 								RTYPE=7 K1=3.7E-11 K2=0.0 K3=0.0 #
 N + HCO -> OCN + H 								RTYPE=7 K1=1.0E-10 K2=0.0 K3=0.0 #
 N + HCO -> CO + NH 								RTYPE=7 K1=5.71E-12 K2=5.0E-1 K3=1.0E+3 #
 N + HCO -> HCN + O 								RTYPE=7 K1=1.7E-10 K2=0.0 K3=0.0 #
 N + HNO -> NO + NH 								RTYPE=7 K1=2.94E-12 K2=5.0E-1 K3=1.0E+3 #
 N + HNO -> N2O + H 								RTYPE=7 K1=1.43E-12 K2=5.0E-1 K3=1.5E+3 #
 N + NH -> N2 + H 									RTYPE=7 K1=5.0E-11 K2=0.0 K3=0.0 #
 N + NO -> N2 + O 									RTYPE=7 K1=3.0E-11 K2=-6.0E-1 K3=0.0 #
 N + NO2 -> N2 + O2 								RTYPE=7 K1=1.0E-12 K2=0.0 K3=0.0 #
 N + NO2 -> NO + NO 								RTYPE=7 K1=1.0E-12 K2=0.0 K3=0.0 #
 N + NO2 -> N2O + O 								RTYPE=7 K1=2.1E-11 K2=0.0 K3=0.0 #
 N + NO2 -> N2 + O + O 								RTYPE=7 K1=2.41E-12 K2=0.0 K3=0.0 #
 N + O2 -> NO + O 									RTYPE=7 K1=1.5E-11 K2=0.0 K3=3.68E+3 #
 N + HO2 -> NH + O2 								RTYPE=7 K1=1.7E-13 K2=0.0 K3=0.0 #
 N + OH -> NO + H 									RTYPE=7 K1=7.5E-11 K2=-1.8E-1 K3=0.0 #
 N + OH -> O + NH 									RTYPE=7 K1=1.88E-11 K2=1.0E-1 K3=1.07E+4 #
 N + PN -> P + N2 									RTYPE=7 K1=1.0E-18 K2=0.0 K3=0.0 #
 N + PO -> PN + O 									RTYPE=7 K1=3.0E-11 K2=-6.0E-1 K3=0.0 #
 N + PO -> P + NO 									RTYPE=7 K1=2.55E-12 K2=0.0 K3=0.0 #
 N2 + O2 -> N2O + O 								RTYPE=7 K1=1.0E-10 K2=0.0 K3=5.52E+4 #
 NH + CN -> HCN + N 								RTYPE=7 K1=2.94E-12 K2=5.0E-1 K3=1.0E+3 #
 NH + H2O -> OH + NH2 								RTYPE=7 K1=1.83E-12 K2=1.6 K3=1.41E+4 #
 NH + NH -> NH2 + N 								RTYPE=7 K1=4.35E-13 K2=0.0 K3=0.0 #
 NH + NH -> N2 + H + H 								RTYPE=7 K1=1.0E-10 K2=0.0 K3=0.0 #
 NH + NH -> N2 + H2 								RTYPE=7 K1=1.7E-11 K2=0.0 K3=0.0 #
 NH + NO -> N2O + H 								RTYPE=7 K1=3.12E-11 K2=0.0 K3=0.0 #
 NH + NO -> N2 + OH 								RTYPE=7 K1=1.46E-11 K2=-5.8E-1 K3=3.7E+1 #
 NH + NO -> O + N2 + H 								RTYPE=7 K1=4.7E-11 K2=5.0E-1 K3=0.0 #
 NH + NO2 -> N2O + OH 								RTYPE=7 K1=4.3E-11 K2=0.0 K3=0.0 #
 NH + NO2 -> HNO + NO 								RTYPE=7 K1=5.72E-12 K2=5.0E-1 K3=2.5E+3 #
 NH + OH -> H2O + N 								RTYPE=7 K1=3.11E-12 K2=1.2 K3=0.0 #
 NH + OH -> HNO + H 								RTYPE=7 K1=3.32E-11 K2=0.0 K3=0.0 #
 NH + OH -> NH2 + O 								RTYPE=7 K1=2.93E-12 K2=1.0E-1 K3=5.8E+3 #
 NH2 + H2CO -> NH2CHO + H 							RTYPE=7 K1=1.0E-10 K2=0.0 K3=0.0 #
 NH2 + NO -> N2 + OH + H 							RTYPE=7 K1=1.49E-12 K2=0.0 K3=0.0 #
 NO + HNO -> N2O + OH 								RTYPE=7 K1=1.41E-11 K2=0.0 K3=1.49E+4 #
 NO + N2O -> NO2 + N2 								RTYPE=7 K1=2.92E-13 K2=2.23 K3=2.33E+4 #
 NO + NH2 -> N2 + H2O 								RTYPE=7 K1=1.7E-11 K2=0.0 K3=0.0 #
 NO + NO -> N2O + O 								RTYPE=7 K1=7.22E-12 K2=0.0 K3=3.32E+4 #
 NO + NO -> O2 + N2 								RTYPE=7 K1=2.51E-11 K2=0.0 K3=3.07E+4 #
 NO + O2 -> NO2 + O 								RTYPE=7 K1=2.8E-12 K2=0.0 K3=2.34E+4 #
 O + CH3CH2 -> CH3CHO + H 							RTYPE=7 K1=1.33E-10 K2=0.0 K3=0.0 #
 O + CH3CH2 -> H2CO + CH3 							RTYPE=7 K1=2.65E-11 K2=0.0 K3=0.0 #
 O + CH -> CO + H 									RTYPE=7 K1=6.6E-11 K2=0.0 K3=0.0 #
 O + CH2 -> CO + H + H 								RTYPE=7 K1=1.2E-10 K2=0.0 K3=0.0 #
 O + CH2 -> CO + H2 								RTYPE=7 K1=8.0E-11 K2=0.0 K3=0.0 #
 O + CH3 -> H2CO + H 								RTYPE=7 K1=1.4E-10 K2=0.0 K3=0.0 #
 O + CH4 -> OH + CH3 								RTYPE=7 K1=2.29E-12 K2=2.2 K3=3.82E+3 #
 O + CN -> CO + N 									RTYPE=7 K1=4.0E-11 K2=0.0 K3=0.0 #
 O + CN -> NO + C 									RTYPE=7 K1=3.81E-11 K2=5.0E-1 K3=1.45E+4 #
 O + CO2 -> O2 + CO 								RTYPE=7 K1=2.46E-11 K2=0.0 K3=2.66E+4 #
 O + H2CN -> OCN + H2 								RTYPE=7 K1=1.0E-10 K2=0.0 K3=0.0 #
 O + H2CO -> CO + OH + H 							RTYPE=7 K1=1.0E-10 K2=0.0 K3=0.0 #
 O + H2CO -> HCO + OH 								RTYPE=7 K1=1.07E-11 K2=1.17 K3=1.24E+3 #
 O + H2O -> HO2 + H 								RTYPE=7 K1=1.23E-53 K2=0.0 K3=0.0 #
 O + H2O -> OH + OH 								RTYPE=7 K1=1.85E-11 K2=9.5E-1 K3=8.57E+3 #
 O + H2O2 -> HO2 + OH 								RTYPE=7 K1=8.54E-14 K2=3.25 K3=1.2E+3 #
 O + HCN -> CO + NH 								RTYPE=7 K1=7.3E-13 K2=1.14 K3=3.74E+3 #
 O + HCN -> OCN + H 								RTYPE=7 K1=3.61E-13 K2=2.1 K3=3.08E+3 #
 O + HCN -> CN + OH 								RTYPE=7 K1=6.21E-10 K2=0.0 K3=1.24E+4 #
 O + HCO -> OH + CO 								RTYPE=7 K1=5.0E-11 K2=0.0 K3=0.0 #
 O + HCO -> H + CO2 								RTYPE=7 K1=5.0E-11 K2=0.0 K3=0.0 #
 O + HNO -> OH + NO 								RTYPE=7 K1=3.8E-11 K2=0.0 K3=0.0 #
 O + HNO -> NO2 + H 								RTYPE=7 K1=1.0E-12 K2=0.0 K3=0.0 #
 O + HNO -> O2 + NH 								RTYPE=7 K1=2.94E-12 K2=5.0E-1 K3=3.5E+3 #
 O + HPO -> PO + OH 								RTYPE=7 K1=3.8E-11 K2=0.0 K3=0.0 #
 O + N2 -> NO + N 									RTYPE=7 K1=2.51E-10 K2=0.0 K3=3.86E+4 #
 O + N2O -> NO + NO 								RTYPE=7 K1=1.15E-10 K2=0.0 K3=1.34E+4 #
 O + N2O -> O2 + N2 								RTYPE=7 K1=1.66E-10 K2=0.0 K3=1.41E+4 #
 O + NH -> NO + H 									RTYPE=7 K1=1.16E-10 K2=0.0 K3=0.0 #
 O + NH -> OH + N 									RTYPE=7 K1=1.16E-11 K2=0.0 K3=0.0 #
 O + NH2 -> NH + OH 								RTYPE=7 K1=2.0E-11 K2=0.0 K3=0.0 #
 O + NH2 -> NO + H2 								RTYPE=7 K1=1.0E-11 K2=0.0 K3=0.0 #
 O + NH2 -> HNO + H 								RTYPE=7 K1=8.0E-11 K2=0.0 K3=0.0 #
 O + NH3 -> OH + NH2 								RTYPE=7 K1=1.89E-11 K2=0.0 K3=4.0E+3 #
 O + NO -> O2 + N 									RTYPE=7 K1=1.18E-11 K2=0.0 K3=2.04E+4 #
 O + NO2 -> NO + O2 								RTYPE=7 K1=1.0E-11 K2=0.0 K3=0.0 #
 O + HO2 -> OH + O2 								RTYPE=7 K1=5.3E-11 K2=0.0 K3=0.0 #
 O + OCN -> NO + CO 								RTYPE=7 K1=1.5E-11 K2=0.0 K3=2.0E+2 #
 O + OCN -> CN + O2 								RTYPE=7 K1=4.05E-10 K2=-1.43 K3=3.5E+3 #
 O + OH -> O2 + H 									RTYPE=7 K1=7.5E-11 K2=-2.5E-1 K3=0.0 #
 O2 + OCN -> NO2 + CO 								RTYPE=7 K1=8.1E-11 K2=0.0 K3=7.73E+2 #
 O2 + OCN -> CO2 + NO 								RTYPE=7 K1=1.32E-12 K2=0.0 K3=0.0 #
 HO2 + HO2 -> H2O2 + O2 							RTYPE=7 K1=5.64E-12 K2=0.0 K3=0.0 #
 OH + CH3CH2 -> CH3CH3 + O 							RTYPE=7 K1=1.04E-18 K2=8.8 K3=2.5E+2 #
 OH + CH2 -> H2CO + H 								RTYPE=7 K1=3.0E-10 K2=0.0 K3=0.0 #
 OH + HCOH -> CO2 + H2 + H 							RTYPE=7 K1=0.0 K2=0.0 K3=0.0 #
 OH + H2CO -> HCO + H2O 							RTYPE=7 K1=1.0E-11 K2=0.0 K3=0.0 #
 OH + H2CO -> HCOOH + H 							RTYPE=7 K1=2.01E-13 K2=0.0 K3=0.0 #
 OH + H2O2 -> H2O + HO2 							RTYPE=7 K1=2.91E-12 K2=0.0 K3=1.6E+2 #
 OH + HCN -> CO + NH2 								RTYPE=7 K1=1.07E-13 K2=0.0 K3=5.89E+3 #
 OH + HCN -> CN + H2O 								RTYPE=7 K1=1.87E-13 K2=1.5 K3=3.89E+3 #
 OH + HCO -> H2O + CO 								RTYPE=7 K1=1.69E-10 K2=0.0 K3=0.0 #
 OH + HNO -> H2O + NO 								RTYPE=7 K1=8.0E-11 K2=0.0 K3=5.0E+2 #
 OH + N2O -> HO2 + N2 								RTYPE=7 K1=3.7E-13 K2=0.0 K3=2.74E+3 #
 OH + N2O -> HNO + NO 								RTYPE=7 K1=1.04E-17 K2=4.33 K3=1.26E+4 #
 OH + NH2 -> NH3 + O 								RTYPE=7 K1=9.18E-14 K2=0.0 K3=0.0 #
 OH + NH2 -> NH + H2O 								RTYPE=7 K1=1.5E-12 K2=0.0 K3=0.0 #
 OH + NH3 -> H2O + NH2 								RTYPE=7 K1=1.47E-13 K2=2.05 K3=7.0 #
 OH + NO -> NO2 + H 								RTYPE=7 K1=5.2E-12 K2=0.0 K3=1.51E+4 #
 OH + HO2 -> H2O + O2 								RTYPE=7 K1=1.2E-10 K2=0.0 K3=0.0 #
 OH + OH -> HO2 + H 								RTYPE=7 K1=1.82E-40 K2=0.0 K3=0.0 #
 OH + OH -> H2O + O 								RTYPE=7 K1=1.65E-12 K2=1.14 K3=5.0E+1 #
 P + O2 -> PO + O 									RTYPE=7 K1=1.0E-13 K2=0.0 K3=0.0 #
 C + H -> CH 										RTYPE=8 K1=1.0E-17 K2=0.0 K3=0.0 #
 C + H2 -> CH2 										RTYPE=8 K1=1.0E-17 K2=0.0 K3=0.0 #
 C + N -> CN 										RTYPE=8 K1=1.0E-17 K2=0.0 K3=0.0 #
 C + O -> CO 										RTYPE=8 K1=2.1E-19 K2=0.0 K3=0.0 #
 CH + H2 -> CH3 									RTYPE=8 K1=3.25E-17 K2=-6.0E-1 K3=0.0 #
 CN + CH3 -> CH3CN 									RTYPE=8 K1=1.0E-16 K2=0.0 K3=0.0 #
 H + O -> OH 										RTYPE=8 K1=9.9E-19 K2=-3.8E-1 K3=0.0 #
 H + OH -> H2O 										RTYPE=8 K1=4.0E-18 K2=-2.0 K3=0.0 #
 O + O -> O2 										RTYPE=8 K1=4.9E-20 K2=1.58 K3=0.0 #
 OH + OH -> H2O2 									RTYPE=8 K1=1.0E-18 K2=-2.0 K3=0.0 #
 CH3CH31+ + E1- -> CH3CH2 + H 						RTYPE=9 K1=1.5E-7 K2=-5.0E-1 K3=0.0 #
 NCCN1+ + E1- -> CN + CN 							RTYPE=9 K1=1.5E-7 K2=-5.0E-1 K3=0.0 #
 CH1+ + E1- -> C + H 								RTYPE=9 K1=7.0E-8 K2=-5.0E-1 K3=0.0 #
 CH21+ + E1- -> C + H + H 							RTYPE=9 K1=4.0E-7 K2=-6.0E-1 K3=0.0 #
 CH21+ + E1- -> C + H2 								RTYPE=9 K1=7.7E-8 K2=-6.0E-1 K3=0.0 #
 CH21+ + E1- -> CH + H 								RTYPE=9 K1=1.6E-7 K2=-6.0E-1 K3=0.0 #
 CH2CO1+ + E1- -> CH2 + CO 							RTYPE=9 K1=2.0E-7 K2=-5.0E-1 K3=0.0 #
 CH2OH1+ + E1- -> CH2 + OH 							RTYPE=9 K1=3.0E-7 K2=-5.0E-1 K3=0.0 #
 CH2OH1+ + E1- -> H2CO + H 							RTYPE=9 K1=3.0E-7 K2=-5.0E-1 K3=0.0 #
 CH31+ + E1- -> CH2 + H 							RTYPE=9 K1=4.0E-7 K2=-3.0E-1 K3=0.0 #
 CH31+ + E1- -> H2 + C + H 							RTYPE=9 K1=3.0E-7 K2=-3.0E-1 K3=0.0 #
 CH31+ + E1- -> CH + H + H 							RTYPE=9 K1=1.6E-7 K2=-3.0E-1 K3=0.0 #
 CH31+ + E1- -> CH + H2 							RTYPE=9 K1=1.4E-7 K2=-3.0E-1 K3=0.0 #
 CH3CH2O1+ + E1- -> CH3 + HCO + H 					RTYPE=9 K1=1.35E-7 K2=-5.0E-1 K3=0.0 #
 CH3CH2O1+ + E1- -> CH2 + H2CO + H 					RTYPE=9 K1=1.35E-7 K2=-5.0E-1 K3=0.0 #
 CH3CH2O1+ + E1- -> CH3 + H2CO 						RTYPE=9 K1=1.5E-8 K2=-5.0E-1 K3=0.0 #
 CH3CH2O1+ + E1- -> CH3CHO + H 						RTYPE=9 K1=1.5E-8 K2=-5.0E-1 K3=0.0 #
 CH3CHO1+ + E1- -> CH3 + HCO 						RTYPE=9 K1=3.0E-7 K2=-5.0E-1 K3=0.0 #
 CH3CN1+ + E1- -> CH2 + HCN 						RTYPE=9 K1=3.0E-7 K2=-5.0E-1 K3=0.0 #
 CH3CN1+ + E1- -> CH3 + CN 							RTYPE=9 K1=2.0E-7 K2=-5.0E-1 K3=0.0 #
 CH3CNH1+ + E1- -> CH3CN + H 						RTYPE=9 K1=1.5E-7 K2=-5.0E-1 K3=0.0 #
 CH3CO1+ + E1- -> CH3 + CO 							RTYPE=9 K1=3.0E-7 K2=-5.0E-1 K3=0.0 #
 CH3NH1+ + E1- -> CH2 + NH2 						RTYPE=9 K1=1.5E-7 K2=-5.0E-1 K3=0.0 #
 CH3NH1+ + E1- -> HCN + H + H2 						RTYPE=9 K1=3.0E-7 K2=-5.0E-1 K3=0.0 #
 CH3NH1+ + E1- -> CH2NH + H 						RTYPE=9 K1=1.5E-7 K2=-5.0E-1 K3=0.0 #
 CH3NH1+ + E1- -> CN + H2 + H2 						RTYPE=9 K1=3.0E-8 K2=-5.0E-1 K3=0.0 #
 CH3NH21+ + E1- -> NH2 + CH3 						RTYPE=9 K1=3.0E-7 K2=-5.0E-1 K3=0.0 #
 CH3NH31+ + E1- -> CH3NH2 + H 						RTYPE=9 K1=1.5E-8 K2=-5.0E-1 K3=0.0 #
 CH3NH31+ + E1- -> CH3 + NH2 + H 					RTYPE=9 K1=1.43E-7 K2=-5.0E-1 K3=0.0 #
 CH3NH31+ + E1- -> CH3 + NH3 						RTYPE=9 K1=1.43E-7 K2=-5.0E-1 K3=0.0 #
 CH3O1+ + E1- -> HCO + H + H 						RTYPE=9 K1=2.0E-7 K2=-5.0E-1 K3=0.0 #
 CH3O1+ + E1- -> H2CO + H 							RTYPE=9 K1=2.0E-7 K2=-5.0E-1 K3=0.0 #
 CH3O1+ + E1- -> CO + H + H2 						RTYPE=9 K1=2.0E-7 K2=-5.0E-1 K3=0.0 #
 HCOOH21+ + E1- -> CO2 + H2 + H 					RTYPE=9 K1=2.85E-7 K2=-5.0E-1 K3=0.0 #
 HCOOH21+ + E1- -> HCOOH + H 						RTYPE=9 K1=1.5E-8 K2=-5.0E-1 K3=0.0 #
 CH3OCH31+ + E1- -> CH3O + CH3 						RTYPE=9 K1=6.0E-7 K2=-5.0E-1 K3=0.0 #
 CH3OCH41+ + E1- -> H2CO + CH4 + H 					RTYPE=9 K1=4.8E-7 K2=-5.0E-1 K3=0.0 #
 CH3OCH41+ + E1- -> CH3O + CH3 + H 					RTYPE=9 K1=6.0E-8 K2=-5.0E-1 K3=0.0 #
 CH3OCH41+ + E1- -> CH3O + CH4 						RTYPE=9 K1=3.0E-8 K2=-5.0E-1 K3=0.0 #
 CH3OCH41+ + E1- -> CH3OCH3 + H 					RTYPE=9 K1=3.0E-8 K2=-5.0E-1 K3=0.0 #
 CH3OH1+ + E1- -> CH3 + OH 							RTYPE=9 K1=6.0E-7 K2=-5.0E-1 K3=0.0 #
 CH3OH21+ + E1- -> CH3 + H2O 						RTYPE=9 K1=8.01E-8 K2=-5.9E-1 K3=0.0 #
 CH3OH21+ + E1- -> CH3 + OH + H 					RTYPE=9 K1=4.54E-7 K2=-5.9E-1 K3=0.0 #
 CH3OH21+ + E1- -> CH2 + H2O + H 					RTYPE=9 K1=1.87E-7 K2=-5.9E-1 K3=0.0 #
 CH3OH21+ + E1- -> CH3OH + H 						RTYPE=9 K1=2.67E-8 K2=-5.9E-1 K3=0.0 #
 CH3OH21+ + E1- -> CH3O + H2 						RTYPE=9 K1=5.34E-8 K2=-5.9E-1 K3=0.0 #
 CH3OH21+ + E1- -> H2CO + H2 + H 					RTYPE=9 K1=8.9E-8 K2=-5.9E-1 K3=0.0 #
 CH41+ + E1- -> CH2 + H + H 						RTYPE=9 K1=3.0E-7 K2=-5.0E-1 K3=0.0 #
 CH41+ + E1- -> CH3 + H 							RTYPE=9 K1=3.0E-7 K2=-5.0E-1 K3=0.0 #
 CH51+ + E1- -> CH + H2 + H2 						RTYPE=9 K1=3.0E-9 K2=-5.2E-1 K3=0.0 #
 CH51+ + E1- -> CH4 + H 							RTYPE=9 K1=1.4E-8 K2=-5.2E-1 K3=0.0 #
 CH51+ + E1- -> CH3 + H2 							RTYPE=9 K1=1.4E-8 K2=-5.2E-1 K3=0.0 #
 CH51+ + E1- -> CH3 + H + H 						RTYPE=9 K1=1.95E-7 K2=-5.2E-1 K3=0.0 #
 CH51+ + E1- -> CH2 + H2 + H 						RTYPE=9 K1=4.8E-8 K2=-5.2E-1 K3=0.0 #
 CN1+ + E1- -> C + N 								RTYPE=9 K1=3.38E-7 K2=-5.5E-1 K3=0.0 #
 CNC1+ + E1- -> CN + C 								RTYPE=9 K1=3.0E-7 K2=-5.0E-1 K3=0.0 #
 CO1+ + E1- -> O + C 								RTYPE=9 K1=2.75E-7 K2=-5.5E-1 K3=0.0 #
 CO21+ + E1- -> O + CO 								RTYPE=9 K1=4.2E-7 K2=-7.5E-1 K3=0.0 #
 COCHO1+ + E1- -> HCO + CO 							RTYPE=9 K1=3.0E-7 K2=-5.0E-1 K3=0.0 #
 COOH1+ + E1- -> CO + OH 							RTYPE=9 K1=3.0E-7 K2=-5.0E-1 K3=0.0 #
 H21+ + E1- -> H + H 								RTYPE=9 K1=2.53E-7 K2=-5.0E-1 K3=0.0 #
 H2CN1+ + E1- -> HNC + H 							RTYPE=9 K1=1.85E-7 K2=-6.5E-1 K3=0.0 #
 H2CN1+ + E1- -> CN + H + H 						RTYPE=9 K1=9.2E-8 K2=-6.5E-1 K3=0.0 #
 H2CN1+ + E1- -> HCN + H 							RTYPE=9 K1=1.85E-7 K2=-6.5E-1 K3=0.0 #
 H2CO1+ + E1- -> HCO + H 							RTYPE=9 K1=1.0E-7 K2=-5.0E-1 K3=0.0 #
 H2CO1+ + E1- -> CO + H + H 						RTYPE=9 K1=5.0E-7 K2=-5.0E-1 K3=0.0 #
 H2NC1+ + E1- -> HNC + H 							RTYPE=9 K1=1.8E-7 K2=-5.0E-1 K3=0.0 #
 H2NC1+ + E1- -> CN + H2 							RTYPE=9 K1=1.8E-8 K2=-5.0E-1 K3=0.0 #
 H2NO1+ + E1- -> HNO + H 							RTYPE=9 K1=1.5E-7 K2=-5.0E-1 K3=0.0 #
 H2NO1+ + E1- -> NO + H2 							RTYPE=9 K1=1.5E-7 K2=-5.0E-1 K3=0.0 #
 H2O1+ + E1- -> O + H + H 							RTYPE=9 K1=3.05E-7 K2=-5.0E-1 K3=0.0 #
 H2O1+ + E1- -> O + H2 								RTYPE=9 K1=3.9E-8 K2=-5.0E-1 K3=0.0 #
 H2O1+ + E1- -> OH + H 								RTYPE=9 K1=8.6E-8 K2=-5.0E-1 K3=0.0 #
 H2PO1+ + E1- -> HPO + H 							RTYPE=9 K1=1.5E-7 K2=-5.0E-1 K3=0.0 #
 H31+ + E1- -> H2 + H 								RTYPE=9 K1=2.59E-8 K2=-5.0E-1 K3=0.0 #
 H31+ + E1- -> H + H + H 							RTYPE=9 K1=4.61E-8 K2=-5.0E-1 K3=0.0 #
 H3O1+ + E1- -> H2O + H 							RTYPE=9 K1=1.1E-7 K2=-5.0E-1 K3=0.0 #
 H3O1+ + E1- -> OH + H2 							RTYPE=9 K1=6.0E-8 K2=-5.0E-1 K3=0.0 #
 H3O1+ + E1- -> H2 + H + O 							RTYPE=9 K1=5.6E-9 K2=-5.0E-1 K3=0.0 #
 H3O1+ + E1- -> OH + H + H 							RTYPE=9 K1=2.6E-7 K2=-5.0E-1 K3=0.0 #
 HCN1+ + E1- -> CN + H 								RTYPE=9 K1=2.0E-7 K2=-5.0E-1 K3=0.0 #
 HCO1+ + E1- -> CO + H 								RTYPE=9 K1=2.4E-7 K2=-6.9E-1 K3=0.0 #
 HOCO1+ + E1- -> OH + CO 							RTYPE=9 K1=3.2E-7 K2=-6.4E-1 K3=0.0 #
 HOCO1+ + E1- -> CO2 + H 							RTYPE=9 K1=6.0E-8 K2=-6.4E-1 K3=0.0 #
 HOCO1+ + E1- -> CO + H + O 						RTYPE=9 K1=8.1E-7 K2=-6.4E-1 K3=0.0 #
 HCOOH1+ + E1- -> HCO + OH 							RTYPE=9 K1=3.0E-7 K2=-5.0E-1 K3=0.0 #
 HCOOH1+ + E1- -> CO2 + H + H 						RTYPE=9 K1=3.0E-7 K2=-5.0E-1 K3=0.0 #
 CH3CONH1+ + E1- -> CH3 + CO + NH 					RTYPE=9 K1=6.75E-8 K2=-5.0E-1 K3=0.0 #
 CH3CONH1+ + E1- -> CH3 + OCN + H 					RTYPE=9 K1=6.75E-8 K2=-5.0E-1 K3=0.0 #
 CH3CONH1+ + E1- -> CH2 + HNCO + H 					RTYPE=9 K1=6.75E-8 K2=-5.0E-1 K3=0.0 #
 CH3CONH1+ + E1- -> CH2CO + NH + H 					RTYPE=9 K1=6.75E-8 K2=-5.0E-1 K3=0.0 #
 CH3CONH1+ + E1- -> CH3 + HNCO 						RTYPE=9 K1=1.5E-8 K2=-5.0E-1 K3=0.0 #
 CH3CONH1+ + E1- -> CH3CO + NH 						RTYPE=9 K1=1.5E-8 K2=-5.0E-1 K3=0.0 #
 CH3COOH1+ + E1- -> CH3 + CO + OH 					RTYPE=9 K1=6.75E-8 K2=-5.0E-1 K3=0.0 #
 CH3COOH1+ + E1- -> CH2 + COOH + H 					RTYPE=9 K1=6.75E-8 K2=-5.0E-1 K3=0.0 #
 CH3COOH1+ + E1- -> CH3 + CO2 + H 					RTYPE=9 K1=6.75E-8 K2=-5.0E-1 K3=0.0 #
 CH3COOH1+ + E1- -> CH2CO + OH + H 					RTYPE=9 K1=6.75E-8 K2=-5.0E-1 K3=0.0 #
 CH3COOH1+ + E1- -> CH3 + COOH 						RTYPE=9 K1=1.5E-8 K2=-5.0E-1 K3=0.0 #
 CH3COOH1+ + E1- -> CH3CO + OH 						RTYPE=9 K1=1.5E-8 K2=-5.0E-1 K3=0.0 #
 HNC1+ + E1- -> CN + H 								RTYPE=9 K1=2.0E-7 K2=-5.0E-1 K3=0.0 #
 HNCHO1+ + E1- -> NH + HCO 							RTYPE=9 K1=3.0E-7 K2=-5.0E-1 K3=0.0 #
 HNCO1+ + E1- -> CO + NH 							RTYPE=9 K1=3.0E-7 K2=-5.0E-1 K3=0.0 #
 HNCOCHO1+ + E1- -> NH + CO + HCO 					RTYPE=9 K1=9.0E-8 K2=-5.0E-1 K3=0.0 #
 HNCOCHO1+ + E1- -> HNCO + CO + H 					RTYPE=9 K1=9.0E-8 K2=-5.0E-1 K3=0.0 #
 HNCOCHO1+ + E1- -> OCN + HCO + H 					RTYPE=9 K1=9.0E-8 K2=-5.0E-1 K3=0.0 #
 HNCOCHO1+ + E1- -> HNCO + HCO 						RTYPE=9 K1=1.5E-8 K2=-5.0E-1 K3=0.0 #
 HNCOCHO1+ + E1- -> NH + COCHO 						RTYPE=9 K1=1.5E-8 K2=-5.0E-1 K3=0.0 #
 HNCOOH1+ + E1- -> NH + CO + OH 					RTYPE=9 K1=9.0E-8 K2=-5.0E-1 K3=0.0 #
 HNCOOH1+ + E1- -> OCN + OH + H 					RTYPE=9 K1=9.0E-8 K2=-5.0E-1 K3=0.0 #
 HNCOOH1+ + E1- -> NH + CO2 + H 					RTYPE=9 K1=9.0E-8 K2=-5.0E-1 K3=0.0 #
 HNCOOH1+ + E1- -> HNCO + OH 						RTYPE=9 K1=1.5E-8 K2=-5.0E-1 K3=0.0 #
 HNCOOH1+ + E1- -> NH + COOH 						RTYPE=9 K1=1.5E-8 K2=-5.0E-1 K3=0.0 #
 HNO1+ + E1- -> NO + H 								RTYPE=9 K1=3.0E-7 K2=-5.0E-1 K3=0.0 #
 HNOH1+ + E1- -> NH + OH 							RTYPE=9 K1=3.0E-7 K2=-5.0E-1 K3=0.0 #
 HOC1+ + E1- -> CO + H 								RTYPE=9 K1=2.0E-7 K2=-7.5E-1 K3=0.0 #
 HOCOOH1+ + E1- -> OH + CO + OH 					RTYPE=9 K1=1.35E-7 K2=-5.0E-1 K3=0.0 #
 HOCOOH1+ + E1- -> OH + CO2 + H 					RTYPE=9 K1=1.35E-7 K2=-5.0E-1 K3=0.0 #
 HOCOOH1+ + E1- -> OH + COOH 						RTYPE=9 K1=3.0E-8 K2=-5.0E-1 K3=0.0 #
 HOCOOH21+ + E1- -> OH + CO + H2O 					RTYPE=9 K1=1.35E-7 K2=-5.0E-1 K3=0.0 #
 HOCOOH21+ + E1- -> COOH + OH + H 					RTYPE=9 K1=1.35E-7 K2=-5.0E-1 K3=0.0 #
 HOCOOH21+ + E1- -> COOH + H2O 						RTYPE=9 K1=1.5E-8 K2=-5.0E-1 K3=0.0 #
 HOCOOH21+ + E1- -> HOCOOH + H 						RTYPE=9 K1=1.5E-8 K2=-5.0E-1 K3=0.0 #
 HPN1+ + E1- -> PN + H 								RTYPE=9 K1=1.5E-7 K2=-5.0E-1 K3=0.0 #
 HPO1+ + E1- -> PO + H 								RTYPE=9 K1=1.5E-7 K2=-5.0E-1 K3=0.0 #
 N21+ + E1- -> N + N 								RTYPE=9 K1=1.8E-7 K2=-3.9E-1 K3=0.0 #
 N2H1+ + E1- -> NH + N 								RTYPE=9 K1=6.4E-8 K2=-5.1E-1 K3=0.0 #
 N2H1+ + E1- -> N2 + H 								RTYPE=9 K1=3.6E-8 K2=-5.1E-1 K3=0.0 #
 OCN1+ + E1- -> CO + N 								RTYPE=9 K1=3.0E-7 K2=-5.0E-1 K3=0.0 #
 NH1+ + E1- -> N + H 								RTYPE=9 K1=1.18E-7 K2=-5.0E-1 K3=0.0 #
 NH21+ + E1- -> N + H + H 							RTYPE=9 K1=2.0E-7 K2=-5.0E-1 K3=0.0 #
 NH21+ + E1- -> NH + H 								RTYPE=9 K1=1.0E-7 K2=-5.0E-1 K3=0.0 #
 NH2CHOH1+ + E1- -> NH2 + HCO + H 					RTYPE=9 K1=1.35E-7 K2=-5.0E-1 K3=0.0 #
 NH2CHOH1+ + E1- -> NH + H2CO + H 					RTYPE=9 K1=1.35E-7 K2=-5.0E-1 K3=0.0 #
 NH2CHOH1+ + E1- -> NH2 + H2CO 						RTYPE=9 K1=1.5E-8 K2=-5.0E-1 K3=0.0 #
 NH2CHOH1+ + E1- -> NH2CHO + H 						RTYPE=9 K1=1.5E-8 K2=-5.0E-1 K3=0.0 #
 NH2CNH1+ + E1- -> NH2 + CN + H 					RTYPE=9 K1=1.35E-7 K2=-5.0E-1 K3=0.0 #
 NH2CNH1+ + E1- -> NH + HNC + H 					RTYPE=9 K1=1.35E-7 K2=-5.0E-1 K3=0.0 #
 NH2CNH1+ + E1- -> NH2 + HNC 						RTYPE=9 K1=1.5E-8 K2=-5.0E-1 K3=0.0 #
 NH2CNH1+ + E1- -> NH2CN + H 						RTYPE=9 K1=1.5E-8 K2=-5.0E-1 K3=0.0 #
 NH2OH1+ + E1- -> NH2 + OH 							RTYPE=9 K1=3.0E-7 K2=-5.0E-1 K3=0.0 #
 NH31+ + E1- -> NH2 + H 							RTYPE=9 K1=1.55E-7 K2=-5.0E-1 K3=0.0 #
 NH31+ + E1- -> NH + H + H 							RTYPE=9 K1=1.55E-7 K2=-5.0E-1 K3=0.0 #
 NH3OH1+ + E1- -> NH3 + OH 							RTYPE=9 K1=3.0E-7 K2=-5.0E-1 K3=0.0 #
 NH41+ + E1- -> NH2 + H2 							RTYPE=9 K1=1.5E-7 K2=-5.0E-1 K3=0.0 #
 NH41+ + E1- -> NH2 + H + H 						RTYPE=9 K1=3.2E-7 K2=-5.0E-1 K3=0.0 #
 NH41+ + E1- -> NH3 + H 							RTYPE=9 K1=1.05E-6 K2=-5.0E-1 K3=0.0 #
 NHCONH1+ + E1- -> NH + OCN + H 					RTYPE=9 K1=2.7E-7 K2=-5.0E-1 K3=0.0 #
 NHCONH1+ + E1- -> NH + HNCO 						RTYPE=9 K1=3.0E-8 K2=-5.0E-1 K3=0.0 #
 NO1+ + E1- -> N + O 								RTYPE=9 K1=4.1E-7 K2=-1.0 K3=0.0 #
 NO21+ + E1- -> O + NO 								RTYPE=9 K1=3.0E-7 K2=-5.0E-1 K3=0.0 #
 O21+ + E1- -> O + O 								RTYPE=9 K1=1.95E-7 K2=-7.0E-1 K3=0.0 #
 O2H1+ + E1- -> O2 + H 								RTYPE=9 K1=3.0E-7 K2=-5.0E-1 K3=0.0 #
 OH1+ + E1- -> O + H 								RTYPE=9 K1=6.3E-9 K2=-4.8E-1 K3=0.0 #
 CHOCHOH1+ + E1- -> HCO + HCO + H 					RTYPE=9 K1=2.7E-7 K2=-5.0E-1 K3=0.0 #
 CHOCHOH1+ + E1- -> HCO + H2CO 						RTYPE=9 K1=1.5E-8 K2=-5.0E-1 K3=0.0 #
 CHOCHOH1+ + E1- -> CHOCHO + H 						RTYPE=9 K1=1.5E-8 K2=-5.0E-1 K3=0.0 #
 CHOCHO1+ + E1- -> CO + HCO + H 					RTYPE=9 K1=2.7E-7 K2=-5.0E-1 K3=0.0 #
 CHOCHO1+ + E1- -> HCO + HCO 						RTYPE=9 K1=1.5E-8 K2=-5.0E-1 K3=0.0 #
 CHOCHO1+ + E1- -> COCHO + H 						RTYPE=9 K1=1.5E-8 K2=-5.0E-1 K3=0.0 #
 HCOCOOH1+ + E1- -> HCO + CO + OH 					RTYPE=9 K1=9.0E-8 K2=-5.0E-1 K3=0.0 #
 HCOCOOH1+ + E1- -> HCO + CO2 + H 					RTYPE=9 K1=9.0E-8 K2=-5.0E-1 K3=0.0 #
 HCOCOOH1+ + E1- -> CO + COOH + H 					RTYPE=9 K1=9.0E-8 K2=-5.0E-1 K3=0.0 #
 HCOCOOH1+ + E1- -> COCHO + OH 						RTYPE=9 K1=1.5E-8 K2=-5.0E-1 K3=0.0 #
 HCOCOOH1+ + E1- -> HCO + COOH 						RTYPE=9 K1=1.5E-8 K2=-5.0E-1 K3=0.0 #
 PN1+ + E1- -> P + N 								RTYPE=9 K1=1.8E-7 K2=-5.0E-1 K3=0.0 #
 PNH21+ + E1- -> NH2 + P 							RTYPE=9 K1=3.0E-7 K2=-5.0E-1 K3=0.0 #
 PNH31+ + E1- -> NH3 + P 							RTYPE=9 K1=3.0E-7 K2=-5.0E-1 K3=0.0 #
 PO1+ + E1- -> P + O 								RTYPE=9 K1=1.8E-7 K2=-5.0E-1 K3=0.0 #
 NH2CHO1+ + E1- -> NH2 + CO + H 					RTYPE=9 K1=1.35E-7 K2=-5.0E-1 K3=0.0 #
 NH2CHO1+ + E1- -> NH + HCO + H 					RTYPE=9 K1=1.35E-7 K2=-5.0E-1 K3=0.0 #
 NH2CHO1+ + E1- -> NH2 + HCO 						RTYPE=9 K1=1.5E-8 K2=-5.0E-1 K3=0.0 #
 NH2CHO1+ + E1- -> NH2CO + H 						RTYPE=9 K1=1.5E-8 K2=-5.0E-1 K3=0.0 #
 NH2CO1+ + E1- -> NH + CO + H 						RTYPE=9 K1=2.7E-7 K2=-5.0E-1 K3=0.0 #
 NH2CO1+ + E1- -> NH2 + CO 							RTYPE=9 K1=1.5E-8 K2=-5.0E-1 K3=0.0 #
 NH2CO1+ + E1- -> HNCO + H 							RTYPE=9 K1=1.5E-8 K2=-5.0E-1 K3=0.0 #
 NH2CONH1+ + E1- -> OCN + NH2 + H 					RTYPE=9 K1=1.35E-7 K2=-5.0E-1 K3=0.0 #
 NH2CONH1+ + E1- -> HNCO + NH + H 					RTYPE=9 K1=1.35E-7 K2=-5.0E-1 K3=0.0 #
 NH2CONH1+ + E1- -> NH + NH2CO 						RTYPE=9 K1=1.5E-8 K2=-5.0E-1 K3=0.0 #
 NH2CONH1+ + E1- -> HNCO + NH2 						RTYPE=9 K1=1.5E-8 K2=-5.0E-1 K3=0.0 #
 NH2COOH1+ + E1- -> NH2 + CO + OH 					RTYPE=9 K1=6.75E-8 K2=-5.0E-1 K3=0.0 #
 NH2COOH1+ + E1- -> NH2 + CO2 + H 					RTYPE=9 K1=6.75E-8 K2=-5.0E-1 K3=0.0 #
 NH2COOH1+ + E1- -> NH + COOH + H 					RTYPE=9 K1=6.75E-8 K2=-5.0E-1 K3=0.0 #
 NH2COOH1+ + E1- -> HNCO + OH + H 					RTYPE=9 K1=6.75E-8 K2=-5.0E-1 K3=0.0 #
 NH2COOH1+ + E1- -> COOH + NH2 						RTYPE=9 K1=1.5E-8 K2=-5.0E-1 K3=0.0 #
 NH2COOH1+ + E1- -> OH + NH2CO 						RTYPE=9 K1=1.5E-8 K2=-5.0E-1 K3=0.0 #
 NH2NH1+ + E1- -> NH + NH + H 						RTYPE=9 K1=2.85E-7 K2=-5.0E-1 K3=0.0 #
 NH2NH1+ + E1- -> NH2 + NH 							RTYPE=9 K1=1.5E-8 K2=-5.0E-1 K3=0.0 #
 NH2NH21+ + E1- -> NH2 + NH + H 					RTYPE=9 K1=2.7E-7 K2=-5.0E-1 K3=0.0 #
 NH2NH21+ + E1- -> NH2NH + H 						RTYPE=9 K1=1.5E-8 K2=-5.0E-1 K3=0.0 #
 NH2NH21+ + E1- -> NH2 + NH2 						RTYPE=9 K1=1.5E-8 K2=-5.0E-1 K3=0.0 #
 NH2NH2H1+ + E1- -> NH2 + NH2 + H 					RTYPE=9 K1=2.7E-7 K2=-5.0E-1 K3=0.0 #
 NH2NH2H1+ + E1- -> NH2 + NH3 						RTYPE=9 K1=1.5E-8 K2=-5.0E-1 K3=0.0 #
 NH2NH2H1+ + E1- -> NH2NH2 + H 						RTYPE=9 K1=1.5E-8 K2=-5.0E-1 K3=0.0 #
 NH2OCH31+ + E1- -> CH3O + NH + H 					RTYPE=9 K1=1.35E-7 K2=-5.0E-1 K3=0.0 #
 NH2OCH31+ + E1- -> H2CO + NH2 + H 					RTYPE=9 K1=1.35E-7 K2=-5.0E-1 K3=0.0 #
 NH2OCH31+ + E1- -> CH3O + NH2 						RTYPE=9 K1=3.0E-8 K2=-5.0E-1 K3=0.0 #
 HCOOCH31+ + E1- -> CH3O + CO + H 					RTYPE=9 K1=1.35E-7 K2=-5.0E-1 K3=0.0 #
 HCOOCH31+ + E1- -> H2CO + HCO + H 					RTYPE=9 K1=1.35E-7 K2=-5.0E-1 K3=0.0 #
 HCOOCH31+ + E1- -> CH3O + HCO 						RTYPE=9 K1=1.5E-8 K2=-5.0E-1 K3=0.0 #
 HCOOCH31+ + E1- -> CH3OCO + H 						RTYPE=9 K1=1.5E-8 K2=-5.0E-1 K3=0.0 #
 CH3OCO1+ + E1- -> H2CO + CO + H 					RTYPE=9 K1=2.7E-7 K2=-5.0E-1 K3=0.0 #
 CH3OCO1+ + E1- -> CH3O + CO 						RTYPE=9 K1=3.0E-8 K2=-5.0E-1 K3=0.0 #
 CH3ONH1+ + E1- -> H2CO + NH + H 					RTYPE=9 K1=2.7E-7 K2=-5.0E-1 K3=0.0 #
 CH3ONH1+ + E1- -> CH3O + NH 						RTYPE=9 K1=3.0E-8 K2=-5.0E-1 K3=0.0 #
 CH3OOH1+ + E1- -> H2CO + OH + H 					RTYPE=9 K1=2.7E-7 K2=-5.0E-1 K3=0.0 #
 CH3OOH1+ + E1- -> CH3O + OH 						RTYPE=9 K1=3.0E-8 K2=-5.0E-1 K3=0.0 #
 CH3OOH21+ + E1- -> CH3O + OH + H 					RTYPE=9 K1=3.0E-8 K2=-5.0E-1 K3=0.0 #
 CH3OOH21+ + E1- -> H2CO + H2O + H 					RTYPE=9 K1=2.4E-7 K2=-5.0E-1 K3=0.0 #
 CH3OOH21+ + E1- -> CH3O + H2O 						RTYPE=9 K1=1.5E-8 K2=-5.0E-1 K3=0.0 #
 CH3OOH21+ + E1- -> CH3OOH + H 						RTYPE=9 K1=1.5E-8 K2=-5.0E-1 K3=0.0 #
 HOCH2CO1+ + E1- -> CH2 + OH + CO 					RTYPE=9 K1=1.43E-7 K2=-5.0E-1 K3=0.0 #
 HOCH2CO1+ + E1- -> H2CO + CO + H 					RTYPE=9 K1=1.43E-7 K2=-5.0E-1 K3=0.0 #
 HOCH2CO1+ + E1- -> CH2OH + CO 						RTYPE=9 K1=1.5E-8 K2=-5.0E-1 K3=0.0 #
 CH2OHNH1+ + E1- -> H2CO + NH + H 					RTYPE=9 K1=2.7E-7 K2=-5.0E-1 K3=0.0 #
 CH2OHNH1+ + E1- -> CH2OH + NH 						RTYPE=9 K1=3.0E-8 K2=-5.0E-1 K3=0.0 #
 HOCH2OH1+ + E1- -> H2CO + OH + H 					RTYPE=9 K1=2.7E-7 K2=-5.0E-1 K3=0.0 #
 HOCH2OH1+ + E1- -> CH2OH + OH 						RTYPE=9 K1=3.0E-8 K2=-5.0E-1 K3=0.0 #
 C1+ + E1- -> C 									RTYPE=10 K1=4.4E-12 K2=-6.1E-1 K3=0.0 #
 CH31+ + E1- -> CH3 								RTYPE=10 K1=1.1E-10 K2=-7.0E-1 K3=0.0 #
 H1+ + E1- -> H 									RTYPE=10 K1=3.5E-12 K2=-7.0E-1 K3=0.0 #
 H21+ + E1- -> H2 									RTYPE=10 K1=2.25E-7 K2=-4.0E-1 K3=0.0 #
 H2CO1+ + E1- -> H2CO 								RTYPE=10 K1=1.1E-10 K2=-7.0E-1 K3=0.0 #
 N1+ + E1- -> N 									RTYPE=10 K1=3.8E-12 K2=-6.2E-1 K3=0.0 #
 O1+ + E1- -> O 									RTYPE=10 K1=3.4E-12 K2=-6.3E-1 K3=0.0 #
 C1+ + C1- -> C + C 								RTYPE=11 K1=2.3E-7 K2=-5.0E-1 K3=0.0 #
 C1+ + H1- -> H + C 								RTYPE=11 K1=2.3E-7 K2=-5.0E-1 K3=0.0 #
 H1+ + C1- -> H + C 								RTYPE=11 K1=2.3E-7 K2=-5.0E-1 K3=0.0 #
 H1+ + H1- -> H + H 								RTYPE=11 K1=2.3E-7 K2=-5.0E-1 K3=0.0 #
 H21+ + H1- -> H + H2 								RTYPE=11 K1=2.3E-7 K2=-5.0E-1 K3=0.0 #
 H31+ + H1- -> H2 + H2 								RTYPE=11 K1=2.3E-7 K2=-5.0E-1 K3=0.0 #
 H3O1+ + H1- -> OH + H2 + H 						RTYPE=11 K1=2.3E-7 K2=-5.0E-1 K3=0.0 #
 H3O1+ + H1- -> H2O + H2 							RTYPE=11 K1=2.3E-7 K2=-5.0E-1 K3=0.0 #
 HCO1+ + H1- -> CO + H2 							RTYPE=11 K1=2.3E-7 K2=-5.0E-1 K3=0.0 #
 N1+ + C1- -> N + C 								RTYPE=11 K1=2.3E-7 K2=-5.0E-1 K3=0.0 #
 N1+ + H1- -> H + N 								RTYPE=11 K1=2.3E-7 K2=-5.0E-1 K3=0.0 #
 NH41+ + H1- -> NH3 + H2 							RTYPE=11 K1=2.3E-7 K2=-5.0E-1 K3=0.0 #
 O1+ + C1- -> O + C 								RTYPE=11 K1=2.3E-7 K2=-5.0E-1 K3=0.0 #
 O1+ + H1- -> H + O 								RTYPE=11 K1=2.3E-7 K2=-5.0E-1 K3=0.0 #
 C + E1- -> C1- 									RTYPE=12 K1=3.0E-15 K2=0.0 K3=0.0 #
 H + E1- -> H1- 									RTYPE=12 K1=3.0E-16 K2=1.0 K3=0.0 #
 O + E1- -> O1- 									RTYPE=12 K1=1.5E-15 K2=0.0 K3=0.0 #
 C -> C1+ + E1- 									RTYPE=13 K1=2.16E-10 K2=0.0 K3=2.61 #
 C1- -> C + E1- 									RTYPE=13 K1=2.4E-7 K2=0.0 K3=9.0E-1 #
 CH3CH2 -> CH3CH21+ + E1- 							RTYPE=13 K1=2.0E-10 K2=0.0 K3=2.5 #
 CH -> CH1+ + E1- 									RTYPE=13 K1=2.9E-10 K2=0.0 K3=2.8 #
 CH2 -> CH21+ + E1- 								RTYPE=13 K1=1.0E-9 K2=0.0 K3=2.3 #
 CH2CO -> CH2CO1+ + E1- 							RTYPE=13 K1=3.44E-10 K2=0.0 K3=2.01 #
 CH3 -> CH31+ + E1- 								RTYPE=13 K1=1.0E-10 K2=0.0 K3=2.1 #
 CH3CHO -> CH3CHO1+ + E1- 							RTYPE=13 K1=2.6E-10 K2=0.0 K3=2.28 #
 CH3CN -> CH3CN1+ + E1- 							RTYPE=13 K1=5.29E-10 K2=0.0 K3=3.11 #
 CH3NH2 -> CH3NH21+ + E1- 							RTYPE=13 K1=2.6E-10 K2=0.0 K3=2.28 #
 CH3OCH3 -> CH3OCH31+ + E1- 						RTYPE=13 K1=2.6E-10 K2=0.0 K3=2.28 #
 CH3OH -> CH3OH1+ + E1- 							RTYPE=13 K1=4.8E-10 K2=0.0 K3=2.57 #
 CN1- -> CN + E1- 									RTYPE=13 K1=2.4E-7 K2=0.0 K3=9.0E-1 #
 H1- -> H + E1- 									RTYPE=13 K1=2.4E-7 K2=0.0 K3=9.0E-1 #
 H2CN -> H2CN1+ + E1- 								RTYPE=13 K1=2.0E-10 K2=0.0 K3=2.5 #
 H2CO -> H2CO1+ + E1- 								RTYPE=13 K1=8.0E-11 K2=0.0 K3=2.8 #
 H2CO -> HCO1+ + H + E1- 							RTYPE=13 K1=1.4E-11 K2=0.0 K3=3.1 #
 H2O -> H2O1+ + E1- 								RTYPE=13 K1=2.1E-11 K2=0.0 K3=3.1 #
 HCO -> HCO1+ + E1- 								RTYPE=13 K1=2.46E-10 K2=0.0 K3=2.11 #
 HCOOCH3 -> HCOOCH31+ + E1- 						RTYPE=13 K1=2.0E-10 K2=0.0 K3=2.5 #
 HCOOH -> HCOOH1+ + E1- 							RTYPE=13 K1=1.73E-10 K2=0.0 K3=2.59 #
 HPO -> HPO1+ + E1- 								RTYPE=13 K1=2.0E-10 K2=0.0 K3=2.5 #
 NH -> NH1+ + E1- 									RTYPE=13 K1=1.0E-11 K2=0.0 K3=2.0 #
 NH2 -> NH21+ + E1- 								RTYPE=13 K1=1.73E-10 K2=0.0 K3=2.59 #
 NH3 -> NH31+ + E1- 								RTYPE=13 K1=1.24E-10 K2=0.0 K3=2.47 #
 NO -> NO1+ + E1- 									RTYPE=13 K1=2.0E-10 K2=0.0 K3=2.0 #
 O1- -> O + E1- 									RTYPE=13 K1=2.4E-7 K2=0.0 K3=9.0E-1 #
 O2 -> O21+ + E1- 									RTYPE=13 K1=6.2E-12 K2=0.0 K3=3.1 #
 HO2 -> O2H1+ + E1- 								RTYPE=13 K1=2.0E-10 K2=0.0 K3=2.5 #
 OH -> OH1+ + E1- 									RTYPE=13 K1=1.6E-12 K2=0.0 K3=3.1 #
 OH1- -> OH + E1- 									RTYPE=13 K1=2.4E-7 K2=0.0 K3=9.0E-1 #
 P -> P1+ + E1- 									RTYPE=13 K1=1.0E-9 K2=0.0 K3=2.65 #
 PN -> PN1+ + E1- 									RTYPE=13 K1=2.0E-10 K2=0.0 K3=2.5 #
 PO -> PO1+ + E1- 									RTYPE=13 K1=2.0E-10 K2=0.0 K3=2.5 #
 CH3CH2OH -> CH3CH2 + OH 							RTYPE=13 K1=5.0E-10 K2=0.0 K3=1.7 #
 CH -> C + H 										RTYPE=13 K1=1.4E-10 K2=0.0 K3=1.5 #
 CH1+ -> C1+ + H 									RTYPE=13 K1=4.6E-12 K2=0.0 K3=3.0 #
 CH2 -> CH + H 										RTYPE=13 K1=5.0E-11 K2=0.0 K3=1.7 #
 CH21+ -> CH1+ + H 									RTYPE=13 K1=1.7E-9 K2=0.0 K3=1.7 #
 CH2CO -> CH2 + CO 									RTYPE=13 K1=9.04E-10 K2=0.0 K3=1.58 #
 CH2NH -> HCN + H2 									RTYPE=13 K1=1.7E-9 K2=0.0 K3=1.63 #
 CH2OH -> CH2 + OH 									RTYPE=13 K1=5.0E-10 K2=0.0 K3=1.7 #
 CH3 -> CH2 + H 									RTYPE=13 K1=3.0E-11 K2=0.0 K3=1.7 #
 CH3 -> CH + H2 									RTYPE=13 K1=3.0E-11 K2=0.0 K3=1.7 #
 CH31+ -> CH21+ + H 								RTYPE=13 K1=1.0E-9 K2=0.0 K3=1.7 #
 CH31+ -> CH1+ + H2 								RTYPE=13 K1=1.0E-9 K2=0.0 K3=1.7 #
 CH3CHO -> CH3 + HCO 								RTYPE=13 K1=3.43E-10 K2=0.0 K3=1.52 #
 CH3CHO -> CH4 + CO 								RTYPE=13 K1=3.43E-10 K2=0.0 K3=1.52 #
 CH3CN -> CH3 + CN 									RTYPE=13 K1=1.56E-9 K2=0.0 K3=1.95 #
 CH3CO -> CH3 + CO 									RTYPE=13 K1=9.0E-10 K2=0.0 K3=1.6 #
 CH3NH -> NH + CH3 									RTYPE=13 K1=1.0E-9 K2=0.0 K3=1.7 #
 CH3NH2 -> CH2NH + H + H 							RTYPE=13 K1=6.63E-11 K2=0.0 K3=1.51 #
 CH3NH2 -> CN + H2 + H2 + H 						RTYPE=13 K1=9.42E-11 K2=0.0 K3=1.76 #
 CH3NH2 -> HCN + H2 + H + H 						RTYPE=13 K1=3.5E-10 K2=0.0 K3=1.73 #
 CH3NH2 -> CH3 + NH2 								RTYPE=13 K1=1.55E-10 K2=0.0 K3=1.74 #
 CH3O -> CH3 + O 									RTYPE=13 K1=5.0E-10 K2=0.0 K3=1.7 #
 CH3OCH3 -> CH3O + CH3 								RTYPE=13 K1=9.0E-10 K2=0.0 K3=1.6 #
 CH3OH -> CH2OH + H 								RTYPE=13 K1=5.0E-10 K2=0.0 K3=1.6 #
 CH3OH -> CH3O + H 									RTYPE=13 K1=2.0E-10 K2=0.0 K3=1.6 #
 CH3OH -> CH3 + OH 									RTYPE=13 K1=5.0E-10 K2=0.0 K3=1.7 #
 CH4 -> CH3 + H 									RTYPE=13 K1=1.6E-10 K2=0.0 K3=2.2 #
 CH4 -> CH2 + H2 									RTYPE=13 K1=4.8E-10 K2=0.0 K3=2.2 #
 CH4 -> CH + H + H2 								RTYPE=13 K1=1.6E-10 K2=0.0 K3=2.2 #
 CN -> C + N 										RTYPE=13 K1=1.0E-9 K2=0.0 K3=2.8 #
 CO -> C + O 										RTYPE=13 K1=3.1E-11 K2=0.0 K3=2.54 #
 CO2 -> CO + O 										RTYPE=13 K1=3.13E-10 K2=0.0 K3=2.03 #
 COCHO -> HCO + CO 									RTYPE=13 K1=9.0E-10 K2=0.0 K3=1.6 #
 COOH -> CO + OH 									RTYPE=13 K1=1.0E-9 K2=0.0 K3=1.8 #
 H2 -> H + H 										RTYPE=13 K1=0.0 K2=0.0 K3=0.0 #
 H21+ -> H1+ + H 									RTYPE=13 K1=2.6E-10 K2=0.0 K3=1.8 #
 H2CN -> HCN + H 									RTYPE=13 K1=1.0E-9 K2=0.0 K3=1.7 #
 H2CO -> CO + H2 									RTYPE=13 K1=4.4E-10 K2=0.0 K3=1.6 #
 H2CO -> CO + H + H 								RTYPE=13 K1=4.4E-10 K2=0.0 K3=1.6 #
 H2O -> OH + H 										RTYPE=13 K1=3.28E-10 K2=0.0 K3=1.63 #
 H2O2 -> OH + OH 									RTYPE=13 K1=8.3E-10 K2=0.0 K3=1.82 #
 H31+ -> H21+ + H 									RTYPE=13 K1=7.9E-9 K2=0.0 K3=2.3 #
 H31+ -> H1+ + H2 									RTYPE=13 K1=2.0E-8 K2=0.0 K3=1.8 #
 HCN -> CN + H 										RTYPE=13 K1=5.48E-10 K2=0.0 K3=2.0 #
 HCO -> H + CO 										RTYPE=13 K1=5.87E-10 K2=0.0 K3=5.3E-1 #
 HCOOCH3 -> HCO + CH3O 								RTYPE=13 K1=5.0E-10 K2=0.0 K3=1.7 #
 HCOOH -> HCO + OH 									RTYPE=13 K1=2.75E-10 K2=0.0 K3=1.8 #
 CH3COCHO -> CH3CO + HCO 							RTYPE=13 K1=9.0E-10 K2=0.0 K3=1.6 #
 CH3COCH3 -> CH3CO + CH3 							RTYPE=13 K1=9.0E-10 K2=0.0 K3=1.6 #
 CH3CONH -> CH3CO + NH 								RTYPE=13 K1=1.0E-9 K2=0.0 K3=1.7 #
 CH3CONH -> CH3 + HNCO 								RTYPE=13 K1=9.0E-10 K2=0.0 K3=1.6 #
 CH3COOH -> CH3CO + OH 								RTYPE=13 K1=5.0E-10 K2=0.0 K3=1.7 #
 CH3COOH -> CH3 + COOH 								RTYPE=13 K1=9.0E-10 K2=0.0 K3=1.6 #
 CH3CONH2 -> CH3CO + NH2 							RTYPE=13 K1=1.0E-9 K2=0.0 K3=1.7 #
 CH3CONH2 -> CH3 + NH2CO 							RTYPE=13 K1=1.0E-9 K2=0.0 K3=1.6 #
 HNC -> CN + H 										RTYPE=13 K1=5.48E-10 K2=0.0 K3=2.0 #
 HNCHO -> NH + HCO 									RTYPE=13 K1=1.0E-9 K2=0.0 K3=1.7 #
 HNCOCHO -> HNCO + HCO 								RTYPE=13 K1=9.0E-10 K2=0.0 K3=1.6 #
 HNCOCHO -> NH + COCHO 								RTYPE=13 K1=1.0E-9 K2=0.0 K3=1.7 #
 HNCONH -> NH + HNCO 								RTYPE=13 K1=1.0E-9 K2=0.0 K3=1.7 #
 HNCOOH -> COOH + NH 								RTYPE=13 K1=1.0E-9 K2=0.0 K3=1.7 #
 HNCOOH -> OH + HNCO 								RTYPE=13 K1=5.0E-10 K2=0.0 K3=1.7 #
 HNO -> NO + H 										RTYPE=13 K1=1.7E-10 K2=0.0 K3=5.3E-1 #
 HNOH -> NH + OH 									RTYPE=13 K1=1.0E-9 K2=0.0 K3=1.8 #
 CH3ONH -> NH + CH3O 								RTYPE=13 K1=1.0E-9 K2=0.0 K3=1.8 #
 HNCH2OH -> NH + CH2OH 								RTYPE=13 K1=1.0E-9 K2=0.0 K3=1.7 #
 HOCOOH -> OH + COOH 								RTYPE=13 K1=5.0E-10 K2=0.0 K3=1.7 #
 HPO -> PO + H 										RTYPE=13 K1=1.0E-9 K2=0.0 K3=1.7 #
 HCOCOCHO -> HCO + COCHO 							RTYPE=13 K1=9.0E-10 K2=0.0 K3=1.6 #
 N2 -> N + N 										RTYPE=13 K1=5.0E-12 K2=0.0 K3=3.0 #
 N2O -> N2 + O 										RTYPE=13 K1=1.4E-9 K2=0.0 K3=1.7 #
 NH -> H + N 										RTYPE=13 K1=4.0E-10 K2=0.0 K3=1.5 #
 NH2 -> NH + H 										RTYPE=13 K1=2.11E-10 K2=0.0 K3=1.52 #
 NH2CN -> NH2 + CN 									RTYPE=13 K1=1.0E-9 K2=0.0 K3=1.7 #
 NH3 -> NH2 + H 									RTYPE=13 K1=4.94E-10 K2=0.0 K3=1.65 #
 NH3 -> NH + H2 									RTYPE=13 K1=1.3E-10 K2=0.0 K3=1.91 #
 NO -> N + O 										RTYPE=13 K1=3.0E-10 K2=0.0 K3=2.0 #
 NO2 -> NO + O 										RTYPE=13 K1=1.29E-9 K2=0.0 K3=2.0 #
 O2 -> O + O 										RTYPE=13 K1=3.3E-10 K2=0.0 K3=1.4 #
 HO2 -> O2 + H 										RTYPE=13 K1=5.0E-10 K2=0.0 K3=1.7 #
 HO2 -> OH + O 										RTYPE=13 K1=5.0E-10 K2=0.0 K3=1.7 #
 OCN -> O + CN 										RTYPE=13 K1=1.0E-11 K2=0.0 K3=2.0 #
 OH -> O + H 										RTYPE=13 K1=1.68E-10 K2=0.0 K3=1.66 #
 OH1+ -> H1+ + O 									RTYPE=13 K1=7.2E-12 K2=0.0 K3=1.8 #
 CHOCHO -> HCO + HCO 								RTYPE=13 K1=9.0E-10 K2=0.0 K3=1.6 #
 HCOCOOH -> COCHO + OH 								RTYPE=13 K1=5.0E-10 K2=0.0 K3=1.7 #
 HCOCOOH -> HCO + COOH 								RTYPE=13 K1=9.0E-10 K2=0.0 K3=1.6 #
 PN -> N + P 										RTYPE=13 K1=1.0E-9 K2=0.0 K3=1.7 #
 PO -> O + P 										RTYPE=13 K1=1.0E-9 K2=0.0 K3=1.7 #
 NH2CO -> NH2 + CO 									RTYPE=13 K1=1.0E-9 K2=0.0 K3=1.7 #
 NH2COCHO -> NH2CO + HCO 							RTYPE=13 K1=9.0E-10 K2=0.0 K3=1.6 #
 NH2COCHO -> NH2 + COCHO 							RTYPE=13 K1=1.0E-9 K2=0.0 K3=1.7 #
 NH2CONH -> NH2 + HNCO 								RTYPE=13 K1=1.0E-9 K2=0.0 K3=1.7 #
 NH2CONH -> NH2CO + NH 								RTYPE=13 K1=1.0E-9 K2=0.0 K3=1.7 #
 NH2COOH -> NH2 + COOH 								RTYPE=13 K1=1.0E-9 K2=0.0 K3=1.7 #
 NH2COOH -> NH2CO + OH 								RTYPE=13 K1=5.0E-10 K2=0.0 K3=1.7 #
 NH2CONH2 -> NH2 + NH2CO 							RTYPE=13 K1=1.0E-9 K2=0.0 K3=1.7 #
 NH2NH -> NH + NH2 									RTYPE=13 K1=1.0E-11 K2=0.0 K3=3.0 #
 NH2NH2 -> NH2 + NH2 								RTYPE=13 K1=1.0E-11 K2=0.0 K3=3.0 #
 NH2OCH3 -> NH2 + CH3O 								RTYPE=13 K1=1.0E-9 K2=0.0 K3=1.8 #
 NH2CH2OH -> NH2 + CH2OH 							RTYPE=13 K1=1.0E-9 K2=0.0 K3=1.7 #
 CH3OCO -> CH3O + CO 								RTYPE=13 K1=5.0E-10 K2=0.0 K3=1.7 #
 CH3OCONH -> CH3O + HNCO 							RTYPE=13 K1=5.0E-10 K2=0.0 K3=1.7 #
 CH3OCONH -> CH3OCO + NH 							RTYPE=13 K1=1.0E-9 K2=0.0 K3=1.7 #
 CH3OCOOH -> CH3OCO + OH 							RTYPE=13 K1=5.0E-10 K2=0.0 K3=1.7 #
 CH3OCOOH -> CH3O + COOH 							RTYPE=13 K1=5.0E-10 K2=0.0 K3=1.7 #
 CH3OOH -> OH + CH3O 								RTYPE=13 K1=1.0E-9 K2=0.0 K3=1.8 #
 CH3OOCH3 -> CH3O + CH3O 							RTYPE=13 K1=1.0E-9 K2=0.0 K3=1.8 #
 CH2OHCHO -> CH2OH + HCO 							RTYPE=13 K1=9.0E-10 K2=0.0 K3=1.6 #
 HOCH2CO -> CH2OH + CO 								RTYPE=13 K1=9.0E-10 K2=0.0 K3=1.6 #
 HOCH2OH -> OH + CH2OH 								RTYPE=13 K1=5.0E-10 K2=0.0 K3=1.7 #
 gC + gN -> gCN 									RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gC + gN -> CN 										RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gC + gNH -> gHNC 									RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gC + gNH -> HNC 									RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gC + gNH2 -> gHNC + gH 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gC + gNH2 -> HNC + H 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gC + gNO -> gCN + gO 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gC + gNO -> CN + O 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gC + gNO -> gOCN 									RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gC + gNO -> OCN 									RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gC + gO -> gCO 									RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gC + gO -> CO 										RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gC + gO2 -> gCO + gO 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gC + gO2 -> CO + O 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gC + gOCN -> gCO + gCN 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gC + gOCN -> CO + CN 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gC + gOH -> gCO + gH 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gC + gOH -> CO + H 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gC + gOH -> gHOC 									RTYPE=14 K1=0.0 K2=0.0 K3=0.0 #
 gC + gOH -> HOC 									RTYPE=14 K1=0.0 K2=0.0 K3=0.0 #
 gCH + gHNO -> gNO + gCH2 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gCH + gHNO -> NO + CH2 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gCH + gNH -> gHCN + gH 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gCH + gNH -> HCN + H 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gCH + gNH -> gHNC + gH 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gCH + gNH -> HNC + H 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gCH + gNH -> gHCNH 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gCH + gNH -> HCNH 									RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gCH + gNH2 -> gCH2NH 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gCH + gNH2 -> CH2NH 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gCH + gNO -> gHCN + gO 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gCH + gNO -> HCN + O 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gCH + gO2 -> gHCO + gO 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gCH + gO2 -> HCO + O 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gCH + gOH -> gHCOH 								RTYPE=14 K1=0.0 K2=0.0 K3=0.0 #
 gCH + gOH -> HCOH 									RTYPE=14 K1=0.0 K2=0.0 K3=0.0 #
 gCH2 + gCH3 -> gCH3CH2 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gCH2 + gCH3 -> CH3CH2 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gCH2 + gHNO -> gCH3 + gNO 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gCH2 + gHNO -> CH3 + NO 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gCH2 + gNH2 -> gCH2NH2 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gCH2 + gNH2 -> CH2NH2 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gCH2 + gO2 -> gH2CO + gO 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gCH2 + gO2 -> H2CO + O 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gCH2OH + gCH3CHO -> gCH3OH + gCH3CO 				RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gCH2OH + gCH3CHO -> CH3OH + CH3CO 					RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gCH2OH + gCH3CHO -> CH3CH2OH + HCO 				RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gCH2OH + gCH3CHO -> CH2OHCHO + CH3 				RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gCH2OH + gCO -> gHOCH2CO 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gCH2OH + gCO -> HOCH2CO 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gCH2OH + gH2CO -> CH3OH + HCO 						RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gCH2OH + gH2CO -> gCH3OH + gHCO 					RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gCH2OH + gHCO -> CH2OHCHO 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gCH2OH + gNH2CHO -> gCH3OH + gNH2CO 				RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gCH2OH + gNH2CHO -> CH3OH + NH2CO 					RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gCH2OH + gCHOCHO -> CH2OHCHO + HCO 				RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gCH3 + gCH2OH -> CH3CH2OH 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gCH3 + gCH3 -> gCH3CH3 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gCH3 + gCH3 -> CH3CH3 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gCH3 + gCH3CHO -> gCH4 + gCH3CO 					RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gCH3 + gCH3CHO -> gCH3CH3 + gHCO 					RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gCH3 + gCH3CHO -> CH4 + CH3CO 						RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gCH3 + gCH3CHO -> CH3CH3 + HCO 					RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gCH3 + gCH3CO -> CH3COCH3 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gCH3 + gCH3O -> CH3OCH3 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gCH3 + gCH3O -> gCH3OCH3 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gCH3 + gCN -> CH3CN 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gCH3 + gCN -> gCH3CN 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gCH3 + gCO -> gCH3CO 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gCH3 + gCO -> CH3CO 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gCH3 + gCOCHO -> CH3COCHO 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gCH3 + gCOOH -> gCH3COOH 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gCH3 + gCOOH -> CH3COOH 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gCH3 + gH2CO -> gCH4 + gHCO 						RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gCH3 + gH2CO -> CH4 + HCO 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gCH3 + gHCO -> CH3CHO 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gCH3 + gHCO -> gCH3CHO 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gCH3 + gHCOOCH3 -> gCH4 + gCH3OCO 					RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gCH3 + gHCOOCH3 -> CH4 + CH3OCO 					RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gCH3 + gHCOOH -> gCH4 + gCOOH 						RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gCH3 + gHCOOH -> CH4 + COOH 						RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gCH3 + gHNCO -> CH3CONH 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gCH3 + gHNCO -> gCH3CONH 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gCH3 + gHNO -> CH4 + NO 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gCH3 + gHNO -> gCH4 + gNO 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gCH3 + gNH2CHO -> CH4 + NH2CO 						RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gCH3 + gNH2CHO -> gCH4 + gNH2CO 					RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gCH3 + gCHOCHO -> CH3CHO + HCO 					RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gCH3 + gCHOCHO -> gCH3CHO + gHCO 					RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gCH3 + gNH2CO -> CH3CONH2 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gCH3O + gCH3CHO -> gHCOOCH3 + gCH3 				RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gCH3O + gCH3CHO -> CH3OH + CH3CO 					RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gCH3O + gCH3CHO -> gCH3OH + gCH3CO 				RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gCH3O + gCH3CHO -> HCOOCH3 + CH3 					RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gCH3O + gCH3O -> CH3OOCH3 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gCH3O + gCO -> gCH3OCO 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gCH3O + gCO -> CH3OCO 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gCH3O + gCOOH -> CH3OCOOH 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gCH3O + gH2CO -> gHCOOCH3 + gH 					RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gCH3O + gH2CO -> gCH3OH + gHCO 					RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gCH3O + gH2CO -> CH3OH + HCO 						RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gCH3O + gH2CO -> HCOOCH3 + H 						RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gCH3O + gHCOOCH3 -> CH3OH + CH3OCO 				RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gCH3O + gHCOOCH3 -> gCH3OH + gCH3OCO 				RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gCH3O + gHCOOH -> gCH3OH + gCOOH 					RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gCH3O + gHCOOH -> CH3OH + COOH 					RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gCH3O + gHNCO -> CH3OCONH 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gCH3O + gNH2CHO -> gCH3OH + gNH2CO 				RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gCH3O + gNH2CHO -> CH3OH + NH2CO 					RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gCH3O + gNH2CHO -> gHCOOCH3 + gNH2 				RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gCH3O + gNH2CHO -> HCOOCH3 + NH2 					RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gCH3O + gCHOCHO -> HCOOCH3 + HCO 					RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gCH3O + gCHOCHO -> gHCOOCH3 + gHCO 				RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gC -> gCH 									RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gC -> CH 										RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gCH3CH2 -> gCH3CH3 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gCH3CH2 -> CH3CH3 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gCH3CH3 -> gCH3CH2 + gH2 						RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gCH3CH3 -> CH3CH2 + H2 						RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gCH -> gCH2 									RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gCH -> CH2 									RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gCH2 -> gCH3 									RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gCH2 -> CH3 									RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gCH2NH -> gCH2NH2 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gCH2NH -> CH2NH2 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gCH2NH -> gCH3NH 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gCH2NH -> CH3NH 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gCH2NH2 -> gCH3NH2 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gCH2NH2 -> CH3NH2 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gCH2OH -> gCH3OH 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gCH2OH -> CH3OH 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gCH3 -> gCH4 									RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gCH3 -> CH4 									RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gCH3CHO -> HCO + CH4 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gCH3CHO -> H2CO + CH3 						RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gCH3CHO -> H2 + CH3CO 						RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gCH3CHO -> gH2 + gCH3CO 						RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gCH3CHO -> gHCO + gCH4 						RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gCH3CHO -> gH2CO + gCH3 						RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gCH3CO -> gCH3CHO 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gCH3CO -> CH3CHO 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gCH3NH -> gCH3NH2 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gCH3NH -> CH3NH2 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gCH3O -> gCH3OH 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gCH3O -> CH3OH 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gCH4 -> gCH3 + gH2 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gCH4 -> CH3 + H2 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gHCNH -> gCH2NH 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gHCNH -> CH2NH 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gHCOH -> gCH2OH 								RTYPE=14 K1=0.0 K2=0.0 K3=0.0 #
 gH + gHCOH -> CH2OH 								RTYPE=14 K1=0.0 K2=0.0 K3=0.0 #
 gH + gCN -> gHCN 									RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gCN -> HCN 									RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gCO -> gHCO 									RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gCO -> HOC 									RTYPE=14 K1=0.0 K2=0.0 K3=0.0 #
 gH + gCO -> HCO 									RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gCO -> gHOC 									RTYPE=14 K1=0.0 K2=0.0 K3=0.0 #
 gH + gCOCHO -> gCHOCHO 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gCOCHO -> CHOCHO 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gCOOH -> HCOOH 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gCOOH -> gHCOOH 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gH -> gH2 									RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gH -> H2 										RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gH2CN -> gCH2NH 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gH2CN -> CH2NH 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gH2CO -> gHCO + gH2 							RTYPE=14 K1=0.0 K2=0.0 K3=0.0 #
 gH + gH2CO -> HCO + H2 							RTYPE=14 K1=0.0 K2=0.0 K3=0.0 #
 gH + gH2CO -> gCH2OH 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gH2CO -> CH2OH 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gH2CO -> gCH3O 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gH2CO -> CH3O 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gH2O2 -> gHO2 + gH2 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gH2O2 -> HO2 + H2 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gH2O2 -> gH2O + gOH 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gH2O2 -> H2O + OH 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gHCO -> gH2CO 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gHCO -> H2CO 									RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gHCOOCH3 -> H2 + CH3OCO 						RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gHCOOCH3 -> CH3OH + HCO 						RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gHCOOCH3 -> gCH3OH + gHCO 					RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gHCOOCH3 -> gH2 + gCH3OCO 					RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gHCOOH -> HCO + H2O 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gHCOOH -> H2 + COOH 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gHCOOH -> gH2 + gCOOH 						RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gHCOOH -> gHCO + gH2O 						RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gCH3CONH -> CH3CONH2 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gHNCHO -> gNH2CHO 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gHNCHO -> NH2CHO 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gHNCO -> HNCHO 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gHNCO -> gHNCHO 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gHNCO -> gNH2CO 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gHNCO -> NH2CO 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gHNCOCHO -> NH2COCHO 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gHNCONH -> gNH2CONH 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gHNCONH -> NH2CONH 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gHNCOOH -> gNH2COOH 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gHNCOOH -> NH2COOH 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gHNO -> gNO + gH2 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gHNO -> NO + H2 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gHNOH -> gNH2OH 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gHNOH -> NH2OH 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gCH3ONH -> gNH2OCH3 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gCH3ONH -> NH2OCH3 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gHNCH2OH -> NH2CH2OH 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gHOC -> gHCOH 								RTYPE=14 K1=0.0 K2=0.0 K3=0.0 #
 gH + gHOC -> HCOH 									RTYPE=14 K1=0.0 K2=0.0 K3=0.0 #
 gH + gN -> gNH 									RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gN -> NH 										RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gN2H2 -> gH2 + gN2 + gH 						RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gN2H2 -> H2 + N2 + H 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gNH -> gNH2 									RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gNH -> NH2 									RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gNH2 -> gNH3 									RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gNH2 -> NH3 									RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gNH2CHO -> gH2 + gNH2CO 						RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gNH2CHO -> HCO + NH3 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gNH2CHO -> H2 + NH2CO 						RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gNH2CHO -> gHCO + gNH3 						RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gNO -> gHNO 									RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gNO -> HNO 									RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gO -> gOH 									RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gO -> OH 										RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gO2 -> gHO2 									RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gO2 -> HO2 									RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gHO2 -> gH2O2 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gHO2 -> H2O2 									RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gO3 -> gO2 + gOH 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gO3 -> O2 + OH 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gOCN -> gHNCO 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gOCN -> HNCO 									RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gOH -> gH2O 									RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gOH -> H2O 									RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gCHOCHO -> H2CO + HCO 						RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gCHOCHO -> gH2CO + gHCO 						RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gNH2CO -> gNH2CHO 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gNH2CO -> NH2CHO 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gNH2CONH -> NH2CONH2 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gNH2NH -> gNH2NH2 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gNH2NH -> NH2NH2 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gCH3OCO -> HCOOCH3 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gCH3OCO -> gHCOOCH3 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH + gHOCH2CO -> CH2OHCHO 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH2 + gC -> gCH2 									RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH2 + gC -> CH2 									RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH2 + gCH2 -> gCH3 + gH 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH2 + gCH2 -> CH3 + H 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH2 + gCH3 -> gCH4 + gH 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH2 + gCH3 -> CH4 + H 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH2 + gCN -> gHCN + gH 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH2 + gCN -> HCN + H 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH2 + gNH2 -> gNH3 + gH 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH2 + gNH2 -> NH3 + H 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH2 + gOH -> gH2O + gH 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gH2 + gOH -> H2O + H 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gHCO + gCH3CO -> CH3COCHO 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gHCO + gCH3O -> HCOOCH3 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gHCO + gCH3O -> gHCOOCH3 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gHCO + gCOCHO -> HCOCOCHO 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gHCO + gCOOH -> HCOCOOH 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gHCO + gCOOH -> gHCOCOOH 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gHCO + gHCO -> CHOCHO 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gHCO + gHCO -> gCHOCHO 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gHCO + gHNCO -> HNCOCHO 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gHCO + gHNCO -> gHNCOCHO 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gHCO + gNH2CO -> NH2COCHO 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gN + gCH -> gHCN 									RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gN + gCH -> HCN 									RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gN + gCH2 -> gH2CN 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gN + gCH2 -> H2CN 									RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gN + gCH3 -> gCH2NH 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gN + gCH3 -> CH2NH 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gN + gN -> gN2 									RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gN + gN -> N2 										RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gN + gNH -> gN2 + gH 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gN + gNH -> N2 + H 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gN + gNH2 -> gN2H2 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gN + gNH2 -> N2H2 									RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gN + gO -> gNO 									RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gN + gO -> NO 										RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gN + gHO2 -> gO2 + gNH 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gN + gHO2 -> O2 + NH 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gNH + gCH2 -> gCH2NH 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gNH + gCH2 -> CH2NH 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gNH + gCH2OH -> gHNCH2OH 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gNH + gCH2OH -> HNCH2OH 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gNH + gCH3 -> CH3NH 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gNH + gCH3 -> gCH3NH 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gNH + gCH3CHO -> gNH2 + gCH3CO 					RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gNH + gCH3CHO -> NH2 + CH3CO 						RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gNH + gCH3CO -> gCH3CONH 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gNH + gCH3CO -> CH3CONH 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gNH + gCH3O -> CH3ONH 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gNH + gCH3O -> gCH3ONH 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gNH + gHCOH -> gNH2CHO 							RTYPE=14 K1=0.0 K2=0.0 K3=0.0 #
 gNH + gHCOH -> NH2CHO 								RTYPE=14 K1=0.0 K2=0.0 K3=0.0 #
 gNH + gCO -> gHNCO 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gNH + gCO -> HNCO 									RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gNH + gCOCHO -> HNCOCHO 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gNH + gCOCHO -> gHNCOCHO 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gNH + gCOOH -> gHNCOOH 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gNH + gCOOH -> HNCOOH 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gNH + gH2CO -> NH2 + HCO 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gNH + gH2CO -> gNH2 + gHCO 						RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gNH + gHCO -> HNCHO 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gNH + gHCO -> gHNCHO 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gNH + gHNCO -> gHNCONH 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gNH + gHNCO -> HNCONH 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gNH + gNH -> gN2H2 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gNH + gNH -> N2H2 									RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gNH + gNH -> gN2 + gH2 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gNH + gNH -> N2 + H2 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gNH + gNH2 -> gNH2NH 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gNH + gNH2 -> NH2NH 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gNH + gNO -> N2 + O + H 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gNH + gNO -> gN2 + gO + gH 						RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gNH + gOH -> gHNOH 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gNH + gOH -> HNOH 									RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gNH + gCHOCHO -> HNCHO + HCO 						RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gNH + gCHOCHO -> gHNCHO + gHCO 					RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gNH + gNH2CO -> NH2CONH 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gNH + gNH2CO -> gNH2CONH 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gNH + gCH3OCO -> CH3OCONH 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gNH2 + gCH2OH -> NH2CH2OH 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gNH2 + gCH3 -> gCH3NH2 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gNH2 + gCH3 -> CH3NH2 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gNH2 + gCH3CHO -> gNH3 + gCH3CO 					RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gNH2 + gCH3CHO -> NH3 + CH3CO 						RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gNH2 + gCH3CHO -> gCH3NH2 + gHCO 					RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gNH2 + gCH3CHO -> gNH2CHO + gCH3 					RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gNH2 + gCH3CHO -> CH3NH2 + HCO 					RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gNH2 + gCH3CHO -> NH2CHO + CH3 					RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gNH2 + gCH3CO -> CH3CONH2 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gNH2 + gCH3O -> gNH2OCH3 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gNH2 + gCH3O -> NH2OCH3 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gNH2 + gCO -> gNH2CO 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gNH2 + gCO -> NH2CO 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gNH2 + gCOCHO -> NH2COCHO 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gNH2 + gCOOH -> NH2COOH 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gNH2 + gCOOH -> gNH2COOH 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gNH2 + gH2CO -> NH3 + HCO 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gNH2 + gH2CO -> NH2CHO + H 						RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gNH2 + gH2CO -> gNH2CHO + gH 						RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gNH2 + gH2CO -> gNH3 + gHCO 						RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gNH2 + gHCO -> gNH2CHO 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gNH2 + gHCO -> NH2CHO 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gNH2 + gHCOOCH3 -> NH3 + CH3OCO 					RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gNH2 + gHCOOCH3 -> gNH3 + gCH3OCO 					RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gNH2 + gHCOOH -> NH3 + COOH 						RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gNH2 + gHCOOH -> gNH3 + gCOOH 						RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gNH2 + gHNCO -> gNH2CONH 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gNH2 + gHNCO -> NH2CONH 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gNH2 + gNH2 -> NH2NH2 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gNH2 + gNH2 -> gNH2NH2 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gNH2 + gNH2CHO -> gNH3 + gNH2CO 					RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gNH2 + gNH2CHO -> NH3 + NH2CO 						RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gNH2 + gNO -> gH2O + gN2 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gNH2 + gNO -> H2O + N2 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gNH2 + gCHOCHO -> gNH2CHO + gHCO 					RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gNH2 + gCHOCHO -> NH2CHO + HCO 					RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gNH2 + gNH2CO -> NH2CONH2 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gO + gCH -> gHCO 									RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gO + gCH -> HCO 									RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gO + gCH2 -> gH2CO 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gO + gCH2 -> H2CO 									RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gO + gCH3 -> gCH3O 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gO + gCH3 -> CH3O 									RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gO + gCN -> gOCN 									RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gO + gCN -> OCN 									RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gO + gCO -> gCO2 									RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gO + gCO -> CO2 									RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gO + gHCO -> gCO2 + gH 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gO + gHCO -> CO2 + H 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gO + gHCO -> gCO + gOH 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gO + gHCO -> CO + OH 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gO + gHNO -> gNO + gOH 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gO + gHNO -> NO + OH 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gO + gHOC -> gCO + gOH 							RTYPE=14 K1=0.0 K2=0.0 K3=0.0 #
 gO + gHOC -> CO + OH 								RTYPE=14 K1=0.0 K2=0.0 K3=0.0 #
 gO + gHOC -> gCO2 + gH 							RTYPE=14 K1=0.0 K2=0.0 K3=0.0 #
 gO + gHOC -> CO2 + H 								RTYPE=14 K1=0.0 K2=0.0 K3=0.0 #
 gO + gNH -> gHNO 									RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gO + gNH -> HNO 									RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gO + gNH2 -> gHNO + gH 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gO + gNH2 -> HNO + H 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gO + gO -> gO2 									RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gO + gO -> O2 										RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gO + gO2 -> gO3 									RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gO + gO2 -> O3 									RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gO + gHO2 -> gO2 + gOH 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gO + gHO2 -> O2 + OH 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gO + gOH -> gHO2 									RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gO + gOH -> HO2 									RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gOH + gCH2 -> gCH2OH 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gOH + gCH2 -> CH2OH 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gOH + gCH2OH -> gHOCH2OH 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gOH + gCH2OH -> HOCH2OH 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gOH + gCH3 -> gCH3OH 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gOH + gCH3 -> CH3OH 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gOH + gCH3CHO -> gHCOOH + gCH3 					RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gOH + gCH3CHO -> CH3OH + HCO 						RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gOH + gCH3CHO -> HCOOH + CH3 						RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gOH + gCH3CHO -> gH2O + gCH3CO 					RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gOH + gCH3CHO -> H2O + CH3CO 						RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gOH + gCH3CHO -> gCH3OH + gHCO 					RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gOH + gCH3CO -> gCH3COOH 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gOH + gCH3CO -> CH3COOH 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gOH + gCH3O -> CH3OOH 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gOH + gCH3O -> gCH3OOH 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gOH + gCO -> gCOOH 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gOH + gCO -> COOH 									RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gOH + gCO -> gCO2 + gH 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gOH + gCO -> CO2 + H 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gOH + gCOCHO -> gHCOCOOH 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gOH + gCOCHO -> HCOCOOH 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gOH + gCOOH -> HOCOOH 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gOH + gCOOH -> gHOCOOH 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gOH + gH2CO -> HCOOH + H 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gOH + gH2CO -> HCO + H2O 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gOH + gH2CO -> gHCOOH + gH 						RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gOH + gH2CO -> gHCO + gH2O 						RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gOH + gHCO -> HCOOH 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gOH + gHCO -> gHCOOH 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gOH + gHCOOCH3 -> gHCOOH + gCH3O 					RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gOH + gHCOOCH3 -> gH2O + gCH3OCO 					RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gOH + gHCOOCH3 -> H2O + CH3OCO 					RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gOH + gHCOOCH3 -> HCOOH + CH3O 					RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gOH + gHCOOH -> H2O + COOH 						RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gOH + gHCOOH -> gH2O + gCOOH 						RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gOH + gHNCO -> gHNCOOH 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gOH + gHNCO -> HNCOOH 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gOH + gNH2 -> gNH2OH 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gOH + gNH2 -> NH2OH 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gOH + gNH2CHO -> gH2O + gNH2CO 					RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gOH + gNH2CHO -> HCOOH + NH2 						RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gOH + gNH2CHO -> gHCOOH + gNH2 					RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gOH + gNH2CHO -> H2O + NH2CO 						RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gOH + gOH -> H2O2 									RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gOH + gOH -> gH2O2 								RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gOH + gCHOCHO -> COCHO + H2O 						RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gOH + gCHOCHO -> HCOOH + HCO 						RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gOH + gCHOCHO -> gHCOOH + gHCO 					RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gOH + gCHOCHO -> gCOCHO + gH2O 					RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gOH + gNH2CO -> NH2COOH 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gOH + gNH2CO -> gNH2COOH 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gOH + gCH3OCO -> CH3OCOOH 							RTYPE=14 K1=1.0 K2=0.0 K3=0.0 #
 gC -> C 											RTYPE=15 K1=1.0 K2=0.0 K3=0.0 #
 gCH3CH2 -> CH3CH2 									RTYPE=15 K1=1.0 K2=0.0 K3=0.0 #
 gCH3CH3 -> CH3CH3 									RTYPE=15 K1=1.0 K2=0.0 K3=0.0 #
 gCH -> CH 											RTYPE=15 K1=1.0 K2=0.0 K3=0.0 #
 gCH2 -> CH2 										RTYPE=15 K1=1.0 K2=0.0 K3=0.0 #
 gCH2CO -> CH2CO 									RTYPE=15 K1=1.0 K2=0.0 K3=0.0 #
 gCH2NH -> CH2NH 									RTYPE=15 K1=1.0 K2=0.0 K3=0.0 #
 gCH2NH2 -> CH2NH2 									RTYPE=15 K1=1.0 K2=0.0 K3=0.0 #
 gCH2OH -> CH2OH 									RTYPE=15 K1=1.0 K2=0.0 K3=0.0 #
 gCH3 -> CH3 										RTYPE=15 K1=1.0 K2=0.0 K3=0.0 #
 gCH3CHO -> CH3CHO 									RTYPE=15 K1=1.0 K2=0.0 K3=0.0 #
 gCH3CN -> CH3CN 									RTYPE=15 K1=1.0 K2=0.0 K3=0.0 #
 gCH3CO -> CH3CO 									RTYPE=15 K1=1.0 K2=0.0 K3=0.0 #
 gCH3NH -> CH3NH 									RTYPE=15 K1=1.0 K2=0.0 K3=0.0 #
 gCH3NH2 -> CH3NH2 									RTYPE=15 K1=1.0 K2=0.0 K3=0.0 #
 gCH3O -> CH3O 										RTYPE=15 K1=1.0 K2=0.0 K3=0.0 #
 gCH3OCH3 -> CH3OCH3 								RTYPE=15 K1=1.0 K2=0.0 K3=0.0 #
 gCH3OH -> CH3OH 									RTYPE=15 K1=1.0 K2=0.0 K3=0.0 #
 gCH4 -> CH4 										RTYPE=15 K1=1.0 K2=0.0 K3=0.0 #
 gHCNH -> HCNH 										RTYPE=15 K1=1.0 K2=0.0 K3=0.0 #
 gHCOH -> HCOH 										RTYPE=15 K1=0.0 K2=0.0 K3=0.0 #
 gCN -> CN 											RTYPE=15 K1=1.0 K2=0.0 K3=0.0 #
 gCO -> CO 											RTYPE=15 K1=1.0 K2=0.0 K3=0.0 #
 gCO2 -> CO2 										RTYPE=15 K1=1.0 K2=0.0 K3=0.0 #
 gCOCHO -> COCHO 									RTYPE=15 K1=1.0 K2=0.0 K3=0.0 #
 gCOOH -> COOH 										RTYPE=15 K1=1.0 K2=0.0 K3=0.0 #
 gH -> H 											RTYPE=15 K1=1.0 K2=0.0 K3=0.0 #
 gH2 -> H2 											RTYPE=15 K1=1.0 K2=0.0 K3=0.0 #
 gH2CN -> H2CN 										RTYPE=15 K1=1.0 K2=0.0 K3=0.0 #
 gH2CO -> H2CO 										RTYPE=15 K1=1.0 K2=0.0 K3=0.0 #
 gH2O -> H2O 										RTYPE=15 K1=1.0 K2=0.0 K3=0.0 #
 gH2O2 -> H2O2 										RTYPE=15 K1=1.0 K2=0.0 K3=0.0 #
 gHCN -> HCN 										RTYPE=15 K1=1.0 K2=0.0 K3=0.0 #
 gHCO -> HCO 										RTYPE=15 K1=1.0 K2=0.0 K3=0.0 #
 gHCOOCH3 -> HCOOCH3 								RTYPE=15 K1=1.0 K2=0.0 K3=0.0 #
 gHCOOH -> HCOOH 									RTYPE=15 K1=1.0 K2=0.0 K3=0.0 #
 gCH3CONH -> CH3CONH 								RTYPE=15 K1=1.0 K2=0.0 K3=0.0 #
 gCH3COOH -> CH3COOH 								RTYPE=15 K1=1.0 K2=0.0 K3=0.0 #
 gHNC -> HNC 										RTYPE=15 K1=1.0 K2=0.0 K3=0.0 #
 gHNCHO -> HNCHO 									RTYPE=15 K1=1.0 K2=0.0 K3=0.0 #
 gHNCO -> HNCO 										RTYPE=15 K1=1.0 K2=0.0 K3=0.0 #
 gHNCOCHO -> HNCOCHO 								RTYPE=15 K1=1.0 K2=0.0 K3=0.0 #
 gHNCONH -> HNCONH 									RTYPE=15 K1=1.0 K2=0.0 K3=0.0 #
 gHNCOOH -> HNCOOH 									RTYPE=15 K1=1.0 K2=0.0 K3=0.0 #
 gHNO -> HNO 										RTYPE=15 K1=1.0 K2=0.0 K3=0.0 #
 gHNOH -> HNOH 										RTYPE=15 K1=1.0 K2=0.0 K3=0.0 #
 gCH3ONH -> CH3ONH 									RTYPE=15 K1=1.0 K2=0.0 K3=0.0 #
 gHNCH2OH -> HNCH2OH 								RTYPE=15 K1=1.0 K2=0.0 K3=0.0 #
 gHOC -> HOC 										RTYPE=15 K1=0.0 K2=0.0 K3=0.0 #
 gHOCOOH -> HOCOOH 									RTYPE=15 K1=1.0 K2=0.0 K3=0.0 #
 gHPO -> HPO 										RTYPE=15 K1=1.0 K2=0.0 K3=0.0 #
 gN -> N 											RTYPE=15 K1=1.0 K2=0.0 K3=0.0 #
 gN2 -> N2 											RTYPE=15 K1=1.0 K2=0.0 K3=0.0 #
 gN2H2 -> N2H2 										RTYPE=15 K1=1.0 K2=0.0 K3=0.0 #
 gN2O -> N2O 										RTYPE=15 K1=1.0 K2=0.0 K3=0.0 #
 gNH -> NH 											RTYPE=15 K1=1.0 K2=0.0 K3=0.0 #
 gNH2 -> NH2 										RTYPE=15 K1=1.0 K2=0.0 K3=0.0 #
 gNH2CHO -> NH2CHO 									RTYPE=15 K1=1.0 K2=0.0 K3=0.0 #
 gNH2CN -> NH2CN 									RTYPE=15 K1=1.0 K2=0.0 K3=0.0 #
 gNH2OH -> NH2OH 									RTYPE=15 K1=1.0 K2=0.0 K3=0.0 #
 gNH3 -> NH3 										RTYPE=15 K1=1.0 K2=0.0 K3=0.0 #
 gNO -> NO 											RTYPE=15 K1=1.0 K2=0.0 K3=0.0 #
 gNO2 -> NO2 										RTYPE=15 K1=1.0 K2=0.0 K3=0.0 #
 gO -> O 											RTYPE=15 K1=1.0 K2=0.0 K3=0.0 #
 gO2 -> O2 											RTYPE=15 K1=1.0 K2=0.0 K3=0.0 #
 gHO2 -> HO2 										RTYPE=15 K1=1.0 K2=0.0 K3=0.0 #
 gO3 -> O3 											RTYPE=15 K1=1.0 K2=0.0 K3=0.0 #
 gOCN -> OCN 										RTYPE=15 K1=1.0 K2=0.0 K3=0.0 #
 gOH -> OH 											RTYPE=15 K1=1.0 K2=0.0 K3=0.0 #
 gCHOCHO -> CHOCHO 									RTYPE=15 K1=1.0 K2=0.0 K3=0.0 #
 gHCOCOOH -> HCOCOOH 								RTYPE=15 K1=1.0 K2=0.0 K3=0.0 #
 gP -> P 											RTYPE=15 K1=1.0 K2=0.0 K3=0.0 #
 gPN -> PN 											RTYPE=15 K1=1.0 K2=0.0 K3=0.0 #
 gPO -> PO 											RTYPE=15 K1=1.0 K2=0.0 K3=0.0 #
 gNH2CO -> NH2CO 									RTYPE=15 K1=1.0 K2=0.0 K3=0.0 #
 gNH2CONH -> NH2CONH 								RTYPE=15 K1=1.0 K2=0.0 K3=0.0 #
 gNH2COOH -> NH2COOH 								RTYPE=15 K1=1.0 K2=0.0 K3=0.0 #
 gNH2NH -> NH2NH 									RTYPE=15 K1=1.0 K2=0.0 K3=0.0 #
 gNH2NH2 -> NH2NH2 									RTYPE=15 K1=1.0 K2=0.0 K3=0.0 #
 gNH2OCH3 -> NH2OCH3 								RTYPE=15 K1=1.0 K2=0.0 K3=0.0 #
 gCH3OCO -> CH3OCO 									RTYPE=15 K1=1.0 K2=0.0 K3=0.0 #
 gCH3OOH -> CH3OOH 									RTYPE=15 K1=1.0 K2=0.0 K3=0.0 #
 gHOCH2CO -> HOCH2CO 								RTYPE=15 K1=1.0 K2=0.0 K3=0.0 #
 gHOCH2OH -> HOCH2OH 								RTYPE=15 K1=1.0 K2=0.0 K3=0.0 #
 gC -> C 											RTYPE=16 K1=1.0 K2=0.0 K3=0.0 #
 gCH3CH2 -> CH3CH2 									RTYPE=16 K1=1.0 K2=0.0 K3=0.0 #
 gCH3CH3 -> CH3CH3 									RTYPE=16 K1=1.0 K2=0.0 K3=0.0 #
 gCH -> CH 											RTYPE=16 K1=1.0 K2=0.0 K3=0.0 #
 gCH2 -> CH2 										RTYPE=16 K1=1.0 K2=0.0 K3=0.0 #
 gCH2CO -> CH2CO 									RTYPE=16 K1=1.0 K2=0.0 K3=0.0 #
 gCH2NH -> CH2NH 									RTYPE=16 K1=1.0 K2=0.0 K3=0.0 #
 gCH2NH2 -> CH2NH2 									RTYPE=16 K1=1.0 K2=0.0 K3=0.0 #
 gCH2OH -> CH2OH 									RTYPE=16 K1=1.0 K2=0.0 K3=0.0 #
 gCH3 -> CH3 										RTYPE=16 K1=1.0 K2=0.0 K3=0.0 #
 gCH3CHO -> CH3CHO 									RTYPE=16 K1=1.0 K2=0.0 K3=0.0 #
 gCH3CN -> CH3CN 									RTYPE=16 K1=1.0 K2=0.0 K3=0.0 #
 gCH3CO -> CH3CO 									RTYPE=16 K1=1.0 K2=0.0 K3=0.0 #
 gCH3NH -> CH3NH 									RTYPE=16 K1=1.0 K2=0.0 K3=0.0 #
 gCH3NH2 -> CH3NH2 									RTYPE=16 K1=1.0 K2=0.0 K3=0.0 #
 gCH3O -> CH3O 										RTYPE=16 K1=1.0 K2=0.0 K3=0.0 #
 gCH3OCH3 -> CH3OCH3 								RTYPE=16 K1=1.0 K2=0.0 K3=0.0 #
 gCH3OH -> CH3OH 									RTYPE=16 K1=1.0 K2=0.0 K3=0.0 #
 gCH4 -> CH4 										RTYPE=16 K1=1.0 K2=0.0 K3=0.0 #
 gHCNH -> HCNH 										RTYPE=16 K1=1.0 K2=0.0 K3=0.0 #
 gHCOH -> HCOH 										RTYPE=16 K1=0.0 K2=0.0 K3=0.0 #
 gCN -> CN 											RTYPE=16 K1=1.0 K2=0.0 K3=0.0 #
 gCO -> CO 											RTYPE=16 K1=1.0 K2=0.0 K3=0.0 #
 gCO2 -> CO2 										RTYPE=16 K1=1.0 K2=0.0 K3=0.0 #
 gCOCHO -> COCHO 									RTYPE=16 K1=1.0 K2=0.0 K3=0.0 #
 gCOOH -> COOH 										RTYPE=16 K1=1.0 K2=0.0 K3=0.0 #
 gH -> H 											RTYPE=16 K1=1.0 K2=0.0 K3=0.0 #
 gH2 -> H2 											RTYPE=16 K1=1.0 K2=0.0 K3=0.0 #
 gH2CN -> H2CN 										RTYPE=16 K1=1.0 K2=0.0 K3=0.0 #
 gH2CO -> H2CO 										RTYPE=16 K1=1.0 K2=0.0 K3=0.0 #
 gH2O -> H2O 										RTYPE=16 K1=1.0 K2=0.0 K3=0.0 #
 gH2O2 -> H2O2 										RTYPE=16 K1=1.0 K2=0.0 K3=0.0 #
 gHCN -> HCN 										RTYPE=16 K1=1.0 K2=0.0 K3=0.0 #
 gHCO -> HCO 										RTYPE=16 K1=1.0 K2=0.0 K3=0.0 #
 gHCOOCH3 -> HCOOCH3 								RTYPE=16 K1=1.0 K2=0.0 K3=0.0 #
 gHCOOH -> HCOOH 									RTYPE=16 K1=1.0 K2=0.0 K3=0.0 #
 gCH3CONH -> CH3CONH 								RTYPE=16 K1=1.0 K2=0.0 K3=0.0 #
 gCH3COOH -> CH3COOH 								RTYPE=16 K1=1.0 K2=0.0 K3=0.0 #
 gHNC -> HNC 										RTYPE=16 K1=1.0 K2=0.0 K3=0.0 #
 gHNCHO -> HNCHO 									RTYPE=16 K1=1.0 K2=0.0 K3=0.0 #
 gHNCO -> HNCO 										RTYPE=16 K1=1.0 K2=0.0 K3=0.0 #
 gHNCOCHO -> HNCOCHO 								RTYPE=16 K1=1.0 K2=0.0 K3=0.0 #
 gHNCONH -> HNCONH 									RTYPE=16 K1=1.0 K2=0.0 K3=0.0 #
 gHNCOOH -> HNCOOH 									RTYPE=16 K1=1.0 K2=0.0 K3=0.0 #
 gHNO -> HNO 										RTYPE=16 K1=1.0 K2=0.0 K3=0.0 #
 gHNOH -> HNOH 										RTYPE=16 K1=1.0 K2=0.0 K3=0.0 #
 gCH3ONH -> CH3ONH 									RTYPE=16 K1=1.0 K2=0.0 K3=0.0 #
 gHNCH2OH -> HNCH2OH 								RTYPE=16 K1=1.0 K2=0.0 K3=0.0 #
 gHOC -> HOC 										RTYPE=16 K1=0.0 K2=0.0 K3=0.0 #
 gHOCOOH -> HOCOOH 									RTYPE=16 K1=1.0 K2=0.0 K3=0.0 #
 gHPO -> HPO 										RTYPE=16 K1=1.0 K2=0.0 K3=0.0 #
 gN -> N 											RTYPE=16 K1=1.0 K2=0.0 K3=0.0 #
 gN2 -> N2 											RTYPE=16 K1=1.0 K2=0.0 K3=0.0 #
 gN2H2 -> N2H2 										RTYPE=16 K1=1.0 K2=0.0 K3=0.0 #
 gN2O -> N2O 										RTYPE=16 K1=1.0 K2=0.0 K3=0.0 #
 gNH -> NH 											RTYPE=16 K1=1.0 K2=0.0 K3=0.0 #
 gNH2 -> NH2 										RTYPE=16 K1=1.0 K2=0.0 K3=0.0 #
 gNH2CHO -> NH2CHO 									RTYPE=16 K1=1.0 K2=0.0 K3=0.0 #
 gNH2CN -> NH2CN 									RTYPE=16 K1=1.0 K2=0.0 K3=0.0 #
 gNH2OH -> NH2OH 									RTYPE=16 K1=1.0 K2=0.0 K3=0.0 #
 gNH3 -> NH3 										RTYPE=16 K1=1.0 K2=0.0 K3=0.0 #
 gNO -> NO 											RTYPE=16 K1=1.0 K2=0.0 K3=0.0 #
 gNO2 -> NO2 										RTYPE=16 K1=1.0 K2=0.0 K3=0.0 #
 gO -> O 											RTYPE=16 K1=1.0 K2=0.0 K3=0.0 #
 gO2 -> O2 											RTYPE=16 K1=1.0 K2=0.0 K3=0.0 #
 gHO2 -> HO2 										RTYPE=16 K1=1.0 K2=0.0 K3=0.0 #
 gO3 -> O3 											RTYPE=16 K1=1.0 K2=0.0 K3=0.0 #
 gOCN -> OCN 										RTYPE=16 K1=1.0 K2=0.0 K3=0.0 #
 gOH -> OH 											RTYPE=16 K1=1.0 K2=0.0 K3=0.0 #
 gCHOCHO -> CHOCHO 									RTYPE=16 K1=1.0 K2=0.0 K3=0.0 #
 gHCOCOOH -> HCOCOOH 								RTYPE=16 K1=1.0 K2=0.0 K3=0.0 #
 gP -> P 											RTYPE=16 K1=1.0 K2=0.0 K3=0.0 #
 gPN -> PN 											RTYPE=16 K1=1.0 K2=0.0 K3=0.0 #
 gPO -> PO 											RTYPE=16 K1=1.0 K2=0.0 K3=0.0 #
 gNH2CO -> NH2CO 									RTYPE=16 K1=1.0 K2=0.0 K3=0.0 #
 gNH2CONH -> NH2CONH 								RTYPE=16 K1=1.0 K2=0.0 K3=0.0 #
 gNH2COOH -> NH2COOH 								RTYPE=16 K1=1.0 K2=0.0 K3=0.0 #
 gNH2NH -> NH2NH 									RTYPE=16 K1=1.0 K2=0.0 K3=0.0 #
 gNH2NH2 -> NH2NH2 									RTYPE=16 K1=1.0 K2=0.0 K3=0.0 #
 gNH2OCH3 -> NH2OCH3 								RTYPE=16 K1=1.0 K2=0.0 K3=0.0 #
 gCH3OCO -> CH3OCO 									RTYPE=16 K1=1.0 K2=0.0 K3=0.0 #
 gCH3OOH -> CH3OOH 									RTYPE=16 K1=1.0 K2=0.0 K3=0.0 #
 gHOCH2CO -> HOCH2CO 								RTYPE=16 K1=1.0 K2=0.0 K3=0.0 #
 gHOCH2OH -> HOCH2OH 								RTYPE=16 K1=1.0 K2=0.0 K3=0.0 #
 gCH -> gC + gH 									RTYPE=17 K1=7.3E+2 K2=0.0 K3=0.0 #
 gCH2 -> gCH + gH 									RTYPE=17 K1=5.0E+2 K2=0.0 K3=0.0 #
 gCH2CO -> gCH2 + gCO 								RTYPE=17 K1=9.15E+2 K2=0.0 K3=0.0 #
 gCH2NH -> gCH2 + gNH 								RTYPE=17 K1=4.98E+3 K2=0.0 K3=0.0 #
 gCH2NH2 -> gCH2 + gNH2 							RTYPE=17 K1=9.5E+3 K2=0.0 K3=0.0 #
 gCH2OH -> gCH2 + gOH 								RTYPE=17 K1=1.5E+3 K2=0.0 K3=0.0 #
 gCH3 -> gCH2 + gH 									RTYPE=17 K1=5.0E+2 K2=0.0 K3=0.0 #
 gCH3CHO -> gCH3 + gHCO 							RTYPE=17 K1=5.0E+2 K2=0.0 K3=0.0 #
 gCH3CN -> gCH3 + gCN 								RTYPE=17 K1=4.76E+3 K2=0.0 K3=0.0 #
 gCH3CO -> gCH3 + gCO 								RTYPE=17 K1=5.0E+2 K2=0.0 K3=0.0 #
 gCH3NH -> gCH2NH + gH 								RTYPE=17 K1=9.5E+3 K2=0.0 K3=0.0 #
 gCH3NH -> gCH3 + gNH 								RTYPE=17 K1=1.04 K2=0.0 K3=0.0 #
 gCH3NH2 -> gCH3 + gNH2 							RTYPE=17 K1=1.04 K2=0.0 K3=0.0 #
 gCH3O -> gCH3 + gO 								RTYPE=17 K1=3.0E+3 K2=0.0 K3=0.0 #
 gCH3OCH3 -> gCH3O + gCH3 							RTYPE=17 K1=1.72E+3 K2=0.0 K3=0.0 #
 gCH3OH -> gCH3 + gOH 								RTYPE=17 K1=1.5E+3 K2=0.0 K3=0.0 #
 gCH3OH -> gCH2OH + gH 								RTYPE=17 K1=5.0E+2 K2=0.0 K3=0.0 #
 gCH3OH -> gCH3O + gH 								RTYPE=17 K1=5.0E+2 K2=0.0 K3=0.0 #
 gCH4 -> gCH2 + gH2 								RTYPE=17 K1=2.34E+3 K2=0.0 K3=0.0 #
 gHCNH -> gCH + gNH 								RTYPE=17 K1=1.03 K2=0.0 K3=0.0 #
 gHCOH -> gCH + gOH 								RTYPE=17 K1=0.0 K2=0.0 K3=0.0 #
 gCN -> gC + gN 									RTYPE=17 K1=1.06E+4 K2=0.0 K3=0.0 #
 gCO -> gC + gO 									RTYPE=17 K1=5.0 K2=0.0 K3=0.0 #
 gCO2 -> gCO + gO 									RTYPE=17 K1=1.71E+3 K2=0.0 K3=0.0 #
 gCOCHO -> gHCO + gCO 								RTYPE=17 K1=5.0E+2 K2=0.0 K3=0.0 #
 gCOOH -> gCO + gOH 								RTYPE=17 K1=1.5E+3 K2=0.0 K3=0.0 #
 gH2CN -> gHCN + gH 								RTYPE=17 K1=1.03 K2=0.0 K3=0.0 #
 gH2CO -> gHCO + gH 								RTYPE=17 K1=1.33E+3 K2=0.0 K3=0.0 #
 gH2CO -> gCO + gH2 								RTYPE=17 K1=1.33E+3 K2=0.0 K3=0.0 #
 gH2O -> gOH + gH 									RTYPE=17 K1=9.7E+2 K2=0.0 K3=0.0 #
 gH2O2 -> gOH + gOH 								RTYPE=17 K1=1.5E+3 K2=0.0 K3=0.0 #
 gHCN -> gCN + gH 									RTYPE=17 K1=3.12E+3 K2=0.0 K3=0.0 #
 gHCO -> gCO + gH 									RTYPE=17 K1=4.21E+2 K2=0.0 K3=0.0 #
 gHCOOCH3 -> gHCO + gCH3O 							RTYPE=17 K1=5.0E+2 K2=0.0 K3=0.0 #
 gHCOOH -> gHCO + gOH 								RTYPE=17 K1=2.49E+2 K2=0.0 K3=0.0 #
 gCH3CONH -> gCH3CO + gNH 							RTYPE=17 K1=1.04 K2=0.0 K3=0.0 #
 gCH3CONH -> gCH3 + gHNCO 							RTYPE=17 K1=5.0E+2 K2=0.0 K3=0.0 #
 gCH3COOH -> gCH3 + gCOOH 							RTYPE=17 K1=5.0E+2 K2=0.0 K3=0.0 #
 gCH3COOH -> gCH3CO + gOH 							RTYPE=17 K1=1.5E+3 K2=0.0 K3=0.0 #
 gHNC -> gCN + gH 									RTYPE=17 K1=3.0E+3 K2=0.0 K3=0.0 #
 gHNCHO -> gNH + gHCO 								RTYPE=17 K1=1.04 K2=0.0 K3=0.0 #
 gHNCO -> gNH + gCO 								RTYPE=17 K1=6.0E+3 K2=0.0 K3=0.0 #
 gHNCOCHO -> gHNCO + gHCO 							RTYPE=17 K1=5.0E+2 K2=0.0 K3=0.0 #
 gHNCOCHO -> gNH + gCOCHO 							RTYPE=17 K1=1.04 K2=0.0 K3=0.0 #
 gHNCONH -> gNH + gHNCO 							RTYPE=17 K1=1.04 K2=0.0 K3=0.0 #
 gHNCOOH -> gCOOH + gNH 							RTYPE=17 K1=1.04 K2=0.0 K3=0.0 #
 gHNCOOH -> gOH + gHNCO 							RTYPE=17 K1=1.5E+3 K2=0.0 K3=0.0 #
 gHNO -> gNH + gO 									RTYPE=17 K1=1.03 K2=0.0 K3=0.0 #
 gHNO -> gNO + gH 									RTYPE=17 K1=1.03 K2=0.0 K3=0.0 #
 gHNOH -> gNH + gOH 								RTYPE=17 K1=1.03 K2=0.0 K3=0.0 #
 gCH3ONH -> gNH + gCH3O 							RTYPE=17 K1=1.03 K2=0.0 K3=0.0 #
 gHNCH2OH -> gNH + gCH2OH 							RTYPE=17 K1=1.04 K2=0.0 K3=0.0 #
 gHOC -> gCO + gH 									RTYPE=17 K1=0.0 K2=0.0 K3=0.0 #
 gHOCOOH -> gOH + gCOOH 							RTYPE=17 K1=1.5E+3 K2=0.0 K3=0.0 #
 gHPO -> gPO + gH 									RTYPE=17 K1=1.5E+3 K2=0.0 K3=0.0 #
 gN2 -> gN + gN 									RTYPE=17 K1=5.0 K2=0.0 K3=0.0 #
 gN2H2 -> gNH + gNH 								RTYPE=17 K1=2.0E+2 K2=0.0 K3=0.0 #
 gN2O -> gNO + gN 									RTYPE=17 K1=1.5E+3 K2=0.0 K3=0.0 #
 gNH -> gN + gH 									RTYPE=17 K1=5.0E+2 K2=0.0 K3=0.0 #
 gNH2 -> gNH + gH 									RTYPE=17 K1=8.0E+1 K2=0.0 K3=0.0 #
 gNH2CHO -> gNH2 + gHCO 							RTYPE=17 K1=3.0E+3 K2=0.0 K3=0.0 #
 gNH2CN -> gNH2 + gCN 								RTYPE=17 K1=9.5E+3 K2=0.0 K3=0.0 #
 gNH2OH -> gNH2 + gOH 								RTYPE=17 K1=3.0E+3 K2=0.0 K3=0.0 #
 gNH3 -> gNH + gH2 									RTYPE=17 K1=5.4E+2 K2=0.0 K3=0.0 #
 gNH3 -> gNH2 + gH 									RTYPE=17 K1=1.32E+3 K2=0.0 K3=0.0 #
 gNO -> gN + gO 									RTYPE=17 K1=4.82E+2 K2=0.0 K3=0.0 #
 gNO2 -> gNO + gO 									RTYPE=17 K1=1.5E+3 K2=0.0 K3=0.0 #
 gO2 -> gO + gO 									RTYPE=17 K1=7.5E+2 K2=0.0 K3=0.0 #
 gHO2 -> gO + gOH 									RTYPE=17 K1=7.5E+2 K2=0.0 K3=0.0 #
 gHO2 -> gO2 + gH 									RTYPE=17 K1=7.5E+2 K2=0.0 K3=0.0 #
 gO3 -> gO2 + gO 									RTYPE=17 K1=1.5E+3 K2=0.0 K3=0.0 #
 gOCN -> gCN + gO 									RTYPE=17 K1=1.5E+3 K2=0.0 K3=0.0 #
 gOH -> gO + gH 									RTYPE=17 K1=5.1E+2 K2=0.0 K3=0.0 #
 gCHOCHO -> gHCO + gHCO 							RTYPE=17 K1=5.0E+2 K2=0.0 K3=0.0 #
 gHCOCOOH -> gHCO + gCOOH 							RTYPE=17 K1=5.0E+2 K2=0.0 K3=0.0 #
 gHCOCOOH -> gCOCHO + gOH 							RTYPE=17 K1=1.5E+3 K2=0.0 K3=0.0 #
 gPN -> gP + gN 									RTYPE=17 K1=5.0E+2 K2=0.0 K3=0.0 #
 gPO -> gP + gO 									RTYPE=17 K1=5.0E+2 K2=0.0 K3=0.0 #
 gNH2CO -> gNH2 + gCO 								RTYPE=17 K1=1.04 K2=0.0 K3=0.0 #
 gNH2CONH -> gNH2 + gHNCO 							RTYPE=17 K1=1.04 K2=0.0 K3=0.0 #
 gNH2CONH -> gNH2CO + gNH 							RTYPE=17 K1=1.04 K2=0.0 K3=0.0 #
 gNH2COOH -> gNH2CO + gOH 							RTYPE=17 K1=1.5E+3 K2=0.0 K3=0.0 #
 gNH2COOH -> gNH2 + gCOOH 							RTYPE=17 K1=1.04 K2=0.0 K3=0.0 #
 gNH2NH -> gNH + gNH2 								RTYPE=17 K1=2.0E+2 K2=0.0 K3=0.0 #
 gNH2NH2 -> gNH2 + gNH2 							RTYPE=17 K1=2.0E+2 K2=0.0 K3=0.0 #
 gNH2OCH3 -> gNH2 + gCH3O 							RTYPE=17 K1=1.03 K2=0.0 K3=0.0 #
 gCH3OCO -> gCH3O + gCO 							RTYPE=17 K1=5.0E+2 K2=0.0 K3=0.0 #
 gCH3OOH -> gOH + gCH3O 							RTYPE=17 K1=1.03 K2=0.0 K3=0.0 #
 gHOCH2CO -> gCH2OH + gCO 							RTYPE=17 K1=5.0E+2 K2=0.0 K3=0.0 #
 gHOCH2OH -> gOH + gCH2OH 							RTYPE=17 K1=1.5E+3 K2=0.0 K3=0.0 #
 gCH2 -> gC + gH2 									RTYPE=18 K1=6.04E+1 K2=0.0 K3=0.0 #
 gCH2 -> gCH + gH 									RTYPE=18 K1=1.26E+2 K2=0.0 K3=0.0 #
 gCH2 -> gC + gH + gH 								RTYPE=18 K1=3.14E+2 K2=0.0 K3=0.0 #
 gCH2CO -> gCH2 + gCO 								RTYPE=18 K1=4.07E+2 K2=0.0 K3=0.0 #
 gCH3 -> gC + gH2 + gH 								RTYPE=18 K1=1.5E+2 K2=0.0 K3=0.0 #
 gCH3 -> gCH + gH + gH 								RTYPE=18 K1=8.0E+1 K2=0.0 K3=0.0 #
 gCH3 -> gCH + gH2 									RTYPE=18 K1=7.0E+1 K2=0.0 K3=0.0 #
 gCH3 -> gCH2 + gH 									RTYPE=18 K1=2.0E+2 K2=0.0 K3=0.0 #
 gCH3CHO -> gCH3 + gHCO 							RTYPE=18 K1=1.12E+3 K2=0.0 K3=0.0 #
 gCH3CN -> gCH3 + gCN 								RTYPE=18 K1=4.98E+2 K2=0.0 K3=0.0 #
 gCH3CN -> gCH2 + gHCN 								RTYPE=18 K1=7.47E+2 K2=0.0 K3=0.0 #
 gCH3NH2 -> gCH3 + gNH2 							RTYPE=18 K1=5.06E+2 K2=0.0 K3=0.0 #
 gCH3NH2 -> gCH2NH + gH2 							RTYPE=18 K1=5.06E+2 K2=0.0 K3=0.0 #
 gCH3OCH3 -> gCH3O + gCH3 							RTYPE=18 K1=1.13E+3 K2=0.0 K3=0.0 #
 gCH3OH -> gCH2 + gOH + gH 							RTYPE=18 K1=2.48E+1 K2=0.0 K3=0.0 #
 gCH3OH -> gH2CO + gH + gH 							RTYPE=18 K1=4.13E+1 K2=0.0 K3=0.0 #
 gCH3OH -> gCH3 + gOH 								RTYPE=18 K1=1.44E+3 K2=0.0 K3=0.0 #
 gCH3OH -> gCO + gH2 + gH + gH 						RTYPE=18 K1=1.65E+1 K2=0.0 K3=0.0 #
 gCH3OH -> gHCO + gH + gH + gH 						RTYPE=18 K1=1.65E+1 K2=0.0 K3=0.0 #
 gCO -> gO + gC 									RTYPE=18 K1=3.0 K2=0.0 K3=0.0 #
 gHCO -> gCO + gH 									RTYPE=18 K1=1.17E+3 K2=0.0 K3=0.0 #
 gHCOOH -> gCO2 + gH + gH 							RTYPE=18 K1=6.5E+2 K2=0.0 K3=0.0 #
 gHNO -> gNO + gH 									RTYPE=18 K1=1.03 K2=0.0 K3=0.0 #
 gNH2 -> gNH + gH 									RTYPE=18 K1=2.17E+2 K2=0.0 K3=0.0 #
 gNH2 -> gN + gH + gH 								RTYPE=18 K1=4.33E+2 K2=0.0 K3=0.0 #
 gNH3 -> gNH2 + gH 									RTYPE=18 K1=2.88E+2 K2=0.0 K3=0.0 #
 gNH3 -> gNH + gH + gH 								RTYPE=18 K1=2.88E+2 K2=0.0 K3=0.0 #
 gNO -> gN + gO 									RTYPE=18 K1=4.94E+2 K2=0.0 K3=0.0 #
 gO2 -> gO + gO 									RTYPE=18 K1=1.17E+2 K2=0.0 K3=0.0 #
 gCH -> gC + gH 									RTYPE=19 K1=1.4E-10 K2=0.0 K3=1.5 #
 gCH2 -> gCH + gH 									RTYPE=19 K1=5.0E-11 K2=0.0 K3=1.7 #
 gCH2CO -> gCH2 + gCO 								RTYPE=19 K1=9.04E-10 K2=0.0 K3=1.58 #
 gCH2NH -> gHCN + gH2 								RTYPE=19 K1=1.7E-9 K2=0.0 K3=1.63 #
 gCH2NH2 -> gCH2 + gNH2 							RTYPE=19 K1=1.0E-9 K2=0.0 K3=1.7 #
 gCH2OH -> gCH2 + gOH 								RTYPE=19 K1=5.0E-10 K2=0.0 K3=1.7 #
 gCH3 -> gCH + gH2 									RTYPE=19 K1=3.0E-11 K2=0.0 K3=1.7 #
 gCH3 -> gCH2 + gH 									RTYPE=19 K1=3.0E-11 K2=0.0 K3=1.7 #
 gCH3CHO -> gCH4 + gCO 								RTYPE=19 K1=3.43E-10 K2=0.0 K3=1.52 #
 gCH3CHO -> gCH3 + gHCO 							RTYPE=19 K1=3.43E-10 K2=0.0 K3=1.52 #
 gCH3CN -> gCH3 + gCN 								RTYPE=19 K1=1.56E-9 K2=0.0 K3=1.95 #
 gCH3CO -> gCH3 + gCO 								RTYPE=19 K1=9.0E-10 K2=0.0 K3=1.6 #
 gCH3NH -> gCH2NH + gH 								RTYPE=19 K1=1.0E-9 K2=0.0 K3=1.7 #
 gCH3NH -> gNH + gCH3 								RTYPE=19 K1=1.0E-9 K2=0.0 K3=1.7 #
 gCH3NH2 -> gHCN + gH2 + gH + gH 					RTYPE=19 K1=3.5E-10 K2=0.0 K3=1.73 #
 gCH3NH2 -> gCH3 + gNH2 							RTYPE=19 K1=1.55E-10 K2=0.0 K3=1.74 #
 gCH3NH2 -> gCH2NH + gH + gH 						RTYPE=19 K1=6.63E-11 K2=0.0 K3=1.51 #
 gCH3NH2 -> gCN + gH2 + gH2 + gH 					RTYPE=19 K1=9.42E-11 K2=0.0 K3=1.76 #
 gCH3O -> gCH3 + gO 								RTYPE=19 K1=5.0E-10 K2=0.0 K3=1.7 #
 gCH3OCH3 -> gCH3O + gCH3 							RTYPE=19 K1=9.0E-10 K2=0.0 K3=1.6 #
 gCH3OH -> gCH2OH + gH 								RTYPE=19 K1=5.0E-10 K2=0.0 K3=1.6 #
 gCH3OH -> gCH3O + gH 								RTYPE=19 K1=2.0E-10 K2=0.0 K3=1.6 #
 gCH3OH -> gCH3 + gOH 								RTYPE=19 K1=5.0E-10 K2=0.0 K3=1.7 #
 gCH4 -> gCH2 + gH2 								RTYPE=19 K1=4.8E-10 K2=0.0 K3=2.2 #
 gCH4 -> gCH + gH2 + gH 							RTYPE=19 K1=1.6E-10 K2=0.0 K3=2.2 #
 gCH4 -> gCH3 + gH 									RTYPE=19 K1=1.6E-10 K2=0.0 K3=2.2 #
 gHCNH -> gCH + gNH 								RTYPE=19 K1=1.0E-10 K2=0.0 K3=1.7 #
 gHCOH -> gCH + gOH 								RTYPE=19 K1=0.0 K2=0.0 K3=1.6 #
 gCN -> gC + gN 									RTYPE=19 K1=1.0E-9 K2=0.0 K3=2.8 #
 gCO -> gC + gO 									RTYPE=19 K1=3.1E-11 K2=0.0 K3=2.54 #
 gCO2 -> gCO + gO 									RTYPE=19 K1=3.13E-10 K2=0.0 K3=2.03 #
 gCOCHO -> gHCO + gCO 								RTYPE=19 K1=9.0E-10 K2=0.0 K3=1.6 #
 gCOOH -> gCO + gOH 								RTYPE=19 K1=1.0E-9 K2=0.0 K3=1.8 #
 gH2CN -> gHCN + gH 								RTYPE=19 K1=1.0E-9 K2=0.0 K3=1.7 #
 gH2CO -> gCO + gH2 								RTYPE=19 K1=4.4E-10 K2=0.0 K3=1.6 #
 gH2CO -> gCO + gH + gH 							RTYPE=19 K1=4.4E-10 K2=0.0 K3=1.6 #
 gH2O -> gOH + gH 									RTYPE=19 K1=3.28E-10 K2=0.0 K3=1.63 #
 gH2O2 -> gOH + gOH 								RTYPE=19 K1=8.3E-10 K2=0.0 K3=1.82 #
 gHCN -> gCN + gH 									RTYPE=19 K1=5.48E-10 K2=0.0 K3=2.0 #
 gHCO -> gH + gCO 									RTYPE=19 K1=5.87E-10 K2=0.0 K3=5.3E-1 #
 gHCOOCH3 -> gHCO + gCH3O 							RTYPE=19 K1=5.0E-10 K2=0.0 K3=1.7 #
 gHCOOH -> gHCO + gOH 								RTYPE=19 K1=2.75E-10 K2=0.0 K3=1.8 #
 gCH3CONH -> gCH3CO + gNH 							RTYPE=19 K1=1.0E-9 K2=0.0 K3=1.7 #
 gCH3CONH -> gCH3 + gHNCO 							RTYPE=19 K1=9.0E-10 K2=0.0 K3=1.6 #
 gCH3COOH -> gCH3CO + gOH 							RTYPE=19 K1=5.0E-10 K2=0.0 K3=1.7 #
 gCH3COOH -> gCH3 + gCOOH 							RTYPE=19 K1=9.0E-10 K2=0.0 K3=1.6 #
 gHNC -> gCN + gH 									RTYPE=19 K1=5.48E-10 K2=0.0 K3=2.0 #
 gHNCHO -> gNH + gHCO 								RTYPE=19 K1=1.0E-9 K2=0.0 K3=1.7 #
 gHNCO -> gNH + gCO 								RTYPE=19 K1=1.0E-9 K2=0.0 K3=1.7 #
 gHNCOCHO -> gHNCO + gHCO 							RTYPE=19 K1=9.0E-10 K2=0.0 K3=1.6 #
 gHNCOCHO -> gNH + gCOCHO 							RTYPE=19 K1=1.0E-9 K2=0.0 K3=1.7 #
 gHNCONH -> gNH + gHNCO 							RTYPE=19 K1=1.0E-9 K2=0.0 K3=1.7 #
 gHNCOOH -> gCOOH + gNH 							RTYPE=19 K1=1.0E-9 K2=0.0 K3=1.7 #
 gHNCOOH -> gOH + gHNCO 							RTYPE=19 K1=5.0E-10 K2=0.0 K3=1.7 #
 gHNO -> gNO + gH 									RTYPE=19 K1=1.7E-10 K2=0.0 K3=5.3E-1 #
 gHNOH -> gNH + gOH 								RTYPE=19 K1=1.0E-9 K2=0.0 K3=1.8 #
 gCH3ONH -> gNH + gCH3O 							RTYPE=19 K1=1.0E-9 K2=0.0 K3=1.8 #
 gHNCH2OH -> gNH + gCH2OH 							RTYPE=19 K1=1.0E-9 K2=0.0 K3=1.7 #
 gHOC -> gCO + gH 									RTYPE=19 K1=0.0 K2=0.0 K3=1.7 #
 gHOCOOH -> gOH + gCOOH 							RTYPE=19 K1=5.0E-10 K2=0.0 K3=1.7 #
 gHPO -> gPO + gH 									RTYPE=19 K1=1.0E-9 K2=0.0 K3=1.7 #
 gN2 -> gN + gN 									RTYPE=19 K1=5.0E-12 K2=0.0 K3=3.0 #
 gN2H2 -> gNH2 + gN 								RTYPE=19 K1=1.0E-9 K2=0.0 K3=1.7 #
 gN2O -> gN2 + gO 									RTYPE=19 K1=1.4E-9 K2=0.0 K3=1.7 #
 gNH -> gH + gN 									RTYPE=19 K1=4.0E-10 K2=0.0 K3=1.5 #
 gNH2 -> gNH + gH 									RTYPE=19 K1=2.11E-10 K2=0.0 K3=1.52 #
 gNH2CHO -> gNH2 + gHCO 							RTYPE=19 K1=1.0E-9 K2=0.0 K3=1.7 #
 gNH2CN -> gNH2 + gCN 								RTYPE=19 K1=1.0E-9 K2=0.0 K3=1.7 #
 gNH2OH -> gNH2 + gOH 								RTYPE=19 K1=1.0E-9 K2=0.0 K3=1.7 #
 gNH3 -> gNH + gH2 									RTYPE=19 K1=1.3E-10 K2=0.0 K3=1.91 #
 gNH3 -> gNH2 + gH 									RTYPE=19 K1=4.94E-10 K2=0.0 K3=1.65 #
 gNO -> gN + gO 									RTYPE=19 K1=3.0E-10 K2=0.0 K3=2.0 #
 gNO2 -> gNO + gO 									RTYPE=19 K1=1.29E-9 K2=0.0 K3=2.0 #
 gO2 -> gO + gO 									RTYPE=19 K1=3.3E-10 K2=0.0 K3=1.4 #
 gHO2 -> gOH + gO 									RTYPE=19 K1=5.0E-10 K2=0.0 K3=1.7 #
 gHO2 -> gO2 + gH 									RTYPE=19 K1=5.0E-10 K2=0.0 K3=1.7 #
 gO3 -> gO2 + gO 									RTYPE=19 K1=1.0E-9 K2=0.0 K3=1.7 #
 gOCN -> gO + gCN 									RTYPE=19 K1=1.0E-11 K2=0.0 K3=2.0 #
 gOH -> gO + gH 									RTYPE=19 K1=1.68E-10 K2=0.0 K3=1.66 #
 gCHOCHO -> gHCO + gHCO 							RTYPE=19 K1=9.0E-10 K2=0.0 K3=1.6 #
 gHCOCOOH -> gCOCHO + gOH 							RTYPE=19 K1=5.0E-10 K2=0.0 K3=1.7 #
 gHCOCOOH -> gHCO + gCOOH 							RTYPE=19 K1=9.0E-10 K2=0.0 K3=1.6 #
 gPN -> gN + gP 									RTYPE=19 K1=1.0E-9 K2=0.0 K3=1.7 #
 gPO -> gO + gP 									RTYPE=19 K1=1.0E-9 K2=0.0 K3=1.7 #
 gNH2CO -> gNH2 + gCO 								RTYPE=19 K1=1.0E-9 K2=0.0 K3=1.7 #
 gNH2CONH -> gNH2 + gHNCO 							RTYPE=19 K1=1.0E-9 K2=0.0 K3=1.7 #
 gNH2CONH -> gNH2CO + gNH 							RTYPE=19 K1=1.0E-9 K2=0.0 K3=1.7 #
 gNH2COOH -> gNH2 + gCOOH 							RTYPE=19 K1=1.0E-9 K2=0.0 K3=1.7 #
 gNH2COOH -> gNH2CO + gOH 							RTYPE=19 K1=5.0E-10 K2=0.0 K3=1.7 #
 gNH2NH -> gNH + gNH2 								RTYPE=19 K1=1.0E-11 K2=0.0 K3=3.0 #
 gNH2NH2 -> gNH2 + gNH2 							RTYPE=19 K1=1.0E-11 K2=0.0 K3=3.0 #
 gNH2OCH3 -> gNH2 + gCH3O 							RTYPE=19 K1=1.0E-9 K2=0.0 K3=1.8 #
 gCH3OCO -> gCH3O + gCO 							RTYPE=19 K1=5.0E-10 K2=0.0 K3=1.7 #
 gCH3OOH -> gOH + gCH3O 							RTYPE=19 K1=1.0E-9 K2=0.0 K3=1.8 #
 gHOCH2CO -> gCH2OH + gCO 							RTYPE=19 K1=9.0E-10 K2=0.0 K3=1.6 #
 gHOCH2OH -> gOH + gCH2OH 							RTYPE=19 K1=5.0E-10 K2=0.0 K3=1.7 #
 gCH -> gC + gH 									RTYPE=20 K1=2.9E-10 K2=0.0 K3=2.8 #
 gCH2 -> gC + gH2 									RTYPE=20 K1=1.21E-10 K2=0.0 K3=2.3 #
 gCH2 -> gCH + gH 									RTYPE=20 K1=2.51E-10 K2=0.0 K3=2.3 #
 gCH2 -> gC + gH + gH 								RTYPE=20 K1=6.28E-10 K2=0.0 K3=2.3 #
 gCH2CO -> gCH2 + gCO 								RTYPE=20 K1=1.15E-10 K2=0.0 K3=2.01 #
 gCH3 -> gH2 + gC + gH 								RTYPE=20 K1=3.0E-11 K2=0.0 K3=2.1 #
 gCH3 -> gCH + gH + gH 								RTYPE=20 K1=1.6E-11 K2=0.0 K3=2.1 #
 gCH3 -> gCH + gH2 									RTYPE=20 K1=1.4E-11 K2=0.0 K3=2.1 #
 gCH3 -> gCH2 + gH 									RTYPE=20 K1=4.0E-11 K2=0.0 K3=2.1 #
 gCH3CHO -> gCH3 + gHCO 							RTYPE=20 K1=2.6E-10 K2=0.0 K3=2.28 #
 gCH3CN -> gCH3 + gCN 								RTYPE=20 K1=1.18E-10 K2=0.0 K3=3.11 #
 gCH3CN -> gCH2 + gHCN 								RTYPE=20 K1=1.76E-10 K2=0.0 K3=3.11 #
 gCH3NH2 -> gCH3 + gNH2 							RTYPE=20 K1=1.3E-10 K2=0.0 K3=2.28 #
 gCH3NH2 -> gCH2NH + gH2 							RTYPE=20 K1=1.3E-10 K2=0.0 K3=2.28 #
 gCH3OCH3 -> gCH3O + gCH3 							RTYPE=20 K1=2.6E-10 K2=0.0 K3=2.28 #
 gCH3OH -> gCH3 + gOH 								RTYPE=20 K1=4.8E-10 K2=0.0 K3=2.57 #
 gH2CN -> gCN + gH + gH 							RTYPE=20 K1=3.98E-11 K2=0.0 K3=2.5 #
 gH2CN -> gHCN + gH 								RTYPE=20 K1=8.01E-11 K2=0.0 K3=2.5 #
 gH2CN -> gHNC + gH 								RTYPE=20 K1=8.01E-11 K2=0.0 K3=2.5 #
 gH2CO -> gHCO + gH 								RTYPE=20 K1=1.33E-11 K2=0.0 K3=2.8 #
 gH2CO -> gCO + gH + gH 							RTYPE=20 K1=6.67E-11 K2=0.0 K3=2.8 #
 gH2CO -> gCO + gH + gH 							RTYPE=20 K1=1.4E-11 K2=0.0 K3=3.1 #
 gH2O -> gH2 + gO 									RTYPE=20 K1=1.9E-12 K2=0.0 K3=3.1 #
 gH2O -> gOH + gH 									RTYPE=20 K1=4.2E-12 K2=0.0 K3=3.1 #
 gH2O -> gO + gH + gH 								RTYPE=20 K1=1.49E-11 K2=0.0 K3=3.1 #
 gHCO -> gCO + gH 									RTYPE=20 K1=2.46E-10 K2=0.0 K3=2.11 #
 gHCOOCH3 -> gCH3OH + gCO 							RTYPE=20 K1=1.0E-10 K2=0.0 K3=2.5 #
 gHCOOCH3 -> gCH3 + gCO2 + gH 						RTYPE=20 K1=1.0E-10 K2=0.0 K3=2.5 #
 gHCOOH -> gCO2 + gH + gH 							RTYPE=20 K1=1.73E-10 K2=0.0 K3=2.59 #
 gHPO -> gPO + gH 									RTYPE=20 K1=1.0E-10 K2=0.0 K3=2.5 #
 gNH -> gN + gH 									RTYPE=20 K1=1.0E-11 K2=0.0 K3=2.0 #
 gNH2 -> gNH + gH 									RTYPE=20 K1=5.77E-11 K2=0.0 K3=2.59 #
 gNH2 -> gN + gH + gH 								RTYPE=20 K1=1.15E-10 K2=0.0 K3=2.59 #
 gNH3 -> gNH2 + gH 									RTYPE=20 K1=6.2E-11 K2=0.0 K3=2.47 #
 gNH3 -> gNH + gH + gH 								RTYPE=20 K1=6.2E-11 K2=0.0 K3=2.47 #
 gNO -> gN + gO 									RTYPE=20 K1=2.0E-10 K2=0.0 K3=2.0 #
 gO2 -> gO + gO 									RTYPE=20 K1=6.2E-12 K2=0.0 K3=3.1 #
 gHO2 -> gO2 + gH 									RTYPE=20 K1=2.0E-10 K2=0.0 K3=2.5 #
 gOH -> gO + gH 									RTYPE=20 K1=1.6E-12 K2=0.0 K3=3.1 #
 gPN -> gP + gN 									RTYPE=20 K1=2.0E-10 K2=0.0 K3=2.5 #
 gPO -> gP + gO 									RTYPE=20 K1=2.0E-10 K2=0.0 K3=2.5 #
 C -> gC 											RTYPE=99 K1=1.0 K2=0.0 K3=0.0 #
 CH3CH2 -> gCH3CH2 									RTYPE=99 K1=1.0 K2=0.0 K3=0.0 #
 CH3CH3 -> gCH3CH3 									RTYPE=99 K1=1.0 K2=0.0 K3=0.0 #
 CH -> gCH 											RTYPE=99 K1=1.0 K2=0.0 K3=0.0 #
 CH2 -> gCH2 										RTYPE=99 K1=1.0 K2=0.0 K3=0.0 #
 CH2CO -> gCH2CO 									RTYPE=99 K1=1.0 K2=0.0 K3=0.0 #
 CH2NH -> gCH2NH 									RTYPE=99 K1=1.0 K2=0.0 K3=0.0 #
 CH2NH2 -> gCH2NH2 									RTYPE=99 K1=1.0 K2=0.0 K3=0.0 #
 CH2OH -> gCH2OH 									RTYPE=99 K1=1.0 K2=0.0 K3=0.0 #
 CH3 -> gCH3 										RTYPE=99 K1=1.0 K2=0.0 K3=0.0 #
 CH3CHO -> gCH3CHO 									RTYPE=99 K1=1.0 K2=0.0 K3=0.0 #
 CH3CN -> gCH3CN 									RTYPE=99 K1=1.0 K2=0.0 K3=0.0 #
 CH3CO -> gCH3CO 									RTYPE=99 K1=1.0 K2=0.0 K3=0.0 #
 CH3NH -> gCH3NH 									RTYPE=99 K1=1.0 K2=0.0 K3=0.0 #
 CH3NH2 -> gCH3NH2 									RTYPE=99 K1=1.0 K2=0.0 K3=0.0 #
 CH3O -> gCH3O 										RTYPE=99 K1=1.0 K2=0.0 K3=0.0 #
 CH3OCH3 -> gCH3OCH3 								RTYPE=99 K1=1.0 K2=0.0 K3=0.0 #
 CH3OH -> gCH3OH 									RTYPE=99 K1=1.0 K2=0.0 K3=0.0 #
 CH4 -> gCH4 										RTYPE=99 K1=1.0 K2=0.0 K3=0.0 #
 HCNH -> gHCNH 										RTYPE=99 K1=1.0 K2=0.0 K3=0.0 #
 HCOH -> gHCOH 										RTYPE=99 K1=0.0 K2=0.0 K3=0.0 #
 CN -> gCN 											RTYPE=99 K1=1.0 K2=0.0 K3=0.0 #
 CO -> gCO 											RTYPE=99 K1=1.0 K2=0.0 K3=0.0 #
 CO2 -> gCO2 										RTYPE=99 K1=1.0 K2=0.0 K3=0.0 #
 COCHO -> gCOCHO 									RTYPE=99 K1=1.0 K2=0.0 K3=0.0 #
 COOH -> gCOOH 										RTYPE=99 K1=1.0 K2=0.0 K3=0.0 #
 H -> gH 											RTYPE=99 K1=1.0 K2=0.0 K3=0.0 #
 H2 -> gH2 											RTYPE=99 K1=1.0 K2=0.0 K3=0.0 #
 H2CN -> gH2CN 										RTYPE=99 K1=1.0 K2=0.0 K3=0.0 #
 H2CO -> gH2CO 										RTYPE=99 K1=1.0 K2=0.0 K3=0.0 #
 H2O -> gH2O 										RTYPE=99 K1=1.0 K2=0.0 K3=0.0 #
 H2O2 -> gH2O2 										RTYPE=99 K1=1.0 K2=0.0 K3=0.0 #
 HCN -> gHCN 										RTYPE=99 K1=1.0 K2=0.0 K3=0.0 #
 HCO -> gHCO 										RTYPE=99 K1=1.0 K2=0.0 K3=0.0 #
 HCOOCH3 -> gHCOOCH3 								RTYPE=99 K1=1.0 K2=0.0 K3=0.0 #
 HCOOH -> gHCOOH 									RTYPE=99 K1=1.0 K2=0.0 K3=0.0 #
 CH3CONH -> gCH3CONH 								RTYPE=99 K1=1.0 K2=0.0 K3=0.0 #
 CH3COOH -> gCH3COOH 								RTYPE=99 K1=1.0 K2=0.0 K3=0.0 #
 HNC -> gHNC 										RTYPE=99 K1=1.0 K2=0.0 K3=0.0 #
 HNCHO -> gHNCHO 									RTYPE=99 K1=1.0 K2=0.0 K3=0.0 #
 HNCO -> gHNCO 										RTYPE=99 K1=1.0 K2=0.0 K3=0.0 #
 HNCOCHO -> gHNCOCHO 								RTYPE=99 K1=1.0 K2=0.0 K3=0.0 #
 HNCONH -> gHNCONH 									RTYPE=99 K1=1.0 K2=0.0 K3=0.0 #
 HNCOOH -> gHNCOOH 									RTYPE=99 K1=1.0 K2=0.0 K3=0.0 #
 HNO -> gHNO 										RTYPE=99 K1=1.0 K2=0.0 K3=0.0 #
 HNOH -> gHNOH 										RTYPE=99 K1=1.0 K2=0.0 K3=0.0 #
 CH3ONH -> gCH3ONH 									RTYPE=99 K1=1.0 K2=0.0 K3=0.0 #
 HNCH2OH -> gHNCH2OH 								RTYPE=99 K1=1.0 K2=0.0 K3=0.0 #
 HOC -> gHOC 										RTYPE=99 K1=0.0 K2=0.0 K3=0.0 #
 HOCOOH -> gHOCOOH 									RTYPE=99 K1=1.0 K2=0.0 K3=0.0 #
 HPO -> gHPO 										RTYPE=99 K1=1.0 K2=0.0 K3=0.0 #
 N -> gN 											RTYPE=99 K1=1.0 K2=0.0 K3=0.0 #
 N2 -> gN2 											RTYPE=99 K1=1.0 K2=0.0 K3=0.0 #
 N2H2 -> gN2H2 										RTYPE=99 K1=1.0 K2=0.0 K3=0.0 #
 N2O -> gN2O 										RTYPE=99 K1=1.0 K2=0.0 K3=0.0 #
 NH -> gNH 											RTYPE=99 K1=1.0 K2=0.0 K3=0.0 #
 NH2 -> gNH2 										RTYPE=99 K1=1.0 K2=0.0 K3=0.0 #
 NH2CHO -> gNH2CHO 									RTYPE=99 K1=1.0 K2=0.0 K3=0.0 #
 NH2CN -> gNH2CN 									RTYPE=99 K1=1.0 K2=0.0 K3=0.0 #
 NH2OH -> gNH2OH 									RTYPE=99 K1=1.0 K2=0.0 K3=0.0 #
 NH3 -> gNH3 										RTYPE=99 K1=1.0 K2=0.0 K3=0.0 #
 NO -> gNO 											RTYPE=99 K1=1.0 K2=0.0 K3=0.0 #
 NO2 -> gNO2 										RTYPE=99 K1=1.0 K2=0.0 K3=0.0 #
 O -> gO 											RTYPE=99 K1=1.0 K2=0.0 K3=0.0 #
 O2 -> gO2 											RTYPE=99 K1=1.0 K2=0.0 K3=0.0 #
 HO2 -> gHO2 										RTYPE=99 K1=1.0 K2=0.0 K3=0.0 #
 O3 -> gO3 											RTYPE=99 K1=1.0 K2=0.0 K3=0.0 #
 OCN -> gOCN 										RTYPE=99 K1=1.0 K2=0.0 K3=0.0 #
 OH -> gOH 											RTYPE=99 K1=1.0 K2=0.0 K3=0.0 #
 CHOCHO -> gCHOCHO 									RTYPE=99 K1=1.0 K2=0.0 K3=0.0 #
 HCOCOOH -> gHCOCOOH 								RTYPE=99 K1=1.0 K2=0.0 K3=0.0 #
 P -> gP 											RTYPE=99 K1=1.0 K2=0.0 K3=0.0 #
 PN -> gPN 											RTYPE=99 K1=1.0 K2=0.0 K3=0.0 #
 PO -> gPO 											RTYPE=99 K1=1.0 K2=0.0 K3=0.0 #
 NH2CO -> gNH2CO 									RTYPE=99 K1=1.0 K2=0.0 K3=0.0 #
 NH2CONH -> gNH2CONH 								RTYPE=99 K1=1.0 K2=0.0 K3=0.0 #
 NH2COOH -> gNH2COOH 								RTYPE=99 K1=1.0 K2=0.0 K3=0.0 #
 NH2NH -> gNH2NH 									RTYPE=99 K1=1.0 K2=0.0 K3=0.0 #
 NH2NH2 -> gNH2NH2 									RTYPE=99 K1=1.0 K2=0.0 K3=0.0 #
 NH2OCH3 -> gNH2OCH3 								RTYPE=99 K1=1.0 K2=0.0 K3=0.0 #
 CH3OCO -> gCH3OCO 									RTYPE=99 K1=1.0 K2=0.0 K3=0.0 #
 CH3OOH -> gCH3OOH 									RTYPE=99 K1=1.0 K2=0.0 K3=0.0 #
 HOCH2CO -> gHOCH2CO 								RTYPE=99 K1=1.0 K2=0.0 K3=0.0 #
 HOCH2OH -> gHOCH2OH 								RTYPE=99 K1=1.0 K2=0.0 K3=0.0 #

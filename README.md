GGackal
=======



### Description

This project aims to port the OSU gas-grain astrochemical code presented by Garrod et al. 2008 ([doi:10.1086/588035](http://doi.org/10.1086/588035)) to Python for greater portability/flexibility.

The short-term goal was to be able to return the same results as what is presented in the manuscript above.

The long-term goal was to be more flexible for updates to the physical/chemical model, and to provide new features that enable better exploration/analysis of the model results through polished interfaces and interactive visualizations.

A manuscript has also been published using this model and one of the contained reaction networks and binding energies, and I implore you to cite this article if you use any of the information from it:

* Laas and Caselli, "Modeling sulfur depletion in interstellar clouds", *Astro. & Astrophys.*, **2019**, *624*, A108, [doi:10.1051/0004-6361/201834446](https://doi.org/10.1051/0004-6361/201834446) (open access)



### License

This software falls under the MIT License (MIT)

Copyright (c) 2015-2019 Jacob C. Laas

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.



### Installation

Installation of the model itself is relatively simple. There are three basic modes that work relatively well:

##### "portable" mode

  In this way, GGackal may simply be downloaded and unpacked to a directory of your choice, and then its main executable file "gg_main.py" is called from a terminal. (optionally, you can add a symbolic link to gg_main.py to a place found in your executable PATH).
  
  All the input files reside in the original directory, and the model is ran one trial at a time with its output files directed elsewhere, to a directory specified in the parameter file.
  
  This was the original mode utilized during development and is the most well-tested.

##### python "package" mode

  In this way, GGackal may be downloaded and unpacked to a temporary folder (e.g. your "Downloads" folder), and then installed as a python package, so that it may be loaded as a python module, and possibly even accessed from an external python framework or user script. Installation to the system would be via the command "python setup.py install" (maybe you need to add "sudo"!), or it may be installed as a user-specific directory via "pip install --user .".
  
  To run it this way, one would have to then execute python, but something like "python -m ggackal check" from a terminal should work fine.

##### "development" mode

  The "development" mode is basically a combination of the two modes above, so that its files always reside in a specific/preferred directory, but it may also be accessed as a python module. Installation would be done via "python setup.py develop" or "pip install --user -e .". Then one can edit its source files and internal routines, but always use this modified version through python with an external package or user script.



### Usage

Just like the multiple installation modes possible, the model may also be run in different ways. The simplest method is to load a terminal and run a trial using "gg_main.py run" file.

The input files all named with the "gg_in_*" prefix, with "gg_in_params.yml" as the primary control interface.

The main executable file also expects some sort of input argument to direct it to do something. You may run a trial using "gg_main.py run", you may load a previous trial using "gg_main.py load SOME/DIR/ECT/ORY", etc. It is advised to begin with "gg_main.py --help" to see all the current options.



### Where to find it

GGackal is hosted at the following places:

* the private MPCDF GitLab repository:
	https://gitlab.mpcdf.mpg.de/jclaas/gg-jcl
* a public GitHub/BitBucket repository:
	https://bitbucket.org/notlaast/ggackal/



### Dependencies

Firstly, this program has only been tested on Ubuntu 14.04 and Ubuntu 16.04. It was primarily used with the native python installation on Ubuntu, but v3.0 saw a port to be compatible with Python 3, via Anaconda3 v5.2.0. This compatibility is a work in progress, but it might mean that you can run GGackal on any computer with a recent python distribution.

The following python packages/modules are absolutely required:

* yaml
* progressbar (v2+)
* Cython
* cytoolz
* numpy (v1.14+)
* scipy (v0.18+)
* symengine (only tested with v0.1, v0.3)
* sympy (only tested with v1.0-v1.1)
* matplotlib (v2.2+)

The following python packages/modules are optional:

* Tkinter (if the tk-based GUI is desired)
* pyqt4 (if the Qt-based GUI or plots are desired)
* gnuplot-py (preferred, for most plots)
* pyqtgraph (v0.10+, for qt-based plots, but doesn't support all types)
* networkx (if network graphs are desired)
* PIL (for the JAC adjacency plots, not working well!)
* cmocean (for a larger variety of color maps in plots)
* pygsl (additional ODE solvers, but slow and therefore untested)

The GUI also makes heavy use of gnuplot (v5+), which is virtually a required external package that is executable by the user.

Also, matplotlib is not utilized much (only for its colormap?), but v2.1 and its pyplot submodule was causing a segfault during loading. You could try:
```
>conda install matplotlib==2.2.3
	or
>pip install matplotlib==2.2.3
```
A newer version (v3) might also work, but it's untested and not a priority..

For e-mails upon trial completion (optional), there should also be a mail server running on 'localhost' (e.g. http://www.postfix.org). If you enable this feature and there's a problem, a print message in the terminal should provide more hints for this.

That's it!

##### Quick and easy installation of optional dependencies

This project has recently been updated to utilize more features of setuptools for easily managing/packaging/installing python packages. During installation using the "pip" method, you can also specify what type of extra packages to install:

* To install *all* the optional packages (and also GGackal itself):  
    ```>pip install --user -e '.[full]'```

* or only a subset of the extra packages:
```
    >pip install --user -e '.[gui]'
    >pip install --user -e '.[plot]'
    >pip install --user -e '.[ode]'
```


##### Manual installation of optional dependencies

Note that a full anaconda installation (v5.2+) will already come preinstalled with most of the packages above, except the following (including their source):

* tk (only conda)
* progressbar (pip or conda-forge)
* symengine (pip or conda-forge)
* cmocean (pip or conda-forge)
* gnuplot-py (only via pip)    *note*: if using py3, use this one: https://github.com/oblalex/gnuplot.py-py3k.git

In a nutshell, you can try the following series of commands to set them up:
```
>conda install tk
>pip install progressbar
>pip install symengine
>pip install cmocean
```




### Development

The development was performed solely by Jacob C. Laas (jclaas@mpe.mpg.de) prior to its public release. At the present time (early-2019), there are no other developers. All correspondence should be directed here.

A general guide (i.e. notes to myself) to the code design/layout can be found at http://cas-ws02.mpe.mpg.de/trac/gg14/wiki/DevPractices.

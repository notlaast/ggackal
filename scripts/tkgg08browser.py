#!/usr/bin/python

import subprocess
try:
	import Tkinter as tk	## Python 2.x
except ImportError:
	import tkinter as tk	## Python 3.x

###
# define parameters
fin_output = 'ggo_output.d'
fin_order = 'ggo_order.d'
codename = "gg08"
defaultiterations = [1,2,34,50,72,80]

###
# define global containers
fabunds = {}
iterinfo = {}
orderinfo = {}

###
# define helper routines

# for sending any command string to a shell
def toshell(shellcommand):
	r"""
	Defines a pipe via the subprocess module to send a shell command.
	
	INPUTS:
	- ``shellcommand`` -- any command that could be run directly from command line
	
	EXAMPLES:
	- toshell('reset') -- resets the terminal screen
	- toshell('ls -l') -- lists files in the working directory
	
	TODO:
	- do something with the return values of spawned process (success vs failure?)
	"""
	return subprocess.Popen(shellcommand,shell=True,stdout=subprocess.PIPE).communicate()[0]

# for plotting in gnuplot
def gnuplotsolutions(xdict,ydict):
	r"""
	Uses the gnuplot to plot time evolutions.
	
	INPUTS:
	- ``xdict`` -- a dictionary containing a single key/val pair, where
			the key is simply the string to use for the x-axis, and
			the val is a list of x-values to serve as the x-axis
	- ``ydict`` -- a dictionary containing key/val pairs, where
			the key are strings to serve as the label for each set of values to plot,
			and each val is a list of y-values
			Note: the length of the y-values must all match the x-axis
	"""
	import Gnuplot
	Gnuplot.GnuplotOpts.gnuplot_command = '/usr/bin/gnuplot --persist'
	gplot = Gnuplot.Gnuplot()
	gplot('set term wxt')
	# initialize temp file
	import tempfile
	f = tempfile.NamedTemporaryFile(mode='w',delete=False)
	filename1 = f.name
	plotlines = []
	# prepare x-axis
	if not len(xdict.keys()) == 1:
		raise ValueError("it appears that you tried to input more than one x-axis: %s" % xdict.keys())
	xlabel = xdict.keys()[0]
	xlabel_string = '{}'.format(xlabel)
	for xval in xdict[xlabel]:
		plotlines.append(str(xval))
	# append y-values as space-separated y1,y2,y3,y4.. points
	plotcommand = "plot "
	los = ydict.keys()
	for n,s in enumerate(los):
		if not len(ydict[s]) == len(xdict[xlabel]):
			raise ValueError("the length of the data series does not appear to match the x-axis' length")
		for i,yval in enumerate(ydict[s]):
			plotlines[i] += ", " + str(yval)
		plotcommand += '"' + filename1 + '" using 1:' + str(n+2) + ' title "' + s + '" with lines, '
	# write to temp file
	for l in plotlines:
		f.write('%s\n' % l)
	f.close()
	# do plots
	gplot('set logscale xy')
	gplot('set offset graph 0.05, graph 0.05, graph 0.05, graph 0.05')
	gplot('set format x "10^{%T}"')
	gplot('set xlabel "'+xlabel_string+'"')
	gplot('set xtics in nomirror')
	gplot('set format y "10^{%T}"')
	gplot('set key auto')
	plotcommand = plotcommand[:-2]
	print "gnuplot command is:\n\t", plotcommand
	gplot(plotcommand)

###
# define child windows

# species lister
def showspecieslist(species):
	specieslist = []
	for s in species:
		specieslist.append(s)
	# set window properties
	win_specieslist = tk.Toplevel()
	win_specieslist.title(codename+"-browser: Species List")
	# create scrollbar and listbox for species list
	speciesscrollbar = tk.Scrollbar(win_specieslist)
	speciesscrollbar.pack(side=tk.RIGHT, fill=tk.Y)
	specieslistbox = tk.Listbox(win_specieslist,
		selectmode=tk.EXTENDED,
		exportselection=0,
		height=40)
	speciesscrollbar.config(command=specieslistbox.yview)
	specieslistbox.config(yscrollcommand=speciesscrollbar.set)
	for s in specieslist:
		specieslistbox.insert(tk.END,s)
	specieslistbox.pack(fill=tk.BOTH, expand=tk.YES)
	# make time plot
	def plottimeplot():
		selectedspecies = [specieslist[int(item)] for item in specieslistbox.curselection()]
		print "will plot:", selectedspecies
		times = []
		for i in sorted(iterinfo.keys()):
			yr = float(iterinfo[i]['t'].rstrip(' yr'))
			times.append(yr)
		xdict = {"Time (yr)": times}
		ydict = {}
		for s in selectedspecies:
			ydict[s] = fabunds[s]
		gnuplotsolutions(xdict,ydict)
	tk.Button(win_specieslist,
		text="Plot Time Evolution",
		command=plottimeplot).pack()
	# show time evolution
	def showtimeevol():
		# generate xy data
		selectedspecies = [specieslist[int(item)] for item in specieslistbox.curselection()]
		xdict = {"iter": range(1,83)}
		ydict = {}
		for s in selectedspecies:
			ydict[s] = fabunds[s]
		# append to list of text lines for viewing
		texttoshow = [""]
		for key,l in xdict.items():
			texttoshow[0] += '{:<10}'.format(str(key))
			for index,val in enumerate(l):
				linenum = index + 1
				# try to insert entry to text list
				try:
					texttoshow[linenum] += '{:<10}'.format(str(val))
				# otherwise just grow text length with new line+entry
				except IndexError:
					texttoshow.append('{:<10}'.format(str(val)))
		for key,l in ydict.items():
			texttoshow[0] += '{:<15}'.format(str(key))
			for index,val in enumerate(l):
				linenum = index + 1
				# try to insert entry to text list
				try:
					texttoshow[linenum] += '{:<15}'.format(str(val))
				# otherwise just grow text length with new line+entry
				except IndexError:
					texttoshow.append('{:<15}'.format(str(val)))
		showgeneralviewer(texttoshow,wintitle="Time Evolution")
	tk.Button(win_specieslist,
		text="Show Time Evolution",
		command=showtimeevol).pack()

# species lister
def showorderbrowser(iterations,species):
	# set window properties
	win_orderbrowser = tk.Toplevel()
	win_orderbrowser.title(codename+"-browser: Order Browser")
	# define frames to hold various components
	textframe = tk.Frame(win_orderbrowser)
	textframe.pack(side=tk.LEFT,fill=tk.BOTH,expand=tk.YES)
	browserframe = tk.Frame(win_orderbrowser)
	browserframe.pack(side=tk.RIGHT,fill=tk.BOTH,expand=tk.YES)
	listframe = tk.Frame(browserframe)
	listframe.pack(side=tk.TOP,fill=tk.BOTH,expand=tk.YES)
	iterframe = tk.Frame(listframe)
	iterframe.pack(side=tk.LEFT,fill=tk.BOTH,expand=tk.YES)
	speciesframe = tk.Frame(listframe)
	speciesframe.pack(side=tk.LEFT,fill=tk.BOTH,expand=tk.YES)
	# create text box for order info
	textbox = tk.Text(textframe,
		height=45, width=115,
		exportselection=0,
		wrap=tk.NONE)
	textyscrollbar = tk.Scrollbar(textframe,orient=tk.VERTICAL)
	textyscrollbar.pack(side=tk.RIGHT, fill=tk.Y)
	textxscrollbar = tk.Scrollbar(textframe,orient=tk.HORIZONTAL)
	textxscrollbar.pack(side=tk.BOTTOM, fill=tk.X)
	textbox.config(xscrollcommand=textxscrollbar.set)
	textbox.config(yscrollcommand=textyscrollbar.set)
	textxscrollbar.config(command=textbox.xview)
	textyscrollbar.config(command=textbox.yview)
	textbox.pack(fill=tk.BOTH, expand=tk.YES)
	# create scrollbar and listbox for iteration list
	iterscrollbar = tk.Scrollbar(iterframe)
	iterscrollbar.pack(side=tk.RIGHT, fill=tk.Y)
	iterlistbox = tk.Listbox(iterframe,
		selectmode=tk.EXTENDED,
		exportselection=0,
		height=40)
	iterscrollbar.config(command=iterlistbox.yview)
	iterlistbox.config(yscrollcommand=iterscrollbar.set)
	for i in iterations:
		iterlistbox.insert(tk.END,str(i))
	iterlistbox.pack(fill=tk.BOTH, expand=tk.YES)
	# create scrollbar and listbox for species list
	speciesscrollbar = tk.Scrollbar(speciesframe)
	speciesscrollbar.pack(side=tk.RIGHT, fill=tk.Y)
	specieslistbox = tk.Listbox(speciesframe,
		selectmode=tk.EXTENDED,
		exportselection=0,
		height=40)
	for s in species:
		specieslistbox.insert(tk.END,s)
	specieslistbox.pack(fill=tk.BOTH, expand=tk.YES)
	specieslistbox.config(yscrollcommand=speciesscrollbar.set)
	speciesscrollbar.config(command=specieslistbox.yview)
	# add update button
	def updateorder():
		textbox.delete(1.0,tk.END)
		# identify selected iterations
		selectediters = [iterations[int(item)] for item in iterlistbox.curselection()]
		if len(selectediters)==0:
			selectediters = defaultiterations
		# identify selected species
		selectedspecies = [species[int(item)] for item in specieslistbox.curselection()]
		if len(selectedspecies)==0:
			selectedspecies = list(species)
		# load them into the textbox
		for i in selectediters:
			textbox.insert(tk.END, "="*100 + "\n")
			textbox.insert(tk.END, "Showing order info for iteration "+str(i)+":\n")
			for s in selectedspecies:
				textbox.insert(tk.END, "-"*50 + "\n")
				textbox.insert(tk.END, orderinfo[i][s] + "\n")
	tk.Button(browserframe,
		text='Update',
		command=updateorder).pack(side=tk.BOTTOM)

# general text viewer
def showgeneralviewer(texttoshow,wintitle="Text Viewer"):
	# set window properties
	win_textviewer = tk.Toplevel()
	win_textviewer.title(codename+"-browser: "+wintitle)
	# determine text width
	width = len(texttoshow[0])
	if width > 150: width = 150
	text = '\n'.join(texttoshow)
	# create text box and scrollbars
	textbox = tk.Text(win_textviewer,
		height=45, width=width,
		exportselection=0,
		wrap=tk.NONE)
	textyscrollbar = tk.Scrollbar(win_textviewer,orient=tk.VERTICAL)
	textyscrollbar.pack(side=tk.RIGHT, fill=tk.Y)
	textxscrollbar = tk.Scrollbar(win_textviewer,orient=tk.HORIZONTAL)
	textxscrollbar.pack(side=tk.BOTTOM, fill=tk.X)
	textbox.config(xscrollcommand=textxscrollbar.set)
	textbox.config(yscrollcommand=textyscrollbar.set)
	textxscrollbar.config(command=textbox.xview)
	textyscrollbar.config(command=textbox.yview)
	# fill text box with text
	textbox.insert(tk.END,text)
	textbox.pack(fill=tk.BOTH, expand=tk.YES)

###
# begin main program

toshell('reset')

# define main window
win_master = tk.Tk()
win_master.title(codename+"-browser")

# parent directory chooser
pwdframe = tk.Frame(win_master)
pwdframe.pack(side=tk.TOP)
tk.Label(pwdframe, text="Choose directory").pack(side=tk.LEFT)
pwdstring = tk.Entry(pwdframe)
pwdstring.insert(0,'')
pwdstring.pack(side=tk.LEFT)
def selectpwd():
	import tkFileDialog
	directory = tkFileDialog.askdirectory()
	directory += "/"
	pwdstring.delete(0,tk.END)
	pwdstring.insert(0,directory)
tk.Button(pwdframe,
	text='Browse',
	command=selectpwd).pack(side=tk.LEFT)

# line between directory chooser and output browser
line = tk.Frame(win_master,height=1,bg="black")
line.pack(fill=tk.X,side=tk.TOP)

# load output
outputframe = tk.Frame(win_master)
outputframe.pack(side=tk.TOP)
def loadoutput():
	print "="*50
	# clear the global container of abundances
	fabunds.clear()
	# load file
	prefix = pwdstring.get()
	fin_fname = prefix + fin_output
	import os.path
	if not os.path.isfile(fin_fname):
		raise IOError('%s does not appear to exist' % fin_fname)
	with open(fin_fname, 'r+') as fin:
		fin_contents = fin.read()
	fin_contents = fin_contents.splitlines()
	print "found", len(fin_contents), "lines"
	# keep track of contents
	location = "header"
	current_species = []
	# walk through contents
	iternum = 0
	for n,line in enumerate(fin_contents):
		linenum = n+1
		# remove padded whitespace and skip blank lines
		line = line.strip()
		if len(line) == 0: continue
		# check something about header
		if location == "header":
			if line == "RESULTS FOR DEPTH POINT  1/ 1":
				print "found iterinfo beginning at line", linenum
				location = "iterinfo"
				continue
			else:
				continue
		elif location == "iterinfo":
			if line == "INITIAL VALUE   FINAL VALUE   FINAL % ERROR":
				print "found totabundinfo beginning at line", linenum
				location = "totabundinfo"
				continue
			else:
				thisiter = {}
				items = line.split(",")
				for entry in items:
					key,val = entry.split('=')
					thisiter[key.strip()] = val.strip().replace('D','E')
				iternum = int(thisiter['IT'])
				del thisiter['IT']
				iterinfo[iternum] = thisiter
		elif location == "totabundinfo":
			if line == "TIME EVOLUTION OF FRACTIONAL ABUNDANCE":
				print "found specabundinfo beginning at line", linenum
				location = "specabundinfo"
				continue
			else:
				continue
		elif location == "specabundinfo":
			if line == "TIME EVOLUTION OF FRACTIONAL ABUNDANCE":
				continue
			elif line == "FINAL CHEMICAL COMPOSITION OF CLOUD MODEL":
				print "found finabundinfo beginning at line", linenum
				location = "finabundinfo"
				continue
			else:
				items = line.split()
				if items[0]=="IT":
					iternum = 0
					# make note of current species and create empty lists for their fabunds
					current_species = items[3:]
					# create empty lists for each species
					for species in current_species:
						fabunds[species] = []
				else:
					iternum += 1
					if not (iternum in iterinfo.keys()):
						thisiter = {}
						iterinfo[iternum] = {'t': items[2].replace('D','E')}
					for val_enum,val in enumerate(items[3:]):
						species = current_species[val_enum]
						fabunds[species].append(val.replace('D','E'))
		elif location == "finabundinfo":
			continue
	# report species info
	allspecies = fabunds.keys()
	print "found", len(allspecies), "species"
	print "="*50
tk.Button(outputframe,
	text='Load ggo_output.d',
	command=loadoutput).pack(side=tk.LEFT,padx=10,pady=10)
# view iteration information
def browseiters():
	iterations = sorted(iterinfo.keys())
	if len(iterations)==0:
		raise IOError("it appears that you haven't loaded the output file %s yet" % fin_output)
	texttoshow = [" iter"]
	# populate header
	for key in sorted(iterinfo[1].keys()):
		texttoshow[0] += '{:^15}'.format(key)
	# add lines with values
	for i in iterations:
		line = '{:>4}'.format(str(i))
		for key in sorted(iterinfo[i].keys()):
			line += '{:^15}'.format(iterinfo[i][key])
		texttoshow.append(line)
	showgeneralviewer(texttoshow,wintitle="Iteration Info")
tk.Button(outputframe,
	text='View Iteration Info',
	command=browseiters).pack(side=tk.TOP,padx=10,pady=10)
# browse species
def browseallspecies():
	allspecies = sorted(fabunds.keys(),reverse=False)
	if len(allspecies)==0:
		raise IOError("it appears that you haven't loaded the output file %s yet" % fin_output)
	showspecieslist(allspecies)
tk.Button(outputframe,
	text='Browse all species',
	command=browseallspecies).pack(side=tk.TOP,padx=10,pady=10)

# line between output and order browsing options
line = tk.Frame(win_master,height=1,bg="black")
line.pack(fill=tk.X,side=tk.TOP)

# load order
orderframe = tk.Frame(win_master)
orderframe.pack(side=tk.TOP)
def loadorder():
	print "="*50
	# clear the global container of abundances
	orderinfo.clear()
	# load file
	prefix = pwdstring.get()
	fin_fname = prefix + fin_order
	import os.path
	if not os.path.isfile(fin_fname):
		raise IOError('%s does not appear to exist' % fin_fname)
	with open(fin_fname, 'r+') as fin:
		fin_contents = fin.read()
	fin_contents = fin_contents.splitlines()
	print "found", len(fin_contents), "lines"
	# walk through contents
	currentiteration = 0
	currentspecies = ""
	for n,line in enumerate(fin_contents):
		linenum = n+1
		# remove padded whitespace and skip blank lines
		strippedline = line.strip()
		if len(strippedline) == 0: continue
		elif line[1]=="-": continue
		# split by whitespace for easy processing
		components = strippedline.split()
		# identify iteration number
		if components[0]=='MOST':
			components = line.split(',')
			thisnewiter = components[1]
			thisnewiter = thisnewiter.split('=')
			thisnewiter = int(thisnewiter[1].strip())
			currentiteration = thisnewiter
			print "found beginning of iteration", currentiteration, "on line", linenum
			orderinfo[currentiteration] = {}
		else:
			if components[1][0]=="-":
				currentspecies = components[1].lstrip("-")
				orderinfo[currentiteration][currentspecies] = line + "\n"
			else:
				orderinfo[currentiteration][currentspecies] += line + "\n"
	iterations = sorted(orderinfo.keys())
	species = sorted(orderinfo[iterations[0]].keys())
	print "found the following iterations in the order file:", iterations
	print "found the following species in the order file:", species
	print "="*50
tk.Button(orderframe,
	text='Load Order',
	command=loadorder).pack(side=tk.LEFT,padx=10,pady=10)
# browse order
def browseorder():
	iterations = sorted(orderinfo.keys())
	species = sorted(orderinfo[iterations[0]].keys())
	showorderbrowser(iterations,species)
tk.Button(orderframe,
	text='Browse Order',
	command=browseorder).pack(side=tk.TOP,padx=10,pady=10)
# print list of reaction numbers
def showincludedrxns():
	import re
	def getnumtot(line):
		if line[0:2]==' *':
			numtot = int(line[3:7].strip())
			return numtot
		else:
			return False
	includedrxns = []
	iterations = sorted(orderinfo.keys())
	species = sorted(orderinfo[iterations[0]].keys())
	for i in iterations[:1]:
		for s in species:
			for line in orderinfo[i][s].split('\n'):
				numtot = getnumtot(line)
				if numtot: includedrxns.append(numtot)
	includedrxns = sorted(list(set(includedrxns)))
	print "found", len(includedrxns), "rxns:", includedrxns
tk.Button(orderframe,
	text='List Reactions (1st iter)',
	command=showincludedrxns).pack(side=tk.TOP,padx=10,pady=10)

###
# finally load gui
win_master.mainloop()


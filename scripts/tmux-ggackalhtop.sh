#!/bin/sh

# create session (killing previous, if it exists)
tmux kill-session -t ggackalhtop
tmux new-session -d -s ggackalhtop
# split twice, to give three panes
tmux split-window -v -p 40
tmux split-window -v -p 25
# set up top pane
tmux select-pane -t 1
tmux send-keys "htop -u $USER" C-m
# set up middle pane
tmux select-pane -t 2
tmux send-keys "watch -n0.5 ./scripts/watchggackal.sh" C-m
# activate bottom pane, as a blank terminal
tmux select-pane -t 3
# activate the session
tmux attach -t ggackalhtop

#!/bin/bash

# determine sage processes
PROCESSES=`ps a | grep -v "grep" | grep -E 'python.*(ggackal|gg_main).*'`

# report findings
echo found:
echo "$PROCESSES"

# set the internal field separator to newline characters
OIFS=$IFS
IFS='\n'

# pull PID from process list
PIDS=()
for p in "$PROCESSES"
do
	pid=`echo $p | sed -r 's/([0-9]+) .*/\1/g'`
	PIDS+=($pid)
done
PIDS=`echo $PIDS | tr '\n' ' '`
echo " " # for some reason, now the space must be added, to print new line
echo PIDs are:
echo $PIDS

# return to the old IFS
IFS=$OIFS

# print pstree
echo 
echo process trees are:
for p in $PIDS
do
	pstree -p $p
done

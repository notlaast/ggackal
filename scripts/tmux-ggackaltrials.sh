#!/bin/sh
echo "./gg_main.py run" > ~/.history/ggackal
echo "./gg_main.py load ../output/trials/" >> ~/.history/ggackal
echo "./gg_main.py comp gg_in_network_origS.d gg_in_network_newS.d --reportlatex '/home/jake/ownCloud/projects/sulfur depletion/manuscript/networkcomp.tex'" >> ~/.history/ggackal
export HISTFILE="~/.history/ggackal"

# create session (killing previous, if it exists)
tmux kill-session -t ggackaltrials
tmux new-session -d -s ggackaltrials
# note that environment variables are "forgotten" for new tmux sessions
export HISTFILE="~/.history/ggackal"
tmux setenv HISTFILE ~/.history/ggackal
# rename first pane to "stage1"
tmux rename-window "stage1"
# new window "stage2"
tmux new-window
tmux rename-window "stage2"
# new window "stage3"
tmux new-window
tmux rename-window "stage3"
tmux split-window -v -p 80
tmux split-window -v -p 25
# new window "networkcomp"
tmux new-window
tmux rename-window "networkcomp"
# new window "manuscript"
tmux new-window -c "/home/jake/ownCloud/projects/sulfur depletion/submitted4"
tmux rename-window "manuscript"
# new window "ipython"
tmux new-window
tmux rename-window "ipython"
# new window "qtfit"
tmux new-window
tmux rename-window "qtfit"
# activate the session
tmux attach -t ggackaltrials
#!/bin/sh
echo "./gg_main.py" > ~/.history/ggackal
export HISTFILE=~/.history/ggackal

# create session (killing previous, if it exists)
tmux kill-session -t ggackaltrials
tmux new-session -d -s ggackaltrials
# activate the session
tmux attach -t ggackaltrials

#!/usr/bin/perl -w

# DESCRIPTION:
#	then performs species renaming of the top section (as a separate file!)
#	of the gg_surf_bigmols_july-2008.d surface data file
#	*note* assumes that the dictionary file is the same as for the full network renaming

use strict;
use Tie::File;

###
# INPUT PARAMETERS

my $dict = 'species dictionary.txt';
if (! -e "$dict") {
	print "could not find file $dict!\n";
	exit 0;
}

my $inputfname = 'gg_surf_re_species_july-2008.d';
if (! -e "$inputfname") {
	print "could not find file $inputfname!\n";
	exit 0;
}

my $outputfname = 'gg_surf_jcl_species_july-2008.d';
if (-e "$outputfname") {
	print "already found a new network file; will delete it!\n";
	unlink($outputfname);
}

###
# SUBROUTINES



###
# MAIN ROUTINE

# open dict file and load contents into memory
open(DICT, "<", $dict);
my @LoDict;
my $linenum = 0;
while(<DICT>) {
	$linenum++;
	next if $linenum == 1;		# skip first line
	next if $_ eq "\n";			# skip empty lines
	next if $_ =~ m/^\#.*/;		# skip if line is commented out
	next if $_ =~ m/^.*(\{|\}).*$/;	# very strictly skip line with braces
	
	chomp($_);
	push @LoDict, $_;
}
close(DICT);
@LoDict = sort @LoDict;

# check number of dictionary entries for sanity
my $nums = $#LoDict + 1;
print "found $nums species in the dictionary\n";

# load entire network file into memory
open(INPUT, "<", $inputfname);
my $fullinput = do { local $/; <INPUT> };
close(INPUT);

# create new instance of the network
my $fulloutput = $fullinput;

# walk through each dictionary entry
my $counter = 1;
foreach (@LoDict) {
	
	# copy entry
	my $entry = $_;
	
	# split the line by tab characters
	my @line = split(/\s+/, $_);
	
	# identify the original species name
	my $old = "$line[0]";
	
	# identify the new species name
	my $new = "$line[1]";
	
	# print out species, for sanity...
	print "$counter: '$old' -> '$new'\n";
	
	# globally search and replace across the network
	$fulloutput =~ s/  \Q$old\E /$new /g;
	
	# manual hacks (which seem to be species which did not make the final cut for the reaction network)
	$fulloutput =~ s/  JN2H /gN2H /g;		# unsure why it's not in the network...
	$fulloutput =~ s/  JHON /gHON /g;
	$fulloutput =~ s/  JCNH2 /gCNH2 /g;
	$fulloutput =~ s/  JC3H5 /gC3H5 /g;
	$fulloutput =~ s/  JC3H6 /gC3H6 /g;
	$fulloutput =~ s/  JC3H7 /gC3H7 /g;
	$fulloutput =~ s/  JC3H8 /gC3H8 /g;
	$fulloutput =~ s/  JC4H5 /gC4H5 /g;
	$fulloutput =~ s/  JC4H6 /gC4H6 /g;
	$fulloutput =~ s/  JNHNO /gNHNO /g;		# to eventually remove
	$fulloutput =~ s/  JNH2NO /gNH2NO /g;	# unsure why it's not in the network...
	
	$counter++;
}

open(OUTPUT, ">", $outputfname);
print OUTPUT $fulloutput;
close(OUTPUT);

# now just fix the spacing of the initial column...
tie my @fullfile, 'Tie::File', "$outputfname";
foreach my $line (@fullfile) {					# loops through the file, line-by-line
	my $oldstring = "";
	my $newstring = "";
	if ($line =~ m/^(\w+\s+)\d/) {
#		print "found $1\n";
		$oldstring = $1;
		$newstring = sprintf '%-20s', $oldstring;
	}
	$line =~ s/$oldstring/$newstring/g;
}
untie @fullfile;


#!/usr/bin/perl -w

# DESCRIPTION:
#	checks the species dictionary against the original list of species, to make to make sure none are missed
#	then performs the full species renaming of a network file, to a new network file

use POSIX;

###
# INPUT PARAMETERS
$list = 'allspecies.txt';
if (! -e "$list") {
	print "could not find file $list!\n";
	exit 0;
}

$dict = 'species_dictionary.txt';
if (! -e "$dict") {
	print "could not find file $dict!\n";
	exit 0;
}

$surfrxns = 'gg_surf_re_rxns_july-2008.d';
$surfrxnslimit = 343;
if (! -e "$surfrxns") {
	print "could not find file $surfrxns!\n";
	exit 0;
}

$network = 'gg_reac_bigmols_osu2005_4-6-2007_re.d';
if (! -e "$network") {
	print "could not find file $network!\n";
	exit 0;
}

$newnetwork = 'gg_network_jcl.d';
if (-e "$newnetwork") {
	print "already found a new network file; will delete it!\n";
	unlink($newnetwork);
}

$headerlength=837;

$tabwidth = 4;


###
# SUBROUTINES

my @LoSpecies;
sub checkLoSpecies {
	$s = $_[0];
	
	my $index = 0;
	foreach $entry (@LoSpecies) {
		if ($s eq $entry) {
			splice(@LoSpecies, $index, 1);
			next;
		}
		$index++;
	}
}

sub trim {
	my $s = shift;
	$s =~ s/^\s+|\s+$//g;
	return $s;
}

sub getnum {
	use POSIX qw(strtod);
	my $str = shift;
	$str =~ s/^\s+//;
	$str =~ s/\s+$//;
	$! = 0;
	my($num, $unparsed) = strtod($str);
	if (($str eq '') || ($unparsed != 0) || $!) {
		return;
	} else {
		return $num;
	} 
} 

sub is_numeric { defined scalar &getnum }

sub cleank {
	my $k = trim($_[0]);
	if (! is_numeric($k)) {
		print "ERROR: $k does not appear to be a posix-accepted numeric\n";
		exit 0;
	}
	if ($k eq "0.00E+00") {return "0.0";}
	elsif ($k eq "0.") {return "0.0";}
	elsif ($k eq "-5.00E-01") {return "-0.5";}
	else {return $k;}
}


###
# MAIN ROUTINE

# open species list and load all species into a list
open(LIST, "<", $list);
$linenum = 0;
while(<LIST>) {
	$linenum++;
	next if $linenum == 1;		# skip first line
	next if $_ eq "\n";			# skip empty lines
	
	$s = $_;
	chomp($s);
	$s =~ s/^(\S+)\s+.*/$1/;
	push @LoSpecies, $s;
}
close(LIST);

# sort this list of species by name, to make searching faster below..
@LoSpecies = sort @LoSpecies;

# check number of species for sanity
$nums = $#LoSpecies + 1;
print "found $nums species in the species list\n";

# open dict file and load contents into memory
open(DICT, "<", $dict);
my @LoDict;
$linenum = 0;
while(<DICT>) {
	$linenum++;
	next if $linenum == 1;		# skip first line
	next if $_ eq "\n";			# skip empty lines
	next if $_ =~ m/^\#.*/;		# skip if line is commented out
	next if $_ =~ m/^.*(\{|\}).*$/;	# very strictly skip line with braces
	
	chomp($_);
	push @LoDict, $_;
}
close(DICT);
@LoDict = sort @LoDict;

# check number of dictionary entries for sanity
$nums = $#LoDict + 1;
print "found $nums species in the dictionary\n";

# compare each entry in the dictionary against the species list
print "will now compare the dictionary against the species list\n";
foreach (@LoDict) {
	$s = $_;
	$s =~ s/(\S+)\t.*/$1/;
	&checkLoSpecies($s);
}

# check if there are any remaining species in the list
# 	(which means they were left out of the process of populating the species dictionary!)
if (! $LoSpecies[0] eq "") {
	print "\nthere seems to be at least one species missing from the dictionary:\n";
	foreach (@LoSpecies) {
		print $_ . "\n";
	}
}

# generate dictionary hash, such that $hash{$oldname}=$newname
my %HoSpecies = ();
foreach (@LoDict) {
	my @tokens = split /\s+/, $_;
	$HoSpecies{$tokens[0]} = $tokens[1];
}

# load entire surface reaction file into array
open(SURFRXNS, "<", $surfrxns) or die "Can't open $surfrxns for read: $!";
my @surfrxnsfile;
while (<SURFRXNS>) {
	push (@surfrxnsfile, $_);
}
close(SURFRXNS);
# remove header section
shift @surfrxnsfile;
# generate hash of surface rxn barriers
my %HoSurfRxnsEa = ();
my %HoSurfRxnsComment = ();
my $counter = 0;
foreach (@surfrxnsfile) {
	# get entries
	my($r1, $r2, $p1, $p2, $p3, $rest) = unpack('a8 a8 a8 a8 a8 a34', $_);
	$r1 = trim($r1);
	$r2 = trim($r2);
	$p1 = trim($p1);
	$p2 = trim($p2);
	$p3 = trim($p3);
	# generate reaction string
	my $reaction = $HoSpecies{$r1};
	if (not $r2 eq "") {
		$reaction .= " + $HoSpecies{$r2}";
	} else {
		print "ERROR: could not identify a second reactant in this line: $_\n";
		exit 0;
	}
	$reaction .= " -> $HoSpecies{$p1}";
	if (not $p2 eq "") {$reaction .= " + $HoSpecies{$p2}";}
	if (not $p3 eq "") {$reaction .= " + $HoSpecies{$p3}";}
	# process the barrier & comment
	$rest = trim($rest);
	my($ea, $comment) = split /\s+/, $rest, 2;
	if (! $comment) {$comment = "";}
	# populate hash of surf rxns
	my @val;
	$HoSurfRxnsEa{$reaction} = cleank($ea);
	$HoSurfRxnsComment{$reaction} = $comment;
	# done
	$counter += 1;
	if ($counter >= $surfrxnslimit) {last;}
}

# load entire network file into array
open(NETWORK, "<", $network) or die "Can't open $network for read: $!";
my @fullnetworkfile;
while (<NETWORK>) {
	push (@fullnetworkfile, $_);
}
close(NETWORK);
@tempnetwork = @fullnetworkfile;
# remove header section
for (my $i=0; $i < $headerlength; $i += 1) {
	shift @tempnetwork;
}

# keep track of longest line
$longestline = 55;

# load each line
$counter = 0;
foreach (@tempnetwork) {
	# get entries
	my($r1, $r2, $r3, $p1, $p2, $p3, $p4, $k1, $k2, $k3, $rtype, $waste, $rnum) = unpack(' a8 a8 a8 a8 a8 a8 a8 a17 a9 a9 a2 a15 a4', $_);
	$r1 = trim($r1);
	$r2 = trim($r2);
	$r3 = trim($r3);
	$p1 = trim($p1);
	$p2 = trim($p2);
	$p3 = trim($p3);
	$p4 = trim($p4);
	$k1 = cleank($k1);
	$k2 = cleank($k2);
	$k3 = cleank($k3);
	$rnum = trim($rnum);
	$comment = "gg08_rnum=$rnum";
	$rtype = trim($rtype);
	# generate reaction string
	my $reaction = $HoSpecies{$r1};
	if (not $r2 eq "") {$reaction .= " + $HoSpecies{$r2}";}
	if (not $r3 eq "") {$reaction .= " + $HoSpecies{$r3}";}
	$reaction .= " -> $HoSpecies{$p1}";
	if (not $p2 eq "") {$reaction .= " + $HoSpecies{$p2}";}
	if (not $p3 eq "") {$reaction .= " + $HoSpecies{$p3}";}
	if (not $p4 eq "") {$reaction .= " + $HoSpecies{$p4}";}
	# fix activation barriers for surface reactions
	if ($rtype eq "14") {
		if (exists $HoSurfRxnsEa{$reaction}) {
			$k3 = $HoSurfRxnsEa{$reaction};
			$ref = trim($HoSurfRxnsComment{$reaction});
			if ($ref eq "") {$comment .= ", gg08_EA_ref=null";}
			else {$comment .= ", gg08_EA_ref='$ref'";}
		}
	}
	# define the kinetics string
	my $kinetics = "RTYPE=$rtype K1=$k1 K2=$k2 K3=$k3 #";
	if (not $comment eq "") {$kinetics .= " $comment";}
	# pad the reaction string with tabs
	my $rxnlen = length $reaction;
	if ($rxnlen > $longestline) {$longestline = $rxnlen;}
	my $numtabs = ceil(($longestline-($rxnlen-$rxnlen%$tabwidth))/$tabwidth);
	$reaction .= "\t"x$numtabs;
	# define the actual line
	my $line = $reaction . $kinetics . "\n";
	print "$line";
	$tempnetwork[$counter] = $line;
	
	$counter += 1;
}

# write to tempfile
open(NETWORK, ">>", $newnetwork);
foreach (@tempnetwork) {
	print NETWORK $_;
}
close(NETWORK);

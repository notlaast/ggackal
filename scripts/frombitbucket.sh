#!/bin/sh

### basic pull command
#git pull origin

### update remote
git fetch update

### loops through all local branches and rebases them on origin's
branches=`git branch -l | sed 's/\*//'`
for b in $branches; do
	echo updating $b
	git checkout $b
	git branch --set-upstream $b origin/$b
	git pull origin
done

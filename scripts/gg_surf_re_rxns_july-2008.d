343                                    EA(K)
JC      JC      JC2                      0.         AR                    *
JC      JC2     JC3                      0.         AR                    *
JC      JC2H    JC3H                     0.         AR(exactly)           *
JC      JC2H3   JC3H3                    0.         AR                    *
JC      JC2N    JC3N                     0.            EH                 *
JC      JC2O    JC3O                     0.         AR                    *
JC      JC2S    JC3S                     0.            EH                 *
JC      JC3     JC4                      0.            EH                 *
JC      JC3H    JC4H                     0.            EH                 *
JC      JC4     JC5                      0.            EH                 *
JC      JC4H    JC5H                     0.            hasegawa           *
JC      JC5     JC6                      0.            EH                 *
JC      JC5H    JC6H                     0.            hasegawa           *
JC      JC6     JC7                      0.            EH                 *
JC      JC6H    JC7H                     0.            hasegawa           *
JC      JC7     JC8                      0.            EH                 *
JC      JC7H    JC8H                     0.            hase               *
JC      JC8     JC9                      0.            EH                 *
JC      JC8H    JC9H                     0.            hase               *
JC      JCH     JC2H                     0.         AR                    *
JC      JCH2    JC2H2                    0.         AR  (H2CC)            *
JC      JCH3    JC2H3                    0.         AR                    *
JC      JCN     JC2N                     0.         AR                    *
JC      JHS     JCS     JH               0.         M84                   *
JC      JN      JCN                      0.         AR                    *
JC      JNH     JHNC                     0.         AR                    *
JC      JNH2    JHNC    JH               0.            EH                 *
JC      JNO     JCN     JO               0.          From our gas network *
JC      JNO     JOCN                     0.         AR (CNO)              *
JC      JNS     JCN     JS               0.         M84                   *
JC      JO      JCO                      0.         AR                    *
JC      JO2     JCO     JO               0.00E+00   AR                    *
JC      JOCN    JCO     JCN              0.         M84                   *
JC      JOH     JCO     JH               0.            EH (Mitchell?)     *
JC      JS      JCS                      0.            EH                 *
JC      JSO     JCO     JS               0.         M84                   *
JCH     JC2     JC3H                     0.         AR                    *
JCH     JC2H    JC3H2                    0.         AR                    *
JCH     JC2H3   JC3H4                    0.         AR (isomer)           *
JCH     JC3     JC4H                     0.            EH                 *
JCH     JC3H    JC4H2                    0.            hasegawa           *
JCH     JC4     JC5H                     0.            EH                 *
JCH     JC4H    JC5H2                    0.            hasegawa           *
JCH     JC5     JC6H                     0.            EH                 *
JCH     JC5H    JC6H2                    0.            hasegawa           *
JCH     JC6     JC7H                     0.            EH                 *
JCH     JC6H    JC7H2                    0.            hasegawa           *
JCH     JC7     JC8H                     0.            EH                 *
JCH     JC7H    JC8H2                    0.            hasegawa           *
JCH     JC8     JC9H                     0.            EH                 *
JCH     JC8H    JC9H2                    0.            hasegawa           *
JCH     JCH     JC2H2                    0.00E+00   AR                    *
JCH     JCH2    JC2H3                    0.         AR                    *
JCH     JCH3    JC2H4                    0.         AR (isomer)           *
JCH     JCN     JHCCN                    0.         AR                    *
JCH     JHNO    JNO     JCH2             0.         M84(West.Ea=0-+2500K) *
JCH     JNH     JCHNH                    0.         AR                    *
JCH     JNH     JHCN    JH               0.           Hase                *
JCH     JNH     JHNC    JH               0.           Hase                *
JCH     JNH2    JCH2NH                   0.         hase -   DPR was CH2NH*
JCH     JNO     JHCN    JO               0.         M84                   *
JCH     JO2     JHCO    JO               0.         AR                    *
JCH2    JCH2    JC2H4                    0.         AR                    *
JCH2    JCH3    JC2H5                    0.         AR                    *
JCH2    JCN     JCH2CN                   0.0        AR (CH2CN)            *
JCH2    JHNO    JCH3    JNO              0.         Westley Ea=0.0-+2500K *
JCH2    JNH2    JCH2NH2                  0.         AR (implemented)      *
JCH2    JO2     JH2CO   JO               0.         AR                    *
JCH3    JC3N    JCH3C3N                  0.                     hasegawa  *
JCH3    JC5N    JCH3C5N                  0.                     hasegawa  *
JCH3    JC7N    JCH3C7N                  0.                     hasegawa  *
JCH3    JCH2OH  JC2H5OH                  0.         AR                    *
JCH3    JCH3    JC2H6                    0.         AR                    *
JCH3    JCN     JCH3CN                   0.         AR (CH3CN)            *
JCH3    JHCO    JCH3CHO                  0.         Makes sense(hasegawa) *
JCH3    JHNO    JCH4    JNO              0.         Westley Ea=0. -+2500K *
JCH4    JC2H    JC2H2   JCH3             2.50E+02   Mitchell 84           *
JH      JC      JCH                      0.         AR                    *
JH      JC2     JC2H                     0.         AR                    *
JH      JC2H    JC2H2                    0.         AR                    *
JH      JC2H2   JC2H3                    1.21E+03 C=_C triple bond HASE   *
JH      JC2H3   JC2H4                    0.         AR                    *
JH      JC2H4   JC2H5                    7.50E+02 C=C  double bond HASE   *
JH      JC2H5   JC2H6                    0.         AR                    *
JH      JC2H6   JC2H5   JH2              4.89E+03   TranFaradSco 65,2116  *
JH      JC2N    JHCCN                    0.         AR                    *
JH      JC2O    JHC2O                    0.         AR                    *
JH      JC3     JC3H                     0.            EH                 *
JH      JC3H    JC3H2                    0.         AR                    *
JH      JC3H2   JC3H3                    1.21E+03 C=_C triple bond        *
JH      JC3H3   JC3H4                    0.         AR                    *
JH      JC3H3N  JH4C3N                   7.50E+02   C=C + H   hase        *
JH      JC3N    JHC3N                    0.            EH                 *
JH      JC3O    JHC3O                    0.         AR                    *
JH      JC4     JC4H                     0.            EH                 *
JH      JC4H    JC4H2                    0.         AR                    *
JH      JC4H2   JC4H3                    1.21E+03 C=_C bond HASE          *
JH      JC4H3   JC4H4                    0.         AR                    *
JH      JC5     JC5H                     0.            EH                 *
JH      JC5H    JC5H2                    0.00E+00      EH                 *
JH      JC5H2   JC5H3                    1.21E+03 C=_C bond HASE          *
JH      JC5H3   JC5H4                    0.0                HASE          *
JH      JC5N    JHC5N                    0.            EH                 *
JH      JC6     JC6H                     0.            EH                 *
JH      JC6H    JC6H2                    0.            EH                 *
JH      JC6H2   JC6H3                    1.21E+03 C=_C bond HASE          *
JH      JC6H3   JC6H4                    0.                 HASE          * 
JH      JC7     JC7H                     0.            EH                 *
JH      JC7H    JC7H2                    0.            EH                 *
JH      JC7H2   JC7H3                    1.21E+03 C=_C bond HASE          *
JH      JC7H3   JC7H4                    0.                 HASE          *
JH      JC7N    JHC7N                    0.            EH                 *
JH      JC8     JC8H                     0.            EH                 *
JH      JC8H    JC8H2                    0.            EH                 *
JH      JC8H2   JC8H3                    1.21E+03  C=_C bond HASE         *
JH      JC8H3   JC8H4                    0.                  HASE         *
JH      JC9     JC9H                     0.            EH                 *
JH      JC9H    JC9H2                    0.            EH                 *
JH      JC9H2   JC9H3                    1.21E+03  C=_C bond HASE         *
JH      JC9H3   JC9H4                    0.                  HASE         *
JH      JC9N    JHC9N                    0.            EH                 *
JH      JCH     JCH2                     0.         AR                    *
JH      JCH2    JCH3                     0.         AR                    *
JH      JCH2CN  JCH3CN                   0.         AR                    *
JH      JH2CN   JCH2NH                   0.         AR -DPR CH2N          *
JH      JCH2NH2 JCH3NH2                  0.         AR -DPR CH3NH         *
JH      JCH2OH  JCH3OH                   0.         AR                    *
JH      JCH3    JCH4                     0.         AR                    *
JH      JCH2NH  JCH3NH                   0.         AR                    *
JH      JCH3NH  JCH3NH2                  0.         AR  -DPR CH3NH        *
JH      JCH4    JCH3    JH2              5.94E+03   JChPhy 50,5076 (1969) *
JH      JCHNH   JCH2NH                   0.         AR                    *
JH      JCH2NH  JCH2NH2                  0.         AR                    *
JH      JCN     JHCN                     0.         AR                    *
JH      JCO     JHCO                     2.50E+03   WOON priv commun -DPR *
JH      JCS     JHCS                     1.00E+03      EH                 *
JH      JFE     JFEH                     0.            EH                 *
JH      JH      JH2                      0.00E+00                         *
JH      JH2C3N  JC3H3N                   0.                   HASE        *
JH      JH2C5N  JH3C5N                   0.                   HASE        *
JH      JH2C7N  JH3C7N                   0.                   HASE        *
JH      JH2C9N  JH3C9N                   0.                   HASE        *
JH      JH2CO   JCH2OH                   2.50E+03   WOON priv commun -DPR *
JH      JH2CO   JCH3O                    2.50E+03   WOON priv commun -DPR *
JH      JH2O2   JH2O    JOH              1.40E+03   TH                    *
JH      JH2O2   JO2H    JH2              1.90E+03   M84                   *
JH      JH2S    JH2     JHS              8.60E+02   TH 8.60E+02 1.35E+03  *
JH      JH4C3N  JH5C3N                   0.         C2H5-CN   hase        *
JH      JHC2O   JCH2CO                   0.         AR                    *
JH      JHC3N   JH2C3N                   1.21E+03   C=_C bond HASE        *
JH      JHC3O   JH2C3O                   0.         AR                    *
JH      JHC5N   JH2C5N                   1.21E+03   C=_C bond HASE        *
JH      JHC7N   JH2C7N                   1.21E+03   C=_C bond HASE        *
JH      JHC9N   JH2C9N                   1.21E+03   C=_C bond HASE        *
JH      JHCCN   JCH2CN                   0.         AR                    *
JH      JHCO    JH2CO                    0.         AR  87kcal/mol exoth  *
JH      JHCS    JH2CS                    0.         guess. Exoth OK       *
JH      JHNO    JNO     JH2              1.50E+03   M84,Origin=Igl&Silk   *
JH      JHS     JH2S                     0.            EH                 *
JH      JMG     JMGH                     0.            EH                 *
JH      JMGH    JMGH2                    0.00E+00      EH                 *
JH      JN      JNH                      0.         AR                    *
JH      JN2H2   JH2     JN2     JH       6.50E+02   Alternative           *
JH      JNA     JNAH                     0.            EH                 *
JH      JNH     JNH2                     0.         AR                    *
JH      JNH2    JNH3                     0.         AR                    *
JH      JNO     JHNO                     0.         AR                    *
JH      JO      JOH                      0.         AR                    *
JH      JO2     JO2H                     1.20E+03   TH                    *
JH      JO2H    JH2O2                    0.         AR                    *
JH      JO3     JO2     JOH              4.50E+02   TH                    *
JH      JOCN    JHNCO                    0.         AR                    *
JH      JOCS    JCO     JHS              0.         M84                   *
JH      JOH     JH2O                     0.         AR                    *
JH      JS      JHS                      0.            EH                 *
JH      JSI     JSIH                     0.            EH                 *
JH      JSIH    JSIH2                    0.            EH                 *
JH      JSIH2   JSIH3                    0.            EH                 *
JH      JSIH3   JSIH4                    0.            EH                 *
JH      JSO2    JO2     JHS              0.            EH 92 Summer       *
JH2     JC      JCH2                     2.50E+03  92Summer. Exoth.       *
JH2     JC2     JC2H    JH               4.20E+03  Our test               *
JH2     JC2H    JC2H2   JH               4.20E+03                         *
JH2     JC3     JC3H    JH               4.20E+03                         *
JH2     JC3H    JC3H2   JH               4.20E+03                         *
JH2     JC4     JC4H    JH               4.20E+03                         *
JH2     JC4H    JC4H2   JH               4.20E+03                         *
JH2     JC5     JC5H    JH               4.20E+03                         *
JH2     JC5H    JC5H2   JH               4.20E+03                         *
JH2     JC6     JC6H    JH               4.20E+03                         *
JH2     JC6H    JC6H2   JH               4.20E+03                         *
JH2     JC7     JC7H    JH               4.20E+03                         *
JH2     JC7H    JC7H2   JH               4.20E+03                         *
JH2     JC8     JC8H    JH               4.20E+03                         *
JH2     JC8H    JC8H2   JH               4.20E+03                         *
JH2     JC9     JC9H    JH               4.20E+03                         *
JH2     JC9H    JC9H2   JH               4.20E+03                         *
JH2     JCH2    JCH3    JH               3.53E+03  NSRDS-NBS67 Westley 80 *
JH2     JCH3    JCH4    JH               6.44E+03  JChPhy 50,5076 (1969)  *
JH2     JCN     JHCN    JH               2.07E+03  M84 and 1988 UMIST     *
JH2     JNH2    JNH3    JH               6.30E+03  M84                    *
JH2     JOH     JH2O    JH               2.10E+03  JPhyChRefDat,13,1259   *
JN      JC2     JC2N                     0.         AR                    *
JN      JC3     JC3N                     0.            EH                 *
JN      JC3H    JHC3N                    0.            EH                 *
JN      JC5     JC5N                     0.            EH                 *
JN      JC5H    JHC5N                    0.            EH                 *
JN      JC7     JC7N                     0.            EH                 *
JN      JC7H    JHC7N                    0.            EH                 *
JN      JC9     JC9N                     0.            EH                 *
JN      JC9H    JHC9N                    0.            EH                 *
JN      JCH     JHCN                     0.         AR                    *
JN      JCH2    JH2CN                    0.         AR                    *
JN      JCH3    JCH2NH                   0.         AR                    *
JN      JHS     JNS     JH               0.         M84                   *
JN      JN      JN2                      0.00E+00   AR                    *
JN      JNH     JN2     JH               0.         AR Alternative        *
JN      JNH2    JN2H2                    0.         AR                    *
JN      JNS     JN2     JS               0.         M84                   *
JN      JO      JNO                      0.         AR                    *
JN      JO2H    JO2     JNH              0.         M84                   *
JN      JS      JNS                      0.            Hasegawa           *
JNH     JCH2    JCH2NH                   0.         AR                    *
JNH     JCH3    JCH3NH                   0.         AR (Implemented)      *
JNH     JNH     JN2     JH2              0.         M84. 180kcal/mol exoth*
JNH     JNH     JN2H2                    0.         AR   130kcal/mol exoth*
JNH     JNO     JN2     JO      JH       0.          From our gas network *
JNH2    JCH3    JCH3NH2                  0.         AR                    *
JNH2    JNO     JH2O    JN2              0.           From our gas network*
JO      JC2     JC2O                     0.         AR (more likely)      *
JO      JC3     JC3O                     0.            EH                 *
JO      JCH     JHCO                     0.         AR                    *
JO      JCH2    JH2CO                    0.         AR                    *
JO      JCH3    JCH3O                    0.         AR (CH3O), Brown      *
JO      JCN     JOCN                     0.         AR                    *
JO      JCO     JCO2                     1.00E+03   dHendecourt etal      *
JO      JCS     JOCS                     0.         TH                    *
JO      JHCO    JCO     JOH              0.         Leung/Eric gas chem   *
JO      JHCO    JCO2    JH               0.         M84 and Eric/gas chem *
JO      JHNO    JNO     JOH              0.             M84               *
JO      JHS     JSO     JH               0.         M84                   *
JO      JNH     JHNO                     0.         AR                    *
JO      JNH2    JHNO    JH               0.         M84                   *
JO      JNS     JNO     JS               0.         M84                   *
JO      JO      JO2                      0.00E+00   AR                    *
JO      JO2     JO3                      0.         AR,TH                 *
JO      JO2H    JO2     JOH              0.         M84                   *
JO      JOH     JO2H                     0.         AR                    *
JO      JS      JSO                      0.         TH                    *
JO      JSO     JSO2                     0.         TH                    *
JOH     JCH2    JCH2OH                   0.00E+00   AR                    *
JOH     JCH3    JCH3OH                   0.0        AR                    *
JOH     JCO     JCO2    JH               8.00E+01   Eric/Chun, Vidali=290K*
JOH     JHCO    JHCOOH                   0.0        AR(HCO2H) makes sense *
JOH     JNH2    JNH2OH                   0.         AR                    *
JOH     JOH     JH2O2                    0.00E+00   AR                    *
JS      JCH     JHCS                     0.          hasegawa             *
JS      JCH3    JH2CS   JH               0.         M84                   *
JS      JCO     JOCS                     0.           From our gas network*
JS      JNH     JNS     JH               0.         M84                   *
JNH2    JHCO    JNH2CHO                  0.            DPR-GUESS          *
JC      JOH     JHOC                     0.         AR                    *
JH      JCO     JHOC                     1.00E+03      EH      eliminated *
JH      JHOC    JCHOH                    0.         AR  eliminated        *
JO      JHOC    JCO     JOH              0.         From above reactions  *
JO      JHOC    JCO2    JH               0.         Paola/Eric            *
JCH     JOH     JCHOH                    0.         AR                    *
JH      JCHOH   JCH2OH                   0.         AR                    *
JH      JHCOOH  JH2     JCOOH            2.85E+03                         *
JNH2    JHCOOH  JNH3    JCOOH            2.85E+03                         *
JCH3    JHCOOH  JCH4    JCOOH            2.85E+03                         *
JOH     JHCOOH  JH2O    JCOOH            1.50E+03                         *
JCH3O   JHCOOH  JCH3OH  JCOOH            2.85E+03                         *
JNH     JH2CO   JNH2    JHCO             2.85E+03                         *
JNH2    JH2CO   JNH3    JHCO             2.85E+03                         *
JCH3    JH2CO   JCH4    JHCO             4.45E+03                         *
JCH3O   JH2CO   JCH3OH  JHCO             1.50E+03                         *
JCH2OH  JH2CO   JCH3OH  JHCO             2.95E+03                         *
JH      JNH2CHO JH2     JQCO             9.63E+03                         *
JNH2    JNH2CHO JNH3    JQCO             2.85E+03                         *
JCH3    JNH2CHO JCH4    JQCO             3.56E+03                         *
JOH     JNH2CHO JH2O    JQCO             1.50E+03                         *
JCH3O   JNH2CHO JCH3OH  JQCO             2.85E+03                         *
JCH2OH  JNH2CHO JCH3OH  JQCO             2.85E+03                         *
JH      JCH3CHO JH2     JCH3CO           2.12E+03                         *
JNH     JCH3CHO JNH2    JCH3CO           5.77E+03                         *
JNH2    JCH3CHO JNH3    JCH3CO           1.25E+03                         *
JCH3    JCH3CHO JCH4    JCH3CO           3.02E+03                         *
JOH     JCH3CHO JH2O    JCH3CO           1.50E+03                         *
JCH3O   JCH3CHO JCH3OH  JCH3CO           2.85E+03                         *
JCH2OH  JCH3CHO JCH3OH  JCH3CO           2.85E+03                         *
JH      JHCOOCH3JH2     JXCO             3.97E+03                         *
JNH2    JHCOOCH3JNH3    JXCO             2.85E+03                         *
JCH3    JHCOOCH3JCH4    JXCO             5.18E+03                         *
JOH     JHCOOCH3JH2O    JXCO             1.50E+03                         *
JCH3O   JHCOOCH3JCH3OH  JXCO             4.12E+03                         *
JH      JYCHO   JH2     JYCO             2.85E+03                         *
JNH     JYCHO   JNH2    JYCO             2.85E+03                         *
JNH2    JYCHO   JNH3    JYCO             2.85E+03                         *
JCH3    JYCHO   JCH4    JYCO             2.85E+03                         *
JOH     JYCHO   JH2O    JYCO             1.50E+03                         *
JCH3O   JYCHO   JCH3OH  JYCO             2.85E+03                         *
JCH2OH  JYCHO   JCH3OH  JYCO             2.85E+03                         *
JOH     JOHCCHO JCOCHO  JH2O             1.50E+03                         *
JH      JHCOOH  JHCO    JH2O             2.45E+03                         *
JNH2    JH2CO   JNH2CHO JH               2.85E+03                         *
JOH     JH2CO   JHCOOH  JH               2.85E+03                         *
JOH     JH2CO   JHCO    JH2O             0.00E+00 Changed 8-10-06 (only 1)*
JCH3O   JH2CO   JHCOOCH3JH               2.85E+03                         *
JH      JNH2CHO JHCO    JNH3             2.10E+03                         *
JOH     JNH2CHO JHCOOH  JNH2             2.10E+03                         *
JCH3O   JNH2CHO JHCOOCH3JNH2             2.10E+03                         *
JH      JCH3CHO JHCO    JCH4             2.40E+03                         *
JH      JCH3CHO JH2CO   JCH3             2.40E+03                         *
JNH2    JCH3CHO JCH3NH2 JHCO             2.40E+03                         *
JNH2    JCH3CHO JNH2CHO JCH3             2.40E+03                         *
JCH3    JCH3CHO JC2H6   JHCO             2.40E+03                         *
JOH     JCH3CHO JCH3OH  JHCO             2.40E+03                         *
JOH     JCH3CHO JHCOOH  JCH3             2.40E+03                         *
JCH3O   JCH3CHO JHCOOCH3JCH3             2.40E+03                         *
JCH2OH  JCH3CHO JC2H5OH JHCO             2.40E+03                         *
JCH2OH  JCH3CHO JYCHO   JCH3             2.40E+03                         *
JH      JHCOOCH3JCH3OH  JHCO             2.45E+03                         *
JOH     JHCOOCH3JHCOOH  JCH3O            2.45E+03                         *
JH      JYCHO   JCH3OH  JHCO             2.40E+03                         *
JNH2    JYCHO   JNH2CHO JCH2OH           2.40E+03                         *
JCH3    JYCHO   JC2H5OH JHCO             2.40E+03                         *
JOH     JYCHO   JHCOOH  JCH2OH           2.40E+03                         *
JCH3O   JYCHO   JHCOOCH3JCH2OH           2.40E+03                         *
JCH2OH  JYCHO   JYY     JHCO             2.40E+03                         *
JH      JOHCCHO JH2CO   JHCO             2.40E+03                         *
JNH     JOHCCHO JHNCHO  JHCO             2.40E+03                         *
JNH2    JOHCCHO JNH2CHO JHCO             2.40E+03                         *
JCH3    JOHCCHO JCH3CHO JHCO             2.40E+03                         *
JOH     JOHCCHO JHCOOH  JHCO             2.40E+03                         *
JCH3O   JOHCCHO JHCOOCH3JHCO             2.40E+03                         *
JCH2OH  JOHCCHO JYCHO   JHCO             2.40E+03                         *
JNH     JCO     JHNCO                    1.50E+03                         *
JNH2    JCO     JQCO                     1.50E+03                         *
JCH3    JCO     JCH3CO                   1.50E+03                         *
JOH     JCO     JCOOH                    1.50E+03                         *
JCH3O   JCO     JXCO                     1.50E+03                         *
JCH2OH  JCO     JYCO                     1.50E+03                         *
---------------------------------------------------------------            -DO NOT INCLUDE FROM HERE
JNH     JCO     JHNCO                    3.00E+03                         *-HENCE SET LINE 619 AS LIMIT AT TOP OF LIST
JNH2    JCO     JQCO                     3.00E+03                         *
JCH3    JCO     JCH3CO                   3.46E+03                         *
JOH     JCO     JCOOH                    3.00E+03                         *
JCH3O   JCO     JXCO                     3.00E+03                         *
JCH2OH  JCO     JYCO                     3.00E+03                         *
--------------------------------------------------------------
JNH     JCHOH   JNH2CHO                  0.            DPR-GUESS          *
JH      JH2CO   JHCO    JH2              1.85E+03   TH  BELOW ARE NOT USED*
JCH     JCH3    JC2H4                    0.         AR(=CHCH3), doubtful  *
JCH     JCO2    JHCO    JCO              3.00E+03   M84 Test only         *
JCH     JC2H2   JC3H2   JH               0.         Mitchell 84           *
JCH     JH2CO   JHCO    JCH2             2.00E+03   M84 Test only         *
JCH2    JNH2    JCH2NH  JH               0.         hasegawa (not adopted)*
JH      JCO     JHCO                     1.00E+03   TH                    *
JH      JCO     JHCO                     1.83E+03   DPR                   *
JH      JH2CO   JCH2OH                   1.85E+03   Eric                NI*
JH      JH2CO   JCH2OH                   6.50E+02   DPR                   *
JH      JHCO    JCO     JH2              0.         M84 89kcal/mol exot NI*
JH      JHNO    JNO     JH2              7.50E+02   Ea<1160K,JPCRD,2,267NI*
JH      JN2H2   JH2     JN2H             6.50E+02   TH To be eliminated NI*
JH      JO2H    JO2     JH2              3.50E+02   M84Try this as well NI*
JH      JN2     JN2H                     1.20E+03   EH To be eliminated NI*
JH      JNNH    JN2H2                    0.         AR To be eliminated NI*
JH      JCNH2   JCHNH2                   0.         AR                  NI*
JH2     JC2     JC2H    JH               2.10E+03  Our estimate           *
JH2     JC3     JC3H    JH               2.10E+03                         *
JH2     JC4     JC4H    JH               2.10E+03                         *
JH2     JC5     JC5H    JH               2.10E+03                         *
JH2     JC6     JC6H    JH               2.10E+03                         *
JH2     JC7     JC7H    JH               2.10E+03                         *
JH2     JC8     JC8H    JH               2.10E+03                         *
JH2     JC9     JC9H    JH               2.10E+03                         *
JH2     JC2H    JC2H2   JH               2.10E+03                         *
JH2     JC3H    JC3H2   JH               2.10E+03                         *
JH2     JC4H    JC4H2   JH               2.10E+03                         *
JH2     JC5H    JC5H2   JH               2.10E+03                         *
JH2     JC6H    JC6H2   JH               2.10E+03                         *
JH2     JC7H    JC7H2   JH               2.10E+03                         *
JH2     JC8H    JC8H2   JH               2.10E+03                         *
JH2     JC9H    JC9H2   JH               2.10E+03                         *
JH2     JOH     JH2O    JH               2.60E+03   TH                    *
JN      JCH2    JHCN    JH               2.50E+02   M84                   *
JN      JCH3    JHCN    JH      JH       2.50E+02   M84                   *
JN      JCH3    JCH2NH                   0.                hasegawa       *
JN      JNH     JNNH                     0.         AR To be eliminated   *
JN      JNH2    JN2     JH2              0.         Westley               *
JN      JOH     JHON                     0.         AR                    *
JN      JNO     JN2     JO               0.         from Our gas network  *
JNH     JNO     JNHNO                    0.         AR Not to be used     *
JNH     JCH3    JCH2NH  JH               0.         hasegawa (not used)   *
JNH2    JNO     JNH2NO                   0.         AR. Not to be used.   *
JO      JC2     JCO     JC               0.         M84 (Not to be used)  *
JO      JCO     JCO2                     0.         TH  not for use. 91Dec*
JOH     JCO     JCO2    JH               3.00E+02   JChPhy 44, 2877 (1966)*
JOH     JCO     JCO2    JH               0.         IMWSmith measured 92  *
JH      JHOC    JCO     JH2              0.            Our guess        NI*
JO      JCO     JCO2                     1.00e+01   DPR guess             *
JO      JCO     JCO2                     2.00e+01   dpr                   *
JO      JCO     JCO2                     3.00e+01   dpr                   *
JO      JCO     JCO2                     4.00e+01   dpr                   *
JO      JCO     JCO2                     5.00e+01   dpr                   *
JO      JCO     JCO2                     6.00e+01   dpr                   *
JO      JCO     JCO2                     7.00e+01   dpr                   *
JO      JCO     JCO2                     8.00e+01   dpr                   *
JO      JCO     JCO2                     9.00e+01   dpr                   *
JO      JCO     JCO2                     1.00e+02   dpr                   *
JO      JCO     JCO2                     2.00e+02   dpr                   *
JO      JCO     JCO2                     3.00e+02   dpr                   *
JO      JCO     JCO2                     4.00e+02   dpr                   *
JO      JCO     JCO2                     5.00e+02   dpr                   *
JO      JCO     JCO2                     6.00e+02   dpr                   *
JO      JCO     JCO2                     7.00e+02   dpr                   *
JO      JCO     JCO2                     8.00e+02   dpr                   *
JO      JCO     JCO2                     9.00e+02   dpr                   *
JO      JCO     JCO2                     1.25e+02   dpr                   *
JH      JCO     JHCO                     5.00E+03   dpr                   *
JH      JH2CO   JCH2OH                   5.00E+03   dpr                   *
JH      JCO     JHCO                     3.50E+03   dpr                   *
JH      JH2CO   JCH2OH                   3.50E+03   dpr                   *
JO      JCO     JCO2                     1.50e+02   dpr                   *
JH      JCO     JHCO                     3.00E+03   dpr                   *
JH      JH2CO   JCH2OH                   3.00E+03   dpr                   *
JO      JCO     JCO2                     1.35e+02   dpr                   *
JO      JCO     JCO2                     1.40e+02   dpr                   *
JH      JCO     JHCO                     2.80E+03   dpr                   *
JH      JH2CO   JCH2OH                   2.80E+03   dpr                   *
JH      JCO     JHCO                     3.20E+03   dpr                   *
JH      JH2CO   JCH2OH                   3.20E+03   dpr                   *

Above, from NH + CHOH down are the reactions for HOC and CHOH - as of     *
12th May do not use these for CH4O  chemistry.                            *
The following reactions are too uncertain to include. Not read.           *
H       HC2NH   xxxxx                    0.00E+00                         *


c--------------------------------------------------------------
c EH = Erics Notes. Some rounded. Others from polarizability. 
c      The others "estimate".
c AR = Allen Robinson 
c      Allen, M.,and Robinson, G.W. 1977, Ap.J.,212,396.
c      Mostly based on Avgul Diselev 1970, Chem.Phys.Carbon,6,p1.
c      These are actually carbonatious and graphite surfaces.
c      Converted by 1kcal/mole = 503.4K, Numbers checked twice by Hasegawa.
c      They say these ED may be reduced to 1/5 on a H2-ice surface.
c TA = Tielens Allamandola
c WT = Watson = mostly from Hollenbach Salpeter
c      Watson, W.D. 1976, Rev.Mod.Phys., 48, 513.
c TH = Tielens and Hagen. 
c      Tielens, A.G.G.M., and Hagen, W. 1982, A&Ap.,114, 245.
c SA = Sandford and Allamandola, 1990, Ap.J.355, 357.
c      Lab. More realistic and up-to-dated.
c      Also, Sandford and Allamandola, 1988,Icarus, 76, 201.
c SA93=Sandford & Allamandola, 1993, ApJ, 417, 815. Lab, Table 2.
c Le = Leger, 1983, A&Ap,123, 271.
c M84= Mitchell 1984, ApJ. S., 54, 81.  or  1984 ApJ., 287, 665.
c AK70 Avgul, N.N., and Kiselev, A.V. 1970, Chemistry and Physics of Carbon,
c      (journal) vol. 6, p1. (editor P.L.Walker).  This article was used 
c      by AR to calculate adsorption energies.
c-------------------------------------------------------------


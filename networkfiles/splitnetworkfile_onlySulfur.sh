#!/bin/bash

PREFIX="gg_in_network"
NETWORKIN=$PREFIX".d"
REMAIN=$PREFIX"_remaining.d"
REMAINTMP=$REMAIN".tmp"

# initiate/reset remaining network file
#echo "" > $NETWORKOUT
rm $PREFIX"_"*
cp $NETWORKIN $REMAIN

# move all sulfur metals
ARRAY=( 'S[^i]' )
OUT=$PREFIX"_sulfur.d"
echo "" > $OUT
for i in ${ARRAY[@]}; do
	echo "moving $i"
	grep ${i}'.*RTYPE' $REMAIN >> $OUT
	grep -v ${i}'.*RTYPE' $REMAIN > $REMAINTMP
	cat $REMAINTMP > $REMAIN
done

rm $REMAINTMP

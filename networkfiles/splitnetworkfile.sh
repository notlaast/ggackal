#!/bin/bash

PREFIX="gg_in_network"
NETWORKIN=$PREFIX".d"
REMAIN=$PREFIX"_remaining.d"
REMAINTMP=$REMAIN".tmp"

# initiate/reset remaining network file
#echo "" > $NETWORKOUT
rm $PREFIX"_"*
cp $NETWORKIN $REMAIN

# move all metals (2nd row elements)
ARRAY=( 'P' 'Fe' 'Cl'  'Mg' 'Si' 'Na' )
OUT=$PREFIX"_metals.d"
echo "" > $OUT
for i in ${ARRAY[@]}; do
	echo "moving $i"
	grep ${i}'.*RTYPE' $REMAIN >> $OUT
	grep -v ${i}'.*RTYPE' $REMAIN > $REMAINTMP
	cat $REMAINTMP > $REMAIN
done

# move all sulfur metals
ARRAY=( 'S[^i]' )
OUT=$PREFIX"_sulfur.d"
echo "" > $OUT
for i in ${ARRAY[@]}; do
	echo "moving $i"
	grep ${i}'.*RTYPE' $REMAIN >> $OUT
	grep -v ${i}'.*RTYPE' $REMAIN > $REMAINTMP
	cat $REMAINTMP > $REMAIN
done

# move carbon chains
ARRAY=( 'C[4-9]' 'C10' )
OUT=$PREFIX"_ccc.d"
echo "" > $OUT
for i in ${ARRAY[@]}; do
	echo "moving $i"
	grep ${i}'.*RTYPE' $REMAIN >> $OUT
	grep -v ${i}'.*RTYPE' $REMAIN > $REMAINTMP
	cat $REMAINTMP > $REMAIN
done

# move anything larger than ~7 atoms
ARRAY=( '[a-zA-Z0-9]\{7,\}' )
OUT=$PREFIX"_poly9.d"
echo "" > $OUT
for i in ${ARRAY[@]}; do
	echo "moving large molecules"
	grep ${i}'.*RTYPE' $REMAIN >> $OUT
	grep -v ${i}'.*RTYPE' $REMAIN > $REMAINTMP
	cat $REMAINTMP > $REMAIN
done

rm $REMAINTMP
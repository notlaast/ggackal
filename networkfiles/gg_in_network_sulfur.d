
S1+ + G1- -> S + G0										RTYPE=0 K1=3.00E-17 K2=5.00E-01 K3=0.0 # gg08_rnum=12
H2S -> H2S1+ + E1-										RTYPE=1 K1=1.70E+03 K2=0.0 K3=0.0 # gg08_rnum=38
HCS -> HCS1+ + E1-										RTYPE=1 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=41
OCS -> OCS1+ + E1-										RTYPE=1 K1=1.44E+03 K2=0.0 K3=0.0 # gg08_rnum=52
S -> S1+ + E1-											RTYPE=1 K1=9.60E+02 K2=0.0 K3=0.0 # gg08_rnum=54
C2S -> CS + C											RTYPE=1 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=70
C3S -> C2 + CS											RTYPE=1 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=80
C4S -> C3 + CS											RTYPE=1 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=89
CS -> C + S												RTYPE=1 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=158
H2CS -> HCS + H											RTYPE=1 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=170
H2S -> H2 + S											RTYPE=1 K1=5.15E+03 K2=0.0 K3=0.0 # gg08_rnum=173
H2S2 -> HS + HS											RTYPE=1 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=174
HCS -> CH + S											RTYPE=1 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=197
HCS -> CS + H											RTYPE=1 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=198
HS -> H + S												RTYPE=1 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=230
S2H -> HS + S											RTYPE=1 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=231
NS -> N + S												RTYPE=1 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=249
OCS -> CO + S											RTYPE=1 K1=5.35E+03 K2=0.0 K3=0.0 # gg08_rnum=255
S2 -> S + S												RTYPE=1 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=276
SiS -> Si + S											RTYPE=1 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=294
SO -> S + O												RTYPE=1 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=295
SO2 -> SO + O											RTYPE=1 K1=1.88E+03 K2=0.0 K3=0.0 # gg08_rnum=296
C1+ + C2S -> C2S1+ + C									RTYPE=2 K1=1.72E-09 K2=-0.5 K3=0.0 # gg08_rnum=346
C1+ + C2S -> C31+ + S									RTYPE=2 K1=1.72E-09 K2=-0.5 K3=0.0 # gg08_rnum=347
C1+ + C3S -> C41+ + S									RTYPE=2 K1=2.36E-09 K2=-0.5 K3=0.0 # gg08_rnum=364
C1+ + C3S -> C3S1+ + C									RTYPE=2 K1=2.36E-09 K2=-0.5 K3=0.0 # gg08_rnum=365
C1+ + C4S -> C4S1+ + C									RTYPE=2 K1=1.80E-09 K2=-0.5 K3=0.0 # gg08_rnum=378
C1+ + C4S -> C51+ + S									RTYPE=2 K1=1.80E-09 K2=-0.5 K3=0.0 # gg08_rnum=379
C1+ + H2CS -> CH21+ + CS								RTYPE=2 K1=2.10E-09 K2=-0.5 K3=0.0 # gg08_rnum=451
C1+ + H2S -> H2S1+ + C									RTYPE=2 K1=3.20E-10 K2=-0.5 K3=0.0 # gg08_rnum=454
C1+ + H2S -> HCS1+ + H									RTYPE=2 K1=9.50E-10 K2=-0.5 K3=0.0 # gg08_rnum=455
C1+ + HS -> CS1+ + H									RTYPE=2 K1=2.40E-09 K2=-0.5 K3=0.0 # gg08_rnum=509
C1+ + NS -> NS1+ + C									RTYPE=2 K1=2.70E-09 K2=-0.5 K3=0.0 # gg08_rnum=529
C1+ + NS -> CS1+ + N									RTYPE=2 K1=2.70E-09 K2=-0.5 K3=0.0 # gg08_rnum=530
C1+ + OCS -> CS1+ + CO									RTYPE=2 K1=1.60E-09 K2=-0.5 K3=0.0 # gg08_rnum=534
C1+ + OCS -> OCS1+ + C									RTYPE=2 K1=4.00E-10 K2=-0.5 K3=0.0 # gg08_rnum=535
C1+ + S -> S1+ + C										RTYPE=2 K1=1.50E-09 K2=0.0 K3=0.0 # gg08_rnum=551
C1+ + SiS -> SiS1+ + C									RTYPE=2 K1=2.50E-09 K2=-0.5 K3=0.0 # gg08_rnum=579
C1+ + SiS -> SiC1+ + S									RTYPE=2 K1=2.50E-09 K2=-0.5 K3=0.0 # gg08_rnum=580
C1+ + SO -> CO1+ + S									RTYPE=2 K1=1.10E-09 K2=-0.5 K3=0.0 # gg08_rnum=581
C1+ + SO -> CS1+ + O									RTYPE=2 K1=1.10E-09 K2=-0.5 K3=0.0 # gg08_rnum=582
C1+ + SO -> SO1+ + C									RTYPE=2 K1=1.10E-09 K2=-0.5 K3=0.0 # gg08_rnum=583
C1+ + SO -> S1+ + CO									RTYPE=2 K1=1.10E-09 K2=-0.5 K3=0.0 # gg08_rnum=584
C1+ + SO2 -> SO1+ + CO									RTYPE=2 K1=2.00E-09 K2=-0.5 K3=0.0 # gg08_rnum=585
C21+ + S -> CS1+ + C									RTYPE=2 K1=5.80E-10 K2=0.0 K3=0.0 # gg08_rnum=632
C21+ + S -> S1+ + C2									RTYPE=2 K1=5.80E-10 K2=0.0 K3=0.0 # gg08_rnum=633
C2H1+ + S -> S1+ + C2H									RTYPE=2 K1=1.20E-09 K2=0.0 K3=0.0 # gg08_rnum=661
HC2H1+ + H2S -> H2S1+ + HC2H							RTYPE=2 K1=9.80E-10 K2=-0.5 K3=0.0 # gg08_rnum=713
HC2H1+ + H2S -> H2C2H1+ + HS							RTYPE=2 K1=2.00E-11 K2=-0.5 K3=0.0 # gg08_rnum=714
H2C2H1+ + H2S -> H3S1+ + HC2H							RTYPE=2 K1=9.70E-10 K2=-0.5 K3=0.0 # gg08_rnum=788
H2C2H1+ + S -> HC2S1+ + H2								RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=798
H2C2H21+ + S -> HC2S1+ + H2 + H							RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=845
CH3CH21+ + H2S -> H3S1+ + H2C2H2						RTYPE=2 K1=9.50E-10 K2=-0.5 K3=0.0 # gg08_rnum=856
C2N1+ + H2S -> HCS1+ + HCN								RTYPE=2 K1=1.20E-09 K2=0.0 K3=0.0 # gg08_rnum=871
C3H1+ + H2S -> H2C2H1+ + CS								RTYPE=2 K1=2.98E-10 K2=-0.5 K3=0.0 # gg08_rnum=891
C3H1+ + H2S -> HCS1+ + HC2H								RTYPE=2 K1=2.98E-10 K2=-0.5 K3=0.0 # gg08_rnum=892
C3H1+ + H2S -> HC3S1+ + H2								RTYPE=2 K1=2.98E-10 K2=-0.5 K3=0.0 # gg08_rnum=893
C3H1+ + OCS -> HC3O1+ + CS								RTYPE=2 K1=4.57E-10 K2=-0.5 K3=0.0 # gg08_rnum=902
C3H1+ + OCS -> HC3S1+ + CO								RTYPE=2 K1=4.57E-10 K2=-0.5 K3=0.0 # gg08_rnum=903
C3H1+ + OCS -> CS1+ + C3O + H							RTYPE=2 K1=4.57E-10 K2=-0.5 K3=0.0 # gg08_rnum=904
C3H21+ + S -> HC3S1+ + H								RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=940
C3H31+ + S -> HC3S1+ + H2								RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=970
HC4H1+ + S -> HC4S1+ + H								RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=1040
C4H31+ + S -> HC4S1+ + H2								RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=1063
CH1+ + H2S -> H3S1+ + C									RTYPE=2 K1=6.30E-10 K2=0.0 K3=0.0 # gg08_rnum=1209
CH1+ + H2S -> HCS1+ + H2								RTYPE=2 K1=6.60E-10 K2=0.0 K3=0.0 # gg08_rnum=1210
CH1+ + OCS -> HOCS1+ + C								RTYPE=2 K1=8.55E-10 K2=0.0 K3=0.0 # gg08_rnum=1230
CH1+ + OCS -> HCS1+ + CO								RTYPE=2 K1=1.05E-09 K2=0.0 K3=0.0 # gg08_rnum=1231
CH1+ + S -> CS1+ + H									RTYPE=2 K1=4.70E-10 K2=0.0 K3=0.0 # gg08_rnum=1233
CH1+ + S -> HS1+ + C									RTYPE=2 K1=4.70E-10 K2=0.0 K3=0.0 # gg08_rnum=1234
CH1+ + S -> S1+ + CH									RTYPE=2 K1=4.70E-10 K2=0.0 K3=0.0 # gg08_rnum=1235
CH21+ + H2S -> HCS1+ + H2 + H							RTYPE=2 K1=1.10E-09 K2=0.0 K3=0.0 # gg08_rnum=1250
CH21+ + H2S -> H3CS1+ + H								RTYPE=2 K1=1.60E-09 K2=0.0 K3=0.0 # gg08_rnum=1251
CH21+ + OCS -> H2CS1+ + CO								RTYPE=2 K1=7.20E-10 K2=0.0 K3=0.0 # gg08_rnum=1264
CH21+ + OCS -> HCS1+ + HCO								RTYPE=2 K1=1.08E-09 K2=0.0 K3=0.0 # gg08_rnum=1265
CH21+ + S -> HCS1+ + H									RTYPE=2 K1=1.40E-09 K2=0.0 K3=0.0 # gg08_rnum=1267
SiCH21+ + S -> Si1+ + H2CS								RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=1280
CH31+ + H2S -> H3CS1+ + H2								RTYPE=2 K1=1.20E-09 K2=-0.5 K3=0.0 # gg08_rnum=1315
CH31+ + HS -> H2CS1+ + H2								RTYPE=2 K1=2.20E-09 K2=-0.5 K3=0.0 # gg08_rnum=1323
CH31+ + OCS -> H3CS1+ + CO								RTYPE=2 K1=1.90E-09 K2=-0.5 K3=0.0 # gg08_rnum=1339
CH31+ + S -> HCS1+ + H2									RTYPE=2 K1=1.40E-09 K2=0.0 K3=0.0 # gg08_rnum=1342
CH31+ + SO -> HOCS1+ + H2								RTYPE=2 K1=4.20E-09 K2=-0.5 K3=0.0 # gg08_rnum=1347
CH41+ + H2S -> H2S1+ + CH4								RTYPE=2 K1=1.60E-09 K2=0.0 K3=0.0 # gg08_rnum=1372
CH41+ + H2S -> H3S1+ + CH3								RTYPE=2 K1=9.50E-10 K2=0.0 K3=0.0 # gg08_rnum=1373
CH41+ + OCS -> OCS1+ + CH4								RTYPE=2 K1=4.20E-10 K2=0.0 K3=0.0 # gg08_rnum=1379
CH41+ + OCS -> HOCS1+ + CH3								RTYPE=2 K1=9.80E-10 K2=0.0 K3=0.0 # gg08_rnum=1380
CH51+ + S -> HS1+ + CH4									RTYPE=2 K1=1.30E-09 K2=0.0 K3=0.0 # gg08_rnum=1405
CN1+ + S -> S1+ + CN									RTYPE=2 K1=1.10E-09 K2=0.0 K3=0.0 # gg08_rnum=1442
CO1+ + H2S -> H2S1+ + CO								RTYPE=2 K1=2.44E-09 K2=0.0 K3=0.0 # gg08_rnum=1466
CO1+ + H2S -> HCO1+ + HS								RTYPE=2 K1=1.56E-10 K2=0.0 K3=0.0 # gg08_rnum=1467
CO1+ + S -> S1+ + CO									RTYPE=2 K1=1.10E-09 K2=0.0 K3=0.0 # gg08_rnum=1482
CO1+ + SO2 -> SO1+ + CO2								RTYPE=2 K1=1.70E-09 K2=0.0 K3=0.0 # gg08_rnum=1483
CO21+ + H2S -> H2S1+ + CO2								RTYPE=2 K1=1.40E-09 K2=0.0 K3=0.0 # gg08_rnum=1491
CS1+ + C -> C1+ + CS									RTYPE=2 K1=1.60E-09 K2=0.0 K3=0.0 # gg08_rnum=1499
CS1+ + CH4 -> HCS1+ + CH3								RTYPE=2 K1=5.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1500
CS1+ + Fe -> Fe1+ + CS									RTYPE=2 K1=1.70E-10 K2=0.0 K3=0.0 # gg08_rnum=1501
CS1+ + H2 -> HCS1+ + H									RTYPE=2 K1=4.80E-10 K2=0.0 K3=0.0 # gg08_rnum=1502
CS1+ + Mg -> Mg1+ + CS									RTYPE=2 K1=2.70E-10 K2=0.0 K3=0.0 # gg08_rnum=1503
CS1+ + Na -> Na1+ + CS									RTYPE=2 K1=2.30E-10 K2=0.0 K3=0.0 # gg08_rnum=1504
CS1+ + O -> CO1+ + S									RTYPE=2 K1=6.00E-11 K2=0.0 K3=0.0 # gg08_rnum=1505
CS1+ + O2 -> OCS1+ + O									RTYPE=2 K1=1.30E-10 K2=0.0 K3=0.0 # gg08_rnum=1506
CS1+ + Si -> Si1+ + CS									RTYPE=2 K1=1.50E-10 K2=0.0 K3=0.0 # gg08_rnum=1507
H1+ + C2S -> C2S1+ + H									RTYPE=2 K1=1.09E-08 K2=-0.5 K3=0.0 # gg08_rnum=1528
H1+ + C3S -> C3S1+ + H									RTYPE=2 K1=1.52E-08 K2=-0.5 K3=0.0 # gg08_rnum=1541
H1+ + C4S -> C4S1+ + H									RTYPE=2 K1=1.17E-08 K2=-0.5 K3=0.0 # gg08_rnum=1552
H1+ + CS -> CS1+ + H									RTYPE=2 K1=1.80E-08 K2=-0.5 K3=0.0 # gg08_rnum=1618
H1+ + H2CS -> H2CS1+ + H								RTYPE=2 K1=6.40E-09 K2=-0.5 K3=0.0 # gg08_rnum=1622
H1+ + H2S -> H2S1+ + H									RTYPE=2 K1=3.80E-09 K2=-0.5 K3=0.0 # gg08_rnum=1624
H1+ + H2S2 -> H2S21+ + H								RTYPE=2 K1=4.67E-09 K2=-0.5 K3=0.0 # gg08_rnum=1625
H1+ + HCS -> CS1+ + H2									RTYPE=2 K1=1.85E-08 K2=-0.5 K3=0.0 # gg08_rnum=1650
H1+ + HS -> HS1+ + H									RTYPE=2 K1=3.50E-09 K2=-0.5 K3=0.0 # gg08_rnum=1665
H1+ + HS -> S1+ + H2									RTYPE=2 K1=3.50E-09 K2=-0.5 K3=0.0 # gg08_rnum=1666
H1+ + S2H -> S2H1+ + H									RTYPE=2 K1=7.79E-09 K2=-0.5 K3=0.0 # gg08_rnum=1667
H1+ + NS -> NS1+ + H									RTYPE=2 K1=1.70E-08 K2=-0.5 K3=0.0 # gg08_rnum=1684
H1+ + OCS -> HS1+ + CO									RTYPE=2 K1=6.50E-09 K2=-0.5 K3=0.0 # gg08_rnum=1687
H1+ + S -> S1+ + H										RTYPE=2 K1=1.30E-09 K2=0.0 K3=0.0 # gg08_rnum=1694
H1+ + S2 -> S21+ + H									RTYPE=2 K1=3.00E-09 K2=0.0 K3=0.0 # gg08_rnum=1695
H1+ + SiS -> SiS1+ + H									RTYPE=2 K1=1.60E-08 K2=-0.5 K3=0.0 # gg08_rnum=1722
H1+ + SO -> SO1+ + H									RTYPE=2 K1=1.40E-08 K2=-0.5 K3=0.0 # gg08_rnum=1723
H21+ + H2S -> HS1+ + H + H2								RTYPE=2 K1=8.60E-10 K2=0.0 K3=0.0 # gg08_rnum=1754
H21+ + H2S -> H2S1+ + H2								RTYPE=2 K1=2.70E-09 K2=0.0 K3=0.0 # gg08_rnum=1755
H21+ + H2S -> S1+ + H2 + H2								RTYPE=2 K1=7.70E-10 K2=0.0 K3=0.0 # gg08_rnum=1756
H2CO1+ + S -> S1+ + H2CO								RTYPE=2 K1=5.50E-10 K2=0.0 K3=0.0 # gg08_rnum=1809
H2CO1+ + S -> HS1+ + HCO								RTYPE=2 K1=5.50E-10 K2=0.0 K3=0.0 # gg08_rnum=1810
H2O1+ + H2S -> H3S1+ + OH								RTYPE=2 K1=7.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1829
H2O1+ + H2S -> H2S1+ + H2O								RTYPE=2 K1=8.90E-10 K2=0.0 K3=0.0 # gg08_rnum=1830
H2O1+ + H2S -> H3O1+ + HS								RTYPE=2 K1=5.90E-10 K2=0.0 K3=0.0 # gg08_rnum=1831
H2O1+ + S -> HS1+ + OH									RTYPE=2 K1=4.30E-10 K2=0.0 K3=0.0 # gg08_rnum=1849
H2O1+ + S -> HSO1+ + H									RTYPE=2 K1=4.30E-10 K2=0.0 K3=0.0 # gg08_rnum=1850
H2O1+ + S -> S1+ + H2O									RTYPE=2 K1=4.30E-10 K2=0.0 K3=0.0 # gg08_rnum=1851
H2S1+ + C -> HCS1+ + H									RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=1853
H2S1+ + Fe -> Fe1+ + H2S								RTYPE=2 K1=1.80E-09 K2=0.0 K3=0.0 # gg08_rnum=1854
H2S1+ + H -> HS1+ + H2									RTYPE=2 K1=2.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1855
H2S1+ + H2O -> H3O1+ + HS								RTYPE=2 K1=7.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1856
H2S1+ + H2S -> H3S1+ + HS								RTYPE=2 K1=5.80E-10 K2=0.0 K3=0.0 # gg08_rnum=1857
H2S1+ + HCO -> HCO1+ + H2S								RTYPE=2 K1=7.00E-10 K2=0.0 K3=0.0 # gg08_rnum=1858
H2S1+ + Mg -> Mg1+ + H2S								RTYPE=2 K1=2.80E-09 K2=0.0 K3=0.0 # gg08_rnum=1859
H2S1+ + N -> NS1+ + H2									RTYPE=2 K1=7.90E-10 K2=0.0 K3=0.0 # gg08_rnum=1860
H2S1+ + Na -> Na1+ + H2S								RTYPE=2 K1=2.50E-09 K2=0.0 K3=0.0 # gg08_rnum=1861
H2S1+ + NH3 -> NH41+ + HS								RTYPE=2 K1=1.30E-09 K2=0.0 K3=0.0 # gg08_rnum=1862
H2S1+ + NH3 -> NH31+ + H2S								RTYPE=2 K1=5.60E-10 K2=0.0 K3=0.0 # gg08_rnum=1863
H2S1+ + NO -> NO1+ + H2S								RTYPE=2 K1=3.70E-10 K2=0.0 K3=0.0 # gg08_rnum=1864
H2S1+ + O -> HS1+ + OH									RTYPE=2 K1=3.10E-10 K2=0.0 K3=0.0 # gg08_rnum=1865
H2S1+ + O -> SO1+ + H2									RTYPE=2 K1=3.10E-10 K2=0.0 K3=0.0 # gg08_rnum=1866
H2S1+ + S -> S1+ + H2S									RTYPE=2 K1=1.10E-09 K2=0.0 K3=0.0 # gg08_rnum=1867
H2S1+ + Si -> Si1+ + H2S								RTYPE=2 K1=1.60E-09 K2=0.0 K3=0.0 # gg08_rnum=1868
H31+ + C2S -> HC2S1+ + H2								RTYPE=2 K1=6.41E-09 K2=-0.5 K3=0.0 # gg08_rnum=1884
H31+ + C3S -> HC3S1+ + H2								RTYPE=2 K1=8.89E-09 K2=-0.5 K3=0.0 # gg08_rnum=1895
H31+ + C4S -> HC4S1+ + H2								RTYPE=2 K1=6.82E-09 K2=-0.5 K3=0.0 # gg08_rnum=1903
H31+ + CS -> HCS1+ + H2									RTYPE=2 K1=1.10E-08 K2=-0.5 K3=0.0 # gg08_rnum=1966
H31+ + H2CS -> H3CS1+ + H2								RTYPE=2 K1=3.80E-09 K2=-0.5 K3=0.0 # gg08_rnum=1969
H31+ + H2S -> H3S1+ + H2								RTYPE=2 K1=2.30E-09 K2=-0.5 K3=0.0 # gg08_rnum=1971
H31+ + H2S2 -> H3S21+ + H2								RTYPE=2 K1=2.74E-09 K2=-0.5 K3=0.0 # gg08_rnum=1972
H31+ + HCS -> H2CS1+ + H2								RTYPE=2 K1=1.09E-08 K2=-0.5 K3=0.0 # gg08_rnum=1989
H31+ + HS -> H2S1+ + H2									RTYPE=2 K1=4.20E-09 K2=-0.5 K3=0.0 # gg08_rnum=2012
H31+ + S2H -> H2S21+ + H2								RTYPE=2 K1=4.56E-09 K2=-0.5 K3=0.0 # gg08_rnum=2013
H31+ + NS -> HNS1+ + H2									RTYPE=2 K1=9.90E-09 K2=-0.5 K3=0.0 # gg08_rnum=2031
H31+ + OCS -> HOCS1+ + H2								RTYPE=2 K1=3.80E-09 K2=-0.5 K3=0.0 # gg08_rnum=2034
H31+ + S -> HS1+ + H2									RTYPE=2 K1=2.60E-09 K2=0.0 K3=0.0 # gg08_rnum=2052
H31+ + S2 -> S2H1+ + H2									RTYPE=2 K1=2.00E-09 K2=0.0 K3=0.0 # gg08_rnum=2053
H31+ + SiS -> HSiS1+ + H2								RTYPE=2 K1=9.40E-09 K2=-0.5 K3=0.0 # gg08_rnum=2072
H31+ + SO -> HSO1+ + H2									RTYPE=2 K1=8.40E-09 K2=-0.5 K3=0.0 # gg08_rnum=2073
H31+ + SO2 -> HSO21+ + H2								RTYPE=2 K1=3.72E-09 K2=-0.5 K3=0.0 # gg08_rnum=2074
H3O1+ + C2S -> HC2S1+ + H2O								RTYPE=2 K1=2.88E-09 K2=-0.5 K3=0.0 # gg08_rnum=2100
H3O1+ + C3S -> HC3S1+ + H2O								RTYPE=2 K1=3.92E-09 K2=-0.5 K3=0.0 # gg08_rnum=2109
H3O1+ + C4S -> HC4S1+ + H2O								RTYPE=2 K1=2.97E-09 K2=-0.5 K3=0.0 # gg08_rnum=2115
H3O1+ + CS -> HCS1+ + H2O								RTYPE=2 K1=4.99E-09 K2=-0.5 K3=0.0 # gg08_rnum=2132
H3O1+ + H2S -> H3S1+ + H2O								RTYPE=2 K1=1.10E-09 K2=-0.5 K3=0.0 # gg08_rnum=2134
H3O1+ + H2S2 -> H3S21+ + H2O							RTYPE=2 K1=1.21E-09 K2=-0.5 K3=0.0 # gg08_rnum=2135
H3O1+ + S2H -> H2S21+ + H2O								RTYPE=2 K1=2.02E-09 K2=-0.5 K3=0.0 # gg08_rnum=2165
H3O1+ + S2 -> S2H1+ + H2O								RTYPE=2 K1=2.00E-09 K2=0.0 K3=0.0 # gg08_rnum=2185
H3S1+ + H -> H2S1+ + H2									RTYPE=2 K1=6.00E-11 K2=0.0 K3=0.0 # gg08_rnum=2214
H3S1+ + H2CO -> CH2OH1+ + H2S							RTYPE=2 K1=2.20E-09 K2=0.0 K3=0.0 # gg08_rnum=2215
H3S1+ + HCN -> H2CN1+ + H2S								RTYPE=2 K1=1.50E-09 K2=0.0 K3=0.0 # gg08_rnum=2216
H3S1+ + HNC -> H2CN1+ + H2S								RTYPE=2 K1=1.50E-09 K2=0.0 K3=0.0 # gg08_rnum=2217
H3S1+ + NH3 -> NH41+ + H2S								RTYPE=2 K1=1.90E-09 K2=0.0 K3=0.0 # gg08_rnum=2218
HCN1+ + S -> HS1+ + CN									RTYPE=2 K1=5.70E-10 K2=0.0 K3=0.0 # gg08_rnum=2247
HCN1+ + S -> S1+ + HCN									RTYPE=2 K1=5.70E-10 K2=0.0 K3=0.0 # gg08_rnum=2248
HCO1+ + C2S -> HC2S1+ + CO								RTYPE=2 K1=2.48E-09 K2=-0.5 K3=0.0 # gg08_rnum=2260
HCO1+ + C3S -> HC3S1+ + CO								RTYPE=2 K1=3.35E-09 K2=-0.5 K3=0.0 # gg08_rnum=2270
HCO1+ + C4S -> HC4S1+ + CO								RTYPE=2 K1=2.52E-09 K2=-0.5 K3=0.0 # gg08_rnum=2276
HCO1+ + CS -> HCS1+ + CO								RTYPE=2 K1=4.30E-09 K2=-0.5 K3=0.0 # gg08_rnum=2319
HCO1+ + H2CS -> H3CS1+ + CO								RTYPE=2 K1=1.50E-09 K2=-0.5 K3=0.0 # gg08_rnum=2322
HCO1+ + H2S -> H3S1+ + CO								RTYPE=2 K1=9.50E-10 K2=-0.5 K3=0.0 # gg08_rnum=2324
HCO1+ + H2S2 -> H3S21+ + CO								RTYPE=2 K1=1.04E-09 K2=-0.5 K3=0.0 # gg08_rnum=2325
HCO1+ + HS -> H2S1+ + CO								RTYPE=2 K1=1.80E-09 K2=-0.5 K3=0.0 # gg08_rnum=2361
HCO1+ + S2H -> H2S21+ + CO								RTYPE=2 K1=1.73E-09 K2=-0.5 K3=0.0 # gg08_rnum=2362
HCO1+ + NS -> HNS1+ + CO								RTYPE=2 K1=3.90E-09 K2=-0.5 K3=0.0 # gg08_rnum=2375
HCO1+ + OCS -> HOCS1+ + CO								RTYPE=2 K1=1.50E-09 K2=-0.5 K3=0.0 # gg08_rnum=2376
HCO1+ + S -> HS1+ + CO									RTYPE=2 K1=3.30E-10 K2=0.0 K3=0.0 # gg08_rnum=2395
HCO1+ + S2 -> S2H1+ + CO								RTYPE=2 K1=2.00E-09 K2=0.0 K3=0.0 # gg08_rnum=2396
HCO1+ + SiS -> HSiS1+ + CO								RTYPE=2 K1=3.60E-09 K2=-0.5 K3=0.0 # gg08_rnum=2413
HCO1+ + SO -> HSO1+ + CO								RTYPE=2 K1=3.30E-09 K2=-0.5 K3=0.0 # gg08_rnum=2414
HCS1+ + NH3 -> NH41+ + CS								RTYPE=2 K1=1.62E-09 K2=-0.5 K3=0.0 # gg08_rnum=2445
HCS1+ + O -> HCO1+ + S									RTYPE=2 K1=5.00E-10 K2=0.0 K3=0.0 # gg08_rnum=2446
HCS1+ + O -> OCS1+ + H									RTYPE=2 K1=5.00E-10 K2=0.0 K3=0.0 # gg08_rnum=2447
He1+ + C2S -> CS1+ + C + He								RTYPE=2 K1=1.40E-09 K2=-0.5 K3=0.0 # gg08_rnum=2477
He1+ + C2S -> C1+ + CS + He								RTYPE=2 K1=1.40E-09 K2=-0.5 K3=0.0 # gg08_rnum=2478
He1+ + C2S -> S1+ + C2 + He								RTYPE=2 K1=1.40E-09 K2=-0.5 K3=0.0 # gg08_rnum=2479
He1+ + C2S -> C21+ + S + He								RTYPE=2 K1=1.40E-09 K2=-0.5 K3=0.0 # gg08_rnum=2480
He1+ + C3S -> C21+ + CS + He							RTYPE=2 K1=3.89E-09 K2=-0.5 K3=0.0 # gg08_rnum=2500
He1+ + C3S -> CS1+ + C2 + He							RTYPE=2 K1=3.89E-09 K2=-0.5 K3=0.0 # gg08_rnum=2501
He1+ + C4S -> C31+ + CS + He							RTYPE=2 K1=2.98E-09 K2=-0.5 K3=0.0 # gg08_rnum=2517
He1+ + C4S -> CS1+ + C3 + He							RTYPE=2 K1=2.98E-09 K2=-0.5 K3=0.0 # gg08_rnum=2518
He1+ + CS -> C1+ + S + He								RTYPE=2 K1=4.70E-09 K2=-0.5 K3=0.0 # gg08_rnum=2639
He1+ + CS -> S1+ + C + He								RTYPE=2 K1=4.70E-09 K2=-0.5 K3=0.0 # gg08_rnum=2640
He1+ + H2CS -> CH21+ + S + He							RTYPE=2 K1=1.11E-09 K2=-0.5 K3=0.0 # gg08_rnum=2646
He1+ + H2CS -> S1+ + CH2 + He							RTYPE=2 K1=1.11E-09 K2=-0.5 K3=0.0 # gg08_rnum=2647
He1+ + H2CS -> CS1+ + H2 + He							RTYPE=2 K1=1.11E-09 K2=-0.5 K3=0.0 # gg08_rnum=2648
He1+ + H2S -> HS1+ + H + He								RTYPE=2 K1=2.20E-10 K2=-0.5 K3=0.0 # gg08_rnum=2652
He1+ + H2S -> H2S1+ + He								RTYPE=2 K1=1.40E-10 K2=-0.5 K3=0.0 # gg08_rnum=2653
He1+ + H2S -> S1+ + H2 + He								RTYPE=2 K1=1.60E-09 K2=-0.5 K3=0.0 # gg08_rnum=2654
He1+ + H2S2 -> HS1+ + HS + He							RTYPE=2 K1=1.20E-09 K2=-0.5 K3=0.0 # gg08_rnum=2655
He1+ + H2S2 -> S2H1+ + H + He							RTYPE=2 K1=1.20E-09 K2=-0.5 K3=0.0 # gg08_rnum=2656
He1+ + HCS -> H1+ + CS + He								RTYPE=2 K1=4.79E-09 K2=-0.5 K3=0.0 # gg08_rnum=2694
He1+ + HCS -> CS1+ + H + He								RTYPE=2 K1=4.79E-09 K2=-0.5 K3=0.0 # gg08_rnum=2695
He1+ + HS -> S1+ + H + He								RTYPE=2 K1=3.70E-09 K2=-0.5 K3=0.0 # gg08_rnum=2758
He1+ + S2H -> S21+ + H + He								RTYPE=2 K1=2.00E-09 K2=-0.5 K3=0.0 # gg08_rnum=2759
He1+ + S2H -> S1+ + HS + He								RTYPE=2 K1=2.00E-09 K2=-0.5 K3=0.0 # gg08_rnum=2760
He1+ + NS -> S1+ + N + He								RTYPE=2 K1=4.30E-09 K2=-0.5 K3=0.0 # gg08_rnum=2789
He1+ + NS -> N1+ + S + He								RTYPE=2 K1=4.30E-09 K2=-0.5 K3=0.0 # gg08_rnum=2790
He1+ + OCS -> S1+ + CO + He								RTYPE=2 K1=8.40E-10 K2=-0.5 K3=0.0 # gg08_rnum=2795
He1+ + OCS -> CO1+ + S + He								RTYPE=2 K1=8.40E-10 K2=-0.5 K3=0.0 # gg08_rnum=2796
He1+ + OCS -> CS1+ + O + He								RTYPE=2 K1=8.40E-10 K2=-0.5 K3=0.0 # gg08_rnum=2797
He1+ + OCS -> O1+ + CS + He								RTYPE=2 K1=8.40E-10 K2=-0.5 K3=0.0 # gg08_rnum=2798
He1+ + S2 -> S1+ + S + He								RTYPE=2 K1=2.00E-09 K2=0.0 K3=0.0 # gg08_rnum=2832
He1+ + SiS -> S1+ + Si + He								RTYPE=2 K1=4.10E-09 K2=-0.5 K3=0.0 # gg08_rnum=2861
He1+ + SiS -> Si1+ + S + He								RTYPE=2 K1=4.10E-09 K2=-0.5 K3=0.0 # gg08_rnum=2862
He1+ + SO -> O1+ + S + He								RTYPE=2 K1=3.70E-09 K2=-0.5 K3=0.0 # gg08_rnum=2863
He1+ + SO -> S1+ + O + He								RTYPE=2 K1=3.70E-09 K2=-0.5 K3=0.0 # gg08_rnum=2864
He1+ + SO2 -> S1+ + O2 + He								RTYPE=2 K1=8.15E-10 K2=-0.5 K3=0.0 # gg08_rnum=2865
He1+ + SO2 -> O21+ + S + He								RTYPE=2 K1=8.15E-10 K2=-0.5 K3=0.0 # gg08_rnum=2866
He1+ + SO2 -> SO1+ + O + He								RTYPE=2 K1=8.15E-10 K2=-0.5 K3=0.0 # gg08_rnum=2867
He1+ + SO2 -> SO21+ + He								RTYPE=2 K1=8.15E-10 K2=-0.5 K3=0.0 # gg08_rnum=2868
HNC1+ + S -> HS1+ + CN									RTYPE=2 K1=5.70E-10 K2=0.0 K3=0.0 # gg08_rnum=2940
HNC1+ + S -> S1+ + HNC									RTYPE=2 K1=5.70E-10 K2=0.0 K3=0.0 # gg08_rnum=2941
HNO1+ + S -> HS1+ + NO									RTYPE=2 K1=1.10E-09 K2=0.0 K3=0.0 # gg08_rnum=2961
HS1+ + C -> CS1+ + H									RTYPE=2 K1=9.90E-10 K2=0.0 K3=0.0 # gg08_rnum=2969
HS1+ + CH -> CH21+ + S									RTYPE=2 K1=5.80E-10 K2=0.0 K3=0.0 # gg08_rnum=2970
HS1+ + CH4 -> H3CS1+ + H2								RTYPE=2 K1=5.40E-10 K2=0.0 K3=0.0 # gg08_rnum=2971
HS1+ + Fe -> Fe1+ + HS									RTYPE=2 K1=1.60E-09 K2=0.0 K3=0.0 # gg08_rnum=2972
HS1+ + H -> S1+ + H2									RTYPE=2 K1=1.10E-10 K2=0.0 K3=0.0 # gg08_rnum=2973
HS1+ + H2O -> H3O1+ + S									RTYPE=2 K1=7.80E-10 K2=0.0 K3=0.0 # gg08_rnum=2974
HS1+ + H2S -> H3S1+ + S									RTYPE=2 K1=2.90E-10 K2=0.0 K3=0.0 # gg08_rnum=2975
HS1+ + H2S -> S2H1+ + H2								RTYPE=2 K1=2.80E-10 K2=0.0 K3=0.0 # gg08_rnum=2976
HS1+ + HCN -> H2CN1+ + S								RTYPE=2 K1=8.90E-10 K2=0.0 K3=0.0 # gg08_rnum=2977
HS1+ + HNC -> H2CN1+ + S								RTYPE=2 K1=8.60E-10 K2=0.0 K3=0.0 # gg08_rnum=2978
HS1+ + Mg -> Mg1+ + HS									RTYPE=2 K1=2.60E-09 K2=0.0 K3=0.0 # gg08_rnum=2979
HS1+ + N -> NS1+ + H									RTYPE=2 K1=7.40E-10 K2=0.0 K3=0.0 # gg08_rnum=2980
HS1+ + Na -> Na1+ + HS									RTYPE=2 K1=2.20E-09 K2=0.0 K3=0.0 # gg08_rnum=2981
HS1+ + NH3 -> NH31+ + HS								RTYPE=2 K1=8.40E-10 K2=0.0 K3=0.0 # gg08_rnum=2982
HS1+ + NH3 -> NH41+ + S									RTYPE=2 K1=7.70E-10 K2=0.0 K3=0.0 # gg08_rnum=2983
HS1+ + NO -> NO1+ + HS									RTYPE=2 K1=4.50E-10 K2=0.0 K3=0.0 # gg08_rnum=2984
HS1+ + O -> SO1+ + H									RTYPE=2 K1=2.90E-10 K2=0.0 K3=0.0 # gg08_rnum=2985
HS1+ + O -> S1+ + OH									RTYPE=2 K1=2.90E-10 K2=0.0 K3=0.0 # gg08_rnum=2986
HS1+ + S -> S1+ + HS									RTYPE=2 K1=9.70E-10 K2=0.0 K3=0.0 # gg08_rnum=2987
HS1+ + Si -> Si1+ + HS									RTYPE=2 K1=1.40E-09 K2=0.0 K3=0.0 # gg08_rnum=2988
HSO21+ + H2O -> H3O1+ + SO2								RTYPE=2 K1=2.13E-09 K2=0.0 K3=0.0 # gg08_rnum=2989
HSO21+ + NH3 -> NH41+ + SO2								RTYPE=2 K1=2.00E-09 K2=0.0 K3=0.0 # gg08_rnum=2990
N1+ + H2S -> HS1+ + NH									RTYPE=2 K1=5.51E-10 K2=0.0 K3=0.0 # gg08_rnum=3020
N1+ + H2S -> H2S1+ + N									RTYPE=2 K1=1.06E-09 K2=0.0 K3=0.0 # gg08_rnum=3021
N1+ + OCS -> S1+ + CO + N								RTYPE=2 K1=3.08E-10 K2=0.0 K3=0.0 # gg08_rnum=3037
N1+ + OCS -> CS1+ + NO									RTYPE=2 K1=7.00E-11 K2=0.0 K3=0.0 # gg08_rnum=3038
N1+ + OCS -> OCS1+ + N									RTYPE=2 K1=1.02E-09 K2=0.0 K3=0.0 # gg08_rnum=3039
N21+ + H2S -> S1+ + N2 + H2								RTYPE=2 K1=2.25E-10 K2=0.0 K3=0.0 # gg08_rnum=3059
N21+ + H2S -> HS1+ + N2 + H								RTYPE=2 K1=1.13E-09 K2=0.0 K3=0.0 # gg08_rnum=3060
N21+ + H2S -> H2S1+ + N2								RTYPE=2 K1=1.50E-10 K2=0.0 K3=0.0 # gg08_rnum=3061
N21+ + OCS -> OCS1+ + N2								RTYPE=2 K1=2.60E-10 K2=0.0 K3=0.0 # gg08_rnum=3075
N21+ + OCS -> S1+ + N2 + CO								RTYPE=2 K1=1.04E-09 K2=0.0 K3=0.0 # gg08_rnum=3076
N21+ + S -> S1+ + N2									RTYPE=2 K1=1.10E-09 K2=0.0 K3=0.0 # gg08_rnum=3078
N2H1+ + S -> HS1+ + N2									RTYPE=2 K1=1.10E-09 K2=0.0 K3=0.0 # gg08_rnum=3103
NH1+ + S -> S1+ + NH									RTYPE=2 K1=6.90E-10 K2=0.0 K3=0.0 # gg08_rnum=3144
NH1+ + S -> HS1+ + N									RTYPE=2 K1=6.90E-10 K2=0.0 K3=0.0 # gg08_rnum=3145
NH1+ + S -> NS1+ + H									RTYPE=2 K1=6.90E-10 K2=0.0 K3=0.0 # gg08_rnum=3146
NH21+ + H2S -> NH31+ + HS								RTYPE=2 K1=4.50E-10 K2=0.0 K3=0.0 # gg08_rnum=3161
NH21+ + H2S -> NH41+ + S								RTYPE=2 K1=1.80E-10 K2=0.0 K3=0.0 # gg08_rnum=3162
NH21+ + H2S -> HS1+ + NH3								RTYPE=2 K1=1.80E-10 K2=0.0 K3=0.0 # gg08_rnum=3163
NH21+ + H2S -> H2S1+ + NH2								RTYPE=2 K1=3.40E-10 K2=0.0 K3=0.0 # gg08_rnum=3164
NH21+ + H2S -> H3S1+ + NH								RTYPE=2 K1=2.40E-10 K2=0.0 K3=0.0 # gg08_rnum=3165
NH21+ + S -> S1+ + NH2									RTYPE=2 K1=4.40E-10 K2=0.0 K3=0.0 # gg08_rnum=3178
NH21+ + S -> HS1+ + NH									RTYPE=2 K1=4.40E-10 K2=0.0 K3=0.0 # gg08_rnum=3179
NH21+ + S -> HNS1+ + H									RTYPE=2 K1=4.40E-10 K2=0.0 K3=0.0 # gg08_rnum=3180
NH31+ + H2S -> NH41+ + HS								RTYPE=2 K1=9.60E-10 K2=-0.5 K3=0.0 # gg08_rnum=3189
NS1+ + O -> NO1+ + S									RTYPE=2 K1=6.10E-10 K2=0.0 K3=0.0 # gg08_rnum=3210
O1+ + H2S -> HS1+ + OH									RTYPE=2 K1=4.20E-10 K2=0.0 K3=0.0 # gg08_rnum=3231
O1+ + H2S -> H2S1+ + O									RTYPE=2 K1=1.80E-09 K2=0.0 K3=0.0 # gg08_rnum=3232
O1+ + H2S -> H2O + S1+									RTYPE=2 K1=2.20E-10 K2=0.0 K3=0.0 # gg08_rnum=3233
O1+ + OCS -> S1+ + CO2									RTYPE=2 K1=2.00E-11 K2=0.0 K3=0.0 # gg08_rnum=3248
O1+ + OCS -> OCS1+ + O									RTYPE=2 K1=6.50E-10 K2=0.0 K3=0.0 # gg08_rnum=3249
O1+ + SO2 -> O21+ + SO									RTYPE=2 K1=8.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3252
O21+ + H2S -> H2S1+ + O2								RTYPE=2 K1=1.40E-09 K2=0.0 K3=0.0 # gg08_rnum=3266
O21+ + S -> SO1+ + O									RTYPE=2 K1=5.40E-10 K2=0.0 K3=0.0 # gg08_rnum=3278
O21+ + S -> S1+ + O2									RTYPE=2 K1=5.40E-10 K2=0.0 K3=0.0 # gg08_rnum=3279
O2H1+ + S -> HS1+ + O2									RTYPE=2 K1=1.10E-09 K2=0.0 K3=0.0 # gg08_rnum=3303
OCS1+ + NH3 -> NH31+ + OCS								RTYPE=2 K1=1.60E-09 K2=-0.5 K3=0.0 # gg08_rnum=3304
OH1+ + H2S -> H2S1+ + OH								RTYPE=2 K1=1.20E-09 K2=0.0 K3=0.0 # gg08_rnum=3324
OH1+ + H2S -> H3S1+ + O									RTYPE=2 K1=8.20E-10 K2=0.0 K3=0.0 # gg08_rnum=3325
OH1+ + S -> HS1+ + O									RTYPE=2 K1=4.30E-10 K2=0.0 K3=0.0 # gg08_rnum=3343
OH1+ + S -> SO1+ + H									RTYPE=2 K1=4.30E-10 K2=0.0 K3=0.0 # gg08_rnum=3344
OH1+ + S -> S1+ + OH									RTYPE=2 K1=4.30E-10 K2=0.0 K3=0.0 # gg08_rnum=3345
S1+ + C2 -> CS1+ + C									RTYPE=2 K1=8.10E-10 K2=0.0 K3=0.0 # gg08_rnum=3380
S1+ + C2H -> C2S1+ + H									RTYPE=2 K1=1.96E-09 K2=-0.5 K3=0.0 # gg08_rnum=3381
S1+ + HC2H -> HC2S1+ + H								RTYPE=2 K1=9.50E-10 K2=0.0 K3=0.0 # gg08_rnum=3382
S1+ + H2C2H -> HC2S1+ + H2								RTYPE=2 K1=1.52E-09 K2=-0.5 K3=0.0 # gg08_rnum=3383
S1+ + C3H -> C3S1+ + H									RTYPE=2 K1=6.87E-09 K2=-0.5 K3=0.0 # gg08_rnum=3384
S1+ + C3H2 -> HC3S1+ + H								RTYPE=2 K1=3.16E-09 K2=-0.5 K3=0.0 # gg08_rnum=3385
S1+ + C4H -> C4S1+ + H									RTYPE=2 K1=1.88E-09 K2=-0.5 K3=0.0 # gg08_rnum=3386
S1+ + HC4H -> C3H21+ + CS								RTYPE=2 K1=2.40E-10 K2=0.0 K3=0.0 # gg08_rnum=3387
S1+ + HC4H -> C4H1+ + HS								RTYPE=2 K1=1.60E-10 K2=0.0 K3=0.0 # gg08_rnum=3388
S1+ + HC4H -> HC4H1+ + S								RTYPE=2 K1=7.20E-10 K2=0.0 K3=0.0 # gg08_rnum=3389
S1+ + HC4H -> HC4S1+ + H								RTYPE=2 K1=4.80E-10 K2=0.0 K3=0.0 # gg08_rnum=3390
S1+ + CH -> CS1+ + H									RTYPE=2 K1=4.40E-09 K2=-0.5 K3=0.0 # gg08_rnum=3391
S1+ + CH2 -> HCS1+ + H									RTYPE=2 K1=7.08E-10 K2=-0.5 K3=0.0 # gg08_rnum=3392
S1+ + CH3 -> H2CS1+ + H									RTYPE=2 K1=1.00E-11 K2=0.0 K3=0.0 # gg08_rnum=3393
S1+ + CH4 -> H3CS1+ + H									RTYPE=2 K1=1.40E-10 K2=0.0 K3=0.0 # gg08_rnum=3394
S1+ + Fe -> Fe1+ + S									RTYPE=2 K1=1.80E-10 K2=0.0 K3=0.0 # gg08_rnum=3395
S1+ + H2CO -> HCO1+ + HS								RTYPE=2 K1=1.10E-09 K2=-0.5 K3=0.0 # gg08_rnum=3396
S1+ + H2CO -> H2S1+ + CO								RTYPE=2 K1=1.10E-09 K2=-0.5 K3=0.0 # gg08_rnum=3397
S1+ + H2S -> S21+ + H2									RTYPE=2 K1=6.40E-10 K2=-0.5 K3=0.0 # gg08_rnum=3398
S1+ + H2S -> S2H1+ + H									RTYPE=2 K1=2.00E-10 K2=-0.5 K3=0.0 # gg08_rnum=3399
S1+ + H2S -> H2S1+ + S									RTYPE=2 K1=4.40E-11 K2=-0.5 K3=0.0 # gg08_rnum=3400
S1+ + HCO -> HCO1+ + S									RTYPE=2 K1=5.00E-10 K2=-0.5 K3=0.0 # gg08_rnum=3401
S1+ + HCO -> HS1+ + CO									RTYPE=2 K1=5.00E-10 K2=-0.5 K3=0.0 # gg08_rnum=3402
S1+ + Mg -> Mg1+ + S									RTYPE=2 K1=2.80E-10 K2=0.0 K3=0.0 # gg08_rnum=3403
S1+ + Na -> Na1+ + S									RTYPE=2 K1=2.60E-10 K2=0.0 K3=0.0 # gg08_rnum=3404
S1+ + NH -> NS1+ + H									RTYPE=2 K1=3.70E-09 K2=-0.5 K3=0.0 # gg08_rnum=3405
S1+ + NH3 -> NH31+ + S									RTYPE=2 K1=1.60E-09 K2=-0.5 K3=0.0 # gg08_rnum=3406
S1+ + NO -> NO1+ + S									RTYPE=2 K1=3.60E-10 K2=-0.5 K3=0.0 # gg08_rnum=3407
S1+ + O2 -> SO1+ + O									RTYPE=2 K1=2.00E-11 K2=0.0 K3=0.0 # gg08_rnum=3408
S1+ + OCS -> S21+ + CO									RTYPE=2 K1=9.10E-10 K2=-0.5 K3=0.0 # gg08_rnum=3409
S1+ + OH -> SO1+ + H									RTYPE=2 K1=4.60E-09 K2=-0.5 K3=0.0 # gg08_rnum=3410
S1+ + Si -> Si1+ + S									RTYPE=2 K1=1.60E-09 K2=0.0 K3=0.0 # gg08_rnum=3411
S1+ + SiC -> SiC1+ + S									RTYPE=2 K1=3.70E-09 K2=-0.5 K3=0.0 # gg08_rnum=3412
S1+ + SiH -> SiS1+ + H									RTYPE=2 K1=1.41E-10 K2=-0.5 K3=0.0 # gg08_rnum=3413
S1+ + SiH -> SiH1+ + S									RTYPE=2 K1=1.41E-10 K2=-0.5 K3=0.0 # gg08_rnum=3414
S1+ + SiS -> SiS1+ + S									RTYPE=2 K1=3.50E-09 K2=-0.5 K3=0.0 # gg08_rnum=3415
S21+ + CH3CH2OH -> H2S21+ + CH3CHO						RTYPE=2 K1=8.50E-11 K2=0.0 K3=0.0 # gg08_rnum=3416
S21+ + CH3CH2OH -> CH3CH2O1+ + S2H						RTYPE=2 K1=1.60E-09 K2=0.0 K3=0.0 # gg08_rnum=3417
S21+ + CH3OCH3 -> CH3CH2O1+ + S2H						RTYPE=2 K1=1.60E-09 K2=0.0 K3=0.0 # gg08_rnum=3418
S21+ + NO -> NO1+ + S2									RTYPE=2 K1=5.10E-10 K2=0.0 K3=0.0 # gg08_rnum=3419
S2H1+ + H2 -> H3S21+									RTYPE=2 K1=1.00E-14 K2=-1.00E+00 K3=0.0 # gg08_rnum=3420
S2H1+ + H2S -> H3S1+ + S2								RTYPE=2 K1=7.95E-10 K2=-0.5 K3=0.0 # gg08_rnum=3421
Si1+ + OCS -> SiS1+ + CO								RTYPE=2 K1=1.50E-09 K2=-0.5 K3=0.0 # gg08_rnum=3443
SiH21+ + S -> HSiS1+ + H								RTYPE=2 K1=1.10E-09 K2=0.0 K3=0.0 # gg08_rnum=3467
SiO1+ + S -> Si1+ + SO									RTYPE=2 K1=1.00E-09 K2=0.0 K3=0.0 # gg08_rnum=3499
SiS1+ + H -> Si1+ + HS									RTYPE=2 K1=1.90E-09 K2=0.0 K3=0.0 # gg08_rnum=3500
SiS1+ + H2 -> HSiS1+ + H								RTYPE=2 K1=1.50E-09 K2=0.0 K3=0.0 # gg08_rnum=3501
SO1+ + Fe -> Fe1+ + SO									RTYPE=2 K1=1.60E-09 K2=0.0 K3=0.0 # gg08_rnum=3502
SO1+ + H2S -> S21+ + H2O								RTYPE=2 K1=1.10E-09 K2=0.0 K3=0.0 # gg08_rnum=3503
SO1+ + Mg -> Mg1+ + SO									RTYPE=2 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3504
SO1+ + N -> NS1+ + O									RTYPE=2 K1=5.00E-11 K2=0.0 K3=0.0 # gg08_rnum=3505
SO1+ + Na -> Na1+ + SO									RTYPE=2 K1=2.30E-09 K2=0.0 K3=0.0 # gg08_rnum=3506
SO1+ + NH3 -> NH31+ + SO								RTYPE=2 K1=1.30E-09 K2=0.0 K3=0.0 # gg08_rnum=3507
SO21+ + CO -> SO1+ + CO2								RTYPE=2 K1=3.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3508
SO21+ + H -> SO1+ + OH									RTYPE=2 K1=4.20E-10 K2=0.0 K3=0.0 # gg08_rnum=3509
SO21+ + H2 -> HSO21+ + H								RTYPE=2 K1=5.00E-12 K2=0.0 K3=0.0 # gg08_rnum=3510
SO21+ + O2 -> O21+ + SO2								RTYPE=2 K1=2.50E-10 K2=0.0 K3=0.0 # gg08_rnum=3511
HS1+ + H2 -> H3S1+										RTYPE=4 K1=1.40E-16 K2=-6.00E-01 K3=0.0 # gg08_rnum=3584
OCS1+ + H2 -> HOCSH1+									RTYPE=4 K1=6.10E-20 K2=-1.50E+00 K3=0.0 # gg08_rnum=3588
S1+ + H2 -> H2S1+										RTYPE=4 K1=1.00E-17 K2=-2.00E-01 K3=0.0 # gg08_rnum=3591
S2H1+ + H2 -> H3S21+									RTYPE=4 K1=1.00E-14 K2=-1.00E+00 K3=0.0 # gg08_rnum=3592
S1- + C -> CS + E1-										RTYPE=5 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3642
S1- + CO -> OCS + E1-									RTYPE=5 K1=3.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3643
S1- + H -> HS + E1-										RTYPE=5 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3644
S1- + N -> NS + E1-										RTYPE=5 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3645
S1- + O -> SO + E1-										RTYPE=5 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3646
S1- + O2 -> SO2 + E1-									RTYPE=5 K1=3.00E-11 K2=0.0 K3=0.0 # gg08_rnum=3647
C + CS -> S + C2										RTYPE=7 K1=1.44E-11 K2=5.00E-01 K3=2.04E+04 # gg08_rnum=3696
C + HS -> CS + H										RTYPE=7 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3703
C + HS -> S + CH										RTYPE=7 K1=1.20E-11 K2=5.80E-01 K3=5.88E+03 # gg08_rnum=3704
C + NS -> CN + S										RTYPE=7 K1=1.50E-10 K2=-1.60E-01 K3=0.0 # gg08_rnum=3713
C + NS -> CS + N										RTYPE=7 K1=1.73E-11 K2=5.00E-01 K3=4.00E+03 # gg08_rnum=3714
C + S2 -> S + CS										RTYPE=7 K1=7.00E-11 K2=0.0 K3=0.0 # gg08_rnum=3720
C + SO -> CS + O										RTYPE=7 K1=3.50E-11 K2=0.0 K3=0.0 # gg08_rnum=3724
C + SO -> CO + S										RTYPE=7 K1=3.50E-11 K2=0.0 K3=0.0 # gg08_rnum=3725
C + SO2 -> CO + SO										RTYPE=7 K1=7.00E-11 K2=0.0 K3=0.0 # gg08_rnum=3726
C2H + CS -> C3S + H										RTYPE=7 K1=1.70E-11 K2=0.0 K3=0.0 # gg08_rnum=3746
C2H + S -> C2S + H										RTYPE=7 K1=1.70E-11 K2=0.0 K3=0.0 # gg08_rnum=3755
CH + S -> HS + C										RTYPE=7 K1=1.73E-11 K2=5.00E-01 K3=4.00E+03 # gg08_rnum=3788
CH3 + H2S -> HS + CH4									RTYPE=7 K1=3.30E-13 K2=0.0 K3=1.10E+03 # gg08_rnum=3815
CN + S -> NS + C										RTYPE=7 K1=5.71E-11 K2=5.00E-01 K3=3.20E+04 # gg08_rnum=3847
CN + S -> CS + N										RTYPE=7 K1=1.73E-11 K2=5.00E-01 K3=0.0 # gg08_rnum=3848
CO + HS -> OCS + H										RTYPE=7 K1=5.95E-14 K2=1.12E+00 K3=8.33E+03 # gg08_rnum=3850
H + H2S -> HS + H2										RTYPE=7 K1=6.60E-11 K2=0.0 K3=1.35E+03 # gg08_rnum=3869
H + HS -> S + H2										RTYPE=7 K1=2.50E-11 K2=0.0 K3=0.0 # gg08_rnum=3878
H + NS -> HS + N										RTYPE=7 K1=7.27E-11 K2=5.00E-01 K3=1.57E+04 # gg08_rnum=3887
H + NS -> S + NH										RTYPE=7 K1=7.27E-11 K2=5.00E-01 K3=2.07E+04 # gg08_rnum=3888
H + OCS -> CO + HS										RTYPE=7 K1=1.23E-11 K2=0.0 K3=1.95E+03 # gg08_rnum=3893
H + S2 -> HS + S										RTYPE=7 K1=2.25E-10 K2=5.00E-01 K3=8.36E+03 # gg08_rnum=3895
H + SO -> HS + O										RTYPE=7 K1=1.73E-11 K2=5.00E-01 K3=1.99E+04 # gg08_rnum=3896
H + SO -> S + OH										RTYPE=7 K1=5.90E-10 K2=-3.10E-01 K3=1.11E+04 # gg08_rnum=3897
H2 + HS -> H2S + H										RTYPE=7 K1=6.52E-12 K2=9.00E-02 K3=8.05E+03 # gg08_rnum=3904
H2 + S -> HS + H										RTYPE=7 K1=1.76E-13 K2=2.88E+00 K3=6.13E+03 # gg08_rnum=3913
HS + HS -> H2S + S										RTYPE=7 K1=3.00E-11 K2=0.0 K3=0.0 # gg08_rnum=3925
N + CS -> S + CN										RTYPE=7 K1=3.80E-11 K2=5.00E-01 K3=1.16E+03 # gg08_rnum=3966
N + HCS -> HCN + S										RTYPE=7 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3971
N + HS -> NS + H										RTYPE=7 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=3974
N + HS -> S + NH										RTYPE=7 K1=1.73E-11 K2=5.00E-01 K3=9.06E+03 # gg08_rnum=3975
N + NS -> N2 + S										RTYPE=7 K1=3.00E-11 K2=-6.00E-01 K3=0.0 # gg08_rnum=3982
N + S2 -> NS + S										RTYPE=7 K1=1.73E-11 K2=5.00E-01 K3=4.00E+03 # gg08_rnum=3991
N + SO -> NO + S										RTYPE=7 K1=1.50E-11 K2=0.0 K3=3.68E+03 # gg08_rnum=3997
N + SO -> NS + O										RTYPE=7 K1=4.68E-11 K2=5.00E-01 K3=8.25E+03 # gg08_rnum=3998
NH + S -> HS + N										RTYPE=7 K1=1.73E-11 K2=5.00E-01 K3=4.00E+03 # gg08_rnum=4013
NO + S -> SO + N										RTYPE=7 K1=1.75E-10 K2=0.0 K3=2.02E+04 # gg08_rnum=4022
NO + S -> NS + O										RTYPE=7 K1=2.94E-11 K2=5.00E-01 K3=1.75E+04 # gg08_rnum=4023
O + CS -> CO + S										RTYPE=7 K1=1.94E-11 K2=0.0 K3=2.31E+02 # gg08_rnum=4077
O + CS -> SO + C										RTYPE=7 K1=4.68E-11 K2=5.00E-01 K3=2.89E+04 # gg08_rnum=4078
O + H2S -> HS + OH										RTYPE=7 K1=9.22E-12 K2=0.0 K3=1.80E+03 # gg08_rnum=4085
O + HCS -> OCS + H										RTYPE=7 K1=5.00E-11 K2=0.0 K3=0.0 # gg08_rnum=4093
O + HCS -> OH + CS										RTYPE=7 K1=5.00E-11 K2=0.0 K3=0.0 # gg08_rnum=4094
O + HS -> S + OH										RTYPE=7 K1=1.74E-11 K2=6.70E-01 K3=9.56E+02 # gg08_rnum=4100
O + HS -> SO + H										RTYPE=7 K1=1.60E-10 K2=5.00E-01 K3=0.0 # gg08_rnum=4101
O + NS -> SO + N										RTYPE=7 K1=1.00E-11 K2=0.0 K3=0.0 # gg08_rnum=4113
O + NS -> NO + S										RTYPE=7 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=4114
O + OCS -> SO + CO										RTYPE=7 K1=1.60E-11 K2=0.0 K3=2.15E+03 # gg08_rnum=4118
O + OCS -> CO2 + S										RTYPE=7 K1=8.30E-11 K2=0.0 K3=5.53E+03 # gg08_rnum=4119
O + S2 -> SO + S										RTYPE=7 K1=1.70E-11 K2=0.0 K3=0.0 # gg08_rnum=4125
O + SO -> S + O2										RTYPE=7 K1=6.60E-13 K2=0.0 K3=2.76E+03 # gg08_rnum=4138
O + SO2 -> SO + O2										RTYPE=7 K1=9.01E-12 K2=0.0 K3=9.84E+03 # gg08_rnum=4139
O2 + SO -> SO2 + O										RTYPE=7 K1=1.10E-14 K2=1.89E+00 K3=1.54E+03 # gg08_rnum=4144
OH + CS -> OCS + H										RTYPE=7 K1=9.39E-14 K2=1.12E+00 K3=8.00E+02 # gg08_rnum=4154
OH + H2S -> H2O + HS									RTYPE=7 K1=6.00E-12 K2=0.0 K3=7.50E+01 # gg08_rnum=4158
OH + S -> HS + O										RTYPE=7 K1=6.60E-11 K2=0.0 K3=0.0 # gg08_rnum=4172
OH + SO -> SO2 + H										RTYPE=7 K1=8.60E-11 K2=0.0 K3=0.0 # gg08_rnum=4174
S + C2 -> CS + C										RTYPE=7 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=4176
S + CH -> CS + H										RTYPE=7 K1=5.00E-11 K2=0.0 K3=0.0 # gg08_rnum=4177
S + CH2 -> CS + H2										RTYPE=7 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=4178
S + CH2 -> HCS + H										RTYPE=7 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=4179
S + CH3 -> H2CS + H										RTYPE=7 K1=1.40E-10 K2=0.0 K3=0.0 # gg08_rnum=4180
S + HS -> S2 + H										RTYPE=7 K1=4.50E-11 K2=0.0 K3=0.0 # gg08_rnum=4181
S + NH -> NS + H										RTYPE=7 K1=1.00E-10 K2=0.0 K3=0.0 # gg08_rnum=4182
S + O2 -> SO + O										RTYPE=7 K1=2.30E-12 K2=0.0 K3=0.0 # gg08_rnum=4183
S + OH -> SO + H										RTYPE=7 K1=6.60E-11 K2=0.0 K3=0.0 # gg08_rnum=4184
S + SO -> S2 + O										RTYPE=7 K1=1.73E-11 K2=5.00E-01 K3=1.15E+04 # gg08_rnum=4185
O + SO -> SO2											RTYPE=8 K1=3.20E-16 K2=-1.60E+00 K3=0.0 # gg08_rnum=4205
S + CO -> OCS											RTYPE=8 K1=1.60E-17 K2=-1.50E+00 K3=0.0 # gg08_rnum=4207
C2S1+ + E1- -> CS + C									RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4265
C2S1+ + E1- -> C2 + S									RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4266
C3S1+ + E1- -> C3 + S									RTYPE=9 K1=1.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4304
C3S1+ + E1- -> C2 + CS									RTYPE=9 K1=1.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4305
C3S1+ + E1- -> C2S + C									RTYPE=9 K1=1.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4306
C4S1+ + E1- -> C2S + C2									RTYPE=9 K1=1.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4335
C4S1+ + E1- -> C3 + CS									RTYPE=9 K1=1.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4336
C4S1+ + E1- -> C3S + C									RTYPE=9 K1=1.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4337
CS1+ + E1- -> C + S										RTYPE=9 K1=2.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4509
H2CS1+ + E1- -> HCS + H									RTYPE=9 K1=3.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4525
H2CS1+ + E1- -> CS + H + H								RTYPE=9 K1=3.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4526
H2S1+ + E1- -> S + H + H								RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4536
H2S1+ + E1- -> HS + H									RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4537
H2S21+ + E1- -> HS + HS									RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4538
H2S21+ + E1- -> S2H + H									RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4539
H3CS1+ + E1- -> H2CS + H								RTYPE=9 K1=3.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4547
H3CS1+ + E1- -> CS + H + H2								RTYPE=9 K1=3.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4548
H3S1+ + E1- -> H2S + H									RTYPE=9 K1=3.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4553
H3S1+ + E1- -> HS + H + H								RTYPE=9 K1=1.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4554
H3S21+ + E1- -> H2S2 + H								RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4555
H3S21+ + E1- -> S2H + H2								RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4556
HC2S1+ + E1- -> C2S + H									RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4565
HC2S1+ + E1- -> CS + CH									RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4566
HC3S1+ + E1- -> C3S + H									RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4568
HC3S1+ + E1- -> C2S + CH								RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4569
HC4S1+ + E1- -> C3S + CH								RTYPE=9 K1=1.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4573
HC4S1+ + E1- -> C4S + H									RTYPE=9 K1=1.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4574
HC4S1+ + E1- -> C2S + C2H								RTYPE=9 K1=1.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4575
HCS1+ + E1- -> CS + H									RTYPE=9 K1=1.84E-07 K2=-5.70E-01 K3=0.0 # gg08_rnum=4586
HCS1+ + E1- -> CH + S									RTYPE=9 K1=7.87E-07 K2=-5.70E-01 K3=0.0 # gg08_rnum=4587
HNS1+ + E1- -> NS + H									RTYPE=9 K1=3.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4682
HOCS1+ + E1- -> OCS + H									RTYPE=9 K1=2.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4693
HOCS1+ + E1- -> OH + CS									RTYPE=9 K1=2.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4694
HS1+ + E1- -> S + H										RTYPE=9 K1=2.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4712
HSiS1+ + E1- -> Si + HS									RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4717
HSiS1+ + E1- -> SiS + H									RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4718
HSO1+ + E1- -> SO + H									RTYPE=9 K1=2.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4719
HSO21+ + E1- -> SO2 + H									RTYPE=9 K1=2.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4720
HSO21+ + E1- -> SO + H + O								RTYPE=9 K1=1.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4721
HSO21+ + E1- -> SO + OH									RTYPE=9 K1=1.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4722
NS1+ + E1- -> N + S										RTYPE=9 K1=2.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4760
OCS1+ + E1- -> CO + S									RTYPE=9 K1=3.00E-07 K2=0.0 K3=0.0 # gg08_rnum=4763
OCS1+ + E1- -> CS + O									RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4764
HOCSH1+ + E1- -> OCS + H2								RTYPE=9 K1=3.00E-07 K2=0.0 K3=0.0 # gg08_rnum=4765
S21+ + E1- -> S + S										RTYPE=9 K1=2.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4872
S2H1+ + E1- -> HS + S									RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4873
S2H1+ + E1- -> S2 + H									RTYPE=9 K1=1.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4874
SiS1+ + E1- -> Si + S									RTYPE=9 K1=2.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4915
SO1+ + E1- -> O + S										RTYPE=9 K1=2.00E-07 K2=-0.5 K3=0.0 # gg08_rnum=4916
SO21+ + E1- -> SO + O									RTYPE=9 K1=2.50E-07 K2=-0.5 K3=0.0 # gg08_rnum=4917
H2CS1+ + E1- -> H2CS									RTYPE=10 K1=1.10E-10 K2=-7.00E-01 K3=0.0 # gg08_rnum=5076
H2S1+ + E1- -> H2S										RTYPE=10 K1=1.10E-10 K2=-7.00E-01 K3=0.0 # gg08_rnum=5077
S1+ + E1- -> S											RTYPE=10 K1=3.90E-12 K2=-6.30E-01 K3=0.0 # gg08_rnum=5083
C1+ + S1- -> C + S										RTYPE=11 K1=2.30E-07 K2=-0.5 K3=0.0 # gg08_rnum=5087
Fe1+ + S1- -> Fe + S									RTYPE=11 K1=2.30E-07 K2=-0.5 K3=0.0 # gg08_rnum=5090
H1+ + S1- -> H + S										RTYPE=11 K1=2.30E-07 K2=-0.5 K3=0.0 # gg08_rnum=5093
He1+ + S1- -> He + S									RTYPE=11 K1=2.30E-07 K2=-0.5 K3=0.0 # gg08_rnum=5101
Mg1+ + S1- -> Mg + S									RTYPE=11 K1=2.30E-07 K2=-0.5 K3=0.0 # gg08_rnum=5104
N1+ + S1- -> N + S										RTYPE=11 K1=2.30E-07 K2=-0.5 K3=0.0 # gg08_rnum=5107
Na1+ + S1- -> Na + S									RTYPE=11 K1=2.30E-07 K2=-0.5 K3=0.0 # gg08_rnum=5110
O1+ + S1- -> O + S										RTYPE=11 K1=2.30E-07 K2=-0.5 K3=0.0 # gg08_rnum=5114
S1+ + C1- -> S + C										RTYPE=11 K1=2.30E-07 K2=-0.5 K3=0.0 # gg08_rnum=5115
S1+ + H1- -> S + H										RTYPE=11 K1=2.30E-07 K2=-0.5 K3=0.0 # gg08_rnum=5116
S1+ + S1- -> S + S										RTYPE=11 K1=2.30E-07 K2=-0.5 K3=0.0 # gg08_rnum=5117
Si1+ + S1- -> Si + S									RTYPE=11 K1=2.30E-07 K2=-0.5 K3=0.0 # gg08_rnum=5120
S + E1- -> S1-											RTYPE=12 K1=5.00E-15 K2=0.0 K3=0.0 # gg08_rnum=5124
H2S2 -> H2S21+ + E1-									RTYPE=13 K1=2.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=5174
HCS -> HCS1+ + E1-										RTYPE=13 K1=2.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=5183
S2H -> S2H1+ + E1-										RTYPE=13 K1=2.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=5187
OCS -> OCS1+ + E1-										RTYPE=13 K1=2.37E-10 K2=0.0 K3=2.71E+00 # gg08_rnum=5197
S -> S1+ + E1-											RTYPE=13 K1=7.20E-10 K2=0.0 K3=2.40E+00 # gg08_rnum=5205
S1- -> S + E1-											RTYPE=13 K1=2.40E-07 K2=0.0 K3=9.00E-01 # gg08_rnum=5206
S2 -> S21+ + E1-										RTYPE=13 K1=2.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=5207
C2S -> C2 + S											RTYPE=13 K1=1.00E-10 K2=0.0 K3=2.00E+00 # gg08_rnum=5236
C3S -> C2 + CS											RTYPE=13 K1=1.00E-10 K2=0.0 K3=2.00E+00 # gg08_rnum=5249
C4S -> C3 + CS											RTYPE=13 K1=1.00E-10 K2=0.0 K3=2.00E+00 # gg08_rnum=5259
CS -> C + S												RTYPE=13 K1=9.70E-10 K2=0.0 K3=2.00E+00 # gg08_rnum=5326
H2CS -> CS + H2											RTYPE=13 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=5332
H2S -> HS + H											RTYPE=13 K1=3.20E-10 K2=0.0 K3=1.70E+00 # gg08_rnum=5335
H2S2 -> HS + HS											RTYPE=13 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=5336
HS -> H + S												RTYPE=13 K1=1.00E-11 K2=0.0 K3=2.00E+00 # gg08_rnum=5379
S2H -> HS + S											RTYPE=13 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=5380
NS -> N + S												RTYPE=13 K1=1.00E-11 K2=0.0 K3=2.00E+00 # gg08_rnum=5394
OCS -> CO + S											RTYPE=13 K1=2.28E-09 K2=0.0 K3=1.60E+00 # gg08_rnum=5399
S2 -> S + S												RTYPE=13 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=5421
SiS -> Si + S											RTYPE=13 K1=1.00E-10 K2=0.0 K3=2.30E+00 # gg08_rnum=5440
SO -> O + S												RTYPE=13 K1=3.30E-10 K2=0.0 K3=1.40E+00 # gg08_rnum=5441
SO2 -> SO + O											RTYPE=13 K1=1.05E-09 K2=0.0 K3=1.74E+00 # gg08_rnum=5442
gC + gC2S -> gC3S										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5483, gg08_EA_ref='EH'
gC + gC2S -> C3S										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5484
gC + gHS -> gCS + gH									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5519, gg08_EA_ref='M84'
gC + gHS -> CS + H										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5520
gC + gNS -> gCN + gS									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5531, gg08_EA_ref='M84'
gC + gNS -> CN + S										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5532
gC + gS -> gCS											RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5543, gg08_EA_ref='EH'
gC + gS -> CS											RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5544
gC + gSO -> gCO + gS									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5545, gg08_EA_ref='M84'
gC + gSO -> CO + S										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5546
gH + gCS -> gHCS										RTYPE=14 K1=1.00E+00 K2=0.0 K3=1.00E+03 # gg08_rnum=5877, gg08_EA_ref='EH'
gH + gCS -> HCS											RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5878
gH + gH2S -> gH2 + gHS									RTYPE=14 K1=1.00E+00 K2=0.0 K3=8.60E+02 # gg08_rnum=5903, gg08_EA_ref='TH 8.60E+02 1.35E+03'
gH + gH2S -> H2 + HS									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5904
gH + gHCS -> gH2CS										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5931, gg08_EA_ref='guess. Exoth OK'
gH + gHCS -> H2CS										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5932
gH + gHS -> gH2S										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5957, gg08_EA_ref='EH'
gH + gHS -> H2S											RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5958
gH + gOCS -> gCO + gHS									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5989, gg08_EA_ref='M84'
gH + gOCS -> CO + HS									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=5990
gH + gS -> gHS											RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6001, gg08_EA_ref='EH'
gH + gS -> HS											RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6002
gH + gSO2 -> gO2 + gHS									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6011, gg08_EA_ref='EH 92 Summer'
gH + gSO2 -> O2 + HS									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6012
gN + gHS -> gNS + gH									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6111, gg08_EA_ref='M84'
gN + gHS -> NS + H										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6112
gN + gNS -> gN2 + gS									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6119, gg08_EA_ref='M84'
gN + gNS -> N2 + S										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6120
gN + gS -> gNS											RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6125, gg08_EA_ref='Hasegawa'
gN + gS -> NS											RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6126
gO + gCS -> gOCS										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6237, gg08_EA_ref='TH'
gO + gCS -> OCS											RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6238
gO + gHS -> gSO + gH									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6249, gg08_EA_ref='M84'
gO + gHS -> SO + H										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6250
gO + gNS -> gNO + gS									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6255, gg08_EA_ref='M84'
gO + gNS -> NO + S										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6256
gO + gS -> gSO											RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6265, gg08_EA_ref='TH'
gO + gS -> SO											RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6266
gO + gSO -> gSO2										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6267, gg08_EA_ref='TH'
gO + gSO -> SO2											RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6268
gS + gCH -> gHCS										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6329, gg08_EA_ref='hasegawa'
gS + gCH -> HCS											RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6330
gS + gCH3 -> gH2CS + gH									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6331, gg08_EA_ref='M84'
gS + gCH3 -> H2CS + H									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6332
gS + gCO -> gOCS										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6333, gg08_EA_ref='From our gas network'
gS + gCO -> OCS											RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6334
gS + gNH -> gNS + gH									RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6335, gg08_EA_ref='M84'
gS + gNH -> NS + H										RTYPE=14 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6336
gC2S -> C2S												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6350
gC3S -> C3S												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6360
gC4S -> C4S												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6368
gCS -> CS												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6433
gH2CS -> H2CS											RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6445
gH2S -> H2S												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6448
gH2S2 -> H2S2											RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6449
gHCS -> HCS												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6472
gHS -> HS												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6497
gS2H -> S2H												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6498
gNS -> NS												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6518
gOCS -> OCS												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6524
gS -> S													RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6542
gS2 -> S2												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6543
gSiS -> SiS												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6562
gSO -> SO												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6563
gSO2 -> SO2												RTYPE=15 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6564
gC2S -> C2S												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6597
gC3S -> C3S												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6607
gC4S -> C4S												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6615
gCS -> CS												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6680
gH2CS -> H2CS											RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6692
gH2S -> H2S												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6695
gH2S2 -> H2S2											RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6696
gHCS -> HCS												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6719
gHS -> HS												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6744
gS2H -> S2H												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6745
gNS -> NS												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6765
gOCS -> OCS												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6771
gS -> S													RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6789
gS2 -> S2												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6790
gSiS -> SiS												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6809
gSO -> SO												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6810
gSO2 -> SO2												RTYPE=16 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=6811
gC2S -> gCS + gC										RTYPE=17 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=6845
gC3S -> gC2 + gCS										RTYPE=17 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=6855
gC4S -> gC3 + gCS										RTYPE=17 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=6864
gCS -> gC + gS											RTYPE=17 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=6932
gH2CS -> gHCS + gH										RTYPE=17 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=6942
gH2S -> gH2 + gS										RTYPE=17 K1=5.15E+03 K2=0.0 K3=0.0 # gg08_rnum=6945
gH2S2 -> gHS + gHS										RTYPE=17 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=6946
gHCS -> gCH + gS										RTYPE=17 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=6969
gHCS -> gCS + gH										RTYPE=17 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=6970
gHS -> gH + gS											RTYPE=17 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=7002
gS2H -> gHS + gS										RTYPE=17 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=7003
gNS -> gN + gS											RTYPE=17 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=7021
gOCS -> gCO + gS										RTYPE=17 K1=5.35E+03 K2=0.0 K3=0.0 # gg08_rnum=7027
gS2 -> gS + gS											RTYPE=17 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=7048
gSiS -> gSi + gS										RTYPE=17 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=7066
gSO -> gS + gO											RTYPE=17 K1=5.00E+02 K2=0.0 K3=0.0 # gg08_rnum=7067
gSO2 -> gSO + gO										RTYPE=17 K1=1.88E+03 K2=0.0 K3=0.0 # gg08_rnum=7068
gH2S -> gS + gH + gH									RTYPE=18 K1=8.50E+02 K2=0.0 K3=0.0 # gg08_rnum=7136
gH2S -> gHS + gH										RTYPE=18 K1=8.50E+02 K2=0.0 K3=0.0 # gg08_rnum=7137
gHCS -> gCS + gH										RTYPE=18 K1=1.50E+03 K2=0.0 K3=0.0 # gg08_rnum=7140
gOCS -> gCS + gO										RTYPE=18 K1=4.80E+02 K2=0.0 K3=0.0 # gg08_rnum=7148
gOCS -> gCO + gS										RTYPE=18 K1=9.60E+02 K2=0.0 K3=0.0 # gg08_rnum=7149
gC2S -> gC2 + gS										RTYPE=19 K1=1.00E-10 K2=0.0 K3=2.00E+00 # gg08_rnum=7164
gC3S -> gC2 + gCS										RTYPE=19 K1=1.00E-10 K2=0.0 K3=2.00E+00 # gg08_rnum=7177
gC4S -> gC3 + gCS										RTYPE=19 K1=1.00E-10 K2=0.0 K3=2.00E+00 # gg08_rnum=7188
gCS -> gC + gS											RTYPE=19 K1=9.70E-10 K2=0.0 K3=2.00E+00 # gg08_rnum=7267
gH2CS -> gCS + gH2										RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7276
gH2S -> gHS + gH										RTYPE=19 K1=3.20E-10 K2=0.0 K3=1.70E+00 # gg08_rnum=7279
gH2S2 -> gHS + gHS										RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7280
gHS -> gH + gS											RTYPE=19 K1=1.00E-11 K2=0.0 K3=2.00E+00 # gg08_rnum=7334
gS2H -> gHS + gS										RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7335
gNS -> gN + gS											RTYPE=19 K1=1.00E-11 K2=0.0 K3=2.00E+00 # gg08_rnum=7353
gOCS -> gCO + gS										RTYPE=19 K1=2.28E-09 K2=0.0 K3=1.60E+00 # gg08_rnum=7359
gS2 -> gS + gS											RTYPE=19 K1=1.00E-09 K2=0.0 K3=1.70E+00 # gg08_rnum=7380
gSiS -> gSi + gS										RTYPE=19 K1=1.00E-10 K2=0.0 K3=2.30E+00 # gg08_rnum=7398
gSO -> gO + gS											RTYPE=19 K1=3.30E-10 K2=0.0 K3=1.40E+00 # gg08_rnum=7399
gSO2 -> gSO + gO										RTYPE=19 K1=1.05E-09 K2=0.0 K3=1.74E+00 # gg08_rnum=7400
gH2S2 -> gHS + gHS										RTYPE=20 K1=1.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=7523
gH2S2 -> gS2H + gH										RTYPE=20 K1=1.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=7524
gHCS -> gCS + gH										RTYPE=20 K1=2.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=7539
gS2H -> gHS + gS										RTYPE=20 K1=1.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=7546
gS2H -> gS2 + gH										RTYPE=20 K1=1.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=7547
gOCS -> gCS + gO										RTYPE=20 K1=7.90E-11 K2=0.0 K3=2.71E+00 # gg08_rnum=7556
gOCS -> gCO + gS										RTYPE=20 K1=1.58E-10 K2=0.0 K3=2.71E+00 # gg08_rnum=7557
gS2 -> gS + gS											RTYPE=20 K1=2.00E-10 K2=0.0 K3=2.50E+00 # gg08_rnum=7564
C2S -> gC2S												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7604
C3S -> gC3S												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7614
C4S -> gC4S												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7622
CS -> gCS												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7687
H2CS -> gH2CS											RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7699
H2S -> gH2S												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7702
H2S2 -> gH2S2											RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7703
HCS -> gHCS												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7726
HS -> gHS												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7751
S2H -> gS2H												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7752
NS -> gNS												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7772
OCS -> gOCS												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7778
S -> gS													RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7796
S2 -> gS2												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7797
SiS -> gSiS												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7816
SO -> gSO												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7817
SO2 -> gSO2												RTYPE=99 K1=1.00E+00 K2=0.0 K3=0.0 # gg08_rnum=7818

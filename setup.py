import sys
import setuptools

with open("README.md", "r") as fh:
	long_description = fh.read()

dependency_links = []
if sys.version_info[0] == 3:
	dependency_links.append("git+https://github.com/oblalex/gnuplot.py-py3k.git")

gui_extras = ["pyqtgraph"]
plot_extras = ["gnuplot-py", "networkx", "PIL", "cmocean"]
ode_extras = ["pygsl"]
full_extras = gui_extras + plot_extras + ode_extras

setuptools.setup(
	name="ggackal",
	version="3.2",
	author="Jacob Laas",
	author_email="jclaas@gmail.com",
	description="A zero-D gas/grain astrochemical model",
	long_description=long_description,
	long_description_content_type="text/markdown",
	url="https://bitbucket.org/notlaast/ggackal/",
	packages=setuptools.find_packages(),
	classifiers=[
		"Programming Language :: Python :: 2",
		"Programming Language :: Python :: 3",
		"License :: OSI Approved :: MIT License",
		"Operating System :: Linux",
	],
	install_requires=[
		"PyYAML",
		"progressbar>=2",
		"Cython",
		"cytoolz",
		"numpy>=1.14",
		"scipy>=0.18",
		"sympy",
		"symengine",
		"matplotlib>=2.2",
	],
	dependency_links=dependency_links,
	extras_require={
		'full': full_extras,
		'gui': gui_extras,
		'plot': plot_extras,
		'ode': ode_extras,
	},
	include_package_data=True, # for copying the extra files during installation
)